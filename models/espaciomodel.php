<?php
include_once 'models/estructura.php';
class EspacioModel extends Model{
    public function __construct(){
    parent::__construct();
    }
    public function existe($espacio){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM ambiente_espacio WHERE descripcion=:descripcion");
            $sql->execute(['descripcion' =>$espacio]);
            $nombre=$sql->fetch();
            
            if($espacio==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
    //echo "<br>insertar datos";
    

    try{
     
             $query=$this->db->connect()->prepare('INSERT INTO ambiente_espacio(descripcion, capacidad, id_espacio_tipo, id_ambiente_detalle,estatus) VALUES
             (:descripcion, :capacidad, :id_espacio_tipo,:id_ambiente_detalle,:estatus)');

            $query->execute(['descripcion'=>$datos['espacio'],'capacidad'=>$datos['capacidad'],'id_espacio_tipo'=>$datos['espaciotipo'],
            'id_ambiente_detalle'=>$datos['ubicacion'], 'estatus'=>$datos['estatus']]);

            return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }
    
    
            }
            
          
            public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT id_ambiente_espacio,ambiente_espacio.descripcion AS espacio,capacidad,ambiente_espacio.estatus AS estatus_ambiente_espacio,espacio_tipo.id_espacio_tipo,espacio_tipo.descripcion AS tipoespacio, ambiente_detalle.descripcion AS estructuraf,ambiente_detalle.estatus AS estatus_estructuraf ,ambiente.id_ambiente,ambiente.descripcion AS ambiente,ambiente_detalle.id_ambiente_detalle FROM ambiente_espacio,ambiente_detalle,espacio_tipo,ambiente WHERE ambiente_espacio.id_ambiente_detalle=ambiente_detalle.id_ambiente_detalle AND ambiente_espacio.id_espacio_tipo=espacio_tipo.id_espacio_tipo AND ambiente.id_ambiente=ambiente_detalle.id_ambiente");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_ambiente_espacio=$row['id_ambiente_espacio'];
              $item->espacio=$row['espacio'];
              $item->capacidad=$row['capacidad'];
              $item->estatus_ambiente_espacio=$row['estatus_ambiente_espacio'];          
              $item->id_espacio_tipo=$row['id_espacio_tipo'];
              $item->tipoespacio=$row['tipoespacio'];
              $item->id_ambiente=$row['id_ambiente'];

              $item->estructuraf=$row['estructuraf'];
              $item->ambiente=$row['ambiente'];
              $item->id_ambiente_detalle=$row['id_ambiente_detalle'];

              $item->estatus_estructuraf=$row['estatus_estructuraf'];     
              
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }
              public function getEstructuraFisica(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT  id_ambiente_detalle,ambiente_detalle.descripcion AS ambiente_detalle,ambiente_detalle.estatus AS estructuraf_estatus,ambiente.id_ambiente,ambiente.descripcion AS ambiente FROM ambiente_detalle,ambiente WHERE ambiente_detalle.id_ambiente=ambiente.id_ambiente AND ambiente_detalle.estatus='1'");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_ambiente_detalle=$row['id_ambiente_detalle'];
              $item->ambiente_detalle=$row['ambiente_detalle'];
              $item->id_ambiente=$row['id_ambiente'];
              $item->ambiente=$row['ambiente'];
            
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }
/*
            public function getAmbiente(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT id_ambiente,ambiente.descripcion AS ambiente,ambiente.codigo AS codigo,direccion,coordenada_latitud, coordenada_longitd, parroquia.id_parroquia AS id_parroquia,parroquia.descripcion AS parroquia, ambiente.estatus AS estatus FROM ambiente,parroquia WHERE ambiente.id_parroquia=parroquia.id_parroquia");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_ambiente=$row['id_ambiente'];
              $item->ambiente=$row['ambiente'];
              $item->codigo=$row['codigo'];
              $item->direccion=$row['direccion'];          
              $item->coordenada_latitud=$row['coordenada_latitud'];
              $item->coordenada_longitd=$row['coordenada_longitd'];
              $item->id_parroquia=$row['id_parroquia'];
              $item->parroquia=$row['parroquia'];
              $item->estatus=$row['estatus'];
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
            }*/


            public function getEspacioTipo(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT id_espacio_tipo,descripcion FROM espacio_tipo");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_espacio_tipo=$row['id_espacio_tipo'];
              $item->descripcion=$row['descripcion'];

              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }

 


              public function update($item){
   



         //  var_dump($item['parroquia'],$item['codigoine'],$item['id_municipio'],$item['id_eje_municipal'],$item['id_parroquia']);
              $query=$this->db->connect()->prepare("UPDATE ambiente_espacio SET descripcion= :descripcion,capacidad= :capacidad,id_espacio_tipo=:id_espacio_tipo,id_ambiente_detalle=:id_ambiente_detalle,estatus=:estatus WHERE id_ambiente_espacio= :id_ambiente_espacio");
            try{
                        $query->execute([
                        'descripcion'=>$item['espacio'],
                        'capacidad'=>$item['capacidad'],                        
                        'id_espacio_tipo'=>$item['espaciotipo'],
                        'estatus'=>$item['estatus'],
                        'id_ambiente_detalle'=>$item['estructuraf'],                        
                        'id_ambiente_espacio'=>$item['id_ambiente_espacio'],

  
                        
                        ]);
            return true;
                       
                        
            }catch(PDOException $e){
                return false;
                 }
            
                }


    }

    ?>