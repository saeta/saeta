<?php
include_once 'datosacademicos/nivel_academico.php';
include_once 'datosacademicos/grado_academico.php';

class GradoacademicoModel extends Model{
    public function __construct(){
        parent::__construct();
    }


    public function existe($grado_academico){
        try{
    
            //var_dump($grado_academico);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM grado_academico WHERE descripcion=:descripcion");
            $sql->execute(['descripcion' =>$grado_academico]);
            $nombre=$sql->fetch();
            
            if($grado_academico==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }

    public function insert($datos){
        //echo "<br>insertar datos";
    
        try{
         
            $query=$this->db->connect()->prepare('INSERT INTO grado_academico(
                descripcion, 
                id_nivel_academico, 
                codigo_ine) VALUES(
                    :descripcion, 
                    :id_nivel_academico, 
                    :codigo_ine)');
    
                $query->execute([
                    'descripcion'=>$datos['descripcion'],
                    'id_nivel_academico'=>$datos['id_nivel_academico'],
                    'codigo_ine'=>$datos['codigo_ine']
                ]);
    
                return true;
          
        }catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                 }
        
        
    }

    public function getNivel(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT id_nivel_academico, descripcion, codigo_ine  FROM nivel_academico");
            
            while($row=$query->fetch()){
                
                $item=new NivelAcademico();
                $item->id_nivel_academico=$row['id_nivel_academico'];
                $item->descripcion=$row['descripcion'];
                $item->codigo_ine=$row['codigo_ine'];
            
                array_push($items,$item);
            
            
            }
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }

    public function getGrado(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT 
            id_grado_academico as id_grado, 
            grado_academico.descripcion AS grado,  
            grado_academico.id_nivel_academico as id_nivel, 
            nivel_academico.descripcion AS nivel, 
            grado_academico.codigo_ine as codigo 
            FROM grado_academico, nivel_academico 
            WHERE  grado_academico.id_nivel_academico=nivel_academico.id_nivel_academico");
            
            while($row=$query->fetch()){
                
                $item=new Grado_Academico();
                $item->id_grado_academico=$row['id_grado'];
                $item->descripcion=$row['grado'];
                $item->id_nivel_academico=$row['id_nivel'];
                $item->descripcion_nivel=$row['nivel'];

                $item->codigo_ine=$row['codigo'];
            
                array_push($items,$item);
            
            
            }
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }
      
    public function update($datos){
  // var_dump($datos);
        //var_dump($item['id_pais'],$item['estado'],$item['codigoine'],$item['id_estado']);
        $query=$this->db->connect()->prepare("UPDATE grado_academico 
        SET descripcion=:descripcion, 
        id_nivel_academico=:id_nivel_academico, 
        codigo_ine=:codigo_ine WHERE id_grado_academico=:id_grado_academico");
        
        try{
            $query->execute([
                'id_grado_academico'=>$datos['id_grado_academico'],
                'descripcion'=>$datos['descripcion'],
                'id_nivel_academico'=>$datos['id_nivel_academico'],
                'codigo_ine'=>$datos['codigo_ine']          
                     ]);
         return true;

        }catch(PDOException $e){
             return false;
        }
         
    }

        public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM grado_academico WHERE id_grado_academico = :id_grado_academico");
    
            try{
                $query->execute([
                'id_grado_academico'=>$id,
                ]);
                return true;
            }catch(PDOException $e){
                return false;
            }
        }
    }


    ?>