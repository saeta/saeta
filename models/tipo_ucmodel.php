
<?php
include_once 'datosacademicos/tipouc.php';

class Tipo_ucModel extends Model{
    public function __construct(){
        parent::__construct();
    }


    public function existe($tipo){
        try{
    
            //var_dump($tipo);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM unidad_curricular_tipo WHERE descripcion=:descripcion");
            $sql->execute(['descripcion' =>$tipo]);
            $nombre=$sql->fetch();
            
            if($tipo==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }

    public function insert($datos){
        //echo "<br>insertar datos";
    
        try{
         
            $query=$this->db->connect()->prepare('INSERT INTO unidad_curricular_tipo(descripcion) VALUES(:descripcion)');
    
                $query->execute([
                    'descripcion'=>$datos['descripcion'],
                ]);
    
                return true;
          
        }catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                 }
        
        
    }

    

    public function getTipo(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT * FROM unidad_curricular_tipo");
            
            while($row=$query->fetch()){
                
                $item=new TipoUC();
                $item->id_unidad_curricular_tipo=$row['id_unidad_curricular_tipo'];
                $item->descripcion=$row['descripcion'];
            
                array_push($items,$item);
            
            
            }
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }
      
    public function update($datos){
   //var_dump($datos);
        //var_dump($item['id_pais'],$item['estado'],$item['codigoine'],$item['id_estado']);
        $query=$this->db->connect()->prepare("UPDATE unidad_curricular_tipo SET descripcion=:descripcion
        WHERE id_unidad_curricular_tipo=:id_unidad_curricular_tipo");
        
        try{
            $query->execute([
                'id_unidad_curricular_tipo'=>$datos['id_tipo_uc'],
                'descripcion'=>$datos['descripcion'],         
                     ]);
         return true;

        }catch(PDOException $e){
             return false;
        }
         
    }

        public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM unidad_curricular_tipo WHERE id_unidad_curricular_tipo = :id_unidad_curricular_tipo");
    
            try{
                $query->execute([
                'id_unidad_curricular_tipo'=>$id
                ]);
                return true;
            }catch(PDOException $e){
                return false;
            }
        }
    }


    ?>