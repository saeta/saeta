<?php
include_once 'datosacademicos/aldea.php';

include_once 'datosacademicos/aldeaprograma.php';
include_once 'datosacademicos/programa_formacion.php';

class H_aldea_programaModel extends Model{
    public function __construct(){
        parent::__construct();
    }

   


   

    public function getAldea(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT * FROM aldea");
            
            while($row=$query->fetch()){
                
                $item=new Aldea();
                $item->id_aldea=$row['id_aldea'];
                $item->descripcion=$row['descripcion'];
                $item->codiga_sucre=$row['codigo_sucre'];
                $item->codigo_aldea=$row['codigo_aldea'];
                $item->estatus=$row['estatus'];
                $item->id_aldea_tipo=$row['id_aldea_tipo'];

                array_push($items,$item);
            
            
            }
            //var_dump($items);
            return $items;
      
        }catch(PDOException $e){
            return[];
        }
      
}
public function getProgramaF(){
    $items=[];
    try{
        $query=$this->db->connect()->query("SELECT 
        id_programa,descripcion,codigo,id_centro_estudio,id_programa_tipo,id_nivel_academico,id_linea_investigacion,estatus
        FROM programa");
    
        while($row=$query->fetch()){
        
            $item=new Programa();
            $item->id_programa=$row['id_programa'];
            $item->descripcion=$row['descripcion'];
            $item->codigo=$row['codigo'];
            $item->id_centro_estudio=$row['id_centro_estudio'];
            $item->id_programa_tipo=$row['id_programa_tipo'];
            $item->id_nivel_academico=$row['id_nivel_academico'];
            $item->id_linea_investigacion=$row['id_linea_investigacion'];
            $item->estatus=$row['estatus'];
    
            array_push($items,$item);
    
    
        }
        return $items;

    }catch(PDOException $e){
    return[];
}

}

    public function getAldeaP(){
            $items=[];
            try{
                $query=$this->db->connect()->query("SELECT * FROM history_aldea_programa");
                
                while($row=$query->fetch()){
                    
                    $item=new AldeaPrograma();
                    $item->id_aldea_programa=$row['id_aldea_programa'];
                    $item->id_aldea=$row['id_aldea'];
                    $item->descripcion_aldea=$row['descripcion_aldea'];
                    $item->id_programa=$row['id_programa'];
                    $item->descripcion_programa=$row['descripcion_programa'];
                    $item->codigo_opsu=$row['codigo_opsu'];
                    $item->tiempo=$row['hora'];
                    //$item->estatus=$row['estatus'];
                    if($row['estatus'] == 1){
                        $item->estatus="Activo";
                    }elseif($row['estatus'] == 0){
                        $item->estatus="Inactivo";
                    }
                    //var_dump($item->estatus);
                
                    array_push($items,$item);
                
                
                }
                return $items;
          
            }catch(PDOException $e){
            return[];
            }
          
    }

   
           
  

}

?>