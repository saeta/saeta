<?php
include_once 'datosacademicos/docente.php';
include_once 'datosacademicos/comitetipo.php';
include_once 'datosacademicos/programa_formacion.php';

class ComitepfaintegranteModel extends Model{
    public function __construct(){
        parent::__construct();
    }

    public function existe($comitepfa_integrante){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT fecha_inicio FROM comitepfa_integrante WHERE  fecha_inicio=:fecha_inicio");
            $sql->execute(['fecha_inicio' =>$comitepfa_integrante]);
            $nombre=$sql->fetch();
            
            if($comitepfa_integrante==$nombre['fecha_inicio']){
                return $nombre['fecha_inicio'];
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
        //echo "<br>insertar datos";
        try{
            
            $query=$this->db->connect()->prepare('INSERT INTO comitepfa_integrante(
                fecha_inicio,
                fecha_fin,
                estatus,
                id_programa,
                id_docente,
                id_comite_tipo
                ) VALUES(:fecha_inicio,
                         :fecha_fin,
                         :estatus,
                         :id_programa,
                         :id_docente, 
                         :id_comite_tipo)');
           
            $query->execute([
                'fecha_inicio'=>$datos['fecha_inicio'],
                'fecha_fin'=>$datos['fecha_fin'],
                'estatus'=>$datos['estatus'],
                'id_programa'=>$datos['id_programa'],
                'id_docente'=>$datos['id_docente'],
                'id_comite_tipo'=>$datos['id_comite_tipo'],
                
                ]);
            
            return true;
       
        }
        catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                
        }
  
    }

    public function getDocente(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT 
            id_docente,
            id_programa,
            id_centro_estudio,
            id_persona,
            id_eje_regional,
            id_docente_dedicacion,
            id_docente_estatus,
            tipo_docente FROM docente");
            
            while($row=$query->fetch()){
                
                $item=new Docente();
                $item->id_docente=$row['id_docente'];
                $item->id_programa=$row['id_programa'];
                $item->id_centro_estudio=$row['id_centro_estudio'];
                $item->id_persona=$row['id_persona'];
                $item->id_eje_regional=$row['id_eje_regional'];
                $item->id_docente_dedicacion=$row['id_docente_dedicacion'];
                $item->id_docente_estatus=$row['id_docente_estatus'];
                $item->tipo_docente=$row['tiṕo_docente'];
            
                array_push($items,$item);
            
            
            }
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
}

    public function getComitetipo(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT id_comite_tipo,descripcion FROM comite_tipo");
        
            while($row=$query->fetch()){
            
                $item=new Comitetipo();
                $item->id_comite_tipo=$row['id_comite_tipo'];
                $item->descripcion=$row['descripcion'];
        
                array_push($items,$item);
        
        
            }
            return $items;
  
        }catch(PDOException $e){
        return[];
    }
  
}
    public function getProgramaF(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT 
            id_programa,descripcion,codigo,id_centro_estudio,id_programa_tipo,id_nivel_academico,id_linea_investigacion,estatus
            FROM programa");
    
            while($row=$query->fetch()){
        
                $item=new ProgramaFormacion();
                $item->id_programa=$row['id_programa'];
                $item->descripcion=$row['descripcion'];
                $item->codigo=$row['codigo'];
                $item->id_centro_estudio=$row['id_centro_estudio'];
                $item->id_programa_tipo=$row['id_programa_tipo'];
                $item->id_nivel_academico=$row['id_nivel_academico'];
                $item->id_linea_investigacion=$row['id_linea_investigacion'];
                $item->estatus=$row['estatus'];
    
                array_push($items,$item);
    
    
            }
            return $items;

        }catch(PDOException $e){
        return[];
    }

}

    public function getComitepfa(){
            $items=[];
            try{
                $query=$this->db->connect()->query("SELECT 
                id_comitepfa_integrante as id_comitepfa,
                comitepfa_integrante.fecha_inicio AS fecha_inicio,
                comitepfa_integrante.fecha_fin as fecha_fin,
                comitepfa_integrante.estatus as estatus,
                comitepfa_integrante.id_programa as id_programa,
                programa.descripcion AS programa,
                comitepfa_integrante.id_docente as id_docente,
                docente.descripcion as docente,
                comitepfa_integrante.id_comite_tipo as id_comite_tipo,
                comite_tipo.descripcion AS comite_tipo
                FROM comitepfa_integrante, programa, docente, comite_tipo
                WHERE comitepfa_integrante.id_programa=programa.id_programa");
                
                while($row=$query->fetch()){
                    
                    $item=new Comitepfa();
                    $item->id_comitepfa_integrante=$row['id_comitepfa'];
                    $item->fecha_inicio=$row['fecha_inicio'];
                    $item->fecha_fin=$row['fecha_fin'];
                    //$item->estatus=$row['estatus'];
                    if($row['estatus'] == 1){
                        $item->estatus="Activo";
                    }elseif($row['estatus'] == 0){
                        $item->estatus="Inactivo";
                    }
                    $item->id_programa=$row['id_programa'];
                    $item->descripcion_programa=$row['programa'];
                    $item->id_docente=$row['id_docente'];
                    $item->descripcion_docente=$row['docente'];
                    $item->id_comite_tipo=$row['id_comite_tipo'];
                    $item->descripcion_comite_tipo=$row['comite_tipo'];
                    
                    //var_dump($item->estatus);
                
                    array_push($items,$item);
                
                
                }
                return $items;
          
            }catch(PDOException $e){
            return[];
            }
          
    }

    public function update($datos){
            //var_dump($datos);
                  //var_dump($item['id_pais'],$item['estado'],$item['fecha_finine'],$item['id_estado']);
                  $query=$this->db->connect()->prepare("UPDATE comitepfa_integrante 
                   SET fecha_inicio=:fecha_inicio,
                   fecha_fin=:fecha_fin,
                   estatus=:estatus,
                   id_programa=:id_programa,
                   id_docente=:id_docente,
                   id_comite_tipo=:id_comite_tipo
                   WHERE id_comitepfa_integrante=:id_comitepfa_integrante");
                  
                  try{
                      $query->execute([
                          'id_comitepfa_integrante'=>$datos['id_comitepfa_integrante'],
                          'fecha_inicio'=>$datos['fecha_inicio'],
                          'fecha_fin'=>$datos['fecha_fin'],
                          'estatus'=>$datos['estatus'],
                          'id_programa'=>$datos['id_programa'],
                          'id_docente'=>$datos['id_docente'],
                          'id_comite_tipo'=>$datos['id_comite_tipo'],
                          
                          
                            
                               ]);
                   return true;
          
                  }catch(PDOException $e){
                       return false;
                  }
                   
    }

    public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM comitepfa_integrante WHERE id_comitepfa_integrante = :id_comitepfa_integrante");
        
                try{
                $query->execute([
                    'id_comitepfa_integrante'=>$id,
                    ]);
                        return true;
        
                }catch(PDOException $e){
        return false;
    }
           
  }

}

?>