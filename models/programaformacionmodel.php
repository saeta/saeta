<?php
include_once 'datosacademicos/programa_formacion.php';
include_once 'datosacademicos/centro_estudio.php';
include_once 'datosacademicos/programa_tipo.php';
include_once 'datosacademicos/nivel_academico.php';
include_once 'datosacademicos/lineainvestigacion.php';


class ProgramaformacionModel extends Model{
    public function __construct(){
        parent::__construct();
    }

    public function existe($programa){
        try{

            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM programa WHERE  descripcion=:descripcion");
            $sql->execute(['descripcion' =>$programa]);
            $nombre=$sql->fetch();

            if($programa==$nombre['descripcion']){
                return $nombre['descripcion'];
            }
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
        //echo "<br>insertar datos";
        try{

            $query=$this->db->connect()->prepare('INSERT INTO programa(
                descripcion,
                codigo,
                id_centro_estudio,
                id_programa_tipo,
                id_nivel_academico,
                id_linea_investigacion,
                estatus
                ) VALUES(:descripcion,
                         :codigo,
                         :id_centro_estudio,
                         :id_programa_tipo,
                         :id_nivel_academico,
                         :id_linea_investigacion,
                         :estatus )');

            $query->execute([
                'descripcion'=>$datos['descripcion'],
                'codigo'=>$datos['codigo'],
                'id_centro_estudio'=>$datos['id_centro_estudio'],
                'id_programa_tipo'=>$datos['id_programa_tipo'],
                'id_nivel_academico'=>$datos['id_nivel_academico'],
                'id_linea_investigacion'=>$datos['id_linea_investigacion'],
                'estatus'=>$datos['estatus']

                ]);

            return true;

        }
        catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;

        }

    }

    public function getCentro(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT
            id_centro_estudio, descripcion, codigo, id_area_conocimiento_opsu, estatus
            FROM centro_estudio");

            while($row=$query->fetch()){

                $item=new Centro();
                $item->id_centro_estudio=$row['id_centro_estudio'];
                $item->descripcion=$row['descripcion'];
                $item->codigo=$row['codigo'];
                $item->id_area_conocimiento_opsu=$row['id_area_conocimiento_opsu'];
                $item->estatus=$row['estatus'];

                array_push($items,$item);

            }
            return $items;

        }catch(PDOException $e){
        return[];
        }

}

public function getTipo(){
    $items=[];
    try{
        $query=$this->db->connect()->query("SELECT
        id_programa_tipo, descripcion, codigo, id_programa_nivel
        FROM programa_tipo");

        while($row=$query->fetch()){

            $item=new Tipo();
            $item->id_programa_tipo=$row['id_programa_tipo'];
            $item->descripcion=$row['descripcion'];
            $item->codigo=$row['codigo'];
            $item->id_programa_nivel=$row['id_programa_nivel'];

            array_push($items,$item);

        }
        return $items;

    }catch(PDOException $e){
    return[];
    }

}

public function getNivel(){
    $items=[];
    try{
        $query=$this->db->connect()->query("SELECT
        id_nivel_academico, descripcion, codigo_ine
        FROM nivel_academico");

        while($row=$query->fetch()){

            $item=new NivelAcademico();
            $item->id_nivel_academico=$row['id_nivel_academico'];
            $item->descripcion=$row['descripcion'];
            $item->codigo_ine=$row['codigo_ine'];

            array_push($items,$item);

        }
        return $items;

    }catch(PDOException $e){
    return[];
    }
}
public function getLinea(){
    $items=[];
    try{
        $query=$this->db->connect()->query("SELECT
        id_linea_investigacion,
        descripcion,
        especificacion,
        id_nucleo_academico
        FROM linea_investigacion");
        while($row=$query->fetch()){
            $item=new LineaInvestigacion();
            $item->id_linea_investigacion=$row['id_linea_investigacion'];
            $item->descripcion=$row['descripcion'];
            $item->especificacion=$row['especificacion'];
            $item->id_nucleo_academico=$row['id_nucleo_academico'];
            array_push($items,$item);
        }
        return $items;

    }catch(PDOException $e){
    return[];
}

}


public function getPrograma(){
    $items=[];
    try{
        $query=$this->db->connect()->query("SELECT 
        id_programa as id_programa,
        programa.descripcion as programa,
        programa.codigo as codigo,
        programa.id_centro_estudio as id_centro,
        centro_estudio.descripcion as centro,
        programa.id_programa_tipo as id_tipo,
        programa_tipo.descripcion as tipo,
        programa.id_nivel_academico as id_nivel,
        nivel_academico.descripcion as nivel,
        programa.id_linea_investigacion as id_linea,
        linea_investigacion.descripcion as linea,
        programa.estatus as estatus
        FROM programa, centro_estudio, programa_tipo, nivel_academico, linea_investigacion
        WHERE programa.id_centro_estudio=centro_estudio.id_centro_estudio
        and programa.id_programa_tipo=programa_tipo.id_programa_tipo
        and programa.id_nivel_academico=nivel_academico.id_nivel_academico
        and programa.id_linea_investigacion=linea_investigacion.id_linea_investigacion");

        while($row=$query->fetch()){

            $item=new Programa();
            $item->id_programa=$row['id_programa'];
            $item->descripcion=$row['programa'];
            $item->codigo=$row['codigo'];
            $item->id_centro_estudio=$row['id_centro'];
            $item->descripcion_centro=$row['centro'];
            $item->id_programa_tipo=$row['id_tipo'];
            $item->descripcion_tipo=$row['tipo'];
            $item->id_nivel_academico=$row['id_nivel'];
            $item->descripcion_nivel=$row['nivel'];
            $item->id_linea_investigacion=$row['id_linea'];
            $item->descripcion_linea=$row['linea'];
            if($row['estatus'] == 1){
                $item->estatus="Activo";
            }elseif($row['estatus'] == 0){
                $item->estatus="Inactivo";
            }

            array_push($items,$item);

        }
        return $items;

    }catch(PDOException $e){
    return[];
}

}

    public function update($datos){
            //var_dump($datos);
                  //var_dump($item['id_pais'],$item['estado'],$item['codigoine'],$item['id_estado']);
                  $query=$this->db->connect()->prepare("UPDATE programa
                   SET descripcion=:descripcion,
                   codigo=:codigo,
                   id_centro_estudio=:id_centro_estudio,
                   id_programa_tipo=:id_programa_tipo,
                   id_nivel_academico=:id_nivel_academico,
                   id_linea_investigacion=:id_linea_investigacion,
                   estatus=:estatus
                   WHERE id_programa=:id_programa");

                  try{
                      $query->execute([
                          'id_programa'=>$datos['id_programa'],
                          'descripcion'=>$datos['descripcion'],
                          'codigo'=>$datos['codigo'],
                          'id_centro_estudio'=>$datos['id_centro_estudio'],
                          'id_programa_tipo'=>$datos['id_programa_tipo'],
                          'id_nivel_academico'=>$datos['id_nivel_academico'],
                          'id_linea_investigacion'=>$datos['id_linea_investigacion'],
                          'estatus'=>$datos['estatus']

                        ]);
                   return true;

                  }catch(PDOException $e){
                       return false;
                  }
    }

    public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM programa WHERE id_programa = :id_programa");

                try{
                $query->execute([
                    'id_programa'=>$id,
                    ]);
                        return true;

                }catch(PDOException $e){
        return false;
    }

  }

}

?>