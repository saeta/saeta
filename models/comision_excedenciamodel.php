<?php
//ini_set("display_errors", 1);
include_once 'datosacademicos/comision_excedencia.php';
include_once 'datosacademicos/tipo_info.php';
include_once 'datosacademicos/docente.php';



class Comision_ExcedenciaModel extends Model{
    public function __construct(){
        parent::__construct();
    }

    
    public function insert($datos){
        //echo "<br>insertar datos";
        try{

            $query=$this->db->connect()->prepare('INSERT INTO comision_excedencia(
                id_tipo_info,
                lugar,
                desde,
                hasta,
                designacion_funcion,
                causa,
                aprobacion,
                id_docente,
                estatus_verificacion
                ) VALUES(:id_tipo_info,
                         :lugar,
                         :desde,
                         :hasta,
                         :designacion_funcion,
                         :causa,
                         :aprobacion,
                         :id_docente,
                         :estatus_verificacion )');

            $query->execute([
                'id_tipo_info'=>$datos['id_tipo_info'],
                'lugar'=>$datos['lugar'],
                'desde'=>$datos['desde'],
                'hasta'=>$datos['hasta'],
                'designacion_funcion'=>$datos['designacion_funcion'],
                'causa'=>$datos['causa'],
                'aprobacion'=>$datos['aprobacion'],
                'id_docente'=>$datos['id_docente'],
                'estatus_verificacion'=>'Sin Verificar'

                ]);

            return true;

        }
        catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;

        }

    }

    public function getInfo(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT
            id_tipo_info, descripcion
            FROM tipo_info");

            while($row=$query->fetch()){

                $item=new TipoInfo();
                $item->id_tipo_info=$row['id_tipo_info'];
                $item->descripcion=$row['descripcion'];

                array_push($items,$item);

            }
            return $items;

        }catch(PDOException $e){
        return[];
        }

}

public function getDocente(){
    $items=[];
    try{
        $query=$this->db->connect()->query("SELECT 
        id_docente,
        id_comision_excedencia,
        id_tipo_info,
        id_persona,
        id_eje_regional,
        id_docente_dedicacion,
        id_docente_estatus,
        tipo_docente,
        id_clasificacion_docente,
        id_escalafon
        FROM docente");
    
        while($row=$query->fetch()){
        
            $item=new Docente();
            $item->id_docente=$row['id_docente'];
            $item->id_comision_excedencia=$row['id_comision_excedencia'];
            $item->id_tipo_info=$row['id_tipo_info'];
            $item->id_persona=$row['id_persona'];
            $item->id_eje_regional=$row['id-eje_regional'];
            $item->id_docente_dedicacion=$row['id_docente_dedicacion'];
            $item->id_docente_estatus=$row['id_docente_estatus'];
            $item->tipo_docente=$row['tipo_docente'];
            $item->id_clasificacion_docente=$row['id_clasificacion_docente'];
            $item->id_escalafon=$row['id_escalafon'];
            

            array_push($items,$item);
    
    
        }
        return $items;

    }catch(PDOException $e){
    return[];
}

}


public function getComision($identificacion){
    $items=[];
    try{
        //var_dump('model',$identificacion);
        //var_dump($id_comision_excedencia);
        $query=$this->db->connect()->prepare("SELECT 
        identificacion,
        primer_nombre,
        primer_apellido,
        id_comision_excedencia as id_comision_excedencia,
        comision_excedencia.id_tipo_info as id_tipo_info,
        tipo_info.descripcion as tipo_info,
        comision_excedencia.lugar as lugar,
        comision_excedencia.desde as desde,
        comision_excedencia.hasta as hasta,
        comision_excedencia.designacion_funcion as designacion_funcion,
        comision_excedencia.causa as causa,
        comision_excedencia.aprobacion as aprobacion,
        comision_excedencia.id_docente as id_docente,
        comision_excedencia.estatus_verificacion as estatus_verificacion
        FROM comision_excedencia, tipo_info, docente,persona, persona_nacionalidad
        WHERE comision_excedencia.id_tipo_info=tipo_info.id_tipo_info
        and comision_excedencia.id_docente=docente.id_docente
        and docente.id_persona=persona.id_persona 
        and persona_nacionalidad.id_persona=persona.id_persona 
        and identificacion=:identificacion");

        $query->execute(['identificacion'=>$identificacion]);

        while($row=$query->fetch()){

            $item=new Comision();
            $item->id_comision_excedencia=$row['id_comision_excedencia'];
            $item->id_tipo_info=$row['id_tipo_info'];
            $item->descripcion_tipo_info=$row['tipo_info'];
            $item->lugar=$row['lugar'];
            $item->desde=$row['desde'];
            $item->hasta=$row['hasta'];
            $item->designacion_funcion=$row['designacion_funcion'];
            $item->causa=$row['causa'];
            $item->aprobacion=$row['aprobacion'];
            if($row['estatus_verificacion'] == 'Verificado'){
                $item->estatus_verificacion="Verificado";
            }elseif($row['estatus_verificacion'] == 'Sin Verificar'){
                $item->estatus_verificacion="Sin Verificar";
            }
            
            array_push($items,$item);

        }
        return $items;

    }catch(PDOException $e){
        //echo $e->getMessage();
    return[];
}

}

public function getbyId($id){
    $items=[];
    try{
        //var_dump('model',$id);
        $query=$this->db->connect()->prepare("SELECT 
        identificacion,
        primer_nombre,
        primer_apellido,
        id_comision_excedencia as id_comision_excedencia,
        comision_excedencia.id_tipo_info as id_tipo_info,
        tipo_info.descripcion as tipo_info,
        comision_excedencia.lugar as lugar,
        comision_excedencia.desde as desde,
        comision_excedencia.hasta as hasta,
        comision_excedencia.designacion_funcion as designacion_funcion,
        comision_excedencia.causa as causa,
        comision_excedencia.aprobacion as aprobacion,
        comision_excedencia.id_docente as id_docente,
        comision_excedencia.estatus_verificacion as estatus_verificacion
        FROM comision_excedencia, tipo_info, docente,persona, persona_nacionalidad
        WHERE comision_excedencia.id_tipo_info=tipo_info.id_tipo_info
        and comision_excedencia.id_docente=docente.id_docente
        and docente.id_persona=persona.id_persona 
        and persona_nacionalidad.id_persona=persona.id_persona 
        and identificacion=:identificacion
        and id_comision_excedencia=:id_comision_excedencia;");

        $query->execute(['identificacion'=>$_SESSION['identificacion'], 'id_comision_excedencia'=>$id]);
        $item=new Comision();
        while($row=$query->fetch()){

            
            $item->id_comision_excedencia=$row['id_comision_excedencia'];
            $item->id_tipo_info=$row['id_tipo_info'];
            $item->descripcion_tipo_info=$row['tipo_info'];
            $item->lugar=$row['lugar'];
            $item->desde=$row['desde'];
            $item->hasta=$row['hasta'];
            $item->designacion_funcion=$row['designacion_funcion'];
            $item->causa=$row['causa'];
            $item->aprobacion=$row['aprobacion'];
            if($row['estatus_verificacion'] == 'Verificado'){
                $item->estatus_verificacion="Verificado";
            }elseif($row['estatus_verificacion'] == 'Sin Verificar'){
                $item->estatus_verificacion="Sin Verificar";
            }
            
            array_push($items,$item);

        }
        return $item;

    }catch(PDOException $e){
    return[];
}

}

    public function update($datos){
            //var_dump($datos);
                  $query=$this->db->connect()->prepare("UPDATE comision_excedencia
                  SET id_tipo_info=:id_tipo_info,
                  lugar=:lugar,
                  desde=:desde,
                  hasta=:hasta,
                  designacion_funcion=:designacion_funcion,
                  causa=:causa,
                  aprobacion=:aprobacion,
                  estatus_verificacion=:estatus_verificacion
                  WHERE id_comision_excedencia=:id_comision_excedencia");

                    try{
                      $query->execute([
                          'id_comision_excedencia'=>$datos['id_comision_excedencia'],
                          'id_tipo_info'=>$datos['id_tipo_info'],
                          'lugar'=>$datos['lugar'],
                          'desde'=>$datos['desde'],
                          'hasta'=>$datos['hasta'],
                          'designacion_funcion'=>$datos['designacion_funcion'],
                          'causa'=>$datos['causa'],
                          'aprobacion'=>$datos['aprobacion'],
                          'estatus_verificacion'=>'Sin Verificar'
                        ]);
                   return true;

                  }catch(PDOException $e){
                       return false;
                  }
    }

    public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM comision_excedencia WHERE id_comision_excedencia = :id_comision_excedencia");

                try{
                $query->execute([
                    'id_comision_excedencia'=>$id,
                    ]);
                        return true;

                }catch(PDOException $e){
        return false;
    }

  }

}

?>