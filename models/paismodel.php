<?php
include_once 'models/estructura.php';
class PaisModel extends Model{
    public function __construct(){
    parent::__construct();
    }
    public function existe($pais){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM pais WHERE descripcion=:descripcion");
            $sql->execute(['descripcion' =>$pais]);
            $nombre=$sql->fetch();
            
            if($pais==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
    //echo "<br>insertar datos";

    try{
     
             $query=$this->db->connect()->prepare('INSERT INTO pais (descripcion, codigo,  estatus) VALUES
             (:descripcion, :codigo, :estatus)');

            $query->execute(['descripcion'=>$datos['pais'],'codigo'=>$datos['codigoine'],
            'estatus'=>$datos['estatus']]);

            return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }
    
    
            }

            public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT * FROM pais");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_pais=$row['id_pais'];
              $item->descripcion=$row['descripcion'];
              $item->codigo=$row['codigo'];
              $item->estatus=$row['estatus'];
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }


              public function update($item){
   
           //var_dump($item['id_pais'],$item['pais'],$item['codigoine'],$item['siglasine'],$item['estatus']);

                $query=$this->db->connect()->prepare("UPDATE pais SET descripcion= :descripcion,
                codigo= :codigo,
                estatus=:estatus WHERE id_pais= :id_pais");
            try{
                        $query->execute([
                        'descripcion'=>$item['pais'],
                        'codigo'=>$item['codigoine'],
                        'estatus'=>$item['estatus'],
                        'id_pais'=>$item['id_pais']
                        ]);
            return true;
                       
                        
            }catch(PDOException $e){
                return false;
                 }
            
                }



    }

    ?>