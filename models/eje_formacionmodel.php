<?php
include_once 'datosacademicos/eje_formacion.php';

class Eje_formacionModel extends Model{
    public function __construct(){
        parent::__construct();
    }


    public function existe($eje_formacion){
        try{
    
            //var_dump($eje_formacion);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM eje_formacion WHERE descripcion=:descripcion");
            $sql->execute(['descripcion' =>$eje_formacion]);
            $nombre=$sql->fetch();
            
            if($eje_formacion==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }

    public function insert($datos){
        //echo "<br>insertar datos";
    
        try{
         
            $query=$this->db->connect()->prepare('INSERT INTO eje_formacion(
                descripcion, 
                codigo) VALUES(
                    :descripcion, 
                    :codigo)');
    
                $query->execute([
                    'descripcion'=>$datos['descripcion'],
                    'codigo'=>$datos['codigo']
                ]);
    
                return true;
          
        }catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                 }
        
        
    }

    public function getEje(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT id_eje_formacion, descripcion, codigo  FROM eje_formacion");
            
            while($row=$query->fetch()){
                
                $item=new Ejeformacion();
                $item->id_eje_formacion=$row['id_eje_formacion'];
                $item->descripcion=$row['descripcion'];
                $item->codigo=$row['codigo'];
            
                array_push($items,$item);
            
            
            }
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }

    
      
    public function update($datos){
  
  //var_dump($datos);
        //var_dump($item['id_pais'],$item['estado'],$item['codigoine'],$item['id_estado']);
        $query=$this->db->connect()->prepare("UPDATE eje_formacion SET descripcion=:descripcion, 
        codigo=:codigo WHERE id_eje_formacion=:id_eje_formacion");
        
        try{
            $query->execute([
                'id_eje_formacion'=>$datos['id_eje_formacion'],
                'descripcion'=>$datos['descripcion'],
                'id_eje_formacion'=>$datos['id_eje_formacion'],
                'codigo'=>$datos['codigo']          
                     ]);
         return true;

        }catch(PDOException $e){
             return false;
        }
         
    }

        public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM eje_formacion WHERE id_eje_formacion = :id_eje_formacion");
    
            try{
                $query->execute([
                'id_eje_formacion'=>$id,
                ]);
                return true;
            }catch(PDOException $e){
                return false;
            }
        }
    }


    ?>