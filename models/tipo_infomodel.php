
<?php
include_once 'datosacademicos/tipo_info.php';

class Tipo_infoModel extends Model{
    public function __construct(){
        parent::__construct();
    }


    public function existe($tipo){
        try{
    
            //var_dump($tipo);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM tipo_info WHERE descripcion=:descripcion");
            $sql->execute(['descripcion' =>$tipo]);
            $nombre=$sql->fetch();
            
            if($tipo==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }

    public function insert($datos){
        //echo "<br>insertar datos";
    
        try{
         
            $query=$this->db->connect()->prepare('INSERT INTO tipo_info(descripcion) VALUES(:descripcion)');
    
                $query->execute([
                    'descripcion'=>$datos['descripcion'],
                ]);
    
                return true;
          
        }catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                 }
        
        
    }

    

    public function getInfo(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT * FROM tipo_info");
            
            while($row=$query->fetch()){
                
                $item=new TipoInfo();
                $item->id_tipo_info=$row['id_tipo_info'];
                $item->descripcion=$row['descripcion'];
            
                array_push($items,$item);
            
            
            }
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }
      
    public function update($datos){
   //var_dump($datos);
        //var_dump($item['id_pais'],$item['estado'],$item['codigoine'],$item['id_estado']);
        $query=$this->db->connect()->prepare("UPDATE tipo_info SET descripcion=:descripcion
        WHERE id_tipo_info=:id_tipo_info");
        
        try{
            $query->execute([
                'id_tipo_info'=>$datos['id_tipo_info'],
                'descripcion'=>$datos['descripcion'],         
                     ]);
         return true;

        }catch(PDOException $e){
             return false;
        }
         
    }

        public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM tipo_info WHERE id_tipo_info = :id_tipo_info");
    
            try{
                $query->execute([
                'id_tipo_info'=>$id
                ]);
                return true;
            }catch(PDOException $e){
                return false;
            }
        }
    }


    ?>