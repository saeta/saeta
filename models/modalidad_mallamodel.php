<?php
include_once 'datosacademicos/modalidadmalla.php';

class Modalidad_mallaModel extends Model{
    public function __construct(){
        parent::__construct();
    }


    public function existe($modalidad_malla){
        try{
    
            //var_dump($modalidad_malla);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM modalidad_malla WHERE descripcion=:descripcion");
            $sql->execute(['descripcion' =>$modalidad_malla]);
            $nombre=$sql->fetch();
            
            if($modalidad_malla==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }

    public function insert($datos){
        //echo "<br>insertar datos";
    
        try{
         
            $query=$this->db->connect()->prepare('INSERT INTO modalidad_malla(descripcion) VALUES(:descripcion)');
    
                $query->execute([
                    'descripcion'=>$datos['descripcion'],
                ]);
    
                return true;
          
        }catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                 }
        
        
    }

    

    public function getMallas(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT * FROM modalidad_malla");
            
            while($row=$query->fetch()){
                
                $item=new ModalidadMalla();
                $item->id_modalidad_malla=$row['id_modalidad_malla'];
                $item->descripcion=$row['descripcion'];
            
                array_push($items,$item);
            
            
            }
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }
      
    public function update($datos){
  // var_dump($datos);
        //var_dump($item['id_pais'],$item['estado'],$item['codigoine'],$item['id_estado']);
        $query=$this->db->connect()->prepare("UPDATE modalidad_malla SET descripcion=:descripcion
        WHERE id_modalidad_malla=:id_modalidad_malla");
        
        try{
            $query->execute([
                'id_modalidad_malla'=>$datos['id_modalidad_malla'],
                'descripcion'=>$datos['descripcion'],         
                     ]);
         return true;

        }catch(PDOException $e){
             return false;
        }
         
    }

        public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM modalidad_malla WHERE id_modalidad_malla = :id_modalidad_malla");
    
            try{
                $query->execute([
                'id_modalidad_malla'=>$id
                ]);
                return true;
            }catch(PDOException $e){
                return false;
            }
        }
    }


    ?>