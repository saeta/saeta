<?php
include_once 'models/ingpropios.php';
class CuentauModel extends Model{
    public function __construct(){
    parent::__construct();
    }



    public function existe($cuenta){
        try{
    
           // var_dump($cuenta);
            $sql = $this->db->connect()->prepare("SELECT * FROM cuenta_bancaria  WHERE descripcion =:descripcion ");
            $sql->execute(['descripcion' =>$cuenta]);
            $nombre=$sql->fetch();
            
            if($cuenta==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }




    public function insert($datos){
    //echo "<br>insertar datos";
           // var_dump($datos['cuenta']);
           // var_dump($datos['banco']);
            //var_dump($datos['tipo_cuenta']);




    try{
     
             $query=$this->db->connect()->prepare('INSERT INTO cuenta_bancaria (descripcion, id_cuenta_bancaria_tipo , id_banco, estatus) VALUES
             (:descripcion,:id_cuenta_bancaria_tipo,:id_banco,:estatus)');

            $query->execute(['descripcion'=>$datos['cuenta'], 'id_cuenta_bancaria_tipo'=>$datos['tipo_cuenta'],'id_banco'=>$datos['banco'],
            'estatus'=>$datos['estatus']]);
            return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }
    
    
            }

            public function get3(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT id_banco,banco.descripcion as bc FROM banco");
              
              while($row=$query->fetch()){
              $item=new ingpropios();
              $item->id_cuenta_bancaria=$row['id_banco'];
              $item->descripcion=$row['bc'];
              
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }

            public function get2(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT id_cuenta_bancaria_tipo,cuenta_bancaria_tipo.descripcion as cb FROM cuenta_bancaria_tipo");
              
              while($row=$query->fetch()){
              $item=new ingpropios();
              $item->id_cuenta_bancaria=$row['id_cuenta_bancaria_tipo'];
              $item->descripcion=$row['cb'];
              
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }

              public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT estatus,id_cuenta_bancaria,banco.id_banco as banco ,cuenta_bancaria_tipo.descripcion as 
              tipo,cuenta_bancaria.descripcion as cuenta , banco.descripcion as banco1,cuenta_bancaria_tipo.descripcion as tipoc,
              cuenta_bancaria_tipo.id_cuenta_bancaria_tipo as 
              id_tipo
              
              FROM cuenta_bancaria, banco,cuenta_bancaria_tipo where
               cuenta_bancaria.id_cuenta_bancaria_tipo=cuenta_bancaria_tipo.id_cuenta_bancaria_tipo 
               and cuenta_bancaria.id_banco=banco.id_banco  ");
              
              while($row=$query->fetch()){
              $item=new ingpropios();
              $item->id_cuenta_bancaria2=$row['id_cuenta_bancaria'];
              $item->id_cuenta_bancaria=$row['tipo'];
              $item->tipo_cuenta=$row['id_tipo'];
              $item->id_banco=$row['banco'];

              $item->estatus=$row['estatus'];

              $item->banco1=$row['banco1'];
              $item->descripcion=$row['cuenta'];

              $item->descripcion2=$row['tipoc'];
              
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }



             public function update($item){
   
       // var_dump($item['banco2']);
       // var_dump($item['cuenta2']);
       // var_dump($item['tipo_cuenta2']);
        // var_dump($item['estatus2']);
                $query=$this->db->connect()->prepare("UPDATE cuenta_bancaria SET descripcion= :descripcion , estatus= :estatus  WHERE id_cuenta_bancaria= :id_cuenta_bancaria");
            try{
                        $query->execute([
                        'descripcion'=>$item['cuenta2'],
                        'id_cuenta_bancaria'=>$item['id_cuenta_bancaria'],
                        'estatus'=>$item['estatus2'],
                        ]);
            return true;
                       
                        
            }catch(PDOException $e){
                return false;
                 }
            
                }
                
               /* public function delete($id_cuenta_bancaria){
                    $query=$this->db->connect()->prepare("DELETE FROM cuenta_bancaria WHERE id_cuenta_bancaria=:id_cuenta_bancaria");
                
                        try{
                        $query->execute([
                            'id_cuenta_bancaria'=>$id_cuenta_bancaria,
                            ]);
                                return true;
                
                        }catch(PDOException $e){
                return false;
                }
                
                
                }*/


                  


    }

    ?>