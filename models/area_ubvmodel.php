<?php
include_once 'datosacademicos/areaopsu.php';
include_once 'datosacademicos/areaubv.php';

class Area_ubvModel extends Model{
    public function __construct(){
        parent::__construct();
    }


    public function existe($area_conocimiento_ubv){
        try{
    
            //var_dump($area_conocimiento_ubv);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM area_conocimiento_ubv WHERE descripcion=:descripcion");
            $sql->execute(['descripcion' =>$area_conocimiento_ubv]);
            $nombre=$sql->fetch();
            if($area_conocimiento_ubv==$nombre['descripcion']){
                
                return $nombre['descripcion'];
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }

    public function insert($datos){
        //echo "<br>insertar datos";
    
        try{
         //var_dump($datos);
            $query=$this->db->connect()->prepare('INSERT INTO area_conocimiento_ubv(
                descripcion,
                codigo,
                estatus, 
                id_area_conocimiento_opsu
                ) VALUES(
                    :descripcion,
                    :codigo,
                    :estatus,
                    :id_area_conocimiento_opsu
                    )');
    
                $query->execute([
                    'descripcion'=>$datos['descripcion'],
                    'id_area_conocimiento_opsu'=>$datos['id_area_conocimiento_opsu'],
                    'codigo'=>$datos['codigo'],
                    'estatus'=>$datos['estatus']

                ]);
    
                return true;
          
        }catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                 }
        
        
    }

    public function getOpsu(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT id_area_conocimiento_opsu, descripcion, codigo  FROM area_conocimiento_opsu");
            
            while($row=$query->fetch()){
                
                $item=new AreaOpsu();
                $item->id_area_conocimiento_opsu=$row['id_area_conocimiento_opsu'];
                $item->descripcion=$row['descripcion'];
                $item->codigo=$row['codigo'];
            
                array_push($items,$item);
            
            
            }
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }

    public function getUbv(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT 
            id_area_conocimiento_ubv as id_ubv,
             area_conocimiento_ubv.descripcion AS area_ubv,
              area_conocimiento_ubv.codigo as codigo, 
              area_conocimiento_ubv.estatus as estatus, 
              area_conocimiento_ubv.id_area_conocimiento_opsu as id_opsu,
               area_conocimiento_opsu.descripcion AS area_opsu 
               FROM area_conocimiento_ubv, area_conocimiento_opsu WHERE area_conocimiento_ubv.id_area_conocimiento_opsu=area_conocimiento_opsu.id_area_conocimiento_opsu");
            
            while($row=$query->fetch()){
                
                $item=new AreaUbv();
                $item->id_area_conocimiento_ubv=$row['id_ubv'];
                $item->descripcion=$row['area_ubv'];
                $item->codigo=$row['codigo'];
                //$item->estatus=$row['estatus'];
                if($row['estatus'] == 1){
                    $item->estatus="Activo";
                }elseif($row['estatus'] == 0){
                    $item->estatus="Inactivo";
                }
                //var_dump($item->estatus);
                $item->id_area_conocimiento_opsu=$row['id_opsu'];
                $item->descripcion_opsu=$row['area_opsu'];

                $item->codigo=$row['codigo'];
            
                array_push($items,$item);
            
            
            }
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }
      
    public function update($datos){
        //var_dump($datos);
        //var_dump($item['id_pais'],$item['estado'],$item['codigoine'],$item['id_estado']);
        $query=$this->db->connect()->prepare("UPDATE area_conocimiento_ubv 
        SET descripcion=:descripcion,
        codigo=:codigo,
        estatus=:estatus,
        id_area_conocimiento_opsu=:id_area_conocimiento_opsu
         WHERE id_area_conocimiento_ubv=:id_area_conocimiento_ubv");
        
        try{
            $query->execute([
                'id_area_conocimiento_ubv'=>$datos['id_area_conocimiento_ubv'],
                'descripcion'=>$datos['descripcion'],
                'id_area_conocimiento_opsu'=>$datos['id_area_conocimiento_opsu'],
                'codigo'=>$datos['codigo'],
                'estatus'=>$datos['estatus']    
                     ]);
         return true;

        }catch(PDOException $e){
             return false;
        }
         
    }

        public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM area_conocimiento_ubv WHERE id_area_conocimiento_ubv = :id_area_conocimiento_ubv");
    
            try{
                $query->execute([
                'id_area_conocimiento_ubv'=>$id,
                ]);
                return true;
            }catch(PDOException $e){
                return false;
            }
        }
    }


    ?>