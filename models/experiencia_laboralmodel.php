<?php
    include_once 'SED.php';
    include_once 'models/estructura.php';
    include_once 'scripts/files.php';

      //encliptar contraseña
    class Experiencia_laboralModel extends Model{
        public function __construct(){
           parent::__construct();
        }
        
        public function existe(){
            
            try{




            } catch(PDOException $e){
                return false;
            }

        }

        // verificar si una institucion existe 
        public function existe_institucion($descripcion){
            
            try{

                //var_dump($descripcion);
                $sql = $this->db->connect()->prepare("SELECT * FROM institucion WHERE descripcion=:descripcion");
                $sql->execute(['descripcion' =>$descripcion]);
                $nombre=$sql->fetch();
                
                if($descripcion==$nombre['descripcion']){
                    
                    return $nombre;
        
                } 
                return false;



            } catch(PDOException $e){
                return false;
            }

        }

        // verificar si una institucion existe 
        public function existe_cargo($descripcion){
            
            try{

                //var_dump($descripcion);
                $sql = $this->db->connect()->prepare("SELECT * FROM cargo_experiencia WHERE descripcion=:descripcion");
                $sql->execute(['descripcion' =>$descripcion]);
                $nombre=$sql->fetch();
                
                if($descripcion==$nombre['descripcion']){
                    
                    return $nombre;
        
                } 
                return false;



            } catch(PDOException $e){
                return false;
            }

        }

        //obtener paises activos
        public function getPais(){
            $items=[];
            
            try{
                $query=$this->db->connect()->query("SELECT * FROM pais");
          
                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id_pais=$row['id_pais'];
                    $item->descripcion=$row['descripcion'];
                    $item->codigo=$row['codigo'];
                    $item->estatus=$row['estatus'];
          
                    array_push($items,$item);
          
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
        }

        //////////////    funcion que llama datos de tablas catalogos   ///////////////
        public function getCatalogo($valor){
            $items=[];
            try{

                $query=$this->db->connect()->query("SELECT * FROM ".$valor."");
          
                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id=$row['id_'.$valor.''];
                    $item->descripcion=$row['descripcion'];
                            
                    array_push($items,$item);
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
          
        }
        
        //obtener instituciones 
        public function getInstitucion(){
            $items=[];
            
            try{
                $query=$this->db->connect()->query("SELECT 
                experiencia_docente.id_institucion,
                institucion.descripcion AS institucion,
                institucion.id_pais,
                pais.descripcion AS pais,
                codigo,
                id_institucion_tipo
                FROM 
                institucion, 
                pais, 
                experiencia_docente
                WHERE institucion.id_pais=pais.id_pais 
                and experiencia_docente.id_institucion=institucion.id_institucion;
                ");
          
                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id_institucion=$row['id_institucion'];
                    $item->id_pais=$row['id_pais'];
                    $item->descripcion=$row['institucion'];
                    $item->descripcion_pais=$row['pais'];
                    $item->codigo=$row['codigo'];
                    $item->id_institucion_tipo=$row['id_institucion_tipo'];
          
                    array_push($items,$item);
          
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
        }

        public function insert($datos){
            try{
                //var_dump($datos);
                //1. guardas el objeto pdo en una variable
                $pdo=$this->db->connect();

                //2. comienzas transaccion
                $pdo->beginTransaction();

                //si la id_institucion viene vacia queire decir que no existe en nuestra base de datos por ende la insertamos
                if(empty($datos['id_institucion'])){

                    
                    $institucion=$pdo->prepare("INSERT INTO institucion(
                        descripcion, 
                        id_pais, 
                        id_institucion_tipo) VALUES(
                            :descripcion,
                            :id_pais,
                            :id_institucion_tipo
                            );");

                    $institucion->execute([
                        'descripcion'=>$datos['institucion'],
                        'id_pais'=>$datos['id_pais'],
                        'id_institucion_tipo'=>$datos['id_institucion_tipo']
                        ]);
                        
                    $id_institucion="(SELECT id_institucion FROM institucion ORDER BY id_institucion DESC LIMIT 1)";
                
                }else{
                    
                    $id_institucion=$datos['id_institucion'];

                }

                //si la id_cargo viene vacia queire decir que no existe en nuestra base de datos por ende la insertamos
                if(empty($datos['id_cargo_experiencia'])){

                    
                    $cargo=$pdo->prepare("INSERT INTO cargo_experiencia(
                        descripcion) 
                    VALUES
                    (:descripcion);");

                    $cargo->execute([
                        'descripcion'=>$datos['cargo']                        
                        ]);
                        
                    $id_cargo_experiencia="(SELECT id_cargo_experiencia FROM cargo_experiencia ORDER BY id_cargo_experiencia DESC LIMIT 1)";
                
                }else{
                    
                    $id_cargo_experiencia=$datos['id_cargo_experiencia'];

                }

                $experiencia=$pdo->prepare("INSERT INTO experiencia_docente
                    (
                    id_tipo_experiencia, 
                    id_cargo_experiencia, 
                    fecha_ingreso, 
                    fecha_egreso, 
                    id_estado, 
                    id_institucion, 
                    id_docente, 
                    ano_experiencia,
                    estatus_verificacion
                    )
                VALUES (
                :id_tipo_experiencia,
                ".$id_cargo_experiencia.",
                :fecha_ingreso,
                :fecha_egreso,
                :id_estado, 
                ".$id_institucion.",
                :id_docente, 
                :ano_experiencia,
                :estatus_verificacion);");
                $experiencia->execute([
                    'id_tipo_experiencia'=>$datos['id_tipo_experiencia'],
                    'fecha_ingreso'=>$datos['fecha_ingreso'],
                    'fecha_egreso'=>$datos['fecha_egreso'],
                    'id_estado'=>$datos['id_estado'],
                    'id_docente'=>$datos['id_docente'],
                    'ano_experiencia'=>$datos['ano_experiencia'],
                    'estatus_verificacion'=>'Sin Verificar'
                ]);
                
                //experiencia trabajo administrativo==1
                if($datos['id_tipo_experiencia']==1){
                    $admin=$pdo->prepare("INSERT INTO trabajo_administrativo(
                        periodo, 
                        id_experiencia_docente
                        ) VALUES ( 
                            :periodo, 
                            (SELECT id_experiencia_docente FROM experiencia_docente ORDER BY id_experiencia_docente DESC LIMIT 1));
                        ");

                    $admin->execute([
                        'periodo'=>$datos['periodo_admin']
                        ]);
                        //institucion pública
                    if($datos['id_institucion_tipo']==1){

                        $doc=new Documento();
                        
                        $pdf_constancia=$doc->getData($datos['pdf_constancia']);
                        
                        if($doc->mover($pdf_constancia['extension'], 
                                'src/documentos/experiencia_laboral/',
                                $pdf_constancia['ruta_tmp'],
                                $pdf_constancia['original_name'],
                                array('pdf')))
                        {
                            $constancia=$pdo->prepare("INSERT INTO documento(
                                descripcion,
                                estatus,
                                id_persona,
                                id_tipo_documento) 
                                VALUES(
                                    :descripcion,
                                    :estatus, 
                                    :id_persona,
                                    (SELECT id_tipo_documento FROM tipo_documento WHERE descripcion='TITULO_ESTUDIO_CONDUCENTE') 
                                    );");
                            $constancia->execute([
                                'descripcion'=>date('Y-m-d H:i:s').$pdf_constancia['original_name'],
                                'estatus'=>'Sin Verificar',
                                'id_persona'=>$datos['id_persona']
                                ]);
                            
                            $constancia_trabajo=$pdo->query("INSERT INTO constancia_administrativo(
                                id_trabajo_administrativo, 
                                id_documento) 
                                VALUES(
                                (SELECT id_trabajo_administrativo FROM trabajo_administrativo ORDER BY id_trabajo_administrativo DESC LIMIT 1),
                                (SELECT id_documento FROM documento ORDER BY id_documento DESC LIMIT 1));
                            ");

                        }
                    }
                }

                //experiencia docencia previa==2
                if($datos['id_tipo_experiencia']==2){
                    $docencia=$pdo->prepare("INSERT INTO docencia_previa(
                        materia, 
                        id_nivel_academico, 
                        id_modalidad_estudio, 
                        semestre, 
                        periodo_lectivo, 
                        naturaleza, 
                        horas_semanales, 
                        estudiantes_atendidos, 
                        id_experiencia_docente)
                            VALUES (
                                :materia, 
                                :id_nivel_academico, 
                                :id_modalidad_estudio, 
                                :semestre, 
                                :periodo_lectivo, 
                                :naturaleza, 
                                :horas_semanales, 
                                :estudiantes_atendidos, 
                                (SELECT id_experiencia_docente FROM experiencia_docente ORDER BY id_experiencia_docente DESC LIMIT 1));
                    ");

                    $docencia->execute([
                        'materia'=>$datos['materia'],
                        'id_nivel_academico'=>$datos['id_nivel_academico'],
                        'id_modalidad_estudio'=>$datos['id_modalidad_estudio'],
                        'semestre'=>$datos['semestre'],
                        'periodo_lectivo'=>$datos['periodo_lectivo'],
                        'naturaleza'=>$datos['naturaleza'],
                        'horas_semanales'=>$datos['horas_semanales'],
                        'estudiantes_atendidos'=>$datos['estudiantes_atendidos']
                        ]);
                }
                //experiencia comision gubernamental previa==3
                if($datos['id_tipo_experiencia']==3){
                    $comision=$pdo->prepare("INSERT INTO comision_gubernamental(
                        lugar, 
                        id_experiencia_docente)
                            VALUES (
                            :lugar, 
                            (SELECT id_experiencia_docente FROM experiencia_docente ORDER BY id_experiencia_docente DESC LIMIT 1));
                    ");
                    $comision->execute(['lugar'=>$datos['lugar']]);
                }

                $pdo->commit();
                return true;

            }catch(PDOException $e){
                //5. regresas a un estado anterior en caso de error
                $pdo->rollBack();
                echo "Fallo: " . $e->getMessage();
                return false;
            }
        }

        //obtener trabajos administrativos 
        public function getExperienciaAdministrativo($id_docente){
            $items=[];
            
            try{
                
                $query=$this->db->connect()->prepare("SELECT 
                id_trabajo_administrativo,
                institucion.descripcion as institucion,
                ano_experiencia,
                cargo_experiencia.descripcion as cargo,
                periodo,
                institucion_tipo.descripcion as institucion_tipo,
                trabajo_administrativo.id_experiencia_docente,
                experiencia_docente.id_tipo_experiencia,
                tipo_experiencia.descripcion as tipo_experiencia,
                institucion.id_pais,
                pais.descripcion as descripcion_pais,
                experiencia_docente.id_estado,
                estado.descripcion as descripcion_estado,
                estatus_verificacion,
                institucion.id_institucion_tipo
                from 
                trabajo_administrativo, 
                experiencia_docente,
                tipo_experiencia,
                docente,
                institucion,
                institucion_tipo,
                cargo_experiencia,
                pais,
                estado
                where 
                trabajo_administrativo.id_experiencia_docente=experiencia_docente.id_experiencia_docente
                and experiencia_docente.id_tipo_experiencia=tipo_experiencia.id_tipo_experiencia
                and experiencia_docente.id_docente=docente.id_docente
                and experiencia_docente.id_institucion=institucion.id_institucion
                and institucion.id_institucion_tipo=institucion_tipo.id_institucion_tipo
                and experiencia_docente.id_cargo_experiencia=cargo_experiencia.id_cargo_experiencia
                and institucion.id_pais=pais.id_pais
                and experiencia_docente.id_estado=estado.id_estado
                and docente.id_docente=:id_docente");
                $query->execute(['id_docente'=>$id_docente]);

                while($row=$query->fetch()){
                    $item=new Estructura();

                    $item->id_trabajo_administrativo=$row['id_trabajo_administrativo'];
                    $item->institucion=$row['institucion'];
                    $item->ano_experiencia=$row['ano_experiencia'];
                    $item->cargo=$row['cargo'];
                    $item->periodo=$row['periodo'];
                    $item->institucion_tipo=$row['institucion_tipo'];

                    $item->tipo_entidad=$row['tipo_entidad'];
                    $item->id_experiencia_docente=$row['id_experiencia_docente'];
                    $item->id_tipo_experiencia=$row['id_tipo_experiencia'];
                    $item->descripcion_tipoexp=$row['descripcion'];
                    $item->id_institucion_tipo=$row['id_institucion_tipo'];
                    
                    $item->id_pais=$row['id_pais'];
                    $item->descripcion_pais=$row['descripcion_pais'];

                    $item->id_estado=$row['id_estado'];
                    $item->descripcion_estado=$row['descripcion_estado'];

                    $item->estatus_verificacion=$row['estatus_verificacion'];

                    array_push($items,$item);
          
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
        }

        //obtener docencias Previas
        public function getExperienciaDocencia($id_docente){
            $items=[];
            
            try{
                
                //quede aqui
                $query=$this->db->connect()->prepare("SELECT 
                t1.id_experiencia_docente,
                id_docencia_previa,
                t5.descripcion as institucion,
                ano_experiencia,
                t7.descripcion as cargo,
                t6.descripcion as institucion_tipo,
                materia,
                t2.id_tipo_experiencia,
                estatus_verificacion,
                t5.id_institucion_tipo
                from
                docencia_previa as t1,
                experiencia_docente as t2,
                tipo_experiencia as t3,
                docente as t4,
                institucion as t5,
                institucion_tipo as t6,
                cargo_experiencia as t7
                
                where 
                t1.id_experiencia_docente=t2.id_experiencia_docente
                and t2.id_tipo_experiencia=t3.id_tipo_experiencia
                and t2.id_docente=t4.id_docente
                and t2.id_institucion=t5.id_institucion
                and t5.id_institucion_tipo=t6.id_institucion_tipo
                and t2.id_cargo_experiencia=t7.id_cargo_experiencia
                and t4.id_docente=:id_docente
                ");
                $query->execute(['id_docente'=>$id_docente]);

                while($row=$query->fetch()){
                    $item=new Estructura();

                    $item->id_docencia_previa=$row['id_docencia_previa'];
                    $item->institucion=$row['institucion'];
                    $item->ano_experiencia=$row['ano_experiencia'];
                    $item->cargo=$row['cargo'];
                    $item->materia=$row['materia'];
                    $item->institucion_tipo=$row['institucion_tipo'];

                    $item->id_experiencia_docente=$row['id_experiencia_docente'];
                    $item->id_tipo_experiencia=$row['id_tipo_experiencia'];
                    $item->estatus_verificacion=$row['estatus_verificacion'];

          
                    array_push($items,$item);
          
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
        }


        //obtener comisiones gubernamental
        public function getExperienciaComision($id_docente){
            $items=[];
            
            try{
                
                //quede aqui
                $query=$this->db->connect()->prepare("SELECT 
                t1.id_experiencia_docente,
                id_comision_gubernamental,
                t5.descripcion as institucion,
                ano_experiencia,
                t7.descripcion as cargo,
                t6.descripcion as institucion_tipo,
                lugar,
                t2.id_tipo_experiencia,
                estatus_verificacion,
                t5.id_institucion_tipo

                from
                comision_gubernamental as t1,
                experiencia_docente as t2,
                tipo_experiencia as t3,
                docente as t4,
                institucion as t5,
                institucion_tipo as t6,
                cargo_experiencia as t7
                
                where 
                t1.id_experiencia_docente=t2.id_experiencia_docente
                and t2.id_tipo_experiencia=t3.id_tipo_experiencia
                and t2.id_docente=t4.id_docente
                and t2.id_institucion=t5.id_institucion
                and t5.id_institucion_tipo=t6.id_institucion_tipo
                and t2.id_cargo_experiencia=t7.id_cargo_experiencia
                and t4.id_docente=:id_docente
                ");
                $query->execute(['id_docente'=>$id_docente]);

                while($row=$query->fetch()){
                    $item=new Estructura();

                    $item->id_comision_gubernamental=$row['id_comision_gubernamental'];
                    $item->institucion=$row['institucion'];
                    $item->ano_experiencia=$row['ano_experiencia'];
                    $item->cargo=$row['cargo'];
                    $item->lugar=$row['lugar'];
                    $item->institucion_tipo=$row['institucion_tipo'];

                    $item->id_experiencia_docente=$row['id_experiencia_docente'];
                    $item->id_tipo_experiencia=$row['id_tipo_experiencia'];
                    $item->estatus_verificacion=$row['estatus_verificacion'];

          
                    array_push($items,$item);
          
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
        }

        //obtener estudios conducentes y no conducentes
        public function getbyIdAdmin($id){
            try{
                $item = new Estructura();
                $sql = $this->db->connect()->prepare("SELECT 
                experiencia_docente.id_tipo_experiencia,
                tipo_experiencia.descripcion as tipo_experiencia, 
                institucion.id_institucion_tipo,
                periodo,
                cargo_experiencia.descripcion as cargo,
                ano_experiencia,
                institucion.id_pais,
                experiencia_docente.id_estado,
                id_trabajo_administrativo,
                experiencia_docente.id_institucion,
                institucion.descripcion as institucion,
                fecha_ingreso,
                fecha_egreso,
                
                institucion_tipo.descripcion as institucion_tipo,
                trabajo_administrativo.id_experiencia_docente,
                tipo_experiencia.descripcion as tipo_experiencia,
                pais.descripcion as descripcion_pais,
                experiencia_docente.id_estado,
                estado.descripcion as descripcion_estado,
                estatus_verificacion

                from 
                trabajo_administrativo, 
                experiencia_docente,
                tipo_experiencia,
                docente,
                institucion,
                institucion_tipo,
                cargo_experiencia,
                pais,
                estado

                where 
                trabajo_administrativo.id_experiencia_docente=experiencia_docente.id_experiencia_docente
                and experiencia_docente.id_tipo_experiencia=tipo_experiencia.id_tipo_experiencia
                and experiencia_docente.id_docente=docente.id_docente
                and experiencia_docente.id_institucion=institucion.id_institucion
                and institucion.id_institucion_tipo=institucion_tipo.id_institucion_tipo
                and experiencia_docente.id_cargo_experiencia=cargo_experiencia.id_cargo_experiencia
                and institucion.id_pais=pais.id_pais
                and experiencia_docente.id_estado=estado.id_estado
                and trabajo_administrativo.id_trabajo_administrativo=:id_trabajo_administrativo;");

                $sql->execute(['id_trabajo_administrativo'=>$id]);
                
                while($row=$sql->fetch()){

                    $item->id_tipo_experiencia=$row['id_tipo_experiencia'];
                    $item->tipo_experiencia=$row['tipo_experiencia'];

                    $item->id_institucion_tipo=$row['id_institucion_tipo'];
                    $item->periodo=$row['periodo'];
                    $item->cargo=$row['cargo'];

                    $item->ano_experiencia=$row['ano_experiencia'];
                    $item->id_pais=$row['id_pais'];
                    $item->id_estado=$row['id_estado'];
                    $item->id_trabajo_administrativo=$row['id_trabajo_administrativo'];

                    $item->modalidad_estudio=$row['modalidad_estudio'];
                    $item->ano_estudio=$row['ano_estudio'];
                    $item->id_institucion=$row['id_institucion'];

                    $item->institucion=$row['institucion'];
                    $item->institucion_tipo=$row['institucion_tipo'];

                    if($item->id_institucion_tipo==1){//1=institucion publica 
                        $constancia = $this->db->connect()->prepare("SELECT * from constancia_administrativo where id_trabajo_administrativo=:id_trabajo_administrativo;");
                        $constancia->execute(['id_trabajo_administrativo'=>$id]);
                        while($row1=$constancia->fetch()){
                            $item->id_constancia_administrativo=$row1['id_constancia_administrativo'];
                            $item->id_documento=$row1['id_documento'];
                            $item->id_trabajo_administrativo=$row1['id_trabajo_administrativo'];
                        }
    
                    }
                    $item->id_docente=$row['id_docente'];

                    $item->fecha_ingreso=date("m/d/Y", strtotime($row['fecha_ingreso']));
                    $item->fecha_egreso=date("m/d/Y", strtotime($row['fecha_egreso']));
                    
                    $item->descripcion_pais=$row['descripcion_pais'];
                    $item->descripcion_estado=$row['descripcion_estado'];
                    $item->estatus_verificacion=$row['estatus_verificacion'];

                    
                }
                return $item;

            }catch(PDOExcepton $e){
                return null;
            }
        }

        //obtener detalle de una docencia previa
        public function getbyIdDocencia($id){
            try{

                $sql = $this->db->connect()->prepare("SELECT 
                experiencia_docente.id_tipo_experiencia,
                tipo_experiencia.descripcion as tipo_experiencia, 

                institucion.id_institucion_tipo,
                cargo_experiencia.descripcion as cargo,
                ano_experiencia,
                institucion.id_pais,
                experiencia_docente.id_estado,
                experiencia_docente.id_institucion,
                institucion.descripcion as institucion,
                fecha_ingreso,
                fecha_egreso,
                
                institucion_tipo.descripcion as institucion_tipo,
                id_docencia_previa,
                docencia_previa.id_experiencia_docente,
                tipo_experiencia.descripcion as tipo_experiencia,
                pais.descripcion as descripcion_pais,
                experiencia_docente.id_estado,
                estado.descripcion as descripcion_estado,
                
                materia,
                docencia_previa.id_nivel_academico,
                nivel_academico.descripcion as nivel_academico,

                docencia_previa.id_modalidad_estudio,
                modalidad_estudio.descripcion as modalidad_estudio,

                semestre,
                periodo_lectivo,
                naturaleza,
                horas_semanales,
                estudiantes_atendidos,
                estatus_verificacion

                from 
                docencia_previa, 
                experiencia_docente,
                tipo_experiencia,
                docente,
                institucion,
                institucion_tipo,
                cargo_experiencia,
                pais,
                estado,
                nivel_academico,
                modalidad_estudio

                where 
                docencia_previa.id_experiencia_docente=experiencia_docente.id_experiencia_docente
                and experiencia_docente.id_tipo_experiencia=tipo_experiencia.id_tipo_experiencia
                and experiencia_docente.id_docente=docente.id_docente
                and experiencia_docente.id_institucion=institucion.id_institucion
                and institucion.id_institucion_tipo=institucion_tipo.id_institucion_tipo
                and experiencia_docente.id_cargo_experiencia=cargo_experiencia.id_cargo_experiencia
                and institucion.id_pais=pais.id_pais
                and experiencia_docente.id_estado=estado.id_estado
                and docencia_previa.id_nivel_academico=nivel_academico.id_nivel_academico
                and docencia_previa.id_modalidad_estudio=modalidad_estudio.id_modalidad_estudio
                and docencia_previa.id_docencia_previa=:id_docencia_previa;");
                
                $item = new Estructura();
                $sql->execute(['id_docencia_previa'=>$id]);

                while($row=$sql->fetch()){

                    $item->id_tipo_experiencia=$row['id_tipo_experiencia'];
                    $item->tipo_experiencia=$row['tipo_experiencia'];

                    $item->id_institucion_tipo=$row['id_institucion_tipo'];
                    $item->periodo=$row['periodo'];
                    $item->cargo=$row['cargo'];

                    $item->ano_experiencia=$row['ano_experiencia'];
                    $item->id_pais=$row['id_pais'];
                    $item->id_estado=$row['id_estado'];
                    $item->id_docencia_previa=$row['id_docencia_previa'];
                    
                    $item->modalidad_estudio=$row['modalidad_estudio'];
                    $item->nivel_academico=$row['nivel_academico'];

                    $item->ano_estudio=$row['ano_estudio'];
                    $item->id_institucion=$row['id_institucion'];

                    
                    $item->institucion=$row['institucion'];
                    $item->institucion_tipo=$row['institucion_tipo'];

                    
                    $item->id_docente=$row['id_docente'];

                    $item->fecha_ingreso=date("m/d/Y", strtotime($row['fecha_ingreso']));
                    $item->fecha_egreso=date("m/d/Y", strtotime($row['fecha_egreso']));
                    
                    $item->descripcion_pais=$row['descripcion_pais'];
                    $item->descripcion_estado=$row['descripcion_estado'];

                    $item->materia=$row['materia'];
                    $item->id_nivel_academico=$row['id_nivel_academico'];
                    $item->id_modalidad_estudio=$row['id_modalidad_estudio'];
                    $item->semestre=$row['semestre'];
                    $item->periodo_lectivo=$row['periodo_lectivo'];
                    $item->naturaleza=$row['naturaleza'];
                    $item->horas_semanales=$row['horas_semanales'];
                    $item->estudiantes_atendidos=$row['estudiantes_atendidos'];
                    $item->estatus_verificacion=$row['estatus_verificacion'];

                }

                return $item;

            }catch(PDOExcepton $e){
                return null;
            }
        }
        
        //obtener detalle de una docencia previa
        public function getByIdComision($id){
            try{
                $sql = $this->db->connect()->prepare("SELECT 
                experiencia_docente.id_tipo_experiencia,
                tipo_experiencia.descripcion as tipo_experiencia, 

                institucion.id_institucion_tipo,
                cargo_experiencia.descripcion as cargo,
                ano_experiencia,
                institucion.id_pais,
                experiencia_docente.id_estado,
                experiencia_docente.id_institucion,
                institucion.descripcion as institucion,
                fecha_ingreso,
                fecha_egreso,
                
                institucion_tipo.descripcion as institucion_tipo,
                id_comision_gubernamental,
                comision_gubernamental.id_experiencia_docente,
                tipo_experiencia.descripcion as tipo_experiencia,
                pais.descripcion as descripcion_pais,
                experiencia_docente.id_estado,
                estado.descripcion as descripcion_estado,
                lugar,
                estatus_verificacion

                from 
                comision_gubernamental, 
                experiencia_docente,
                tipo_experiencia,
                docente,
                institucion,
                institucion_tipo,
                cargo_experiencia,
                pais,
                estado

                where 
                comision_gubernamental.id_experiencia_docente=experiencia_docente.id_experiencia_docente
                and experiencia_docente.id_tipo_experiencia=tipo_experiencia.id_tipo_experiencia
                and experiencia_docente.id_docente=docente.id_docente
                and experiencia_docente.id_institucion=institucion.id_institucion
                and institucion.id_institucion_tipo=institucion_tipo.id_institucion_tipo
                and experiencia_docente.id_cargo_experiencia=cargo_experiencia.id_cargo_experiencia
                and institucion.id_pais=pais.id_pais
                and experiencia_docente.id_estado=estado.id_estado
                and comision_gubernamental.id_comision_gubernamental=:id_comision_gubernamental;");
                $item = new Estructura();
                $sql->execute(['id_comision_gubernamental'=>$id]);

                while($row=$sql->fetch()){

                    $item->id_tipo_experiencia=$row['id_tipo_experiencia'];
                    $item->tipo_experiencia=$row['tipo_experiencia'];

                    $item->id_institucion_tipo=$row['id_institucion_tipo'];
                    $item->periodo=$row['periodo'];
                    $item->cargo=$row['cargo'];

                    $item->ano_experiencia=$row['ano_experiencia'];
                    $item->id_pais=$row['id_pais'];
                    $item->id_estado=$row['id_estado'];
                    $item->id_docencia_previa=$row['id_docencia_previa'];

                    $item->modalidad_estudio=$row['modalidad_estudio'];
                    $item->ano_estudio=$row['ano_estudio'];
                    $item->id_institucion=$row['id_institucion'];

                    
                    $item->institucion=$row['institucion'];
                    $item->institucion_tipo=$row['institucion_tipo'];

                    
                    $item->id_docente=$row['id_docente'];

                    $item->fecha_ingreso=date("m/d/Y", strtotime($row['fecha_ingreso']));
                    $item->fecha_egreso=date("m/d/Y", strtotime($row['fecha_egreso']));
                    
                    $item->descripcion_pais=$row['descripcion_pais'];
                    $item->descripcion_estado=$row['descripcion_estado'];

                    $item->lugar=$row['lugar'];
                    $item->estatus_verificacion=$row['estatus_verificacion'];

                }

                return $item;


            }catch(PDOExcepton $e){
                return null;
            }
        }


        public function update($datos){
            try{
                //var_dump('modelo>>',$datos['fecha_ingreso']);
                //1. guardas el objeto pdo en una variable
                $pdo=$this->db->connect();
                //2. comienzas transaccion
                $pdo->beginTransaction();
                
                
                //var_dump(empty($datos['id_cargo_experiencia']));
                if(empty($datos['id_institucion'])){

                    
                    $institucion=$pdo->prepare("INSERT INTO institucion(
                        descripcion, 
                        id_pais, 
                        id_institucion_tipo) VALUES(
                            :descripcion,
                            :id_pais,
                            :id_institucion_tipo
                            );");

                    $institucion->execute([
                        'descripcion'=>$datos['institucion'],
                        'id_pais'=>$datos['id_pais'],
                        'id_institucion_tipo'=>$datos['id_institucion_tipo']
                        ]);
                        
                    $id_institucion="(SELECT id_institucion FROM institucion ORDER BY id_institucion DESC LIMIT 1)";
                
                }else{
                    
                    $id_institucion=$datos['id_institucion'];

                }
               
                
                //si la id_cargo viene vacia queire decir que no existe en nuestra base de datos por ende la insertamos
                if(empty($datos['id_cargo_experiencia'])){

                    
                    $cargo=$pdo->prepare("INSERT INTO cargo_experiencia(
                        descripcion) 
                    VALUES
                    (:descripcion);");

                    $cargo->execute([
                        'descripcion'=>$datos['cargo']                        
                        ]);
                        
                    $id_cargo_experiencia="(SELECT id_cargo_experiencia FROM cargo_experiencia ORDER BY id_cargo_experiencia DESC LIMIT 1)";
                
                }else{
                    
                    $id_cargo_experiencia=$datos['id_cargo_experiencia'];
                    
                }

                //var_dump($id_institucion, " ", $id_cargo_experiencia);
                switch($datos['id_tipo_experiencia']){
                    case 1:
                        $exp="UPDATE 
                        experiencia_docente 
                        SET
                        id_cargo_experiencia=".$id_cargo_experiencia.",
                        fecha_ingreso=:fecha_ingreso,
                        fecha_egreso=:fecha_egreso,
                        id_estado=:id_estado,
                        id_institucion=".$id_institucion.",
                        ano_experiencia=:ano_experiencia,
                        estatus_verificacion=:estatus_verificacion
                        FROM trabajo_administrativo
                        WHERE 
                        trabajo_administrativo.id_experiencia_docente=experiencia_docente.id_experiencia_docente
                        and id_trabajo_administrativo=:id_experiencia_tabla;";
                        break;
                    case 2:
                        $exp="UPDATE 
                            experiencia_docente 
                            SET
                            id_cargo_experiencia=".$id_cargo_experiencia.",
                            fecha_ingreso=:fecha_ingreso,
                            fecha_egreso=:fecha_egreso,
                            id_estado=:id_estado,
                            id_institucion=".$id_institucion.",
                            ano_experiencia=:ano_experiencia,
                            estatus_verificacion=:estatus_verificacion
                            FROM docencia_previa
                            WHERE 
                            docencia_previa.id_experiencia_docente=experiencia_docente.id_experiencia_docente
                            and id_docencia_previa=:id_experiencia_tabla;";
                        break;
                    case 3:
                        $exp="UPDATE 
                            experiencia_docente 
                            SET
                            id_cargo_experiencia=".$id_cargo_experiencia.",
                            fecha_ingreso=:fecha_ingreso,
                            fecha_egreso=:fecha_egreso,
                            id_estado=:id_estado,
                            id_institucion=".$id_institucion.",
                            ano_experiencia=:ano_experiencia,
                            estatus_verificacion=:estatus_verificacion
                            FROM comision_gubernamental
                            WHERE 
                            comision_gubernamental.id_experiencia_docente=experiencia_docente.id_experiencia_docente
                            and id_comision_gubernamental=:id_experiencia_tabla;";
                        break;
                }
                
                $experiencia=$pdo->prepare($exp);
                
                $experiencia->execute([
                    'fecha_ingreso'=>$datos['fecha_ingreso'],
                    'fecha_egreso'=>$datos['fecha_egreso'],
                    'id_estado'=>$datos['id_estado'],
                    'ano_experiencia'=>$datos['ano_experiencia'],
                    'id_experiencia_tabla'=>$datos['id_experiencia_tabla'],
                    'estatus_verificacion'=>'Sin Verificar'
                ]);
                
                switch($datos['id_tipo_experiencia']){

                    case 1:
                        $admin=$pdo->prepare("UPDATE 
                        trabajo_administrativo
                        SET
                        periodo=:periodo
                        where id_trabajo_administrativo=:id_experiencia_tabla;");
                        $admin->execute([     
                            'periodo'=>$datos['periodo_admin'],              
                            'id_experiencia_tabla'=>$datos['id_experiencia_tabla']
                        ]);
                        break;

                    case 2:
                        $docencia=$pdo->prepare("UPDATE 
                            docencia_previa
                            SET
                            materia=:materia,
                            id_nivel_academico=:id_nivel_academico,
                            id_modalidad_estudio=:id_modalidad_estudio,
                            semestre=:semestre,
                            periodo_lectivo=:periodo_lectivo,
                            naturaleza=:naturaleza,
                            horas_semanales=:horas_semanales,
                            estudiantes_atendidos=:estudiantes_atendidos
                            where id_docencia_previa=:id_experiencia_tabla");
                        $docencia->execute([
                            'materia'=>$datos['materia'],
                            'id_nivel_academico'=>$datos['id_nivel_academico'],
                            'id_modalidad_estudio'=>$datos['id_modalidad_estudio'],
                            'semestre'=>$datos['semestre'],
                            'periodo_lectivo'=>$datos['periodo_lectivo'],
                            'naturaleza'=>$datos['naturaleza'],
                            'horas_semanales'=>$datos['horas_semanales'],
                            'estudiantes_atendidos'=>$datos['estudiantes_atendidos'],
                            'id_experiencia_tabla'=>$datos['id_experiencia_tabla']
                        ]);
                        break;
                    case 3:
                        $comision=$pdo->prepare("UPDATE 
                        comision_gubernamental 
                        SET
                        lugar=:lugar
                        WHERE 
                        id_comision_gubernamental=:id_experiencia_tabla;");
                        $comision->execute([     
                            'lugar'=>$datos['lugar'],              
                            'id_experiencia_tabla'=>$datos['id_experiencia_tabla']
                        ]);
                        break;

                }

                //4. consignas la transaccion (en caso de que no suceda ningun fallo)
                $pdo->commit();
                return true; 
                

            } catch(PDOException $e){
                //5. regresas a un estado anterior en caso de error
                $pdo->rollBack();
                echo "Fallo: " . $e->getMessage();
                return false;
            }

        }

        public function getDocumentbyID($id){

            try{
                $item = new Estructura();
                $sql = $this->db->connect()->prepare("SELECT 
                id_constancia_administrativo,
                t1.id_trabajo_administrativo,
                t1.id_documento,
                t2.descripcion
                from 
                constancia_administrativo as t1,
                documento as t2
                where 
                t1.id_documento=t2.id_documento
                and t1.id_trabajo_administrativo=:id_trabajo_administrativo          
                ");
    
                
                    $sql->execute(['id_trabajo_administrativo'=>$id]);
                    
    
                    while($row=$sql->fetch()){
    
                        $item->id_constancia_administrativo=$row['id_constancia_administrativo'];
                        $item->id_trabajo_administrativo=$row['id_trabajo_administrativo'];
                        $item->id_documento=$row['id_documento'];
                        $item->descripcion=$row['descripcion'];
                    }
                    return $item;
                }catch(PDOExcepton $e){
                    return null;
                }

        }

        public function delete($datos){
            try{
                //var_dump($datos);
                
                //1. guardas el objeto pdo en una variable
                $pdo=$this->db->connect();

                //2. comienzas transaccion
                $pdo->beginTransaction();

                //1=trabajo administrativo
                switch($datos['id_tipo_experiencia']){
                    case 1:
                        //1=publica
                        if($datos['id_institucion_tipo'] == 1){

                            $pdf=$pdo->prepare("SELECT 
                                constancia_administrativo.id_documento, 
                                descripcion 
                                from 
                                documento, 
                                constancia_administrativo 
                                where 
                                constancia_administrativo.id_documento=documento.id_documento 
                                and id_trabajo_administrativo=:id_experiencia_tabla;");
                            $pdf->execute(['id_experiencia_tabla'=>$datos['id_experiencia_tabla']]);

                            $id=$pdf->fetch(PDO::FETCH_ASSOC);



                            $ruta='src/documentos/experiencia_laboral/'.$id['descripcion'];
                            if(unlink($ruta)){
                                $publica=$pdo->prepare("DELETE from constancia_administrativo where id_trabajo_administrativo=:id_experiencia_tabla;");
                                $publica->execute(['id_experiencia_tabla'=>$datos['id_experiencia_tabla']]);

                                $doc=$pdo->prepare("DELETE from documento where id_documento=:id_documento;");
                                $doc->execute(['id_documento'=>$id['id_documento']]);
                            }
                        }

                        $admin=$pdo->prepare("DELETE from trabajo_administrativo where id_trabajo_administrativo=:id_experiencia_tabla;");
                        $admin->execute(['id_experiencia_tabla'=>$datos['id_experiencia_tabla']]);
                        break;

                    case 2:
                        $docencia=$pdo->prepare("DELETE from docencia_previa where id_docencia_previa=:id_experiencia_tabla;");
                        $docencia->execute(['id_experiencia_tabla'=>$datos['id_experiencia_tabla']]);
                        break;

                    case 3:
                        $comision=$pdo->prepare("DELETE from comision_gubernamental where id_comision_gubernamental=:id_experiencia_tabla;");
                        $comision->execute(['id_experiencia_tabla'=>$datos['id_experiencia_tabla']]);
                        break;

                }
                
                $experiencia=$pdo->prepare("DELETE from experiencia_docente where id_experiencia_docente=:id_experiencia_docente;");
                $experiencia->execute(['id_experiencia_docente'=>$datos['id_experiencia_docente']]);




                $pdo->commit();
                return true;

            } catch(PDOException $e){
                //5. regresas a un estado anterior en caso de error
                $pdo->rollBack();
                //echo "Fallo: " . $e->getMessage();
                return false;
            }
        }


    }
        
    

?>