<?php 
    //funcion para obtener los datos de las imagenes
    class Documento{

        public function getData($archivo){
            //var_dump($archivo);
            // Obtener información del archivo subido
            $ruta_tmp = $archivo['tmp_name'];
            $nomb_archivo = $archivo['name'];
            $tam_bytes = $archivo['size'];
            $tipo_archivo = $archivo['type'];
            //separa por el delimitador "."
            $fileNameCmps1 = explode(".", $nomb_archivo);
            /*
                la funcion end apunta al ultimo elemento de un arreglo
                la funcion strtolower convierte  mayusculas a minusculas
            */
            $extension_archivo = strtolower(end($fileNameCmps1));
            $nuevo_nomb = md5(time() . $nomb_archivo) . '.' . $extension_archivo;
            //var_dump($extension_archivo);
            $var=array(
                            'ruta_tmp'=>$archivo['tmp_name'],
                            'nomb_archivo'=>$nuevo_nomb,
                            'original_name'=>$archivo['name'],
                            'tam_bytes'=>$archivo['size'],
                            'tipo_archivo'=>$archivo['type'],
                            'extension'=>$extension_archivo
                        );
    
            return $var;
        }
    
        //funcion para mover los archivos
    function mover($extension,$ruta_destino,$ruta_temporal,$name_archivo, $extensiones_permitidas){
        //var_dump($extension,$ruta_destino,$ruta_temporal,$name_archivo);
        //var_dump('mover',$extensiones_permitidas);
        //$extensiones_permitidas = array('csv', 'gif', 'png', 'jpeg', 'xlsx');
        if (in_array($extension, $extensiones_permitidas)) {
            $ruta_carga = $ruta_destino;
            $destino = $ruta_carga .date('Y-m-d H:i:s'). $name_archivo;
            //var_dump($destino);

            /* 
                la funcion move_uploaded_file mueve el archivo cargado a una nueva ubicacion
            */
            
                if(move_uploaded_file($ruta_temporal, $destino))
                {
                    return true;
                } else{
                    //echo "primer if";

                    return false;
                }
            
            }else{
                //echo "segundo if";
                return false;
            }

    }
    
    }
?>