<?php
include_once 'datosacademicos/programa_tipo.php';
include_once 'datosacademicos/programanivel.php';

class ProgramatipoModel extends Model{
    public function __construct(){
        parent::__construct();
    }

    public function existe($programa_tipo){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM programa_tipo WHERE  descripcion=:descripcion");
            $sql->execute(['descripcion' =>$programa_tipo]);
            $nombre=$sql->fetch();
            
            if($programa_tipo==$nombre['descripcion']){
                return $nombre['descripcion'];
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
        //echo "<br>insertar datos";
        try{
            
            $query=$this->db->connect()->prepare('INSERT INTO programa_tipo(
                descripcion,codigo,
                id_programa_nivel) VALUES(:descripcion, :codigo, :id_programa_nivel)');
           
            $query->execute([
                'descripcion'=>$datos['descripcion'],
                'codigo'=>$datos['codigo'],
                'id_programa_nivel'=>$datos['id_programa_nivel']
                ]);
            
            return true;
       
        }
        catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                
        }
  
    }

    public function getNivel(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT id_programa_nivel, descripcion, estatus,codigo  FROM programa_nivel");
            
            while($row=$query->fetch()){
                
                $item=new ProgramaNivel();
                $item->id_programa_nivel=$row['id_programa_nivel'];
                $item->descripcion=$row['descripcion'];
                $item->estatus=$row['estatus'];
                $item->codigo=$row['codigo'];


                array_push($items,$item);
            
            
            }
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }

    public function getTipo(){
            $items=[];
            try{
                $query=$this->db->connect()->query("SELECT 
                id_programa_tipo as id_tipo, 
                programa_tipo.descripcion AS tipo,
                programa_tipo.codigo as codigo,  
                programa_tipo.id_programa_nivel as id_nivel, 
                programa_nivel.descripcion AS nivel 
                FROM programa_tipo, programa_nivel WHERE  programa_tipo.id_programa_nivel=programa_nivel.id_programa_nivel");
                
                while($row=$query->fetch()){
                    
                    $item=new Tipo();
                    $item->id_programa_tipo=$row['id_tipo'];
                    $item->descripcion=$row['tipo'];
                    $item->codigo=$row['codigo'];
                    $item->id_programa_nivel=$row['id_nivel'];
                    $item->descripcion_nivel=$row['nivel'];
                
                    array_push($items,$item);
                
                
                }
                return $items;
          
            }catch(PDOException $e){
            return[];
            }
          
    }

    public function update($datos){
            //var_dump($datos);
                  //var_dump($item['id_pais'],$item['estado'],$item['codigoine'],$item['id_estado']);
                  $query=$this->db->connect()->prepare("UPDATE programa_tipo 
                  SET descripcion=:descripcion, 
                  codigo=:codigo, 
                  id_programa_nivel=:id_programa_nivel 
                  WHERE id_programa_tipo=:id_programa_tipo");
                  
                  try{
                      $query->execute([
                          'id_programa_tipo'=>$datos['id_programa_tipo'],
                          'descripcion'=>$datos['descripcion'],
                          'codigo'=>$datos['codigo'],
                          'id_programa_nivel'=>$datos['id_programa_nivel'],
                                   
                               ]);
                   return true;
          
                  }catch(PDOException $e){
                       return false;
                  }
                   
    }

    public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM programa_tipo WHERE id_programa_tipo = :id_programa_tipo");
        
                try{
                $query->execute([
                    'id_programa_tipo'=>$id,
                    ]);
                        return true;
        
                }catch(PDOException $e){
        return false;
    }
           
  }

}

?>