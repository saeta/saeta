<?php
include_once 'datosacademicos/aldea.php';

include_once 'datosacademicos/aldeaprograma.php';
include_once 'datosacademicos/programa_formacion.php';

class Aldea_programaModel extends Model{
    public function __construct(){
        parent::__construct();
    }

    public function existe($codigo_opsu){
        try{
    
            //var_dump($codigo_opsu);
            $sql = $this->db->connect()->prepare("SELECT codigo_opsu FROM aldea_programa WHERE codigo_opsu=:codigo_opsu");
            $sql->execute(['codigo_opsu'=>$codigo_opsu]);
            $nombre=$sql->fetch();
            
            if($codigo_opsu==$nombre['codigo_opsu']){
                
                return $nombre['codigo_opsu'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }


    public function insert($datos){
        //echo "<br>insertar datos";
        try{
            //var_dump($datos);
            $query=$this->db->connect()->prepare('INSERT INTO aldea_programa(
                codigo_opsu,
                id_aldea,
                id_programa,
                estatus) VALUES(:codigo_opsu, 
                                :id_aldea, 
                                :id_programa,
                                :estatus)');
           
            $query->execute([
                'codigo_opsu'=>$datos['codigo_opsu'],
                'id_aldea'=>$datos['id_aldea'],
                'id_programa'=>$datos['id_programa'],
                'estatus'=>$datos['estatus'],
                ]);
            
            return true;
       
        }
        catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                
        }
  
    }

    public function getAldea(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT * FROM aldea");
            
            while($row=$query->fetch()){
                
                $item=new Aldea();
                $item->id_aldea=$row['id_aldea'];
                $item->descripcion=$row['descripcion'];
                $item->codiga_sucre=$row['codigo_sucre'];
                $item->codigo_aldea=$row['codigo_aldea'];
                $item->estatus=$row['estatus'];
                $item->id_aldea_tipo=$row['id_aldea_tipo'];

                array_push($items,$item);
            
            
            }
            //var_dump($items);
            return $items;
      
        }catch(PDOException $e){
            return[];
        }
      
}
public function getProgramaF(){
    $items=[];
    try{
        $query=$this->db->connect()->query("SELECT 
        id_programa,descripcion,codigo,id_centro_estudio,id_programa_tipo,id_nivel_academico,id_linea_investigacion,estatus
        FROM programa");
    
        while($row=$query->fetch()){
        
            $item=new Programa();
            $item->id_programa=$row['id_programa'];
            $item->descripcion=$row['descripcion'];
            $item->codigo=$row['codigo'];
            $item->id_centro_estudio=$row['id_centro_estudio'];
            $item->id_programa_tipo=$row['id_programa_tipo'];
            $item->id_nivel_academico=$row['id_nivel_academico'];
            $item->id_linea_investigacion=$row['id_linea_investigacion'];
            $item->estatus=$row['estatus'];
    
            array_push($items,$item);
    
    
        }
        return $items;

    }catch(PDOException $e){
    return[];
}

}

    public function getAldeaP(){
            $items=[];
            try{
                $query=$this->db->connect()->query("SELECT 
                id_aldea_programa as id_aldea_programa,
                aldea_programa.id_aldea as id_aldea,
                aldea.descripcion AS aldea,
                aldea_programa.id_programa as id_programa,
                programa.descripcion AS programa,
                aldea_programa.codigo_opsu as codigo_opsu,
                aldea_programa.estatus as estatus
                FROM aldea_programa, aldea, programa 
                WHERE aldea_programa.id_aldea=aldea.id_aldea AND aldea_programa.id_programa=programa.id_programa");
                
                while($row=$query->fetch()){
                    
                    $item=new AldeaPrograma();
                    $item->id_aldea_programa=$row['id_aldea_programa'];
                    $item->id_aldea=$row['id_aldea'];
                    $item->descripcion_aldea=$row['aldea'];
                    $item->id_programa=$row['id_programa'];
                    $item->descripcion_programa=$row['programa'];
                    $item->codigo_opsu=$row['codigo_opsu'];
                    //$item->estatus=$row['estatus'];
                    if($row['estatus'] == 1){
                        $item->estatus="Activo";
                    }elseif($row['estatus'] == 0){
                        $item->estatus="Inactivo";
                    }
                    //var_dump($item->estatus);
                
                    array_push($items,$item);
                
                
                }
                return $items;
          
            }catch(PDOException $e){
            return[];
            }
          
    }

    public function update($datos){
            //  var_dump($datos['id_aldea_programa']);
                  //var_dump($item['id_pais'],$item['estado'],$item['codigo_opsuine'],$item['id_estado']);
                  $query=$this->db->connect()->prepare("UPDATE aldea_programa 
                   SET id_aldea=:id_aldea,
                   id_programa=:id_programa,
                   codigo_opsu=:codigo_opsu,
                   estatus=:estatus
                   WHERE id_aldea_programa=:id_aldea_programa");
                  
                  try{
                      $query->execute([
                          'id_aldea_programa'=>$datos['id_aldea_programa'],
                          'codigo_opsu'=>$datos['codigo_opsu'],
                          'id_aldea'=>$datos['id_aldea'],
                          'id_programa'=>$datos['id_programa'],
                          'estatus'=>$datos['estatus'],
                            
                               ]);
                   return true;
          
                  }catch(PDOException $e){
                       return false;
                  }
                   
    }

    public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM aldea_programa WHERE id_aldea_programa = :id_aldea_programa");
        
                try{
                $query->execute([
                    'id_aldea_programa'=>$id,
                    ]);
                        return true;
        
                }catch(PDOException $e){
        return false;
    }
           
  }

}

?>