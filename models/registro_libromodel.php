<?php
include_once 'datosacademicos/docente.php';
include_once 'datosacademicos/libro.php';
class Registro_libroModel extends Model{
    public function __construct(){
        parent::__construct();
    }


    public function existe($registrolibro){
        try{
    
            //var_dump($area_conocimiento_opsu);
            $sql = $this->db->connect()->prepare("SELECT nombre_libro FROM libro WHERE nombre_libro=:nombre_libro");
            $sql->execute(['nombre_libro'=>$registrolibro]);
            $nombre=$sql->fetch();
            
            if($registrolibro==$nombre['nombre_libro']){
                
                return $nombre['nombre_libro'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }

    public function insert($datos){
        //echo "<br>insertar datos";
    

        try{
        // var_dump($datos['nombre_revista']);
            $query=$this->db->connect()->prepare('INSERT INTO libro(
                nombre_libro,
                editorial,
                isbn_libro,
                ano_publicacion,
                ciudad_publicacion,
                volumenes,
                nro_paginas,
                url_libro,
                id_docente,
                estatus_verificacion

                ) VALUES(
                    :nombre_libro,
                    :editorial,
                    :isbn_libro,
                    :ano_publicacion,
                    :ciudad_publicacion,
                    :volumenes,
                    :nro_paginas,
                    :url_libro,
                    :id_docente,
                    :estatus_verificacion
                    )');
    
            $query->execute([
                    'nombre_libro'=>$datos['nombre_libro'],
                    'editorial'=>$datos['editorial'],
                    'isbn_libro'=>$datos['isbn_libro'],
                    'ano_publicacion'=>$datos['ano_publicacion'],
                    'ciudad_publicacion'=>$datos['ciudad_publicacion'],
                    'volumenes'=>$datos['volumenes'],
                    'nro_paginas'=>$datos['nro_paginas'],
                    'url_libro'=>$datos['url_libro'],
                    'id_docente'=>$datos['id_docente'],
                    'estatus_verificacion'=>'Sin Verificar'
                ]);
    
                return true;
          
        }catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                 }
        
        
    }

    
    public function getDocente(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT 
            docente.id_persona, 
            id_docente,
            identificacion, 
            primer_nombre, 
            primer_apellido
            FROM 
            persona, 
            persona_nacionalidad,
            docente
            WHERE docente.id_persona=persona.id_persona 
            AND persona_nacionalidad.id_persona=persona.id_persona;");
            
            while($row=$query->fetch()){
                
                $item=new Docente();
                $item->id_docente=$row['id_docente'];
                $item->id_persona=$row['id_persona'];
                $item->identificacion=$row['identificacion'];
                $item->primer_nombre=$row['primer_nombre'];
                $item->primer_apellido=$row['primer_apellido'];

                array_push($items,$item);
            
            
            }
            //var_dump($items);
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }
      
    public function getLibro(){
        $items=[];
        try{
            $query=$this->db->connect()->prepare("SELECT id_libro,
             libro.nombre_libro,
             libro.editorial,
             libro.isbn_libro,
             libro.ano_publicacion,
             libro.ciudad_publicacion,
             libro.volumenes,
             libro.nro_paginas,
             libro.url_libro,
             persona_nacionalidad.id_persona_nacionalidad,
                docente.id_docente,
                identificacion,
                estatus_verificacion
        
            FROM persona, persona_nacionalidad, libro, docente WHERE persona_nacionalidad.id_persona=persona.id_persona and libro.id_docente=docente.id_docente 
            and docente.id_persona=persona.id_persona and identificacion=:identificacion;");
            $query->execute(['identificacion'=>$_SESSION['identificacion']]);
            
            while($row=$query->fetch()){
                
                $item=new Registro_libro();
                $item->id_libro=$row['id_libro'];
                $item->nombre_libro=$row['nombre_libro'];
                $item->editorial=$row['editorial'];
                $item->isbn_libro=$row['isbn_libro'];
                $item->ano_publicacion=$row['ano_publicacion'];
                $item->ciudad_publicacion=$row['ciudad_publicacion'];
                $item->volumenes=$row['volumenes'];
                $item->nro_paginas=$row['nro_paginas'];
                $item->url_libro=$row['url_libro'];
                $item->id_docente=$row['id_docente'];
                $item->estatus_verificacion=$row['estatus_verificacion'];
                //$item->estatus=$row['estatus'];
                //$item->descripcion_opsu=$row['descripcion_opsu'];
                
                
                //$item->descripcion_opsu=$row['areaopsu'];
               
            
                array_push($items,$item);
            
            
            }
            //var_dump($items);
            return $items;
            
        }catch(PDOException $e){
        return[];
        }
      
    }

    public function getbyId($id){

            //var_dump($id);

        $items=[];
        
        try{
            $query=$this->db->connect()->prepare("SELECT id_libro,
             libro.nombre_libro,
             libro.editorial,
             libro.isbn_libro,
             libro.ano_publicacion,
             libro.ciudad_publicacion,
             libro.volumenes,
             libro.nro_paginas,
             libro.url_libro,
                docente.id_docente,
                identificacion,
                primer_nombre,
                primer_apellido,
                estatus_verificacion
               
            FROM persona, persona_nacionalidad, libro, docente WHERE libro.id_docente=docente.id_docente 
            and docente.id_persona=persona.id_persona 
            and persona_nacionalidad.id_persona=persona.id_persona and identificacion=:identificacion and id_libro=:id_libro;");
            $query->execute([
                'identificacion'=>$_SESSION['identificacion'],
                'id_libro'=>$id
                ]);
            

            $item=new Registro_libro();
            while($row=$query->fetch()){
                
                
                
                $item->id_libro=$row['id_libro'];
                $item->nombre_libro=$row['nombre_libro'];
                $item->editorial=$row['editorial'];
                $item->isbn_libro=$row['isbn_libro'];
                $item->ano_publicacion=$row['ano_publicacion'];
                $item->ciudad_publicacion=$row['ciudad_publicacion'];
                $item->volumenes=$row['volumenes'];
                $item->nro_paginas=$row['nro_paginas'];
                $item->url_libro=$row['url_libro'];
                $item->id_docente=$row['id_docente'];
                $item->estatus_verificacion=$row['estatus_verificacion'];
                //$item->estatus=$row['estatus'];
                //$item->descripcion_opsu=$row['descripcion_opsu'];
                
                
                //$item->descripcion_opsu=$row['areaopsu'];
               
            
                array_push($items,$item);
            
            
            }


            //var_dump($item);
            return $item;
            
        }catch(PDOException $e){
        return[];
        }
      
    }


    

    public function update($datos){
       //var_dump($datos['nombre_arte2']);
       //var_dump($datos['ano_arte2']);
       //var_dump($datos['entidad_promotora2']);
       //var_dump($datos['url_otro2']);
       //var_dump($datos['id_arte_software']);
        //var_dump($datos);
        $query=$this->db->connect()->prepare("UPDATE libro SET 
        nombre_libro=:nombre_libro,
        editorial=:editorial,
        isbn_libro=:isbn_libro,
        ano_publicacion=:ano_publicacion,
        ciudad_publicacion=:ciudad_publicacion,
        volumenes=:volumenes,
        nro_paginas=:nro_paginas,
        url_libro=:url_libro,
        estatus_verificacion=:estatus_verificacion
       
        WHERE id_libro=:id_libro");
        
        try{
            $query->execute([
                'id_libro'=>$datos['id_libro'],

                'nombre_libro'=>$datos['nombre_libro2'],
                'editorial'=>$datos['editorial2'],
                'isbn_libro'=>$datos['isbn_libro2'],
                'ano_publicacion'=>$datos['ano_publicacion2'],
                'ciudad_publicacion'=>$datos['ciudad_publicacion2'],
                'volumenes'=>$datos['volumenes2'],
                'nro_paginas'=>$datos['nro_paginas2'],
                'url_libro'=>$datos['url_libro2'],
                'estatus_verificacion'=>'Sin Verificar'
               
                
              ]);
              //creo q falta descripcion del area de conocimientoooooooooooooooooooooooooo   
         
         
              return true;

        }catch(PDOException $e){
             return false;
        }
         
    }

        public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM libro WHERE id_libro = :id_libro");
    
            try{
                $query->execute([
                'id_libro'=>$id
                ]);
                return true;
            }catch(PDOException $e){
                return false;
            }
        }
    }


    ?>