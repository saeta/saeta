<?php
include_once 'datosacademicos/docente.php';
//include_once 'datosacademicos/documento.php';
include_once 'datosacademicos/reconocimiento.php';
include_once 'models/estructura.php';
include_once 'scripts/files.php';
class Registro_reconocimientoModel extends Model{
    public function __construct(){
        parent::__construct();
    }


    public function existe($reconocimiento){
        try{
    
            //var_dump($area_conocimiento_opsu);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM reconocimiento WHERE descripcion=:descripcion");
            $sql->execute(['descripcion'=>$reconocimiento]);
            $descripcion=$sql->fetch();
            
            if($reconocimiento==$descripcion['descripcion']){
                
                return $descripcion['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }



        //////////////    funcion que llama datos de tablas catalogos   ///////////////
        public function getCatalogo($valor){
            $items=[];
            try{

                $query=$this->db->connect()->query("SELECT * FROM ".$valor."");
          
                while($row=$query->fetch()){
                    $item=new Estructura();
                   $item->id=$row['id_'.$valor.''];
                    $item->descripcion=$row['descripcion'];
                            
                    array_push($items,$item);
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
          
        }


    public function insert($datos){
                // var_dump($datos);

        //echo "<br>insertar datos";
    

        try{

                //1. guardas el objeto pdo en una variable
                $pdo=$this->db->connect();

                //2. comienzas transaccion
                $pdo->beginTransaction();


            $doc=new Documento();
            $pdf_trabajo=$doc->getData($datos['pdf_reconocimiento']);
            //var_dump($pdf_trabajo);
            $band=false;
            //el mover no se esta ejecutando: revisar extensiones y permisos
            if($doc->mover($pdf_trabajo['extension'], 
                'src/documentos/reconocimientos/',
                $pdf_trabajo['ruta_tmp'],
                $pdf_trabajo['original_name'],
                array('pdf'))
            )

            {
                $band=true;
                $doc_trabajo=$pdo->prepare("INSERT INTO documento(
                    descripcion,
                    estatus,
                    id_persona,
                    id_tipo_documento) 
                    VALUES(
                        :descripcion,
                        :estatus, 
                        :id_persona,
                        (SELECT id_tipo_documento FROM tipo_documento WHERE descripcion='Acta Reconocimiento') 
                        );");

                $doc_trabajo->execute([
                    'descripcion'=>date('Y-m-d H:i:s').$pdf_trabajo['original_name'],
                    'estatus'=>'Sin Verificar',
                    'id_persona'=>$datos['id_persona']
                    ]);

                 //var_dump($datos);
            $query=$pdo->prepare('INSERT INTO reconocimiento(
               id_tipo_reconocimiento,
               descripcion,
               entidad_promotora,
               ano_reconocimiento,
               caracter,
               id_documento,
                id_docente,
                estatus_verificacion
               
                ) VALUES(
                    :id_tipo_reconocimiento,
                    :descripcion,
                    :entidad_promotora,
                    :ano_reconocimiento,
                    :caracter,
                    (SELECT id_documento from documento ORDER BY id_documento DESC LIMIT 1),
                    :id_docente,
                    :estatus_verificacion
                    )');
    
            $query->execute([
                'id_tipo_reconocimiento'=>$datos['id_tipo_reconocimiento'],
                    'descripcion'=>$datos['descripcion'],
                    'entidad_promotora'=>$datos['entidad_promotora'],
                    'ano_reconocimiento'=>$datos['ano_reconocimiento'],
                    'caracter'=>$datos['caracter'],
                  //  'id_documento'=>$datos['id_documento'],
                    'id_docente'=>$datos['id_docente'],
                   // 'estatus_verificacion'=>$datos['estatus_verificacion']
                    'estatus_verificacion'=>'Sin Verificar'

                 


                ]);
            }
            //var_dump('bandera >>>>',$band);

         //4. consignas la transaccion (en caso de que no suceda ningun fallo)
         $pdo->commit();
         // var_dump($pdo->inTransaction());
                return true;
          
        }catch(PDOException $e){
            //echo "Matricula duplicada";
             //5. regresas a un estado anterior en caso de error
            $pdo->rollBack();
            return false;
                 }
        
    }

    
    public function getDocente(){
        $items=[];
        try{

                 //1. guardas el objeto pdo en una variable
                 $pdo=$this->db->connect();

                 //2. comienzas transaccion
                 $pdo->beginTransaction();
            $query=$pdo->query("SELECT 
            docente.id_persona, 
            id_docente,
            identificacion, 
            primer_nombre, 
            primer_apellido
            FROM 
            persona, 
            persona_nacionalidad,
            docente
            WHERE docente.id_persona=persona.id_persona 
            AND persona_nacionalidad.id_persona=persona.id_persona");
            
            while($row=$query->fetch()){
                
                $item=new Docente();
                $item->id_docente=$row['id_docente'];
                $item->id_persona=$row['id_persona'];
                $item->identificacion=$row['identificacion'];
                $item->primer_nombre=$row['primer_nombre'];
                $item->primer_apellido=$row['primer_apellido'];

                array_push($items,$item);
            
            
            }
            //var_dump($items);

             //4. consignas la transaccion (en caso de que no suceda ningun fallo)
         $pdo->commit();
            return $items;
      
        }catch(PDOException $e){

             //5. regresas a un estado anterior en caso de error
             $pdo->rollBack();
        return[];
        }
      
    }

    public function getTipoReco(){
        $items=[];
       try{
      $query=$this->db->connect()->query("SELECT id_tipo_reconocimiento, descripcion FROM tipo_reconocimiento");
      
      while($row=$query->fetch()){
      $item=new Tiporeconocimiento();
      $item->id_tipo_reconocimiento=$row['id_tipo_reconocimiento'];
      $item->descripcion=$row['descripcion'];
     
      array_push($items,$item);
      
      
      }
      return $items;
      
      }catch(PDOException $e){
      return[];
      }
      
    }
      
    public function getReconocimiento(){
        $items=[];
        try{


                //1. guardas el objeto pdo en una variable
                $pdo=$this->db->connect();

                //2. comienzas transaccion
                $pdo->beginTransaction();


            $query=$pdo->prepare("SELECT id_reconocimiento,
            reconocimiento.descripcion,
             reconocimiento.id_tipo_reconocimiento,
             tipo_reconocimiento.descripcion as tipo_reconocimiento,
             reconocimiento.entidad_promotora,
             reconocimiento.ano_reconocimiento,
             reconocimiento.caracter,
             documento.id_documento,
             reconocimiento.estatus_verificacion,
                reconocimiento.id_docente
               
                
            FROM 
           
            documento,
            tipo_reconocimiento, 
            reconocimiento, 
            docente 
            WHERE reconocimiento.id_docente=docente.id_docente 
            and reconocimiento.id_tipo_reconocimiento=tipo_reconocimiento.id_tipo_reconocimiento
            and reconocimiento.id_documento=documento.id_documento
             and reconocimiento.id_docente=:id_docente;");


            $query->execute(['id_docente'=>$_SESSION['id_docente']]);
            
            while($row=$query->fetch()){
                
                $item=new Registro_reconocimiento(); //colocar estructura
                $item->id_reconocimiento=$row['id_reconocimiento'];
                $item->descripcion=$row['descripcion'];
                $item->id_tipo_reconocimiento=$row['id_tipo_reconocimiento'];
                $item->descripcion_tipo_reconocimiento=$row['tipo_reconocimiento'];
                $item->entidad_promotora=$row['entidad_promotora'];
                $item->ano_reconocimiento=$row['ano_reconocimiento'];
                $item->caracter=$row['caracter'];
                $item->id_documento=$row['id_documento'];
                $item->id_docente=$row['id_docente'];
                $item->estatus_verificacion=$row['estatus_verificacion'];
               
                //$item->estatus=$row['estatus'];
                //$item->descripcion_opsu=$row['descripcion_opsu'];
                
                
                //$item->descripcion_opsu=$row['areaopsu'];
               
            
                array_push($items,$item);
            
            
            }
            //var_dump($items);

              //4. consignas la transaccion (en caso de que no suceda ningun fallo)
         $pdo->commit();
            return $items;
            
        }catch(PDOException $e){
             //5. regresas a un estado anterior en caso de error
             $pdo->rollBack();
        return[];
        }
      
    }

    public function getbyId($id){

         
        $items=[];
        $item=new Registro_reconocimiento();
        try{

              
            $query=$this->db->connect()->prepare("SELECT id_reconocimiento,
            reconocimiento.descripcion,

             reconocimiento.id_tipo_reconocimiento,
             
             tipo_reconocimiento.descripcion as descripcion_tipo_reconocimiento,
             reconocimiento.entidad_promotora,
             reconocimiento.ano_reconocimiento,
             reconocimiento.caracter,
             reconocimiento.id_documento,
             documento.descripcion as descripcion_documento,
             reconocimiento.estatus_verificacion,
            reconocimiento.id_docente
               
                
            FROM 
           
            documento,
            tipo_reconocimiento, 
            reconocimiento, 
            docente 
            WHERE reconocimiento.id_docente=docente.id_docente 
            and reconocimiento.id_tipo_reconocimiento=tipo_reconocimiento.id_tipo_reconocimiento
            and reconocimiento.id_documento=documento.id_documento
             and id_reconocimiento=:id_reconocimiento;");
            
            
            $query->execute([
                'id_reconocimiento'=>$id
                

                
                ]);
            

           
            while($row=$query->fetch()){
                
                
                $item->id_reconocimiento=$row['id_reconocimiento'];
                $item->descripcion=$row['descripcion'];
                $item->descripcion_documento=$row['descripcion_documento'];

                $item->id_tipo_reconocimiento=$row['id_tipo_reconocimiento'];
                $item->descripcion_tipo=$row['descripcion_tipo_reconocimiento'];
               
                $item->entidad_promotora=$row['entidad_promotora'];
                $item->ano_reconocimiento=$row['ano_reconocimiento'];
                $item->caracter=$row['caracter'];
                $item->id_documento=$row['id_documento'];
                $item->id_docente=$row['id_docente'];
                $item->estatus_verificacion=$row['estatus_verificacion'];
               
                //$item->estatus=$row['estatus'];
                //$item->descripcion_opsu=$row['descripcion_opsu'];
                
                
                //$item->descripcion_opsu=$row['areaopsu'];
               
            
                array_push($items,$item);
            
            
            }


            //var_dump($item);

            return $item;
            
        }catch(PDOException $e){
             //5. regresas a un estado anterior en caso de error
             //$pdo->rollBack();
        return[];
        }
      
    }


    

    public function update($datos){

            //1. guardas el objeto pdo en una variable
            $pdo=$this->db->connect();

            //2. comienzas transaccion
            $pdo->beginTransaction();


      
        $query=$pdo->prepare("UPDATE reconocimiento SET 
        id_tipo_reconocimiento=:id_tipo_reconocimiento,
        descripcion=:descripcion,
        entidad_promotora=:entidad_promotora,
        ano_reconocimiento=:ano_reconocimiento,
        caracter=:caracter,
        estatus_verificacion=:estatus_verificacion
   
         WHERE id_reconocimiento=:id_reconocimiento");
        
        try{
            $query->execute([
                'id_reconocimiento'=>$datos['id_reconocimiento'],
                'id_tipo_reconocimiento'=>$datos['id_tipo_reconocimiento'],
                'descripcion'=>$datos['descripcion'],
                'entidad_promotora'=>$datos['entidad_promotora'],
                'ano_reconocimiento'=>$datos['ano_reconocimiento'],
                //'id_documento'=>$datos['id_documento'],
                'caracter'=>$datos['caracter'],
                'estatus_verificacion'=>'Sin Verificar'
               // 'estatus_verificacion'=>$datos['estatus_verificacion']
               
                
              ]);
              //creo q falta descripcion del area de conocimientoooooooooooooooooooooooooo   
         
         //4. consignas la transaccion (en caso de que no suceda ningun fallo)
         $pdo->commit();
              return true;

        }catch(PDOException $e){

             //5. regresas a un estado anterior en caso de error
             $pdo->rollBack();
             return false;
        }
         
    }


    public function getDocumentbyID($id){

        try{
            $item = new Estructura();
            $sql = $this->db->connect()->prepare("SELECT 
            id_reconocimiento,
            reconocimiento.id_documento,
            documento.descripcion
            from 
            reconocimiento,
            documento
            where 
            reconocimiento.id_documento=documento.id_documento
            and reconocimiento.id_reconocimiento=:id_reconocimiento          
            ");

            
                $sql->execute(['id_reconocimiento'=>$id]);
                

                while($row=$sql->fetch()){

                    $item->id_documento=$row['id_documento'];
                    $item->id_id_reconocimiento=$row['id_reconocimiento'];
                    $item->descripcion=$row['descripcion'];
                }
                return $item;
            }catch(PDOExcepton $e){
                return null;
            }

    }





















       
    }


    ?>