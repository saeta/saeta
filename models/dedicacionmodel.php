<?php
include_once 'datosacademicos/dedicacion.php';

class DedicacionModel extends Model{
    public function __construct(){
        parent::__construct();
    }


    public function existe($dedicacion){
        try{
    
            //var_dump($dedicacion);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM docente_dedicacion WHERE descripcion=:descripcion");
            $sql->execute(['descripcion'=>$dedicacion]);
            $nombre=$sql->fetch();
            
            if($dedicacion==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }

    public function insert($datos){
        //echo "<br>insertar datos";
    
        try{
         
            $query=$this->db->connect()->prepare('INSERT INTO docente_dedicacion(
                descripcion,
                total_horas,
                hora_docencia_minima,
                hora_docencia_maxima) VALUES(
                    :descripcion,
                    :total_horas,
                    :hora_docencia_minima,
                    :hora_docencia_maxima
                    )');
    
                $query->execute([
                    'descripcion'=>$datos['descripcion'],
                    'total_horas'=>$datos['horas_totales'],
                    'hora_docencia_minima'=>$datos['horas_minimas'],
                    'hora_docencia_maxima'=>$datos['horas_maximas']
                ]);
    
                return true;
          
        }catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                 }
        
        
    }

    

    public function get(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT * FROM docente_dedicacion");
            
            while($row=$query->fetch()){
                
                $item=new DedicacionD();
                $item->id_docente_dedicacion=$row['id_docente_dedicacion'];
                $item->descripcion=$row['descripcion'];
                $item->total_horas=$row['total_horas'];
                $item->hora_docencia_minima=$row['hora_docencia_minima'];
                $item->hora_docencia_maxima=$row['hora_docencia_maxima'];

                array_push($items,$item);
            
            
            }
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }
      
    public function update($datos){
        //var_dump($datos);
        //var_dump($item['id_pais'],$item['estado'],$item['codigoine'],$item['id_estado']);
        $query=$this->db->connect()->prepare("UPDATE docente_dedicacion SET 
        descripcion=:descripcion,
        total_horas=:total_horas,
        hora_docencia_minima=:hora_docencia_minima,
        hora_docencia_maxima=:hora_docencia_maxima
        WHERE id_docente_dedicacion=:id_docente_dedicacion");
        
        try{
            $query->execute([
                'id_docente_dedicacion'=>$datos['id_docente_dedicacion'],
                'descripcion'=>$datos['descripcion'],
                'total_horas'=>$datos['horas_totales'],
                'hora_docencia_minima'=>$datos['horas_minimas'],
                'hora_docencia_maxima'=>$datos['horas_maximas']
                 ]);
         return true;

        }catch(PDOException $e){
             return false;
        }
         
    }

        public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM docente_dedicacion WHERE id_docente_dedicacion = :id_docente_dedicacion");
    
            try{
                $query->execute([
                'id_docente_dedicacion'=>$id
                ]);
                return true;
            }catch(PDOException $e){
                return false;
            }
        }
    }


    ?>