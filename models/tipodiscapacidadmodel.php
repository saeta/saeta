<?php
include_once 'models/persona.php';
class TipoDiscapacidadModel extends Model{
    public function __construct(){
    parent::__construct();
    }
    public function existe($discapacidad){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM tipo_discapacidad WHERE descripcion=:descripcion");
            $sql->execute(['descripcion' =>$discapacidad]);
            $nombre=$sql->fetch();
            
            if($discapacidad==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
    //echo "<br>insertar datos";

    try{
     
             $query=$this->db->connect()->prepare('INSERT INTO tipo_discapacidad (descripcion) VALUES
             (:descripcion)');

            $query->execute(['descripcion'=>$datos['discapacidad']]);

            return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }
    
    
            }

            public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT * FROM tipo_discapacidad");
              
              while($row=$query->fetch()){
              $item=new Persona();
              $item->id_tipo_discapacidad=$row['id_tipo_discapacidad'];
              $item->descripcion=$row['descripcion'];
              
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }


              public function update($item){
   
       // var_dump($item['id_tipo_discapacidad2']);
       // var_dump($item['discapacidad2']);

                $query=$this->db->connect()->prepare("UPDATE tipo_discapacidad SET descripcion= :descripcion WHERE id_tipo_discapacidad= :id_tipo_discapacidad");
            try{
                        $query->execute([
                        'descripcion'=>$item['discapacidad2'],
                        'id_tipo_discapacidad'=>$item['id_tipo_discapacidad2'],
  
                        
                        ]);
            return true;
                       
                        
            }catch(PDOException $e){
                return false;
                 }
            
                }



                public function delete($id_tipo_discapacidad){
                    $query=$this->db->connect()->prepare("DELETE FROM tipo_discapacidad WHERE id_tipo_discapacidad=:id_tipo_discapacidad");
                
                        try{
                        $query->execute([
                            'id_tipo_discapacidad'=>$id_tipo_discapacidad,
                            ]);
                                return true;
                
                        }catch(PDOException $e){
                return false;
                }
                
                
                    }






    }

    ?>