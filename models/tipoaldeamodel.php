<?php
include_once 'models/estructura.php';
class TipoaldeaModel extends Model{
    public function __construct(){
    parent::__construct();
    }
    public function existe($aldeatipo){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM aldea_tipo WHERE descripcion=:descripcion");
            $sql->execute(['descripcion' =>$aldeatipo]);
            $nombre=$sql->fetch();
            
            if($aldeatipo==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
    //echo "<br>insertar datos";


    try{
     
             $query=$this->db->connect()->prepare('INSERT INTO aldea_tipo(descripcion) VALUES
             (:descripcion)');

            $query->execute(['descripcion'=>$datos['aldeatipo']]);

            return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }
    
    
            }

            public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT *FROM aldea_tipo");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_aldea_tipo=$row['id_aldea_tipo'];
              $item->descripcion=$row['descripcion'];
   
            
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }


            public function getAmbiente(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT id_ambiente,descripcion FROM ambiente");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_ambiente=$row['id_ambiente'];
              $item->descripcion=$row['descripcion'];
            
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }

 


              public function update($item){
   


         //  var_dump($item['parroquia'],$item['codigoine'],$item['id_municipio'],$item['id_eje_municipal'],$item['id_parroquia']);
              $query=$this->db->connect()->prepare("UPDATE aldea_tipo SET descripcion=:descripcion WHERE id_aldea_tipo=:id_aldea_tipo");
            try{
                        $query->execute([
                        'descripcion'=>$item['aldeatipo'],
                        'id_aldea_tipo'=>$item['id_aldea_tipo'],
  
                        
                        ]);
            return true;
                       
                        
            }catch(PDOException $e){
                return false;
                 }
            
                }
public function delete($id_publicacion){
   

    $query2=$this->db->connect()->prepare("SELECT nomb_archivo FROM publicacion WHERE id_publicacion=:id_publicacion");
    $query2->execute([
        'id_publicacion'=>$id_publicacion,
        ]);
    $nomb_archivo=$query2->fetch();
   
//var_dump($nomb_archivo);

  //  unlink("src/multimedia/".$nomb_archivo);//como diferencia si el msmo nombre es el mismo en todas las fotos

  list($img1,$img2,$img3,$img4) = explode(',', $nomb_archivo);

$ruta1="src/multimedia/".$img1;
var_dump($ruta1);
unlink($ruta1);
$ruta2="src/multimedia/".$img2;
unlink($ruta2);
var_dump($ruta2);
$ruta3="src/multimedia/".$img3;
unlink($ruta3);
var_dump($ruta3);
$ruta4="src/multimedia/".$img4;
unlink($ruta4);
var_dump($ruta4);
$query=$this->db->connect()->prepare("DELETE FROM publicacion WHERE id_publicacion = :id_publicacion");

        try{
        $query->execute([
            'id_publicacion'=>$id_publicacion,
            ]);
                return true;



           }catch(PDOException $e){
return false;
}


    }






    }

    ?>