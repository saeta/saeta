<?php
include_once 'models/estructura.php';
class EstructurafModel extends Model{
    public function __construct(){
    parent::__construct();
    }
    public function existe($estructura){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM ambiente_detalle WHERE descripcion=:descripcion");
            $sql->execute(['descripcion' =>$estructura]);
            $nombre=$sql->fetch();
            
            if($estructura==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
    //echo "<br>insertar datos";


    try{
     
             $query=$this->db->connect()->prepare('INSERT INTO ambiente_detalle(descripcion, estatus, id_ambiente) VALUES
             (:descripcion, :estatus, :id_ambiente)');

            $query->execute(['descripcion'=>$datos['estructura'],'estatus'=>$datos['estatus'],'id_ambiente'=>$datos['id_ambiente']]);

            return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }
    
    
            }

            public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT id_ambiente_detalle,ambiente_detalle.descripcion AS estrutura_fisica,ambiente_detalle.estatus AS estatus, ambiente.id_ambiente,ambiente.descripcion AS ambiente FROM ambiente_detalle,ambiente WHERE ambiente_detalle.id_ambiente=ambiente.id_ambiente");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_ambiente_detalle=$row['id_ambiente_detalle'];
              $item->estrutura_fisica=$row['estrutura_fisica'];
              $item->estatus=$row['estatus'];
              $item->id_ambiente=$row['id_ambiente'];          
              $item->ambiente=$row['ambiente'];
            
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }


            public function getAmbiente(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT id_ambiente,descripcion FROM ambiente");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_ambiente=$row['id_ambiente'];
              $item->descripcion=$row['descripcion'];
            
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }

 


              public function update($item){
   


         //  var_dump($item['parroquia'],$item['codigoine'],$item['id_municipio'],$item['id_eje_municipal'],$item['id_parroquia']);
              $query=$this->db->connect()->prepare("UPDATE ambiente_detalle SET descripcion= :descripcion,estatus= :estatus,id_ambiente=:id_ambiente WHERE id_ambiente_detalle= :id_ambiente_detalle");
            try{
                        $query->execute([
                        'descripcion'=>$item['estructura'],
                        'estatus'=>$item['estatus'],                        
                        'id_ambiente'=>$item['id_ambiente'],
                        'id_ambiente_detalle'=>$item['id_ambiente_detalle'],
  
                        
                        ]);
            return true;
                       
                        
            }catch(PDOException $e){
                return false;
                 }
            
                }
public function delete($id_publicacion){
   

    $query2=$this->db->connect()->prepare("SELECT nomb_archivo FROM publicacion WHERE id_publicacion=:id_publicacion");
    $query2->execute([
        'id_publicacion'=>$id_publicacion,
        ]);
    $nomb_archivo=$query2->fetch();
   
//var_dump($nomb_archivo);

  //  unlink("src/multimedia/".$nomb_archivo);//como diferencia si el msmo nombre es el mismo en todas las fotos

  list($img1,$img2,$img3,$img4) = explode(',', $nomb_archivo);

$ruta1="src/multimedia/".$img1;
var_dump($ruta1);
unlink($ruta1);
$ruta2="src/multimedia/".$img2;
unlink($ruta2);
var_dump($ruta2);
$ruta3="src/multimedia/".$img3;
unlink($ruta3);
var_dump($ruta3);
$ruta4="src/multimedia/".$img4;
unlink($ruta4);
var_dump($ruta4);
$query=$this->db->connect()->prepare("DELETE FROM publicacion WHERE id_publicacion = :id_publicacion");

        try{
        $query->execute([
            'id_publicacion'=>$id_publicacion,
            ]);
                return true;



           }catch(PDOException $e){
return false;
}


    }






    }

    ?>