<?php
//ini_set("display_errors", 1);
include_once 'datosacademicos/docencia_previa_ubv.php';
include_once 'datosacademicos/areaubv.php';
include_once 'datosacademicos/docente.php';
include_once 'datosacademicos/programa_formacion.php';
include_once 'datosacademicos/aldea.php';
include_once 'models/estructura.php';
include_once 'datosacademicos/centro_estudio.php';



class Docencia_previaModel extends Model{
    public function __construct(){
        parent::__construct();
    }


    public function insert($datos){
        //echo "<br>insertar datos";
        //var_dump($datos);
        try{

            $query=$this->db->connect()->prepare('INSERT INTO docencia_previa_ubv(
                id_area_conocimiento_ubv,
                id_programa,
                id_eje_regional,
                id_eje_municipal,
                id_aldea,
                id_centro_estudio,
                fecha_ingreso,
                estatus_verificacion,
                id_docente
                ) VALUES(:id_area_conocimiento_ubv,
                         :id_programa,
                         :id_eje_regional,
                         :id_eje_municipal,
                         :id_aldea,
                         :id_centro_estudio,
                         :fecha_ingreso,
                         :estatus_verificacion,
                         :id_docente)');

            $query->execute([
                'id_area_conocimiento_ubv'=>$datos['id_area_conocimiento_ubv'],
                'id_programa'=>$datos['id_programa'],
                'id_eje_regional'=>$datos['id_eje_regional'],
                'id_eje_municipal'=>$datos['id_eje_municipal'],
                'id_aldea'=>$datos['id_aldea'],
                'id_centro_estudio'=>$datos['id_centro_estudio'],
                'fecha_ingreso'=>$datos['fecha_ingreso'],
                'estatus_verificacion'=>'Sin Verificar',
                'id_docente'=>$datos['id_docente']
                
                ]);

            return true;
           
        }
        catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;

        }

    }

    public function getUbv(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT 
            id_area_conocimiento_ubv,
            descripcion,
            codigo,
            estatus,
            id_area_conocimiento_opsu
            FROM area_conocimiento_ubv");
        
            while($row=$query->fetch()){
            
                $item=new AreaUbv();
                $item->id_area_conocimiento_ubv=$row['id_area_conocimiento_ubv'];
                $item->descripcion=$row['descripcion'];
                $item->codigo=$row['codigo'];
                $item->estatus=$row['estatus'];
                $item->id_area_conocimiento_opsu=$row['id_area_conocimiento_opsu'];
                
                
                
        
                array_push($items,$item);
        
        
            }
            return $items;
    
        }catch(PDOException $e){
        return[];
    }
    
    }
    
    public function getDocente(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT 
            id_docente,
            id_programa,
            id_centro_estudio,
            id_persona,
            id_eje_regional,
            id_docente_dedicacion,
            id_docente_estatus,
            tipo_docente,
            id_clasificacion_docente,
            id_escalafon
            FROM docente");
        
            while($row=$query->fetch()){
            
                $item=new Docente();
                $item->id_docente=$row['id_docente'];
                $item->id_programa=$row['id_programa'];
                $item->id_centro_estudio=$row['id_centro_estudio'];
                $item->id_persona=$row['id_persona'];
                $item->id_eje_regional=$row['id_eje_regional'];
                $item->id_docente_dedicacion=$row['id_docente_dedicacion'];
                $item->id_docente_estatus=$row['id_docente_estatus'];
                $item->tipo_docente=$row['tipo_docente'];
                $item->id_clasificacion_docente=$row['id_clasificacion_docente'];
                $item->id_escalafon=$row['id_escalafon'];
                
    
                array_push($items,$item);
        
        
            }
            return $items;
    
        }catch(PDOException $e){
        return[];
    }
    
}

    public function getProgramaF(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT 
            id_programa,descripcion,codigo,id_centro_estudio,id_programa_tipo,id_nivel_academico,id_linea_investigacion,estatus
            FROM programa");
        
            while($row=$query->fetch()){
            
                $item=new Programa();
                $item->id_programa=$row['id_programa'];
                $item->descripcion=$row['descripcion'];
                $item->codigo=$row['codigo'];
                $item->id_centro_estudio=$row['id_centro_estudio'];
                $item->id_programa_tipo=$row['id_programa_tipo'];
                $item->id_nivel_academico=$row['id_nivel_academico'];
                $item->id_linea_investigacion=$row['id_linea_investigacion'];
                $item->estatus=$row['estatus'];
        
                array_push($items,$item);
        
        
            }
            return $items;
    
        }catch(PDOException $e){
        return[];
    }
    
    }
    
    public function getEjeRegional(){
        $items=[];
       try{
      $query=$this->db->connect()->query("SELECT id_eje_regional, descripcion, codigo, estatus FROM eje_regional");
      
      while($row=$query->fetch()){
      $item=new Estructura();
      $item->id_eje_regional=$row['id_eje_regional'];
      $item->descripcion=$row['descripcion'];
      $item->codigo=$row['codigo'];
      $item->estatus=$row['estatus'];
     
    
      
      array_push($items,$item);
      
      
      }
      return $items;
      
      }catch(PDOException $e){
      return[];
      }
      
    }

      public function getEjeMunicipal(){
        $items=[];
       try{
      $query=$this->db->connect()->query("SELECT id_eje_municipal, descripcion, id_eje_regional, estatus FROM eje_municipal");
      
      while($row=$query->fetch()){
      $item=new Estructura();
      $item->id_eje_municipal=$row['id_eje_municipal'];
      $item->descripcion=$row['descripcion'];
      $item->id_eje_regional=$row['id_eje_regional'];
      $item->estatus=$row['estatus'];
     
      
      array_push($items,$item);
      
      
      }
      return $items;
      
      }catch(PDOException $e){
      return[];
      }
      
      }
     
    public function getAldea(){
        $items=[];
       try{
      $query=$this->db->connect()->query("SELECT 
      id_aldea, 
      descripcion, 
      codigo_sucre, 
      codigo_aldea, 
      id_aldea_tipo,
      id_ambiente FROM aldea");
      
      while($row=$query->fetch()){
      $item=new Aldea();
      $item->id_aldea=$row['id_aldea'];
      $item->descripcion=$row['descripcion'];
      $item->codigo_sucre=$row['codigo_sucre'];
      $item->codigo_aldea=$row['descripcion'];
      $item->id_aldea_tipo=$row['id_aldea_tipo'];
      $item->id_ambiente=$row['id_ambiente'];
      array_push($items,$item);
      
      
      }
      return $items;
      
      }catch(PDOException $e){
      return[];

      }
    }
    public function getCentro(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT
            id_centro_estudio, descripcion, codigo, id_area_conocimiento_opsu, estatus
            FROM centro_estudio");

            while($row=$query->fetch()){

                $item=new Centro();
                $item->id_centro_estudio=$row['id_centro_estudio'];
                $item->descripcion=$row['descripcion'];
                $item->codigo=$row['codigo'];
                $item->id_area_conocimiento_opsu=$row['id_area_conocimiento_opsu'];
                $item->estatus=$row['estatus'];

                array_push($items,$item);

            }
            return $items;

        }catch(PDOException $e){
        return[];
        }

}



public function getDocencia($identificacion){
    $items=[];
    try{
        $query=$this->db->connect()->prepare("SELECT 
        identificacion,
        primer_nombre,
        primer_apellido,
        id_docencia_previa_ubv as id_docencia_previa_ubv,
        docencia_previa_ubv.id_area_conocimiento_ubv as id_area_conocimiento_ubv,
        area_conocimiento_ubv.descripcion as area_ubv,
        docencia_previa_ubv.id_docente as id_docente,
        docencia_previa_ubv.id_programa as id_programa,
        programa.descripcion as programa,
        docencia_previa_ubv.id_eje_regional as id_eje_regional,
        eje_regional.descripcion as eje_regional,
        docencia_previa_ubv.id_eje_municipal as id_eje_municipal,
        eje_municipal.descripcion as eje_municipal,
        docencia_previa_ubv.id_aldea as id_aldea,
        aldea.descripcion as aldea,
        docencia_previa_ubv.id_centro_estudio as id_centro,
        centro_estudio.descripcion as centro,
        docencia_previa_ubv.fecha_ingreso as fecha_ingreso,
        docencia_previa_ubv.estatus_verificacion as estatus_verificacion
        FROM docencia_previa_ubv, 
        area_conocimiento_ubv,
        programa, eje_regional, eje_municipal, aldea,
        centro_estudio,
        docente, persona, persona_nacionalidad
        WHERE docencia_previa_ubv.id_area_conocimiento_ubv=area_conocimiento_ubv.id_area_conocimiento_ubv
        and docencia_previa_ubv.id_programa=programa.id_programa
        and docencia_previa_ubv.id_eje_regional=eje_regional.id_eje_regional
        and docencia_previa_ubv.id_eje_municipal=eje_municipal.id_eje_municipal
        and docencia_previa_ubv.id_aldea=aldea.id_aldea
        and docencia_previa_ubv.id_centro_estudio=centro_estudio.id_centro_estudio
        and docencia_previa_ubv.id_docente=docente.id_docente 
        and docente.id_persona=persona.id_persona 
        and persona_nacionalidad.id_persona=persona.id_persona 
        and identificacion=:identificacion");

        $query->execute(['identificacion'=>$identificacion]);

        while($row=$query->fetch()){

            $item=new Docencia();
            $item->id_docencia_previa_ubv=$row['id_docencia_previa_ubv'];
            $item->id_area_conocimiento_ubv=$row['id_area_conocimiento_ubv'];
            $item->descripcion_area_ubv=$row['area_ubv'];
            $item->id_programa=$row['id_programa'];
            $item->descripcion_programa=$row['programa'];
            $item->id_eje_regional=$row['id_eje_regional'];
            $item->descripcion_eje_regional=$row['eje_regional'];
            $item->id_eje_municipal=$row['id_eje_municipal'];
            $item->descripcion_eje_municipal=$row['eje_municipal'];
            $item->id_aldea=$row['id_aldea'];
            $item->descripcion_aldea=$row['aldea'];
            $item->id_centro_estudio=$row['id_centro'];
            $item->descripcion_centro=$row['centro'];
            $item->id_docente=$row['id_docente'];
            $item->fecha_ingreso=$row['fecha_ingreso'];
            if($row['estatus_verificacion'] == 'Verificado'){
                $item->estatus_verificacion="Verificado";
            }elseif($row['estatus_verificacion'] == 'Sin Verificar'){
                $item->estatus_verificacion="Sin Verificar";
            }

            array_push($items,$item);

        }
        return $items;

    }catch(PDOException $e){
    return[];
}

}

public function getbyId($id){
    
    try{
        $item=new Docencia();
        $sql=$this->db->connect()->prepare("SELECT 
        id_docencia_previa_ubv as id_docencia_previa_ubv,
        docencia_previa_ubv.id_area_conocimiento_ubv as id_area_conocimiento_ubv,
        area_conocimiento_ubv.descripcion as area_ubv,
        docencia_previa_ubv.id_docente as id_docente,
        docencia_previa_ubv.id_programa as id_programa,
        programa.descripcion as programa,
        docencia_previa_ubv.id_eje_regional as id_eje_regional,
        eje_regional.descripcion as eje_regional,
        docencia_previa_ubv.id_eje_municipal as id_eje_municipal,
        eje_municipal.descripcion as eje_municipal,
        docencia_previa_ubv.id_aldea as id_aldea,
        aldea.descripcion as aldea,
        docencia_previa_ubv.id_centro_estudio as id_centro,
        centro_estudio.descripcion as centro,
        docencia_previa_ubv.fecha_ingreso as fecha_ingreso,
        docencia_previa_ubv.estatus_verificacion as estatus_verificacion
        FROM docencia_previa_ubv, 
        area_conocimiento_ubv,
        programa, eje_regional, eje_municipal, aldea,
        centro_estudio,
        docente, persona, persona_nacionalidad
        WHERE docencia_previa_ubv.id_area_conocimiento_ubv=area_conocimiento_ubv.id_area_conocimiento_ubv
        and docencia_previa_ubv.id_programa=programa.id_programa
        and docencia_previa_ubv.id_eje_regional=eje_regional.id_eje_regional
        and docencia_previa_ubv.id_eje_municipal=eje_municipal.id_eje_municipal
        and docencia_previa_ubv.id_aldea=aldea.id_aldea
        and docencia_previa_ubv.id_centro_estudio=centro_estudio.id_centro_estudio
        and docencia_previa_ubv.id_docente=docente.id_docente 
        and docente.id_persona=persona.id_persona 
        and persona_nacionalidad.id_persona=persona.id_persona
        and id_docencia_previa_ubv=:id_docencia_previa_ubv");

$sql->execute(['id_docencia_previa_ubv'=>$id]);

 
        while($row=$sql->fetch()){

           
            $item->id_docencia_previa_ubv=$row['id_docencia_previa_ubv'];
            $item->id_area_conocimiento_ubv=$row['id_area_conocimiento_ubv'];
            $item->descripcion_area_ubv=$row['area_ubv'];
            $item->id_docente=$row['id_docente'];
            $item->id_programa=$row['id_programa'];
            $item->descripcion_programa=$row['programa'];
            $item->id_eje_regional=$row['id_eje_regional'];
            $item->descripcion_eje_regional=$row['eje_regional'];
            $item->id_eje_municipal=$row['id_eje_municipal'];
            $item->descripcion_eje_municipal=$row['eje_municipal'];
            $item->id_aldea=$row['id_aldea'];
            $item->descripcion_aldea=$row['aldea'];
            $item->id_centro_estudio=$row['id_centro'];
            $item->descripcion_centro=$row['centro'];
            $item->fecha_ingreso=$row['fecha_ingreso'];
            if($row['estatus_verificacion'] == 'Verificado'){
                $item->estatus_verificacion="Verificado";
            }elseif($row['estatus_verificacion'] == 'Sin Verificar'){
                $item->estatus_verificacion="Sin Verificar";
            }

            array_push($items,$item);

        }
        return $item;

    }catch(PDOException $e){
    return[];
}

}


    public function update($datos){
           // var_dump($datos);
                  $query=$this->db->connect()->prepare("UPDATE docencia_previa_ubv  
                   SET id_area_conocimiento_ubv=:id_area_conocimiento_ubv,
                   id_programa=:id_programa,
                   id_eje_regional=:id_eje_regional,
                   id_eje_municipal=:id_eje_municipal,
                   id_aldea=:id_aldea,
                   id_centro_estudio=:id_centro_estudio,
                   fecha_ingreso=:fecha_ingreso,
                   estatus_verificacion=:estatus_verificacion
                   WHERE id_docencia_previa_ubv=:id_docencia_previa_ubv");

                  try{
                      $query->execute([
                          'id_docencia_previa_ubv'=>$datos['id_docencia_previa_ubv'],
                          'id_area_conocimiento_ubv'=>$datos['id_area_conocimiento_ubv'],
                          'id_programa'=>$datos['id_programa'],
                          'id_eje_regional'=>$datos['id_eje_regional'],
                          'id_eje_municipal'=>$datos['id_eje_municipal'],
                          'id_aldea'=>$datos['id_aldea'],
                          'id_centro_estudio'=>$datos['id_centro_estudio'],
                          'fecha_ingreso'=>$datos['fecha_ingreso'],
                          'estatus_verificacion'=>'Sin Verificar'

                        ]);
                   return true;

                  }catch(PDOException $e){
                       return false;
                  }
    }


    public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM docencia_previa_ubv WHERE id_docencia_previa_ubv = :id_docencia_previa_ubv");

                try{
                $query->execute([
                    'id_docencia_previa_ubv'=>$id,
                    ]);
                        return true;

                }catch(PDOException $e){
        return false;
    }

  }

}

?>