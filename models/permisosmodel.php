<?php
include_once 'models/estructura.php';
class PermisosModel extends Model{
    public function __construct(){
    parent::__construct();
    }
    public function existe($permiso){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM perfil WHERE descripcion=:descripcion");
            $sql->execute(['descripcion' =>$permiso]);
            $nombre=$sql->fetch();
            
            if($permiso==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }

    public function existeROL($id_rol,$permiso){
      try{
  
          //var_dump($cedula);
          $sql = $this->db->connect()->prepare("SELECT descripcion,id_rol FROM perfil WHERE descripcion =:descripcion");
          $sql->execute(['descripcion' =>$permiso]);
          $nombre=$sql->fetch();
          
          if($id_rol==$nombre['id_rol'] ){
              
              return $nombre['descripcion'];
  
          } 
          return false;
      } catch(PDOException $e){
          return false;
      }
  }


    public function insert($datos){
    //echo "<br>insertar datos";
    

    try{
    
    
       
             $query=$this->db->connect()->prepare('INSERT INTO perfil(descripcion,id_rol) VALUES (:descripcion,:id_rol)');

             $roles=$datos['roles'];
             
              //Recorrer el arreglo de ofertas academicas
              for ($i=0;$i<count($roles);$i++)    
              {     
            $query->execute(['descripcion'=>$datos['permiso'],'id_rol'=>$roles[$i]]);
           
              } 
             
            return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }
    
    
            }
            
          
              public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT perfil.id_perfil,perfil.descripcion AS perfil,rol.id_rol,rol.descripcion AS rol FROM perfil,rol WHERE perfil.id_rol= rol.id_rol");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_perfil=$row['id_perfil'];
              $item->perfil=$row['perfil'];
              $item->id_rol=$row['id_rol'];
              $item->rol=$row['rol'];
              
            
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }
              public function getRoles(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT *FROM rol");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_rol=$row['id_rol'];
              $item->descripcion=$row['descripcion'];
              
            
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }

              public function update($item){
   



         //  var_dump($item['parroquia'],$item['codigoine'],$item['id_municipio'],$item['id_eje_municipal'],$item['id_parroquia']);
              $query=$this->db->connect()->prepare("UPDATE perfil SET descripcion= :descripcion,id_rol=:id_rol WHERE id_perfil= :id_perfil");
            try{
                        $query->execute([
                        'descripcion'=>$item['permiso'],  
                        'id_rol'=>$item['id_rol'],  
                        'id_perfil'=>$item['id_perfil'],

  
                        
                        ]);
            return true;
                       
                        
            }catch(PDOException $e){
                return false;
                 }
            
                }
public function delete($codigo){
   

    
       $query=$this->db->connect()->prepare("DELETE FROM telefono_codigo_area WHERE id_telefono_codigo_area = :id_telefono_codigo_area");

        try{
        $query->execute([
            'id_telefono_codigo_area'=>$codigo,
            ]);
                return true;



           }catch(PDOException $e){
return false;
}


    }






    }

    ?>