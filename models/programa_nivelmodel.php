<?php
include_once 'datosacademicos/programanivel.php';

class Programa_nivelModel extends Model{
    public function __construct(){
        parent::__construct();
    }


    public function existe($programa_nivel){
        try{
            //var_dump($programa_nivel);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM programa_nivel WHERE descripcion=:descripcion");
            $sql->execute(['descripcion'=>$programa_nivel]);
            $nombre=$sql->fetch();
            
            if($programa_nivel==$nombre['descripcion']){
                return $nombre['descripcion'];
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }

    public function insert($datos){
        //echo "<br>insertar datos";
    
        try{
         //var_dump($datos);
            $query=$this->db->connect()->prepare('INSERT INTO programa_nivel(
                descripcion,
                estatus,
                codigo) VALUES(
                    :descripcion,
                    :estatus,
                    :codigo
                    )');
    
                $query->execute([
                    'descripcion'=>$datos['descripcion'],
                    'estatus'=>$datos['estatus'],
                    'codigo'=>$datos['codigo']
                ]);
    
                return true;
          
        }catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                 }
        
        
    }

    

    public function get(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT * FROM programa_nivel");
            
            while($row=$query->fetch()){
                
                $item=new ProgramaNivel();
                $item->id_programa_nivel=$row['id_programa_nivel'];
                $item->descripcion=$row['descripcion'];
                if($row['estatus'] == 1){
                    $item->estatus="Activo";
                }elseif($row['estatus'] == 0){
                    $item->estatus="Inactivo";
                }

                $item->codigo=$row['codigo'];

                array_push($items,$item);
            
            
            }
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }
      
    public function update($datos){
        //var_dump($datos);
        //var_dump($item['id_pais'],$item['estado'],$item['codigoine'],$item['id_estado']);
        $query=$this->db->connect()->prepare("UPDATE programa_nivel SET 
        descripcion=:descripcion,
        estatus=:estatus,
        codigo=:codigo
        WHERE id_programa_nivel=:id_programa_nivel");
        
        try{
            $query->execute([
                'id_programa_nivel'=>$datos['id_programa_nivel'],
                'descripcion'=>$datos['descripcion'],
                'estatus'=>$datos['estatus1'],
                'codigo'=>$datos['codigo']
                 ]);
         return true;

        }catch(PDOException $e){
             return false;
        }
         
    }

        public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM programa_nivel WHERE id_programa_nivel = :id_programa_nivel");

            try{
                $query->execute([
                'id_programa_nivel'=>$id
                ]);
                return true;
            }catch(PDOException $e){
                return false;
            }
        }
    }


    ?>