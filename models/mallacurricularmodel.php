<?php
include_once 'datosacademicos/malla_curricular.php';
include_once 'datosacademicos/modalidadmalla.php';
include_once 'datosacademicos/programa_formacion.php';

class MallacurricularModel extends Model{
    public function __construct(){
        parent::__construct();
    }

    public function existe($malla_curricular){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM malla_curricular WHERE  descripcion=:descripcion");
            $sql->execute(['descripcion' =>$malla_curricular]);
            $nombre=$sql->fetch();
            
            if($malla_curricular==$nombre['descripcion']){
                return $nombre['descripcion'];
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
        //echo "<br>insertar datos";
        try{
            
            $query=$this->db->connect()->prepare('INSERT INTO malla_curricular(
                descripcion,
                codigo,
                salida_intermedia,
                id_modalidad_malla,
                id_programa,
                estatus) VALUES(:descripcion,
                                :codigo,
                                :salida_intermedia, 
                                :id_modalidad_malla, 
                                :id_programa,
                                :estatus)');
           
            $query->execute([
                'descripcion'=>$datos['descripcion'],
                'codigo'=>$datos['codigo'],
                'salida_intermedia'=>$datos['salida_intermedia'],
                'id_modalidad_malla'=>$datos['id_modalidad_malla'],
                'id_programa'=>$datos['id_programa'],
                'estatus'=>$datos['estatus'],
                ]);
            
            return true;
       
        }
        catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                
        }
  
    }

    public function getModalidadM(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT id_modalidad_malla,descripcion FROM modalidad_malla");
            
            while($row=$query->fetch()){
                
                $item=new ModalidadMalla();
                $item->id_modalidad_malla=$row['id_modalidad_malla'];
                $item->descripcion=$row['descripcion'];
            
                array_push($items,$item);
            
            
            }
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
}
    public function getProgramaF(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT 
            id_programa,descripcion,codigo,id_centro_estudio,id_programa_tipo,id_nivel_academico,id_linea_investigacion,estatus
            FROM programa");
        
            while($row=$query->fetch()){
            
                $item=new Programa();
                $item->id_programa=$row['id_programa'];
                $item->descripcion=$row['descripcion'];
                $item->codigo=$row['codigo'];
                $item->id_centro_estudio=$row['id_centro_estudio'];
                $item->id_programa_tipo=$row['id_programa_tipo'];
                $item->id_nivel_academico=$row['id_nivel_academico'];
                $item->id_linea_investigacion=$row['id_linea_investigacion'];
                $item->estatus=$row['estatus'];
        
                array_push($items,$item);
        
        
            }
            return $items;
  
        }catch(PDOException $e){
        return[];
    }
  
}

    public function getMalla(){
            $items=[];
            try{
                $query=$this->db->connect()->query("SELECT 
                id_malla_curricular as id_malla,
                malla_curricular.descripcion AS malla,
                malla_curricular.codigo as codigo,
                malla_curricular.salida_intermedia as salida_intermedia,
                malla_curricular.id_modalidad_malla as id_modalidad,
                modalidad_malla.descripcion AS modalidad,
                malla_curricular.id_programa as id_programa,
                programa.descripcion AS programa,
                malla_curricular.estatus as estatus
                FROM malla_curricular, modalidad_malla, programa 
                WHERE malla_curricular.id_modalidad_malla=modalidad_malla.id_modalidad_malla
                and malla_curricular.id_programa=programa.id_programa");
                
                while($row=$query->fetch()){
                    
                    $item=new Malla();
                    $item->id_malla_curricular=$row['id_malla'];
                    $item->descripcion=$row['malla'];
                    $item->codigo=$row['codigo'];
                    //$item->salida_intermedia=$row['salida_intermedia'];
                    if($row['salida_intermedia'] == 1){
                        $item->salida_intermedia="Activo";
                    }elseif($row['salida_intermedia'] == 0){
                        $item->salida_intermedia="Inactivo";
                    }
                    $item->id_modalidad_malla=$row['id_modalidad'];
                    $item->descripcion_modalidad=$row['modalidad'];
                    $item->id_programa=$row['id_programa'];
                    $item->descripcion_programa=$row['programa'];
                    //$item->estatus=$row['estatus'];
                    if($row['estatus'] == 1){
                        $item->estatus="Activo";
                    }elseif($row['estatus'] == 0){
                        $item->estatus="Inactivo";
                    }
                    //var_dump($item->estatus);
                
                    array_push($items,$item);
                
                
                }
                return $items;
          
            }catch(PDOException $e){
            return[];
            }
          
    }

    public function update($datos){
            //var_dump($datos);
                  //var_dump($item['id_pais'],$item['estado'],$item['codigoine'],$item['id_estado']);
                  $query=$this->db->connect()->prepare("UPDATE malla_curricular 
                   SET descripcion=:descripcion,
                   codigo=:codigo,
                   salida_intermedia=:salida_intermedia,
                   id_modalidad_malla=:id_modalidad_malla,
                   id_programa=:id_programa,
                   estatus=:estatus
                   WHERE id_malla_curricular=:id_malla_curricular");
                  
                  try{
                      $query->execute([
                          'id_malla_curricular'=>$datos['id_malla_curricular'],
                          'descripcion'=>$datos['descripcion'],
                          'codigo'=>$datos['codigo'],
                          'salida_intermedia'=>$datos['salida_intermedia'],
                          'id_modalidad_malla'=>$datos['id_modalidad_malla'],
                          'id_programa'=>$datos['id_programa'],
                          'estatus'=>$datos['estatus'],
                            
                               ]);
                   return true;
          
                  }catch(PDOException $e){
                       return false;
                  }
                   
    }

    public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM malla_curricular WHERE id_malla_curricular = :id_malla_curricular");
        
                try{
                $query->execute([
                    'id_malla_curricular'=>$id,
                    ]);
                        return true;
        
                }catch(PDOException $e){
        return false;
    }
           
  }

}

?>