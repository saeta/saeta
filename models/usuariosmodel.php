<?php
include_once 'SED.php';
include_once 'models/persona.php';
include_once 'models/estructura.php';

class UsuariosModel extends Model{
    public function __construct(){
    parent::__construct();
    }
    public function existe($numero_documento){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT documento_identidad_tipo.descripcion AS tipo_documento,identificacion FROM persona_nacionalidad,documento_identidad_tipo WHERE persona_nacionalidad.id_documento_identidad_tipo=documento_identidad_tipo.id_documento_identidad_tipo AND identificacion=:identificacion");
            $sql->execute(['identificacion' =>$numero_documento]);
            $nombre=$sql->fetch();
            
            if($numero_documento==$nombre['identificacion']){
                
                return $nombre['tipo_documento']." ".$nombre['identificacion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }




//esta fucnicon registrar Usuarios EA utilizando una Transacion en casso de fallos
  
    public function insert($datos){
 
    try{
        //1. guardas el objeto pdo en una variable
        $pdo=$this->db->connect();
        
         //2. comienzas transaccion
         $pdo->beginTransaction();

      	//3. hacer toas las consultas comenzando desde el la variable del paso 1 (ejemplo: $sql=$pdo->prepare(...); $sql=$pdo->query(...);, etc)

        //Tabla Persona
        $query1=$pdo->prepare('INSERT INTO persona(primer_nombre, segundo_nombre, primer_apellido,segundo_apellido,fecha_nacimiento,id_estado_civil,id_genero,id_pais) VALUES
        (:primer_nombre, :segundo_nombre,:primer_apellido,:segundo_apellido,:fecha_nacimiento,:id_estado_civil,:id_genero,:id_pais)');
  
        $query1->execute(['primer_nombre'=>$datos['pnombre'], 'segundo_nombre'=>$datos['snombre'], 'primer_apellido'=>$datos['papellido'], 
        'segundo_apellido'=>$datos['sapellido'], 'fecha_nacimiento'=>date("Y/m/d", strtotime($datos['fnac'])), 'id_estado_civil'=>$datos['estadoCivil'],
         'id_genero'=>$datos['genero'], 'id_pais'=>$datos['pais']]);
    

        //Toma el id de persona
        $query2 = $pdo->prepare("SELECT id_persona  FROM persona ORDER BY id_persona DESC LIMIT 1");
        $query2 ->execute();
        $id_persona = $query2->fetch();
        $id_persona['id_persona'];

       //Tabla Ednia
       if(!empty($datos['id_etnia'])){ 
        $query3=$pdo->prepare('INSERT INTO persona_etnia(id_persona, id_etnia) VALUES
        (:id_persona, :id_etnia)');  
        $query3->execute(['id_persona'=>$id_persona['id_persona'], 'id_etnia'=>$datos['id_etnia']]);
       }
        //Tabla Persona Nacionalidad
        $query4=$pdo->prepare('INSERT INTO persona_nacionalidad(identificacion,id_nacionalidad,id_documento_identidad_tipo,id_persona) VALUES
        (:identificacion, :id_nacionalidad,:id_documento_identidad_tipo,:id_persona)');  
        $query4->execute(['identificacion'=>$datos['numero_documento'], 'id_nacionalidad'=>$datos['id_nacionalidad'], 'id_documento_identidad_tipo'=>$datos['id_documento_identidad_tipo'], 'id_persona'=>$id_persona['id_persona']]);

       //TABLE domicilio_persona
        $query4=$pdo->prepare('INSERT INTO domicilio_persona(id_parroquia,id_persona,domicilio_detalle,id_domicilio_detalle_tipo) VALUES
        (:id_parroquia,:id_persona,:domicilio_detalle,:id_domicilio_detalle_tipo)');  
        $query4->execute(['id_parroquia'=>$datos['id_parroquia'],'id_persona'=>$id_persona['id_persona'],'domicilio_detalle'=>$datos['direccion'], 'id_domicilio_detalle_tipo'=>$datos['tipo_domicilio']]);

        //TABLE persona_correo
        $query4=$pdo->prepare('INSERT INTO persona_correo(correo,id_persona,id_correo_tipo) VALUES
        (:correo, :id_persona,:id_correo_tipo)');  
        $query4->execute(['correo'=>$datos['correo'], 'id_persona'=>$id_persona['id_persona'], 'id_correo_tipo'=>$datos['id_correo_tipo']]);

        //TABLE persona_telefono
        $query4=$pdo->prepare('INSERT INTO persona_telefono(telefono,id_telefono_codigo_area,id_persona,id_telefono_tipo) VALUES
        (:telefono, :id_telefono_codigo_area,:id_persona,:id_telefono_tipo)');  
        $query4->execute(['telefono'=>$datos['telefono_numero'], 'id_telefono_codigo_area'=>$datos['id_telefono_codigo_area'], 'id_persona'=>$id_persona['id_persona'], 'id_telefono_tipo'=>$datos['id_telefono_tipo']]);

        //TABLE persona_discapacidad
        if(!empty($datos['id_discapacidad']) && !empty($datos['codigo_conais'])){ 
        $query4=$pdo->prepare('INSERT INTO persona_discapacidad(id_persona,id_discapacidad,codigo_conapdis,observacion) VALUES
        (:id_persona, :id_discapacidad,:codigo_conapdis,:observacion)');  
        $query4->execute(['id_persona'=>$id_persona['id_persona'], 'id_discapacidad'=>$datos['id_discapacidad'], 'codigo_conapdis'=>$datos['codigo_conais'], 'observacion'=>$datos['observaciones']]);
        }

        //TABLE persona_discapacidad AGREGANDO UNA SEGUNDA DISCAPACIDAD     
        if(!empty($datos['id_discapacidad1']) && !empty($datos['codigo_conais'])){ 
            $query4=$pdo->prepare('INSERT INTO persona_discapacidad(id_persona,id_discapacidad,codigo_conapdis,observacion) VALUES
            (:id_persona, :id_discapacidad,:codigo_conapdis,:observacion)');  
            $query4->execute(['id_persona'=>$id_persona['id_persona'], 'id_discapacidad'=>$datos['id_discapacidad1'], 'codigo_conapdis'=>$datos['codigo_conais'], 'observacion'=>$datos['observaciones1']]);
            }



        //TABLE usuario
        //se encripta la contraseña
        $crypt= new SED();
        $clave=$crypt->encryption($datos['numero_documento']);

        $query4=$pdo->prepare('INSERT INTO usuario(id_persona,usuario,password,fecha,estatus,id_perfil) VALUES
        (:id_persona, :usuario,:password,:fecha,:estatus,:id_perfil)');  
        $query4->execute(['id_persona'=>$id_persona['id_persona'], 'usuario'=>'ubv'.$datos['numero_documento'],
        'password'=>$clave, 'fecha'=>date('Y-m-d H:i:s'), 'estatus'=>$datos['estatus'], 'id_perfil'=>$datos['id_perfil']]);
     

        //Toma el id de usuario
        $query2 = $pdo->prepare("SELECT id_usuario  FROM usuario ORDER BY id_usuario DESC LIMIT 1");
        $query2 ->execute();
        $usuario = $query2->fetch();
        $usuario['id_usuario'];

            //tabla rol
            $roles=$datos['id_rol'];

          // var_dump( $usuario['id_usuario'],$roles);
            $query=$pdo->prepare('INSERT INTO usuario_rol(id_usuario, id_rol) VALUES (:id_usuario, :id_rol)');
                  //Recorrer el arreglo de ofertas academicas
                for ($i=0;$i<count($roles);$i++)    
                {       
            $query->execute(['id_usuario'=>$usuario['id_usuario'],'id_rol'=>$roles[$i] ]);
                } 



          //4. consignas la transaccion (en caso de que no suceda ningun fallo)
          $pdo->commit();        
          return true;

      
    }catch(PDOException $e){
     //5. regresas a un estado anterior en caso de error
     $pdo->rollBack();
     return false;
             }
    
            }
            
          
            public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT persona.id_persona,
              nacionalidad.descripcion AS nacionalidad,
              persona_nacionalidad.identificacion,
              primer_nombre,
              primer_apellido,
              usuario.fecha AS registro,
              perfil.descripcion AS perfil,
              usuario,
              password,
              estatus,
              documento_identidad_tipo.id_documento_identidad_tipo,
              documento_identidad_tipo.descripcion as tipo_documento
              FROM persona,persona_nacionalidad,nacionalidad,perfil,usuario, documento_identidad_tipo
              WHERE  persona.id_persona=persona_nacionalidad.id_persona 
              AND persona_nacionalidad.id_nacionalidad=nacionalidad.id_nacionalidad 
              AND  persona.id_persona=usuario.id_persona 
              AND  persona_nacionalidad.id_documento_identidad_tipo=documento_identidad_tipo.id_documento_identidad_tipo 
              AND persona_nacionalidad.identificacion!=12345678
              AND  usuario.id_perfil=perfil.id_perfil;");
              /*
                FECHA: Sabado, 25 JULIO 2020
                cambio en la consulta que trae todos los usuarios,
                agregue la exepcion de el usuario administrados 
                'AND persona_nacionalidad.identificacion!=12345678'
              */
              while($row=$query->fetch()){
              $item=new Persona();
              $item->id_persona=$row['id_persona'];
              $item->nacionalidad=$row['nacionalidad'];
              $item->identificacion=$row['identificacion'];
              $item->primer_nombre=$row['primer_nombre'];   
              $item->primer_apellido=$row['primer_apellido'];
              $item->registro=$row['registro'];
              $item->perfil=$row['perfil'];
              $item->usuario=$row['usuario'];
              $item->password=$row['password'];
              $item->estatus=$row['estatus'];
              $item->id_documento_identidad_tipo=$row['id_documento_identidad_tipo'];
              $item->tipo_documento=$row['tipo_documento'];

              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }

             
              //////////////    funcion que llama datos de tablas catalogos   ///////////////
              public function getCatalogo($valor){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT * FROM ".$valor."");
              
              while($row=$query->fetch()){
              $item=new Persona();
              $item->id=$row['id_'.$valor.''];
              $item->descripcion=$row['descripcion'];
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }
            ///////////////////////////////////////////////////////

              public function getPais(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT * FROM pais WHERE estatus='true'");
              
              while($row=$query->fetch()){
              $item=new Persona();
              $item->id_pais=$row['id_pais'];
              $item->descripcion=$row['descripcion'];

              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }
            

              

              public function getEstado(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT *FROM estado");
                            
              while($row=$query->fetch()){
              $item=new Persona();
              $item->id_estado=$row['id_estado'];
              $item->descripcion=$row['descripcion'];
              $item->id_pais=$row['id_pais'];
              
          

              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }

              public function getNacionalidad(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT * FROM nacionalidad");
              
              while($row=$query->fetch()){
              $item=new Persona();
              $item->id_nacionalidad=$row['id_nacionalidad'];
              $item->descripcion=$row['descripcion'];
              $item->id_pais=$row['id_pais'];

              array_push($items,$item);
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }

              public function getCiudad(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT *FROM ciudad");
                            
              while($row=$query->fetch()){
              $item=new Persona();
              $item->id_ciudad=$row['id_ciudad'];
              $item->descripcion=$row['descripcion'];
              $item->id_estado=$row['id_estado'];

              array_push($items,$item);
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }


//////////////////////////////////////////////////////////////////////
//CONSULTA ANTES DE MODIFICACION
public function getbyIdUsuarioInfoRESPALDO($id_persona){
  $item = new Persona();
 
  $query = $this->db->connect()->prepare("SELECT persona.id_persona,primer_nombre,segundo_nombre,primer_apellido,segundo_apellido,fecha_nacimiento,estado_civil.id_estado_civil,
  estado_civil.descripcion AS estado_civil,genero.id_genero, genero.descripcion AS genero,pais.id_pais,pais.descripcion AS pais,
  estado.id_estado,estado.descripcion AS estado,municipio.id_municipio, municipio.descripcion AS municipio,identificacion,
  nacionalidad.id_nacionalidad,nacionalidad.descripcion AS nacionalidad,documento_identidad_tipo.id_documento_identidad_tipo ,
  documento_identidad_tipo.descripcion AS documento_identidad,telefono,telefono_tipo.id_telefono_tipo,telefono_tipo.descripcion AS telefono_tipo,
  telefono_codigo_area.id_telefono_codigo_area,telefono_codigo_area.descripcion AS telefono_coodigo_area,correo,correo_tipo.id_correo_tipo,
  correo_tipo.descripcion AS correo_tipo,etnia.descripcion AS etnia,parroquia.id_parroquia,parroquia.descripcion AS parroquia,
  domicilio_persona.domicilio_detalle,domicilio_detalle_tipo.id_domicilio_detalle_tipo,
  domicilio_detalle_tipo.descripcion AS domicilio_detalle_tipo, etnia.id_etnia,usuario,password,fecha,usuario.estatus AS usuario_estatus,perfil.id_perfil,perfil.descripcion AS perfil 
   FROM estado_civil,genero,pais,persona_nacionalidad,nacionalidad,documento_identidad_tipo,persona_telefono,telefono_tipo,telefono_codigo_area,
  persona_correo,correo_tipo,domicilio_persona,domicilio_detalle_tipo,usuario,perfil,parroquia,municipio,estado, persona  
  FULL JOIN persona_etnia ON persona.id_persona=persona_etnia.id_persona 
  FULL JOIN etnia ON persona_etnia.id_etnia=etnia.id_etnia    
  WHERE
  persona.id_estado_civil=estado_civil.id_estado_civil
  AND  persona.id_genero=genero.id_genero 
  AND  persona.id_pais=pais.id_pais
  AND persona.id_persona=persona_nacionalidad.id_persona 
  AND persona_nacionalidad.id_nacionalidad=nacionalidad.id_nacionalidad 
  AND persona_nacionalidad.id_documento_identidad_tipo=documento_identidad_tipo.id_documento_identidad_tipo 
  AND persona.id_persona=persona_telefono.id_persona 
  AND  persona_telefono.id_telefono_tipo=telefono_tipo.id_telefono_tipo 
  AND persona_telefono.id_telefono_codigo_area=telefono_codigo_area.id_telefono_codigo_area
  AND persona.id_persona=persona_correo.id_persona
  AND persona_correo.id_correo_tipo=correo_tipo.id_correo_tipo
  AND persona.id_persona=domicilio_persona.id_persona
  AND domicilio_persona.id_domicilio_detalle_tipo=domicilio_detalle_tipo.id_domicilio_detalle_tipo 
  AND persona.id_persona=usuario.id_persona
  AND perfil.id_perfil=usuario.id_perfil 
  AND domicilio_persona.id_parroquia=parroquia.id_parroquia
  AND parroquia.id_municipio=municipio.id_municipio
  AND municipio.id_estado=estado.id_estado
  AND persona.id_persona=:id_persona");
  
  try{
      $query ->execute(['id_persona'=>$id_persona]);

      while($row = $query->fetch()){

      
          $item->id_persona = $row['id_persona'];
          $item->primer_nombre = $row['primer_nombre'];
          $item->segundo_nombre = $row['segundo_nombre'];
          $item->primer_apellido = $row['primer_apellido'];
          $item->segundo_apellido = $row['segundo_apellido'];
          $item->fecha_nacimiento =  date("m/d/Y", strtotime($row['fecha_nacimiento']));
          $item->id_estado_civil = $row['id_estado_civil'];
          $item->estado_civil = $row['estado_civil'];
          $item->id_genero = $row['id_genero'];
          $item->genero = $row['genero'];
          $item->id_pais = $row['id_pais'];
          $item->pais = $row['pais'];
          $item->id_estado = $row['id_estado'];
          $item->estado = $row['estado'];
          $item->id_municipio = $row['id_municipio'];
          $item->municipio = $row['municipio'];
          $item->identificacion = $row['identificacion'];
          $item->id_nacionalidad = $row['id_nacionalidad'];
          $item->nacionalidad = $row['nacionalidad'];
          $item->id_documento_identidad_tipo = $row['id_documento_identidad_tipo'];
          $item->documento_identidad = $row['documento_identidad'];
          $item->telefono = $row['telefono'];
          $item->id_telefono_tipo = $row['id_telefono_tipo'];
          $item->telefono_tipo = $row['telefono_tipo'];
          $item->id_telefono_codigo_area = $row['id_telefono_codigo_area'];
          $item->telefono_coodigo_area = $row['telefono_coodigo_area'];
          $item->correo = $row['correo'];
          $item->id_correo_tipo = $row['id_correo_tipo'];
          $item->correo_tipo = $row['correo_tipo'];
          $item->id_etnia = $row['id_etnia'];
          $item->etnia = $row['etnia'];
          $item->codigo_conapdis = $row['codigo_conapdis'];
          $item->observacion = $row['observacion'];
         
         // se quito discapacida ya que una persona puede tener varias discapacidades esta se consultara aparte com usuario que es igual
        //  $item->tipo_discapacidad = $row['tipo_discapacidad'];
        //  $item->discapacidad = $row['discapacidad'];
        
          // $item->ciudad = $row['ciudad'];
        // $item->estado = $row['estado'];
          $item->id_parroquia = $row['id_parroquia'];
          $item->parroquia = $row['parroquia'];
          $item->domicilio_detalle = $row['domicilio_detalle'];
          $item->id_domicilio_detalle_tipo = $row['id_domicilio_detalle_tipo'];
          $item->domicilio_detalle_tipo = $row['domicilio_detalle_tipo'];
       //usuario
    
          $item->usuario = $row['usuario'];
          //se muestra la consrasea desencriptada
          $crypt= new SED();
          $item->password = $crypt->decryption($row['password']);
          $item->fecha = $row['fecha'];
          $item->usuario_estatus = $row['usuario_estatus'];
          $item->id_perfil = $row['id_perfil'];
          $item->perfil = $row['perfil'];
          
     }


      return $item;
  }catch(PDOException  $e){
      return null;
  }
}

/* MODIFICADO EL 31/03/2020: 
sustituida para agregar fecha de ingreso y nro de hijos a la consulta
*/
    public function getbyIdUsuarioInfo($id_persona){
      $item = new Persona();
     
      $query = $this->db->connect()->prepare("SELECT persona.id_persona,primer_nombre,segundo_nombre,primer_apellido,segundo_apellido,fecha_nacimiento,estado_civil.id_estado_civil,
      estado_civil.descripcion AS estado_civil,genero.id_genero, genero.descripcion AS genero,pais.id_pais,pais.descripcion AS pais,
      estado.id_estado,estado.descripcion AS estado,municipio.id_municipio, municipio.descripcion AS municipio,identificacion,
      nacionalidad.id_nacionalidad,nacionalidad.descripcion AS nacionalidad,documento_identidad_tipo.id_documento_identidad_tipo ,
      documento_identidad_tipo.descripcion AS documento_identidad,telefono,telefono_tipo.id_telefono_tipo,telefono_tipo.descripcion AS telefono_tipo,
      telefono_codigo_area.id_telefono_codigo_area,telefono_codigo_area.descripcion AS telefono_coodigo_area,
      etnia.descripcion AS etnia,parroquia.id_parroquia,parroquia.descripcion AS parroquia,
      domicilio_persona.domicilio_detalle,domicilio_detalle_tipo.id_domicilio_detalle_tipo,
      domicilio_detalle_tipo.descripcion AS domicilio_detalle_tipo, etnia.id_etnia,usuario,password,usuario.fecha,usuario.estatus AS usuario_estatus,perfil.id_perfil,perfil.descripcion AS perfil,
      persona_fecha_ingreso.fecha as fecha_ingreso, persona_hijo.descripcion as nro_hijos
       FROM estado_civil,genero,pais,persona_nacionalidad,nacionalidad,documento_identidad_tipo,
      domicilio_persona,domicilio_detalle_tipo,usuario,perfil,parroquia,municipio,estado, persona_fecha_ingreso, persona_hijo, 
      persona 
      FULL JOIN persona_telefono ON persona.id_persona=persona_telefono.id_persona  
            FULL JOIN telefono_tipo ON persona_telefono.id_telefono_tipo=telefono_tipo.id_telefono_tipo  
                  FULL JOIN telefono_codigo_area ON persona_telefono.id_telefono_codigo_area=telefono_codigo_area.id_telefono_codigo_area  

      FULL JOIN persona_etnia ON persona.id_persona=persona_etnia.id_persona 
      FULL JOIN etnia ON persona_etnia.id_etnia=etnia.id_etnia    
      WHERE
      persona.id_estado_civil=estado_civil.id_estado_civil
      AND  persona.id_genero=genero.id_genero 
      AND  persona.id_pais=pais.id_pais
      AND persona.id_persona=persona_nacionalidad.id_persona 
      AND persona_nacionalidad.id_nacionalidad=nacionalidad.id_nacionalidad 
      AND persona_nacionalidad.id_documento_identidad_tipo=documento_identidad_tipo.id_documento_identidad_tipo 
      AND persona.id_persona=domicilio_persona.id_persona
      AND domicilio_persona.id_domicilio_detalle_tipo=domicilio_detalle_tipo.id_domicilio_detalle_tipo 
      AND persona.id_persona=usuario.id_persona
      AND perfil.id_perfil=usuario.id_perfil 
      AND domicilio_persona.id_parroquia=parroquia.id_parroquia
      AND parroquia.id_municipio=municipio.id_municipio
      AND municipio.id_estado=estado.id_estado
      AND persona_hijo.id_persona=persona.id_persona
      AND persona_fecha_ingreso.id_persona=persona.id_persona
      AND persona.id_persona=:id_persona");
      
      try{
          $query->execute(['id_persona'=>$id_persona]);

          while($row = $query->fetch()){

          
              $item->id_persona = $row['id_persona'];
              $item->primer_nombre = $row['primer_nombre'];
              $item->segundo_nombre = $row['segundo_nombre'];
              $item->primer_apellido = $row['primer_apellido'];
              $item->segundo_apellido = $row['segundo_apellido'];
              $item->fecha_nacimiento =  date("m/d/Y", strtotime($row['fecha_nacimiento']));
              $item->id_estado_civil = $row['id_estado_civil'];
              $item->estado_civil = $row['estado_civil'];
              $item->id_genero = $row['id_genero'];
              $item->genero = $row['genero'];
              $item->id_pais = $row['id_pais'];
              $item->pais = $row['pais'];
              $item->id_estado = $row['id_estado'];
              $item->estado = $row['estado'];
              $item->id_municipio = $row['id_municipio'];
              $item->municipio = $row['municipio'];
              $item->identificacion = $row['identificacion'];
              $item->id_nacionalidad = $row['id_nacionalidad'];
              $item->nacionalidad = $row['nacionalidad'];
              $item->id_documento_identidad_tipo = $row['id_documento_identidad_tipo'];
              $item->documento_identidad = $row['documento_identidad'];
              $item->telefono = $row['telefono'];
              $item->id_telefono_tipo = $row['id_telefono_tipo'];
              $item->telefono_tipo = $row['telefono_tipo'];
              $item->id_telefono_codigo_area = $row['id_telefono_codigo_area'];
              $item->telefono_coodigo_area = $row['telefono_coodigo_area'];
              
              //traer los correos del usuario
              $correos = $this->db->connect()->prepare("SELECT t2.id_persona_correo,
              t1.id_persona, 
              t2.id_correo_tipo, 
              t3.descripcion as correo_tipo,
              t2.correo 
              from persona as t1, 
              persona_correo as t2,
              correo_tipo as t3 where 
              t1.id_persona=t2.id_persona 
              and t2.id_correo_tipo=t3.id_correo_tipo
              and t1.id_persona=:id_persona");
              $correos->execute(['id_persona'=>$id_persona]);
              while($row2 = $correos->fetch()){
                
                if($row2['id_correo_tipo']==5){//personal
                  $item->correo_personal = [
                    "id_persona_correo"=>$row2['id_persona_correo'],
                    "id_correo_tipo"=>$row2['id_correo_tipo'],
                    "correo_tipo"=>$row2['correo_tipo'],
                    "correo"=>$row2['correo']
                    ];
                }

                if($row2['id_correo_tipo']==6){//institucional
                  $item->correo_institucional = [
                    "id_persona_correo"=>$row2['id_persona_correo'],
                    "id_correo_tipo"=>$row2['id_correo_tipo'],
                    "correo_tipo"=>$row2['correo_tipo'],
                    "correo"=>$row2['correo']
                    ];
                }
              }
              
              $item->id_etnia = $row['id_etnia'];
              $item->etnia = $row['etnia'];
              $item->codigo_conapdis = $row['codigo_conapdis'];
              $item->observacion = $row['observacion'];
             
             // se quito discapacida ya que una persona puede tener varias discapacidades esta se consultara aparte com usuario que es igual
            //  $item->tipo_discapacidad = $row['tipo_discapacidad'];
            //  $item->discapacidad = $row['discapacidad'];
            
              // $item->ciudad = $row['ciudad'];
            // $item->estado = $row['estado'];
              $item->id_parroquia = $row['id_parroquia'];
              $item->parroquia = $row['parroquia'];
              $item->domicilio_detalle = $row['domicilio_detalle'];
              $item->id_domicilio_detalle_tipo = $row['id_domicilio_detalle_tipo'];
              $item->domicilio_detalle_tipo = $row['domicilio_detalle_tipo'];
           //usuario
        
              $item->usuario = $row['usuario'];
              //se muestra la consrasea desencriptada
              $crypt= new SED();
              $item->password = $crypt->decryption($row['password']);
              $item->fecha = $row['fecha'];
              $item->usuario_estatus = $row['usuario_estatus'];
              $item->id_perfil = $row['id_perfil'];
              if($row['id_perfil']==1){//si es igual trabajador academico
                $docente = $this->db->connect()->prepare("SELECT t1.id_docente,
                t1.id_centro_estudio,
                t3.descripcion as centro_estudio,
                t1.id_eje_municipal,
                t4.descripcion as eje_municipal,
                t1.id_docente_dedicacion,
                t5.descripcion as docente_dedicacion,
                t1.id_clasificacion_docente,
                t6.descripcion as clasificacion_docente,
                t1.id_escalafon,
                t7.descripcion as escalafon,
                t1.id_docente_estatus,
                t8.descripcion as docente_estatus,
                t4.id_eje_regional,
                t9.descripcion as eje_regional
                FROM docente as t1,
                centro_estudio as t3,
                eje_municipal as t4,
                docente_dedicacion as t5,
                clasificacion_docente as t6,
                escalafon as t7,
                docente_estatus as t8,
                eje_regional as t9
                WHERE 
                t1.id_centro_estudio=t3.id_centro_estudio
                and t1.id_eje_municipal=t4.id_eje_municipal
                and t1.id_docente_dedicacion=t5.id_docente_dedicacion
                and t1.id_clasificacion_docente=t6.id_clasificacion_docente
                and t1.id_escalafon=t7.id_escalafon
                and t1.id_docente_estatus=t8.id_docente_estatus
                and t4.id_eje_regional=t9.id_eje_regional
                and id_persona=:id_persona;");

                $docente->execute(['id_persona'=>$id_persona]);

                while($row1 = $docente->fetch()){
                  $item->id_docente = $row1['id_docente'];
                  $item->id_centro_estudio = $row1['id_centro_estudio'];
                  $item->centro_estudio = $row1['centro_estudio'];
                  $item->id_eje_municipal = $row1['id_eje_municipal'];
                  $item->eje_municipal = $row1['eje_municipal'];
                  $item->id_docente_dedicacion = $row1['id_docente_dedicacion'];
                  $item->docente_dedicacion = $row1['docente_dedicacion'];
                  $item->id_clasificacion_docente = $row1['id_clasificacion_docente'];
                  $item->clasificacion_docente = $row1['clasificacion_docente'];
                  $item->id_escalafon = $row1['id_escalafon'];
                  $item->escalafon = $row1['escalafon'];
                  $item->id_docente_estatus = $row1['id_docente_estatus'];
                  $item->docente_estatus = $row1['docente_estatus'];
                  $item->id_eje_regional = $row1['id_eje_regional'];
                  $item->eje_regional = $row1['eje_regional'];

                  $existe_docente_aldea=$this->db->connect()->prepare("SELECT count(*) from docente_aldea where id_docente=:id_docente");
                  $existe_docente_aldea->execute(['id_docente'=>$row1['id_docente']]);
                  $doc=$existe_docente_aldea->fetch();
                  if($doc[0]>0){
                    $aldea_docente=$this->db->connect()->prepare("SELECT id_docente_aldea,
                    t1.id_docente,
                    t1.id_aldea_ubv,
                    t2.descripcion as aldea_docente
                    from docente_aldea as t1, 
                    aldea_ubv as t2 
                    where t1.id_aldea_ubv=t2.id_aldea_ubv 
                    and id_docente=:id_docente");
                    $aldea_docente->execute(['id_docente'=>$row1['id_docente']]);
                    $ald=$aldea_docente->fetch();
                    $item->id_aldea_ubv = $ald['id_aldea_ubv'];
                    $item->aldea_docente = $ald['aldea_docente'];
                  }
                  
                }
              }

              $item->perfil = $row['perfil'];

              //FECHA DE INGRESO Y NRO DE HIJOS
              $item->fecha_ingreso =  date("m/d/Y", strtotime($row['fecha_ingreso']));
              $item->nro_hijos = $row['nro_hijos'];

              

          }
          return $item;
      }catch(PDOException  $e){
          return null;
      }
  }

  public function getPreguntas_u($id_persona){
    try{
      $items=[];
      //traer las preguntas
      $preguntas = $this->db->connect()->prepare("SELECT t2.id_usuario,
      t2.id_pregunta,
      t3.descripcion as pregunta,
      t2.respuesta
      from usuario as t1,
      usuario_pregunta as t2,
      pregunta as t3
      where t2.id_usuario=t1.id_usuario
      and t2.id_pregunta=t3.id_pregunta
      and t1.id_persona=:id_persona;");
      $preguntas->execute(['id_persona'=>$id_persona]);
      $i=0;
      while($row=$preguntas->fetch(PDO::FETCH_NUM)){
        $item = new Persona();
        $item->id_pregunta = $row[$i];
        $item->pregunta = $row[$i];
        $item->respuesta = $row[$i];
        $i++;
        array_push($items,$item);

      }

      return $items;
    }catch(PDOException  $e){
      return null;
    }
  }
// informacion de usuario en este caso roles pues un usuario puede tener muchos roles
// MODIFICADO EL 31/03/2020: Arregle la consulta ya que no estaba correctamente estructurada
  public function getbyIdUsuarioInfoU($id_persona){
         $items=[];
    $query = $this->db->connect()->prepare("SELECT usuario_rol.id_usuario, 
    usuario_rol.id_rol, rol.descripcion AS rol 
    FROM rol,usuario_rol,usuario, persona
    WHERE usuario_rol.id_rol=rol.id_rol
    AND usuario_rol.id_usuario=usuario.id_usuario 
    AND usuario.id_persona=persona.id_persona
    AND usuario.id_persona=:id_persona");
    try{
        $query ->execute(['id_persona'=>$id_persona]);

        while($row = $query->fetch()){
            $item = new Persona();
            $item->id_usuario = $row['id_usuario'];

       //rol       
            $item->id_rol = $row['id_rol'];
            $item->rol = $row['rol'];
         //  var_dump($row['usuario']);
            array_push($items,$item);
       }


        return $items;
    }catch(PDOException  $e){
        return null;
    }
}

public function getProgramasDocente($id_docente){
  $items=[];
  $query = $this->db->connect()->prepare("select t1.id_docente_programa,
  t1.id_programa,
  t3.descripcion as programa,
  t1.id_docente
  from docente_programa as t1,
  docente as t2,
  programa as t3
  where t1.id_docente=t2.id_docente
  and t1.id_programa=t3.id_programa
  and t1.id_docente=:id_docente;");
  try{
    $query ->execute(['id_docente'=>$id_docente]);

    while($row = $query->fetch()){
        $item = new Persona();
        $item->id_docente_programa = $row['id_docente_programa'];
        $item->id_programa = $row['id_programa'];
        $item->programa = $row['programa'];
        $item->id_docente = $row['id_docente'];
        array_push($items,$item);
    }
    return $items;
  }catch(PDOException  $e){
    return null;
  }
}

public function getProgramaDocentebyID($id_docente_programa){

  try{
      $query=$this->db->connect()->prepare("SELECT t1.id_docente_programa,
      t1.id_programa,
      t3.descripcion as programa,
      t1.id_docente
      from docente_programa as t1,
      docente as t2,
      programa as t3
      where t1.id_docente=t2.id_docente
      and t1.id_programa=t3.id_programa
      and t1.id_docente_programa=:id_docente_programa;");
      $query->execute(['id_docente_programa'=>$id_docente_programa]);

      $item=new Estructura();
      while($row=$query->fetch()){
        $item->id_docente_programa = $row['id_docente_programa'];
        $item->id_programa = $row['id_programa'];
        $item->programa = $row['programa'];
        $item->id_docente = $row['id_docente'];
      }
    
      return $item;
  } catch(PDOException $e){
      return false;
  }
}
//para las discapacidades

public function getbyIdUsuarioInfoD($id_persona){
  $items=[];

$query = $this->db->connect()->prepare("SELECT persona.id_persona,persona_discapacidad.id_persona_discapacidad,discapacidad.id_discapacidad,codigo_conapdis,observacion,discapacidad.descripcion AS discapacidad,tipo_discapacidad.id_tipo_discapacidad,tipo_discapacidad.descripcion AS tipo_discapacidad  
FROM persona
FULL JOIN persona_discapacidad  ON persona.id_persona=persona_discapacidad.id_persona
FULL JOIN discapacidad  ON discapacidad.id_discapacidad=persona_discapacidad.id_discapacidad
FULL JOIN tipo_discapacidad  ON discapacidad.id_tipo_discapacidad=tipo_discapacidad.id_tipo_discapacidad
WHERE persona.id_persona=:id_persona");

try{
 $query ->execute(['id_persona'=>$id_persona]);

 while($row = $query->fetch()){
     $item = new Persona();
        
     $item->id_persona = $row['id_persona'];
     $item->id_persona_discapacidad = $row['id_persona_discapacidad'];
     $item->id_discapacidad = $row['id_discapacidad'];
     $item->codigo_conapdis = $row['codigo_conapdis'];
     $item->observacion = $row['observacion'];
     $item->discapacidad = $row['discapacidad'];
     $item->id_tipo_discapacidad = $row['id_tipo_discapacidad'];
     $item->tipo_discapacidad = $row['tipo_discapacidad'];
     //var_dump($row['discapacidad']);
     array_push($items,$item);
}


 return $items;
}catch(PDOException $e){
 return null;
}
}

public function getbyIdUsuarioInfoN($id_persona){
  $item = new Persona();
$query = $this->db->connect()->prepare("SELECT
persona.id_persona,
persona_hijo.id_persona_hijo,
persona_hijo.descripcion as numero_hijos,
persona_fecha_ingreso.id_persona_fecha_ingreso,
persona_fecha_ingreso.fecha as fecha_ingreso,
clasificacion_docente.id_clasificacion_docente,
clasificacion_docente.descripcion as clasificacion_docente,
escalafon.id_escalafon,
escalafon.descripcion as cargo,
docente_dedicacion.id_docente_dedicacion,
docente_dedicacion.descripcion as dedicacion,
docente_estatus.id_docente_estatus,
docente_estatus.descripcion as estatus,
nacionalidad.id_nacionalidad,
nacionalidad.descripcion as nacionalidad,
nacionalidad.id_pais,
pais.descripcion as pais


FROM persona,persona_hijo,persona_fecha_ingreso,clasificacion_docente, 
docente,escalafon,docente_dedicacion,docente_estatus,nacionalidad,persona_nacionalidad,pais
WHERE persona.id_persona=persona_hijo.id_persona
AND persona_hijo.id_persona_hijo=persona_hijo.id_persona_hijo
AND persona.id_persona=persona_fecha_ingreso.id_persona
AND persona_fecha_ingreso.id_persona_fecha_ingreso=persona_fecha_ingreso.id_persona_fecha_ingreso
AND persona.id_persona=docente.id_persona
AND docente.id_clasificacion_docente=clasificacion_docente.id_clasificacion_docente
AND docente.id_escalafon=escalafon.id_escalafon
AND docente.id_docente_dedicacion=docente_dedicacion.id_docente_dedicacion
AND docente.id_docente_estatus=docente_estatus.id_docente_estatus
AND persona.id_persona=persona_nacionalidad.id_persona 
AND persona_nacionalidad.id_nacionalidad=nacionalidad.id_nacionalidad
AND nacionalidad.id_pais=pais.id_pais
AND persona.id_persona=:id_persona");

try{
 $query ->execute(['id_persona'=>$id_persona]);

 while($row = $query->fetch()){
     
     $item->id_persona = $row['id_persona'];
     $item->numero_hijos=$row['numero_hijos'];
     $item->fecha_ingreso=$row['fecha_ingreso'];
     $item->clasificacion_docente=$row['clasificacion_docente'];
     $item->cargo=$row['cargo'];
     $item->dedicacion=$row['dedicacion'];
     $item->estatus=$row['estatus'];
     $item->nacionalidad=$row['nacionalidad'];
     $item->pais=$row['pais'];

              $item->usuario = $row['usuario'];
              //se muestra la consrasea desencriptada
              $crypt= new SED();
              $item->password = $crypt->decryption($row['password']);
              $item->fecha = $row['fecha'];
              $item->usuario_estatus = $row['usuario_estatus'];
              $item->id_perfil = $row['id_perfil'];
              $item->perfil = $row['perfil'];

     
}


 return $item;
}catch(PDOException  $e){
 return null;
}
}
////////////////////////////////////////////////////////////////////

              public function update($datos){
   
                try{
                  //var_dump($datos);
                  
                     //1. guardas el objeto pdo en una variable
                    $pdo=$this->db->connect();
                    
                    //2. comienzas transaccion
                    $pdo->beginTransaction();

                     //3. hacer toas las consultas comenzando desde el la variable del paso 1 (ejemplo: $sql=$pdo->prepare(...); $sql=$pdo->query(...);, etc)

                    //Tabla Persona
                    //ESTAR PENDIENTE CON FECHA YA QUE SI SE DEJA LA MISMA ESTA YA VIENE FORMATEADA Y SE VUELVE A FORMATEAR Y SE PIERDE ..
                      $query1=$pdo->prepare('UPDATE  persona SET primer_nombre=:primer_nombre, segundo_nombre=:segundo_nombre, primer_apellido=:primer_apellido,
                      segundo_apellido=:segundo_apellido,fecha_nacimiento=:fecha_nacimiento,id_estado_civil=:id_estado_civil,id_genero=:id_genero,id_pais=:id_pais WHERE id_persona=:id_persona');
                
                      $query1->execute([
                        'primer_nombre'=>$datos['pnombre'], 
                        'segundo_nombre'=>$datos['snombre'], 
                        'primer_apellido'=>$datos['papellido'], 
                      'segundo_apellido'=>$datos['sapellido'], 
                      'fecha_nacimiento'=>date("Y/m/d", strtotime($datos['fnac'])), 
                      'id_estado_civil'=>$datos['estadoCivil'],
                       'id_genero'=>$datos['genero'], 
                       'id_pais'=>$datos['pais'], 
                       'id_persona'=>$datos['id_persona']]);

            
                     //Tabla Ednia
                     $query=$pdo->prepare('DELETE FROM persona_etnia WHERE id_persona=:id_persona');
                     //Recorrer el arreglo de ofertas academicas
                     $query->execute(['id_persona'=>$datos['id_persona']]);

                     if(!empty($datos['id_etnia'])){ 
             
                    $query3=$pdo->prepare('INSERT INTO persona_etnia(id_persona, id_etnia) VALUES
                    (:id_persona, :id_etnia)');  
                    $query3->execute(['id_persona'=>$datos['id_persona'], 'id_etnia'=>$datos['id_etnia']]);
                    }
                                    
                      //Tabla Persona Nacionalidad
                      $query4=$pdo->prepare('UPDATE persona_nacionalidad SET 
                      identificacion=:identificacion,
                      id_nacionalidad=:id_nacionalidad,
                      id_documento_identidad_tipo=:id_documento_identidad_tipo 
                      WHERE id_persona=:id_persona');  
                      
                      $query4->execute(['identificacion'=>$datos['numero_documento'], 
                      'id_nacionalidad'=>$datos['id_nacionalidad'],
                     'id_documento_identidad_tipo'=>$datos['id_documento_identidad_tipo'], 
                     'id_persona'=>$datos['id_persona']]);
             
                     
                    // var_dump($datos['id_parroquia'],$datos['direccion'],$datos['tipo_domicilio'],$datos['id_persona']);
                     //TABLE domicilio_persona
                     //REVISAR ID_PARROQUIA 
                      $query4=$pdo->prepare('UPDATE domicilio_persona SET 
                      id_parroquia=:id_parroquia,
                      domicilio_detalle=:domicilio_detalle,
                      id_domicilio_detalle_tipo=:id_domicilio_detalle_tipo 
                      WHERE domicilio_persona.id_persona=:id_persona ');  
                      
                      $query4->execute([
                        'id_parroquia'=>$datos['id_parroquia'],
                        'domicilio_detalle'=>$datos['direccion'],
                        'id_domicilio_detalle_tipo'=>$datos['tipo_domicilio'],
                        'id_persona'=>$datos['id_persona']]);
                    
                     
                      //TABLE persona_correo
                      $query4=$pdo->prepare('UPDATE persona_correo SET correo=:correo,id_correo_tipo=:id_correo_tipo  WHERE id_persona=:id_persona ');  
                      $query4->execute(['correo'=>$datos['correo'],'id_correo_tipo'=>$datos['id_correo_tipo'],'id_persona'=>$datos['id_persona']]);
              
                      //TABLE persona_telefono

                     // var_dump($datos['telefono_numero'],$datos['id_telefono_codigo_area'],$datos['id_telefono_tipo'],$datos['id_persona']);
                      $query4=$pdo->prepare('UPDATE persona_telefono SET telefono=:telefono,id_telefono_codigo_area=:id_telefono_codigo_area
                      ,id_telefono_tipo=:id_telefono_tipo WHERE id_persona=:id_persona');  
                      $query4->execute(['telefono'=>$datos['telefono_numero'], 'id_telefono_codigo_area'=>$datos['id_telefono_codigo_area'], 'id_telefono_tipo'=>$datos['id_telefono_tipo'], 'id_persona'=>$datos['id_persona']]);
              
                      //TABLE persona_discapacidad
                     //var_dump($datos['id_persona_discapacidad'],$datos['id_discapacidad'],$datos['codigo_conais']);
                    
                    
                     $query=$pdo->prepare('DELETE FROM persona_discapacidad WHERE id_persona=:id_persona');
                     //Recorrer el arreglo de ofertas academicas
                     $query->execute(['id_persona'=>$datos['id_persona']]);

                     if(!empty($datos['id_discapacidad']) && !empty($datos['codigo_conais'])){ 
                      $query4=$pdo->prepare('INSERT INTO persona_discapacidad(id_persona,id_discapacidad,codigo_conapdis,observacion) VALUES
                      (:id_persona, :id_discapacidad,:codigo_conapdis,:observacion)');  
                      $query4->execute(['id_persona'=>$datos['id_persona'], 'id_discapacidad'=>$datos['id_discapacidad'], 'codigo_conapdis'=>$datos['codigo_conais'], 'observacion'=>$datos['observaciones']]);
                    
                    }
              
                      //TABLE persona_discapacidad AGREGANDO UNA SEGUNDA DISCAPACIDAD     
                     // var_dump($datos['id_persona_discapacidad1'],$datos['id_discapacidad1'],$datos['codigo_conais']);
                      if(!empty($datos['id_discapacidad1']) && !empty($datos['codigo_conais'])){ 
                      
                        $query4=$pdo->prepare('INSERT INTO persona_discapacidad(id_persona,id_discapacidad,codigo_conapdis,observacion) VALUES
                        (:id_persona, :id_discapacidad,:codigo_conapdis,:observacion)');  
                        $query4->execute(['id_persona'=>$datos['id_persona'], 'id_discapacidad'=>$datos['id_discapacidad1'], 'codigo_conapdis'=>$datos['codigo_conais'], 'observacion'=>$datos['observaciones1']]);
                      }
                      
                      //TABLE usuario
                      //se encripta la contraseña
                      $crypt= new SED();
                   //  var_dump($datos['password']);
                      $clave=$crypt->encryption($datos['password']);
                     // var_dump($clave);
                      $query4=$pdo->prepare('UPDATE usuario  SET usuario=:usuario,password=:password,estatus=:estatus,id_perfil=:id_perfil WHERE id_persona=:id_persona');  
                      $query4->execute(['usuario'=>'ubv'.$datos['numero_documento'],'password'=>$clave,'estatus'=>$datos['estatus'], 'id_perfil'=>$datos['id_perfil'], 'id_persona'=>$datos['id_persona']]);
                    
                    //Toma el id de persona
                    $query2 = $pdo->prepare("SELECT id_usuario  FROM usuario WHERE id_persona=:id_persona");
                    $query2->execute(['id_persona'=>$datos['id_persona']]);
                    $usuario = $query2->fetch();
                    $usuario['id_usuario'];
                     
                      //tabla rol
                     //Eliminamos los roles actules del usuario para agregarlos desde cero ya que si el usurio tiene uno y se pretende agregar otro con un update no bastara.
                      $query=$pdo->prepare('DELETE FROM usuario_rol WHERE id_usuario=:id_usuario');
                              //Recorrer el arreglo de ofertas academicas
                      $query->execute(['id_usuario'=>$usuario['id_usuario']]);

                      
                      $roles=$datos['id_rol'];
                      $query=$pdo->prepare('INSERT INTO usuario_rol(id_usuario, id_rol) VALUES (:id_usuario, :id_rol)');
                                      //Recorrer el arreglo de ofertas academicas
                                        for ($i=0;$i<count($roles);$i++)    
                                        {       
                      $query->execute(['id_usuario'=>$usuario['id_usuario'],'id_rol'=>$roles[$i]]);
                                        } 
                   //4. consignas la transaccion (en caso de que no suceda ningun fallo)
                   $pdo->commit();                    
                   return true;
              
                  }catch(PDOException $e){
                    //5. regresas a un estado anterior en caso de error
                    $pdo->rollBack();
                    return false;

                 }
            
                }

                








                public function getEjeRegional(){
                  $items=[];
                  try{
                      $ejes_regionales=$this->db->connect()->query('SELECT * FROM eje_regional');
                      //$ejes_regionales->execute(['estatus'=>'true']);
                      while($row=$ejes_regionales->fetch()){
      
                          $item=new Estructura();
      
                          $item->id_eje_regional=$row['id_eje_regional'];
                          $item->descripcion=$row['descripcion'];
                          $item->codigo=$row['codigo'];
                          $item->estatus=$row['estatus'];          
                          
                          array_push($items,$item);
                      }
                      return $items;
                
                }catch(PDOException $e){
                  return[];
                }
                
              }



              //obtener centros de estudios activos
        public function getCentro(){
          $items=[];
          try{
              $query=$this->db->connect()->prepare("SELECT 
              id_centro_estudio as id_centro,
              centro_estudio.codigo,
              centro_estudio.descripcion,
              centro_estudio.estatus,
              area_conocimiento_opsu.descripcion as descripcion_opsu,
              area_conocimiento_opsu.id_area_conocimiento_opsu as id_area_opsu
              FROM 
              centro_estudio, 
              area_conocimiento_opsu
              WHERE centro_estudio.id_area_conocimiento_opsu=area_conocimiento_opsu.id_area_conocimiento_opsu
              AND centro_estudio.estatus=:estatus;");
              $query->execute(['estatus'=>true]);
              while($row=$query->fetch()){
                  
                  $item=new Estructura();
                  $item->id_centro_estudio=$row['id_centro'];
                  $item->codigo=$row['codigo'];
                  $item->descripcion=$row['descripcion'];
                 $item->id_area_conocimiento_opsu=$row['id_area_opsu'];
                  if($row['estatus'] == 1){
                      $item->estatus="Activo";
                  }elseif($row['estatus'] == 0){
                      $item->estatus="Inactivo";
                  }
                  $item->descripcion_opsu=$row['descripcion_opsu'];
                  
                  
                 
              
                  array_push($items,$item);
              
              
              }
              return $items;
              
          }catch(PDOException $e){
          return[];
          }
        
      }


       //obtener programas de formacion activos
       public function getPrograma(){
        $items=[];
        try{
            $query=$this->db->connect()->prepare("SELECT 
            id_programa as id_programa,
            programa.descripcion as programa,
            programa.codigo as codigo,
            programa.id_centro_estudio as id_centro,
            centro_estudio.descripcion as centro,
            programa.id_programa_tipo as id_tipo,
            programa_tipo.descripcion as tipo,
            programa.id_nivel_academico as id_nivel,
            nivel_academico.descripcion as nivel,
            programa.id_linea_investigacion as id_linea,
            linea_investigacion.descripcion as linea,
            programa.estatus as estatus
            FROM programa, centro_estudio, programa_tipo, nivel_academico, linea_investigacion
            WHERE programa.id_centro_estudio=centro_estudio.id_centro_estudio
            and programa.id_programa_tipo=programa_tipo.id_programa_tipo
            and programa.id_nivel_academico=nivel_academico.id_nivel_academico
            and programa.id_linea_investigacion=linea_investigacion.id_linea_investigacion
            and programa.estatus=:estatus;");
            $query->execute(['estatus'=>'1']);

            while($row=$query->fetch()){
    
                $item=new Estructura();
                $item->id_programa=$row['id_programa'];
                $item->descripcion=$row['programa'];
                $item->codigo=$row['codigo'];
                $item->id_centro_estudio=$row['id_centro'];
                $item->descripcion_centro=$row['centro'];
                $item->id_programa_tipo=$row['id_tipo'];
                $item->descripcion_tipo=$row['tipo'];
                $item->id_nivel_academico=$row['id_nivel'];
                $item->descripcion_nivel=$row['nivel'];
                $item->id_linea_investigacion=$row['id_linea'];
                $item->descripcion_linea=$row['linea'];
                if($row['estatus'] == 1){
                    $item->estatus="Activo";
                }elseif($row['estatus'] == 0){
                    $item->estatus="Inactivo";
                }
    
                array_push($items,$item);
    
            }
            return $items;
    
        }catch(PDOException $e){
        return[];
        }
    
    }


    //obtener lista de preguntas
    public function getPregunta(){
      $items=[];
      try{
          $preguntas=$this->db->connect()->query("SELECT * FROM pregunta");
    
          while($row=$preguntas->fetch(PDO::FETCH_ASSOC)){

              $item=new Estructura();

              $item->id_pregunta=$row['id_pregunta'];
              $item->descripcion=$row['descripcion'];
              array_push($items,$item);
          
          
          }
          return $items;
    
    }catch(PDOException $e){
    return[];
    }
    
  }

      public function getEstadosbyPais($id_pais){
        $items=[];
        try{
          $query=$this->db->connect()->prepare("SELECT * FROM estado WHERE id_pais = :id_pais;");
          $query->execute(['id_pais'=>$id_pais]);
          while($row=$query->fetch()){
            $item=new Estructura();
            $item->id_estado=$row['id_estado'];
            $item->descripcion=$row['descripcion'];

            array_push($items,$item);
            
          
          }
          return $items;
          
        }catch(PDOException $e){
          return[];
        }
        
      }

      public function getMunicipiosbyEstado($id_estado){
        $items=[];
        try{
          $query=$this->db->connect()->prepare("SELECT * FROM municipio WHERE id_estado=:id_estado;");
          $query->execute(['id_estado'=>$id_estado]);
          while($row=$query->fetch()){
            $item=new Estructura();
            $item->id_municipio=$row['id_municipio'];
            $item->descripcion=$row['descripcion'];

            array_push($items,$item);
            
          
          }
          return $items;
          
        }catch(PDOException $e){
          return[];
        }
        
      }

      public function getParroquiasbyMunicipio($id_municipio){
        $items=[];
        try{
          $query=$this->db->connect()->prepare("SELECT * FROM parroquia WHERE id_municipio=:id_municipio;");
          $query->execute(['id_municipio'=>$id_municipio]);
          while($row=$query->fetch()){
            $item=new Estructura();
            $item->id_parroquia=$row['id_parroquia'];
            $item->descripcion=$row['descripcion'];

            array_push($items,$item);
            
          
          }
          return $items;
          
        }catch(PDOException $e){
          return[];
        }
        
      }
      //combodependiente de discapacidades
      public function getDiscapacidadesbyTipo($id_tipo_discapacidad){
        $items=[];
        try{
          $query=$this->db->connect()->prepare("SELECT * FROM discapacidad WHERE id_tipo_discapacidad=:id_tipo_discapacidad");
          $query->execute(['id_tipo_discapacidad'=>$id_tipo_discapacidad]);
          while($row=$query->fetch()){
            $item=new Estructura();
            $item->id_discapacidad=$row['id_discapacidad'];
            $item->descripcion=$row['descripcion'];

            array_push($items,$item);
            
          
          }
          return $items;
          
        }catch(PDOException $e){
          return[];
        }
        
      }

      public function insertUser($datos){
 
            try{
                //1. guardas el objeto pdo en una variable
                $pdo=$this->db->connect();
                
                 //2. comienzas transaccion
                 $pdo->beginTransaction();
        
                //3. hacer toas las consultas comenzando desde el la variable del paso 1 (ejemplo: $sql=$pdo->prepare(...); $sql=$pdo->query(...);, etc)
        
                //Tabla Persona
                $query1=$pdo->prepare('INSERT INTO persona(primer_nombre, segundo_nombre, primer_apellido,segundo_apellido,fecha_nacimiento,id_estado_civil,id_genero,id_pais) VALUES
                (:primer_nombre, :segundo_nombre,:primer_apellido,:segundo_apellido,:fecha_nacimiento,:id_estado_civil,:id_genero,:id_pais)');
          
                $query1->execute(['primer_nombre'=>$datos['pnombre'], 'segundo_nombre'=>$datos['snombre'], 'primer_apellido'=>$datos['papellido'], 
                'segundo_apellido'=>$datos['sapellido'], 'fecha_nacimiento'=>date("Y/m/d", strtotime($datos['fnac'])), 'id_estado_civil'=>$datos['estadoCivil'],
                 'id_genero'=>$datos['genero'], 'id_pais'=>$datos['pais']]);
            
        
                //Toma el id de persona
                $query2 = $pdo->prepare("SELECT id_persona  FROM persona ORDER BY id_persona DESC LIMIT 1");
                $query2 ->execute();
                $id_persona = $query2->fetch();
                $id_persona['id_persona'];
        
               //Tabla Ednia
               if(!empty($datos['id_etnia'])){ 
                $query3=$pdo->prepare('INSERT INTO persona_etnia(id_persona, id_etnia) VALUES
                (:id_persona, :id_etnia)');  
                $query3->execute(['id_persona'=>$id_persona['id_persona'], 'id_etnia'=>$datos['id_etnia']]);
               }
                //Tabla Persona Nacionalidad
                $query4=$pdo->prepare('INSERT INTO persona_nacionalidad(identificacion,id_nacionalidad,id_documento_identidad_tipo,id_persona) VALUES
                (:identificacion, :id_nacionalidad,:id_documento_identidad_tipo,:id_persona)');  
                $query4->execute(['identificacion'=>$datos['numero_documento'], 'id_nacionalidad'=>$datos['id_nacionalidad'], 'id_documento_identidad_tipo'=>$datos['id_documento_identidad_tipo'], 'id_persona'=>$id_persona['id_persona']]);
        
               //TABLE domicilio_persona
                $query4=$pdo->prepare('INSERT INTO domicilio_persona(id_parroquia,id_persona,domicilio_detalle,id_domicilio_detalle_tipo) VALUES
                (:id_parroquia,:id_persona,:domicilio_detalle,:id_domicilio_detalle_tipo)');  
                $query4->execute(['id_parroquia'=>$datos['id_parroquia'],'id_persona'=>$id_persona['id_persona'],'domicilio_detalle'=>$datos['direccion'], 'id_domicilio_detalle_tipo'=>$datos['tipo_domicilio']]);
        
                
                
                //TABLE persona_correo
              if(!empty($datos['correo_institucional'])){ // 
                  $correo_institucional=$pdo->prepare('INSERT INTO persona_correo(correo,id_persona,id_correo_tipo) VALUES
                  (:correo, :id_persona,:id_correo_tipo)');  
                  $correo_institucional->execute(['correo'=>$datos['correo_institucional'], 'id_persona'=>$id_persona['id_persona'], 'id_correo_tipo'=>'6']);
               
              }
                
                
                //TABLE persona_correo
                $query4=$pdo->prepare('INSERT INTO persona_correo(correo,id_persona,id_correo_tipo) VALUES
                (:correo, :id_persona,:id_correo_tipo)');  
                $query4->execute(['correo'=>$datos['correo_personal'], 'id_persona'=>$id_persona['id_persona'], 'id_correo_tipo'=>'5']);
        

                //tabla fecha ingreso 
                $sql_hijos=$pdo->prepare("INSERT INTO persona_hijo(
                  descripcion,
                  id_persona)
                  VALUES(
                  :numero_hijos,
                  (SELECT id_persona FROM persona ORDER BY id_persona DESC LIMIT 1)
                  );");

                $sql_hijos->execute([
                    'numero_hijos'=>$datos['numero_hijos']
                ]);
                // tabla hijos 
                $sql_fecha_ingreso=$pdo->prepare("INSERT INTO persona_fecha_ingreso(
                    fecha,
                    id_persona)
                    VALUES(
                    :fecha_ingreso,
                    (SELECT id_persona FROM persona ORDER BY id_persona DESC LIMIT 1)
                    );");

                $sql_fecha_ingreso->execute([
                    'fecha_ingreso'=>date("Y/m/d", strtotime($datos['fecha_ingreso']))
                ]);
                if($datos['id_perfil']==1){//si el usuario tiene perfil de trabajador academico
                  
                  $sql_docente=$pdo->prepare("INSERT INTO docente(
                    id_centro_estudio,
                    id_persona,
                    id_eje_municipal,
                    id_docente_dedicacion,
                    id_docente_estatus,
                    tipo_docente,
                    id_clasificacion_docente,
                    id_escalafon
                    ) VALUES(
                    :centro_estudio,
                    (SELECT id_persona FROM persona ORDER BY id_persona DESC LIMIT 1),
                    :id_eje_municipal,
                    :estatus_d,
                    :dedicacion,
                    'SIDTA',
                    :clasificacion,
                    :escalafon
                    );");
        
                  $sql_docente->execute([
                      'centro_estudio'=>$datos['centro_estudio'],
                      'id_eje_municipal'=>$datos['eje_municipal'],
                      'dedicacion'=>$datos['dedicacion'],
                      'estatus_d'=>$datos['estatus_d'],
                      'clasificacion'=>$datos['clasificacion'],
                      'escalafon'=>$datos['escalafon']
                  ]);

                  if(!empty($datos['aldea'])){
                    $existe_aldea=$pdo->prepare("SELECT count(*) from aldea_ubv where descripcion=:descripcion");
                    $existe_aldea->execute(['descripcion'=>$datos['aldea']]);
                    $row1=$existe_aldea->fetch();
        
                    if($row1[0]>0){
                      //insertar 
                      //obtener id de la aldea
                      $aldea_ubv=$pdo->prepare("SELECT * from aldea_ubv where descripcion=:descripcion");
                      $aldea_ubv->execute(['descripcion'=>$datos['aldea']]);
                      $ald=$aldea_ubv->fetch();
                      
                      $aldea_docente=$pdo->prepare("INSERT INTO docente_aldea(id_docente, id_aldea_ubv)
                      VALUES((SELECT id_docente FROM docente ORDER BY id_docente DESC LIMIT 1), :id_aldea_ubv)");
                      $aldea_docente->execute(['id_aldea_ubv'=>$ald['id_aldea_ubv']]);
                    }else{
                      //insertamos en aldea
                      $aldea_ubv=$pdo->prepare("INSERT INTO aldea_ubv (descripcion) 
                      VALUES(:descripcion)");
                      $aldea_ubv->execute(['descripcion'=>$datos['aldea']]);
        
                      //recuperamos el id de la aldea insertada
                      $aldea_ubv=$pdo->prepare("SELECT * from aldea_ubv where descripcion=:descripcion");
                      $aldea_ubv->execute(['descripcion'=>$datos['aldea']]);
                      $ald=$aldea_ubv->fetch();
        
                      //actualizamos el id de la aldea
                      $aldea_docente=$pdo->prepare("INSERT INTO docente_aldea(id_docente, id_aldea_ubv)
                      VALUES((SELECT id_docente FROM docente ORDER BY id_docente DESC LIMIT 1), :id_aldea_ubv)");
                      $aldea_docente->execute(['id_aldea_ubv'=>$ald['id_aldea_ubv']]);
                    }
                  }

                  //tabla docente_programa
                  $programas=$datos['programa'];
                  $docente_programa=$pdo->prepare('INSERT INTO docente_programa(id_docente, id_programa) 
                  VALUES ((SELECT id_docente FROM docente ORDER BY id_docente DESC LIMIT 1), :id_programa)');
                  for ($i=0;$i<count($programas);$i++)    
                  {       
                    $docente_programa->execute(['id_programa'=>$programas[$i]]);
                  } 
                }


                //TABLE persona_telefono
                $query4=$pdo->prepare('INSERT INTO persona_telefono(telefono,id_telefono_codigo_area,id_persona,id_telefono_tipo) VALUES
                (:telefono, :id_telefono_codigo_area,:id_persona,:id_telefono_tipo)');  
                $query4->execute(['telefono'=>$datos['telefono_numero'], 'id_telefono_codigo_area'=>$datos['id_telefono_codigo_area'], 'id_persona'=>$id_persona['id_persona'], 'id_telefono_tipo'=>$datos['id_telefono_tipo']]);
        
                //TABLE persona_discapacidad
                if(!empty($datos['id_discapacidad'])){ 
                $query4=$pdo->prepare('INSERT INTO persona_discapacidad(id_persona,id_discapacidad,codigo_conapdis,observacion) VALUES
                (:id_persona, :id_discapacidad,:codigo_conapdis,:observacion)');  
                $query4->execute(['id_persona'=>$id_persona['id_persona'], 'id_discapacidad'=>$datos['id_discapacidad'], 'codigo_conapdis'=>$datos['codigo_conais'], 'observacion'=>$datos['observaciones']]);
                }
        
                //TABLE persona_discapacidad AGREGANDO UNA SEGUNDA DISCAPACIDAD     
                if(!empty($datos['id_discapacidad1'])){ 
                    $query4=$pdo->prepare('INSERT INTO persona_discapacidad(id_persona,id_discapacidad,codigo_conapdis,observacion) VALUES
                    (:id_persona, :id_discapacidad,:codigo_conapdis,:observacion)');  
                    $query4->execute(['id_persona'=>$id_persona['id_persona'], 'id_discapacidad'=>$datos['id_discapacidad1'], 'codigo_conapdis'=>$datos['codigo_conais'], 'observacion'=>$datos['observaciones1']]);
                    }
        
        
        
                //TABLE usuario
                //se encripta la contraseña
                $crypt= new SED();
                $clave=$crypt->encryption($datos['numero_documento']);
        
                $query4=$pdo->prepare('INSERT INTO usuario(id_persona,usuario,password,fecha,estatus,id_perfil) VALUES
                (:id_persona, :usuario,:password,:fecha,:estatus,:id_perfil)');  
                $query4->execute(['id_persona'=>$id_persona['id_persona'], 'usuario'=>'ubv'.$datos['numero_documento'],
                'password'=>$clave, 'fecha'=>date('Y-m-d H:i:s'), 'estatus'=>$datos['estatus'], 'id_perfil'=>$datos['id_perfil']]);
             
                $sql_pregunta=$pdo->prepare("INSERT INTO usuario_pregunta(
                  id_pregunta,
                  respuesta,
                  id_usuario)
                  VALUES(
                  :pregunta1,
                  :respuesta1,
                  (SELECT id_usuario FROM usuario ORDER BY id_usuario DESC LIMIT 1)
                  ), (:pregunta2,
                  :respuesta2,
                  (SELECT id_usuario FROM usuario ORDER BY id_usuario DESC LIMIT 1)
                  );");
        
              $sql_pregunta->execute([
                  'pregunta1'=>$datos['pregunta1'],
                  'respuesta1'=>$datos['respuesta1'],
                  'pregunta2'=>$datos['pregunta2'],
                  'respuesta2'=>$datos['respuesta2']
              ]);
              
                //Toma el id de usuario
                $query2 = $pdo->prepare("SELECT id_usuario  FROM usuario ORDER BY id_usuario DESC LIMIT 1");
                $query2 ->execute();
                $usuario = $query2->fetch();
                $usuario['id_usuario'];
        
                    //tabla rol
                    $roles=$datos['id_rol'];
        
                  // var_dump( $usuario['id_usuario'],$roles);
                    $query=$pdo->prepare('INSERT INTO usuario_rol(id_usuario, id_rol) VALUES (:id_usuario, :id_rol)');
                          //Recorrer el arreglo de ofertas academicas
                        for ($i=0;$i<count($roles);$i++)    
                        {       
                    $query->execute(['id_usuario'=>$usuario['id_usuario'],'id_rol'=>$roles[$i] ]);
                        } 
        
        
        
                  //4. consignas la transaccion (en caso de que no suceda ningun fallo)
                  $pdo->commit();        
                  return true;
        
              
            }catch(PDOException $e){
             //5. regresas a un estado anterior en caso de error
             $pdo->rollBack();
             return false;
                     }
            
                    }



        //obtener los datos de SIGAD para registrar al docente
        public function getbyCedula($cedula){

          try{
              $query=$this->db->connect()->prepare("SELECT * FROM nomina_docente_sigad WHERE cedula=:cedula");
              $query->execute(['cedula'=>$cedula]);

              $item=new Estructura();
              while($row=$query->fetch()){

                  $item->id_nomina_docente_sigad=$row['id_nomina_docente_sigad'];
                  $item->cedula=$row['cedula'];
                  
                  $item->nombres=$row['nombres'];

                  $item->nacionalidad=$row['nacionalidad'];
                  $item->fecha_nacimiento=$row['fecha_nacimiento'];
                  $item->pais_nacimiento=$row['pais_nacimiento'];
                  $item->estado_nacimiento=$row['estado_nacimiento'];
                  $item->genero=$row['genero'];
                  $item->estado_civil=$row['estado_civil'];
                  $item->telefono=$row['telefono'];
                  $item->correo_electronico=$row['correo_electronico'];
                  $item->direccion=$row['direccion'];
                  $item->numero_hijos=$row['numero_hijos'];
                  $item->cargo=$row['cargo'];
                  $item->clasificacion_docente=$row['clasificacion_docente'];
                  $item->fecha_ingreso=$row['fecha_ingreso'];
                  $item->dedicacion=$row['dedicacion'];
                  $item->estatus=$row['estatus'];

            }
            if(empty($item->cedula)){
              return false;
            }
            return $item;
              
              

          } catch(PDOException $e){
              return false;
          }

      }

      public function getID($id, $tipo){
        try{
          switch($tipo){
            case 1://estado civil
            $query=$this->db->connect()->prepare("SELECT id_estado_civil FROM estado_civil where descripcion=:estado_civil;");
            $query->execute(['estado_civil'=>$id]);
            $row=$query->fetch();
            return $row['id_estado_civil'];
            break;
            case 2://genero
            $query=$this->db->connect()->prepare("SELECT id_genero FROM genero where descripcion=:genero");
            $query->execute(['genero'=>$id]);
            $row=$query->fetch();
            return $row['id_genero'];
            break;
            case 3://tipo de documento de identidad
            $query=$this->db->connect()->prepare("SELECT id_documento_identidad_tipo FROM documento_identidad_tipo WHERE descripcion=:nacionalidad");
            $query->execute(['nacionalidad'=>$id]);
            $row=$query->fetch();
            return $row['id_documento_identidad_tipo'];
            break;
            case 4://nacionalidad
            $query=$this->db->connect()->prepare("SELECT id_nacionalidad FROM nacionalidad, pais WHERE nacionalidad.id_pais=pais.id_pais AND pais.descripcion=:pais");
            $query->execute(['pais'=>$id]);
            $row=$query->fetch();
            return $row['id_nacionalidad'];
            break;
            case 5://pais de nacimiento
            $query=$this->db->connect()->prepare("SELECT id_pais FROM pais where descripcion=:pais");
            $query->execute(['pais'=>$id]);
            $row=$query->fetch();
            return $row['id_pais'];
            break;
            case 6://codigo de area
            $query=$this->db->connect()->prepare("SELECT id_telefono_codigo_area FROM telefono_codigo_area where descripcion=:cod_area");
            $query->execute(['cod_area'=>$id]);
            $row=$query->fetch();
            return $row['id_telefono_codigo_area'];
            break;
            case 7://tipo de correo
            $query=$this->db->connect()->prepare("SELECT id_correo_tipo FROM correo_tipo WHERE descripcion=:correo_tipo");
            $query->execute(['correo_tipo'=>$id]);
            $row=$query->fetch();
            return $row['id_correo_tipo'];
            break;
            case 8://dedicacion docente
            $query=$this->db->connect()->prepare("SELECT id_docente_dedicacion FROM docente_dedicacion WHERE descripcion=:dedicacion");
            $query->execute(['dedicacion'=>$id]);
            $row=$query->fetch();
            return $row['id_docente_dedicacion'];
            break;
            case 9://estatus docente
            $query=$this->db->connect()->prepare("SELECT id_docente_estatus FROM docente_estatus WHERE descripcion=:estatus");
            $query->execute(['estatus'=>$id]);
            $row=$query->fetch();
            return $row['id_docente_estatus'];
            break;
            case 10://clasificacion docente
            $query=$this->db->connect()->prepare("SELECT id_clasificacion_docente FROM clasificacion_docente WHERE descripcion=:clasificacion_docente");
            $query->execute(['clasificacion_docente'=>$id]);
            $row=$query->fetch();
            return $row['id_clasificacion_docente'];
            break;
            case 11://escalafon docente
            $query=$this->db->connect()->prepare("SELECT id_escalafon FROM escalafon WHERE descripcion=:escalafon");
            $query->execute(['escalafon'=>$id]);
            $row=$query->fetch();
            return $row['id_escalafon'];
            break;


          }
          
        } catch(PDOException $e){
          return false;
        }
      }

//MODIFICADO EL 01/04/2020: NUEVO update de usuarios

public function updateUser($datos){
   
  try{
    
       //1. guardas el objeto pdo en una variable
      $pdo=$this->db->connect();
      
      //2. comienzas transaccion
      $pdo->beginTransaction();

       //3. hacer toas las consultas comenzando desde el la variable del paso 1 (ejemplo: $sql=$pdo->prepare(...); $sql=$pdo->query(...);, etc)

      //Tabla Persona
      //ESTAR PENDIENTE CON FECHA YA QUE SI SE DEJA LA MISMA ESTA YA VIENE FORMATEADA Y SE VUELVE A FORMATEAR Y SE PIERDE ..
        $query1=$pdo->prepare('UPDATE  persona SET primer_nombre=:primer_nombre, segundo_nombre=:segundo_nombre, primer_apellido=:primer_apellido,
        segundo_apellido=:segundo_apellido,fecha_nacimiento=:fecha_nacimiento,id_estado_civil=:id_estado_civil,id_genero=:id_genero,id_pais=:id_pais WHERE id_persona=:id_persona');
  
        $query1->execute([
          'primer_nombre'=>$datos['pnombre'], 
          'segundo_nombre'=>$datos['snombre'], 
          'primer_apellido'=>$datos['papellido'], 
        'segundo_apellido'=>$datos['sapellido'], 
        'fecha_nacimiento'=>date("Y/m/d", strtotime($datos['fnac'])), 
        'id_estado_civil'=>$datos['estadoCivil'],
         'id_genero'=>$datos['genero'], 
         'id_pais'=>$datos['npais'], 
         'id_persona'=>$datos['id_persona']]);


       //Tabla Ednia
       $query=$pdo->prepare('DELETE FROM persona_etnia WHERE id_persona=:id_persona');
       //Recorrer el arreglo de ofertas academicas
       $query->execute(['id_persona'=>$datos['id_persona']]);

       if(!empty($datos['id_etnia'])){ 

      $query3=$pdo->prepare('INSERT INTO persona_etnia(id_persona, id_etnia) VALUES
      (:id_persona, :id_etnia)');  
      $query3->execute(['id_persona'=>$datos['id_persona'], 'id_etnia'=>$datos['id_etnia']]);
      }
                      
        //Tabla Persona Nacionalidad
        $query4=$pdo->prepare('UPDATE persona_nacionalidad SET 
        identificacion=:identificacion,
        id_nacionalidad=:id_nacionalidad,
        id_documento_identidad_tipo=:id_documento_identidad_tipo 
        WHERE id_persona=:id_persona');  
        
        $query4->execute(['identificacion'=>$datos['numero_documento'], 
        'id_nacionalidad'=>$datos['id_nacionalidad'],
       'id_documento_identidad_tipo'=>$datos['id_documento_identidad_tipo'], 
       'id_persona'=>$datos['id_persona']]);

       
      // var_dump($datos['id_parroquia'],$datos['direccion'],$datos['tipo_domicilio'],$datos['id_persona']);
       //TABLE domicilio_persona
       //REVISAR ID_PARROQUIA 
        $query4=$pdo->prepare('UPDATE domicilio_persona SET 
        id_parroquia=:id_parroquia,
        domicilio_detalle=:domicilio_detalle,
        id_domicilio_detalle_tipo=:id_domicilio_detalle_tipo 
        WHERE domicilio_persona.id_persona=:id_persona ');  
        
        $query4->execute([
          'id_parroquia'=>$datos['id_parroquia'],
          'domicilio_detalle'=>$datos['direccion'],
          'id_domicilio_detalle_tipo'=>$datos['tipo_domicilio'],
          'id_persona'=>$datos['id_persona']]);
      
       
        //TABLE persona_correo
        if(!empty($datos['correo_institucional'])){
          $existe=$pdo->prepare("SELECT count(*) from persona_correo where correo=:correo");
          $existe->execute(['correo'=>$datos['correo_institucional']]);
          $row=$existe->fetch();
          
          if($row[0]>0){
            $correo_institucional=$pdo->prepare('UPDATE persona_correo SET correo=:correo WHERE id_persona_correo=:id_persona_correo');  
            $correo_institucional->execute([
              'correo'=>$datos['correo_institucional'],
              'id_persona_correo'=>$datos['id_persona_institucional']
              ]); 
          }else{
            $correo_institucional=$pdo->prepare('INSERT INTO persona_correo(correo,id_persona,id_correo_tipo) VALUES
            (:correo, :id_persona,:id_correo_tipo)');  
            $correo_institucional->execute(['correo'=>$datos['correo_institucional'], 'id_persona'=>$datos['id_persona'], 'id_correo_tipo'=>6]);
          }
        }
        $correo_personal=$pdo->prepare('UPDATE persona_correo SET correo=:correo WHERE id_persona_correo=:id_persona_correo');  
        $correo_personal->execute([
          'correo'=>$datos['correo'],
          'id_persona_correo'=>$datos['id_persona_correo']
          ]);

        //TABLE persona_telefono
        $existe_tlf=$pdo->prepare("SELECT count(*) from persona_telefono where id_persona=:id_persona");
        $existe_tlf->execute(['id_persona'=>$datos['id_persona']]);
        $row_tlf=$existe_tlf->fetch();
        if($row_tlf[0]>0){
          $query4=$pdo->prepare('UPDATE persona_telefono SET telefono=:telefono,id_telefono_codigo_area=:id_telefono_codigo_area
          ,id_telefono_tipo=:id_telefono_tipo WHERE id_persona=:id_persona');  
          $query4->execute(['telefono'=>$datos['telefono_numero'], 'id_telefono_codigo_area'=>$datos['id_telefono_codigo_area'], 'id_telefono_tipo'=>$datos['id_telefono_tipo'], 'id_persona'=>$datos['id_persona']]);
        }else{
          $query4=$pdo->prepare('INSERT INTO persona_telefono(telefono,id_telefono_codigo_area,id_persona,id_telefono_tipo) VALUES
          (:telefono, :id_telefono_codigo_area,:id_persona,:id_telefono_tipo)');  
          $query4->execute(['telefono'=>$datos['telefono_numero'], 'id_telefono_codigo_area'=>$datos['id_telefono_codigo_area'], 'id_persona'=>$datos['id_persona'], 'id_telefono_tipo'=>$datos['id_telefono_tipo']]);
        }

        //TABLE persona_discapacidad
       //var_dump($datos['id_persona_discapacidad'],$datos['id_discapacidad'],$datos['codigo_conais']);
        
      //TABLE persona_hijo
      $hijos=$pdo->prepare('UPDATE persona_hijo SET descripcion=:descripcion  WHERE id_persona=:id_persona');  
      $hijos->execute(['descripcion'=>$datos['nhijos'],'id_persona'=>$datos['id_persona']]);
      
      //TABLE persona_fecha_ingreso
      $fingreso=$pdo->prepare('UPDATE persona_fecha_ingreso SET fecha=:fecha  WHERE id_persona=:id_persona');  
      $fingreso->execute(['fecha'=>date("Y/m/d", strtotime($datos['fingreso'])),'id_persona'=>$datos['id_persona']]);
      
       $query=$pdo->prepare('DELETE FROM persona_discapacidad WHERE id_persona=:id_persona');
       //Recorrer el arreglo de ofertas academicas
       $query->execute(['id_persona'=>$datos['id_persona']]);

       if(!empty($datos['id_discapacidad'])){ 
        $query4=$pdo->prepare('INSERT INTO persona_discapacidad(id_persona,id_discapacidad,codigo_conapdis,observacion) VALUES
        (:id_persona, :id_discapacidad,:codigo_conapdis,:observacion)');  
        $query4->execute(['id_persona'=>$datos['id_persona'], 'id_discapacidad'=>$datos['id_discapacidad'], 'codigo_conapdis'=>$datos['codigo_conais'], 'observacion'=>$datos['observaciones']]);
      
      }

        //TABLE persona_discapacidad AGREGANDO UNA SEGUNDA DISCAPACIDAD     
       // var_dump($datos['id_persona_discapacidad1'],$datos['id_discapacidad1'],$datos['codigo_conais']);
        if(!empty($datos['id_discapacidad1'])){ 
        
          $query4=$pdo->prepare('INSERT INTO persona_discapacidad(id_persona,id_discapacidad,codigo_conapdis,observacion) VALUES
          (:id_persona, :id_discapacidad,:codigo_conapdis,:observacion)');  
          $query4->execute(['id_persona'=>$datos['id_persona'], 'id_discapacidad'=>$datos['id_discapacidad1'], 'codigo_conapdis'=>$datos['codigo_conais'], 'observacion'=>$datos['observaciones1']]);
        }
        
        //TABLE usuario
        //se encripta la contraseña
        $crypt= new SED();
     //  var_dump($datos['password']);
        $clave=$crypt->encryption($datos['password']);
       // var_dump($clave);
        $query4=$pdo->prepare('UPDATE usuario  SET usuario=:usuario,password=:password,estatus=:estatus,id_perfil=:id_perfil WHERE id_persona=:id_persona');  
        $query4->execute(['usuario'=>'ubv'.$datos['numero_documento'],'password'=>$clave,'estatus'=>$datos['estatus'], 'id_perfil'=>$datos['id_perfil'], 'id_persona'=>$datos['id_persona']]);
      


        /*
        13/02/21
        REVISAR DEDICACION YA QUE SE LE ESTA PASANDO UN STRING EN LUGAR DE UN ENTERO 
        ADEMAS DE QUE EL CAMPO ES TIPO TEXT  READONLY Y NO SELECT. 

        NOTA: EN CASO DE QUE UN USUARIO COMUL QUIERA CAMBIAR SU PERFIL A TRABAJADOR ACADEMICO
        ESTE NO PODRA PUES EL MISMO CANPO DE DEDICACION NO LO DEJARA CULMINAR EL FORM
        */
        if($datos['id_perfil']==1){//1=perfil de trabajador academico
          $docente=$pdo->prepare('UPDATE docente  SET
          id_centro_estudio=:id_centro_estudio,
          id_eje_municipal=:id_eje_municipal,
          id_docente_dedicacion=:id_docente_dedicacion, 
          id_docente_estatus=:id_docente_estatus,
          id_clasificacion_docente=:id_clasificacion_docente, 
          id_escalafon=:id_escalafon 
          WHERE id_persona=:id_persona;');  
          $docente->execute([
              'id_centro_estudio'=>$datos['centro_estudio'],
              'id_eje_municipal'=>$datos['eje_municipal'],
              'id_docente_dedicacion'=>$datos['dedicacion'], 
              'id_docente_estatus'=>$datos['estatus_d'], 
              'id_clasificacion_docente'=>$datos['clasificacion'], 
              'id_escalafon'=>$datos['escalafon'],
              'id_persona'=>$datos['id_persona']
              ]);
          if(!empty($datos['aldea'])){
            $existe_aldea=$pdo->prepare("SELECT count(*) from aldea_ubv where descripcion=:descripcion");
            $existe_aldea->execute(['descripcion'=>$datos['aldea']]);
            $row1=$existe_aldea->fetch();

            if($row1[0]>0){
              //actualizamos 
              //obtener id de la aldea
              $aldea_ubv=$pdo->prepare("SELECT * from aldea_ubv where descripcion=:descripcion");
              $aldea_ubv->execute(['descripcion'=>$datos['aldea']]);
              $ald=$aldea_ubv->fetch();
              //evaluar si el docente tiene un registro en docente aldea
              $existe_docente_aldea=$pdo->prepare("SELECT count(*) from docente_aldea where id_docente=:id_docente");
              $existe_docente_aldea->execute(['id_docente'=>$datos['id_docente']]);
              $doc=$existe_docente_aldea->fetch();
              if($doc[0]>0){
                $aldea_docente=$pdo->prepare("UPDATE docente_aldea SET id_aldea_ubv=:id_aldea_ubv WHERE id_docente=:id_docente");
                $aldea_docente->execute(['id_aldea_ubv'=>$ald['id_aldea_ubv'], 'id_docente'=>$datos['id_docente']]);  
              }else{
                $aldea_docente=$pdo->prepare("INSERT INTO docente_aldea(id_docente, id_aldea_ubv)
                VALUES(:id_docente, :id_aldea_ubv)");
                $aldea_docente->execute(['id_aldea_ubv'=>$ald['id_aldea_ubv'], 'id_docente'=>$datos['id_docente']]); 
              }
            }else{
              //insertamos en aldea
              $aldea_ubv=$pdo->prepare("INSERT INTO aldea_ubv (descripcion) 
              VALUES(:descripcion)");
              $aldea_ubv->execute(['descripcion'=>$datos['aldea']]);

              //recuperamos el id de la aldea insertada
              $aldea_ubv=$pdo->prepare("SELECT * from aldea_ubv where descripcion=:descripcion");
              $aldea_ubv->execute(['descripcion'=>$datos['aldea']]);
              $ald=$aldea_ubv->fetch();

              //actualizamos el id de la aldea
              //evaluar si el docente tiene un registro en docente aldea
              $existe_docente_aldea=$pdo->prepare("SELECT count(*) from docente_aldea where id_docente=:id_docente");
              $existe_docente_aldea->execute(['id_docente'=>$datos['id_docente']]);
              $doc=$existe_docente_aldea->fetch();
              if($doc[0]>0){
                $aldea_docente=$pdo->prepare("UPDATE docente_aldea SET id_aldea_ubv=:id_aldea_ubv WHERE id_docente=:id_docente");
                $aldea_docente->execute(['id_aldea_ubv'=>$ald['id_aldea_ubv'], 'id_docente'=>$datos['id_docente']]);
              }else{
                $aldea_docente=$pdo->prepare("INSERT INTO docente_aldea(id_docente, id_aldea_ubv)
                VALUES(:id_docente, :id_aldea_ubv)");
                $aldea_docente->execute(['id_aldea_ubv'=>$ald['id_aldea_ubv'], 'id_docente'=>$datos['id_docente']]); 
              }
            }
          }//end-aldea

          //actualizar docente_programa
          $delete_dp=$pdo->prepare('DELETE FROM docente_programa WHERE id_docente=:id_docente');
          $delete_dp->execute(['id_docente'=>$datos['id_docente']]);
          
          $programas=$datos['programa'];
          
          $docente_programa=$pdo->prepare('INSERT INTO docente_programa(id_docente, id_programa) 
          VALUES (:id_docente, :id_programa)');
          for ($i=0;$i<count($programas);$i++)    
          {       
            $docente_programa->execute([
              'id_docente'=>$datos['id_docente'],
              'id_programa'=>$programas[$i]
              ]);
          } 
        }//end-docente
      //Toma el id de persona
      $query2 = $pdo->prepare("SELECT id_usuario  FROM usuario WHERE id_persona=:id_persona");
      $query2->execute(['id_persona'=>$datos['id_persona']]);
      $usuario = $query2->fetch();
      $usuario['id_usuario'];
       
        //tabla rol
       //Eliminamos los roles actules del usuario para agregarlos desde cero ya que si el usurio tiene uno y se pretende agregar otro con un update no bastara.
        $query=$pdo->prepare('DELETE FROM usuario_rol WHERE id_usuario=:id_usuario');
                //Recorrer el arreglo de ofertas academicas
        $query->execute(['id_usuario'=>$usuario['id_usuario']]);

        
        $roles=$datos['id_rol'];
        $query=$pdo->prepare('INSERT INTO usuario_rol(id_usuario, id_rol) VALUES (:id_usuario, :id_rol)');
                        //Recorrer el arreglo de ofertas academicas
                          for ($i=0;$i<count($roles);$i++)    
                          {       
        $query->execute(['id_usuario'=>$usuario['id_usuario'],'id_rol'=>$roles[$i]]);
                          } 
     //4. consignas la transaccion (en caso de que no suceda ningun fallo)
     $pdo->commit();                    
     return true;

    }catch(PDOException $e){
      //5. regresas a un estado anterior en caso de error
      $pdo->rollBack();
      return false;

   }

  }


  public function getbyIdDisc($id_persona_discapacidad){

    try{
        $query=$this->db->connect()->prepare("SELECT persona.id_persona,persona_discapacidad.id_persona_discapacidad,discapacidad.id_discapacidad,codigo_conapdis,observacion,discapacidad.descripcion AS discapacidad,tipo_discapacidad.id_tipo_discapacidad,tipo_discapacidad.descripcion AS tipo_discapacidad  
        FROM persona
        FULL JOIN persona_discapacidad  ON persona.id_persona=persona_discapacidad.id_persona
        FULL JOIN discapacidad  ON discapacidad.id_discapacidad=persona_discapacidad.id_discapacidad
        FULL JOIN tipo_discapacidad  ON discapacidad.id_tipo_discapacidad=tipo_discapacidad.id_tipo_discapacidad
        WHERE persona_discapacidad.id_persona_discapacidad=:id_persona_discapacidad");
        $query->execute(['id_persona_discapacidad'=>$id_persona_discapacidad]);

        $item=new Estructura();
        while($row=$query->fetch()){

          $item->id_persona_discapacidad = $row['id_persona_discapacidad'];
          $item->id_discapacidad = $row['id_discapacidad'];
          $item->codigo_conapdis = $row['codigo_conapdis'];
          $item->observacion = $row['observacion'];
          $item->discapacidad = $row['discapacidad'];
          $item->id_tipo_discapacidad = $row['id_tipo_discapacidad'];
          $item->tipo_discapacidad = $row['tipo_discapacidad'];
      }
      
      return $item;
        
        

    } catch(PDOException $e){
        return false;
    }

  }


    public function getDomicilioPersona($id_persona){

      try{
          $query=$this->db->connect()->prepare("SELECT domicilio_detalle, 
          t1.id_domicilio_detalle_tipo,
          t6.descripcion as tipo_domicilio,
          t1.id_parroquia, 
          t2.descripcion as parroquia,
          t2.id_municipio, 
          t3.descripcion as municipio,
          t3.id_estado, 
          t4.descripcion as estado,
          t4.id_pais, 
          t5.descripcion as pais 
          from domicilio_persona as t1,
          parroquia as t2,
          municipio as t3,
          estado as t4,
          pais as t5,
          domicilio_detalle_tipo as t6
          where t1.id_parroquia=t2.id_parroquia
          and t2.id_municipio=t3.id_municipio
          and t3.id_estado=t4.id_estado
          and t4.id_pais=t5.id_pais
          and t1.id_domicilio_detalle_tipo=t6.id_domicilio_detalle_tipo          
          and id_persona=:id_persona");
          $query->execute(['id_persona'=>$id_persona]);

          $item=new Estructura();
          while($row=$query->fetch()){
            $item->id_domicilio_detalle_tipo = $row['id_domicilio_detalle_tipo'];
            $item->tipo_domicilio = $row['tipo_domicilio'];
            $item->domicilio_detalle = $row['domicilio_detalle'];
            $item->id_parroquia = $row['id_parroquia'];
            $item->parroquia = $row['parroquia'];
            $item->id_municipio = $row['id_municipio'];
            $item->municipio = $row['municipio'];
            $item->id_estado = $row['id_estado'];
            $item->estado = $row['estado'];
            $item->id_pais = $row['id_pais'];
            $item->pais = $row['pais'];

        }
        
        return $item;
          
          

      } catch(PDOException $e){
          return false;
      }

    }

    public function getRolbyID($id_persona, $id_rol){

      try{
          $query=$this->db->connect()->prepare("SELECT usuario_rol.id_usuario, 
          usuario_rol.id_rol, rol.descripcion AS rol 
          FROM rol,usuario_rol,usuario, persona
          WHERE usuario_rol.id_rol=rol.id_rol
          AND usuario_rol.id_usuario=usuario.id_usuario 
          AND usuario.id_persona=persona.id_persona
          AND usuario.id_persona=:id_persona 
          AND usuario_rol.id_rol=:id_rol");
          $query->execute([
            'id_persona'=>$id_persona,
            'id_rol'=>$id_rol
            ]);

          $item=new Estructura();
          while($row=$query->fetch()){
            $item->id_usuario = $row['id_usuario'];
            $item->id_rol = $row['id_rol'];
            $item->rol = $row['rol'];

        }
        
        return $item;
          
          

      } catch(PDOException $e){
          return false;
      }

    }


    public function getEjeMunipalbyER($id_eje_regional){
      $items=[];
      try{
        $query=$this->db->connect()->prepare("SELECT * FROM eje_municipal WHERE id_eje_regional = :id_eje_regional;");
        $query->execute(['id_eje_regional'=>$id_eje_regional]);
        while($row=$query->fetch()){
          $item=new Estructura();
          $item->id_eje_municipal=$row['id_eje_municipal'];
          $item->descripcion=$row['descripcion'];

          array_push($items,$item);
          
        
        }
        return $items;
        
      }catch(PDOException $e){
        return[];
      }
      
    }
  }//fin-class
    ?>