<?php
include_once 'datosacademicos/docente.php';
include_once 'datosacademicos/arte.php';
class Arte_softwareModel extends Model{
    public function __construct(){

        parent::__construct();
    }


    public function existe($artesoftware){
        try{
    
            //var_dump($area_conocimiento_opsu);
            $sql = $this->db->connect()->prepare("SELECT nombre_arte FROM arte_software WHERE nombre_arte=:nombre_arte");
            $sql->execute(['nombre_arte'=>$artesoftware]);
            $nombre=$sql->fetch();
            
            if($artesoftware==$nombre['nombre_arte']){
                
                return $nombre['nombre_arte'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }

    public function insert($datos){
        //echo "<br>insertar datos";
    

        try{
         //var_dump($datos);
            $query=$this->db->connect()->prepare('INSERT INTO arte_software(
                nombre_arte,
                id_docente,
                ano_arte,
                entidad_promotora,
                url_otro,
                estatus_verificacion
                ) VALUES(
                    :nombre_arte,
                    :id_docente,
                    :ano_arte,
                    :entidad_promotora,
                    :url_otro,
                    :estatus_verificacion
                    )');
    
            $query->execute([
                    'nombre_arte'=>$datos['nombre_arte'],
                    'id_docente'=>$datos['id_docente'],
                    'ano_arte'=>$datos['ano_arte'],
                    'entidad_promotora'=>$datos['entidad_promotora'],
                    'url_otro'=>$datos['url_otro'],
                    'estatus_verificacion'=>'Sin Verificar'
                ]);
    
                return true;
          
        }catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                 }
        
        
    }

    
    public function getDocente(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT 
            docente.id_persona, 
            id_docente,
            identificacion, 
            primer_nombre, 
            primer_apellido
            FROM 
            persona, 
            persona_nacionalidad,
            docente
            WHERE docente.id_persona=persona.id_persona 
            AND persona_nacionalidad.id_persona=persona.id_persona");
            
            while($row=$query->fetch()){
                
                $item=new Docente();
                $item->id_docente=$row['id_docente'];
                $item->id_persona=$row['id_persona'];
                $item->identificacion=$row['identificacion'];
                $item->primer_nombre=$row['primer_nombre'];
                $item->primer_apellido=$row['primer_apellido'];

                array_push($items,$item);
            
            
            }
            //var_dump($items);
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }
      
    public function getArte(){
        $items=[];
        try{
            $query=$this->db->connect()->prepare("SELECT id_arte_software as id_arte,
             arte_software.nombre_arte,
             arte_software.ano_arte,
             arte_software.entidad_promotora,
             arte_software.url_otro,
                docente.id_docente,
                identificacion,
                primer_nombre,
                primer_apellido,
                estatus_verificacion
            FROM persona, persona_nacionalidad, arte_software, docente WHERE arte_software.id_docente=docente.id_docente 
            and docente.id_persona=persona.id_persona 
            and persona_nacionalidad.id_persona=persona.id_persona and identificacion=:identificacion");
            $query->execute(['identificacion'=>$_SESSION['identificacion']]);
            
            while($row=$query->fetch()){
                
                $item=new Arte_software();
                $item->id_arte_software=$row['id_arte'];
                $item->nombre_arte=$row['nombre_arte'];
                $item->ano_arte=$row['ano_arte'];
                $item->entidad_promotora=$row['entidad_promotora'];
                $item->url_otro=$row['url_otro'];
                $item->estatus_verificacion=$row['estatus_verificacion'];
                $item->id_docente=$row['id_docente'];
               
                //$item->estatus=$row['estatus'];
                //$item->descripcion_opsu=$row['descripcion_opsu'];
                
                
                //$item->descripcion_opsu=$row['areaopsu'];
               
            
                array_push($items,$item);
            
            
            }
            //var_dump($items);
            return $items;
            
        }catch(PDOException $e){
        return[];
        }
      
    }

    public function getbyId($id){

            //var_dump($id);

        $items=[];
        
        try{
            $query=$this->db->connect()->prepare("SELECT id_arte_software as id_arte,
             arte_software.nombre_arte,
             arte_software.ano_arte,
             arte_software.entidad_promotora,
             arte_software.url_otro,
                docente.id_docente,
                identificacion,
                primer_nombre,
                primer_apellido,
                estatus_verificacion
            FROM persona, persona_nacionalidad, arte_software, docente WHERE arte_software.id_docente=docente.id_docente 
            and docente.id_persona=persona.id_persona 
            and persona_nacionalidad.id_persona=persona.id_persona and identificacion=:identificacion and id_arte_software=:id_arte_software");
            $query->execute([
                'identificacion'=>$_SESSION['identificacion'],
                'id_arte_software'=>$id
                ]);
            

            $item=new Arte_software();
            while($row=$query->fetch()){
                
                
                $item->id_arte_software=$row['id_arte'];
                $item->nombre_arte=$row['nombre_arte'];
                $item->ano_arte=$row['ano_arte'];
                $item->entidad_promotora=$row['entidad_promotora'];
                $item->url_otro=$row['url_otro'];
                $item->estatus_verificacion=$row['estatus_verificacion'];
                $item->id_docente=$row['id_docente'];
               
                //$item->estatus=$row['estatus'];
                //$item->descripcion_opsu=$row['descripcion_opsu'];
                
                
                //$item->descripcion_opsu=$row['areaopsu'];
               
            
                array_push($items,$item);
            
            
            }


            //var_dump($item);
            return $item;
            
        }catch(PDOException $e){
        return[];
        }
      
    }


    

    public function update($datos){
       //var_dump($datos['nombre_arte2']);
       //var_dump($datos['ano_arte2']);
       //var_dump($datos['entidad_promotora2']);
       //var_dump($datos['url_otro2']);
       //var_dump($datos['id_arte_software']);

        $query=$this->db->connect()->prepare("UPDATE arte_software SET 
        nombre_arte=:nombre_arte,
        ano_arte=:ano_arte,
        entidad_promotora=:entidad_promotora,
        url_otro=:url_otro,
        estatus_verificacion=:estatus_verificacion
       
        WHERE id_arte_software=:id_arte_software");
        
        try{
            $query->execute([
                'id_arte_software'=>$datos['id_arte_software'],

                'nombre_arte'=>$datos['nombre_arte2'],
                'ano_arte'=>$datos['ano_arte2'],
                'entidad_promotora'=>$datos['entidad_promotora2'],
                'url_otro'=>$datos['url_otro2'],
                'estatus_verificacion'=>'Sin Verificar'
               
                
              ]);
              //creo q falta descripcion del area de conocimientoooooooooooooooooooooooooo   
         
         
              return true;

        }catch(PDOException $e){
             return false;
        }
         
    }

        public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM arte_software WHERE id_arte_software = :id_arte_software");
    
            try{
                $query->execute([
                'id_arte_software'=>$id
                ]);
                return true;
            }catch(PDOException $e){
                return false;
            }
        }
    }


    ?>