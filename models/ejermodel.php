<?php
include_once 'models/estructura.php';
class EjerModel extends Model{
    public function __construct(){
    parent::__construct();
    }
    public function existe($codigoine){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion,codigo FROM eje_regional WHERE codigo=:codigo");
            $sql->execute(['codigo' =>$codigoine]);
            $nombre=$sql->fetch();
            
            if($codigoine==$nombre['codigo']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
    //echo "<br>insertar datos";

    try{
       // var_dump($datos['ejeregional'],$datos['codigoine'], $datos['estatus']);
     
             $query=$this->db->connect()->prepare('INSERT INTO eje_regional(descripcion, codigo,estatus) VALUES
             (:descripcion, :codigo, :estatus)');

            $query->execute(['descripcion'=>$datos['ejeregional'],'codigo'=>$datos['codigoine'],
            'estatus'=>$datos['estatus']]);

            return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }
    
    
            }

            public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT *from eje_regional;");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_eje_regional=$row['id_eje_regional'];
              $item->descripcion=$row['descripcion'];
              $item->codigo=$row['codigo'];
              $item->estatus=$row['estatus'];
             
            
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }
              public function getPais(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT * FROM pais");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_pais=$row['id_pais'];
              $item->descripcion=$row['descripcion'];
              $item->codigo=$row['codigo'];
              $item->siglas=$row['siglas'];
              $item->estatus=$row['estatus'];
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }



public function update($item){

   //var_dump($item['eje'],$item['codigoEje'],$item['estatus'],$item['id_eje_regional']);

         $query=$this->db->connect()->prepare("UPDATE eje_regional SET descripcion= :descripcion,codigo= :codigo,estatus=:estatus WHERE id_eje_regional= :id_eje_regional");
     try{
                 $query->execute([
                 'descripcion'=>$item['eje'],
                 'codigo'=>$item['codigoEje'],          
                 'estatus'=>$item['estatus'],
                 'id_eje_regional'=>$item['id_eje_regional'],
                 
                 ]);
     return true;
                
                 
     }catch(PDOException $e){
         return false;
          }
     
         }




    }

    ?>