<?php
include_once 'models/ingpropios.php';
class CuentaBancariaModel extends Model{
    public function __construct(){
    parent::__construct();
    }
    public function existe($cuenta){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM cuenta_bancaria_tipo WHERE descripcion=:descripcion");
            $sql->execute(['descripcion' =>$cuenta]);
            $nombre=$sql->fetch();
            
            if($cuenta==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
    //echo "<br>insertar datos";

    try{
     
             $query=$this->db->connect()->prepare('INSERT INTO cuenta_bancaria_tipo (descripcion) VALUES
             (:descripcion)');

            $query->execute(['descripcion'=>$datos['cuenta']]);
 
            return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }
    
    
            }

            public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT * FROM cuenta_bancaria_tipo");
              
              while($row=$query->fetch()){
              $item=new ingpropios();
              $item->id_cuenta_bancaria_tipo=$row['id_cuenta_bancaria_tipo'];
              $item->descripcion=$row['descripcion'];
              
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }


              public function update($item){
   
       // var_dump($item['id_cuenta_bancaria_tipo2']);
        //var_dump($item['cuenta2']);

                $query=$this->db->connect()->prepare("UPDATE cuenta_bancaria_tipo SET descripcion= :descripcion WHERE id_cuenta_bancaria_tipo= :id_cuenta_bancaria_tipo");
            try{
                        $query->execute([
                        'descripcion'=>$item['cuenta2'],
                        'id_cuenta_bancaria_tipo'=>$item['id_cuenta_bancaria_tipo2'],
  
                        
                        ]);
            return true;
                       
                        
            }catch(PDOException $e){
                return false;
                 }
            
                }
                
                public function delete($id_cuenta_bancaria_tipo){
                    $query=$this->db->connect()->prepare("DELETE FROM cuenta_bancaria_tipo WHERE id_cuenta_bancaria_tipo=:id_cuenta_bancaria_tipo");
                
                        try{
                        $query->execute([
                            'id_cuenta_bancaria_tipo'=>$id_cuenta_bancaria_tipo,
                            ]);
                                return true;
                
                        }catch(PDOException $e){
                return false;
                }
                
                
                    }


                  


    }

    ?>