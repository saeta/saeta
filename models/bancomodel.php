<?php
include_once 'models/ingpropios.php';
class BancoModel extends Model{
    public function __construct(){
    parent::__construct();
    }
    public function existe($banco){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM banco WHERE  descripcion=:descripcion");
            $sql->execute(['descripcion' =>$banco]);
            $nombre=$sql->fetch();
            
            if($banco==$nombre['descripcion']){
                return $nombre['descripcion'];
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
    //echo "<br>insertar datos";

    try{
   //  var_dump($datos['banco'],$datos['codigo'],$datos['estado']);


             $query=$this->db->connect()->prepare('INSERT INTO banco(descripcion, codigo) VALUES (:descripcion, :codigo)');

            $query->execute(['descripcion'=>$datos['banco'],'codigo'=>$datos['codigo']]);

            return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }
    
    
            }

            public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT * FROM banco WHERE id_banco = id_banco");
              
              while($row=$query->fetch()){
              $item=new ingpropios();
              $item->id_banco=$row['id_banco'];
              $item->descripcion=$row['descripcion'];
              $item->codigo=$row['codigo'];
         
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }
             
/*
public function getbyId($id){
    $item = new Publicacion();

    $query = $this->db->connect()->prepare("SELECT * FROM publicacion WHERE id_publicacion = :id_publicacion");
    
    try{
        $query ->execute(['id_publicacion'=>$id]);

        while($row = $query->fetch()){
            $item->titulo = $row['titulo'];
            $item->extracto = $row['extracto'];
            $item->categoria = $row['categoria'];

        }
        return $item;
    }catch(PDOExcepton $e){
        return null;
    }
}*/


public function update($item){
   
    //var_dump($item['id_pais'],$item['estado'],$item['codigo'],$item['id_estado']);

          

         $query=$this->db->connect()->prepare("UPDATE banco SET descripcion= :descripcion,codigo= :codigo WHERE id_banco= :id_banco");
     try{
                 $query->execute([
                 'descripcion'=>$item['banco'],
                 'codigo'=>$item['codigo'],          
                 
                 'id_banco'=>$item['id_banco'],
                 
                 ]);
     return true;
                
                 
     }catch(PDOException $e){
         return false;
          }
     
         }

         public function delete($id_banco){
            $query=$this->db->connect()->prepare("DELETE FROM banco WHERE id_banco=:id_banco");
        
                try{
                $query->execute([
                    'id_banco'=>$id_banco,
                    ]);
                        return true;
        
                }catch(PDOException $e){
        return false;
        }
        
        
            }




    }

    ?>