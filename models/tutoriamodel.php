<?php
//ini_set("display_errors", 1);
include_once 'datosacademicos/tutoria.php';
include_once 'datosacademicos/docente.php';
include_once 'datosacademicos/areaubv.php';
include_once 'datosacademicos/lineainvestigacion.php';
include_once 'datosacademicos/nucleo_academico.php';
include_once 'datosacademicos/centro_estudio.php';
include_once 'datosacademicos/programa_formacion.php';
include_once 'models/estructura.php';


class TutoriaModel extends Model{
    public function __construct(){
        parent::__construct();
    }

    public function existe($tutoria){
        try{

            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM tutoria WHERE  descripcion=:descripcion");
            $sql->execute(['descripcion' =>$tutoria]);
            $nombre=$sql->fetch();

            if($tutoria==$nombre['descripcion']){
                return $nombre['descripcion'];
            }
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
        //echo "<br>insertar datos";
        try{

            $query=$this->db->connect()->prepare('INSERT INTO tutoria(
                ano_tutoria,
                periodo_lectivo,
                estatus_verificacion,
                id_docente,
                id_area_conocimiento_ubv,
                id_linea_investigacion,
                id_nucleo_academico,
                id_centro_estudio,
                id_programa,
                id_eje_regional,
                nro_estudiantes,
                descripcion
                ) VALUES(:ano_tutoria,
                         :periodo_lectivo,
                         :estatus_verificacion,
                         :id_docente,
                         :id_area_conocimiento_ubv,
                         :id_linea_investigacion,
                         :id_nucleo_academico,
                         :id_centro_estudio,
                         :id_programa,
                         :id_eje_regional,
                         :nro_estudiantes,
                         :descripcion )');

            $query->execute([
                'ano_tutoria'=>$datos['ano_tutoria'],
                'periodo_lectivo'=>$datos['periodo_lectivo'],
                'estatus_verificacion'=>'Sin Verificar',
                'id_docente'=>$datos['id_docente'],
                'id_area_conocimiento_ubv'=>$datos['id_area_conocimiento_ubv'],
                'id_linea_investigacion'=>$datos['id_linea_investigacion'],
                'id_nucleo_academico'=>$datos['id_nucleo_academico'],
                'id_centro_estudio'=>$datos['id_centro_estudio'],
                'id_programa'=>$datos['id_programa'],
                'id_eje_regional'=>$datos['id_eje_regional'],
                'nro_estudiantes'=>$datos['nro_estudiantes'],
                'descripcion'=>$datos['descripcion']
                ]);

            return true;

        }
        catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;

        }

    }

    public function getDocente(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT 
            id_docente,
            id_programa,
            id_centro_estudio,
            id_persona,
            id_eje_regional,
            id_docente_dedicacion,
            id_docente_estatus,
            tipo_docente,
            id_clasificacion_docente,
            id_escalafon
            FROM docente");
        
            while($row=$query->fetch()){
            
                $item=new Docente();
                $item->id_docente=$row['id_docente'];
                $item->id_programa=$row['id_programa'];
                $item->id_centro_estudio=$row['id_centro_estudio'];
                $item->id_persona=$row['id_persona'];
                $item->id_eje_regional=$row['id_eje_regional'];
                $item->id_docente_dedicacion=$row['id_docente_dedicacion'];
                $item->id_docente_estatus=$row['id_docente_estatus'];
                $item->tipo_docente=$row['tipo_docente'];
                $item->id_clasificacion_docente=$row['id_clasificacion_docente'];
                $item->id_escalafon=$row['id_escalafon'];
                
    
                array_push($items,$item);
        
        
            }
            return $items;
    
        }catch(PDOException $e){
        return[];
    }
    
}

public function getUbv(){
    $items=[];
    try{
        $query=$this->db->connect()->query("SELECT 
        id_area_conocimiento_ubv,
        descripcion,
        codigo,
        estatus,
        id_area_conocimiento_opsu
        FROM area_conocimiento_ubv");
    
        while($row=$query->fetch()){
        
            $item=new AreaUbv();
            $item->id_area_conocimiento_ubv=$row['id_area_conocimiento_ubv'];
            $item->descripcion=$row['descripcion'];
            $item->codigo=$row['codigo'];
            $item->estatus=$row['estatus'];
            $item->id_area_conocimiento_opsu=$row['id_area_conocimiento_opsu'];
            
            
            
    
            array_push($items,$item);
    
    
        }
        return $items;

    }catch(PDOException $e){
    return[];
}

}
    

public function getLinea(){
    $items=[];
    try{
        $query=$this->db->connect()->query("SELECT
        id_linea_investigacion,
        descripcion,
        especificacion,
        id_nucleo_academico
        FROM linea_investigacion");
        while($row=$query->fetch()){
            $item=new LineaInvestigacion();
            $item->id_linea_investigacion=$row['id_linea_investigacion'];
            $item->descripcion=$row['descripcion'];
            $item->especificacion=$row['especificacion'];
            $item->id_nucleo_academico=$row['id_nucleo_academico'];
            array_push($items,$item);
        }
        return $items;

    }catch(PDOException $e){
    return[];
}

}

public function getNucleo(){
    $items=[];
    try{
        $query=$this->db->connect()->query("SELECT 
        id_nucleo_academico as id_nucleo, 
        nucleo_academico.descripcion AS nucleo,  
        nucleo_academico.id_centro_estudio as id_centro, 
        centro_estudio.descripcion AS centro FROM nucleo_academico, 
        centro_estudio 
        WHERE  nucleo_academico.id_centro_estudio=centro_estudio.id_centro_estudio");
        
        while($row=$query->fetch()){
                
            $item=new Nucleo();
            $item->id_nucleo_academico=$row['id_nucleo'];
            $item->descripcion=$row['nucleo'];
            $item->id_centro_estudio=$row['id_centro'];
            $item->descripcion_centro=$row['centro'];
        
            array_push($items,$item);
        
        
        }

        //var_dump($items);
        return $items;
  
    }catch(PDOException $e){
    return[];
    }
  
}

public function getCentro(){
    $items=[];
    try{
        $query=$this->db->connect()->query("SELECT
        id_centro_estudio, descripcion, codigo, id_area_conocimiento_opsu, estatus
        FROM centro_estudio");

        while($row=$query->fetch()){

            $item=new Centro();
            $item->id_centro_estudio=$row['id_centro_estudio'];
            $item->descripcion=$row['descripcion'];
            $item->codigo=$row['codigo'];
            $item->id_area_conocimiento_opsu=$row['id_area_conocimiento_opsu'];
            $item->estatus=$row['estatus'];

            array_push($items,$item);

        }
        return $items;

    }catch(PDOException $e){
    return[];
    }

}

public function getProgramaF(){
    $items=[];
    try{
        $query=$this->db->connect()->query("SELECT 
        id_programa,descripcion,codigo,id_centro_estudio,id_programa_tipo,id_nivel_academico,id_linea_investigacion,estatus
        FROM programa");
    
        while($row=$query->fetch()){
        
            $item=new Programa();
            $item->id_programa=$row['id_programa'];
            $item->descripcion=$row['descripcion'];
            $item->codigo=$row['codigo'];
            $item->id_centro_estudio=$row['id_centro_estudio'];
            $item->id_programa_tipo=$row['id_programa_tipo'];
            $item->id_nivel_academico=$row['id_nivel_academico'];
            $item->id_linea_investigacion=$row['id_linea_investigacion'];
            $item->estatus=$row['estatus'];
    
            array_push($items,$item);
    
    
        }
        return $items;

    }catch(PDOException $e){
    return[];
   }

}

public function getEjeRegional(){
    $items=[];
   try{
  $query=$this->db->connect()->query("SELECT * FROM eje_regional");
  
  while($row=$query->fetch()){
  $item=new Estructura();
  $item->id_eje_regional=$row['id_eje_regional'];
  $item->descripcion=$row['descripcion'];
  $item->codigo=$row['codigo'];
  $item->estatus=$row['estatus'];
 

  
  array_push($items,$item);
  
  
  }
  return $items;
  
  }catch(PDOException $e){
  return[];
  }
  
}

public function getTutoria($identificacion){
    $items=[];
    
    try{
        //var_dump('model',$identificacion);
        $query=$this->db->connect()->prepare("SELECT 
        identificacion,
        primer_nombre,
        primer_apellido,
        id_tutoria as id_tutoria,
        tutoria.ano_tutoria as ano_tutoria,
        tutoria.periodo_lectivo as periodo_lectivo,
        tutoria.estatus_verificacion as estatus_verificacion,
        tutoria.id_docente as id_docente,
        tutoria.id_area_conocimiento_ubv as id_area_conocimiento_ubv,
        area_conocimiento_ubv.descripcion AS area_ubv,
        tutoria.id_linea_investigacion as id_linea,
        linea_investigacion.descripcion as linea,
        tutoria.id_nucleo_academico as id_nucleo,
        nucleo_academico.descripcion as nucleo,
        tutoria.id_centro_estudio as id_centro,
        centro_estudio.descripcion as centro,
        tutoria.id_programa as id_programa,
        programa.descripcion as programa,
        tutoria.id_eje_regional as id_eje_regional,
        eje_regional.descripcion as eje_regional,
        tutoria.nro_estudiantes as nro_estudiantes,
        tutoria.descripcion as tutoria
        FROM tutoria, 
        area_conocimiento_ubv,
        linea_investigacion,
        nucleo_academico, 
        centro_estudio, programa, 
        eje_regional,
        docente, persona, persona_nacionalidad
        WHERE tutoria.id_area_conocimiento_ubv=area_conocimiento_ubv.id_area_conocimiento_ubv
        and tutoria.id_linea_investigacion=linea_investigacion.id_linea_investigacion
        and tutoria.id_nucleo_academico=nucleo_academico.id_nucleo_academico
        and tutoria.id_centro_estudio=centro_estudio.id_centro_estudio
        and tutoria.id_programa=programa.id_programa
        and tutoria.id_eje_regional=eje_regional.id_eje_regional
        and tutoria.id_docente=docente.id_docente 
        and docente.id_persona=persona.id_persona 
        and persona_nacionalidad.id_persona=persona.id_persona 
        and identificacion=:identificacion");
       
        $query->execute(['identificacion'=>$identificacion]);
                

        while($row=$query->fetch()){
            $item=new Tutoria();
            $item->id_tutoria=$row['id_tutoria'];
            $item->ano_tutoria=$row['ano_tutoria'];
            $item->periodo_lectivo=$row['periodo_lectivo'];
            if($row['estatus_verificacion'] == 'Verificado'){
                $item->estatus_verificacion="Verificado";
            }elseif($row['estatus_verificacion'] == 'Sin Verificar'){
                $item->estatus_verificacion="Sin Verificar";
            }
            $item->id_docente=$row['id_docente'];
            $item->id_area_conocimiento_ubv=$row['id_area_conocimiento_ubv'];
            $item->descripcion_area_ubv=$row['area_ubv'];
            $item->id_linea_investigacion=$row['id_linea'];
            $item->descripcion_linea=$row['linea'];
            $item->id_nucleo_academico=$row['id_nucleo'];
            $item->descripcion_nucleo=$row['nucleo'];
            $item->id_centro_estudio=$row['id_centro'];
            $item->descripcion_centro=$row['centro'];
            $item->id_programa=$row['id_programa'];
            $item->descripcion_programa=$row['programa'];
            $item->id_eje_regional=$row['id_eje_regional'];
            $item->descripcion_eje_regional=$row['eje_regional'];
            $item->nro_estudiantes=$row['nro_estudiantes'];
            $item->descripcion=$row['tutoria'];
            
            

            array_push($items,$item);

        }
        return $items;

    }catch(PDOException $e){
        //echo $e->getMessage();
    return[];
}

}

public function getbyId($id){
   
    
    try{
        //var_dump('model',$id);
        $item=new Tutoria();
        $sql=$this->db->connect()->prepare("SELECT 
        identificacion,
        primer_nombre,
        primer_apellido,
        id_tutoria as id_tutoria,
        tutoria.ano_tutoria as ano_tutoria,
        tutoria.periodo_lectivo as periodo_lectivo,
        tutoria.estatus_verificacion as estatus_verificacion,
        tutoria.id_docente as id_docente,
        tutoria.id_area_conocimiento_ubv as id_area_conocimiento_ubv,
        area_conocimiento_ubv.descripcion AS area_ubv,
        tutoria.id_linea_investigacion as id_linea,
        linea_investigacion.descripcion as linea,
        tutoria.id_nucleo_academico as id_nucleo,
        nucleo_academico.descripcion as nucleo,
        tutoria.id_centro_estudio as id_centro,
        centro_estudio.descripcion as centro,
        tutoria.id_programa as id_programa,
        programa.descripcion as programa,
        tutoria.id_eje_regional as id_eje_regional,
        eje_regional.descripcion as eje_regional,
        tutoria.nro_estudiantes as nro_estudiantes,
        tutoria.descripcion as tutoria
        FROM tutoria, 
        area_conocimiento_ubv,
        linea_investigacion,
        nucleo_academico, 
        centro_estudio, programa, 
        eje_regional,
        docente, persona, persona_nacionalidad
        WHERE tutoria.id_area_conocimiento_ubv=area_conocimiento_ubv.id_area_conocimiento_ubv
        and tutoria.id_linea_investigacion=linea_investigacion.id_linea_investigacion
        and tutoria.id_nucleo_academico=nucleo_academico.id_nucleo_academico
        and tutoria.id_centro_estudio=centro_estudio.id_centro_estudio
        and tutoria.id_programa=programa.id_programa
        and tutoria.id_eje_regional=eje_regional.id_eje_regional
        and tutoria.id_docente=docente.id_docente 
        and docente.id_persona=persona.id_persona 
        and persona_nacionalidad.id_persona=persona.id_persona 
        and id_tutoria=:id_tutoria;");

       $sql->execute(['id_tutoria'=>$id ]);
                
        
        while($row=$sql->fetch()){
            
            $item->id_tutoria=$row['id_tutoria'];
            $item->ano_tutoria=$row['ano_tutoria'];
            $item->periodo_lectivo=$row['periodo_lectivo'];
            if($row['estatus_verificacion'] == 'Verificado'){
                $item->estatus_verificacion="Verificado";
            }elseif($row['estatus_verificacion'] == 'Sin Verificar'){
                $item->estatus_verificacion="Sin Verificar";
            }
            $item->id_docente=$row['id_docente'];
            $item->id_area_conocimiento_ubv=$row['id_area_conocimiento_ubv'];
            $item->descripcion_area_ubv=$row['area_ubv'];
            $item->id_linea_investigacion=$row['id_linea'];
            $item->descripcion_linea=$row['linea'];
            $item->id_nucleo_academico=$row['id_nucleo'];
            $item->descripcion_nucleo=$row['nucleo'];
            $item->id_centro_estudio=$row['id_centro'];
            $item->descripcion_centro=$row['centro'];
            $item->id_programa=$row['id_programa'];
            $item->descripcion_programa=$row['programa'];
            $item->id_eje_regional=$row['id_eje_regional'];
            $item->descripcion_eje_regional=$row['eje_regional'];
            $item->nro_estudiantes=$row['nro_estudiantes'];
            $item->descripcion=$row['tutoria'];
            
            

            //array_push($items,$item);

        }
        return $item;

    }catch(PDOException $e){
        //echo $e->getMessage();
    return[];
}

}

    public function update($datos){
            //var_dump($datos);
                  $query=$this->db->connect()->prepare("UPDATE tutoria
                  SET ano_tutoria=:ano_tutoria,
                  periodo_lectivo=:periodo_lectivo,
                  id_area_conocimiento_ubv=:id_area_conocimiento_ubv,
                  id_linea_investigacion=:id_linea_investigacion,
                  id_nucleo_academico=:id_nucleo_academico,
                  id_centro_estudio=:id_centro_estudio,
                  id_programa=:id_programa,
                  id_eje_regional=:id_eje_regional,
                  nro_estudiantes=:nro_estudiantes,
                  descripcion=:descripcion,
                  estatus_verificacion=:estatus_verificacion
                  WHERE id_tutoria=:id_tutoria");

                  try{
                      $query->execute([
                          'id_tutoria'=>$datos['id_tutoria'],
                          'ano_tutoria'=>$datos['ano_tutoria'],
                          'periodo_lectivo'=>$datos['periodo_lectivo'],
                          'id_area_conocimiento_ubv'=>$datos['id_area_conocimiento_ubv'],
                          'id_linea_investigacion'=>$datos['id_linea_investigacion'],
                          'id_nucleo_academico'=>$datos['id_nucleo_academico'],
                          'id_centro_estudio'=>$datos['id_centro_estudio'],
                          'id_programa'=>$datos['id_programa'],
                          'id_eje_regional'=>$datos['id_eje_regional'],
                          'nro_estudiantes'=>$datos['nro_estudiantes'],
                          'descripcion'=>$datos['descripcion'],
                          'estatus_verificacion'=>'Sin Verificar'

                        ]);
                   return true;

                  }catch(PDOException $e){
                       return false;
                  }
    }

    public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM tutoria WHERE id_tutoria = :id_tutoria");

                try{
                $query->execute([
                    'id_tutoria'=>$id,
                    ]);
                        return true;

                }catch(PDOException $e){
        return false;
    }

  }

}

?>