<?php
include_once 'datosacademicos/areaopsu.php';
include_once 'datosacademicos/centroestudio.php';
include_once 'estructura.php';

class Centro_estudioModel extends Model{
    public function __construct(){
        parent::__construct();
    }


    public function existe($centroestudio){
        try{
    
            //var_dump($area_conocimiento_opsu);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM centro_estudio WHERE descripcion=:descripcion");
            $sql->execute(['descripcion'=>$centroestudio]);
            $nombre=$sql->fetch();
            
            if($centroestudio==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }

    public function insert($datos){
        //echo "<br>insertar datos";
    

        try{
         //var_dump($datos);
            $query=$this->db->connect()->prepare('INSERT INTO centro_estudio(
                descripcion,
                id_area_conocimiento_opsu,
                codigo,
                estatus) VALUES(
                    :descripcion,
                    :id_area_conocimiento_opsu,
                    :codigo,
                    :estatus
                    )');
    
            $query->execute([
                    'descripcion'=>$datos['descripcion'],
                    'id_area_conocimiento_opsu'=>$datos['id_area_conocimiento_opsu'],
                    'codigo'=>$datos['codigo'],
                    'estatus'=>$datos['estatus']
                ]);
    
                return true;
          
        }catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                 }
        
        
    }

    

    public function getAreaOpsu(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT id_area_conocimiento_opsu, descripcion, codigo FROM area_conocimiento_opsu");
            
            while($row=$query->fetch()){
                
                $item=new AreaOpsu();
                $item->id_area_conocimiento_opsu=$row['id_area_conocimiento_opsu'];
                $item->descripcion=$row['descripcion'];
                $item->codigo=$row['codigo'];

                array_push($items,$item);
            
            
            }
            //var_dump($items);
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }
      
    public function getCentro(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT id_centro_estudio as id_centro,
             centro_estudio.codigo,
              centro_estudio.descripcion,
               centro_estudio.estatus,
                area_conocimiento_opsu.descripcion as descripcion_opsu,
                area_conocimiento_opsu.id_area_conocimiento_opsu as id_area_opsu
            FROM centro_estudio, area_conocimiento_opsu WHERE centro_estudio.id_area_conocimiento_opsu=area_conocimiento_opsu.id_area_conocimiento_opsu");
            
            while($row=$query->fetch()){
                
                $item=new CentroEstudio();
                $item->id_centro_estudio=$row['id_centro'];
                $item->codigo=$row['codigo'];
                $item->descripcion=$row['descripcion'];
               $item->id_area_conocimiento_opsu=$row['id_area_opsu'];
                if($row['estatus'] == 1){
                    $item->estatus="Activo";
                }elseif($row['estatus'] == 0){
                    $item->estatus="Inactivo";
                }
                //$item->estatus=$row['estatus'];
                $item->descripcion_opsu=$row['descripcion_opsu'];
                
                
                //$item->descripcion_opsu=$row['areaopsu'];
               
            
                array_push($items,$item);
            
            
            }
            //var_dump($items);
            return $items;
            
        }catch(PDOException $e){
        return[];
        }
      
    }



    public function update($datos){
       // var_dump($datos);
        //var_dump($item['id_pais'],$item['estado'],$item['codigoine'],$item['id_estado']);
        $query=$this->db->connect()->prepare("UPDATE centro_estudio SET 
        descripcion=:descripcion,
        codigo=:codigo,
        estatus=:estatus,
        id_area_conocimiento_opsu=:id_area_conocimiento_opsu
        WHERE id_centro_estudio=:id_centro_estudio");
        
        try{
            $query->execute([
                'id_centro_estudio'=>$datos['id_centro_estudio'],
                'descripcion'=>$datos['descripcion'],
                'codigo'=>$datos['codigo1'],
                'estatus'=>$datos['estatus1'],
                'id_area_conocimiento_opsu'=>$datos['id_area_conocimiento_opsu']
                
              ]);
              //creo q falta descripcion del area de conocimientoooooooooooooooooooooooooo   
         
         
              return true;

        }catch(PDOException $e){
             return false;
        }
         
    }

        public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM centro_estudio WHERE id_centro_estudio = :id_centro_estudio");
    
            try{
                $query->execute([
                'id_centro_estudio'=>$id
                ]);
                return true;
            }catch(PDOException $e){
                return false;
            }
        }



        public function getCoordDir(){
            $items=[];
            try{
                $query=$this->db->connect()->query("SELECT 
                id_usuario, 
                usuario, 
                t1.id_perfil,
                t2.descripcion as perfil,
                t1.id_persona,
                identificacion,
                primer_nombre,
                primer_apellido
                
                
                from 
                usuario as t1,
                perfil as t2, 
                persona as t3,
                persona_nacionalidad as t4
                where 
                t1.id_persona=t3.id_persona
                and t4.id_persona=t3.id_persona
                and t1.id_perfil=t2.id_perfil
                and (t1.id_perfil=2 or t1.id_perfil=3);");
                
                while($row=$query->fetch()){
                    
                    $item=new Estructura();
                    $item->id_usuario=$row['id_usuario'];
                    $item->usuario=$row['usuario'];
                    $item->id_perfil=$row['id_perfil'];
                    $item->id_persona=$row['id_persona'];
                    $item->perfil=$row['perfil'];
                    $item->primer_nombre=$row['primer_nombre'];
                    $item->primer_apellido=$row['primer_apellido'];
                    $item->identificacion=$row['identificacion'];

                    
                
                    array_push($items,$item);
                
                
                }
                //var_dump($items);
                return $items;
                
            }catch(PDOException $e){
            return[];
            }
          
        }


        public function insertAsociar($datos){
        
            try{
                
                $query=$this->db->connect()->prepare('INSERT INTO persona_centro_estudio(
                    id_persona, 
                    id_centro_estudio, 
                    estatus) VALUES(
                        :id_persona, 
                        :id_centro_estudio, 
                        :estatus
                    );');
        
                $query->execute([
                        'id_persona'=>$datos['id_persona'],
                        'id_centro_estudio'=>$datos['id_centro_estudio'],
                        'estatus'=>'Activo'
                    ]);
        
                return true;
              
            }catch(PDOException $e){
                //echo "Matricula duplicada";
                return false;
                     }
        }

        public function existeAsociar($datos){
            try{
        
                $sql = $this->db->connect()->prepare("SELECT count(*) as conteo FROM persona_centro_estudio 
                WHERE id_centro_estudio=:id_centro_estudio 
                AND id_persona=:id_persona;");

                $sql->execute([
                    'id_persona'=>$datos['id_persona'],
                    'id_centro_estudio'=>$datos['id_centro_estudio']
                    ]);

                $nombre=$sql->fetch();

                if($nombre['conteo'] >=1){
                    
                    return true;
        
                }
                return false;
            } catch(PDOException $e){
                return false;
            }
        }

        public function existeCentroActivo($datos){
            try{
        
                $sql = $this->db->connect()->prepare("SELECT count(*) as conteo FROM persona_centro_estudio 
                WHERE id_centro_estudio=:id_centro_estudio AND estatus=:estatus;");

                $sql->execute([
                    'estatus'=>'Activo',
                    'id_centro_estudio'=>$datos['id_centro_estudio']
                    ]);

                $nombre=$sql->fetch();

                if($nombre['conteo'] > 2){
                    
                    return true;
        
                }
                return false;
            } catch(PDOException $e){
                return false;
            }
        }

        public function existePersonaActivo($datos){
            try{
       
                $sql = $this->db->connect()->prepare("SELECT count(*) as conteo 
                FROM 
                persona as t1,
                usuario as t2,
                persona_centro_estudio as t3
                
                WHERE 
                t2.id_persona=t1.id_persona
                AND t3.id_persona=t1.id_persona
                and t3.id_persona=:id_persona 
                AND t2.id_perfil=:id_perfil
                AND t3.estatus=:estatus;");
                
                $sql->execute([
                    'estatus'=>'Activo',
                    'id_persona'=>$datos['id_persona'],
                    'id_perfil'=>$datos['id_perfil']
                    ]);

                $nombre=$sql->fetch();

                if($nombre['conteo'] >=1){
                    
                    return true;
        
                }
                return false;
            } catch(PDOException $e){
                return false;
            }
        }


        public function getAsociaciones(){
            $items=[];
            try{
                $query=$this->db->connect()->query("SELECT 
                id_persona_centro_estudio,
                t1.id_persona,
                primer_nombre,
                primer_apellido,
                t1.id_centro_estudio,
                t3.descripcion as centro_estudio,
                t1.estatus,
                t4.identificacion,
                substring(t5.descripcion, 1, 1) as nacionalidad,
                usuario,
                t7.descripcion as perfil
                
                FROM 
                persona_centro_estudio as t1,
                persona as t2,
                centro_estudio as t3,
                persona_nacionalidad as t4,
                documento_identidad_tipo as t5,
                usuario as t6,
                perfil as t7
                
                where 
                t1.id_persona=t2.id_persona
                AND t1.id_centro_estudio=t3.id_centro_estudio
                AND t4.id_persona=t1.id_persona
                AND t4.id_documento_identidad_tipo=t5.id_documento_identidad_tipo
                AND t6.id_persona=t2.id_persona
                AND t6.id_perfil=t7.id_perfil;");
                
                while($row=$query->fetch()){
                    
                    $item=new Estructura();
                    $item->id_persona_centro_estudio=$row['id_persona_centro_estudio'];
                    $item->id_persona=$row['id_persona'];
                    $item->primer_nombre=$row['primer_nombre'];
                    $item->primer_apellido=$row['primer_apellido'];
                    $item->id_centro_estudio=$row['id_centro_estudio'];
                    $item->centro_estudio=$row['centro_estudio'];
                    $item->estatus=$row['estatus'];
                    $item->identificacion=$row['identificacion'];
                    $item->nacionalidad=$row['nacionalidad'];
                    $item->usuario=$row['usuario'];
                    $item->perfil=$row['perfil'];

                    array_push($items,$item);
                
                
                }
                //var_dump($items);
                return $items;
                
            }catch(PDOException $e){
                return[];
            }
          
        }

        public function getByIdAsoc($id_persona_centro_estudio){

            $items=[];
            try{

                $query=$this->db->connect()->prepare("SELECT 
                id_persona_centro_estudio,
                t1.id_persona,
                primer_nombre,
                primer_apellido,
                t1.id_centro_estudio,
                t3.descripcion as centro_estudio,
                t1.estatus,
                t4.identificacion,
                substring(t5.descripcion, 1, 1) as nacionalidad,
                usuario,
                t6.id_perfil,
                t7.descripcion as perfil
                
                FROM 
                persona_centro_estudio as t1,
                persona as t2,
                centro_estudio as t3,
                persona_nacionalidad as t4,
                documento_identidad_tipo as t5,
                usuario as t6,
                perfil as t7
                
                where 
                t1.id_persona=t2.id_persona
                AND t1.id_centro_estudio=t3.id_centro_estudio
                AND t4.id_persona=t1.id_persona
                AND t4.id_documento_identidad_tipo=t5.id_documento_identidad_tipo
                AND t6.id_persona=t2.id_persona
                AND t6.id_perfil=t7.id_perfil
                AND t1.id_persona_centro_estudio=:id_persona_centro_estudio;");
                
                $query->execute(['id_persona_centro_estudio'=>$id_persona_centro_estudio]);

                $item=new Estructura();

                while($row=$query->fetch()){
                    
                    $item->id_persona_centro_estudio=$row['id_persona_centro_estudio'];
                    $item->id_persona=$row['id_persona'];
                    $item->primer_nombre=$row['primer_nombre'];
                    $item->primer_apellido=$row['primer_apellido'];
                    $item->id_centro_estudio=$row['id_centro_estudio'];
                    $item->centro_estudio=$row['centro_estudio'];
                    $item->estatus=$row['estatus'];
                    $item->identificacion=$row['identificacion'];
                    $item->nacionalidad=$row['nacionalidad'];
                    $item->usuario=$row['usuario'];
                    $item->id_perfil=$row['id_perfil'];
                    $item->perfil=$row['perfil'];

                }

                return $item;
                
            }catch(PDOException $e){
                return[];
            }
          
        }


        public function editAsociar($datos){
        
            try{
                // se tiene que actualizar los id de persona y centro de estudio ? 
                $query=$this->db->connect()->prepare('UPDATE persona_centro_estudio 
                SET
                /*id_persona=:id_persona,
                id_centro_estudio=:id_centro_estudio,*/
                estatus=:estatus
                WHERE 
                id_persona_centro_estudio=:id_persona_centro_estudio;');
        
                $query->execute([
                        'id_persona_centro_estudio'=>$datos['id_persona_centro_estudio'],
                        /*'id_persona'=>$datos['id_persona'],
                        'id_centro_estudio'=>$datos['id_centro_estudio'],*/
                        'estatus'=>$datos['estatus']
                    ]);
        
                return true;
              
            }catch(PDOException $e){
                //echo "Matricula duplicada";
                return false;
                     }
        }

    }


    ?>