<?php
include_once 'models/ingpropios.php';
class PagoModel extends Model{
    public function __construct(){
    parent::__construct();
    }


    public function existe($pago){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM unidad_pago WHERE  descripcion=:descripcion");
            $sql->execute(['descripcion' =>$pago]);
            $nombre=$sql->fetch();
            
            if($pago==$nombre['descripcion']){
                return $nombre['descripcion'];
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
        //echo "<br>insertar datos";
    
        try{
    //  var_dump($datos['estatus'],$datos['pago'],$datos['monto'] ,$datos['fecha'] ,$datos['hora']);
    
    
                 $query=$this->db->connect()->prepare('INSERT INTO unidad_pago(descripcion, monto,estatus,fecha_registro,hora_registro)
                  VALUES (:descripcion, :monto,:estatus, :fecha_registro,:hora_registro)');
    
                $query->execute(['descripcion'=>$datos['pago'],'monto'=>$datos['monto']
                ,'estatus'=>$datos['estatus'],'fecha_registro'=>$datos['fecha'],'hora_registro'=>$datos['hora']]);
    
                return true;    
          
        }catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                 }
        
        
                }


            public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT * FROM unidad_pago WHERE id_unidad_pago = id_unidad_pago");
              
              while($row=$query->fetch()){
              $item=new ingpropios();
              $item->id_unidad_pago=$row['id_unidad_pago'];
              $item->descripcion=$row['descripcion'];
              $item->monto=$row['monto'];
              $item->fecha_registro=$row['fecha_registro'];
              $item->hora_registro=$row['hora_registro'];
              $item->estatus=$row['estatus'];
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }


              public function update($item){
                //var_dump($item['id_unidad_pago']);
                 //var_dump($item['pago2']);
     
                 //var_dump($item['estatus2']);
     
                 //var_dump( $item['monto2']);
     
            
                           
                 
                          $query=$this->db->connect()->prepare("UPDATE unidad_pago SET descripcion= :descripcion,monto= :monto,
                          estatus= :estatus  WHERE id_unidad_pago= :id_unidad_pago");
                      try{
                                  $query->execute([
                                     'id_unidad_pago'=>$item['id_unidad_pago'],
                                  'descripcion'=>$item['pago2'],
                                  'monto'=>$item['monto2'],
                                  'estatus'=>$item['estatus2'],
                                  
                                  ]);
                      return true;
                                 
                                  
                      }catch(PDOException $e){
                          return false;
                           }
                      
                          }
 

    }

    ?>