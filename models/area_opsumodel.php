<?php
include_once 'datosacademicos/areaopsu.php';

class Area_opsuModel extends Model{
    public function __construct(){
        parent::__construct();
    }


    public function existe($area_conocimiento_opsu){
        try{
    
            //var_dump($area_conocimiento_opsu);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM area_conocimiento_opsu WHERE descripcion=:descripcion");
            $sql->execute(['descripcion'=>$area_conocimiento_opsu]);
            $nombre=$sql->fetch();
            
            if($area_conocimiento_opsu==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }

    public function insert($datos){
        //echo "<br>insertar datos";
    
        try{
         //var_dump($datos);
            $query=$this->db->connect()->prepare('INSERT INTO area_conocimiento_opsu(
                descripcion,
                codigo) VALUES(
                    :descripcion,
                    :codigo
                    )');
    
                $query->execute([
                    'descripcion'=>$datos['descripcion'],
                    'codigo'=>$datos['codigo']
                ]);
    
                return true;
          
        }catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                 }
        
        
    }

    

    public function get(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT * FROM area_conocimiento_opsu");
            
            while($row=$query->fetch()){
                
                $item=new AreaOpsu();
                $item->id_area_conocimiento_opsu=$row['id_area_conocimiento_opsu'];
                $item->descripcion=$row['descripcion'];
                $item->codigo=$row['codigo'];

                array_push($items,$item);
            
            
            }
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }
      
    public function update($datos){
        //var_dump($datos);
        //var_dump($item['id_pais'],$item['estado'],$item['codigoine'],$item['id_estado']);
        $query=$this->db->connect()->prepare("UPDATE area_conocimiento_opsu SET 
        descripcion=:descripcion,
        codigo=:codigo
        WHERE id_area_conocimiento_opsu=:id_area_conocimiento_opsu");
        
        try{
            $query->execute([
                'id_area_conocimiento_opsu'=>$datos['id_area_conocimiento_opsu'],
                'descripcion'=>$datos['descripcion'],
                'codigo'=>$datos['codigo'],
                 ]);
         return true;

        }catch(PDOException $e){
             return false;
        }
         
    }

        public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM area_conocimiento_opsu WHERE id_area_conocimiento_opsu = :id_area_conocimiento_opsu");
    
            try{
                $query->execute([
                'id_area_conocimiento_opsu'=>$id
                ]);
                return true;
            }catch(PDOException $e){
                return false;
            }
        }
    }


    ?>