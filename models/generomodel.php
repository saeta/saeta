<?php
include_once 'models/estructura.php';
class GeneroModel extends Model{
    public function __construct(){
    parent::__construct();
    }
    public function existe($genero){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM genero WHERE  descripcion=:descripcion");
            $sql->execute(['descripcion' =>$genero]);
            $nombre=$sql->fetch();
            
            if($genero==$nombre['descripcion']){
                return $nombre['descripcion'];
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
    //echo "<br>insertar datos";

    try{
   //  var_dump($datos['genero'],$datos['codigo'],$datos['estado']);


             $query=$this->db->connect()->prepare('INSERT INTO genero(descripcion, codigo) VALUES (:descripcion, :codigo)');

            $query->execute(['descripcion'=>$datos['genero'],'codigo'=>$datos['codigo']]);

            return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }
    
    
            }

            public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT * FROM genero WHERE id_genero = id_genero");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_genero=$row['id_genero'];
              $item->descripcion=$row['descripcion'];
              $item->codigo=$row['codigo'];
         
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }
             
/*
public function getbyId($id){
    $item = new Publicacion();

    $query = $this->db->connect()->prepare("SELECT * FROM publicacion WHERE id_publicacion = :id_publicacion");
    
    try{
        $query ->execute(['id_publicacion'=>$id]);

        while($row = $query->fetch()){
            $item->titulo = $row['titulo'];
            $item->extracto = $row['extracto'];
            $item->categoria = $row['categoria'];

        }
        return $item;
    }catch(PDOExcepton $e){
        return null;
    }
}*/


public function update($item){
   
    //var_dump($item['id_pais'],$item['estado'],$item['codigo'],$item['id_estado']);

          

         $query=$this->db->connect()->prepare("UPDATE genero SET descripcion= :descripcion,codigo= :codigo WHERE id_genero= :id_genero");
     try{
                 $query->execute([
                 'descripcion'=>$item['genero'],
                 'codigo'=>$item['codigo'],          
                 
                 'id_genero'=>$item['id_genero'],
                 
                 ]);
     return true;
                
                 
     }catch(PDOException $e){
         return false;
          }
     
         }

         public function delete($id_genero){
            $query=$this->db->connect()->prepare("DELETE FROM genero WHERE id_genero=:id_genero");
        
                try{
                $query->execute([
                    'id_genero'=>$id_genero,
                    ]);
                        return true;
        
                }catch(PDOException $e){
        return false;
        }
        
        
            }




    }

    ?>