<?php
    include_once 'SED.php';
    include_once 'models/estructura.php';
    include_once '../controllers/scripts/files.php';

      //encliptar contraseña
    class Datos_academicosModel extends Model{
        public function __construct(){
           parent::__construct();
        }
        
        //verificar que un estudio conducente, no conducente o idioma existe
        public function existe($estudio){
            
            try{

                $sql = $this->db->connect()->prepare("SELECT estudio FROM estudio WHERE estudio=:estudio");
                $sql->execute(['estudio' =>$estudio]);
                $ci=$sql->fetch();
                //var_dump('existe',$ci);

                if($estudio==$ci['estudio']){
                    
                    return true;
    
                } 



            } catch(PDOException $e){
                return false;
            }

        }
        
        // verificar si una institucion existe 
        public function existe_institucion($descripcion){
            
            try{

                //var_dump($descripcion);
                $sql = $this->db->connect()->prepare("SELECT * FROM institucion WHERE descripcion=:descripcion");
                $sql->execute(['descripcion' =>$descripcion]);
                $nombre=$sql->fetch();
                
                if($descripcion==$nombre['descripcion']){
                    
                    return $nombre;
        
                } 
                return false;



            } catch(PDOException $e){
                return false;
            }

        }

        // para insertar estudios conducente, no conducentes e idiomas
        public function insertEstudio($datos){
            try{
                //1. guardas el objeto pdo en una variable
                $pdo=$this->db->connect();

                //2. comienzas transaccion
                $pdo->beginTransaction();


                //si la id_institucion viene vacia queire decir que no existe en nuestra base de datos por ende la insertamos
                if(empty($datos['id_institucion'])){

                    
                    $institucion=$pdo->prepare("INSERT INTO institucion(
                        descripcion, 
                        id_pais, 
                        id_institucion_tipo) VALUES(
                            :descripcion,
                            :id_pais,
                            :id_institucion_tipo
                            );");

                    $institucion->execute([
                        'descripcion'=>$datos['institucion'],
                        'id_pais'=>$datos['id_pais'],
                        'id_institucion_tipo'=>$datos['id_institucion_tipo']
                        ]);
                        
                    $id_institucion="(SELECT id_institucion FROM institucion ORDER BY id_institucion DESC LIMIT 1)";
                
                }else{
                    
                    $id_institucion=$datos['id_institucion'];

                }
                    //insertamos el estudio con la institucion
                    $estudio=$pdo->prepare("INSERT INTO estudio(
                        estudio,
                        id_tipo_estudio,
                        estatus,
                        id_pais,
                        id_modalidad_estudio,
                        ano_estudio,
                        id_institucion,
                        id_docente,
                        estatus_verificacion) 
                        VALUES(
                        :estudio,
                        :id_tipo_estudio,
                        :estatus,
                        :id_pais,
                        :id_modalidad_estudio,
                        :ano_estudio,
                        ".$id_institucion.",
                        :id_docente,
                        :estatus_verificacion
                        );");

                    $estudio->execute([
                        'estudio'=>$datos['estudio'],
                        'id_tipo_estudio'=>$datos['id_tipo_estudio'],
                        'estatus'=>$datos['estatus'],
                        'id_pais'=>$datos['id_pais'],
                        'id_modalidad_estudio'=>$datos['id_modalidad_estudio'],
                        'ano_estudio'=>$datos['ano_estudio'],
                        'id_docente'=>$datos['id_docente'],
                        'estatus_verificacion'=>'Sin Verificar'
                       ]);
                        
                    if($datos['id_tipo_estudio'] == 1){//si el estudio es conducente
                        
                        $nivel_conducente=$pdo->prepare("INSERT INTO estudio_nivel(
                            id_estudio,
                            id_nivel_academico,
                            id_programa_nivel
                            ) VALUES(
                                (SELECT id_estudio FROM estudio ORDER BY id_estudio DESC LIMIT 1),
                                :id_nivel_academico,
                                :id_programa_nivel
                            );");
                        
                        $nivel_conducente->execute([
                            'id_nivel_academico'=>$datos['id_nivel_academico'],
                            'id_programa_nivel'=>$datos['id_programa_nivel']
                            ]);
                    }

                    if($datos['id_tipo_estudio'] == 1 || $datos['id_tipo_estudio'] == 2){

                        $conducente=$pdo->prepare("INSERT INTO estudio_conducente(
                            titulo_obtenido, 
                            id_estudio) VALUES(
                            :titulo_obtenido,
                            (SELECT id_estudio FROM estudio ORDER BY id_estudio DESC LIMIT 1)
                            );");
                        
                        $conducente->execute([
                            'titulo_obtenido'=>$datos['titulo_obtenido']
                            ]);

                    }elseif($datos['id_tipo_estudio'] == 3){

                        //var_dump($datos['id_tipo_estudio']);
                        $idioma=$pdo->prepare("INSERT INTO estudio_idioma(
                            nivel_lectura, 
                            nivel_escritura, 
                            nivel_habla, 
                            nivel_comprende, 
                            id_estudio, 
                            id_idioma)
                            VALUES ( 
                            :nivel_lectura, 
                            :nivel_escritura, 
                            :nivel_habla,
                            :nivel_comprende, 
                            (SELECT id_estudio FROM estudio ORDER BY id_estudio DESC LIMIT 1),
                            :id_idioma);");
                        
                        $idioma->execute([
                            'nivel_lectura'=>$datos['nivel_lectura'],
                            'nivel_escritura'=>$datos['nivel_escritura'],
                            'nivel_habla'=>$datos['nivel_habla'],
                            'nivel_comprende'=>$datos['nivel_comprende'],
                            'id_idioma'=>$datos['id_idioma']
                            ]);

                    }
                    
                       
                            

                                //var_dump($datos['estatus'], $datos['resumen']);
                                if($datos['estatus']=="concluido"){


                                    
                                    //mover documento al servidor
                                    
                                    
                                    $doc=new Documento();
                                                                        
                                    $pdf_titulo=$doc->getData($datos['pdf_titulo']);
                                    
                                    //el mover no se esta ejecutando: revisar extensiones y permisos
                                    if($doc->mover($pdf_titulo['extension'], 
                                                    'src/documentos/datos_academicos_idiomas/',
                                                    $pdf_titulo['ruta_tmp'],
                                                    $pdf_titulo['original_name'],
                                                    array('pdf')))
                                    {
                                        
                                        

                                        if($datos['id_tipo_estudio']==1){//1=conducente

                                            //falta relacion con tabla documento para cargar el titulo obtenido
                                            $concluido=$pdo->prepare("INSERT INTO trabajo_grado(
                                                titulo, 
                                                resumen, 
                                                id_estudio_conducente) 
                                                VALUES(
                                                :titulo, 
                                                :resumen,
                                                (SELECT id_estudio_conducente FROM estudio_conducente ORDER BY id_estudio_conducente DESC LIMIT 1)
                                                );");
                                            $concluido->execute([
                                                'titulo'=>$datos['titulo_trabajo'],
                                                'resumen'=>$datos['resumen']
                                                ]);
                                                
                                        } 
                                       
                                            /*
                                            la descripcion del documento es para guardar el nombre del documento
                                        */
                                        $doc_titulo=$pdo->prepare("INSERT INTO documento(
                                            descripcion,
                                            estatus,
                                            id_persona,
                                            id_tipo_documento) 
                                            VALUES(
                                                :descripcion,
                                                :estatus, 
                                                :id_persona,
                                                (SELECT id_tipo_documento FROM tipo_documento WHERE descripcion='TITULO_ESTUDIO_CONDUCENTE') 
                                                );");
                                        $doc_titulo->execute([
                                            'descripcion'=>date('Y-m-d H:i:s').$pdf_titulo['original_name'],
                                            'estatus'=>'Sin Verificar',
                                            'id_persona'=>$datos['id_persona']
                                            ]);

                                        $titulo_estudio=$pdo->query("INSERT INTO certificado_titulo_estudio(
                                            id_estudio, 
                                            id_documento) 
                                            VALUES(
                                            (SELECT id_estudio FROM estudio ORDER BY id_estudio DESC LIMIT 1),
                                            (SELECT id_documento FROM documento ORDER BY id_documento DESC LIMIT 1)
                                            );");
                                        
                                    } else{
                                        echo "falla";
                                        $pdo->rollBack();
                                        return false;
                                    }


                                }
                    //4. consignas la transaccion (en caso de que no suceda ningun fallo)
                    $pdo->commit();


                    return true; 

               
                
                

            } catch(PDOException $e){
                //5. regresas a un estado anterior en caso de error
                $pdo->rollBack();
                //echo "Fallo: " . $e->getMessage();
                return false;
            }

        }
        
        
        
         //obtener paises activos
         public function getPais(){
            $items=[];
            
            try{
                $query=$this->db->connect()->query("SELECT * FROM pais");
          
                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id_pais=$row['id_pais'];
                    $item->descripcion=$row['descripcion'];
                    $item->codigo=$row['codigo'];
                    $item->estatus=$row['estatus'];
          
                    array_push($items,$item);
          
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
        }


        //////////////    funcion que llama datos de tablas catalogos   ///////////////
        public function getCatalogo($valor){
            $items=[];
            try{
                $query=$this->db->connect()->query("SELECT * FROM ".$valor."");
          
                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id=$row['id_'.$valor.''];
                    $item->descripcion=$row['descripcion'];
                    
                    array_push($items,$item);
                
                
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
          
        }

        //obtener instituciones 
        public function getInstitucion(){
            $items=[];
            
            try{
                $query=$this->db->connect()->query("SELECT 
                id_institucion,
                institucion.descripcion AS institucion,
                institucion.id_pais,
                pais.descripcion AS pais,
                codigo,
                id_institucion_tipo
                FROM 
                institucion, pais 
                WHERE institucion.id_pais=pais.id_pais;
                ");
          
                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id_institucion=$row['id_institucion'];
                    $item->id_pais=$row['id_pais'];
                    $item->descripcion=$row['institucion'];
                    $item->descripcion_pais=$row['pais'];
                    $item->codigo=$row['codigo'];
                    $item->id_institucion_tipo=$row['id_institucion_tipo'];
          
                    array_push($items,$item);
          
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
        }

        //obtener idiomas 
        public function getIdioma(){
            $items=[];
            
            try{
                $query=$this->db->connect()->query("SELECT * FROM idioma;");
          
                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id_idioma=$row['id_idioma'];
                    $item->code=$row['code'];
                    $item->name=$row['name'];
                    $item->nativename=$row['nativename'];
          
                    array_push($items,$item);
          
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
        }


        // obtener estudios conducentes
        public function getEstudioConducente($id){
            $items=[];
            
            try{
                $query=$this->db->connect()->prepare("SELECT 
                estudio_conducente.id_estudio_conducente,
                titulo_obtenido, 
                estatus_verificacion,
                estudio_conducente.id_estudio,
                estudio,
                estudio.id_tipo_estudio,
                tipo_estudio.descripcion as tipo_estudio,
                ano_estudio,
                estudio.id_pais,
                pais.descripcion as pais,
                estudio.id_modalidad_estudio,
                modalidad_estudio.descripcion as modalidad_estudio,
                estudio.id_institucion,
                institucion.descripcion as institucion,
                institucion.id_institucion_tipo,
                institucion_tipo.descripcion as  institucion_tipo,
                estudio.estatus,
                id_docente,
                estudio_nivel.id_nivel_academico,
                nivel_academico.descripcion as nivel_academico,
                estudio_nivel.id_programa_nivel,
                programa_nivel.descripcion as programa_nivel
                from
                estudio_conducente,
                estudio,
                tipo_estudio,
                pais,
                modalidad_estudio,
                institucion,
                institucion_tipo,
                estudio_nivel,
                nivel_academico,
                programa_nivel
                where
                estudio_conducente.id_estudio=estudio.id_estudio
                and estudio.id_tipo_estudio=tipo_estudio.id_tipo_estudio
                and estudio.id_pais=pais.id_pais
                and estudio.id_modalidad_estudio=modalidad_estudio.id_modalidad_estudio
                and estudio.id_institucion=institucion.id_institucion 
                and institucion.id_institucion_tipo=institucion_tipo.id_institucion_tipo 
                and estudio_nivel.id_estudio=estudio.id_estudio
                and estudio_nivel.id_nivel_academico=nivel_academico.id_nivel_academico
                and estudio_nivel.id_programa_nivel=programa_nivel.id_programa_nivel
                and id_docente=:id_docente;");

                $query->execute(['id_docente'=>$id]);
                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id_estudio_conducente=$row['id_estudio_conducente'];

                    $item->id_estudio=$row['id_estudio'];
                    $item->estudio=$row['estudio'];
                    $item->id_tipo_estudio=$row['id_tipo_estudio'];
                    $item->tipo_estudio=$row['tipo_estudio'];
                    $item->estatus=$row['estatus'];
                            
                    $item->id_pais=$row['id_pais'];
                    $item->pais=$row['pais'];
                    $item->id_modalidad_estudio=$row['id_modalidad_estudio'];
                    $item->modalidad_estudio=$row['modalidad_estudio'];
                    $item->ano_estudio=$row['ano_estudio'];
                    $item->id_institucion=$row['id_institucion'];
                    $item->institucion=$row['institucion'];
                    $item->id_institucion_tipo=$row['id_institucion_tipo'];
                    $item->institucion_tipo=$row['institucion_tipo'];

                    $item->id_docente=$row['id_docente'];
                    $item->titulo_obtenido=$row['titulo_obtenido'];
        
                    $item->id_nivel_academico=$row['id_nivel_academico'];
                    $item->nivel_academico=$row['nivel_academico'];

                    $item->id_programa_nivel=$row['id_programa_nivel'];
                    $item->programa_nivel=$row['programa_nivel'];

                    $item->estatus_verificacion=$row['estatus_verificacion'];

                    array_push($items,$item);
          
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
        }

        //obtener estudios no conducentes a grado
        public function getEstudio($id){
            $items=[];
            
            try{
                $query=$this->db->connect()->prepare("SELECT 
                estudio_conducente.id_estudio_conducente,
                estudio_conducente.id_estudio as id_estudio,
                estudio, 
                estudio_conducente.titulo_obtenido,
                estudio.id_tipo_estudio,
                tipo_estudio.descripcion as tipo_estudio,
                estudio.estatus,
                estatus_verificacion
                from 
                estudio, 
                estudio_conducente,
                tipo_estudio
                WHERE 
                estudio_conducente.id_estudio=estudio.id_estudio 
                and estudio.id_tipo_estudio=tipo_estudio.id_tipo_estudio
                and estudio.id_tipo_estudio=2
                and id_docente=:id_docente;");

                $query->execute(['id_docente'=>$id]);
                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id_estudio=$row['id_estudio'];
                    $item->estudio=$row['estudio'];
                    $item->id_tipo_estudio=$row['id_tipo_estudio'];
                    $item->descripcion_tipo=$row['tipo_estudio'];
                    $item->estatus=$row['estatus'];
                    $item->id_estudio_conducente=$row['id_estudio_conducente'];
                    $item->titulo_obtenido=$row['titulo_obtenido'];
                    $item->estatus_verificacion=$row['estatus_verificacion'];

                    array_push($items,$item);
          
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
        }


        //obtener estudios de idioma
        public function getEstudioIdioma($id){
            $items=[];
            
            try{


                
                $query=$this->db->connect()->prepare("SELECT 
                estudio_idioma.id_estudio_idioma,
                estudio_idioma.id_estudio,
                estudio, 
                estudio_idioma.id_idioma,
                idioma.name as idioma,
                estudio.id_tipo_estudio,
                tipo_estudio.descripcion as tipo_estudio,
                estudio.estatus,
                estudio.estatus_verificacion
                 FROM 
                 estudio_idioma, 
                 idioma,
                 estudio,
                 tipo_estudio
                 WHERE 
                 estudio_idioma.id_idioma=idioma.id_idioma
                 and estudio_idioma.id_estudio=estudio.id_estudio 
                 and estudio.id_tipo_estudio=tipo_estudio.id_tipo_estudio    
                 and id_docente=:id_docente;");
                
                $query->execute(['id_docente'=>$id]);

                while($row=$query->fetch()){
                    $item=new Estructura();
                    
                    $item->id_estudio=$row['id_estudio'];
                    $item->estudio=$row['estudio'];
                    $item->id_tipo_estudio=$row['id_tipo_estudio'];
                    $item->descripcion_tipo=$row['tipo_estudio'];
                    $item->estatus=$row['estatus'];
                    $item->id_estudio_idioma=$row['id_estudio_idioma'];
                    $item->idioma=$row['idioma'];
                    $item->estatus_verificacion=$row['estatus_verificacion'];

                    array_push($items,$item);
          
                }

                
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
        }


        //obtener estudios conducentes y no conducentes
        public function getByIdEstudioConducente($id){
    
            try{

                $item = new Estructura();
                $sql = $this->db->connect()->prepare("SELECT 
                estudio_conducente.id_estudio_conducente,
                titulo_obtenido, 
                estatus_verificacion,
                estudio_conducente.id_estudio,
                estudio,
                estudio.id_tipo_estudio,
                tipo_estudio.descripcion as tipo_estudio,
                ano_estudio,
                estudio.id_pais,
                pais.descripcion as pais,
                estudio.id_modalidad_estudio,
                modalidad_estudio.descripcion as modalidad_estudio,
                estudio.id_institucion,
                institucion.descripcion as institucion,
                institucion.id_institucion_tipo,
                institucion_tipo.descripcion as  institucion_tipo,
                estudio.estatus,
                id_docente,
                estudio_nivel.id_nivel_academico,
                nivel_academico.descripcion as nivel_academico,
                estudio_nivel.id_programa_nivel,
                programa_nivel.descripcion as programa_nivel,
                estatus_verificacion
                from
                estudio_conducente,
                estudio,
                tipo_estudio,
                pais,
                modalidad_estudio,
                institucion,
                institucion_tipo,
                estudio_nivel,
                nivel_academico,
                programa_nivel
                where
                estudio_conducente.id_estudio=estudio.id_estudio
                and estudio.id_tipo_estudio=tipo_estudio.id_tipo_estudio
                and estudio.id_pais=pais.id_pais
                and estudio.id_modalidad_estudio=modalidad_estudio.id_modalidad_estudio
                and estudio.id_institucion=institucion.id_institucion 
                and institucion.id_institucion_tipo=institucion_tipo.id_institucion_tipo 
                and estudio_nivel.id_estudio=estudio.id_estudio
                and estudio_nivel.id_nivel_academico=nivel_academico.id_nivel_academico
                and estudio_nivel.id_programa_nivel=programa_nivel.id_programa_nivel
                and id_estudio_conducente=:id_estudio_conducente;");
        
                    
                $sql->execute(['id_estudio_conducente'=>$id]);
                        
        
                while($row=$sql->fetch()){
        
                    $item->id_estudio=$row['id_estudio'];
                    $item->estudio=$row['estudio'];
                    $item->id_tipo_estudio=$row['id_tipo_estudio'];
                    $item->tipo_estudio=$row['tipo_estudio'];
                    $item->estatus=$row['estatus'];
                    $item->estatus_verificacion=$row['estatus_verificacion'];

                            
                    $item->id_pais=$row['id_pais'];
                    $item->pais=$row['pais'];
                    $item->id_modalidad_estudio=$row['id_modalidad_estudio'];
                    $item->modalidad_estudio=$row['modalidad_estudio'];
                    $item->ano_estudio=$row['ano_estudio'];
                    $item->id_institucion=$row['id_institucion'];
                    $item->institucion=$row['institucion'];
                    $item->id_institucion_tipo=$row['id_institucion_tipo'];
                    $item->institucion_tipo=$row['institucion_tipo'];

                    $item->id_docente=$row['id_docente'];
                    $item->titulo_obtenido=$row['titulo_obtenido'];
        
                    $item->id_nivel_academico=$row['id_nivel_academico'];
                    $item->nivel_academico=$row['nivel_academico'];

                    $item->id_programa_nivel=$row['id_programa_nivel'];
                    $item->programa_nivel=$row['programa_nivel'];

                }
        
                if($item->id_tipo_estudio==1 && $item->estatus=="concluido"){
                    $trabajo_grado = $this->db->connect()->prepare("SELECT * from trabajo_grado where id_estudio_conducente=:id_estudio_conducente");
                    $trabajo_grado->execute(['id_estudio_conducente'=>$id]);
                    while($row1=$trabajo_grado->fetch()){
                        $item->id_trabajo_grado=$row1['id_trabajo_grado'];
                        $item->titulo=$row1['titulo'];
                        $item->resumen=$row1['resumen'];
                        $item->id_estudio_conducente=$row1['id_estudio_conducente'];
        
                    }
        
                }
        
                return $item;
            }catch(PDOExcepton $e){
                return null;
            }
        }
        

        //obtener estudios no conducentes
        public function getbyIdEstudio($id){
            
            try{
            $item = new Estructura();
            $sql = $this->db->connect()->prepare("SELECT 
            estudio_conducente.id_estudio_conducente,
            titulo_obtenido, 
            estatus_verificacion,
            estudio_conducente.id_estudio,
            estudio,
            estudio.id_tipo_estudio,
            tipo_estudio.descripcion as tipo_estudio,
            ano_estudio,
            estudio.id_pais,
            pais.descripcion as pais,
            estudio.id_modalidad_estudio,
            modalidad_estudio.descripcion as modalidad_estudio,
            estudio.id_institucion,
            institucion.descripcion as institucion,
            institucion.id_institucion_tipo,
            institucion_tipo.descripcion as  institucion_tipo,
            estudio.estatus,
            id_docente
            from
            estudio_conducente,
            estudio,
            tipo_estudio,
            pais,
            modalidad_estudio,
            institucion,
            institucion_tipo
            where
            estudio_conducente.id_estudio=estudio.id_estudio
            and estudio.id_tipo_estudio=tipo_estudio.id_tipo_estudio
            and estudio.id_pais=pais.id_pais
            and estudio.id_modalidad_estudio=modalidad_estudio.id_modalidad_estudio
            and estudio.id_institucion=institucion.id_institucion 
            and institucion.id_institucion_tipo=institucion_tipo.id_institucion_tipo 
            and id_estudio_conducente=:id_estudio_conducente;
            
            ");

            
                $sql->execute(['id_estudio_conducente'=>$id]);
                

                while($row=$sql->fetch()){

                   
                    $item->id_estudio=$row['id_estudio'];
                    $item->estudio=$row['estudio'];
                    $item->id_tipo_estudio=$row['id_tipo_estudio'];
                    $item->tipo_estudio=$row['tipo_estudio'];
                    $item->estatus=$row['estatus'];
                    
                    $item->id_pais=$row['id_pais'];
                    $item->pais=$row['pais'];
                    $item->id_modalidad_estudio=$row['id_modalidad_estudio'];
                    $item->modalidad_estudio=$row['modalidad_estudio'];
                    $item->ano_estudio=$row['ano_estudio'];
                    $item->id_institucion=$row['id_institucion'];
                    $item->institucion=$row['institucion'];
                    $item->id_institucion_tipo=$row['id_institucion_tipo'];
                    $item->institucion_tipo=$row['institucion_tipo'];

                    $item->id_docente=$row['id_docente'];

                    $item->titulo_obtenido=$row['titulo_obtenido'];


                }

                if($item->id_tipo_estudio==1 && $item->estatus=="concluido"){
                    $trabajo_grado = $this->db->connect()->prepare("SELECT * from trabajo_grado where id_estudio_conducente=:id_estudio_conducente");
                    $trabajo_grado->execute(['id_estudio_conducente'=>$id]);
                    while($row1=$trabajo_grado->fetch()){
                        $item->id_trabajo_grado=$row1['id_trabajo_grado'];
                        $item->titulo=$row1['titulo'];
                        $item->resumen=$row1['resumen'];
                        $item->id_estudio_conducente=$row1['id_estudio_conducente'];

                    }

                }

                return $item;
            }catch(PDOExcepton $e){
                return null;
            }
        }

        //obtener estudios de idiomas
        public function getByIDEstudioIdioma($id){
            
            try{
                $item = new Estructura();
                $sql = $this->db->connect()->prepare("SELECT 
                estudio_idioma.id_estudio_idioma,
                
                estudio_idioma.id_estudio,
                estudio,
                estudio.id_pais,
                pais.descripcion as pais,
                estudio.id_modalidad_estudio,
                modalidad_estudio.descripcion as modalidad_estudio,
                estudio.id_institucion,
                institucion.descripcion as institucion,
                institucion.id_institucion_tipo,
                institucion_tipo.descripcion as institucion_tipo,
                ano_estudio,
                estudio_idioma.id_idioma,
                idioma.name as idioma,
                estudio.id_tipo_estudio,
                tipo_estudio.descripcion as tipo_estudio,
                estudio_idioma.nivel_lectura,
                estudio_idioma.nivel_escritura,
                estudio_idioma.nivel_habla,
                estudio_idioma.nivel_comprende,
                estudio.estatus
                FROM 
                estudio,
                estudio_idioma,
                idioma,
                tipo_estudio,
                pais,
                modalidad_estudio,
                institucion,
                institucion_tipo
                WHERE 
                estudio_idioma.id_estudio=estudio.id_estudio
                and estudio_idioma.id_idioma=idioma.id_idioma 
                and estudio.id_tipo_estudio=tipo_estudio.id_tipo_estudio
                and estudio.id_pais=pais.id_pais
                and estudio.id_modalidad_estudio=modalidad_estudio.id_modalidad_estudio
                and estudio.id_institucion=institucion.id_institucion 
                and institucion.id_institucion_tipo=institucion_tipo.id_institucion_tipo 
                and id_estudio_idioma=:id_estudio_idioma;
                ");

            
                $sql->execute(['id_estudio_idioma'=>$id]);
                

                while($row=$sql->fetch()){

                   
                    $item->id_estudio=$row['id_estudio'];
                    $item->estudio=$row['estudio'];
                    $item->id_tipo_estudio=$row['id_tipo_estudio'];
                    $item->tipo_estudio=$row['tipo_estudio'];
                    $item->estatus=$row['estatus'];
                    $item->id_pais=$row['id_pais'];
                    $item->pais=$row['pais'];
                    $item->id_modalidad_estudio=$row['id_modalidad_estudio'];
                    $item->modalidad_estudio=$row['modalidad_estudio'];
                    $item->ano_estudio=$row['ano_estudio'];
                    $item->id_institucion=$row['id_institucion'];
                    $item->institucion=$row['institucion'];
                    $item->id_institucion_tipo=$row['id_institucion_tipo'];
                    $item->institucion_tipo=$row['institucion_tipo'];

                    $item->id_docente=$row['id_docente'];

                    $item->id_idioma=$row['id_idioma'];
                    $item->idioma=$row['idioma'];

                    $item->nivel_lectura=$row['nivel_lectura'];
                    $item->nivel_escritura=$row['nivel_escritura'];
                    $item->nivel_habla=$row['nivel_habla'];
                    $item->nivel_comprende=$row['nivel_comprende'];


                }
                return $item;
            }catch(PDOExcepton $e){
                return null;
            }

        }

        public function update($datos){

            try{
                //var_dump('modelo>>',$datos['id_estudio_tabla']);
                //1. guardas el objeto pdo en una variable
                $pdo=$this->db->connect();

                //2. comienzas transaccion
                $pdo->beginTransaction();


                
                //si la id_institucion viene vacia queire decir que no existe en nuestra base de datos por ende la insertamos
                if(empty($datos['id_institucion'])){

                    $institucion=$pdo->prepare("INSERT INTO institucion(
                        descripcion, 
                        id_pais, 
                        id_institucion_tipo) VALUES(
                            :descripcion,
                            :id_pais,
                            :id_institucion_tipo
                            );");

                    $institucion->execute([
                        'descripcion'=>$datos['institucion'],
                        'id_pais'=>$datos['id_pais'],
                        'id_institucion_tipo'=>$datos['id_institucion_tipo']
                        ]);
                    


                    $id_institucion="(SELECT id_institucion FROM institucion ORDER BY id_institucion DESC LIMIT 1)";

                }else{
                    $id_institucion=$datos['id_institucion'];
                }

                //modificar tabla estudio nivel 
                if($datos['id_tipo_estudio']==1){

                    $estudio_nivel=$pdo->prepare("UPDATE estudio_nivel 
                    set id_nivel_academico=:id_nivel_academico, 
                    id_programa_nivel=:id_programa_nivel
                    where id_estudio=:id_estudio;");

                    $estudio_nivel->execute([
                        'id_nivel_academico'=>$datos['id_nivel_academico'],
                        'id_programa_nivel'=>$datos['id_programa_nivel'],
                        'id_estudio'=>$datos['id_estudio']
                        ]);
                }

                //inner join segun tabla estudio_conducente o estudio_idioma
                if($datos['id_tipo_estudio']==1 || $datos['id_tipo_estudio']==2){
                    $inner_estudio="UPDATE estudio
                    SET 
                    estudio=:estudio,
                    estatus=:estatus,
                    id_pais=:id_pais,
                    id_modalidad_estudio=:id_modalidad_estudio,
                    ano_estudio=:ano_estudio,
                    id_institucion=".$id_institucion.",
                    estatus_verificacion=:estatus_verificacion
                    FROM estudio_conducente
                    WHERE estudio_conducente.id_estudio=estudio.id_estudio
                    and id_estudio_conducente=:id_estudio_tabla;";
                    
                } elseif($datos['id_tipo_estudio']==3){
                    $inner_estudio="UPDATE estudio
                    SET 
                    estudio=:estudio,
                    estatus=:estatus,
                    id_pais=:id_pais,
                    id_modalidad_estudio=:id_modalidad_estudio,
                    ano_estudio=:ano_estudio,
                    id_institucion=".$id_institucion."
                    FROM estudio_idioma
                    WHERE estudio_idioma.id_estudio=estudio.id_estudio
                    and id_estudio_idioma=:id_estudio_tabla;";
                }
                
                $estudio=$pdo->prepare($inner_estudio);
                
                $estudio->execute([
                    'estudio'=>$datos['estudio'],
                    'estatus'=>$datos['estatus'],
                    'id_pais'=>$datos['id_pais'],
                    'id_modalidad_estudio'=>$datos['id_modalidad_estudio'],
                    'ano_estudio'=>$datos['ano_estudio'],
                    'id_estudio_tabla'=>$datos['id_estudio_tabla'],
                    'estatus_verificacion'=>'Sin Verificar'
                    ]);


                    if($datos['id_tipo_estudio']==1 || $datos['id_tipo_estudio']==2){
                        $conducente=$pdo->prepare("UPDATE 
                            estudio_conducente 
                            SET
                            titulo_obtenido=:titulo_obtenido 
                            WHERE 
                            id_estudio_conducente=:id_estudio_tabla");
                    
                        $conducente->execute([
                            'titulo_obtenido'=>$datos['titulo_obtenido'],
                            'id_estudio_tabla'=>$datos['id_estudio_tabla'],
                        ]);
                    }
                            //3=idioma
                            if($datos['id_tipo_estudio']==3){
                                $idioma=$pdo->prepare("UPDATE 
                                estudio_idioma 
                                SET 
                                nivel_lectura=:nivel_lectura,
                                nivel_escritura=:nivel_escritura,
                                nivel_habla=:nivel_habla,
                                nivel_comprende=:nivel_comprende,
                                id_idioma=:id_idioma
                                where id_estudio_idioma=:id_estudio_tabla");
                                $idioma->execute([
                                    'nivel_lectura'=>$datos['nivel_lectura'],
                                    'nivel_escritura'=>$datos['nivel_escritura'],
                                    'nivel_habla'=>$datos['nivel_habla'],
                                    'nivel_comprende'=>$datos['nivel_comprende'],
                                    'id_idioma'=>$datos['id_idioma'],
                                    'id_estudio_tabla'=>$datos['id_estudio_tabla']
                                ]);
                            }
                    if(!empty($datos['pdf_titulo']['name'])){
                            $doc=new Documento();
                                                                        
                            $pdf_titulo=$doc->getData($datos['pdf_titulo']);
                            //var_dump($pdf_titulo);
                            //el mover no se esta ejecutando: revisar extensiones y permisos
                            if($doc->mover($pdf_titulo['extension'], 
                                           'src/documentos/datos_academicos_idiomas/',
                                            $pdf_titulo['ruta_tmp'],
                                            $pdf_titulo['original_name'],
                                            array('pdf')))
                            {

                                if($datos['id_tipo_estudio']==1){
                                    //falta relacion con tabla documento para cargar el titulo obtenido
                                    $concluido=$pdo->prepare("INSERT INTO trabajo_grado(
                                        titulo, 
                                        resumen, 
                                        id_estudio_conducente) 
                                        VALUES(
                                        :titulo, 
                                        :resumen,
                                        (SELECT id_estudio_conducente FROM estudio_conducente ORDER BY id_estudio_conducente DESC LIMIT 1)
                                        );");
                                    $concluido->execute([
                                        'titulo'=>$datos['titulo_trabajo'],
                                        'resumen'=>$datos['resumen']
                                        ]);

                                }
                                       /*
                                            la descripcion del documento es para guardar el nombre del documento
                                        */
                                        $doc_titulo=$pdo->prepare("INSERT INTO documento(
                                            descripcion,
                                            estatus,
                                            id_persona,
                                            id_tipo_documento) 
                                            VALUES(
                                                :descripcion,
                                                :estatus, 
                                                :id_persona,
                                                (SELECT id_tipo_documento FROM tipo_documento WHERE descripcion='TITULO_ESTUDIO_CONDUCENTE') 
                                                );");
                                        $doc_titulo->execute([
                                            'descripcion'=>date('Y-m-d H:i:s').$pdf_titulo['original_name'],
                                            'estatus'=>'Sin Verificar',
                                            'id_persona'=>$datos['id_persona']
                                            ]);

                                        $titulo_estudio=$pdo->query("INSERT INTO certificado_titulo_estudio(
                                            id_estudio, 
                                            id_documento) 
                                            VALUES(
                                            (SELECT id_estudio FROM estudio ORDER BY id_estudio DESC LIMIT 1),
                                            (SELECT id_documento FROM documento ORDER BY id_documento DESC LIMIT 1)
                                            );");


                            }
                            

                    


                }

               

                if(empty($datos['pdf_titulo']['name']) && $datos['id_tipo_estudio']==1){
                    $update_teg=$pdo->prepare("UPDATE 
                    trabajo_grado 
                    SET 
                    titulo=:titulo,
                    resumen=:resumen
                    WHERE 
                    id_estudio_conducente=:id_estudio_tabla");
                    $update_teg->execute([
                        'titulo'=>$datos['titulo_trabajo'],
                        'resumen'=>$datos['resumen'],
                        'id_estudio_tabla'=>$datos['id_estudio_tabla']
                    ]);
                }
                //update tablas estudio_conducente

                //4. consignas la transaccion (en caso de que no suceda ningun fallo)
                $pdo->commit();
                return true; 
                

            } catch(PDOException $e){
                //5. regresas a un estado anterior en caso de error
                $pdo->rollBack();
                //echo "Fallo: " . $e->getMessage();
                return false;
            }
        }


        public function getDocumentbyID($id){

            try{
                $item = new Estructura();
                $sql = $this->db->connect()->prepare("SELECT 
                id_certificado_titulo_estudio,
                certificado_titulo_estudio.id_estudio,
                estudio,
                certificado_titulo_estudio.id_documento,
                documento.descripcion
                FROM 
                certificado_titulo_estudio, 
                documento,
                estudio
                WHERE 
                certificado_titulo_estudio.id_documento=documento.id_documento 
                AND certificado_titulo_estudio.id_estudio=estudio.id_estudio 
                AND certificado_titulo_estudio.id_estudio=:id_estudio;                
                ");
    
                
                    $sql->execute(['id_estudio'=>$id]);
                    
    
                    while($row=$sql->fetch()){
    
                        $item->id_certificado_titulo_estudio=$row['id_certificado_titulo_estudio'];
                        $item->id_estudio=$row['id_estudio'];
                        $item->estudio=$row['estudio'];
                        $item->id_documento=$row['id_documento'];
                        $item->descripcion=$row['descripcion'];
                        
                    }
                    return $item;
                }catch(PDOExcepton $e){
                    return null;
                }



        }



        public function delete($datos){

            try{
                //var_dump($datos);
                
                //1. guardas el objeto pdo en una variable
                $pdo=$this->db->connect();

                //2. comienzas transaccion
                $pdo->beginTransaction();

                //var_dump($datos['id_tipo_estudio']==1 || $datos['id_tipo_estudio']==2);
                if($datos['id_tipo_estudio']==1 || $datos['id_tipo_estudio']==2){

                    if($datos['id_tipo_estudio']==1 && $datos['estatus']==1){

                        $teg=$pdo->prepare("DELETE from trabajo_grado where id_estudio_conducente=:id_estudio_tabla;");
                        $teg->execute(['id_estudio_tabla'=>$datos['id_estudio_tabla']]);

                    }

                    $conducente=$pdo->prepare("DELETE from estudio_conducente where id_estudio_conducente=:id_estudio_tabla;");
                    $conducente->execute(['id_estudio_tabla'=>$datos['id_estudio_tabla']]);


                }
               

                if($datos['id_tipo_estudio']==3){
                    $idioma=$pdo->prepare("DELETE from estudio_idioma where id_estudio_idioma=:id_estudio_tabla;");
                    $idioma->execute(['id_estudio_tabla'=>$datos['id_estudio_tabla']]);
                    
                }
                if($datos['estatus']==1){

                    $pdf=$pdo->prepare("SELECT 
                    certificado_titulo_estudio.id_documento, 
                    descripcion 
                    from 
                    documento, 
                    certificado_titulo_estudio 
                    where 
                    certificado_titulo_estudio.id_documento=documento.id_documento 
                    and id_estudio=:id_estudio;
                    ");
                    $pdf->execute(['id_estudio'=>$datos['id_estudio']]);

                    $id=$pdf->fetch(PDO::FETCH_ASSOC);



                    $ruta='src/documentos/datos_academicos_idiomas/'.$id['descripcion'];

                    if(unlink($ruta)){
                        $certificado_titulo=$pdo->prepare("DELETE from certificado_titulo_estudio where id_estudio=:id_estudio;");
                        $certificado_titulo->execute(['id_estudio'=>$datos['id_estudio']]);

                        $doc=$pdo->prepare("DELETE from documento where id_documento=:id_documento;");
                        $doc->execute(['id_documento'=>$id['id_documento']]);
                    }
                   

                }
                
                $estudio=$pdo->prepare("DELETE from estudio where id_estudio=:id_estudio;");
                $estudio->execute(['id_estudio'=>$datos['id_estudio']]);
                
                $pdo->commit();
                return true;

            } catch(PDOException $e){
                //5. regresas a un estado anterior en caso de error
                $pdo->rollBack();
                //echo "Fallo: " . $e->getMessage();
                return false;
            }
        }

    


    }
        
    

?>