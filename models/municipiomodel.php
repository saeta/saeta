<?php
include_once 'models/estructura.php';
class MunicipioModel extends Model{
    public function __construct(){
    parent::__construct();
    }
    public function existe($codigoine){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion,codigo FROM municipio WHERE codigo=:codigo");
            $sql->execute(['codigo' =>$codigoine]);
            $nombre=$sql->fetch();
            
            if($codigoine==$nombre['codigo']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
    //echo "<br>insertar datos";

    try{
   //  var_dump($datos['municipio'],$datos['codigoine'],$datos['estado']);


             $query=$this->db->connect()->prepare('INSERT INTO municipio(descripcion, codigo,id_estado) VALUES (:descripcion, :codigo, :id_estado)');

            $query->execute(['descripcion'=>$datos['municipio'],'codigo'=>$datos['codigoine'],'id_estado'=>$datos['estado']]);

            return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }
    
    
            }

            public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT id_municipio,municipio.descripcion AS municipio,municipio.codigo AS codigo ,estado.id_estado, estado.descripcion AS estado FROM municipio,estado WHERE estado.id_estado=municipio.id_estado");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_municipio=$row['id_municipio'];
              $item->municipio=$row['municipio'];
              $item->codigo=$row['codigo'];
              $item->id_estado=$row['id_estado'];
              $item->estado=$row['estado'];
            
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }
              public function getEstado(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT * FROM estado");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_estado=$row['id_estado'];
              $item->descripcion=$row['descripcion'];
              $item->codigo=$row['codigo'];
              $item->id_pais=$row['id_pais'];
             
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }



public function update($item){
   
    //var_dump($item['id_pais'],$item['estado'],$item['codigoine'],$item['id_estado']);

          

         $query=$this->db->connect()->prepare("UPDATE municipio SET descripcion= :descripcion,codigo= :codigo,id_estado=:id_estado WHERE id_municipio= :id_municipio");
     try{
                 $query->execute([
                 'descripcion'=>$item['municipio'],
                 'codigo'=>$item['codigoine'],          
                 'id_estado'=>$item['estado'],
                 'id_municipio'=>$item['id_municipio'],
                 
                 ]);
     return true;
                
                 
     }catch(PDOException $e){
         return false;
          }
     
         }



    }

    ?>