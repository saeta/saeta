<?php
include_once 'datosacademicos/unidad_curricular.php';

class UnidadcurricularModel extends Model{
    public function __construct(){
        parent::__construct();
    }

    public function existe($unidad_curricular){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM unidad_curricular WHERE  descripcion=:descripcion");
            $sql->execute(['descripcion' =>$unidad_curricular]);
            $nombre=$sql->fetch();
            
            if($unidad_curricular==$nombre['descripcion']){
                return $nombre['descripcion'];
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
        //echo "<br>insertar datos";
        //var_dump($datos);
        try{
            
            $query=$this->db->connect()->prepare('INSERT INTO unidad_curricular(descripcion,estatus) VALUES(:descripcion,:estatus)');
           
            $query->execute([
                'descripcion'=>$datos['descripcion'],
                'estatus'=>$datos['estatus']
                ]);
            
            return true;
       
        }
        catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                
        }
  
    }


    public function getUnidad(){
            $items=[];
            try{
                $query=$this->db->connect()->query("SELECT 
                id_unidad_curricular as id_unidad_curricular,
                unidad_curricular.descripcion as unidad_curricular,
                unidad_curricular.estatus as estatus
                FROM unidad_curricular");
                
                while($row=$query->fetch()){
                    
                    $item=new Unidad();
                    $item->id_unidad_curricular=$row['id_unidad_curricular'];
                    $item->descripcion=$row['descripcion'];
                    if($row['estatus'] == 1){
                        $item->estatus="Activo";
                    }elseif($row['estatus'] == 0){
                        $item->estatus="Inactivo";
                    }
                
                    array_push($items,$item);
                
                
                }
                return $items;
          
            }catch(PDOException $e){
            return[];
            }
          
    }

    public function update($datos){
            //var_dump($datos);
                  //var_dump($item['id_pais'],$item['estado'],$item['codigoine'],$item['id_estado']);
                  $query=$this->db->connect()->prepare("UPDATE unidad_curricular SET descripcion=:descripcion, estatus=:estatus
                   WHERE id_unidad_curricular=:id_unidad_curricular");
                  
                  try{
                      $query->execute([
                          'id_unidad_curricular'=>$datos['id_unidad_curricular'],
                          'descripcion'=>$datos['descripcion'],
                          'estatus'=>$datos['estatus']
                               ]);
                   return true;
          
                  }catch(PDOException $e){
                       return false;
                  }
                   
    }

    public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM unidad_curricular WHERE id_unidad_curricular = :id_unidad_curricular");
        
                try{
                $query->execute([
                    'id_unidad_curricular'=>$id,
                    ]);
                        return true;
        
                }catch(PDOException $e){
        return false;
    }
           
  }

}

?>