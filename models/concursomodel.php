<?php
    include_once 'models/estructura.php';
    include_once 'scripts/files.php';

    class ConcursoModel extends Model{
        public function __construct(){
           parent::__construct();
        }
        
        public function existe($id){
            
            try{

               
                $sql = $this->db->connect()->prepare("SELECT id_docente FROM concurso WHERE id_docente=:id_docente");
                $sql->execute(['id_docente'=>$id]);
                $nombre=$sql->fetch();
                
                if($id==$nombre['id_docente']){
                    
                    return $nombre['id_docente'];
                } 
                return false;



            } catch(PDOException $e){
                return false;
            }

        }

        public function insert($datos){
            try{
                
                
                //1. guardas el objeto pdo en una variable
                $pdo=$this->db->connect();

                //2. comienzas transaccion
                $pdo->beginTransaction();

                $doc=new Documento();

                $pdf_resolucion=$doc->getData($datos['pdf_resolucion']);
                //el mover no se esta ejecutando: revisar extensiones y permisos
                if($doc->mover($pdf_resolucion['extension'], 
                    'src/documentos/concursos/',
                    $pdf_resolucion['ruta_tmp'],
                    $pdf_resolucion['original_name'],
                    array('pdf'))
                    ){
                        
                        $doc_res=$pdo->prepare("INSERT INTO documento(
                            descripcion,
                            estatus,
                            id_persona,
                            id_tipo_documento) 
                            VALUES(
                                :descripcion,
                                :estatus, 
                                :id_persona,
                                (SELECT id_tipo_documento FROM tipo_documento WHERE descripcion='RESOLUCION_CONCURSO') 
                                );");
                        $doc_res->execute([
                            'descripcion'=>date('Y-m-d H:i:s').$pdf_resolucion['original_name'],
                            'estatus'=>'Sin Verificar',
                            'id_persona'=>$datos['id_persona']
                            ]);
                            
                        $concurso=$pdo->prepare("INSERT INTO concurso(
                            fecha_concurso, 
                            id_escalafon, 
                            id_docente, 
                            id_documento) VALUES(
                            :fecha_concurso,
                            :id_escalafon,
                            :id_docente,
                            (select id_documento from documento ORDER BY id_documento DESC LIMIT 1)
                            );");
                        $concurso->execute([
                            'fecha_concurso'=>$datos['fecha_concurso'],
                            'id_escalafon'=>$datos['id_escalafon'],
                            'id_docente'=>$datos['id_docente']
                            ]);
                } 
                //4. consignas la transaccion (en caso de que no suceda ningun fallo)
                $pdo->commit();
                return true; 
            } catch(PDOException $e){
                //5. regresas a un estado anterior en caso de error
                $pdo->rollBack();
                //echo "Fallo: " . $e->getMessage();
                return false;
            }
        }

// creamos el getbyId

public function getbyId($id){

         


    $items=[];
    $item=new Estructura();
    try{

        $query=$this->db->connect()->prepare("SELECT id_concurso,
        concurso.fecha_concurso,
        concurso.id_escalafon,
        escalafon.descripcion as escalafon,
                concurso.id_docente,
                concurso.id_documento,
                documento.descripcion as documento
                 from 
                 concurso, 
                 escalafon,
                 documento 
                 where 
                 concurso.id_escalafon=escalafon.id_escalafon 
                 and concurso.id_documento=documento.id_documento
                 and id_concurso=:id_concurso;");

        
                $query->execute(['id_concurso'=>$id]);

             while($row=$query->fetch()){
            
            
                $item->id_concurso=$row['id_concurso'];
                $item->fecha_concurso=date("m-d-Y", strtotime($row['fecha_concurso']));
                $item->id_escalafon=$row['id_escalafon'];
                $item->escalafon=$row['escalafon'];          
                $item->id_documento=$row['id_documento'];
                $item->id_docente=$row['id_docente'];
                $item->documento=$row['documento'];
        
        }


        //var_dump($item);

        return $item;
        
    }catch(PDOException $e){
         //5. regresas a un estado anterior en caso de error
         //$pdo->rollBack();
    return[];
    }
  
}





public function update($datos){
    

try{
    
    //1. guardas el objeto pdo en una variable
    $pdo=$this->db->connect();

    //2. comienzas transaccion
    $pdo->beginTransaction();



$query=$pdo->prepare("UPDATE concurso SET 
id_concurso=:id_concurso,
id_escalafon=:id_escalafon,
fecha_concurso=:fecha_concurso,
id_docente=:id_docente

 WHERE id_concurso=:id_concurso");
    $query->execute([
        'id_concurso'=>$datos['id_concurso'],
        'id_escalafon'=>$datos['id_escalafon'],
        'fecha_concurso'=>$datos['fecha_concurso'],
        'id_docente'=>$datos['id_docente']
       
       
        
      ]);
    
 
 //4. consignas la transaccion (en caso de que no suceda ningun fallo)
 $pdo->commit();
      return true;

}catch(PDOException $e){

     //5. regresas a un estado anterior en caso de error
     $pdo->rollBack();
     return false;
}
 
}









        public function get($id){
            $items=[];
            try{
                $query=$this->db->connect()->prepare("SELECT 
                id_concurso,
                fecha_concurso,
                concurso.id_escalafon,
                escalafon.descripcion as escalafon,
                id_docente,
                concurso.id_documento,
                documento.descripcion as documento
                 from 
                 concurso, 
                 escalafon,
                 documento 
                 where 
                 concurso.id_escalafon=escalafon.id_escalafon 
                 and concurso.id_documento=documento.id_documento
                 and id_docente=:id_docente");
                $query->execute(['id_docente'=>$id]);
                while($row=$query->fetch()){
                    $item=new Estructura();
                    
                    $item->id_concurso=$row['id_concurso'];
                    $item->fecha_concurso=$row['fecha_concurso'];
                    $item->id_escalafon=$row['id_escalafon'];
                    $item->escalafon=$row['escalafon'];          
                    $item->id_documento=$row['id_documento'];
                    $item->id_docente=$row['id_docente'];
                    $item->documento=$row['documento'];
                    
                    array_push($items,$item);
          
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
          
        }

        //////////////    funcion que llama datos de tablas catalogos   ///////////////
        public function getCatalogo($valor){
            $items=[];
            try{
                $query=$this->db->connect()->query("SELECT * FROM ".$valor."");
          
                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id=$row['id_'.$valor.''];
                    $item->descripcion=$row['descripcion'];
                    
                    array_push($items,$item);
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
        }


        public function delete($id){
            try{
                //var_dump($id);
                
                //1. guardas el objeto pdo en una variable
                $pdo=$this->db->connect();

                //2. comienzas transaccion
                $pdo->beginTransaction();

                $pdf=$pdo->prepare("SELECT 
                    t1.id_documento,
                    t2.descripcion
                    from 
                    concurso as t1,
                    documento as t2
                    where 
                    t1.id_documento=t2.id_documento
                    AND id_concurso=:id_concurso;");

                $pdf->execute(['id_concurso'=>$id]);
               
                $doc_concurso=$pdf->fetch(PDO::FETCH_ASSOC);

                $ruta='src/documentos/concursos/'.$doc_concurso['descripcion'];

                if(unlink($ruta)){
                    $concurso=$pdo->prepare("DELETE from concurso where id_concurso=:id_concurso;");
                    $concurso->execute(['id_concurso'=>$id]);
                    
                    $doc=$pdo->prepare("DELETE from documento where id_documento=:id_documento;");
                    $doc->execute(['id_documento'=>$doc_concurso['id_documento']]);

                }


                $pdo->commit();
                return true;

            } catch(PDOException $e){
                //5. regresas a un estado anterior en caso de error
                $pdo->rollBack();
                //echo "Fallo: " . $e->getMessage();
                return false;
            }
        }




    }
        
    

?>