<?php
include_once 'models/ingpropios.php';
class ArancelModel extends Model{
    public function __construct(){
    parent::__construct();
    }


    public function existe($codigo){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT codigo FROM arancel WHERE  codigo=:codigo");
            $sql->execute(['codigo' =>$codigo]);
            $nombre=$sql->fetch();
            
            if($codigo==$nombre['codigo']){
                return $nombre['codigo'];
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }



    public function insert($datos){
        //echo "<br>insertar datos";
    
        try{
       //  var_dump($datos['banco'],$datos['codigo'],$datos['estado']);
    

        //var_dump($datos['arancel']);
        //var_dump($datos['fechai']);
        //var_dump($datos['fechaf']);
        //var_dump($datos['estatus']);
        //var_dump($datos['cantidad']);
  //var_dump($datos['modo']);
    
                 $query=$this->db->connect()->prepare('INSERT INTO arancel (fecha_inicio,fecha_fin,descripcion,cantidad_ut, estatus, modo,codigo) 
                 VALUES (:fecha_inicio, :fecha_fin, :descripcion, :cantidad_ut, :estatus ,:modo,:codigo)');
    
                $query->execute(['fecha_inicio'=>$datos['fechai'],'fecha_fin'=>$datos['fechaf'],
                'descripcion'=>$datos['arancel'],'cantidad_ut'=>$datos['cantidad'],'estatus'=>$datos['estatus'],'modo'=>$datos['modo']
                ,'codigo'=>$datos['codigo']
                ]);
    
                //$query->execute(['fechai'=>$datos['fechai'],'fechaf'=>$datos['fechaf'],
               // 'arancel'=>$datos['arancel'],'cantidad'=>$datos['cantidad'],'estatus'=>$datos['estatus']]);
                return true;
          
        }catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                 }
        
        
                }

              public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT * from  arancel");
              
              while($row=$query->fetch()){
              $item=new ingpropios();
              $item->id_arancel=$row['id_arancel'];
              $item->arancel=$row['descripcion'];
              $item->fechai=$row['fecha_inicio'];
              $item->fechaf=$row['fecha_fin'];

              

              $item->estatus=$row['estatus'];
              $item->cantidad=$row['cantidad_ut'];
              $item->modo=$row['modo'];
              $item->codigo=$row['codigo'];
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }




              public function update($item){
   
           // var_dump($item['id_arancel2']);

           // var_dump($item['arancel2']);

           // var_dump( $item['fechai2']);

           // var_dump($item['fechaf2']);

           // var_dump($item['estatus2']);

           // var_dump($item['cantidad2']);

           // var_dump( $item['modo2']);
            
           // var_dump( $item['codigo2']);
            
                      
            
                     $query=$this->db->connect()->prepare("UPDATE arancel SET descripcion= :descripcion,codigo= :codigo,
                     fecha_inicio= :fecha_inicio ,fecha_fin= :fecha_fin , cantidad_ut = :cantidad_ut , modo= :modo, estatus= :estatus  WHERE id_arancel= :id_arancel");
                 try{
                             $query->execute([
                                'id_arancel'=>$item['id_arancel2'],
                             'descripcion'=>$item['arancel2'],
                             'fecha_inicio'=>$item['fechai2'],          
                             'fecha_fin'=>$item['fechaf2'],    
                             'estatus'=>$item['estatus2'],
                             'cantidad_ut'=>$item['cantidad2'],
                             'modo'=>$item['modo2'],
                             'codigo'=>$item['codigo2'],
                             
                             ]);
                 return true;
                            
                             
                 }catch(PDOException $e){
                     return false;
                      }
                 
                     }




              



                  


    }

    ?>