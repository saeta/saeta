<?php
//ini_set("display_errors", 1);
include_once 'datosacademicos/diseno_uc_docente.php';
include_once 'datosacademicos/tramo.php';
include_once 'datosacademicos/tipouc.php';
include_once 'datosacademicos/programa_formacion.php';
include_once 'datosacademicos/areaubv.php';
include_once 'datosacademicos/docente.php';
include_once 'models/estructura.php';
include_once 'scripts/files.php';

class Diseno_ucModel extends Model{
    public function __construct(){
        parent::__construct();
    }

    public function existe($diseno_uc_docente){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT nombre FROM diseno_uc_docente WHERE  nombre=:nombre");
            $sql->execute(['nombre' =>$diseno_uc_docente]);
            $nombre=$sql->fetch();
            
            if($diseno_uc_docente==$nombre['nombre']){
                return $nombre['nombre'];
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }

     //////////////    funcion que llama datos de tablas catalogos   ///////////////
     public function getCatalogo($valor){
        $items=[];
        try{

            $query=$this->db->connect()->query("SELECT * FROM ".$valor."");
      
            while($row=$query->fetch()){
                $item=new Estructura();
               $item->id=$row['id_'.$valor.''];
                $item->descripcion=$row['descripcion'];
                        
                array_push($items,$item);
            }
            return $items;
      
        }catch(PDOException $e){
            return[];
        }
      
    }


    public function insert($datos){
        //echo "<br>insertar datos";
        //var_dump($datos);
        try{

              //1. guardas el objeto pdo en una variable
              $pdo=$this->db->connect();

                //2. comienzas transaccion
                $pdo->beginTransaction();


            $doc=new Documento();
            $pdf_documento=$doc->getData($datos['pdf_diseno']);
            //var_dump($pdf_documento);
            $band=false;
            //el mover no se esta ejecutando: revisar extensiones y permisos
            if($doc->mover($pdf_documento['extension'], 
                'src/documentos/diseno/',
                $pdf_documento['ruta_tmp'],
                $pdf_documento['original_name'],
                array('pdf'))
            )

            {
                $band=true;
                $doc_documento=$pdo->prepare("INSERT INTO documento(
                    descripcion,
                    estatus,
                    id_persona,
                    id_tipo_documento) 
                    VALUES(
                        :descripcion,
                        :estatus, 
                        :id_persona,
                        (SELECT id_tipo_documento FROM tipo_documento WHERE descripcion='Certificado Diseño UC') 
                        );");

                $doc_documento->execute([
                    'descripcion'=>date('Y-m-d H:i:s').$pdf_documento['original_name'],
                    'estatus'=>'Sin Verificar',
                    'id_persona'=>$datos['id_persona']
                    ]);
            

            $query=$pdo->prepare('INSERT INTO diseno_uc_docente(
                nombre,
                ano_creacion,
                id_tramo,
                id_unidad_curricular_tipo,
                id_programa,
                id_area_conocimiento_ubv,
                id_documento,
                id_docente,
                estatus_verificacion) VALUES(:nombre,
                                :ano_creacion,
                                :id_tramo, 
                                :id_unidad_curricular_tipo, 
                                :id_programa,
                                :id_area_conocimiento_ubv,
                                (SELECT id_documento FROM documento ORDER BY id_documento DESC LIMIT 1),
                                :id_docente,
                                :estatus_verificacion)');
           
            $query->execute([
                'nombre'=>$datos['nombre'],
                'ano_creacion'=>$datos['ano_creacion'],
                'id_tramo'=>$datos['id_tramo'],
                'id_unidad_curricular_tipo'=>$datos['id_unidad_curricular_tipo'],
                'id_programa'=>$datos['id_programa'],
                'id_area_conocimiento_ubv'=>$datos['id_area_conocimiento_ubv'],
                'id_docente'=>$datos['id_docente'],
                'estatus_verificacion'=>'Sin Verificar',
                ]);
            
            }        

            //4. consignas la transaccion (en caso de que no suceda ningun fallo)
            $pdo->commit();
            // var_dump($pdo->inTransaction());
                   return true;
             
           }catch(PDOException $e){
               //echo "Matricula duplicada";
                //5. regresas a un estado anterior en caso de error
               $pdo->rollBack();
               return false;
                    }
           
       }

    public function getTramo(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT id_tramo,descripcion,codigo 
            FROM tramo");
            
            while($row=$query->fetch()){
                
                $item=new Tramo();
                $item->id_tramo=$row['id_tramo'];
                $item->descripcion=$row['descripcion'];
                $item->codigo=$row['codigo'];
            
                array_push($items,$item);
            
            
            }
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
}

public function getTipo(){
    $items=[];
    try{
        $query=$this->db->connect()->query("SELECT 
        id_unidad_curricular_tipo,descripcion
        FROM unidad_curricular_tipo");
    
        while($row=$query->fetch()){
        
            $item=new TipoUC();
            $item->id_unidad_curricular_tipo=$row['id_unidad_curricular_tipo'];
            $item->descripcion=$row['descripcion'];
           
    
            array_push($items,$item);
    
    
        }
        return $items;

    }catch(PDOException $e){
    return[];
}

}

public function getProgramaF(){
    $items=[];
    try{
        $query=$this->db->connect()->query("SELECT 
        id_programa,descripcion,codigo,id_centro_estudio,id_programa_tipo,id_nivel_academico,id_linea_investigacion,estatus
        FROM programa");
    
        while($row=$query->fetch()){
        
            $item=new Programa();
            $item->id_programa=$row['id_programa'];
            $item->descripcion=$row['descripcion'];
            $item->codigo=$row['codigo'];
            $item->id_centro_estudio=$row['id_centro_estudio'];
            $item->id_programa_tipo=$row['id_programa_tipo'];
            $item->id_nivel_academico=$row['id_nivel_academico'];
            $item->id_linea_investigacion=$row['id_linea_investigacion'];
            $item->estatus=$row['estatus'];
    
            array_push($items,$item);
    
    
        }
        return $items;

    }catch(PDOException $e){
    return[];
}

}



public function getUbv(){
    $items=[];
    try{
        $query=$this->db->connect()->query("SELECT 
        id_area_conocimiento_ubv,
        descripcion,
        codigo,
        estatus,
        id_area_conocimiento_opsu
        FROM area_conocimiento_ubv");
    
        while($row=$query->fetch()){
        
            $item=new AreaUbv();
            $item->id_area_conocimiento_ubv=$row['id_area_conocimiento_ubv'];
            $item->descripcion=$row['descripcion'];
            $item->codigo=$row['codigo'];
            $item->estatus=$row['estatus'];
            $item->id_area_conocimiento_opsu=$row['id_area_conocimiento_opsu'];
            
            
            
    
            array_push($items,$item);
    
    
        }
        return $items;

    }catch(PDOException $e){
    return[];
}

}




public function getDocente(){
    $items=[];
    try{

             //1. guardas el objeto pdo en una variable
             $pdo=$this->db->connect();

             //2. comienzas transaccion
             $pdo->beginTransaction();
        $query=$pdo->query("SELECT 
        docente.id_persona, 
        id_docente,
        identificacion, 
        primer_nombre, 
        primer_apellido
        FROM 
        persona, 
        persona_nacionalidad,
        docente
        WHERE docente.id_persona=persona.id_persona 
        AND persona_nacionalidad.id_persona=persona.id_persona");
        
        while($row=$query->fetch()){
            
            $item=new Docente();
            $item->id_docente=$row['id_docente'];
            $item->id_persona=$row['id_persona'];
            $item->identificacion=$row['identificacion'];
            $item->primer_nombre=$row['primer_nombre'];
            $item->primer_apellido=$row['primer_apellido'];

            array_push($items,$item);
        
        
        }
        //var_dump($items);

         //4. consignas la transaccion (en caso de que no suceda ningun fallo)
     $pdo->commit();
        return $items;
  
    }catch(PDOException $e){

         //5. regresas a un estado anterior en caso de error
         $pdo->rollBack();
    return[];
    }
  
}

public function getDiseno($identificacion){
    $items=[];
    try{
        //var_dump('model',$identificacion);
        $query=$this->db->connect()->prepare("SELECT 
        identificacion,
        primer_nombre,
        primer_apellido,
                id_diseno_uc as id_diseno_uc,
                diseno_uc_docente.nombre AS diseno_uc,
                diseno_uc_docente.ano_creacion as ano_creacion,
                diseno_uc_docente.id_tramo as id_tramo,
                tramo.descripcion AS tramo,
                diseno_uc_docente.id_unidad_curricular_tipo as id_unidad_curricular_tipo,
                unidad_curricular_tipo.descripcion AS tipo_uc,
                diseno_uc_docente.id_programa as id_programa,
                programa.descripcion AS programa,
                diseno_uc_docente.id_area_conocimiento_ubv as id_area_conocimiento_ubv,
                area_conocimiento_ubv.descripcion AS area_ubv,
                diseno_uc_docente.id_documento as id_documento,
                documento.descripcion AS documento,
                diseno_uc_docente.id_docente as id_docente,
                diseno_uc_docente.estatus_verificacion as estatus_verificacion
                FROM diseno_uc_docente, 
                tramo, unidad_curricular_tipo, programa, 
                area_conocimiento_ubv, documento,docente, 
                persona, persona_nacionalidad
                WHERE diseno_uc_docente.id_tramo=tramo.id_tramo
                and diseno_uc_docente.id_unidad_curricular_tipo=unidad_curricular_tipo.id_unidad_curricular_tipo
                and diseno_uc_docente.id_programa=programa.id_programa
                and diseno_uc_docente.id_area_conocimiento_ubv=area_conocimiento_ubv.id_area_conocimiento_ubv
                and diseno_uc_docente.id_documento=documento.id_documento
                and diseno_uc_docente.id_docente=docente.id_docente 
                and docente.id_persona=persona.id_persona 
                and persona_nacionalidad.id_persona=persona.id_persona 
                and identificacion=:identificacion");

$query->execute(['identificacion'=>$identificacion]);
        
        while($row=$query->fetch()){
            
            $item=new Diseno();
            $item->id_diseno_uc=$row['id_diseno_uc'];
            $item->nombre=$row['diseno_uc'];
            $item->ano_creacion=$row['ano_creacion'];
            $item->id_tramo=$row['id_tramo'];
            $item->descripcion_tramo=$row['tramo'];
            $item->id_unidad_curricular_tipo=$row['id_unidad_curricular_tipo'];
            $item->descripcion_tipo_uc=$row['tipo_uc'];
            $item->id_programa=$row['id_programa'];
            $item->descripcion_programa=$row['programa'];
            $item->id_area_conocimiento_ubv=$row['id_area_conocimiento_ubv'];
            $item->descripcion_area_ubv=$row['area_ubv'];
            $item->id_documento=$row['id_documento'];
            $item->descripcion_documento=$row['documento'];
            
            //$item->estatus_verificacion=$row['estatus_verificacion'];
            if($row['estatus_verificacion'] == 'Verificado'){
                $item->estatus_verificacion="Verificado";
            }elseif($row['estatus_verificacion'] == 'Sin Verificar'){
                $item->estatus_verificacion="Sin Verificar";
            }
            //var_dump($item->estatus_verificacion);
        
            array_push($items,$item);
        
        
        }
        //var_dump($items);
        return $items;
  
    }catch(PDOException $e){
    return[];
    }
  
}

public function getbyId($id){
       
    try{
        //var_dump('model',$id);
        $item=new Diseno(); 
        $sql=$this->db->connect()->prepare("SELECT 
        identificacion,
        primer_nombre,
        primer_apellido,
                id_diseno_uc as id_diseno_uc,
                diseno_uc_docente.nombre AS diseno_uc,
                diseno_uc_docente.ano_creacion as ano_creacion,
                diseno_uc_docente.id_tramo as id_tramo,
                tramo.descripcion AS tramo,
                diseno_uc_docente.id_unidad_curricular_tipo as id_unidad_curricular_tipo,
                unidad_curricular_tipo.descripcion AS tipo_uc,
                diseno_uc_docente.id_programa as id_programa,
                programa.descripcion AS programa,
                diseno_uc_docente.id_area_conocimiento_ubv as id_area_conocimiento_ubv,
                area_conocimiento_ubv.descripcion AS area_ubv,
                diseno_uc_docente.id_documento as id_documento,
                documento.descripcion AS documento,
                diseno_uc_docente.id_docente as id_docente,
                diseno_uc_docente.estatus_verificacion as estatus_verificacion
                FROM diseno_uc_docente, tramo, unidad_curricular_tipo, programa, area_conocimiento_ubv, documento,docente, persona, persona_nacionalidad
                WHERE diseno_uc_docente.id_tramo=tramo.id_tramo
                and diseno_uc_docente.id_unidad_curricular_tipo=unidad_curricular_tipo.id_unidad_curricular_tipo
                and diseno_uc_docente.id_programa=programa.id_programa
                and diseno_uc_docente.id_area_conocimiento_ubv=area_conocimiento_ubv.id_area_conocimiento_ubv
                and diseno_uc_docente.id_documento=documento.id_documento
                and diseno_uc_docente.id_docente=docente.id_docente 
                and docente.id_persona=persona.id_persona 
                and persona_nacionalidad.id_persona=persona.id_persona 
                and id_diseno_uc=:id_diseno_uc;");
                $sql->execute(['id_diseno_uc'=>$id]);
        
                   
        while($row=$sql->fetch()){
            
            
            $item->id_diseno_uc=$row['id_diseno_uc'];
            $item->nombre=$row['diseno_uc'];
            $item->ano_creacion=$row['ano_creacion'];
            $item->id_tramo=$row['id_tramo'];

            $item->descripcion_tramo=$row['tramo'];
            
            $item->id_unidad_curricular_tipo=$row['id_unidad_curricular_tipo'];
            $item->descripcion_tipo_uc=$row['tipo_uc'];
            $item->id_programa=$row['id_programa'];
            $item->descripcion_programa=$row['programa'];
            $item->id_area_conocimiento_ubv=$row['id_area_conocimiento_ubv'];
            $item->descripcion_area_ubv=$row['area_ubv'];
            $item->id_documento=$row['id_documento'];
            $item->descripcion_documento=$row['documento'];
            $item->id_docente=$row['id_docente'];
            
            //$item->estatus_verificacion=$row['estatus_verificacion'];
            if($row['estatus_verificacion'] == 'Verificado'){
                $item->estatus_verificacion="Verificado";
            }elseif($row['estatus_verificacion'] == 'Sin Verificar'){
                $item->estatus_verificacion="Sin Verificar";
            }
            //var_dump($item->estatus_verificacion);
        
            //array_push($items,$item);
        
        
        }
        return $item;
  
    }catch(PDOException $e){
    return[];
    }
  
}



public function update($datos){
        //var_dump($datos);
              $query=$this->db->connect()->prepare("UPDATE diseno_uc_docente 
              SET nombre=:nombre,
              ano_creacion=:ano_creacion,
              id_tramo=:id_tramo,
              id_unidad_curricular_tipo=:id_unidad_curricular_tipo,
              id_programa=:id_programa,
              id_area_conocimiento_ubv=:id_area_conocimiento_ubv,
              estatus_verificacion=:estatus_verificacion
             
              WHERE id_diseno_uc=:id_diseno_uc");
              
              try{
                  $query->execute([
                      'id_diseno_uc'=>$datos['id_diseno_uc'],
                      'nombre'=>$datos['nombre'],
                      'ano_creacion'=>$datos['ano_creacion'],
                      'id_tramo'=>$datos['id_tramo'],
                      'id_unidad_curricular_tipo'=>$datos['id_unidad_curricular_tipo'],
                      'id_programa'=>$datos['id_programa'],
                      'id_area_conocimiento_ubv'=>$datos['id_area_conocimiento_ubv'],
                      'estatus_verificacion'=>'Sin Verificar'
                      
                      
                        
                           ]);
               return true;
      
              }catch(PDOException $e){
                   return false;
              }
               
}

public function delete($id){
        $query=$this->db->connect()->prepare("DELETE FROM diseno_uc_docente WHERE id_diseno_uc = :id_diseno_uc");
    
            try{
            $query->execute([
                'id_diseno_uc'=>$id,
                ]);
                    return true;
    
            }catch(PDOException $e){
    return false;
}
       
}

}

?>