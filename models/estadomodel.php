<?php
include_once 'models/estructura.php';
class EstadoModel extends Model{
    public function __construct(){
    parent::__construct();
    }
    public function existe($estado){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM estado WHERE descripcion=:descripcion");
            $sql->execute(['descripcion' =>$estado]);
            $nombre=$sql->fetch();
            
            if($estado==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
    //echo "<br>insertar datos";

    try{
     
             $query=$this->db->connect()->prepare('INSERT INTO estado(descripcion, codigo,id_pais) VALUES
             (:descripcion, :codigo, :id_pais)');

            $query->execute(['descripcion'=>$datos['estado'],'codigo'=>$datos['codigoine'],
            'id_pais'=>$datos['id_pais']]);

            return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }
    
    
            }

            public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT id_estado,estado.descripcion AS estado,estado.codigo,pais.id_pais,pais.descripcion AS pais ,pais.estatus AS estatus_pais FROM estado,pais WHERE estado.id_pais=pais.id_pais");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_estado=$row['id_estado'];
              $item->estado=$row['estado'];
              $item->codigo=$row['codigo'];
              $item->id_pais=$row['id_pais'];
              $item->pais=$row['pais'];           
              $item->estatus_pais=$row['estatus_pais'];

            
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }
              public function getPais(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT * FROM pais WHERE estatus='true'");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_pais=$row['id_pais'];
              $item->descripcion=$row['descripcion'];
              $item->codigo=$row['codigo'];
              $item->siglas=$row['siglas'];
              $item->estatus=$row['estatus'];
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }
/*
public function getbyId($id){
    $item = new Publicacion();

    $query = $this->db->connect()->prepare("SELECT * FROM publicacion WHERE id_publicacion = :id_publicacion");
    
    try{
        $query ->execute(['id_publicacion'=>$id]);

        while($row = $query->fetch()){
            $item->titulo = $row['titulo'];
            $item->extracto = $row['extracto'];
            $item->categoria = $row['categoria'];

        }
        return $item;
    }catch(PDOExcepton $e){
        return null;
    }
}*/


public function update($item){
   
    //var_dump($item['id_pais'],$item['estado'],$item['codigoine'],$item['id_estado']);

         $query=$this->db->connect()->prepare("UPDATE estado SET descripcion= :descripcion,codigo= :codigo,id_pais=:id_pais WHERE id_estado= :id_estado");
     try{
                 $query->execute([
                 'descripcion'=>$item['estado'],
                 'codigo'=>$item['codigoine'],          
                 'id_estado'=>$item['id_estado'],
                 'id_pais'=>$item['id_pais'],
                 
                 ]);
     return true;
                
                 
     }catch(PDOException $e){
         return false;
          }
     
         }




    }

    ?>