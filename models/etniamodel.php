<?php
include_once 'models/persona.php';
class EtniaModel extends Model{
    public function __construct(){
    parent::__construct();
    }
    public function existe($etnia){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM etnia WHERE descripcion=:descripcion");
            $sql->execute(['descripcion' =>$etnia]);
            $nombre=$sql->fetch();
            
            if($etnia==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
    //echo "<br>insertar datos";

    try{
     
             $query=$this->db->connect()->prepare('INSERT INTO etnia (descripcion) VALUES
             (:descripcion)');

            $query->execute(['descripcion'=>$datos['etnia']]);

            return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }
    
    
            }

            public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT * FROM etnia");
              
              while($row=$query->fetch()){
              $item=new Persona();
              $item->id_etnia=$row['id_etnia'];
              $item->descripcion=$row['descripcion'];
              
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }


              public function update($item){
   
        //var_dump($item['id_etnia2']);
        //var_dump($item['etnia2']);

                $query=$this->db->connect()->prepare("UPDATE etnia SET descripcion= :descripcion WHERE id_etnia= :id_etnia");
            try{
                        $query->execute([
                        'descripcion'=>$item['etnia2'],
                        'id_etnia'=>$item['id_etnia2'],
  
                        
                        ]);
            return true;
                       
                        
            }catch(PDOException $e){
                return false;
                 }
            
                }
                

                public function delete($id_etnia){
                    $query=$this->db->connect()->prepare("DELETE FROM etnia WHERE id_etnia=:id_etnia ");
                
                        try{
                        $query->execute([
                            'id_etnia'=>$id_etnia,
                            ]);
                                return true;
                
                        }catch(PDOException $e){
                return false;
                }
                
                
                    }





    }

    ?>