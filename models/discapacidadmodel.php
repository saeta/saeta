<?php
include_once 'models/persona.php';
class DiscapacidadModel extends Model{
    public function __construct(){
    parent::__construct();
    }




    public function existe($discapacidad){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM discapacidad WHERE descripcion=:descripcion");
            $sql->execute(['descripcion' =>$discapacidad]);
            $nombre=$sql->fetch();
            
            if($discapacidad==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }

    public function insert($datos){
    //echo "<br>insertar datos";
    try{
        $query=$this->db->connect()->query("SELECT * FROM discapacidad");
        while($row=$query->fetch()){
            $item=new Persona();
            $item->id_discapacidad=$row['id_tipo_discapacidad'];
           
        }
        
        $query=$this->db->connect()->prepare('INSERT INTO discapacidad (descripcion,id_tipo_discapacidad) VALUES
             (:descripcion,:id_tipo_discapacidad)');

            $query->execute(['descripcion'=>$datos['discapacidad'],'id_tipo_discapacidad'=>$datos['id_discapacidad']]);
          
            return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }
    
    
            }
        
            public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT discapacidad.id_tipo_discapacidad,discapacidad.id_discapacidad,discapacidad.descripcion as discapacidad , tipo_discapacidad.descripcion as tipo_discapacidad FROM discapacidad,tipo_discapacidad  WHERE discapacidad.id_tipo_discapacidad=tipo_discapacidad.id_tipo_discapacidad ");
              
              while($row=$query->fetch()){
              $item=new persona();
              $item->id_discapacidad=$row['id_discapacidad'];
             // var_dump($item->id_discapacidad=$row['id_discapacidad']);
              $item->descripcion=$row['discapacidad'];
              $item->descripcion_n=$row['tipo_discapacidad'];
              $item->id_tipo_discapacidad=$row['id_tipo_discapacidad'];
              array_push($items,$item);
              
              }
              return $items;
              }catch(PDOException $e){
              return[];
              }
              
              }
              

              public function geto(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT  * FROM tipo_discapacidad");
              
              while($row=$query->fetch()){
              $item=new Persona();
              $item->id_tipo_discapacidad=$row['id_tipo_discapacidad'];
              $item->descripcion=$row['descripcion'];
              array_push($items,$item);
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }



              public function update($item){
              //  var_dump($item['discapacidad2']);
                //var_dump($item['id_discapacidad2']);

             
                     $query=$this->db->connect()->prepare("UPDATE discapacidad SET descripcion=:descripcion WHERE id_discapacidad =:id_discapacidad");
                 try{
                $query->execute(['descripcion'=>$item['discapacidad2'],'id_discapacidad'=>$item['id_discapacidad2'],]);
                 return true;
                            
                             
                 }catch(PDOException $e){
                     return false;
                      }
                 
                     }

                 public function delete($id_discapacidad){
                        $query=$this->db->connect()->prepare("DELETE FROM discapacidad WHERE id_discapacidad=:id_discapacidad ");
                    
                            try{
                            $query->execute([
                                'id_discapacidad'=>$id_discapacidad,
                                ]);
                                    return true;
                    
                            }catch(PDOException $e){
                    return false;
                    }
                    
                    
                        }


                        



    }

    ?>