<?php
include_once 'datosacademicos/nucleo_academico.php';
include_once 'datosacademicos/lineainvestigacion.php';

class Linea_investigacionModel extends Model{
    public function __construct(){
        parent::__construct();
    }


    public function existe($linea_investigacion){
        try{
    
            //var_dump($linea_investigacion);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM linea_investigacion WHERE descripcion=:descripcion");
            $sql->execute(['descripcion' =>$linea_investigacion]);
            $nombre=$sql->fetch();
            if($linea_investigacion==$nombre['descripcion']){
                
                return $nombre['descripcion'];
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }

    public function insert($datos){
        //echo "<br>insertar datos";
    
        try{
         //var_dump($datos);
            $query=$this->db->connect()->prepare('INSERT INTO linea_investigacion(
                descripcion,
                codigo,
                especificacion, 
                id_nucleo_academico
                ) VALUES(
                    :descripcion,
                    :codigo,
                    :especificacion,
                    :id_nucleo_academico
                    )');
    
                $query->execute([
                    'descripcion'=>$datos['descripcion'],
                    'id_nucleo_academico'=>$datos['id_nucleo_academico'],
                    'codigo'=>$datos['codigo'],
                    'especificacion'=>$datos['especificacion']

                ]);
    
                return true;
          
        }catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                 }
        
        
    }

    public function getNucleo(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT id_nucleo_academico as id_nucleo, nucleo_academico.descripcion AS nucleo,  nucleo_academico.id_centro_estudio as id_centro, centro_estudio.descripcion AS centro FROM nucleo_academico, centro_estudio WHERE  nucleo_academico.id_centro_estudio=centro_estudio.id_centro_estudio");
            
            while($row=$query->fetch()){
                    
                $item=new Nucleo();
                $item->id_nucleo_academico=$row['id_nucleo'];
                $item->descripcion=$row['nucleo'];
                $item->id_centro_estudio=$row['id_centro'];
                $item->descripcion_centro=$row['centro'];
            
                array_push($items,$item);
            
            
            }

            //var_dump($items);
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }

    public function getLinea(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT 
            id_linea_investigacion as id_linea,
             linea_investigacion.descripcion AS linea_investigacion,
              linea_investigacion.codigo as codigo, 
              linea_investigacion.especificacion, 
              linea_investigacion.id_nucleo_academico as id_nucleo,
               nucleo_academico.descripcion AS nucleo 
               FROM linea_investigacion, nucleo_academico WHERE linea_investigacion.id_nucleo_academico=nucleo_academico.id_nucleo_academico");
            
            while($row=$query->fetch()){
                
                $item=new LineaInvestigacion();
                $item->id_linea_investigacion=$row['id_linea'];
                $item->descripcion=$row['linea_investigacion'];
                $item->codigo=$row['codigo'];
                $item->especificacion=$row['especificacion'];
               
                //var_dump($item->especificacion);
                $item->id_nucleo_academico=$row['id_nucleo'];
                $item->descripcion_nucleo=$row['nucleo'];

            
                array_push($items,$item);
            
            
            }

            //var_dump($items);

            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }
      
    public function update($datos){
        //var_dump($datos);
        //var_dump($item['id_pais'],$item['estado'],$item['codigoine'],$item['id_estado']);
        $query=$this->db->connect()->prepare("UPDATE linea_investigacion 
        SET descripcion=:descripcion,
        codigo=:codigo,
        especificacion=:especificacion,
        id_nucleo_academico=:id_nucleo_academico
         WHERE id_linea_investigacion=:id_linea_investigacion");
        
        try{
            $query->execute([
                'id_linea_investigacion'=>$datos['id_linea_investigacion'],
                'descripcion'=>$datos['descripcion'],
                'id_nucleo_academico'=>$datos['id_nucleo_academico'],
                'codigo'=>$datos['codigo'],
                'especificacion'=>$datos['especificacion']    
                     ]);
         return true;

        }catch(PDOException $e){
             return false;
        }
         
    }

        public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM linea_investigacion WHERE id_linea_investigacion = :id_linea_investigacion");
    
            try{
                $query->execute([
                'id_linea_investigacion'=>$id,
                ]);
                return true;
            }catch(PDOException $e){
                return false;
            }
        }
    }


    ?>