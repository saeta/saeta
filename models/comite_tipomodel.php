<?php
include_once 'datosacademicos/comitetipo.php';

class Comite_tipoModel extends Model{
    public function __construct(){
        parent::__construct();
    }


    public function existe($comite_tipo){
        try{
    
            //var_dump($comite_tipo);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM comite_tipo WHERE descripcion=:descripcion");
            $sql->execute(['descripcion' =>$comite_tipo]);
            $nombre=$sql->fetch();
            
            if($comite_tipo==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }

    public function insert($datos){
        //echo "<br>insertar datos";
    //var_dump($datos);
        try{
         
            $query=$this->db->connect()->prepare('INSERT INTO comite_tipo(descripcion, estatus) VALUES(:descripcion, :estatus)');
    
                $query->execute([
                    'descripcion'=>$datos['descripcion'],
                    'estatus'=>$datos['estatus']
                ]);
    
                return true;
          
        }catch(PDOException $e){
            return false;
                 }
        
        
    }

    

    public function get(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT * FROM comite_tipo");
            
            while($row=$query->fetch()){
                
                $item=new ComiteTipo();
                $item->id_comite_tipo=$row['id_comite_tipo'];
                $item->descripcion=$row['descripcion'];
                $item->estatus=$row['estatus'];
                
                if($row['estatus'] == true){
                    $item->estatus="Activo";
                }elseif($row['estatus'] == false){
                    $item->estatus="Inactivo";
                }

                array_push($items,$item);
            
            
            }
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }
      
    public function update($datos){
   //var_dump($datos);
        //var_dump($item['id_pais'],$item['estado'],$item['codigoine'],$item['id_estado']);
        $query=$this->db->connect()->prepare("UPDATE comite_tipo SET descripcion=:descripcion, estatus=:estatus
        WHERE id_comite_tipo=:id_comite_tipo");
        
        try{
            $query->execute([
                'id_comite_tipo'=>$datos['id_comite_tipo'],
                'descripcion'=>$datos['descripcion'],
                'estatus'=>$datos['estatus']     
                     ]);
         return true;

        }catch(PDOException $e){
             return false;
        }
         
    }

        public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM comite_tipo WHERE id_comite_tipo = :id_comite_tipo");
    
            try{
                $query->execute([
                'id_comite_tipo'=>$id
                ]);
                return true;
            }catch(PDOException $e){
                return false;
            }
        }
    }


    ?>