<?php
include_once 'datosacademicos/nivel_academico.php';

class Nivel_academicoModel extends Model{
    public function __construct(){
        parent::__construct();
    }


    public function existe($nivel_academico){
        try{
    
            //var_dump($nivel_academico);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM nivel_academico WHERE descripcion=:descripcion");
            $sql->execute(['descripcion' =>$nivel_academico]);
            $nombre=$sql->fetch();
            
            if($nivel_academico==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }

    public function insert($datos){
        //echo "<br>insertar datos";
        //var_dump($datos);
        try{
         
            $query=$this->db->connect()->prepare('INSERT INTO nivel_academico(
                descripcion, 
                codigo_ine) VALUES(
                    :descripcion, 
                    :codigo_ine)');
    
                $query->execute([
                    'descripcion'=>$datos['descripcion'],
                    'codigo_ine'=>$datos['codigo_ine']
                ]);
    
                return true;
          
        }catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                 }
        
        
    }

    public function get(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT id_nivel_academico, descripcion, codigo_ine  FROM nivel_academico");
            
            while($row=$query->fetch()){
                
                $item=new NivelAcademico();
                $item->id_nivel_academico=$row['id_nivel_academico'];
                $item->descripcion=$row['descripcion'];
                $item->codigo_ine=$row['codigo_ine'];
            
                array_push($items,$item);
            
            
            }
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }

    
      
    public function update($datos){
  
  //var_dump($datos);
        //var_dump($item['id_pais'],$item['estado'],$item['codigo_ineine'],$item['id_estado']);
        $query=$this->db->connect()->prepare("UPDATE nivel_academico SET descripcion=:descripcion, 
        codigo_ine=:codigo_ine WHERE id_nivel_academico=:id_nivel_academico");
        
        try{
            $query->execute([
                'id_nivel_academico'=>$datos['id_nivel_academico'],
                'descripcion'=>$datos['descripcion'],
                'id_nivel_academico'=>$datos['id_nivel_academico'],
                'codigo_ine'=>$datos['codigo_ine']          
                     ]);
         return true;

        }catch(PDOException $e){
             return false;
        }
         
    }

        public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM nivel_academico WHERE id_nivel_academico = :id_nivel_academico");
    
            try{
                $query->execute([
                'id_nivel_academico'=>$id,
                ]);
                return true;
            }catch(PDOException $e){
                return false;
            }
        }
    }


    ?>