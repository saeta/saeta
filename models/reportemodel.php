<?php
include_once 'models/persona.php';
class ReporteModel extends Model{
    public function __construct(){
    parent::__construct();
    }


            
          
              public function get_total_docente(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT COUNT(*) as total_docente from  nomina_docente_sigad ");
         
              $row=$query->fetch(PDO::FETCH_ASSOC);
              return $row["total_docente"];

              }catch(PDOException $e){
              return[];
              }
              
              }

              public function get_total_docente_censados(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT COUNT(*) as censados from  docente,docente_estatus,persona,censo where 
              docente.id_docente_estatus=docente_estatus.id_docente_estatus and persona.id_persona=docente.id_persona and censo.id_persona=docente.id_persona and censo.estatus='1'  and docente.id_docente_estatus='1'");
         
              $row=$query->fetch(PDO::FETCH_ASSOC);
              return $row["censados"];

              }catch(PDOException $e){
              return[];
              }
              
              }

              public function get_total_centro_estudio(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT centro_estudio.descripcion as centro,COUNT(*) as cant from docente,centro_estudio,censo where docente.id_centro_estudio=centro_estudio.id_centro_estudio and censo.id_persona=docente.id_persona and docente.id_docente_estatus='1' GROUP BY centro_estudio.id_centro_estudio ");
              
              while($row=$query->fetch()){
              $item=new Persona();
              $item->centro=$row['centro'];
              $item->cant=$row['cant'];

              array_push($items,$item);
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }


              public function get_total_eje_regional(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT eje_regional.descripcion as eje,COUNT(*) as cant from docente_estatus,docente,eje_municipal,censo,eje_regional where docente.id_docente_estatus=docente_estatus.id_docente_estatus and docente.id_eje_municipal=eje_municipal.id_eje_municipal and eje_regional.id_eje_regional=eje_municipal.id_eje_regional  and censo.id_persona=docente.id_persona and docente.id_docente_estatus='1' GROUP BY eje_regional.id_eje_regional,eje_municipal.descripcion");
              
              while($row=$query->fetch()){
              $item=new Persona();
              $item->eje=$row['eje'];
              $item->cant=$row['cant'];

              array_push($items,$item);
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }

              public function get_sede(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT DISTINCT(estado_nacimiento)as nombre FROM nomina_docente_sigad");
              
              while($row=$query->fetch()){
              $item=new Persona();
              $item->nombre=$row['nombre'];
              array_push($items,$item);
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }




              public function get_sede_estado($nombre){
               // var_dump($nombre);
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT centro_estudio.descripcion as centro_e,count(*) as total FROM 
              nomina_docente_sigad,
              persona_nacionalidad,
              docente,
              persona,
              centro_estudio,
              censo
               where 
              persona_nacionalidad.identificacion=nomina_docente_sigad.cedula and 
              persona.id_persona=persona_nacionalidad.id_persona and
              docente.id_persona=persona.id_persona and
              centro_estudio.id_centro_estudio=docente.id_centro_estudio and
              censo.id_persona=persona.id_persona and
              estado_nacimiento='$nombre' GROUP BY centro_estudio.descripcion");
              
                


              while($row=$query->fetch()){
              $item=new Persona();
              $item->centro_e=$row['centro_e'];
              $item->total=$row['total'];
              $item->nombre=$nombre;

              array_push($items,$item);
              }

                if($item->centro_e==''){

                    return false;
               
                }
              
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }







              public function persona($centro,$estado){
                // var_dump($nombre);
                 $items=[];
                try{
               $query=$this->db->connect()->query("SELECT  * FROM 
               nomina_docente_sigad,
               persona_nacionalidad,
               docente,
               persona,
               centro_estudio,
               censo
                where 
               persona_nacionalidad.identificacion=nomina_docente_sigad.cedula and 
               persona.id_persona=persona_nacionalidad.id_persona and
               docente.id_persona=persona.id_persona and
               centro_estudio.id_centro_estudio=docente.id_centro_estudio and
               censo.id_persona=persona.id_persona and
               estado_nacimiento='$estado' and
                 centro_estudio.descripcion='$centro';");
 
               while($row=$query->fetch()){
               $item=new Persona();
               $item->nombres=$row['nombres'];
               $item->cedula=$row['cedula'];
               $item->estado=$estado;
               $item->centro=$centro;
               array_push($items,$item);
               }
 
             
               
               return $items;
               
               }catch(PDOException $e){
               return[];
               }
               
               }
 




            }
  

    ?>