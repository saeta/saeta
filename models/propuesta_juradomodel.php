<?php
    include_once 'SED.php';
    include_once 'estructura.php';

      //encliptar contraseña
    class Propuesta_juradoModel extends Model{
        public function __construct(){
           parent::__construct();
        }
        

        public function getJurado(){
            $items=[];
            try{
                $query=$this->db->connect()->query("select 
                id_jurado,
                t1.id_propuesta_jurado,
                t3.estatus,
                t3.id_solicitud_ascenso,
                t1.id_tipo_jurado,
                t6.descripcion as tipo_jurado,
                t1.id_docente,
                t2.id_escalafon,
                t8.descripcion as escalafon,
                t4.id_persona,
                identificacion,
                primer_nombre,
                segundo_nombre,
                primer_apellido,
                segundo_apellido,
                fecha_solicitud,
                ciudad,
                t9.descripcion as estatus_ascenso
                
                
                 from 
                jurado as t1, 
                docente as t2,
                propuesta_jurado as t3,
                persona as t4,
                persona_nacionalidad as t5,
                tipo_jurado as t6,
                solicitud_ascenso as t7,
                escalafon as t8,
                estatus_ascenso as t9
                where 
                t1.id_docente=t2.id_docente
                and t2.id_persona=t4.id_persona
                and t5.id_persona=t4.id_persona
                and t1.id_propuesta_jurado=t3.id_propuesta_jurado
                and t1.id_tipo_jurado=t6.id_tipo_jurado
                and t3.id_solicitud_ascenso=t7.id_solicitud_ascenso
                and t2.id_escalafon=t8.id_escalafon
                and t7.id_estatus_ascenso=t9.id_estatus_ascenso
                and t7.id_estatus_ascenso=4;");
                      
                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id=$row['id_jurado'];
                    $item->id_propuesta_jurado=$row['id_propuesta_jurado'];
                    $item->estatus=$row['estatus'];
                    $item->id_tipo_jurado=$row['id_tipo_jurado'];
                    $item->tipo_jurado=$row['tipo_jurado'];
                    $item->id_docente=$row['id_docente'];
                    $item->id_persona=$row['id_persona'];
                    $item->identificacion=$row['identificacion'];
                    $item->primer_nombre=$row['primer_nombre'];
                    $item->segundo_nombre=$row['segundo_nombre'];
                    $item->primer_apellido=$row['primer_apellido'];
                    $item->segundo_apellido=$row['segundo_apellido'];
                    $item->id_solicitud_ascenso=$row['id_solicitud_ascenso'];
                    $item->fecha_solicitud=$row['fecha_solicitud'];
                    $item->ciudad=$row['ciudad'];
                    $item->id_escalafon=$row['id_escalafon'];
                    $item->escalafon=$row['escalafon'];
                    $item->estatus_ascenso=$row['estatus_ascenso'];


                    array_push($items,$item);
                      
                      
                }
                return $items;
                      
            }catch(PDOException $e){
                return[];
            }
                      
        }

        //////////////    funcion que llama datos de tablas catalogos   ///////////////
        public function getCatalogo($valor){
            $items=[];
            try{
                $query=$this->db->connect()->query("SELECT * FROM ".$valor."");
                      
                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id=$row['id_'.$valor.''];
                    $item->descripcion=$row['descripcion'];
                      
                    array_push($items,$item);
                      
                      
                }
                return $items;
                      
            }catch(PDOException $e){
                return[];
            }
                      
        }
        


    }
        
    

?>