<?php
    include_once 'SED.php';//clase para encriptar contraseña
    include_once 'perfildocente/docentesigad.php';
    include_once 'perfildocente/ejeregional.php';
    include_once 'datosacademicos/centroestudio.php';
    include_once 'datosacademicos/programa_formacion.php';
    include_once 'perfildocente/pregunta.php';
    include_once 'models/estructura.php';

    class LoginModel extends Model{
        public function __construct(){
            parent::__construct();
        }
        
        //verificar si el docente existe en la bbdd
        public function existe($identificacion){
            try{

                $sql = $this->db->connect()->prepare("SELECT identificacion FROM persona_nacionalidad WHERE identificacion=:identificacion");
                $sql->execute(['identificacion' =>$identificacion]);
                $ci=$sql->fetch();
                //var_dump('existe',$ci);

                if($identificacion==$ci['identificacion']){
                    
                    return true;
    
                } 
            } catch(PDOException $e){
                return false;
            }
        }

        //autenticacion de usuarios
        public function getLogin($datos){
            //var_dump($datos);
            try{ 

               
                
                $crypt= new SED();
                $clave=$crypt->encryption($datos['password']);
               // var_dump($clave);

                /*$query=$this->db->connect()->prepare("SELECT 
		        usuario, 
                identificacion, 
                primer_nombre, 
                primer_apellido,
                usuario.id_perfil, 
                perfil.descripcion,
                docente.id_docente,
                password,
                docente.id_escalafon,
                persona.id_persona,
                docente.id_clasificacion_docente
                FROM 
                usuario, persona, 
                persona_nacionalidad, perfil,
                docente,
                clasificacion_docente
                WHERE usuario.id_persona=persona.id_persona 
                AND persona_nacionalidad.id_persona=persona.id_persona 
                AND usuario.id_perfil=perfil.id_perfil 
		        AND docente.id_persona=persona.id_persona
                AND docente.id_clasificacion_docente=clasificacion_docente.id_clasificacion_docente
                AND usuario=:usuario 
                AND password=:password;");*/

                $query=$this->db->connect()->prepare("SELECT 
                        usuario, 
                        identificacion, 
                        primer_nombre, 
                        primer_apellido,
                        usuario.id_perfil, 
                        perfil.descripcion,
                        password,
                        persona.id_persona
                        FROM 
                        usuario, persona, 
                        persona_nacionalidad, 
                        perfil
                        WHERE usuario.id_persona=persona.id_persona 
                        AND persona_nacionalidad.id_persona=persona.id_persona 
                        AND usuario.id_perfil=perfil.id_perfil 
                        AND usuario=:usuario 
                        AND password=:password;");

                $query->execute(['usuario'=> $datos['usuario'], 'password' =>$clave]);
                
                $var=$query->fetch(PDO::FETCH_ASSOC);

                if($var['id_perfil']==1){

                    $docente=$this->db->connect()->prepare("SELECT 
                    docente.id_docente,
                    docente.id_escalafon,
                    docente.id_clasificacion_docente
                    FROM 
                    docente,
                    clasificacion_docente,
                    persona
                    WHERE 
                    docente.id_persona=persona.id_persona
                    AND docente.id_clasificacion_docente=clasificacion_docente.id_clasificacion_docente
                    AND docente.id_persona=:id_persona;");
                    $docente->execute(['id_persona'=>$var['id_persona']]);
                    $datos_docente=$docente->fetch(PDO::FETCH_ASSOC);

                }
                //var_dump($var);
                //if($id_perfil['id_perfil'] != 3 && $var['identificacion']==$datos['identificacion'] && $var['password']==SED::encryption($datos['password'])){
                //perfiles que pueden iniciar sesion
                if($var['id_perfil'] < 1 || $var['id_perfil'] > 11){   
                   return false;
                }
                //echo " <<<<<<<" . $var['password'];
                //var_dump($var['usuario'], $datos['usuario'], $var['password'], $datos['password']);
                if($var['usuario']==$datos['usuario'] && $var['password'] == $clave){
                   //echo "aqui";
                    //variables de sesion
                    session_start();
                    $_SESSION['id_persona']=$var['id_persona'];
                    $_SESSION['identificacion']=$var['identificacion'];
                    $_SESSION['primer_nombre']=$var['primer_nombre'];
                    $_SESSION['primer_apellido']=$var['primer_apellido'];
                    $_SESSION['password']=$var['password'];
                    $_SESSION['descripcion']=$var['descripcion'];
                    $_SESSION['usuario']=$var['usuario'];
                    $_SESSION['id_perfil']=$var['id_perfil'];

                    if($var['id_perfil']==1){

                        $docente=$this->db->connect()->prepare("SELECT 
                        docente.id_docente,
                        docente.id_escalafon,
                        docente.id_clasificacion_docente
                        FROM 
                        docente,
                        clasificacion_docente,
                        persona
                        WHERE 
                        docente.id_persona=persona.id_persona
                        AND docente.id_clasificacion_docente=clasificacion_docente.id_clasificacion_docente
                        AND docente.id_persona=:id_persona;");
                        $docente->execute(['id_persona'=>$var['id_persona']]);
                        $datos_docente=$docente->fetch(PDO::FETCH_ASSOC);
    
                        $_SESSION['id_docente']=$datos_docente['id_docente'];
                        $_SESSION['id_escalafon']=$datos_docente['id_escalafon'];
                        $_SESSION['id_clasificacion_docente']=$datos_docente['id_clasificacion_docente'];
                        
                    }

                    $roles_user=$this->db->connect()->prepare("SELECT usuario_rol.id_usuario, 
                    usuario_rol.id_rol, 
                    rol.descripcion AS rol 
                    FROM rol,
                    usuario_rol,
                    usuario, 
                    persona
                    WHERE usuario_rol.id_rol=rol.id_rol
                    AND usuario_rol.id_usuario=usuario.id_usuario 
                    AND usuario.id_persona=persona.id_persona
                    AND usuario.id_persona=:id_persona");
                    $roles_user->execute(['id_persona'=>$var['id_persona']]);
                    $_SESSION['Agregar']=false;
                    $_SESSION['Consultar']=false;
                    $_SESSION['Editar']=false;
                    $_SESSION['Eliminar']=false;

                    while($row=$roles_user->fetch(PDO::FETCH_ASSOC)){
                        switch($row['rol']){
                            case 'Agregar': 
                            $_SESSION['Agregar']=true;
                            
                            break;
                            case 'Consultar': 
                            $_SESSION['Consultar']=true;
                            break;
                            case 'Editar': 
                            $_SESSION['Editar']=true;

                            break;
                            case 'Eliminar': 
                            $_SESSION['Eliminar']=true;
                            break;
                        }
                    }
                    

                    return true;
                }else {
                    return false;
                }

            } catch(PDOException $e){
                return false;
            }


        }

        //obtener los datos de SIGAD para registrar al docente
        public function getbyCedula($cedula){

            try{
                $query=$this->db->connect()->prepare("SELECT * FROM nomina_docente_sigad WHERE cedula=:cedula");
                $query->execute(['cedula'=>$cedula]);

                $item=new DocenteSigad();
                while($row=$query->fetch()){

                    $item->id_nomina_docente_sigad=$row['id_nomina_docente_sigad'];
                    $item->cedula=$row['cedula'];
                    $item->nombres=$row['nombres'];

                    $item->nacionalidad=$row['nacionalidad'];
                    $item->fecha_nacimiento=$row['fecha_nacimiento'];
                    $item->pais_nacimiento=$row['pais_nacimiento'];
                    $item->estado_nacimiento=$row['estado_nacimiento'];
                    $item->genero=$row['genero'];
                    $item->estado_civil=$row['estado_civil'];
                    $item->telefono=$row['telefono'];
                    $item->correo_electronico=$row['correo_electronico'];
                    $item->direccion=$row['direccion'];
                    $item->numero_hijos=$row['numero_hijos'];
                    $item->cargo=$row['cargo'];
                    $item->clasificacion_docente=$row['clasificacion_docente'];
                    $item->fecha_ingreso=$row['fecha_ingreso'];
                    $item->dedicacion=$row['dedicacion'];
                    $item->estatus=$row['estatus'];

                    array_push($items,$item);

                }

                return $item;

            } catch(PDOException $e){
                return false;
            }

        }

        //funcion para registrar un docente en SAETA
        public function insert($datos){

            try{
                //var_dump($datos['programa'], $datos['eje_regional'], $datos['centro_estudio']);
                //var_dump($datos);
                
                //1. guardas el objeto pdo en una variable
                $pdo=$this->db->connect();

                //2. comienzas transaccion
                $pdo->beginTransaction();
               
                /*

                    3. hacer toas las consultas comenzando desde el la 
                    variable del paso 1 (ejemplo: $sql=$pdo->prepare(...); 
                    $sql=$pdo->query(...);, etc)

                */                
                $query_persona=$pdo->prepare("INSERT INTO persona(
                    primer_nombre, 
                    segundo_nombre, 
                    primer_apellido, 
                    segundo_apellido, 
                    fecha_nacimiento,
                    id_estado_civil,
                    id_genero,
                    id_pais
                    ) VALUES(
                    :primer_nombre,
                    :segundo_nombre,
                    :primer_apellido,
                    :segundo_apellido,
                    :fecha_nacimiento,
                    (SELECT id_estado_civil FROM estado_civil where descripcion=:estado_civil),
                    (SELECT id_genero FROM genero where descripcion=:genero),
                    (SELECT id_pais FROM pais where descripcion=:pais)
                    );");
                    $query_persona->execute([
                        'primer_nombre'=>$datos['primer_nombre'],
                        'segundo_nombre'=>$datos['segundo_nombre'],
                        'primer_apellido'=>$datos['primer_apellido'],
                        'segundo_apellido'=>$datos['segundo_apellido'],
                        'fecha_nacimiento'=>$datos['fecha_nacimiento'],
                        'estado_civil'=>$datos['estado_civil'],
                        'genero'=>$datos['genero'],
                        'pais'=>$datos['pais_nacimiento']
                        ]);

                $sql_nacionalidad=$pdo->prepare("INSERT INTO persona_nacionalidad(
                    identificacion, 
                    id_nacionalidad, 
                    id_documento_identidad_tipo, 
                    id_persona) VALUES(
                    :identificacion,
                    (SELECT id_nacionalidad FROM nacionalidad, pais WHERE nacionalidad.id_pais=pais.id_pais AND pais.descripcion=:pais),
                    (SELECT id_documento_identidad_tipo FROM documento_identidad_tipo WHERE descripcion=:nacionalidad),
                    (SELECT id_persona FROM persona ORDER BY id_persona DESC LIMIT 1)
                    );");

                $sql_nacionalidad->execute([
                    'identificacion'=>$datos['cedula'],
                    'pais'=>$datos['pais_nacimiento'],
                    'nacionalidad'=>$datos['nacionalidad']
                    ]);
                

                if(!empty($datos['telefono']) && !empty($datos['cod_area'])){
                    $sql_telefono=$pdo->prepare("INSERT INTO persona_telefono(
                        telefono, 
                        id_telefono_codigo_area, 
                        id_persona, 
                        id_telefono_tipo) VALUES(
                        :telefono,
                        (SELECT id_telefono_codigo_area FROM telefono_codigo_area where descripcion=:cod_area),
                        (SELECT id_persona FROM persona ORDER BY id_persona DESC LIMIT 1),
                        (SELECT id_telefono_tipo FROM telefono_tipo where descripcion=:telefono_tipo)
                        );");
                    
                    $sql_telefono->execute([
                        'telefono'=>$datos['telefono'],
                        'cod_area'=>$datos['cod_area'],
                        'telefono_tipo'=>'Personal'
                    ]);

                }

                if(!empty($datos['correo_electronico'])){
                    $sql_correo=$pdo->prepare("INSERT INTO persona_correo(
                        correo,
                        id_persona,
                        id_correo_tipo) VALUES(
                        :correo,
                        (SELECT id_persona FROM persona ORDER BY id_persona DESC LIMIT 1),
                        (SELECT id_correo_tipo FROM correo_tipo WHERE descripcion=:correo_tipo)
                        );");
                    $sql_correo->execute([
                        'correo'=>$datos['correo_electronico'],
                        'correo_tipo'=>$datos['correo_tipo']
                    ]);


                }

                $sql_hijos=$pdo->prepare("INSERT INTO persona_hijo(
                    descripcion,
                    id_persona)
                    VALUES(
                    :numero_hijos,
                    (SELECT id_persona FROM persona ORDER BY id_persona DESC LIMIT 1)
                    );");
                $sql_hijos->execute([
                    'numero_hijos'=>$datos['numero_hijos']
                ]);


                $sql_fecha_ingreso=$pdo->prepare("INSERT INTO persona_fecha_ingreso(
                    fecha,
                    id_persona)
                    VALUES(
                    :fecha_ingreso,
                    (SELECT id_persona FROM persona ORDER BY id_persona DESC LIMIT 1)
                    );");
                $sql_fecha_ingreso->execute([
                    'fecha_ingreso'=>$datos['fecha_ingreso']
                ]);
                   
                $sql_docente=$pdo->prepare("INSERT INTO docente(
                    id_centro_estudio,
                    id_persona,
                    id_eje_municipal,
                    id_docente_dedicacion,
                    id_docente_estatus,
                    tipo_docente,
                    id_clasificacion_docente,
                    id_escalafon
                    ) VALUES(
                    :centro_estudio,
                    (SELECT id_persona FROM persona ORDER BY id_persona DESC LIMIT 1),
                    :eje_municipal,
                    (SELECT id_docente_estatus FROM docente_estatus WHERE descripcion=:estatus),
                    (SELECT id_docente_dedicacion FROM docente_dedicacion WHERE descripcion=:dedicacion),
                    'SIDTA',
                    (SELECT id_clasificacion_docente FROM clasificacion_docente WHERE descripcion=:clasificacion_docente),
                    (SELECT id_escalafon FROM escalafon WHERE descripcion=:cargo)
                    );");
                
                $sql_docente->execute([
                    'centro_estudio'=>$datos['centro_estudio'],
                    'eje_municipal'=>$datos['eje_municipal'],
                    'estatus'=>$datos['estatus'],
                    'dedicacion'=>$datos['dedicacion'],
                    'clasificacion_docente'=>$datos['clasificacion_docente'],
                    'cargo'=>$datos['cargo']
                ]);

                if(!empty($datos['aldea'])){
                    $existe_aldea=$pdo->prepare("SELECT count(*) from aldea_ubv where descripcion=:descripcion");
                    $existe_aldea->execute(['descripcion'=>$datos['aldea']]);
                    $row1=$existe_aldea->fetch();
        
                    if($row1[0]>0){
                      //insertar 
                      //obtener id de la aldea
                      $aldea_ubv=$pdo->prepare("SELECT * from aldea_ubv where descripcion=:descripcion");
                      $aldea_ubv->execute(['descripcion'=>$datos['aldea']]);
                      $ald=$aldea_ubv->fetch();
                      
                      $aldea_docente=$pdo->prepare("INSERT INTO docente_aldea(id_docente, id_aldea_ubv)
                      VALUES((SELECT id_docente FROM docente ORDER BY id_docente DESC LIMIT 1), :id_aldea_ubv)");
                     
                    }else{
                      //insertamos en aldea
                      $aldea_ubv=$pdo->prepare("INSERT INTO aldea_ubv (descripcion) 
                      VALUES(:descripcion)");
                      $aldea_ubv->execute(['descripcion'=>$datos['aldea']]);
        
                      //recuperamos el id de la aldea insertada
                      $aldea_ubv=$pdo->prepare("SELECT * from aldea_ubv where descripcion=:descripcion");
                      $aldea_ubv->execute(['descripcion'=>$datos['aldea']]);
                      $ald=$aldea_ubv->fetch();
        
                      //actualizamos el id de la aldea
                      $aldea_docente=$pdo->prepare("INSERT INTO docente_aldea(id_docente, id_aldea_ubv)
                      VALUES((SELECT id_docente FROM docente ORDER BY id_docente DESC LIMIT 1), :id_aldea_ubv)");
                      $aldea_docente->execute(['id_aldea_ubv'=>$ald['id_aldea_ubv']]);
                    }
                }//end-docente_aldea
                //tabla docente_programa
                $programas=$datos['programa'];
                $docente_programa=$pdo->prepare('INSERT INTO docente_programa(id_docente, id_programa) 
                VALUES ((SELECT id_docente FROM docente ORDER BY id_docente DESC LIMIT 1), :id_programa)');
                for ($i=0;$i<count($programas);$i++)    
                {       
                    $docente_programa->execute(['id_programa'=>$programas[$i]]);
                } 


                //encriptar contraseña
                $crypt= new SED();
                $clave=$crypt->encryption($datos['password']);

                $sql_usuario=$pdo->prepare("INSERT INTO usuario(
                    id_persona, 
                    usuario, 
                    password, 
                    fecha, 
                    estatus, 
                    id_perfil)
                    VALUES (
                    (SELECT id_persona FROM persona ORDER BY id_persona DESC LIMIT 1),
                    :usuario, 
                    :password,
                    current_date, 
                    :estatus,
                    (SELECT id_perfil FROM perfil WHERE descripcion=:perfil)
                    );");
                $sql_usuario->execute([
                    'usuario'=>$datos['usuario'],
                    'password'=>$clave,
                    'estatus'=>'1',
                    'perfil'=>'Trabajador Académico'
                ]);

                $sql_pregunta=$pdo->prepare("INSERT INTO usuario_pregunta(
                    id_pregunta,
                    respuesta,
                    id_usuario)
                    VALUES(
                    :pregunta1,
                    :respuesta1,
                    (SELECT id_usuario FROM usuario ORDER BY id_usuario DESC LIMIT 1)
                    ), (:pregunta2,
                    :respuesta2,
                    (SELECT id_usuario FROM usuario ORDER BY id_usuario DESC LIMIT 1)
                    );");
                $sql_pregunta->execute([
                    'pregunta1'=>$datos['pregunta1'],
                    'respuesta1'=>$datos['respuesta1'],
                    'pregunta2'=>$datos['pregunta2'],
                    'respuesta2'=>$datos['respuesta2']
                ]);

                //var_dump($datos['direccion']);
                $sql_domicilio=$pdo->prepare("INSERT INTO domicilio_persona(
                    id_parroquia,
                    id_persona,
                    domicilio_detalle,
                    id_domicilio_detalle_tipo)
                    VALUES (
                    :parroquia,
                    (SELECT id_persona FROM persona ORDER BY id_persona DESC LIMIT 1),
                    :domicilio_detalle,
                    :id_domicilio_detalle_tipo);");
                
                $sql_domicilio->execute([
                    'parroquia'=>$datos['parroquia'],
                    'domicilio_detalle'=>$datos['direccion'],
                    'id_domicilio_detalle_tipo'=>1
                ]);
                    //4. consignas la transaccion (en caso de que no suceda ningun fallo)
                    $pdo->commit();

                    return true;
                    
                } catch(PDOException $e){
                    //5. regresas a un estado anterior en caso de error
                    $pdo->rollBack();
                //echo "Fallo: " . $e->getMessage();
                return false;
            }

        }


        //obtener lista de ejes regionales
        // duda: porque todos los ejes regionales estan como false?
        //esto afecta la consulta ya que debo filtrar por el estatus del eje_regional
        public function getEjeRegional(){
            $items=[];
            try{
                $ejes_regionales=$this->db->connect()->query("SELECT * FROM eje_regional");
          
                while($row=$ejes_regionales->fetch(PDO::FETCH_ASSOC)){

                    $item=new EjeRegional();

                    $item->id_eje_regional=$row['id_eje_regional'];
                    $item->descripcion=$row['descripcion'];
                    $item->codigo=$row['codigo'];
                    $item->estatus=$row['estatus'];          
                    
                    array_push($items,$item);
                
                
                }
                return $items;
          
          }catch(PDOException $e){
            return[];
          }
          
        }

        //obtener centros de estudios activos
        public function getCentro(){
            $items=[];
            try{
                $query=$this->db->connect()->prepare("SELECT 
                id_centro_estudio as id_centro,
                centro_estudio.codigo,
                centro_estudio.descripcion,
                centro_estudio.estatus,
                area_conocimiento_opsu.descripcion as descripcion_opsu,
                area_conocimiento_opsu.id_area_conocimiento_opsu as id_area_opsu
                FROM 
                centro_estudio, 
                area_conocimiento_opsu
                WHERE centro_estudio.id_area_conocimiento_opsu=area_conocimiento_opsu.id_area_conocimiento_opsu
                AND centro_estudio.estatus=:estatus;");
                $query->execute(['estatus'=>true]);
                while($row=$query->fetch()){
                    
                    $item=new CentroEstudio();
                    $item->id_centro_estudio=$row['id_centro'];
                    $item->codigo=$row['codigo'];
                    $item->descripcion=$row['descripcion'];
                   $item->id_area_conocimiento_opsu=$row['id_area_opsu'];
                    if($row['estatus'] == 1){
                        $item->estatus="Activo";
                    }elseif($row['estatus'] == 0){
                        $item->estatus="Inactivo";
                    }
                    $item->descripcion_opsu=$row['descripcion_opsu'];
                    
                    
                   
                
                    array_push($items,$item);
                
                
                }
                return $items;
                
            }catch(PDOException $e){
            return[];
            }
          
        }

        //obtener programas de formacion activos
        public function getPrograma(){
            $items=[];
            try{
                $query=$this->db->connect()->prepare("SELECT 
                id_programa as id_programa,
                programa.descripcion as programa,
                programa.codigo as codigo,
                programa.id_centro_estudio as id_centro,
                centro_estudio.descripcion as centro,
                programa.id_programa_tipo as id_tipo,
                programa_tipo.descripcion as tipo,
                programa.id_nivel_academico as id_nivel,
                nivel_academico.descripcion as nivel,
                programa.id_linea_investigacion as id_linea,
                linea_investigacion.descripcion as linea,
                programa.estatus as estatus
                FROM programa, centro_estudio, programa_tipo, nivel_academico, linea_investigacion
                WHERE programa.id_centro_estudio=centro_estudio.id_centro_estudio
                and programa.id_programa_tipo=programa_tipo.id_programa_tipo
                and programa.id_nivel_academico=nivel_academico.id_nivel_academico
                and programa.id_linea_investigacion=linea_investigacion.id_linea_investigacion
                and programa.estatus=:estatus;");
                $query->execute(['estatus'=>'1']);

                while($row=$query->fetch()){
        
                    $item=new Programa();
                    $item->id_programa=$row['id_programa'];
                    $item->descripcion=$row['programa'];
                    $item->codigo=$row['codigo'];
                    $item->id_centro_estudio=$row['id_centro'];
                    $item->descripcion_centro=$row['centro'];
                    $item->id_programa_tipo=$row['id_tipo'];
                    $item->descripcion_tipo=$row['tipo'];
                    $item->id_nivel_academico=$row['id_nivel'];
                    $item->descripcion_nivel=$row['nivel'];
                    $item->id_linea_investigacion=$row['id_linea'];
                    $item->descripcion_linea=$row['linea'];
                    if($row['estatus'] == 1){
                        $item->estatus="Activo";
                    }elseif($row['estatus'] == 0){
                        $item->estatus="Inactivo";
                    }
        
                    array_push($items,$item);
        
                }
                return $items;
        
            }catch(PDOException $e){
            return[];
            }
        
        }

        //obtener lista de preguntas
        public function getPregunta(){
            $items=[];
            try{
                $preguntas=$this->db->connect()->query("SELECT * FROM pregunta");
          
                while($row=$preguntas->fetch(PDO::FETCH_ASSOC)){

                    $item=new Pregunta();

                    $item->id_pregunta=$row['id_pregunta'];
                    $item->descripcion=$row['descripcion'];
                    array_push($items,$item);
                
                
                }
                return $items;
          
          }catch(PDOException $e){
          return[];
          }
          
        }
    
        //obtener paises activos
        public function getPais(){
            $items=[];
            
            try{
                $query=$this->db->connect()->query("SELECT * FROM pais");
          
                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id_pais=$row['id_pais'];
                    $item->descripcion=$row['descripcion'];
                    $item->codigo=$row['codigo'];
                    $item->estatus=$row['estatus'];
          
                    array_push($items,$item);
          
          
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
        }

        //obtener estados
        public function getEstado(){
            
            $items=[];
            try{
                $query=$this->db->connect()->query("SELECT id_estado,estado.descripcion AS estado,estado.codigo,pais.id_pais,pais.descripcion AS pais ,pais.estatus AS estatus_pais FROM estado,pais WHERE estado.id_pais=pais.id_pais");
          
                while($row=$query->fetch()){
                    
                    $item=new Estructura();
                    $item->id_estado=$row['id_estado'];
                    $item->estado=$row['estado'];
                    $item->codigo=$row['codigo'];
                    $item->id_pais=$row['id_pais'];
                    $item->pais=$row['pais'];           
                    $item->estatus_pais=$row['estatus_pais'];

                    array_push($items,$item);
                
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
          
        }

        //obtener municipios
        public function getMunicipio(){
            $items=[];
            try{
                $query=$this->db->connect()->query("SELECT id_municipio,municipio.descripcion AS municipio,municipio.codigo AS codigo ,estado.id_estado, estado.descripcion AS estado FROM municipio,estado WHERE estado.id_estado=municipio.id_estado");
          
                while($row=$query->fetch()){
                $item=new Estructura();
                $item->id_municipio=$row['id_municipio'];
                $item->municipio=$row['municipio'];
                $item->codigo=$row['codigo'];
                $item->id_estado=$row['id_estado'];
                $item->estado=$row['estado'];
                
                
                array_push($items,$item);
                
                
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
          
        }

        //obtener parroquias
        public function getParroquia(){
            $items=[];
            try{
                $query=$this->db->connect()->query("SELECT id_parroquia,parroquia.descripcion AS parroquia,parroquia.codigo AS codigo,municipio.id_municipio,eje_municipal.id_eje_municipal,municipio.descripcion AS municipio,eje_municipal.descripcion AS eje_municipal,eje_municipal.estatus AS  eje_municipal_estutus FROM parroquia,municipio,eje_municipal WHERE parroquia.id_municipio=municipio.id_municipio AND parroquia.id_eje_municipal=eje_municipal.id_eje_municipal");
          
                while($row=$query->fetch()){
                    
                    $item=new Estructura();
                    $item->id_parroquia=$row['id_parroquia'];
                    $item->parroquia=$row['parroquia'];
                    $item->codigo=$row['codigo'];
                    $item->id_municipio=$row['id_municipio'];          
                    $item->municipio=$row['municipio'];
                    $item->id_eje_municipal=$row['id_eje_municipal'];
                    $item->eje_municipal=$row['eje_municipal'];
                    $item->eje_municipal_estutus=$row['eje_municipal_estutus'];
                    
                    array_push($items,$item);
                
                
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
          
        }
    

        //consultar ascensos
        public function getModAscenso($fecha_actual){
        //var_dump($fecha_actual);
            $items=[];
            try{
                $query=$this->db->connect()->query("SELECT 
                            id_activacion,
                            periodo_lectivo, 
                            fecha_inicio,
                            fecha_fin,
                            activacion.id_tipo_activacion,
                            activacion.descripcion as motivo_activacion,
                            tipo_activacion.descripcion as tipo_activacion

                            FROM activacion, tipo_activacion WHERE activacion.id_tipo_activacion=tipo_activacion.id_tipo_activacion;");
                            
                $row=$query->fetch(PDO::FETCH_ASSOC);
                if($fecha_actual>=date('Y/m/d', strtotime($row['fecha_inicio'])) 
                        && $fecha_actual<=date('Y/m/d', strtotime($row['fecha_fin']))){
                    $estatus='1';
                    //var_dump($sql->execute(['estatus'=>$estatus, 'id_activacion'=>$row['id_activacion']]));
                    
                    //var_dump('dos',$_SESSION['estatus_ascenso']);
                    
                    return true;
                }else{
                    return false;
                }

               

            } catch(PDOException $e){
                return false;
            }

        }



         //////////////    funcion que llama datos de tablas catalogos   ///////////////
         public function getCatalogo($valor){
            $items=[];
           try{
          $query=$this->db->connect()->query("SELECT * FROM ".$valor."");
          
          while($row=$query->fetch()){
          $item=new Estructura();
          $item->id=$row['id_'.$valor.''];
          $item->descripcion=$row['descripcion'];
          
          array_push($items,$item);
          
          }
          return $items;
          
          }catch(PDOException $e){
          return[];
          }
          
          }
        ///////////////////////////////////////////////////////


        public function getEjeMunipalbyER($id_eje_regional){
            $items=[];
            try{
              $query=$this->db->connect()->prepare("SELECT * FROM eje_municipal WHERE id_eje_regional = :id_eje_regional;");
              $query->execute(['id_eje_regional'=>$id_eje_regional]);
              while($row=$query->fetch()){
                $item=new Estructura();
                $item->id_eje_municipal=$row['id_eje_municipal'];
                $item->descripcion=$row['descripcion'];
      
                array_push($items,$item);
                
              
              }
              return $items;
              
            }catch(PDOException $e){
              return[];
            }
            
          }
    }
        
    

?>
