<?php
    include_once 'SED.php';
    include_once 'estructura.php';

      //encliptar contraseña
    class HomeModel extends Model{
        public function __construct(){
           parent::__construct();
        }
        
        //consultar ascensos
        public function getModAscenso(){
            //var_dump($fecha_actual);
                $items=[];
                try{
                    $query=$this->db->connect()->query("SELECT 
                    id_activacion,
                    periodo_lectivo, 
                    fecha_inicio,
                    fecha_fin,
                    activacion.id_tipo_activacion,
                    activacion.descripcion as motivo_activacion,
                    tipo_activacion.descripcion as tipo_activacion

                    FROM 
                    activacion, 
                    tipo_activacion 
                    WHERE 
                    activacion.id_tipo_activacion=tipo_activacion.id_tipo_activacion
                    and activacion.id_tipo_activacion=1 ORDER BY id_activacion DESC;");
                                
                    $row=$query->fetch(PDO::FETCH_ASSOC);
                    return $row;
    
                   
    
                } catch(PDOException $e){
                    return false;
                }
    
            }

            public function getSolicitudes(){

                    $items=[];
                    try{
                        $query=$this->db->connect()->query("SELECT 
                            t1.id_solicitud_ascenso,
                            t1.id_estatus_ascenso,
                            t4.descripcion as estatus_ascenso,
                            t1.id_docente,
                            t3.id_persona,
                            primer_nombre,
                            primer_apellido,
                            t6.descripcion as escalafon_siguiente
                            from 
                            solicitud_ascenso as t1,
                            docente as t2,
                            persona as t3,
                            estatus_ascenso as t4,
                            solicitud_escalafon_siguiente as t5,
                            escalafon as t6
                            where 
                            t1.id_docente=t2.id_docente
                            and t2.id_persona=t3.id_persona
                            and t1.id_estatus_ascenso=t4.id_estatus_ascenso
                            and t5.id_escalafon=t6.id_escalafon
                            and t5.id_solicitud_ascenso=t1.id_solicitud_ascenso
                            and t1.id_estatus_ascenso=1 
                            ORDER BY id_solicitud_ascenso DESC LIMIT 5;");

                        while($row=$query->fetch(PDO::FETCH_ASSOC)){
                            $item = new Estructura;
                            $item->id_solicitud_ascenso=$row['id_solicitud_ascenso'];
                            $item->id_estatus_ascenso=$row['id_estatus_ascenso'];
                            $item->estatus_ascenso=$row['estatus_ascenso'];
                            $item->id_docente=$row['id_docente'];
                            $item->id_persona=$row['id_persona'];
                            $item->primer_nombre=$row['primer_nombre'];
                            $item->primer_apellido=$row['primer_apellido'];
                            $item->id_escalafon=$row['id_escalafon'];
                            $item->escalafon_siguiente=$row['escalafon_siguiente'];
                            array_push($items, $item);
                        }
                        
                        return $items;
        
                    } catch(PDOException $e){
                        return false;
                    }
        
                }

                public function getPropuestas(){

                    $items=[];
                    try{
                        $query=$this->db->connect()->query("SELECT * from propuesta_jurado where estatus='En Espera'
                        ORDER BY id_propuesta_jurado LIMIT 5;");

                        while($row=$query->fetch(PDO::FETCH_ASSOC)){
                            $item = new Estructura;
                            $item->id_propuesta_jurado=$row['id_propuesta_jurado'];
                            $item->id_docente=$row['id_docente'];
                            $item->estatus=$row['estatus'];
                            $item->id_solicitud_ascenso=$row['id_solicitud_ascenso'];
                            
                            array_push($items, $item);
                        }
                        
                        return $items;
        
                    } catch(PDOException $e){
                        return false;
                    }
        
                }

                public function getMemos($id_tipo_documento){

                    $items=[];
                    try{
                        $query=$this->db->connect()->query("SELECT 
                        id_memo_solicitud,
                        memo_solicitud.id_documento,
                        documento.descripcion,
                        documento.id_tipo_documento
                        from memo_solicitud,
                        documento 
                        where 
                        memo_solicitud.id_documento=documento.id_documento
                        and id_tipo_documento=10
                        ORDER BY id_memo_solicitud LIMIT 5;");

                        while($row=$query->fetch(PDO::FETCH_ASSOC)){
                            $item = new Estructura;
                            $item->id_memo_solicitud=$row['id_memo_solicitud'];
                            $item->id_documento=$row['id_documento'];
                            $item->descripcion=$row['descripcion'];
                            $item->id_tipo_documento=$row['id_tipo_documento'];
                            
                            array_push($items, $item);
                        }
                        
                        return $items;
        
                    } catch(PDOException $e){
                        return false;
                    }
        
                }

        










































































































            public function getCantidadConcursos(){


                $items=[];
                try{
                    $query=$this->db->connect()->query("SELECT count (id_concurso) AS concurso FROM concurso;");
                    $row=$query->fetch(PDO::FETCH_ASSOC);
                    //var_dump($row);  
                     return $row['concurso'];
    
                } catch(PDOException $e){
                    return false;
                }
            }


   public function getCantidadAscensos(){

                $items=[];
                try{
                    $query=$this->db->connect()->query("SELECT count (id_historia_ascenso) AS historiaascenso FROM historia_ascenso;");
                    $row=$query->fetch(PDO::FETCH_ASSOC);
                    return $row['historiaascenso'];
    
                } catch(PDOException $e){
                    return false;

                }
            }

// pre y postgrado son estudios conducentes
            public function getCantidadPostPreGrado(){


                $items=[];
                try{
                    $query=$this->db->connect()->query("SELECT count (id_estudio_conducente) AS conducente FROM estudio_conducente;");
                    $row=$query->fetch(PDO::FETCH_ASSOC);
                    return $row['conducente'];
    
                } catch(PDOException $e){
                    return false;

                }
            }
// esta tambien creo que esta mala, la que puedo usar para esta consulta es la de abajo de no conducentes cambiandole el tipo a 1
// post y pre 
           // select count(*) as cant_postgrado
           // from estudio_nivel where id_programa_nivel=1
            //  group by id_programa_nivel

        //revisar estas estan creo q malas:
           // conducente: SELECT count (*) AS clasificaciondocente FROM estudio where id_tipo_estudio='1'
           // no conduncente: SELECT count (*) AS clasificaciondocente FROM estudio where id_tipo_estudio='2'


// no conducentes: condu, e idiomas

       //     select count(*) as cant_noconducentes
        //   from estudio where id_tipo_estudio=2
        //     group by id_tipo_estudio


            public function getDocentesOrdinarios(){


                $items=[];
                try{
                    $query=$this->db->connect()->query("SELECT count (*) AS ordinarios FROM docente where id_clasificacion_docente='1';");
                    $row=$query->fetch(PDO::FETCH_ASSOC);
                    return $row['ordinarios'];
    
                } catch(PDOException $e){
                    return false;

                }
            }

            public function getDocentesContratados(){


                $items=[];
                try{
                    $query=$this->db->connect()->query("SELECT count (*) AS contratados FROM docente where id_clasificacion_docente='2';");
                    $row=$query->fetch(PDO::FETCH_ASSOC);
                    return $row['contratados'];
    
                } catch(PDOException $e){
                    return false;

                }
            }

            public function getEstudioConducente(){

                $items=[];
                try{
                    $query=$this->db->connect()->query("SELECT count (*) AS cant_conducentes FROM estudio where id_tipo_estudio='1';");
                    $row=$query->fetch(PDO::FETCH_ASSOC);
                    return $row['cant_conducentes'];
    
                } catch(PDOException $e){
                    return false;

                }
            }


            public function getEstudioNoconducente(){


                $items=[];
                try{
                    $query=$this->db->connect()->query("SELECT count (*) AS cant_noconducentes FROM estudio where id_tipo_estudio='2';");
                    $row=$query->fetch(PDO::FETCH_ASSOC);
                    return $row['cant_noconducentes'];
    
                } catch(PDOException $e){
                    return false;

                }
            }

            public function getIdioma(){


                $items=[];
                try{
                    $query=$this->db->connect()->query("SELECT count (*) AS idiomas FROM estudio where id_tipo_estudio='3';");
                    $row=$query->fetch(PDO::FETCH_ASSOC);
                    return $row['idiomas'];
    
                } catch(PDOException $e){
                    return false;

                }
            }


























    }
        
    

?>