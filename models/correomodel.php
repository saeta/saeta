<?php
include_once 'models/persona.php';
class CorreoModel extends Model{
    public function __construct(){
    parent::__construct();
    }
    public function existe($correo){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM correo_tipo WHERE descripcion=:descripcion");
            $sql->execute(['descripcion' =>$correo]);
            $nombre=$sql->fetch();
            
            if($correo==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
    //echo "<br>insertar datos";

    try{
     
             $query=$this->db->connect()->prepare('INSERT INTO correo_tipo (descripcion) VALUES
             (:descripcion)');

            $query->execute(['descripcion'=>$datos['correo']]);

            return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }
    
    
            }

            public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT * FROM correo_tipo");
              
              while($row=$query->fetch()){
              $item=new Persona();
              $item->id_correo_tipo=$row['id_correo_tipo'];
              $item->descripcion=$row['descripcion'];
              
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }


              public function update($item){
   
     //   var_dump($item['id_correo']);
       // var_dump($item['correo2']);

                $query=$this->db->connect()->prepare("UPDATE correo_tipo SET descripcion= :descripcion WHERE id_correo_tipo= :id_correo_tipo");
            try{
                        $query->execute([
                        'descripcion'=>$item['correo2'],
                        'id_correo_tipo'=>$item['id_correo'],
  
                        
                        ]);
            return true;
                       
                        
            }catch(PDOException $e){
                return false;
                 }
            
                }
   
                public function delete($id_correo_tipo){
                    $query=$this->db->connect()->prepare("DELETE FROM correo_tipo WHERE id_correo_tipo=:id_correo_tipo ");
                
                        try{
                        $query->execute([
                            'id_correo_tipo'=>$id_correo_tipo,
                            ]);
                                return true;
                
                        }catch(PDOException $e){
                return false;
                }
                
                
                    }  





    }

    ?>