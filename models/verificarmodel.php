<?php
    include_once 'SED.php';
    include_once 'models/estructura.php';

      //encliptar contraseña
    class VerificarModel extends Model{
        public function __construct(){
           parent::__construct();
        }
        
        
        //obtener tipos de activacion de SAETA
        public function getDocentes(){
                    
            $items=[];
            try{
                $query=$this->db->connect()->query("SELECT     
                substr(t5.descripcion, 1, 1) as nacionalidad,     
                identificacion, 
                primer_nombre, 
                segundo_nombre, 
		        primer_apellido,
                segundo_apellido, 
                docente.id_docente,
                docente.id_escalafon 
                FROM 
		        persona, 
                persona_nacionalidad, 
                docente,
                documento_identidad_tipo as t5
                WHERE  
                persona_nacionalidad.id_persona=persona.id_persona 
                and persona_nacionalidad.id_documento_identidad_tipo=t5.id_documento_identidad_tipo
                AND docente.id_persona=persona.id_persona");
          
                while($row=$query->fetch()){

                    $item=new Estructura();
                    $item->nacionalidad=$row['nacionalidad'];

                    $item->identificacion=$row['identificacion'];
                    $item->primer_nombre=$row['primer_nombre'];
                    $item->segundo_nombre=$row['segundo_nombre'];

                    $item->primer_apellido=$row['primer_apellido'];
                    $item->segundo_apellido=$row['segundo_apellido'];

                    $item->id_docente=$row['id_docente'];
                    $item->id_escalafon=$row['id_escalafon'];

                    array_push($items,$item);

                }
                return $items;
          
            }catch(PDOException $e){
                return [];
            }
          
        }
        //obtener estudios no conducentes a grado
        public function getDatosAcedemicosDocente($id_docente){
                    
            $items=[];
            try{
                $query=$this->db->connect()->prepare("SELECT 
                estudio_conducente.id_estudio_conducente,
                titulo_obtenido, 
                estatus_verificacion,
                estudio_conducente.id_estudio,
                estudio,
                estudio.id_tipo_estudio,
                tipo_estudio.descripcion as tipo_estudio,
                ano_estudio,
                estudio.id_pais,
                pais.descripcion as pais,
                estudio.id_modalidad_estudio,
                modalidad_estudio.descripcion as modalidad_estudio,
                estudio.id_institucion,
                institucion.descripcion as institucion,
                institucion.id_institucion_tipo,
                institucion_tipo.descripcion as  institucion_tipo,
                estudio.estatus,
                id_docente
                from
                estudio_conducente,
                estudio,
                tipo_estudio,
                pais,
                modalidad_estudio,
                institucion,
                institucion_tipo
                where
                estudio_conducente.id_estudio=estudio.id_estudio
                and estudio.id_tipo_estudio=tipo_estudio.id_tipo_estudio
                and estudio.id_pais=pais.id_pais
                and estudio.id_modalidad_estudio=modalidad_estudio.id_modalidad_estudio
                and estudio.id_institucion=institucion.id_institucion 
                and institucion.id_institucion_tipo=institucion_tipo.id_institucion_tipo 
                and estudio.id_tipo_estudio=2
                and estudio.id_docente=:id_docente;");
                
                $query->execute([
                    'id_docente'=>$id_docente
                    ]);

                while($row=$query->fetch(PDO::FETCH_ASSOC)){

                    $item=new Estructura();
                    $item->id_estudio=$row['id_estudio'];
                    $item->id_estudio_conducente=$row['id_estudio_conducente'];

                    $item->estudio=$row['estudio'];
                    $item->id_tipo_estudio=$row['id_tipo_estudio'];
                    $item->tipo_estudio=$row['tipo_estudio'];
                    $item->estatus=$row['estatus'];
                    
                    $item->id_pais=$row['id_pais'];
                    $item->pais=$row['pais'];
                    $item->id_modalidad_estudio=$row['id_modalidad_estudio'];
                    $item->modalidad_estudio=$row['modalidad_estudio'];
                    $item->ano_estudio=$row['ano_estudio'];
                    $item->id_institucion=$row['id_institucion'];
                    $item->institucion=$row['institucion'];
                    $item->id_institucion_tipo=$row['id_institucion_tipo'];
                    $item->institucion_tipo=$row['institucion_tipo'];

                    $item->id_docente=$row['id_docente'];

                    $item->titulo_obtenido=$row['titulo_obtenido'];
                    $item->estatus_verificacion=$row['estatus_verificacion'];
                    
                    

                    if($item->estatus=="concluido"){

                        $pdf=$this->db->connect()->prepare("SELECT 
                        certificado_titulo_estudio.id_documento, 
                        descripcion 
                        from 
                        documento, 
                        certificado_titulo_estudio 
                        where 
                        certificado_titulo_estudio.id_documento=documento.id_documento 
                        and id_estudio=:id_estudio;");
                        $pdf->execute(['id_estudio'=>$row['id_estudio']]);
                        
                        while($row2=$pdf->fetch(PDO::FETCH_ASSOC)){
                            $item->id_documento=$row2['id_documento'];
                            $item->descripcion_documento=$row2['descripcion'];

                        }

                    }
                    
                    array_push($items,$item);

                }
                return $items;
          
            }catch(PDOException $e){
                return [];
            }
          
        }


        public function getEstudiosConducentesDocente($id_docente){
                    
            $items=[];
            try{
                $query=$this->db->connect()->prepare("SELECT 
                estudio_conducente.id_estudio_conducente,
                titulo_obtenido, 
                estatus_verificacion,
                estudio_conducente.id_estudio,
                estudio,
                estudio.id_tipo_estudio,
                tipo_estudio.descripcion as tipo_estudio,
                ano_estudio,
                estudio.id_pais,
                pais.descripcion as pais,
                estudio.id_modalidad_estudio,
                modalidad_estudio.descripcion as modalidad_estudio,
                estudio.id_institucion,
                institucion.descripcion as institucion,
                institucion.id_institucion_tipo,
                institucion_tipo.descripcion as  institucion_tipo,
                estudio.estatus,
                id_docente,
                estudio_nivel.id_nivel_academico,
                nivel_academico.descripcion as nivel_academico,
                estudio_nivel.id_programa_nivel,
                programa_nivel.descripcion as programa_nivel
                from
                estudio_conducente,
                estudio,
                tipo_estudio,
                pais,
                modalidad_estudio,
                institucion,
                institucion_tipo,
                estudio_nivel,
                nivel_academico,
                programa_nivel
                where
                estudio_conducente.id_estudio=estudio.id_estudio
                and estudio.id_tipo_estudio=tipo_estudio.id_tipo_estudio
                and estudio.id_pais=pais.id_pais
                and estudio.id_modalidad_estudio=modalidad_estudio.id_modalidad_estudio
                and estudio.id_institucion=institucion.id_institucion 
                and institucion.id_institucion_tipo=institucion_tipo.id_institucion_tipo 
                and estudio_nivel.id_estudio=estudio.id_estudio
                and estudio_nivel.id_nivel_academico=nivel_academico.id_nivel_academico
                and estudio_nivel.id_programa_nivel=programa_nivel.id_programa_nivel
                and estudio.id_docente=:id_docente;");
                
                $query->execute([
                    'id_docente'=>$id_docente
                    ]);

                while($row=$query->fetch(PDO::FETCH_ASSOC)){

                    $item=new Estructura();
                    $item->id_estudio=$row['id_estudio'];
                    $item->id_estudio_conducente=$row['id_estudio_conducente'];

                    $item->estudio=$row['estudio'];
                    $item->id_tipo_estudio=$row['id_tipo_estudio'];
                    $item->tipo_estudio=$row['tipo_estudio'];
                    $item->estatus=$row['estatus'];
                    
                    $item->id_pais=$row['id_pais'];
                    $item->pais=$row['pais'];
                    $item->id_modalidad_estudio=$row['id_modalidad_estudio'];
                    $item->modalidad_estudio=$row['modalidad_estudio'];
                    $item->ano_estudio=$row['ano_estudio'];
                    $item->id_institucion=$row['id_institucion'];
                    $item->institucion=$row['institucion'];
                    $item->id_institucion_tipo=$row['id_institucion_tipo'];
                    $item->institucion_tipo=$row['institucion_tipo'];

                    $item->id_docente=$row['id_docente'];

                    $item->titulo_obtenido=$row['titulo_obtenido'];
                    $item->estatus_verificacion=$row['estatus_verificacion'];
                    
                    $item->id_nivel_academico=$row['id_nivel_academico'];
                    $item->nivel_academico=$row['nivel_academico'];
                    $item->id_programa_nivel=$row['id_programa_nivel'];
                    $item->programa_nivel=$row['programa_nivel'];

                    if($item->estatus=="concluido"){

                            $trabajo_grado = $this->db->connect()->prepare("SELECT * from trabajo_grado where id_estudio_conducente=:id_estudio_conducente");
                            $trabajo_grado->execute(['id_estudio_conducente'=>$row['id_estudio_conducente']]);
                            while($row1=$trabajo_grado->fetch(PDO::FETCH_ASSOC)){
                                $item->id_trabajo_grado=$row1['id_trabajo_grado'];
                                $item->titulo=$row1['titulo'];
                                $item->resumen=$row1['resumen'];
                                $item->id_estudio_conducente=$row1['id_estudio_conducente'];
        
                            }
    
                        $pdf=$this->db->connect()->prepare("SELECT 
                        certificado_titulo_estudio.id_documento, 
                        descripcion 
                        from 
                        documento, 
                        certificado_titulo_estudio 
                        where 
                        certificado_titulo_estudio.id_documento=documento.id_documento 
                        and id_estudio=:id_estudio;");
                        $pdf->execute(['id_estudio'=>$row['id_estudio']]);
                        
                        while($row2=$pdf->fetch(PDO::FETCH_ASSOC)){
                            $item->id_documento=$row2['id_documento'];
                            $item->descripcion_documento=$row2['descripcion'];

                        }

                    }
                    
                    array_push($items,$item);

                }
                return $items;
          
            }catch(PDOException $e){
                return [];
            }
          
        }


                //obtener estudios de idiomas
                public function getDatosAcademicosIdiomasDocente($id_docente){
            
                    try{
                        $items=[];
                        $query=$this->db->connect()->prepare("SELECT 
                        estudio_idioma.id_estudio_idioma,
                         
                        estudio_idioma.id_estudio,
                        estudio,
                        estudio.id_pais,
                        pais.descripcion as pais,
                        estudio.id_modalidad_estudio,
                        modalidad_estudio.descripcion as modalidad_estudio,
                        estudio.id_institucion,
                        institucion.descripcion as institucion,
                        institucion.id_institucion_tipo,
                        institucion_tipo.descripcion as institucion_tipo,
                        ano_estudio,
                        estudio_idioma.id_idioma,
                        idioma.name as idioma,
                        estudio.id_tipo_estudio,
                        tipo_estudio.descripcion as tipo_estudio,
                        estudio_idioma.nivel_lectura,
                        estudio_idioma.nivel_escritura,
                        estudio_idioma.nivel_habla,
                        estudio_idioma.nivel_comprende,
                        estudio.estatus,
                        estatus_verificacion
                        FROM 
                        estudio,
                        estudio_idioma,
                        idioma,
                        tipo_estudio,
                        pais,
                        modalidad_estudio,
                        institucion,
                        institucion_tipo
                        WHERE 
                        estudio_idioma.id_estudio=estudio.id_estudio
                        and estudio_idioma.id_idioma=idioma.id_idioma 
                        and estudio.id_tipo_estudio=tipo_estudio.id_tipo_estudio
                        and estudio.id_pais=pais.id_pais
                        and estudio.id_modalidad_estudio=modalidad_estudio.id_modalidad_estudio
                        and estudio.id_institucion=institucion.id_institucion 
                        and institucion.id_institucion_tipo=institucion_tipo.id_institucion_tipo 
                        and estudio.id_docente=:id_docente;");

                        $query->execute(['id_docente'=>$id_docente]);


                        while($row=$query->fetch(PDO::FETCH_ASSOC)){

                            $item=new Estructura();
                            $item->id_estudio=$row['id_estudio'];
                            $item->id_estudio_idioma=$row['id_estudio_idioma'];

                            $item->estudio=$row['estudio'];
                            $item->id_tipo_estudio=$row['id_tipo_estudio'];
                            $item->tipo_estudio=$row['tipo_estudio'];
                            $item->estatus=$row['estatus'];
                            $item->id_pais=$row['id_pais'];
                            $item->pais=$row['pais'];
                            $item->id_modalidad_estudio=$row['id_modalidad_estudio'];
                            $item->modalidad_estudio=$row['modalidad_estudio'];
                            $item->ano_estudio=$row['ano_estudio'];
                            $item->id_institucion=$row['id_institucion'];
                            $item->institucion=$row['institucion'];
                            $item->id_institucion_tipo=$row['id_institucion_tipo'];
                            $item->institucion_tipo=$row['institucion_tipo'];
        
                            $item->id_docente=$row['id_docente'];
        
                            $item->id_idioma=$row['id_idioma'];
                            $item->idioma=$row['idioma'];
        
                            $item->nivel_lectura=$row['nivel_lectura'];
                            $item->nivel_escritura=$row['nivel_escritura'];
                            $item->nivel_habla=$row['nivel_habla'];
                            $item->nivel_comprende=$row['nivel_comprende'];
        
                            $item->estatus_verificacion=$row['estatus_verificacion'];


                            array_push($items,$item);


                        }
                        return $items;


                    }catch(PDOExcepton $e){
                        return [];
                    }
        
                }
        

        public function verificacion($id){


            try{
                //1. guardas el objeto pdo en una variable
                $pdo=$this->db->connect();

                //2. comienzas transaccion
                $pdo->beginTransaction();

                $query=$pdo->prepare("UPDATE estudio SET estatus_verificacion='Verificado' WHERE id_estudio=:id_estudio;");
                $query->execute(['id_estudio'=>$id]);
                
                $pdo->commit();
                return true;
            } catch(PDOException $e){
                //5. regresas a un estado anterior en caso de error
                $pdo->rollBack();
                //echo "Fallo: " . $e->getMessage();
                return false;
            }
        }




        //obtener trabajos administrativos 
        public function getExperienciaAdministrativo($id_docente){
            $items=[];
            
            try{
                
                $query=$this->db->connect()->prepare("SELECT 
                id_trabajo_administrativo,
                institucion.descripcion as institucion,
                ano_experiencia,
                cargo_experiencia.descripcion as cargo,
                periodo,
                institucion.id_institucion_tipo,
                institucion_tipo.descripcion as institucion_tipo,
                trabajo_administrativo.id_experiencia_docente,
                experiencia_docente.id_tipo_experiencia,
                tipo_experiencia.descripcion as tipo_experiencia,
                institucion.id_pais,
                pais.descripcion as descripcion_pais,
                experiencia_docente.id_estado,
                estado.descripcion as descripcion_estado,
                estatus_verificacion,
                fecha_ingreso,
                fecha_egreso,
                trabajo_administrativo.id_experiencia_docente

                from 
                trabajo_administrativo, 
                experiencia_docente,
                tipo_experiencia,
                docente,
                institucion,
                institucion_tipo,
                cargo_experiencia,
                pais,
                estado
                where 
                trabajo_administrativo.id_experiencia_docente=experiencia_docente.id_experiencia_docente
                and experiencia_docente.id_tipo_experiencia=tipo_experiencia.id_tipo_experiencia
                and experiencia_docente.id_docente=docente.id_docente
                and experiencia_docente.id_institucion=institucion.id_institucion
                and institucion.id_institucion_tipo=institucion_tipo.id_institucion_tipo
                and experiencia_docente.id_cargo_experiencia=cargo_experiencia.id_cargo_experiencia
                and institucion.id_pais=pais.id_pais
                and experiencia_docente.id_estado=estado.id_estado
                and docente.id_docente=:id_docente");
                $query->execute(['id_docente'=>$id_docente]);

                while($row=$query->fetch()){
                    $item=new Estructura();

                    $item->id_trabajo_administrativo=$row['id_trabajo_administrativo'];
                    $item->id_experiencia_docente=$row['id_experiencia_docente'];

                    $item->institucion=$row['institucion'];
                    $item->ano_experiencia=$row['ano_experiencia'];
                    $item->cargo=$row['cargo'];
                    $item->periodo=$row['periodo'];
                    $item->institucion_tipo=$row['institucion_tipo'];

                    $item->tipo_entidad=$row['tipo_entidad'];
                    $item->id_experiencia_docente=$row['id_experiencia_docente'];
                    $item->id_tipo_experiencia=$row['id_tipo_experiencia'];
                    $item->tipo_experiencia=$row['tipo_experiencia'];
                    $item->id_institucion_tipo=$row['id_institucion_tipo'];

                    if($item->id_institucion_tipo==1){//1=institucion publica 
                        $constancia = $this->db->connect()->prepare("SELECT documento.descripcion from 
                        constancia_administrativo, 
                        documento 
                        where 
                        constancia_administrativo.id_documento=documento.id_documento
                        and id_trabajo_administrativo=:id_trabajo_administrativo;");
                        $constancia->execute(['id_trabajo_administrativo'=>$row['id_trabajo_administrativo']]);
                        while($row1=$constancia->fetch()){
                            $item->descripcion_documento=$row1['descripcion'];
                        }

                    }

                    $item->id_pais=$row['id_pais'];
                    $item->descripcion_pais=$row['descripcion_pais'];

                    $item->id_estado=$row['id_estado'];
                    $item->descripcion_estado=$row['descripcion_estado'];

                    $item->estatus_verificacion=$row['estatus_verificacion'];
                    $item->fecha_ingreso=date("d/m/Y", strtotime($row['fecha_ingreso']));
                    $item->fecha_egreso=date("d/m/Y", strtotime($row['fecha_egreso']));

                    array_push($items,$item);
          
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
        }

        public function verificacionExp($id){


            try{
                //1. guardas el objeto pdo en una variable
                $pdo=$this->db->connect();

                //2. comienzas transaccion
                $pdo->beginTransaction();

                $query=$pdo->prepare("UPDATE experiencia_docente SET estatus_verificacion='Verificado' WHERE id_experiencia_docente=:id_experiencia_docente;");
                $query->execute(['id_experiencia_docente'=>$id]);
                
                $pdo->commit();
                return true;
            } catch(PDOException $e){
                //5. regresas a un estado anterior en caso de error
                $pdo->rollBack();
                //echo "Fallo: " . $e->getMessage();
                return false;
            }
        }

      
        //obtener docencias Previas
        public function getExperienciaDocencia($id_docente){
            $items=[];
            
            try{
                
                //quede aqui
                $query=$this->db->connect()->prepare("SELECT 
                experiencia_docente.id_tipo_experiencia,
                tipo_experiencia.descripcion as tipo_experiencia, 

                institucion.id_institucion_tipo,
                cargo_experiencia.descripcion as cargo,
                ano_experiencia,
                institucion.id_pais,
                experiencia_docente.id_estado,
                experiencia_docente.id_institucion,
                institucion.descripcion as institucion,
                fecha_ingreso,
                fecha_egreso,
                
                institucion_tipo.descripcion as institucion_tipo,
                id_docencia_previa,
                docencia_previa.id_experiencia_docente,
                tipo_experiencia.descripcion as tipo_experiencia,
                pais.descripcion as descripcion_pais,
                experiencia_docente.id_estado,
                estado.descripcion as descripcion_estado,
                
                materia,
                docencia_previa.id_nivel_academico,
                nivel_academico.descripcion as nivel_academico,

                docencia_previa.id_modalidad_estudio,
                modalidad_estudio.descripcion as modalidad_estudio,

                semestre,
                periodo_lectivo,
                naturaleza,
                horas_semanales,
                estudiantes_atendidos,
                estatus_verificacion

                from 
                docencia_previa, 
                experiencia_docente,
                tipo_experiencia,
                docente,
                institucion,
                institucion_tipo,
                cargo_experiencia,
                pais,
                estado,
                nivel_academico,
                modalidad_estudio

                where 
                docencia_previa.id_experiencia_docente=experiencia_docente.id_experiencia_docente
                and experiencia_docente.id_tipo_experiencia=tipo_experiencia.id_tipo_experiencia
                and experiencia_docente.id_docente=docente.id_docente
                and experiencia_docente.id_institucion=institucion.id_institucion
                and institucion.id_institucion_tipo=institucion_tipo.id_institucion_tipo
                and experiencia_docente.id_cargo_experiencia=cargo_experiencia.id_cargo_experiencia
                and institucion.id_pais=pais.id_pais
                and experiencia_docente.id_estado=estado.id_estado
                and docencia_previa.id_nivel_academico=nivel_academico.id_nivel_academico
                and docencia_previa.id_modalidad_estudio=modalidad_estudio.id_modalidad_estudio
                and experiencia_docente.id_docente=:id_docente
                ");
                $query->execute(['id_docente'=>$id_docente]);

                while($row=$query->fetch()){
                    $item=new Estructura();
                    
                    $item->id_experiencia_docente=$row['id_experiencia_docente'];

                    $item->id_tipo_experiencia=$row['id_tipo_experiencia'];
                    $item->tipo_experiencia=$row['tipo_experiencia'];

                    $item->id_institucion_tipo=$row['id_institucion_tipo'];
                    $item->periodo=$row['periodo'];
                    $item->cargo=$row['cargo'];

                    $item->ano_experiencia=$row['ano_experiencia'];
                    $item->id_pais=$row['id_pais'];
                    $item->id_estado=$row['id_estado'];
                    $item->id_docencia_previa=$row['id_docencia_previa'];
                    
                    $item->modalidad_estudio=$row['modalidad_estudio'];
                    $item->nivel_academico=$row['nivel_academico'];

                    $item->ano_estudio=$row['ano_estudio'];
                    $item->id_institucion=$row['id_institucion'];

                    
                    $item->institucion=$row['institucion'];
                    $item->institucion_tipo=$row['institucion_tipo'];

                    
                    $item->id_docente=$row['id_docente'];

                    $item->fecha_ingreso=date("d-m-Y", strtotime($row['fecha_ingreso']));
                    $item->fecha_egreso=date("d-m-Y", strtotime($row['fecha_egreso']));
                    
                    $item->descripcion_pais=$row['descripcion_pais'];
                    $item->descripcion_estado=$row['descripcion_estado'];

                    $item->materia=$row['materia'];
                    $item->id_nivel_academico=$row['id_nivel_academico'];
                    $item->id_modalidad_estudio=$row['id_modalidad_estudio'];
                    $item->semestre=$row['semestre'];
                    $item->periodo_lectivo=$row['periodo_lectivo'];
                    $item->naturaleza=$row['naturaleza'];
                    $item->horas_semanales=$row['horas_semanales'];
                    $item->estudiantes_atendidos=$row['estudiantes_atendidos'];
                    $item->estatus_verificacion=$row['estatus_verificacion'];


          
                    array_push($items,$item);
          
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
        }

        //obtener comisiones gubernamental
        public function getExperienciaComision($id_docente){
            $items=[];
            
            try{
                
                //quede aqui
                $query=$this->db->connect()->prepare("SELECT 
                experiencia_docente.id_tipo_experiencia,
                tipo_experiencia.descripcion as tipo_experiencia, 

                institucion.id_institucion_tipo,
                cargo_experiencia.descripcion as cargo,
                ano_experiencia,
                institucion.id_pais,
                experiencia_docente.id_estado,
                experiencia_docente.id_institucion,
                institucion.descripcion as institucion,
                fecha_ingreso,
                fecha_egreso,
                
                institucion_tipo.descripcion as institucion_tipo,
                id_comision_gubernamental,
                comision_gubernamental.id_experiencia_docente,
                tipo_experiencia.descripcion as tipo_experiencia,
                pais.descripcion as descripcion_pais,
                experiencia_docente.id_estado,
                estado.descripcion as descripcion_estado,
                lugar,
                estatus_verificacion,
                id_comision_gubernamental

                from 
                comision_gubernamental, 
                experiencia_docente,
                tipo_experiencia,
                docente,
                institucion,
                institucion_tipo,
                cargo_experiencia,
                pais,
                estado

                where 
                comision_gubernamental.id_experiencia_docente=experiencia_docente.id_experiencia_docente
                and experiencia_docente.id_tipo_experiencia=tipo_experiencia.id_tipo_experiencia
                and experiencia_docente.id_docente=docente.id_docente
                and experiencia_docente.id_institucion=institucion.id_institucion
                and institucion.id_institucion_tipo=institucion_tipo.id_institucion_tipo
                and experiencia_docente.id_cargo_experiencia=cargo_experiencia.id_cargo_experiencia
                and institucion.id_pais=pais.id_pais
                and experiencia_docente.id_estado=estado.id_estado
                and experiencia_docente.id_docente=:id_docente;");
                $query->execute(['id_docente'=>$id_docente]);

                while($row=$query->fetch()){
                    $item=new Estructura();

                    $item->id_comision_gubernamental=$row['id_comision_gubernamental'];

                    $item->id_experiencia_docente=$row['id_experiencia_docente'];

                    $item->id_tipo_experiencia=$row['id_tipo_experiencia'];
                    $item->tipo_experiencia=$row['tipo_experiencia'];

                    $item->id_institucion_tipo=$row['id_institucion_tipo'];
                    $item->periodo=$row['periodo'];
                    $item->cargo=$row['cargo'];

                    $item->ano_experiencia=$row['ano_experiencia'];
                    $item->id_pais=$row['id_pais'];
                    $item->id_estado=$row['id_estado'];
                    $item->id_docencia_previa=$row['id_docencia_previa'];

                    $item->modalidad_estudio=$row['modalidad_estudio'];
                    $item->ano_estudio=$row['ano_estudio'];
                    $item->id_institucion=$row['id_institucion'];

                    
                    $item->institucion=$row['institucion'];
                    $item->institucion_tipo=$row['institucion_tipo'];

                    
                    $item->id_docente=$row['id_docente'];

                    $item->fecha_ingreso=date("m/d/Y", strtotime($row['fecha_ingreso']));
                    $item->fecha_egreso=date("m/d/Y", strtotime($row['fecha_egreso']));
                    
                    $item->descripcion_pais=$row['descripcion_pais'];
                    $item->descripcion_estado=$row['descripcion_estado'];

                    $item->lugar=$row['lugar'];
                    $item->estatus_verificacion=$row['estatus_verificacion'];

          
                    array_push($items,$item);
          
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
        }




         //obtener Libros
   public function getLibroDocente($id_docente){
            
    try{
        $items=[];
        $query=$this->db->connect()->prepare("SELECT 
       id_libro,
             libro.nombre_libro,
             libro.editorial,
             libro.isbn_libro,
             libro.ano_publicacion,
             libro.ciudad_publicacion,
             libro.volumenes,
             libro.nro_paginas,
             libro.url_libro,
                libro.id_docente,
                estatus_verificacion
        FROM 
        libro, docente
        WHERE 
        libro.id_docente=docente.id_docente
       
        and libro.id_docente=:id_docente;");

        $query->execute(['id_docente'=>$id_docente]);


        while($row=$query->fetch(PDO::FETCH_ASSOC)){

            $item=new Estructura();


            $item->id_libro=$row['id_libro'];
            $item->nombre_libro=$row['nombre_libro'];
            $item->editorial=$row['editorial'];
            $item->isbn_libro=$row['isbn_libro'];
            $item->ano_publicacion=$row['ano_publicacion'];
            $item->ciudad_publicacion=$row['ciudad_publicacion'];
            $item->volumenes=$row['volumenes'];
            $item->nro_paginas=$row['nro_paginas'];
            $item->url_libro=$row['url_libro'];
            $item->id_docente=$row['id_docente'];
            $item->estatus_verificacion=$row['estatus_verificacion'];


            array_push($items,$item);


        }
        return $items;


    }catch(PDOExcepton $e){
        return [];
    }

}


public function verificacionLibro($id){


    try{
        //1. guardas el objeto pdo en una variable
        $pdo=$this->db->connect();

        //2. comienzas transaccion
        $pdo->beginTransaction();

        $query=$pdo->prepare("UPDATE libro SET estatus_verificacion='Verificado' WHERE id_libro=:id_libro;");
        $query->execute(['id_libro'=>$id]);
        
        $pdo->commit();
        return true;
    } catch(PDOException $e){
        //5. regresas a un estado anterior en caso de error
        $pdo->rollBack();
        //echo "Fallo: " . $e->getMessage();
        return false;
    }
}




      //obtener articulos o revista
      public function getRevistaDocente($id_docente){
            
        try{
            $items=[];
            $query=$this->db->connect()->prepare("SELECT 
           id_articulo_revista,
                 articulo_revista.nombre_revista,
                 articulo_revista.titulo_articulo,
                 articulo_revista.issn_revista,
                 articulo_revista.ano_revista,
                 articulo_revista.nro_revista,
                 articulo_revista.nro_pag_inicial,
                 articulo_revista.url_revista,
                    articulo_revista.id_docente,
                    estatus_verificacion
            FROM 
            articulo_revista, docente
            WHERE 
            articulo_revista.id_docente=docente.id_docente
           
            and articulo_revista.id_docente=:id_docente;");
    
            $query->execute(['id_docente'=>$id_docente]);
    
    
            while($row=$query->fetch(PDO::FETCH_ASSOC)){
    
                $item=new Estructura();
    
    
                $item->id_articulo_revista=$row['id_articulo_revista'];
                $item->nombre_revista=$row['nombre_revista'];
                $item->titulo_articulo=$row['titulo_articulo'];
                $item->issn_revista=$row['issn_revista'];
                $item->ano_revista=$row['ano_revista'];
                $item->nro_revista=$row['nro_revista'];
                $item->nro_pag_inicial=$row['nro_pag_inicial'];
                $item->url_revista=$row['url_revista'];
                $item->estatus_verificacion=$row['estatus_verificacion'];
                $item->id_docente=$row['id_docente'];
    
    
                array_push($items,$item);
    
    
            }
            return $items;
    
    
        }catch(PDOExcepton $e){
            return [];
        }
    
    }



public function verificacionRevista($id){


    try{
        //1. guardas el objeto pdo en una variable
        $pdo=$this->db->connect();

        //2. comienzas transaccion
        $pdo->beginTransaction();

        $query=$pdo->prepare("UPDATE articulo_revista SET estatus_verificacion='Verificado' WHERE id_articulo_revista=:id_articulo_revista;");
        $query->execute(['id_articulo_revista'=>$id]);
        
        $pdo->commit();
        return true;
    } catch(PDOException $e){
        //5. regresas a un estado anterior en caso de error
        $pdo->rollBack();
        //echo "Fallo: " . $e->getMessage();
        return false;
    }
}





  //obtener articulos o revista
  public function getArtesoftwareDocente($id_docente){
            
    try{
        $items=[];
        $query=$this->db->connect()->prepare("SELECT 
       id_arte_software,
       arte_software.nombre_arte,
        arte_software.ano_arte,
        arte_software.entidad_promotora,
        arte_software.url_otro,
        arte_software.id_docente,
        estatus_verificacion

        FROM 
        arte_software, docente
        WHERE 
        arte_software.id_docente=docente.id_docente
       
        and arte_software.id_docente=:id_docente;");

        $query->execute(['id_docente'=>$id_docente]);


        while($row=$query->fetch(PDO::FETCH_ASSOC)){

            $item=new Estructura();


            $item->id_arte_software=$row['id_arte_software'];
                $item->nombre_arte=$row['nombre_arte'];
                $item->ano_arte=$row['ano_arte'];
                $item->entidad_promotora=$row['entidad_promotora'];
                $item->url_otro=$row['url_otro'];
                $item->estatus_verificacion=$row['estatus_verificacion'];
                $item->id_docente=$row['id_docente'];
               


            array_push($items,$item);


        }
        return $items;


    }catch(PDOExcepton $e){
        return [];
    }

}



public function verificacionArtesoftware($id){


try{
    //1. guardas el objeto pdo en una variable
    $pdo=$this->db->connect();

    //2. comienzas transaccion
    $pdo->beginTransaction();

    $query=$pdo->prepare("UPDATE arte_software SET estatus_verificacion='Verificado' WHERE id_arte_software=:id_arte_software;");
    $query->execute(['id_arte_software'=>$id]);
    
    $pdo->commit();
    return true;
} catch(PDOException $e){
    //5. regresas a un estado anterior en caso de error
    $pdo->rollBack();
    //echo "Fallo: " . $e->getMessage();
    return false;
}
}


//obtener articulos o revista
public function getPonenciaDocente($id_docente){
            
    try{
        $items=[];
        $query=$this->db->connect()->prepare("SELECT 
       id_ponencia,
       ponencia.nombre_ponencia,
             ponencia.nombre_evento,
             ponencia.ano_ponencia,
             ponencia.ciudad,
             ponencia.titulo_memoria,
             ponencia.volumen,
             ponencia.identificador,
             ponencia.pag_inicial,
             ponencia.formato,
             ponencia.url_ponencia,
             ponencia.id_docente,
             estatus_verificacion

        FROM 
        ponencia, docente
        WHERE 
        ponencia.id_docente=docente.id_docente
       
        and ponencia.id_docente=:id_docente;");

        $query->execute(['id_docente'=>$id_docente]);


        while($row=$query->fetch(PDO::FETCH_ASSOC)){

            $item=new Estructura();


            $item->id_ponencia=$row['id_ponencia'];
                $item->nombre_ponencia=$row['nombre_ponencia'];
                $item->nombre_evento=$row['nombre_evento'];
                $item->ano_ponencia=$row['ano_ponencia'];
                $item->ciudad=$row['ciudad'];
                $item->titulo_memoria=$row['titulo_memoria'];
                $item->volumen=$row['volumen'];
                $item->identificador=$row['identificador'];
                $item->pag_inicial=$row['pag_inicial'];
                $item->formato=$row['formato'];
                $item->url_ponencia=$row['url_ponencia'];
                $item->estatus_verificacion=$row['estatus_verificacion'];
                $item->id_docente=$row['id_docente'];
               


            array_push($items,$item);


        }
        return $items;


    }catch(PDOExcepton $e){
        return [];
    }

}



public function verificacionPonencia($id){


try{
    //1. guardas el objeto pdo en una variable
    $pdo=$this->db->connect();

    //2. comienzas transaccion
    $pdo->beginTransaction();

    $query=$pdo->prepare("UPDATE ponencia SET estatus_verificacion='Verificado' WHERE id_ponencia=:id_ponencia;");
    $query->execute(['id_ponencia'=>$id]);
    
    $pdo->commit();
    return true;
} catch(PDOException $e){
    //5. regresas a un estado anterior en caso de error
    $pdo->rollBack();
    //echo "Fallo: " . $e->getMessage();
    return false;
}
}



//obtener articulos o revista
public function getReconocimientoDocente($id_docente){
            
    try{
        $items=[];
        $query=$this->db->connect()->prepare("SELECT 
       id_reconocimiento,
       reconocimiento.descripcion,
       tipo_reconocimiento.descripcion as descripcion_tipo_reconocimiento,
        reconocimiento.entidad_promotora,
        reconocimiento.ano_reconocimiento,
        reconocimiento.caracter,
              documento.id_documento,
        reconocimiento.id_docente,
        estatus_verificacion,
        documento.descripcion as descripcion_reconocimiento

        FROM 
        reconocimiento, docente, tipo_reconocimiento, documento
        WHERE 
        reconocimiento.id_docente=docente.id_docente
         and reconocimiento.id_tipo_reconocimiento=tipo_reconocimiento.id_tipo_reconocimiento
            and reconocimiento.id_documento=documento.id_documento
             and reconocimiento.id_docente=:id_docente;");

        $query->execute(['id_docente'=>$id_docente]);


        while($row=$query->fetch(PDO::FETCH_ASSOC)){

            $item=new Estructura();


            $item->id_reconocimiento=$row['id_reconocimiento'];
                $item->descripcion=$row['descripcion'];
                
                $item->id_tipo_reconocimiento=$row['id_tipo_reconocimiento'];
                $item->descripcion_tipo=$row['descripcion_tipo_reconocimiento'];
               
                $item->entidad_promotora=$row['entidad_promotora'];
                $item->ano_reconocimiento=$row['ano_reconocimiento'];
                $item->caracter=$row['caracter'];
                $item->id_documento=$row['id_documento'];
                $item->id_docente=$row['id_docente'];
                $item->estatus_verificacion=$row['estatus_verificacion'];
                $item->descripcion_reconocimiento=$row['descripcion_reconocimiento'];



            array_push($items,$item);


        }
        return $items;


    }catch(PDOExcepton $e){
        return [];
    }

}



public function verificacionReconocimiento($id){


try{
    //1. guardas el objeto pdo en una variable
    $pdo=$this->db->connect();

    //2. comienzas transaccion
    $pdo->beginTransaction();

    $query=$pdo->prepare("UPDATE reconocimiento SET estatus_verificacion='Verificado' WHERE id_reconocimiento=:id_reconocimiento;");
    $query->execute(['id_reconocimiento'=>$id]);
    
    $pdo->commit();
    return true;
} catch(PDOException $e){
    //5. regresas a un estado anterior en caso de error
    $pdo->rollBack();
    //echo "Fallo: " . $e->getMessage();
    return false;
}
}



 //obtener diseno uc docente
 public function getDisenoDocente($id_docente){
            
    try{
        $items=[];
        $query=$this->db->connect()->prepare("SELECT 
       id_diseno_uc,
       diseno_uc_docente.nombre AS diseno_uc,
       diseno_uc_docente.ano_creacion,
       diseno_uc_docente.id_tramo,
       tramo.descripcion AS tramo,
       diseno_uc_docente.id_unidad_curricular_tipo,
       unidad_curricular_tipo.descripcion AS tipo_uc,
       diseno_uc_docente.id_programa,
       programa.descripcion AS programa,
       diseno_uc_docente.id_area_conocimiento_ubv,
       area_conocimiento_ubv.descripcion AS area_ubv,
       diseno_uc_docente.id_documento,
       documento.descripcion AS documento,
       diseno_uc_docente.id_docente,
        estatus_verificacion

        FROM 
        diseno_uc_docente,tramo, unidad_curricular_tipo, 
        programa, area_conocimiento_ubv, documento, docente
        WHERE diseno_uc_docente.id_tramo=tramo.id_tramo
        and diseno_uc_docente.id_unidad_curricular_tipo=unidad_curricular_tipo.id_unidad_curricular_tipo
        and diseno_uc_docente.id_programa=programa.id_programa
        and diseno_uc_docente.id_area_conocimiento_ubv=area_conocimiento_ubv.id_area_conocimiento_ubv
        and diseno_uc_docente.id_documento=documento.id_documento
        and diseno_uc_docente.id_docente=docente.id_docente 
        and diseno_uc_docente.id_docente=:id_docente;");

        $query->execute(['id_docente'=>$id_docente]);


        while($row=$query->fetch(PDO::FETCH_ASSOC)){

            $item=new Estructura();
            $item->id_diseno_uc=$row['id_diseno_uc'];
            $item->nombre=$row['diseno_uc'];
            $item->ano_creacion=$row['ano_creacion'];
            $item->id_tramo=$row['id_tramo'];
            $item->descripcion_tramo=$row['tramo'];
            $item->id_unidad_curricular_tipo=$row['id_unidad_curricular_tipo'];
            $item->descripcion_tipo_uc=$row['tipo_uc'];
            $item->id_programa=$row['id_programa'];
            $item->descripcion_programa=$row['programa'];
            $item->id_area_conocimiento_ubv=$row['id_area_conocimiento_ubv'];
            $item->descripcion_area_ubv=$row['area_ubv'];
            $item->id_documento=$row['id_documento'];
            $item->descripcion_documento=$row['documento'];
            $item->estatus_verificacion=$row['estatus_verificacion'];
            $item->id_docente=$row['id_docente'];
               


            array_push($items,$item);


        }
        return $items;


    }catch(PDOExcepton $e){
        return [];
    }

}



public function verificacionDiseno($id){


try{
    //1. guardas el objeto pdo en una variable
    $pdo=$this->db->connect();

    //2. comienzas transaccion
    $pdo->beginTransaction();

    $query=$pdo->prepare("UPDATE diseno_uc_docente SET estatus_verificacion='Verificado' WHERE id_diseno_uc=:id_diseno_uc;");
    $query->execute(['id_diseno_uc'=>$id]);
    
    $pdo->commit();
    return true;
} catch(PDOException $e){
    //5. regresas a un estado anterior en caso de error
    $pdo->rollBack();
    //echo "Fallo: " . $e->getMessage();
    return false;
}
}


//obtener tutoria o pasantias
public function gettutoriaDocente($id_docente){
            
    try{
        $items=[];
        $query=$this->db->connect()->prepare("SELECT 
        id_tutoria as id_tutoria,
        tutoria.ano_tutoria as ano_tutoria,
        tutoria.periodo_lectivo as periodo_lectivo,
        tutoria.id_docente,
        tutoria.id_area_conocimiento_ubv as id_area_conocimiento_ubv,
        area_conocimiento_ubv.descripcion AS area_ubv,
        tutoria.id_linea_investigacion as id_linea,
        linea_investigacion.descripcion as linea,
        tutoria.id_nucleo_academico as id_nucleo,
        nucleo_academico.descripcion as nucleo,
        tutoria.id_centro_estudio as id_centro,
        centro_estudio.descripcion as centro,
        tutoria.id_programa as id_programa,
        programa.descripcion as programa,
        tutoria.id_eje_regional as id_eje_regional,
        eje_regional.descripcion as eje_regional,
        tutoria.nro_estudiantes as nro_estudiantes,
        tutoria.descripcion as tutoria,
        estatus_verificacion
        FROM tutoria, 
        area_conocimiento_ubv,
        linea_investigacion,
        nucleo_academico, 
        centro_estudio, programa, 
        eje_regional,
        docente
        WHERE tutoria.id_area_conocimiento_ubv=area_conocimiento_ubv.id_area_conocimiento_ubv
        and tutoria.id_linea_investigacion=linea_investigacion.id_linea_investigacion
        and tutoria.id_nucleo_academico=nucleo_academico.id_nucleo_academico
        and tutoria.id_centro_estudio=centro_estudio.id_centro_estudio
        and tutoria.id_programa=programa.id_programa
        and tutoria.id_eje_regional=eje_regional.id_eje_regional
        and tutoria.id_docente=docente.id_docente 
        and tutoria.id_docente=:id_docente;");

        $query->execute(['id_docente'=>$id_docente]);


        while($row=$query->fetch(PDO::FETCH_ASSOC)){

            $item=new Estructura();
            $item->id_tutoria=$row['id_tutoria'];
            $item->ano_tutoria=$row['ano_tutoria'];
            $item->periodo_lectivo=$row['periodo_lectivo'];
            $item->id_area_conocimiento_ubv=$row['id_area_conocimiento_ubv'];
            $item->descripcion_area_ubv=$row['area_ubv'];
            $item->id_linea_investigacion=$row['id_linea'];
            $item->descripcion_linea=$row['linea'];
            $item->id_nucleo_academico=$row['id_nucleo'];
            $item->descripcion_nucleo=$row['nucleo'];
            $item->id_centro_estudio=$row['id_centro'];
            $item->descripcion_centro=$row['centro'];
            $item->id_programa=$row['id_programa'];
            $item->descripcion_programa=$row['programa'];
            $item->id_eje_regional=$row['id_eje_regional'];
            $item->descripcion_eje_regional=$row['eje_regional'];
            $item->nro_estudiantes=$row['nro_estudiantes'];
            $item->descripcion=$row['tutoria'];
            $item->estatus_verificacion=$row['estatus_verificacion'];
            $item->id_docente=$row['id_docente'];
               


            array_push($items,$item);


        }
        return $items;


    }catch(PDOExcepton $e){
        return [];
    }

}



public function verificacionTutoria($id){


try{
    //1. guardas el objeto pdo en una variable
    $pdo=$this->db->connect();

    //2. comienzas transaccion
    $pdo->beginTransaction();

    $query=$pdo->prepare("UPDATE tutoria SET estatus_verificacion='Verificado' WHERE id_tutoria=:id_tutoria;");
    $query->execute(['id_tutoria'=>$id]);
    
    $pdo->commit();
    return true;
} catch(PDOException $e){
    //5. regresas a un estado anterior en caso de error
    $pdo->rollBack();
    //echo "Fallo: " . $e->getMessage();
    return false;
}
}

 //obtener docencia_previa_ubv
 public function getDocenciaUBVDocente($id_docente){
            
    try{
        $items=[];
        $query=$this->db->connect()->prepare("SELECT 
        id_docencia_previa_ubv as id_docencia_previa_ubv,
        docencia_previa_ubv.id_area_conocimiento_ubv as id_area_conocimiento_ubv,
        area_conocimiento_ubv.descripcion as area_ubv,
        docencia_previa_ubv.id_docente,
        docencia_previa_ubv.id_programa as id_programa,
        programa.descripcion as programa,
        docencia_previa_ubv.id_eje_regional as id_eje_regional,
        eje_regional.descripcion as eje_regional,
        docencia_previa_ubv.id_eje_municipal as id_eje_municipal,
        eje_municipal.descripcion as eje_municipal,
        docencia_previa_ubv.id_aldea as id_aldea,
        aldea.descripcion as aldea,
        docencia_previa_ubv.id_centro_estudio as id_centro,
        centro_estudio.descripcion as centro,
        docencia_previa_ubv.fecha_ingreso as fecha_ingreso,
         estatus_verificacion
        FROM docencia_previa_ubv, 
        area_conocimiento_ubv,
        programa, eje_regional, eje_municipal, aldea,
        centro_estudio,
        docente
        WHERE docencia_previa_ubv.id_area_conocimiento_ubv=area_conocimiento_ubv.id_area_conocimiento_ubv
        and docencia_previa_ubv.id_programa=programa.id_programa
        and docencia_previa_ubv.id_eje_regional=eje_regional.id_eje_regional
        and docencia_previa_ubv.id_eje_municipal=eje_municipal.id_eje_municipal
        and docencia_previa_ubv.id_aldea=aldea.id_aldea
        and docencia_previa_ubv.id_centro_estudio=centro_estudio.id_centro_estudio
        and docencia_previa_ubv.id_docente=docente.id_docente 
        and docencia_previa_ubv.id_docente=:id_docente;");

        $query->execute(['id_docente'=>$id_docente]);


        while($row=$query->fetch(PDO::FETCH_ASSOC)){

            $item=new Estructura();
            $item->id_docencia_previa_ubv=$row['id_docencia_previa_ubv'];
            $item->id_area_conocimiento_ubv=$row['id_area_conocimiento_ubv'];
            $item->descripcion_area_ubv=$row['area_ubv'];
            $item->id_programa=$row['id_programa'];
            $item->descripcion_programa=$row['programa'];
            $item->id_eje_regional=$row['id_eje_regional'];
            $item->descripcion_eje_regional=$row['eje_regional'];
            $item->id_eje_municipal=$row['id_eje_municipal'];
            $item->descripcion_eje_municipal=$row['eje_municipal'];
            $item->id_aldea=$row['id_aldea'];
            $item->descripcion_aldea=$row['aldea'];
            $item->id_centro_estudio=$row['id_centro'];
            $item->descripcion_centro=$row['centro'];
            $item->fecha_ingreso=$row['fecha_ingreso'];
            $item->estatus_verificacion=$row['estatus_verificacion'];
            $item->id_docente=$row['id_docente'];
               


            array_push($items,$item);


        }
        return $items;


    }catch(PDOExcepton $e){
        return [];
    }

}



public function verificacionDocencia($id){


try{
    //1. guardas el objeto pdo en una variable
    $pdo=$this->db->connect();

    //2. comienzas transaccion
    $pdo->beginTransaction();

    $query=$pdo->prepare("UPDATE docencia_previa_ubv SET estatus_verificacion='Verificado' WHERE id_docencia_previa_ubv=:id_docencia_previa_ubv;");
    $query->execute(['id_docencia_previa_ubv'=>$id]);
    
    $pdo->commit();
    return true;
} catch(PDOException $e){
    //5. regresas a un estado anterior en caso de error
    $pdo->rollBack();
    //echo "Fallo: " . $e->getMessage();
    return false;
}
}


//obtener docencia_previa_ubv
public function getComisionexcDocente($id_docente){
            
    try{
        $items=[];
        $query=$this->db->connect()->prepare("SELECT 
        id_comision_excedencia as id_comision_excedencia,
        comision_excedencia.id_tipo_info as id_tipo_info,
        tipo_info.descripcion as tipo_info,
        comision_excedencia.lugar as lugar,
        comision_excedencia.desde as desde,
        comision_excedencia.hasta as hasta,
        comision_excedencia.designacion_funcion as designacion_funcion,
        comision_excedencia.causa as causa,
        comision_excedencia.aprobacion as aprobacion,
        comision_excedencia.id_docente as id_docente,
         estatus_verificacion
        FROM comision_excedencia, 
        tipo_info,
        docente
        WHERE comision_excedencia.id_tipo_info=tipo_info.id_tipo_info
        and comision_excedencia.id_docente=docente.id_docente 
        and comision_excedencia.id_docente=:id_docente;");

        $query->execute(['id_docente'=>$id_docente]);


        while($row=$query->fetch(PDO::FETCH_ASSOC)){

            $item=new Estructura();
            $item->id_comision_excedencia=$row['id_comision_excedencia'];
            $item->id_tipo_info=$row['id_tipo_info'];
            $item->descripcion_tipo_info=$row['tipo_info'];
            $item->lugar=$row['lugar'];
            $item->desde=$row['desde'];
            $item->hasta=$row['hasta'];
            $item->designacion_funcion=$row['designacion_funcion'];
            $item->causa=$row['causa'];
            $item->aprobacion=$row['aprobacion'];
            $item->estatus_verificacion=$row['estatus_verificacion'];
            $item->id_docente=$row['id_docente'];
               


            array_push($items,$item);


        }
        return $items;


    }catch(PDOExcepton $e){
        return [];
    }

}



public function verificacionComisionexc($id){


try{
    //1. guardas el objeto pdo en una variable
    $pdo=$this->db->connect();

    //2. comienzas transaccion
    $pdo->beginTransaction();

    $query=$pdo->prepare("UPDATE comision_excedencia SET estatus_verificacion='Verificado' WHERE id_comision_excedencia=:id_comision_excedencia;");
    $query->execute(['id_comision_excedencia'=>$id]);
    
    $pdo->commit();
    return true;
} catch(PDOException $e){
    //5. regresas a un estado anterior en caso de error
    $pdo->rollBack();
    //echo "Fallo: " . $e->getMessage();
    return false;
}
}




















        public function getDocenteByid($id){
            try{

                $detalle=$this->db->connect()->prepare("SELECT 
                substr(t5.descripcion, 1, 1) as nacionalidad,
                identificacion,
                primer_nombre, 
                primer_apellido
                from 
                docente as t1, 
                persona as t2,
                persona_nacionalidad as t3,
                documento_identidad_tipo as t5
                where
                t1.id_persona=t2.id_persona
                and t3.id_persona=t2.id_persona
                and t3.id_documento_identidad_tipo=t5.id_documento_identidad_tipo
                and id_docente=:id_docente;
                ");
                $detalle->execute(['id_docente'=>$id]);
                $item=new Estructura();

                while($row=$detalle->fetch(PDO::FETCH_ASSOC)){
                    $item->nacionalidad=$row['nacionalidad'];
                    $item->identificacion=$row['identificacion'];
                    $item->primer_nombre=$row['primer_nombre'];
                    $item->primer_apellido=$row['primer_apellido'];

                }

                return $item;
            }catch(PDOExcepton $e){
                return [];
            }
        
        }

        





    }

    
        
    

?>