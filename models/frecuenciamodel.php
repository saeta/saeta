<?php
include_once 'datosacademicos/frecuencia.php';

class FrecuenciaModel extends Model{
    public function __construct(){
        parent::__construct();
    }


    public function existe($frecuencia){
        try{
    
            //var_dump($frecuencia);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM frecuencia WHERE descripcion=:descripcion");
            $sql->execute(['descripcion' =>$frecuencia]);
            $nombre=$sql->fetch();
            
            if($frecuencia==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }

    public function insert($datos){
        //echo "<br>insertar datos";
    //var_dump($datos);
        try{
         
            $query=$this->db->connect()->prepare('INSERT INTO frecuencia(descripcion) VALUES(:descripcion)');
    
                $query->execute([
                    'descripcion'=>$datos['descripcion'],
                ]);
    
                return true;
          
        }catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                 }
        
        
    }

    

    public function getFrecuencia(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT * FROM frecuencia");
            
            while($row=$query->fetch()){
                
                $item=new FrecuenciaC();
                $item->id_frecuencia=$row['id_frecuencia'];
                $item->descripcion=$row['descripcion'];
            
                array_push($items,$item);
            
            
            }
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }
      
    public function update($datos){
   //var_dump($datos);
        //var_dump($item['id_pais'],$item['estado'],$item['codigoine'],$item['id_estado']);
        $query=$this->db->connect()->prepare("UPDATE frecuencia SET descripcion=:descripcion
        WHERE id_frecuencia=:id_frecuencia");
        
        try{
            $query->execute([
                'id_frecuencia'=>$datos['id_frecuencia'],
                'descripcion'=>$datos['descripcion'],         
                     ]);
         return true;

        }catch(PDOException $e){
             return false;
        }
         
    }

        public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM frecuencia WHERE id_frecuencia = :id_frecuencia");
    
            try{
                $query->execute([
                'id_frecuencia'=>$id
                ]);
                return true;
            }catch(PDOException $e){
                return false;
            }
        }
    }


    ?>