<?php
include_once 'models/estructura.php';
class ParroquiaModel extends Model{
    public function __construct(){
    parent::__construct();
    }
    public function existe($codigoine){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion,codigo FROM parroquia WHERE codigo=:codigo");
            $sql->execute(['codigo' =>$codigoine]);
            $nombre=$sql->fetch();
            
            if($codigoine==$nombre['codigo']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
    //echo "<br>insertar datos";

    try{
     
             $query=$this->db->connect()->prepare('INSERT INTO parroquia(descripcion, codigo, id_municipio, id_eje_municipal) VALUES
             (:descripcion, :codigo, :id_municipio,:id_eje_municipal)');

            $query->execute(['descripcion'=>$datos['parroquia'],'codigo'=>$datos['codigoine'],'id_municipio'=>$datos['municipio'],
            'id_eje_municipal'=>$datos['eje_municipal']]);

            return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }
    
    
            }
            public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT id_parroquia,parroquia.descripcion AS parroquia,parroquia.codigo AS codigo,municipio.id_municipio,eje_municipal.id_eje_municipal,municipio.descripcion AS municipio,eje_municipal.descripcion AS eje_municipal,eje_municipal.estatus AS  eje_municipal_estutus FROM parroquia,municipio,eje_municipal WHERE parroquia.id_municipio=municipio.id_municipio AND parroquia.id_eje_municipal=eje_municipal.id_eje_municipal");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_parroquia=$row['id_parroquia'];
              $item->parroquia=$row['parroquia'];
              $item->codigo=$row['codigo'];
              $item->id_municipio=$row['id_municipio'];          
              $item->municipio=$row['municipio'];
              $item->id_eje_municipal=$row['id_eje_municipal'];
              $item->eje_municipal=$row['eje_municipal'];
              $item->eje_municipal_estutus=$row['eje_municipal_estutus'];
              

              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }

            public function getMunicipio(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT * FROM municipio");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_municipio=$row['id_municipio'];
              $item->descripcion=$row['descripcion'];
              $item->codigo=$row['codigo'];
              $item->id_estado=$row['id_estado'];
              
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }
              public function getEjeMunicipal(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT * FROM eje_municipal WHERE estatus ='1'");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_eje_municipal=$row['id_eje_municipal'];
              $item->descripcion=$row['descripcion'];
              $item->id_eje_regional=$row['id_eje_regional'];
              $item->estatus=$row['estatus'];
             
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }
             


              public function update($item){
   
         //  var_dump($item['parroquia'],$item['codigoine'],$item['id_municipio'],$item['id_eje_municipal'],$item['id_parroquia']);
              $query=$this->db->connect()->prepare("UPDATE parroquia SET descripcion= :descripcion,codigo= :codigo,id_municipio=:id_municipio,id_eje_municipal=:id_eje_municipal WHERE id_parroquia= :id_parroquia");
            try{
                        $query->execute([
                        'descripcion'=>$item['parroquia'],
                        'codigo'=>$item['codigoine'],                        
                        'id_municipio'=>$item['id_municipio'],
                        'id_eje_municipal'=>$item['id_eje_municipal'],
                        'id_parroquia'=>$item['id_parroquia'],
  
                        
                        ]);
            return true;
                       
                        
            }catch(PDOException $e){
                return false;
                 }
            
                }
public function delete($id_publicacion){
   

    $query2=$this->db->connect()->prepare("SELECT nomb_archivo FROM publicacion WHERE id_publicacion=:id_publicacion");
    $query2->execute([
        'id_publicacion'=>$id_publicacion,
        ]);
    $nomb_archivo=$query2->fetch();
   
//var_dump($nomb_archivo);

  //  unlink("src/multimedia/".$nomb_archivo);//como diferencia si el msmo nombre es el mismo en todas las fotos

  list($img1,$img2,$img3,$img4) = explode(',', $nomb_archivo);

$ruta1="src/multimedia/".$img1;
var_dump($ruta1);
unlink($ruta1);
$ruta2="src/multimedia/".$img2;
unlink($ruta2);
var_dump($ruta2);
$ruta3="src/multimedia/".$img3;
unlink($ruta3);
var_dump($ruta3);
$ruta4="src/multimedia/".$img4;
unlink($ruta4);
var_dump($ruta4);
$query=$this->db->connect()->prepare("DELETE FROM publicacion WHERE id_publicacion = :id_publicacion");

        try{
        $query->execute([
            'id_publicacion'=>$id_publicacion,
            ]);
                return true;



           }catch(PDOException $e){
return false;
}


    }






    }

    ?>