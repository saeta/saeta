<?php
include_once 'datosacademicos/modalidad_estudio.php';

class MestudioModel extends Model{
    public function __construct(){
        parent::__construct();
    }


    public function existe($modalidad_estudio){
        try{
    
            //var_dump($modalidad_estudio);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM modalidad_estudio WHERE descripcion=:descripcion");
            $sql->execute(['descripcion' =>$modalidad_estudio]);
            $nombre=$sql->fetch();
            
            if($modalidad_estudio==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }

    public function insert($datos){
        //echo "<br>insertar datos";
    
        try{
         
            $query=$this->db->connect()->prepare('INSERT INTO modalidad_estudio(descripcion) VALUES(:descripcion)');
    
                $query->execute([
                    'descripcion'=>$datos['descripcion'],
                ]);
    
                return true;
          
        }catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                 }
        
        
    }

    

    public function get(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT * FROM modalidad_estudio");
            
            while($row=$query->fetch()){
                
                $item=new MEstudios();
                $item->id_modalidad_estudio=$row['id_modalidad_estudio'];
                $item->descripcion=$row['descripcion'];
            
                array_push($items,$item);
            
            
            }
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }
      
    public function update($datos){
   //var_dump($datos);
        //var_dump($item['id_pais'],$item['estado'],$item['codigoine'],$item['id_estado']);
        $query=$this->db->connect()->prepare("UPDATE modalidad_estudio SET descripcion=:descripcion
        WHERE id_modalidad_estudio=:id_modalidad_estudio");
        
        try{
            $query->execute([
                'id_modalidad_estudio'=>$datos['id_modalidad_estudio'],
                'descripcion'=>$datos['descripcion'],         
                     ]);
         return true;

        }catch(PDOException $e){
             return false;
        }
         
    }

        public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM modalidad_estudio WHERE id_modalidad_estudio = :id_modalidad_estudio");
    
            try{
                $query->execute([
                'id_modalidad_estudio'=>$id
                ]);
                return true;
            }catch(PDOException $e){
                return false;
            }
        }
    }


    ?>