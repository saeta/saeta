<?php
include_once 'models/estructura.php';
class EjemModel extends Model{
    public function __construct(){
    parent::__construct();
    }
    public function existe($codigoejem){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion,codigo FROM eje_municipal WHERE codigo=:codigo");
            $sql->execute(['codigo' =>$codigoejem]);
            $nombre=$sql->fetch();
            
            if($codigoejem==$nombre['codigo']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
    //echo "<br>insertar datos";



    try{
       // var_dump($datos['ejeregional'],$datos['codigoine'], $datos['estatus']);
     
             $query=$this->db->connect()->prepare('INSERT INTO eje_municipal(descripcion, codigo,id_eje_regional,estatus) VALUES
             (:descripcion, :codigo,:id_eje_regional, :estatus)');

            $query->execute(['descripcion'=>$datos['ejemunicipal'],'codigo'=>$datos['codigoejem'],
            'id_eje_regional'=>$datos['ejeregional'],'estatus'=>$datos['estatus']]);

            return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }
    
    
            }
            public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT 
              id_eje_municipal,
              eje_municipal.descripcion AS eje_municipal,
              eje_municipal.codigo AS codigo,
              eje_municipal.estatus AS estatus_eje,
              eje_regional.id_eje_regional,
              eje_regional.descripcion AS eje_regional,
              eje_regional.estatus AS estatus_eje_regional   
              FROM eje_regional,eje_municipal WHERE eje_municipal.id_eje_regional=eje_regional.id_eje_regional");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_eje_municipal=$row['id_eje_municipal'];
              $item->eje_municipal=$row['eje_municipal'];
              $item->codigo=$row['codigo'];
              $item->estatus_eje=$row['estatus_eje'];

              $item->id_eje_regional=$row['id_eje_regional'];
              $item->eje_regional=$row['eje_regional'];             
              $item->estatus_eje_regional=$row['estatus_eje_regional'];
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }
            public function getEjeRegional(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT *from eje_regional WHERE estatus='1'");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_eje_regional=$row['id_eje_regional'];
              $item->descripcion=$row['descripcion'];
              $item->codigo=$row['codigo'];
              $item->estatus=$row['estatus'];
             
            
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }
          



public function update($item){
           
   //var_dump($item['eje'],$item['codigoEje'],$item['estatus'],$item['id_eje_regional']);

         $query=$this->db->connect()->prepare("UPDATE eje_municipal SET descripcion= :descripcion,codigo= :codigo,id_eje_regional=:id_eje_regional,estatus=:estatus WHERE id_eje_municipal= :id_eje_municipal");
     try{
                 $query->execute([
                 'descripcion'=>$item['ejemunicipal'],
                 'codigo'=>$item['codigoeje'],
                 'estatus'=>$item['estatus'],          
                 'id_eje_regional'=>$item['id_eje_regional'],
                 'id_eje_municipal'=>$item['id_eje_municipal'],
                 
                 ]);
     return true;
                
                 
     }catch(PDOException $e){
         return false;
          }
     
         }




    }

    ?>