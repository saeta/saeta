<?php
include_once 'models/estructura.php';
class AldeatelfModel extends Model{
    public function __construct(){
    parent::__construct();
    }
    public function existe($numero){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT aldea_telefono.descripcion AS aldea_telefono,telefono_codigo_area.descripcion AS telefono_codigo_area  FROM aldea_telefono,telefono_codigo_area WHERE aldea_telefono.descripcion=:descripcion");
            $sql->execute(['descripcion' =>$numero]);
            $nombre=$sql->fetch();
            
            if($numero==$nombre['aldea_telefono']){
                
                return $nombre['telefono_codigo_area'].$nombre['aldea_telefono'] ;
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
    //echo "<br>insertar datos";
            
    try{
     
             $query=$this->db->connect()->prepare('INSERT INTO aldea_telefono(descripcion,id_telefono_codigo_area,id_aldea) VALUES (:descripcion,:id_telefono_codigo_area,:id_aldea)');
             $query->execute(['descripcion'=>$datos['numero'],'id_telefono_codigo_area'=>$datos['codigoarea'],'id_aldea'=>$datos['aldea']]);

            return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }
    
    
            }
            
          
              public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT aldea.id_aldea,aldea.descripcion AS aldea,telefono_codigo_area.id_telefono_codigo_area ,telefono_codigo_area.descripcion AS telefono_codigo_area, aldea_telefono.id_aldea_telefono,aldea_telefono.descripcion AS aldea_telefono FROM telefono_codigo_area,aldea_telefono,aldea WHERE aldea.id_aldea=aldea_telefono.id_aldea AND aldea_telefono.id_telefono_codigo_area=telefono_codigo_area.id_telefono_codigo_area");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_aldea=$row['id_aldea'];
              $item->aldea=$row['aldea'];
              $item->id_telefono_codigo_area=$row['id_telefono_codigo_area'];
              $item->telefono_codigo_area=$row['telefono_codigo_area'];
              $item->id_aldea_telefono=$row['id_aldea_telefono'];
              $item->aldea_telefono=$row['aldea_telefono'];
           
              
            
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }


              public function getCodArea(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT *FROM telefono_codigo_area");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_telefono_codigo_area=$row['id_telefono_codigo_area'];
              $item->descripcion=$row['descripcion'];
              
            
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }
              public function getAldea(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT *FROM aldea WHERE estatus='1'");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_aldea=$row['id_aldea'];
              $item->descripcion=$row['descripcion'];
              
            
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }

              public function update($item){
   
             //var_dump($item['id_aldea_telefono']);
              $query=$this->db->connect()->prepare("UPDATE aldea_telefono SET descripcion= :descripcion,id_telefono_codigo_area=:id_telefono_codigo_area,id_aldea=:id_aldea WHERE id_aldea_telefono= :id_aldea_telefono");
            try{
                        $query->execute([
                        'descripcion'=>$item['numeroo'],  
                        'id_telefono_codigo_area'=>$item['codigoareaa'],  
                        'id_aldea'=>$item['aldeaa'],            
                        'id_aldea_telefono'=>$item['id_aldea_telefono'],

  
                        
                        ]);
            return true;
                       
                        
            }catch(PDOException $e){
                return false;
                 }
            
                }
public function delete($id_publicacion){
   

    $query2=$this->db->connect()->prepare("SELECT nomb_archivo FROM publicacion WHERE id_publicacion=:id_publicacion");
    $query2->execute([
        'id_publicacion'=>$id_publicacion,
        ]);
    $nomb_archivo=$query2->fetch();
   
//var_dump($nomb_archivo);

  //  unlink("src/multimedia/".$nomb_archivo);//como diferencia si el msmo nombre es el mismo en todas las fotos

  list($img1,$img2,$img3,$img4) = explode(',', $nomb_archivo);

$ruta1="src/multimedia/".$img1;
var_dump($ruta1);
unlink($ruta1);
$ruta2="src/multimedia/".$img2;
unlink($ruta2);
var_dump($ruta2);
$ruta3="src/multimedia/".$img3;
unlink($ruta3);
var_dump($ruta3);
$ruta4="src/multimedia/".$img4;
unlink($ruta4);
var_dump($ruta4);
$query=$this->db->connect()->prepare("DELETE FROM publicacion WHERE id_publicacion = :id_publicacion");

        try{
        $query->execute([
            'id_publicacion'=>$id_publicacion,
            ]);
                return true;



           }catch(PDOException $e){
return false;
}


    }






    }

    ?>