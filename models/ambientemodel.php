<?php
include_once 'models/estructura.php';
class AmbienteModel extends Model{
    public function __construct(){
    parent::__construct();
    }
    public function existe($codigo){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion,codigo FROM ambiente WHERE codigo=:codigo");
            $sql->execute(['codigo' =>$codigo]);
            $nombre=$sql->fetch();
            
            if($codigo==$nombre['codigo']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
    //echo "<br>insertar datos";


    try{
     
             $query=$this->db->connect()->prepare('INSERT INTO ambiente(descripcion, codigo, direccion, coordenada_latitud,coordenada_longitd,id_parroquia,estatus) VALUES
             (:descripcion, :codigo, :direccion,:coordenada_latitud,:coordenada_longitd,:id_parroquia,:estatus)');

            $query->execute(['descripcion'=>$datos['ambiente'],'codigo'=>$datos['codigo'],'direccion'=>$datos['direccion'],
            'coordenada_latitud'=>$datos['latitud'], 'coordenada_longitd'=>$datos['logitud'], 
            'id_parroquia'=>$datos['parroquia'], 'estatus'=>$datos['estatus']]);

            return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }
    
    
            }

            public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT id_ambiente,ambiente.descripcion AS ambiente,ambiente.codigo AS codigo,direccion,coordenada_latitud, coordenada_longitd, parroquia.id_parroquia AS id_parroquia,parroquia.descripcion AS parroquia, ambiente.estatus AS estatus FROM ambiente,parroquia WHERE ambiente.id_parroquia=parroquia.id_parroquia");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_ambiente=$row['id_ambiente'];
              $item->ambiente=$row['ambiente'];
              $item->codigo=$row['codigo'];
              $item->direccion=$row['direccion'];          
              $item->coordenada_latitud=$row['coordenada_latitud'];
              $item->coordenada_longitd=$row['coordenada_longitd'];
              $item->id_parroquia=$row['id_parroquia'];
              $item->parroquia=$row['parroquia'];
              $item->estatus=$row['estatus'];
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }


            public function getParroquia(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT id_parroquia,parroquia.descripcion AS parroquia,parroquia.codigo AS codigo,municipio.id_municipio,eje_municipal.id_eje_municipal,municipio.descripcion AS municipio,eje_municipal.descripcion AS eje_municipal FROM parroquia,municipio,eje_municipal WHERE parroquia.id_municipio=municipio.id_municipio AND parroquia.id_eje_municipal=eje_municipal.id_eje_municipal");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_parroquia=$row['id_parroquia'];
              $item->parroquia=$row['parroquia'];
              $item->codigo=$row['codigo'];
              $item->id_municipio=$row['id_municipio'];          
              $item->municipio=$row['municipio'];
              $item->id_eje_municipal=$row['id_eje_municipal'];
              $item->eje_municipal=$row['eje_municipal'];
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }

 


              public function update($item){
   


         //  var_dump($item['parroquia'],$item['codigoine'],$item['id_municipio'],$item['id_eje_municipal'],$item['id_parroquia']);
              $query=$this->db->connect()->prepare("UPDATE ambiente SET descripcion= :descripcion,codigo= :codigo,direccion=:direccion,coordenada_latitud=:coordenada_latitud,coordenada_longitd=:coordenada_longitd,id_parroquia=:id_parroquia,estatus=:estatus WHERE id_ambiente= :id_ambiente");
            try{
                        $query->execute([
                        'descripcion'=>$item['ambiente'],
                        'codigo'=>$item['codigo'],                        
                        'direccion'=>$item['direccion'],
                        'coordenada_latitud'=>$item['latitud'],
                        'coordenada_longitd'=>$item['logitud'],

                        'id_parroquia'=>$item['parroquia'],
                        'estatus'=>$item['estatus'],
                        'id_ambiente'=>$item['id_ambiente'],
  
                        
                        ]);
            return true;
                       
                        
            }catch(PDOException $e){
                return false;
                 }
            
                }


    }

    ?>