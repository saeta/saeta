<?php
 //include_once 'SED.php';
include_once 'datosacademicos/docente.php';
include_once 'datosacademicos/ponencia.php';
class Registro_ponenciaModel extends Model{
    public function __construct(){
        parent::__construct();
    }


    public function existe($registroponencia){
        try{
    
            //var_dump($area_conocimiento_opsu);
            $sql = $this->db->connect()->prepare("SELECT nombre_ponencia FROM ponencia WHERE nombre_ponencia=:nombre_ponencia");
            $sql->execute(['nombre_ponencia'=>$registroponencia]);
            $nombre=$sql->fetch();
            
            if($registroponencia==$nombre['nombre_ponencia']){
                
                return $nombre['nombre_ponencia'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }

    public function insert($datos){
        //echo "<br>insertar datos";
        try{


           //1. guardas el objeto pdo en una variable
          // $pdo=$this->db->connect();

           //2. comienzas transaccion
          // $pdo->beginTransaction();



           
           $query=$this->db->connect()->prepare('INSERT INTO ponencia(
                nombre_ponencia,
                nombre_evento,
                ano_ponencia,
                ciudad,
                titulo_memoria,
                volumen,
                identificador,
                pag_inicial,
                formato,
                url_ponencia,
                estatus_verificacion,
                id_docente

                ) VALUES(
                    :nombre_ponencia,
                    :nombre_evento,
                    :ano_ponencia,
                    :ciudad,
                    :titulo_memoria,
                    :volumen,
                    :identificador,
                    :pag_inicial,
                    :formato,
                    :url_ponencia,
                    :estatus_verificacion,
                    :id_docente
                    )');
    
            $query->execute([
                    'nombre_ponencia'=>$datos['nombre_ponencia'],
                    'nombre_evento'=>$datos['nombre_evento'],
                    'ano_ponencia'=>$datos['ano_ponencia'],
                    'ciudad'=>$datos['ciudad'],
                    'titulo_memoria'=>$datos['titulo_memoria'],
                    'volumen'=>$datos['volumen'],
                    'identificador'=>$datos['identificador'],
                    'pag_inicial'=>$datos['pag_inicial'],
                    'formato'=>$datos['formato'],
                    'url_ponencia'=>$datos['url_ponencia'],
                    'estatus_verificacion'=>'Sin Verificar',
                    'id_docente'=>$datos['id_docente']
                ]);
      //4. consignas la transaccion (en caso de que no suceda ningun fallo)
    //  $pdo->commit();
                return true;
          
        }catch(PDOException $e){
            //5. regresas a un estado anterior en caso de error
           // $pdo->rollBack();

            return false;
                 }
        
        
    }

    
    public function getDocente(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT 
            docente.id_persona, 
            id_docente,
            identificacion, 
            primer_nombre, 
            primer_apellido
            FROM 
            persona, 
            persona_nacionalidad,
            docente
            WHERE docente.id_persona=persona.id_persona 
            AND persona_nacionalidad.id_persona=persona.id_persona;");
            
            while($row=$query->fetch()){
                
                $item=new Docente();
                $item->id_docente=$row['id_docente'];
                $item->id_persona=$row['id_persona'];
                $item->identificacion=$row['identificacion'];
                $item->primer_nombre=$row['primer_nombre'];
                $item->primer_apellido=$row['primer_apellido'];

                array_push($items,$item);
            
            
            }
            //var_dump($items);
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }
      
    public function getPonencia(){
        $items=[];
        try{
            $query=$this->db->connect()->prepare("SELECT id_ponencia,
             ponencia.nombre_ponencia,
             ponencia.nombre_evento,
             ponencia.ano_ponencia,
             ponencia.ciudad,
             ponencia.titulo_memoria,
             ponencia.volumen,
             ponencia.identificador,
             ponencia.pag_inicial,
             ponencia.formato,
             ponencia.url_ponencia,
             persona_nacionalidad.id_persona_nacionalidad,
                docente.id_docente,
                identificacion,
                estatus_verificacion
        
            FROM persona, persona_nacionalidad, ponencia, docente WHERE persona_nacionalidad.id_persona=persona.id_persona and ponencia.id_docente=docente.id_docente 
            and docente.id_persona=persona.id_persona and identificacion=:identificacion;");
            $query->execute(['identificacion'=>$_SESSION['identificacion']]);
            
            while($row=$query->fetch()){
                
                $item=new ponencia();
                $item->id_ponencia=$row['id_ponencia'];
                $item->nombre_ponencia=$row['nombre_ponencia'];
                $item->nombre_evento=$row['nombre_evento'];
                $item->ano_ponencia=$row['ano_ponencia'];
                $item->ciudad=$row['ciudad'];
                $item->titulo_memoria=$row['titulo_memoria'];
                $item->volumen=$row['volumen'];
                $item->identificador=$row['identificador'];
                $item->pag_inicial=$row['pag_inicial'];
                $item->formato=$row['formato'];
                $item->url_ponencia=$row['url_ponencia'];
                $item->estatus_verificacion=$row['estatus_verificacion'];
                $item->id_docente=$row['id_docente'];
               
                //$item->estatus=$row['estatus'];
                //$item->descripcion_opsu=$row['descripcion_opsu'];
                
                
                //$item->descripcion_opsu=$row['areaopsu'];
               
            
                array_push($items,$item);
            
            
            }
            //var_dump($items);
            return $items;
            
        }catch(PDOException $e){
        return[];
        }
      
    }

    public function getbyId($id){

            //var_dump($id);

        $items=[];
        
        try{
            $query=$this->db->connect()->prepare("SELECT id_ponencia,
             ponencia.nombre_ponencia,
             ponencia.nombre_evento,
             ponencia.ano_ponencia,
             ponencia.ciudad,
             ponencia.titulo_memoria,
             ponencia.volumen,
             ponencia.identificador,
             ponencia.pag_inicial,
             ponencia.formato,
             ponencia.url_ponencia,
                docente.id_docente,
                identificacion,
                primer_nombre,
                primer_apellido,
                estatus_verificacion
               
            FROM persona, persona_nacionalidad, ponencia, docente WHERE ponencia.id_docente=docente.id_docente 
            and docente.id_persona=persona.id_persona 
            and persona_nacionalidad.id_persona=persona.id_persona and identificacion=:identificacion and id_ponencia=:id_ponencia;");
            $query->execute([
                'identificacion'=>$_SESSION['identificacion'],
                'id_ponencia'=>$id
                ]);
            

            $item=new Registro_ponencia();
            while($row=$query->fetch()){
                
                
                
                $item->id_ponencia=$row['id_ponencia'];
                $item->nombre_ponencia=$row['nombre_ponencia'];
                $item->nombre_evento=$row['nombre_evento'];
                $item->ano_ponencia=$row['ano_ponencia'];
                $item->ciudad=$row['ciudad'];
                $item->titulo_memoria=$row['titulo_memoria'];
                $item->volumen=$row['volumen'];
                $item->identificador=$row['identificador'];
                $item->pag_inicial=$row['pag_inicial'];
                $item->formato=$row['formato'];
                $item->url_ponencia=$row['url_ponencia'];
                $item->estatus_verificacion=$row['estatus_verificacion'];
                $item->id_docente=$row['id_docente'];
               
                //$item->estatus=$row['estatus'];
                //$item->descripcion_opsu=$row['descripcion_opsu'];
                
                
                //$item->descripcion_opsu=$row['areaopsu'];
               
            
                array_push($items,$item);
            
            
            }


            //var_dump($item);
            return $item;
            
        }catch(PDOException $e){
        return[];
        }
      
    }


    

    public function update($datos){
     
        $query=$this->db->connect()->prepare("UPDATE ponencia SET 
        nombre_ponencia=:nombre_ponencia,
        nombre_evento=:nombre_evento,
        ano_ponencia=:ano_ponencia,
        ciudad=:ciudad,
        titulo_memoria=:titulo_memoria,
        volumen=:volumen,
        identificador=:identificador,
        pag_inicial=:pag_inicial,
        formato=:formato,
        url_ponencia=:url_ponencia,
        estatus_verificacion=:estatus_verificacion
       
        WHERE id_ponencia=:id_ponencia");
        
        try{
            $query->execute([
                'id_ponencia'=>$datos['id_ponencia'],

                'nombre_ponencia'=>$datos['nombre_ponencia2'],
                'nombre_evento'=>$datos['nombre_evento2'],
                'ano_ponencia'=>$datos['ano_ponencia2'],
                'ciudad'=>$datos['ciudad2'],
                'titulo_memoria'=>$datos['titulo_memoria2'],
                'volumen'=>$datos['volumen2'],
                'identificador'=>$datos['identificador2'],
                'pag_inicial'=>$datos['pag_inicial2'],
                'formato'=>$datos['formato2'],
                'url_ponencia'=>$datos['url_ponencia2'],
                'estatus_verificacion'=>'Sin Verificar'
               
                
              ]);
              //creo q falta descripcion del area de conocimientoooooooooooooooooooooooooo   
         
         
              return true;

        }catch(PDOException $e){
             return false;
        }
         
    }

        public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM ponencia WHERE id_ponencia = :id_ponencia");
    
            try{
                $query->execute([
                'ponencia'=>$id
                ]);
                return true;
            }catch(PDOException $e){
                return false;
            }
        }
    }


    ?>