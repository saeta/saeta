<?php
include_once 'models/estructura.php';
class CiudadModel extends Model{
    public function __construct(){
    parent::__construct();
    }
    public function existe($codigoine){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion,codigo FROM ciudad WHERE codigo=:codigo");
            $sql->execute(['codigo' =>$codigoine]);
            $nombre=$sql->fetch();
            
            if($codigoine==$nombre['codigo']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
    //echo "<br>insertar datos";
  // var_dump($datos['ciudad'],$datos['codigoine'],$datos['codigoine']);

    try{
     
             $query=$this->db->connect()->prepare('INSERT INTO ciudad(descripcion,id_estado ,codigo) VALUES
             (:descripcion, :id_estado, :codigo)');

            $query->execute(['descripcion'=>$datos['ciudad'],'id_estado'=>$datos['id_estado'],'codigo'=>$datos['codigoine']]);

            return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }
    
    
            }
            public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT id_ciudad,ciudad.codigo AS codigo,ciudad.descripcion AS ciudad, estado.descripcion AS estado,estado.id_estado FROM ciudad,estado WHERE ciudad.id_estado=estado.id_estado");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_ciudad=$row['id_ciudad'];
              $item->ciudad=$row['ciudad'];
              $item->codigo=$row['codigo'];
              $item->estado=$row['estado'];          
              $item->id_estado=$row['id_estado'];
            
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }


              public function getEstado(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT * FROM estado");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_estado=$row['id_estado'];
              $item->descripcion=$row['descripcion'];
              $item->codigo=$row['codigo'];
              $item->id_pais=$row['id_pais'];
             
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }
             


              public function update($item){
         
         //  var_dump($item['parroquia'],$item['codigoine'],$item['id_municipio'],$item['id_eje_municipal'],$item['id_parroquia']);
              $query=$this->db->connect()->prepare("UPDATE ciudad SET descripcion= :descripcion,id_estado= :id_estado,codigo=:codigo WHERE id_ciudad= :id_ciudad");
            try{
                        $query->execute([
                        'descripcion'=>$item['ciudad'],                                            
                        'id_estado'=>$item['id_estado'],
                        'codigo'=>$item['codigoine'],   
                        'id_ciudad'=>$item['id_ciudad'],
  
                        
                        ]);
            return true;
                       
                        
            }catch(PDOException $e){
                return false;
                 }
            
                }



    }

    ?>