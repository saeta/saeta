<?php
include_once 'datosacademicos/docente.php';
include_once 'datosacademicos/revista.php';
class Articulo_revistaModel extends Model{
    public function __construct(){
        parent::__construct();
    }


    public function existe($articulorevista){
        try{
    
            //var_dump($area_conocimiento_opsu);
            $sql = $this->db->connect()->prepare("SELECT nombre_revista FROM articulo_revista WHERE nombre_revista=:nombre_revista");
            $sql->execute(['nombre_revista'=>$articulorevista]);
            $nombre=$sql->fetch();
            
            if($articulorevista==$nombre['nombre_revista']){
                
                return $nombre['nombre_revista'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }

    public function insert($datos){
        //echo "<br>insertar datos";
    

        try{
        // var_dump($datos['nombre_revista']);
            $query=$this->db->connect()->prepare('INSERT INTO articulo_revista(
                nombre_revista,
                id_docente,
                titulo_articulo,
                issn_revista,
                ano_revista,
                nro_revista,
                nro_pag_inicial,
                url_revista,
                estatus_verificacion

                ) VALUES(
                    :nombre_revista,
                    :id_docente,
                    :titulo_articulo,
                    :issn_revista,
                    :ano_revista,
                    :nro_revista,
                    :nro_pag_inicial,
                    :url_revista,
                    :estatus_verificacion
                    )');
    
            $query->execute([
                    'nombre_revista'=>$datos['nombre_revista'],
                    'id_docente'=>$datos['id_docente'],
                    'titulo_articulo'=>$datos['titulo_articulo'],
                    'issn_revista'=>$datos['issn_revista'],
                    'ano_revista'=>$datos['ano_revista'],
                    'nro_revista'=>$datos['nro_revista'],
                    'nro_pag_inicial'=>$datos['nro_pag_inicial'],
                    'url_revista'=>$datos['url_revista'],
                    'estatus_verificacion'=>'Sin Verificar'
                ]);
    
                return true;
          
        }catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                 }
        
        
    }

    
    public function getDocente(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT 
            docente.id_persona, 
            id_docente,
            identificacion, 
            primer_nombre, 
            primer_apellido
            FROM 
            persona, 
            persona_nacionalidad,
            docente
            WHERE docente.id_persona=persona.id_persona 
            AND persona_nacionalidad.id_persona=persona.id_persona;");
            
            while($row=$query->fetch()){
                
                $item=new Docente();
                $item->id_docente=$row['id_docente'];
                $item->id_persona=$row['id_persona'];
                $item->identificacion=$row['identificacion'];
                $item->primer_nombre=$row['primer_nombre'];
                $item->primer_apellido=$row['primer_apellido'];

                array_push($items,$item);
            
            
            }
            //var_dump($items);
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }
      
    public function getRevista(){
        $items=[];
        try{
            $query=$this->db->connect()->prepare("SELECT id_articulo_revista,
             articulo_revista.nombre_revista,
             articulo_revista.titulo_articulo,
             articulo_revista.issn_revista,
             articulo_revista.ano_revista,
             articulo_revista.nro_revista,
             articulo_revista.nro_pag_inicial,
             articulo_revista.url_revista,
             persona_nacionalidad.id_persona_nacionalidad,
                docente.id_docente,
                identificacion,
                estatus_verificacion
        
            FROM persona, persona_nacionalidad, articulo_revista, docente WHERE persona_nacionalidad.id_persona=persona.id_persona and articulo_revista.id_docente=docente.id_docente 
            and docente.id_persona=persona.id_persona and identificacion=:identificacion;");
            $query->execute(['identificacion'=>$_SESSION['identificacion']]);
            
            while($row=$query->fetch()){
                
                $item=new Articulo_revista();
                $item->id_articulo_revista=$row['id_articulo_revista'];
                $item->nombre_revista=$row['nombre_revista'];
                $item->titulo_articulo=$row['titulo_articulo'];
                $item->issn_revista=$row['issn_revista'];
                $item->ano_revista=$row['ano_revista'];
                $item->nro_revista=$row['nro_revista'];
                $item->nro_pag_inicial=$row['nro_pag_inicial'];
                $item->url_revista=$row['url_revista'];
                $item->estatus_verificacion=$row['estatus_verificacion'];
                $item->id_docente=$row['id_docente'];
               
                //$item->estatus=$row['estatus'];
                //$item->descripcion_opsu=$row['descripcion_opsu'];
                
                
                //$item->descripcion_opsu=$row['areaopsu'];
               
            
                array_push($items,$item);
            
            
            }
            //var_dump($items);
            return $items;
            
        }catch(PDOException $e){
        return[];
        }
      
    }

    public function getbyId($id){

            //var_dump($id);

        $items=[];
        
        try{
            $query=$this->db->connect()->prepare("SELECT id_articulo_revista,
             articulo_revista.nombre_revista,
             articulo_revista.titulo_articulo,
             articulo_revista.issn_revista,
             articulo_revista.ano_revista,
             articulo_revista.nro_revista,
             articulo_revista.nro_pag_inicial,
             articulo_revista.url_revista,
                docente.id_docente,
                 identificacion,
                 primer_nombre,
                primer_apellido,
                estatus_verificacion
                
            FROM persona, persona_nacionalidad, articulo_revista, docente WHERE articulo_revista.id_docente=docente.id_docente 
            and docente.id_persona=persona.id_persona 
            and persona_nacionalidad.id_persona=persona.id_persona and identificacion=:identificacion and id_articulo_revista=:id_articulo_revista;");
            $query->execute([
                'identificacion'=>$_SESSION['identificacion'],
                'id_articulo_revista'=>$id
                ]);
            

            $item=new Articulo_revista();
            while($row=$query->fetch()){
                
                
                $item->id_articulo_revista=$row['id_articulo_revista'];
                $item->nombre_revista=$row['nombre_revista'];
                $item->titulo_articulo=$row['titulo_articulo'];
                $item->issn_revista=$row['issn_revista'];
                $item->ano_revista=$row['ano_revista'];
                $item->nro_revista=$row['nro_revista'];
                $item->nro_pag_inicial=$row['nro_pag_inicial'];
                $item->url_revista=$row['url_revista'];
                $item->estatus_verificacion=$row['estatus_verificacion'];
                $item->id_docente=$row['id_docente'];
               
                //$item->estatus=$row['estatus'];
                //$item->descripcion_opsu=$row['descripcion_opsu'];
                
                
                //$item->descripcion_opsu=$row['areaopsu'];
               
            
                array_push($items,$item);
            
            
            }


            //var_dump($item);
            return $item;
            
        }catch(PDOException $e){
        return[];
        }
      
    }


    

    public function update($datos){
       //var_dump($datos['nombre_arte2']);
       //var_dump($datos['ano_arte2']);
       //var_dump($datos['entidad_promotora2']);
       //var_dump($datos['url_otro2']);
       //var_dump($datos['id_arte_software']);

        $query=$this->db->connect()->prepare("UPDATE articulo_revista SET 
        nombre_revista=:nombre_revista,
        titulo_articulo=:titulo_articulo,
        issn_revista=:issn_revista,
        ano_revista=:ano_revista,
        nro_revista=:nro_revista,
        nro_pag_inicial=:nro_pag_inicial,
        url_revista=:url_revista,
        estatus_verificacion=:estatus_verificacion
        WHERE id_articulo_revista=:id_articulo_revista");
        
        try{
            $query->execute([
                'id_articulo_revista'=>$datos['id_articulo_revista'],

                'nombre_revista'=>$datos['nombre_revista2'],
                'titulo_articulo'=>$datos['titulo_articulo2'],
                'issn_revista'=>$datos['issn_revista2'],
                'ano_revista'=>$datos['ano_revista2'],
                'nro_revista'=>$datos['nro_revista2'],
                'nro_pag_inicial'=>$datos['nro_pag_inicial2'],
                'url_revista'=>$datos['url_revista2'],
                'estatus_verificacion'=>'Sin Verificar'
               
                
              ]);
              //creo q falta descripcion del area de conocimientoooooooooooooooooooooooooo   
         
         
              return true;

        }catch(PDOException $e){
             return false;
        }
         
    }

        public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM articulo_revista WHERE id_articulo_revista = :id_articulo_revista");
    
            try{
                $query->execute([
                'id_articulo_revista'=>$id
                ]);
                return true;
            }catch(PDOException $e){
                return false;
            }
        }
    }


    ?>