<?php 
    include_once 'models/estructura.php';
    include_once 'scripts/files.php';

    class Solicitar_ascensoModel extends Model{
        public function __construct(){
        parent::__construct();
        }
        
        public function existe($id){
            
            try{

                 
                $sql = $this->db->connect()->prepare("SELECT id_docente FROM solicitud_ascenso WHERE id_docente=:id_docente");
                $sql->execute(['id_docente'=>$id]);
                $nombre=$sql->fetch();
                
                if($id==$nombre['id_docente']){
                    
                    return $nombre['id_docente'];
        
                } 
                return false;



            } catch(PDOException $e){
                return false;
            }

        }

        public function existeMemo($id_solicitud_ascenso, $id_tipo_documento){
            
            try{

                 
                $sql = $this->db->connect()->prepare("SELECT 
                id_memo_solicitud,
                memo_solicitud.id_documento,
                documento.descripcion,
                documento.id_tipo_documento,
                memo_solicitud.estatus
                from memo_solicitud,
                documento 
                where 
                memo_solicitud.id_documento=documento.id_documento
                and id_tipo_documento=:id_tipo_documento
                and id_solicitud_ascenso=:id_solicitud_ascenso;");
                $sql->execute([
                    'id_solicitud_ascenso'=>$id_solicitud_ascenso,
                    'id_tipo_documento'=>$id_tipo_documento
                    ]);
                $nombre=$sql->fetch();

                if($id_tipo_documento==$nombre['id_tipo_documento']){
                    
                    return $nombre['id_tipo_documento'];
        
                } 
                return false;



            } catch(PDOException $e){
                return false;
            }

        }

        //////////////    funcion que llama datos de tablas catalogos   ///////////////
        public function getEscalafon(){
            $items=[];
            try{
                $query=$this->db->connect()->prepare("SELECT 
                * 
                from 
                escalafon 
                where 
                descripcion<>:uno
                and descripcion<>:dos
                and descripcion<>:tres;");
                $query->execute([
                    'uno'=>'AUXILIAR I',
                    'dos'=>'AUXILIAR II',
                    'tres'=>'AUXILIAR III'
                    ]);
                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id=$row['id_escalafon'];
                    $item->descripcion=$row['descripcion'];
                    
                    array_push($items,$item);
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
        }
        //verificar si el docente tiene un concurso registrado
        public function getConcursobyid($id){
            try{

                $query=$this->db->connect()->prepare("SELECT 
                * 
                from concurso 
                where id_docente=:id_docente;");
                $query->execute(['id_docente'=>$id]);
                
                
                $row=$query->fetch();
                
                return $row;

            } catch(PDOException $e){
                return false;
            }
        }
        //verificar si el docente tiene un doctorado verificado
        public function getDoctoradobyid($id){
            try{

                $query=$this->db->connect()->prepare("SELECT 
                t2.id_estudio,
                t1.estatus,
                t2.id_nivel_academico
                
                from 
                estudio as t1,
                estudio_nivel as t2,
                nivel_academico as t3 
                where 
                id_tipo_estudio=1 
                and t2.id_estudio=t1.id_estudio
                and t2.id_nivel_academico=t3.id_nivel_academico
                and estatus='concluido'
                and t2.id_nivel_academico=27
                and id_docente=:id_docente;");
                $query->execute(['id_docente'=>$id]);
                
                
                $row=$query->fetch();
                
                return $row;

            } catch(PDOException $e){
                return false;
            }
        }


        public function insert($datos){

            try{

                //1. guardas el objeto pdo en una variable
                $pdo=$this->db->connect();

                //2. comienzas transaccion
                $pdo->beginTransaction();
                
                
                $doc=new Documento();
                $pdf_trabajo=$doc->getData($datos['pdf_trabajo']);

                

                
                //mover pdf trabajo ascenso
                if($doc->mover($pdf_trabajo['extension'], 
                    'src/documentos/ascensos/',
                    $pdf_trabajo['ruta_tmp'],
                    $pdf_trabajo['original_name'],
                    array('pdf'))
                ){
                    $doc_trabajo=$pdo->prepare("INSERT INTO documento(
                        descripcion,
                        estatus,
                        id_persona,
                        id_tipo_documento) 
                        VALUES(
                            :descripcion,
                            :estatus, 
                            :id_persona,
                            (SELECT id_tipo_documento FROM tipo_documento WHERE descripcion='TRABAJO_ASCENSO') 
                            );");

                    $doc_trabajo->execute([
                        'descripcion'=>date('Y-m-d H:i:s').$pdf_trabajo['original_name'],
                        'estatus'=>'Sin Verificar',
                        'id_persona'=>$datos['id_persona']
                        ]);

                    $trabajo=$pdo->prepare("INSERT INTO trabajo_ascenso(
                        titulo, 
                        id_documento) 
                        VALUES(
                            :titulo, 
                            (SELECT id_documento from documento ORDER BY id_documento DESC LIMIT 1)
                            );");
                    $trabajo->execute(['titulo'=>$datos['titulo']]);
                } 

                $pdf_pida=$doc->getData($datos['pdf_pida']);

                //mover documento acta pida
                //el mover no se esta ejecutando: revisar extensiones y permisos
                if($doc->mover($pdf_pida['extension'], 
                    'src/documentos/ascensos/',
                    $pdf_pida['ruta_tmp'],
                    $pdf_pida['original_name'],
                    array('pdf'))
                ){
                    $doc_pida=$pdo->prepare("INSERT INTO documento(
                        descripcion,
                        estatus,
                        id_persona,
                        id_tipo_documento) 
                        VALUES(
                            :descripcion,
                            :estatus, 
                            :id_persona,
                            (SELECT id_tipo_documento FROM tipo_documento WHERE descripcion='ACTA_PIDA') 
                            );");

                    $doc_pida->execute([
                        'descripcion'=>date('Y-m-d H:i:s').$pdf_pida['original_name'],
                        'estatus'=>'Sin Verificar',
                        'id_persona'=>$datos['id_persona']
                        ]);
                    
                    $solicitud_pida=$pdo->query("INSERT INTO solicitud_acta_pida(
                        id_documento
                        ) 
                        VALUES(
                            (select id_documento from documento ORDER BY id_documento DESC LIMIT 1)
                            );");
                } 

                $pdf_proyecto=$doc->getData($datos['pdf_proyecto']);

                //mover documento acta PROYECTO
                //el mover no se esta ejecutando: revisar extensiones y permisos
                if($doc->mover($pdf_proyecto['extension'], 
                    'src/documentos/ascensos/',
                    $pdf_proyecto['ruta_tmp'],
                    $pdf_proyecto['original_name'],
                    array('pdf'))
                ){
                    $doc_proyecto=$pdo->prepare("INSERT INTO documento(
                        descripcion,
                        estatus,
                        id_persona,
                        id_tipo_documento) 
                        VALUES(
                            :descripcion,
                            :estatus, 
                            :id_persona,
                            (SELECT id_tipo_documento FROM tipo_documento WHERE descripcion='ACTA_PROYECTO') 
                            );");

                    $doc_proyecto->execute([
                        'descripcion'=>date('Y-m-d H:i:s').$pdf_proyecto['original_name'],
                        'estatus'=>'Sin Verificar',
                        'id_persona'=>$datos['id_persona']
                        ]);
                    
                    $solicitud_proyecto=$pdo->query("INSERT INTO solicitud_acta_proyecto
                    (id_documento)
                    VALUES(
                        (select id_documento from documento ORDER BY id_documento DESC LIMIT 1)
                        );");

                } 

                $solicitud=$pdo->prepare("INSERT INTO solicitud_ascenso(
                    id_escalafon, 
                    id_estatus_ascenso, 
                    id_docente,
                    id_trabajo_ascenso,
                    fecha_solicitud,
                    id_concurso,
                    id_solicitud_acta_pida,
                    id_solicitud_acta_proyecto,
                    ciudad,
                    fecha_consulta
                    ) VALUES(
                    (select id_escalafon from docente where id_docente=:id_docente1),
                    :id_estatus_ascenso,
                    :id_docente2,
                    (select id_trabajo_ascenso from trabajo_ascenso ORDER BY id_trabajo_ascenso DESC LIMIT 1),
                    :fecha_solicitud,
                    (select id_concurso from concurso where id_docente=:id_docente3 ORDER BY id_concurso DESC LIMIT 1),
                    (select id_solicitud_acta_pida from solicitud_acta_pida ORDER BY id_solicitud_acta_pida DESC LIMIT 1),
                    (select id_solicitud_acta_proyecto from solicitud_acta_proyecto ORDER BY id_solicitud_acta_proyecto DESC LIMIT 1),
                    :ciudad,
                    :fecha_consulta
                    );");
                
                $solicitud->execute([
                    'id_docente1'=>$datos['id_docente'],
                    'id_estatus_ascenso'=>1,
                    'id_docente2'=>$datos['id_docente'],
                    'id_docente3'=>$datos['id_docente'],
                    'fecha_solicitud'=>date('Y-m-d'),
                    'ciudad'=>$datos['ciudad'],
                    'fecha_consulta'=>$datos['fecha_consulta'],
                    ]);

                    //si el escalafon a a scender es asociado o titular
                    /*if(!empty($datos['pdf_titulo_doctor'])){
                        
                        $pdf_titulo_doctor=$doc->getData($datos['pdf_titulo_doctor']);
                        

                        if($doc->mover($pdf_titulo_doctor['extension'], 
                        'src/documentos/ascensos/',
                        $pdf_titulo_doctor['ruta_tmp'],
                        $pdf_titulo_doctor['original_name'],
                        array('pdf'))
                        ){
                            
                            $doc_titulo=$pdo->prepare("INSERT INTO documento(
                                descripcion,
                                estatus,
                                id_persona,
                                id_tipo_documento) 
                                VALUES(
                                    :descripcion,
                                    :estatus, 
                                    :id_persona,
                                    (SELECT id_tipo_documento FROM tipo_documento WHERE descripcion=:tipo_documento) 
                                    );");
                            $doc_titulo->execute($doc_titulo->execute([
                                'descripcion'=>date('Y-m-d H:i:s').$pdf_titulo_doctor['original_name'],
                                'estatus'=>'Sin Verificar',
                                'id_persona'=>$datos['id_persona'],
                                'tipo_documento'=>'TITULO_DOCTOR'
                                ]));
                                
                            $titulo=$pdo->prepare("INSERT INTO 
                                memo_solicitud(
                                id_documento, 
                                id_solicitud_ascenso,
                                estatus
                                ) VALUES(
                                    (select id_documento from documento ORDER BY id_documento DESC LIMIT 1),
                                    (select id_solicitud_ascenso from solicitud_ascenso ORDER BY id_solicitud_ascenso DESC LIMIT 1),
                                    :estatus);");
                            $titulo->execute([
                                'estatus'=>'En Espera'
                                ]);
                                
                                
                        } 
                    }*/

                    //////////////////////////////////////////////
                   
                $escalafon_sig=$pdo->prepare("INSERT INTO solicitud_escalafon_siguiente(
                            id_solicitud_ascenso,
                            id_escalafon) 
                            VALUES(
                            (select id_solicitud_ascenso from solicitud_ascenso ORDER BY id_solicitud_ascenso DESC LIMIT 1),
                            :id_escalafon
                            );");
                $escalafon_sig->execute(['id_escalafon'=>$datos['escalafon_next']]);
                
                    
                //movemos la resolucion
                if(!empty($datos['pdf_resolucion'])){
                    $pdf_resolucion=$doc->getData($datos['pdf_resolucion']);

                    if($doc->mover($pdf_resolucion['extension'], 
                    'src/documentos/ascensos/',
                    $pdf_resolucion['ruta_tmp'],
                    $pdf_resolucion['original_name'],
                    array('pdf'))
                    ){
                    $doc_res=$pdo->prepare("INSERT INTO documento(
                        descripcion,
                        estatus,
                        id_persona,
                        id_tipo_documento) 
                        VALUES(
                            :descripcion,
                            :estatus, 
                            :id_persona,
                            (SELECT id_tipo_documento FROM tipo_documento WHERE descripcion='RESOLUCION_ASCENSO') 
                            );");

                    $doc_res->execute([
                        'descripcion'=>date('Y-m-d H:i:s').$pdf_resolucion['original_name'],
                        'estatus'=>'Sin Verificar',
                        'id_persona'=>$datos['id_persona']
                        ]);
                    }

                    $res=$pdo->query("INSERT INTO solicitud_resolucion(id_solicitud_ascenso, id_documento) VALUES(
                        (select id_solicitud_ascenso from solicitud_ascenso ORDER BY id_solicitud_ascenso DESC LIMIT 1),
                        (select id_documento from documento ORDER BY id_documento DESC LIMIT 1)
                        );");
                }

                //4. consignas la transaccion (en caso de que no suceda ningun fallo)
                $pdo->commit();
                return true; 

            } catch(PDOException $e){
                //5. regresas a un estado anterior en caso de error
                $pdo->rollBack();
                //echo "Fallo: " . $e->getMessage();

                return false;
            }
        }
        
        public function get($id){
            $items=[];
            try{
                $query=$this->db->connect()->prepare("SELECT 
                    id_solicitud_ascenso,
                    t1.id_escalafon,
                    t2.descripcion as escalafon,
                    t1.id_estatus_ascenso,
                    t3.descripcion as estatus_ascenso,
                    t1.id_docente,
                    fecha_solicitud,
                    ciudad
                    from 
                    solicitud_ascenso as t1,
                    escalafon as t2,
                    estatus_ascenso as t3 where
                    t1.id_escalafon=t2.id_escalafon
                    and t1.id_estatus_ascenso=t3.id_estatus_ascenso
                    and id_docente=:id_docente;");
                $query->execute(['id_docente'=>$id]);

                while($row=$query->fetch()){
                    $item=new Estructura();
                    
                    $item->id_solicitud_ascenso=$row['id_solicitud_ascenso'];
                    $item->id_escalafon=$row['id_escalafon'];
                    $item->escalafon=$row['escalafon'];
                    $item->id_estatus_ascenso=$row['id_estatus_ascenso'];          
                    $item->estatus_ascenso=$row['estatus_ascenso'];
                    $item->id_docente=$row['id_docente'];
                    $item->fecha_solicitud=$row['fecha_solicitud'];
                    $item->ciudad=$row['ciudad'];
                    
                    array_push($items,$item);
          
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
          
        }

        public function getByIdDocente($id){
            try{
                
                
                $sql = $this->db->connect()->prepare("SELECT 
                primer_nombre,
                segundo_nombre,
                primer_apellido,
                segundo_apellido,
                t4.descripcion as nacionalidad,
                t3.identificacion,
                t1.id_escalafon,
                t5.descripcion as escalafon,
                t6.descripcion as centro_estudio,
                correo,
                t1.id_centro_estudio
                from 
                docente as t1,
                persona as t2,
                persona_nacionalidad as t3,
                documento_identidad_tipo as t4,
                escalafon as t5,
                centro_estudio as t6,
                persona_correo as t7

                where 
                t1.id_persona=t2.id_persona
                and t3.id_persona=t2.id_persona
                and t3.id_documento_identidad_tipo=t4.id_documento_identidad_tipo
                and t1.id_escalafon=t5.id_escalafon
                and t1.id_centro_estudio=t6.id_centro_estudio
                and t1.id_persona=t2.id_persona
                and t7.id_persona=t2.id_persona
                and t1.id_docente=:id_docente;");
                $sql->execute(['id_docente'=>$id]);
                $item = new Estructura();
                while($row=$sql->fetch()){
                    $item->id_persona=$row['id_persona'];
                    $item->id_docente=$row['id_docente'];
                    $item->primer_nombre=$row['primer_nombre'];
                    $item->segundo_nombre=$row['segundo_nombre'];
                    $item->primer_apellido=$row['primer_apellido'];
                    $item->segundo_apellido=$row['segundo_apellido'];
                    $item->nacionalidad=$row['nacionalidad'];
                    $item->identificacion=$row['identificacion'];
                    $item->id_escalafon=$row['id_escalafon'];

                    $item->escalafon=$row['escalafon'];
                    $item->centro_estudio=$row['centro_estudio'];
                    $item->correo=$row['correo'];
                    $item->id_centro_estudio=$row['id_centro_estudio'];

                    $item->fecha_solicitud=date("d-m-Y", strtotime($row['fecha_solicitud']));
                    //$item->fecha_consulta=date("d-m-Y", strtotime($row['fecha_consulta']));
                    $item->ciudad="";
                }
                return $item;
            }catch(PDOExcepton $e){
                return null;
            }
        }


        public function getAscensosAdmin(){
            $items=[];
            try{
                $query=$this->db->connect()->query("SELECT 
                id_solicitud_ascenso,
                t1.id_escalafon,
                t2.descripcion as escalafon,
                t1.id_estatus_ascenso,
                t3.descripcion as estatus_ascenso,
                t1.id_docente,
                fecha_solicitud,
                ciudad,
                t4.id_persona,
                t5.primer_nombre,
                t5.primer_apellido,
                identificacion
                from 
                solicitud_ascenso as t1,
                escalafon as t2,
                estatus_ascenso as t3 ,
                docente as t4,
                persona as t5,
                persona_nacionalidad as t6
                where
                t1.id_escalafon=t2.id_escalafon
                and t1.id_estatus_ascenso=t3.id_estatus_ascenso
                and t1.id_docente=t4.id_docente
                and t4.id_persona=t5.id_persona
                and t6.id_persona=t5.id_persona;");

                while($row=$query->fetch()){
                    $item=new Estructura();
                    
                    $item->id_solicitud_ascenso=$row['id_solicitud_ascenso'];
                    $item->id_escalafon=$row['id_escalafon'];
                    $item->escalafon=$row['escalafon'];
                    $item->id_estatus_ascenso=$row['id_estatus_ascenso'];          
                    $item->estatus_ascenso=$row['estatus_ascenso'];
                    $item->id_docente=$row['id_docente'];
                    $item->fecha_solicitud=$row['fecha_solicitud'];
                    $item->ciudad=$row['ciudad'];
                    $item->id_persona=$row['id_persona'];
                    $item->primer_nombre=$row['primer_nombre'];
                    $item->primer_apellido=$row['primer_apellido'];
                    $item->identificacion=$row['identificacion'];

                    array_push($items,$item);
          
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
          
        }

        public function getByIdSolicitud($id){
            try{

                $query=$this->db->connect()->prepare("SELECT 
                id_solicitud_ascenso,
                t1.id_escalafon,
                t2.descripcion as escalafon,
                t1.id_estatus_ascenso,
                t3.descripcion as estatus_ascenso,
                t1.id_docente,
                fecha_solicitud,
                fecha_consulta,
                ciudad,
                t4.id_persona,
                t5.primer_nombre,
                t5.segundo_nombre,
                t5.primer_apellido,
                t5.segundo_apellido,
                identificacion,
                t4.id_centro_estudio,
                t7.descripcion as centro_estudio,
                t4.id_eje_regional,
                t8.descripcion as eje_regional,
                t7.id_area_conocimiento_opsu,
                t9.descripcion as area_conocimiento_opsu,
                t1.id_solicitud_acta_pida,
                t10.correo

                from 
                solicitud_ascenso as t1,
                escalafon as t2,
                estatus_ascenso as t3 ,
                docente as t4,
                persona as t5,
                persona_nacionalidad as t6,
                centro_estudio as t7,
                eje_regional as t8,
                area_conocimiento_opsu as t9,
                persona_correo as t10
                where
                t1.id_escalafon=t2.id_escalafon
                and t1.id_estatus_ascenso=t3.id_estatus_ascenso
                and t1.id_docente=t4.id_docente
                and t4.id_persona=t5.id_persona
                and t6.id_persona=t5.id_persona
                and t4.id_centro_estudio=t7.id_centro_estudio
                and t4.id_eje_regional=t8.id_eje_regional
                and t7.id_area_conocimiento_opsu=t9.id_area_conocimiento_opsu
                and t10.id_persona=t5.id_persona
                and t1.id_solicitud_ascenso=:id_solicitud_ascenso;");
                
                $query->execute(['id_solicitud_ascenso'=>$id]);
                
                $item = new Estructura();

                while($row=$query->fetch()){
                    $item->id_solicitud_ascenso=$row['id_solicitud_ascenso'];
                    $item->id_escalafon=$row['id_escalafon'];
                    $item->escalafon=$row['escalafon'];
                    $item->id_estatus_ascenso=$row['id_estatus_ascenso'];          
                    $item->estatus_ascenso=$row['estatus_ascenso'];
                    $item->id_docente=$row['id_docente'];
                    $item->fecha_solicitud=date("d-m-Y", strtotime($row['fecha_solicitud']));
                    $item->ciudad=$row['ciudad'];
                    $item->id_persona=$row['id_persona'];
                    $item->primer_nombre=$row['primer_nombre'];
                    $item->segundo_nombre=$row['segundo_nombre'];

                    $item->primer_apellido=$row['primer_apellido'];
                    $item->segundo_apellido=$row['segundo_apellido'];

                    $item->identificacion=$row['identificacion'];
                    $item->fecha_consulta=date("d-m-Y", strtotime($row['fecha_consulta']));

                    $item->id_centro_estudio=$row['id_centro_estudio'];
                    $item->centro_estudio=$row['centro_estudio'];
                    $item->id_eje_regional=$row['id_eje_regional'];
                    $item->eje_regional=$row['eje_regional'];
                    $item->id_area_conocimiento_opsu=$row['id_area_conocimiento_opsu'];
                    $item->area_conocimiento_opsu=$row['area_conocimiento_opsu'];

                    $item->correo=$row['correo'];

                }                
                return $item;

            } catch(PDOException $e){
                return false;
            }
        }


        public function getByIdSolicitudEscalafonSiguiente($id){
            try{

                $query=$this->db->connect()->prepare("SELECT 
                t3.id_solicitud_ascenso,
                t3.id_escalafon,
                t2.descripcion as escalafon_siguiente
                from 
                solicitud_ascenso as t1,
                escalafon as t2,
                solicitud_escalafon_siguiente as t3
                WHERE 
                t3.id_solicitud_ascenso=t1.id_solicitud_ascenso
                and t3.id_escalafon=t2.id_escalafon
                and t3.id_solicitud_ascenso=:id_solicitud_ascenso;");
                
                $query->execute(['id_solicitud_ascenso'=>$id]);
                
                $item = new Estructura();

                while($row=$query->fetch()){
                    $item->id_solicitud_ascenso=$row['id_solicitud_ascenso'];
                    $item->id_escalafon_siguiente=$row['id_escalafon'];
                    $item->escalafon_siguiente=$row['escalafon_siguiente'];
                    
                }                
                return $item;

            } catch(PDOException $e){
                return false;
            }
        }

        public function getByIdSolicitudDocente($id){
            try{
                $query=$this->db->connect()->prepare("SELECT id_solicitud_ascenso from solicitud_ascenso where id_docente=:id_docente;
                ");
                $query->execute(['id_docente'=>$id]);
                $row=$query->fetch();
                return $row['id_solicitud_ascenso'];
            } catch(PDOException $e){
                return false;
            }
        }

        public function getByIdSolicitud_ACTA_PROYECTO($id){
            try{

                $query=$this->db->connect()->prepare("SELECT 
                id_solicitud_ascenso,
                solicitud_acta_proyecto.id_solicitud_acta_proyecto, 
                solicitud_acta_proyecto.id_documento as id_documento_proyecto, 
                documento.descripcion as acta_proyecto,
                documento.id_tipo_documento,
                tipo_documento.descripcion as tipo_documento
                from 
                solicitud_ascenso,
                documento,
                solicitud_acta_proyecto,
                tipo_documento  
                where 
                solicitud_ascenso.id_solicitud_acta_proyecto=solicitud_acta_proyecto.id_solicitud_acta_proyecto
                and solicitud_acta_proyecto.id_documento=documento.id_documento            
                and documento.id_tipo_documento=tipo_documento.id_tipo_documento
                and id_solicitud_ascenso=:id_solicitud_ascenso;");
                
                $query->execute(['id_solicitud_ascenso'=>$id]);
                
                $item = new Estructura();

                while($row=$query->fetch()){
                    $item->id_solicitud_ascenso=$row['id_solicitud_ascenso'];
                    $item->id_solicitud_acta_proyecto=$row['id_solicitud_acta_proyecto'];
                    $item->id_documento_proyecto=$row['id_documento_proyecto'];
                    $item->acta_proyecto=$row['acta_proyecto'];
                    $item->id_tipo_documento=$row['id_tipo_documento'];
                    $item->tipo_documento=$row['tipo_documento'];
                }                
                return $item;

            } catch(PDOException $e){
                return false;
            }
        }

        public function getByIdSolicitud_TRABAJO_ASCENSO($id){
            try{

                $query=$this->db->connect()->prepare("SELECT
                id_solicitud_ascenso, 
                trabajo_ascenso.id_trabajo_ascenso, 
                titulo,
                trabajo_ascenso.id_documento as id_documento_trabajo, 
                documento.descripcion as trabajo_ascenso,
                documento.id_tipo_documento,
                tipo_documento.descripcion as tipo_documento
                from 
                solicitud_ascenso,
                documento,
                trabajo_ascenso,
                tipo_documento
                where 
                solicitud_ascenso.id_trabajo_ascenso=trabajo_ascenso.id_trabajo_ascenso
                and trabajo_ascenso.id_documento=documento.id_documento                 
                and documento.id_tipo_documento=tipo_documento.id_tipo_documento
                and id_solicitud_ascenso=:id_solicitud_ascenso;");
                
                $query->execute(['id_solicitud_ascenso'=>$id]);
                
                $item = new Estructura();

                while($row=$query->fetch()){
                    $item->id_solicitud_ascenso=$row['id_solicitud_ascenso'];
                    $item->id_trabajo_ascenso=$row['id_trabajo_ascenso'];
                    $item->id_documento_trabajo=$row['id_documento_trabajo'];
                    $item->trabajo_ascenso=$row['trabajo_ascenso'];
                    $item->id_tipo_documento=$row['id_tipo_documento'];
                    $item->tipo_documento=$row['tipo_documento'];

                    $item->titulo=$row['titulo'];

                }                
                return $item;

            } catch(PDOException $e){
                return false;
            }
        }


        public function getByIdSolicitud_ACTA_PIDA($id){
            try{

                $query=$this->db->connect()->prepare("SELECT 
                id_solicitud_ascenso, 
                solicitud_acta_pida.id_solicitud_acta_pida, 
                solicitud_acta_pida.id_documento as id_documento_pida, 
                documento.descripcion as acta_pida,
                documento.id_tipo_documento,
                tipo_documento.descripcion as tipo_documento
                from 
                solicitud_ascenso,
                documento,
                solicitud_acta_pida,
                tipo_documento 
                where 
                solicitud_ascenso.id_solicitud_acta_pida=solicitud_acta_pida.id_solicitud_acta_pida
                and solicitud_acta_pida.id_documento=documento.id_documento 
                and documento.id_tipo_documento=tipo_documento.id_tipo_documento
                and id_solicitud_ascenso=:id_solicitud_ascenso;");

                $query->execute(['id_solicitud_ascenso'=>$id]);
                
                $item = new Estructura();

                while($row=$query->fetch()){
                    $item->id_solicitud_ascenso=$row['id_solicitud_ascenso'];
                    $item->id_solicitud_acta_pida=$row['id_solicitud_acta_pida'];
                    $item->id_documento_pida=$row['id_documento_pida'];
                    $item->acta_pida=$row['acta_pida'];
                    $item->id_tipo_documento=$row['id_tipo_documento'];
                    $item->tipo_documento=$row['tipo_documento'];

                }                
                return $item;

            } catch(PDOException $e){
                return false;
            }
        }

        public function getByIdSolicitud_RESOLUCION($id){
            try{

                $query=$this->db->connect()->prepare("SELECT 
                solicitud_resolucion.id_solicitud_resolucion, 
                solicitud_resolucion.id_solicitud_ascenso, 
                solicitud_resolucion.id_documento as id_documento_res, 
                documento.descripcion as resolucion,
                documento.id_tipo_documento,
                tipo_documento.descripcion as tipo_documento
                from 
                solicitud_ascenso,
                documento,
                solicitud_resolucion,
                tipo_documento 
                where 
                solicitud_resolucion.id_solicitud_ascenso=solicitud_ascenso.id_solicitud_ascenso
                and solicitud_resolucion.id_documento=documento.id_documento 
                and documento.id_tipo_documento=tipo_documento.id_tipo_documento
                and solicitud_resolucion.id_solicitud_ascenso=:id_solicitud_ascenso;");

                $query->execute(['id_solicitud_ascenso'=>$id]);
      
                $item = new Estructura();

                while($row=$query->fetch()){
                    $item->id_solicitud_ascenso=$row['id_solicitud_ascenso'];
                    $item->id_solicitud_resolucion=$row['id_solicitud_resolucion'];
                    $item->id_documento_res=$row['id_documento_res'];
                    $item->resolucion=$row['resolucion'];
                    $item->id_tipo_documento=$row['id_tipo_documento'];
                    $item->tipo_documento=$row['tipo_documento'];
                }   
                return $item;

            } catch(PDOException $e){
                return false;
            }
        }


        // funciones para evaluar si hay verificaciones pendientes 
        // retornan false cuando encuentran un registro sin verificar
        // retornan verdadero cuando todos los registros estan verificados
        public function getDisenoUC($id){
            $items=[];
            try{
                $query=$this->db->connect()->prepare("SELECT * from diseno_uc_docente where id_docente=:id_docente;");
                $query->execute(['id_docente'=>$id]);


                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id_diseno_uc=$row['id_diseno_uc'];
                    $item->nombre=$row['nombre'];
                    $item->ano_creacion=$row['ano_creacion'];
                    $item->id_tramo=$row['id_tramo'];
                    $item->id_unidad_curricular_tipo=$row['id_unidad_curricular_tipo'];
                    $item->id_programa=$row['id_programa'];
                    $item->id_area_conocimiento_ubv=$row['id_area_conocimiento_ubv'];
                    $item->id_documento=$row['id_documento'];
                    $item->id_docente=$row['id_docente'];
                    $item->estatus_verificacion=$row['estatus_verificacion'];
                    if($row['estatus_verificacion']=='Sin Verificar'){
                        return false;
                    }
                    array_push($items,$item);
                }
                return true;
          
            }catch(PDOException $e){
                return false;
            }
          
        }


        public function getTutoria($id){
            $items=[];
            try{
                $query=$this->db->connect()->prepare("SELECT * from tutoria where id_docente=:id_docente;");
                $query->execute(['id_docente'=>$id]);


                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id_tutoria=$row['id_tutoria'];
                    $item->nombre=$row['nombre'];
                    $item->ano_creacion=$row['ano_creacion'];
                    $item->id_tramo=$row['id_tramo'];
                    $item->id_unidad_curricular_tipo=$row['id_unidad_curricular_tipo'];
                    $item->id_programa=$row['id_programa'];
                    $item->id_area_conocimiento_ubv=$row['id_area_conocimiento_ubv'];
                    $item->id_documento=$row['id_documento'];
                    $item->id_docente=$row['id_docente'];
                    $item->estatus_verificacion=$row['estatus_verificacion'];
                    if($row['estatus_verificacion']=='Sin Verificar'){
                        return false;
                    }
                    array_push($items,$item);
                }
                return true;
          
            }catch(PDOException $e){
                return false;
            }
          
        }


        public function getDocenciaUBV($id){
            $items=[];
            try{
                $query=$this->db->connect()->prepare("SELECT * from docencia_previa_ubv where id_docente=:id_docente;");
                $query->execute(['id_docente'=>$id]);


                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id_docencia_previa_ubv=$row['id_docencia_previa_ubv'];
                    $item->id_area_conocimiento_ubv=$row['id_area_conocimiento_ubv'];
                    $item->id_programa=$row['id_programa'];
                    $item->id_eje_regional=$row['id_eje_regional'];
                    $item->id_eje_municipal=$row['id_eje_municipal'];
                    $item->id_aldea=$row['id_aldea'];
                    $item->id_centro_estudio=$row['id_centro_estudio'];
                    $item->fecha_ingreso=$row['fecha_ingreso'];
                    $item->id_docente=$row['id_docente'];
                    $item->estatus_verificacion=$row['estatus_verificacion'];
                    if($row['estatus_verificacion']=='Sin Verificar'){
                        return false;
                    }
                    array_push($items,$item);
                }
                return true;
          
            }catch(PDOException $e){
                return false;
            }
          
        }

        public function getComision($id){
            $items=[];
            try{
                $query=$this->db->connect()->prepare("SELECT * from comision_excedencia where id_docente=:id_docente;");
                $query->execute(['id_docente'=>$id]);


                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id_comision_excedencia=$row['id_comision_excedencia'];
                    $item->id_tipo_info=$row['id_tipo_info'];
                    $item->lugar=$row['lugar'];
                    
                    $item->desde=$row['desde'];
                    $item->hasta=$row['hasta'];
                    $item->designacion_funcion=$row['designacion_funcion'];
                    $item->causa=$row['causa'];
                    $item->aprobacion=$row['aprobacion'];
                    $item->id_docente=$row['id_docente'];
                    $item->estatus_verificacion=$row['estatus_verificacion'];
                    if($row['estatus_verificacion']=='Sin Verificar'){
                        return false;
                    }
                    array_push($items,$item);
                }
                return true;
          
            }catch(PDOException $e){
                return false;
            }
          
        }

        public function getArticuloRevista($id){
            $items=[];
            try{
                $query=$this->db->connect()->prepare("SELECT * from articulo_revista where id_docente=:id_docente;");
                $query->execute(['id_docente'=>$id]);


                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id_articulo_revista=$row['id_articulo_revista'];
                    $item->nombre_revista=$row['nombre_revista'];
                    $item->titulo_articulo=$row['titulo_articulo'];
                    
                    $item->issn_revista=$row['issn_revista'];
                    $item->ano_revista=$row['ano_revista'];
                    $item->nro_revista=$row['nro_revista'];
                    $item->nro_pag_inicial=$row['nro_pag_inicial'];
                    $item->url_revista=$row['url_revista'];
                    $item->id_docente=$row['id_docente'];
                    $item->estatus_verificacion=$row['estatus_verificacion'];
                    if($row['estatus_verificacion']=='Sin Verificar'){
                        return false;
                    }
                    array_push($items,$item);
                }
                return true;
          
            }catch(PDOException $e){
                return false;
            }
        }


        public function getLibro($id){
            $items=[];
            try{
                $query=$this->db->connect()->prepare("SELECT * from libro where id_docente=:id_docente;");
                $query->execute(['id_docente'=>$id]);


                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id_libro=$row['id_libro'];
                    $item->nombre_libro=$row['nombre_libro'];
                    $item->editorial=$row['editorial'];
                    
                    $item->isbn_libro=$row['isbn_libro'];
                    $item->ano_publicacion=$row['ano_publicacion'];
                    $item->ciudad_publicacion=$row['ciudad_publicacion'];
                    $item->volumenes=$row['volumenes'];
                    $item->nro_paginas=$row['nro_paginas'];
                    $item->url_libro=$row['url_libro'];

                    $item->id_docente=$row['id_docente'];
                    $item->estatus_verificacion=$row['estatus_verificacion'];
                    if($row['estatus_verificacion']=='Sin Verificar'){
                        return false;
                    }
                    array_push($items,$item);
                }
                return true;
          
            }catch(PDOException $e){
                return false;
            }
        }

        public function getArteSoftware($id){
            $items=[];
            try{
                $query=$this->db->connect()->prepare("SELECT * from arte_software where id_docente=:id_docente;");
                $query->execute(['id_docente'=>$id]);


                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id_arte_software=$row['id_arte_software'];
                    $item->nombre_arte=$row['nombre_arte'];
                    $item->ano_arte=$row['ano_arte'];
                    
                    $item->entidad_promotora=$row['entidad_promotora'];
                    $item->url_otro=$row['url_otro'];
                    

                    $item->id_docente=$row['id_docente'];
                    $item->estatus_verificacion=$row['estatus_verificacion'];
                    if($row['estatus_verificacion']=='Sin Verificar'){
                        return false;
                    }
                    array_push($items,$item);
                }
                return true;
          
            }catch(PDOException $e){
                return false;
            }
        }

        public function getPonencia($id){
            $items=[];
            try{
                $query=$this->db->connect()->prepare("SELECT * from ponencia where id_docente=:id_docente;");
                $query->execute(['id_docente'=>$id]);

                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id_ponencia=$row['id_ponencia'];
                    $item->nombre_ponencia=$row['nombre_ponencia'];
                    $item->nombre_evento=$row['nombre_evento'];
                    
                    $item->ano_ponencia=$row['ano_ponencia'];
                    $item->ciudad=$row['ciudad'];
                    $item->titulo_memoria=$row['titulo_memoria'];
                    $item->volumen=$row['volumen'];
                    $item->identificador=$row['identificador'];
                    $item->pag_inicial=$row['pag_inicial'];
                    $item->formato=$row['formato'];
                    $item->url_ponencia=$row['url_ponencia'];

                    $item->id_docente=$row['id_docente'];
                    $item->estatus_verificacion=$row['estatus_verificacion'];
                    if($row['estatus_verificacion']=='Sin Verificar'){
                        return false;
                    }
                    array_push($items,$item);
                }
                return true;
          
            }catch(PDOException $e){
                return false;
            }
        }

        public function getReconocimiento($id){
            $items=[];
            try{
                $query=$this->db->connect()->prepare("SELECT * from reconocimiento where id_docente=:id_docente;");
                $query->execute(['id_docente'=>$id]);

                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id_reconocimiento=$row['id_reconocimiento'];
                    $item->id_tipo_reconocimiento=$row['id_tipo_reconocimiento'];
                    $item->descripcion=$row['descripcion'];
                    
                    $item->entidad_promotora=$row['entidad_promotora'];
                    $item->ano_reconocimiento=$row['ano_reconocimiento'];
                    $item->caracter=$row['caracter'];
                    $item->id_documento=$row['id_documento'];
                    

                    $item->id_docente=$row['id_docente'];
                    $item->estatus_verificacion=$row['estatus_verificacion'];
                    if($row['estatus_verificacion']=='Sin Verificar'){
                        return false;
                    }
                    array_push($items,$item);
                }
                return true;
          
            }catch(PDOException $e){
                return false;
            }
        }

        public function getExperiencia($id){
            $items=[];
            try{
                $query=$this->db->connect()->prepare("SELECT * from experiencia_docente where id_docente=:id_docente;");
                $query->execute(['id_docente'=>$id]);

                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id_experiencia_docente=$row['id_experiencia_docente'];
                    $item->id_tipo_experiencia=$row['id_tipo_experiencia'];
                    $item->id_cargo_experiencia=$row['id_cargo_experiencia'];
                    
                    $item->fecha_ingreso=$row['fecha_ingreso'];
                    $item->fecha_egreso=$row['fecha_egreso'];
                    $item->id_estado=$row['id_estado'];
                    $item->id_institucion=$row['id_institucion'];
                    $item->ano_experiencia=$row['ano_experiencia'];


                    $item->id_docente=$row['id_docente'];
                    $item->estatus_verificacion=$row['estatus_verificacion'];
                    if($row['estatus_verificacion']=='Sin Verificar'){
                        return false;
                    }
                    array_push($items,$item);
                }
                return true;
          
            }catch(PDOException $e){
                return false;
            }
        }

        public function getEstudios($id){
            $items=[];
            try{
                $query=$this->db->connect()->prepare("SELECT * from estudio where id_docente=:id_docente;");
                $query->execute(['id_docente'=>$id]);


                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id_estudio=$row['id_estudio'];
                    $item->estudio=$row['estudio'];
                    $item->id_tipo_estudio=$row['id_tipo_estudio'];
                    $item->estatus=$row['estatus'];
                    $item->id_pais=$row['id_pais'];
                    $item->id_modalidad_estudio=$row['id_modalidad_estudio'];
                    $item->ano_estudio=$row['ano_estudio'];
                    $item->id_institucion=$row['id_institucion'];
                    $item->id_docente=$row['id_docente'];
                    $item->estatus_verificacion=$row['estatus_verificacion'];
                    if($row['estatus_verificacion']=='Sin Verificar'){
                        return false;
                    }
                    array_push($items,$item);
                }
                return true;
          
            }catch(PDOException $e){
                return false;
            }
          
        }


        public function updateEstatusSolicitud($id){
            try{

                $query=$this->db->connect()->prepare("UPDATE 
                solicitud_ascenso 
                SET id_estatus_ascenso=:id_estatus_ascenso 
                where id_solicitud_ascenso=:id_solicitud_ascenso;
                ");
                $query->execute([
                    'id_estatus_ascenso'=>$id['id_estatus_ascenso'],
                    'id_solicitud_ascenso'=>$id['id_solicitud_ascenso']
                ]);

                return true;

            }catch(PDOException $e){
                return false;
            }
        }



        /////// PROPUESTA DE JURADO //////////////////////

        //////////////    funcion que llama datos de tablas catalogos   ///////////////
        public function getCatalogo($valor){
            $items=[];
            try{
                $query=$this->db->connect()->query("SELECT * FROM ".$valor."");
                      
                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id=$row['id_'.$valor.''];
                    $item->descripcion=$row['descripcion'];
                      
                    array_push($items,$item);
                      
                      
                }
                return $items;
                      
            }catch(PDOException $e){
                return[];
            }
                      
        }
        ///////////////////////////////////////////////////////
        public function getUbv(){
            $items=[];
            try{
                $query=$this->db->connect()->query("SELECT 
                id_area_conocimiento_ubv as id_ubv,
                 area_conocimiento_ubv.descripcion AS area_ubv,
                  area_conocimiento_ubv.codigo as codigo, 
                  area_conocimiento_ubv.estatus as estatus, 
                  area_conocimiento_ubv.id_area_conocimiento_opsu as id_opsu,
                   area_conocimiento_opsu.descripcion AS area_opsu 
                   FROM area_conocimiento_ubv, area_conocimiento_opsu WHERE area_conocimiento_ubv.id_area_conocimiento_opsu=area_conocimiento_opsu.id_area_conocimiento_opsu");
                
                while($row=$query->fetch()){
                    
                    $item=new Estructura();
                    $item->id_area_conocimiento_ubv=$row['id_ubv'];
                    $item->descripcion=$row['area_ubv'];
                    $item->codigo=$row['codigo'];
                    //$item->estatus=$row['estatus'];
                    if($row['estatus'] == 1){
                        $item->estatus="Activo";
                    }elseif($row['estatus'] == 0){
                        $item->estatus="Inactivo";
                    }
                    //var_dump($item->estatus);
                    $item->id_area_conocimiento_opsu=$row['id_opsu'];
                    $item->descripcion_opsu=$row['area_opsu'];
    
                    $item->codigo=$row['codigo'];
                
                    array_push($items,$item);
                
                
                }
                return $items;
          
            }catch(PDOException $e){
            return[];
            }
          
        }
        
        public function getEjeRegional(){
            $items=[];
            try{
                $query=$this->db->connect()->query("SELECT * from eje_regional where estatus='true';");
                
                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id_eje_regional=$row['id_eje_regional'];
                    $item->descripcion=$row['descripcion'];
                    $item->codigo=$row['codigo'];
                    $item->estatus=$row['estatus'];
                    array_push($items,$item);
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
          
        }

        //obtener docentes que sean de un escalafon superior al del docente que realizo la solicitud
        public function getDocentes($id_escalafon){
            $items=[];
            try{
                //escalafones: 7=auxiliar I, 8=auxiliar II, 9=auxiliar III
                if($id_escalafon == 7 || $id_escalafon == 8 || $id_escalafon == 9){

                    $query=$this->db->connect()->query("SELECT 
                    id_docente,
                    t1.id_persona,
                    identificacion,
                    primer_nombre,
                    primer_apellido,
                    t1.id_escalafon,
                    t4.descripcion as escalafon
                    from 
                    docente as t1,
                    persona as t2,
                    persona_nacionalidad as t3,
                    escalafon as t4
                    where 
                    t1.id_persona=t2.id_persona
                    and t3.id_persona=t2.id_persona
                    and t1.id_escalafon=t4.id_escalafon
                    and t1.id_escalafon<>7
                    and t1.id_escalafon<>8
                    and t1.id_escalafon<>9;"); 
                    

                }else{

                    $query=$this->db->connect()->prepare("SELECT 
                    id_docente,
                    t1.id_persona,
                    identificacion,
                    primer_nombre,
                    primer_apellido,
                    t1.id_escalafon,
                    t4.descripcion as escalafon
                    from 
                    docente as t1,
                    persona as t2,
                    persona_nacionalidad as t3,
                    escalafon as t4
                    where 
                    t1.id_persona=t2.id_persona
                    and t3.id_persona=t2.id_persona
                    and t1.id_escalafon=t4.id_escalafon
                    and t1.id_escalafon<>7
                    and t1.id_escalafon<>8
                    and t1.id_escalafon<>9
                    and t1.id_escalafon>=:id_escalafon;");
                    $query->execute(['id_escalafon'=>$id_escalafon]);

                }
                
                
                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id_docente=$row['id_docente'];
                    $item->id_persona=$row['id_persona'];
                    $item->identificacion=$row['identificacion'];
                    $item->primer_nombre=$row['primer_nombre'];
                    $item->primer_apellido=$row['primer_apellido'];
                    $item->id_escalafon=$row['id_escalafon'];
                    $item->escalafon=$row['escalafon'];
                    array_push($items,$item);
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
          
        }
       
        public function insertPropuesta($datos){
            try{
                //1. guardas el objeto pdo en una variable
                $pdo=$this->db->connect();

                //2. comienzas transaccion
                $pdo->beginTransaction();

                $propuesta=$pdo->prepare("INSERT INTO 
                propuesta_jurado(
                    id_docente, 
                    estatus,
                    id_solicitud_ascenso
                    ) VALUES(
                        :id_docente,
                        :estatus,
                        :id_solicitud_ascenso
                    );
                ");
                $propuesta->execute([
                    'id_docente'=>$datos['id_docente'],
                    'estatus'=>'En Espera',
                    'id_solicitud_ascenso'=>$datos['id_solicitud_ascenso']
                ]);

                $juradoP1=$pdo->prepare("INSERT INTO jurado(
                    id_propuesta_jurado, 
                    id_tipo_jurado, 
                    id_docente
                    ) VALUES(
                    (SELECT id_propuesta_jurado FROM propuesta_jurado ORDER BY id_propuesta_jurado DESC LIMIT 1),
                    :id_tipo_jurado,
                    :id_docente
                    );");
                $juradoP1->execute([
                    'id_tipo_jurado'=>3,
                    'id_docente'=>$datos['docenteP1']
                    ]);

                    $juradoP2=$pdo->prepare("INSERT INTO jurado(
                        id_propuesta_jurado, 
                        id_tipo_jurado, 
                        id_docente
                        ) VALUES(
                        (SELECT id_propuesta_jurado FROM propuesta_jurado ORDER BY id_propuesta_jurado DESC LIMIT 1),
                        :id_tipo_jurado,
                        :id_docente
                        );");
    
                    $juradoP2->execute([
                        'id_tipo_jurado'=>1,
                        'id_docente'=>$datos['docenteP2']
                        ]);
                        $juradoP3=$pdo->prepare("INSERT INTO jurado(
                            id_propuesta_jurado, 
                            id_tipo_jurado, 
                            id_docente
                            ) VALUES(
                            (SELECT id_propuesta_jurado FROM propuesta_jurado ORDER BY id_propuesta_jurado DESC LIMIT 1),
                            :id_tipo_jurado,
                            :id_docente
                            );");
        
                        $juradoP3->execute([
                            'id_tipo_jurado'=>1,
                            'id_docente'=>$datos['docenteP3']
                            ]);

                            $juradoS1=$pdo->prepare("INSERT INTO jurado(
                                id_propuesta_jurado, 
                                id_tipo_jurado, 
                                id_docente
                                ) VALUES(
                                (SELECT id_propuesta_jurado FROM propuesta_jurado ORDER BY id_propuesta_jurado DESC LIMIT 1),
                                :id_tipo_jurado,
                                :id_docente
                                );");
            
                            $juradoS1->execute([
                                'id_tipo_jurado'=>2,
                                'id_docente'=>$datos['docenteS1']
                                ]);

                                $juradoS2=$pdo->prepare("INSERT INTO jurado(
                                    id_propuesta_jurado, 
                                    id_tipo_jurado, 
                                    id_docente
                                    ) VALUES(
                                    (SELECT id_propuesta_jurado FROM propuesta_jurado ORDER BY id_propuesta_jurado DESC LIMIT 1),
                                    :id_tipo_jurado,
                                    :id_docente
                                    );");
                
                                $juradoS2->execute([
                                    'id_tipo_jurado'=>2,
                                    'id_docente'=>$datos['docenteS2']
                                    ]);

                                    $juradoS3=$pdo->prepare("INSERT INTO jurado(
                                        id_propuesta_jurado, 
                                        id_tipo_jurado, 
                                        id_docente
                                        ) VALUES(
                                        (SELECT id_propuesta_jurado FROM propuesta_jurado ORDER BY id_propuesta_jurado DESC LIMIT 1),
                                        :id_tipo_jurado,
                                        :id_docente
                                        );");
                    
                                    $juradoS3->execute([
                                        'id_tipo_jurado'=>2,
                                        'id_docente'=>$datos['docenteS3']
                                        ]);

                                        $estatus=$pdo->query("SELECT * FROM propuesta_jurado ORDER BY id_propuesta_jurado DESC LIMIT 1");
                                        $est=$estatus->fetch();
                                        

                //4. consignas la transaccion (en caso de que no suceda ningun fallo)
                $pdo->commit();
                //retornamos un array para obtener el id y el estatus de la propuesta del jurado 
                return $est;
            }catch(PDOException $e){
                //5. regresas a un estado anterior en caso de error
                $pdo->rollBack();
                //echo "Fallo: " . $e->getMessage();

                return[];
            }
      
        }


        public function getSolicitudEscSig($id_solicitud_ascenso){
            $items=[];
            try{
                $query=$this->db->connect()->prepare("SELECT 
                t1.id_solicitud_ascenso,
                t2.id_escalafon as id_escalafon_siguiente,
                t3.descripcion as escalafon_siguiente,
                t4.id_eje_regional,
                t6.descripcion as eje_regional,
                t7.id_persona_fecha_ingreso,
                t7.fecha as fecha_ingreso,
                t1.id_concurso,
                t8.fecha_concurso,
                fecha_consulta,
                t9.id_persona_nacionalidad,
                identificacion,
                primer_nombre,
                segundo_nombre,
                primer_apellido,
                segundo_apellido,
                titulo,
                t4.id_centro_estudio,
                t11.descripcion as centro_estudio
                
                from 
                solicitud_ascenso as t1, 
                solicitud_escalafon_siguiente as t2,
                escalafon as t3,
                docente as t4,
                persona as t5,
                eje_regional as t6,
                persona_fecha_ingreso as t7,
                concurso as t8,
                persona_nacionalidad as t9,
                trabajo_ascenso as t10,
                centro_estudio as t11
                
                where 
                t2.id_solicitud_ascenso=t1.id_solicitud_ascenso
                and t2.id_escalafon=t3.id_escalafon
                and t1.id_docente=t4.id_docente
                and t4.id_persona=t5.id_persona
                and t4.id_eje_regional=t6.id_eje_regional
                and t7.id_persona=t5.id_persona
                and t1.id_concurso=t8.id_concurso
                and t9.id_persona=t5.id_persona
                and t1.id_trabajo_ascenso=t10.id_trabajo_ascenso
                and t4.id_centro_estudio=t11.id_centro_estudio
                and t1.id_solicitud_ascenso=:id_solicitud_ascenso;
                ");
                $query->execute(['id_solicitud_ascenso'=>$id_solicitud_ascenso]);

                $item=new Estructura();
                while($row=$query->fetch()){
                    
                    $item->id_solicitud_ascenso=$row['id_solicitud_ascenso'];
                    $item->escalafon_siguiente=$row['escalafon_siguiente'];
                    $item->id_eje_regional=$row['id_eje_regional'];
                    $item->eje_regional=$row['eje_regional'];
                    $item->id_persona_fecha_ingreso=$row['id_persona_fecha_ingreso'];
                    $item->fecha_ingreso=$row['fecha_ingreso'];
                    $item->id_concurso=$row['id_concurso'];
                    $item->fecha_concurso=$row['fecha_concurso'];
                    $item->fecha_consulta=date("d-m-Y", strtotime($row['fecha_consulta']));
                    $item->id_persona_nacionalidad=$row['id_persona_nacionalidad'];
                    $item->identificacion=$row['identificacion'];
                    $item->primer_nombre=$row['primer_nombre'];
                    $item->segundo_nombre=$row['segundo_nombre'];
                    $item->primer_apellido=$row['primer_apellido'];
                    $item->segundo_apellido=$row['segundo_apellido'];
                    $item->titulo=$row['titulo'];
                    $item->id_centro_estudio=$row['id_centro_estudio'];
                    $item->centro_estudio=$row['centro_estudio'];

                }
                return $item;
          
            }catch(PDOException $e){
                return[];
            }
          
        }

        public function getDocEscalafonActual($id_docente){
            $items=[];
            try{
                $query=$this->db->connect()->prepare("SELECT 
                docente.id_escalafon, 
                escalafon.descripcion 
                from 
                docente 
                INNER JOIN 
                escalafon 
                ON docente.id_escalafon=escalafon
                .id_escalafon 
                where id_docente=:id_docente;
                ");
                $query->execute(['id_docente'=>$id_docente]);

                $item=new Estructura();
                while($row=$query->fetch()){
                    
                    $item->id_escalafon_actual=$row['id_escalafon'];
                    $item->escalafon_actual=$row['descripcion'];
                    
                }
                return $item;
          
            }catch(PDOException $e){
                return[];
            }
          
        }

        public function getJuradoDocente($id_docente, $id_tipo_jurado){
            $items=[];
            try{
                $query=$this->db->connect()->prepare("SELECT 
                t2.id_propuesta_jurado,
                t2.id_jurado,
                t2.id_tipo_jurado,
                t4.descripcion as tipo_jurado,
                t2.id_docente,
                t3.id_persona,
                identificacion,
                primer_nombre,
                segundo_nombre,
                primer_apellido,
                segundo_apellido,
                t3.id_escalafon,
                t7.descripcion as escalafon_jurado
                
                from
                propuesta_jurado as t1,
                jurado as t2,
                docente as t3,
                tipo_jurado as t4,
                persona as t5,
                persona_nacionalidad as t6,
                escalafon as t7
                where 
                t2.id_propuesta_jurado=t1.id_propuesta_jurado
                and t2.id_docente=t3.id_docente
                and t2.id_tipo_jurado=t4.id_tipo_jurado
                and t3.id_persona=t5.id_persona
                and t6.id_persona=t5.id_persona
                and t3.id_escalafon=t7.id_escalafon
                and t1.id_docente=:id_docente
                and t2.id_tipo_jurado=:id_tipo_jurado;
                ");
                $query->execute([
                    'id_docente'=>$id_docente,
                    'id_tipo_jurado'=>$id_tipo_jurado
                    ]);

                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id_propuesta_jurado=$row['id_propuesta_jurado'];
                    $item->id_jurado=$row['id_jurado'];
                    $item->id_tipo_jurado=$row['id_tipo_jurado'];
                    $item->tipo_jurado=$row['tipo_jurado'];
                    $item->id_docente=$row['id_docente'];
                    $item->id_persona=$row['id_persona'];
                    $item->identificacion=$row['identificacion'];
                    $item->primer_nombre=$row['primer_nombre'];
                    $item->segundo_nombre=$row['segundo_nombre'];
                    $item->primer_apellido=$row['primer_apellido'];
                    $item->segundo_apellido=$row['segundo_apellido'];

                    $item->id_escalafon=$row['id_escalafon'];
                    $item->escalafon_jurado=$row['escalafon_jurado'];
                    array_push($items,$item);
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
          
        }


    public function getPropuesta($id_solicitud_ascenso){
        try{
            $items=[];
            $query=$this->db->connect()->prepare("SELECT
            estatus 
            from 
            propuesta_jurado 
            where 
            id_solicitud_ascenso=:id_solicitud_ascenso
            ORDER BY 
            id_propuesta_jurado 
            DESC LIMIT 1;");
            $query->execute(['id_solicitud_ascenso'=>$id_solicitud_ascenso]);

            $estatus=$query->fetch();
            return $estatus['estatus'];
        }catch(PDOException $e){
            return[];
        }
    }

    public function updateEstatusPropuesta($datos){
        try{
            $query=$this->db->connect()->prepare("UPDATE 
            propuesta_jurado 
            set estatus=:estatus 
            where id_solicitud_ascenso=:id_solicitud_ascenso;
            ");
            $query->execute([
                'estatus'=>$datos['estatus'],
                'id_solicitud_ascenso'=>$datos['id_solicitud_ascenso']
            ]);


            
            return true;

        }catch(PDOException $e){
            return false;
        }
    }

    public function insertMemo($datos){
        try{
           
            //1. guardas el objeto pdo en una variable
            $pdo=$this->db->connect();

            //2. comienzas transaccion
            $pdo->beginTransaction();
            $doc=new Documento();
            $pdf_memo_solicitud=$doc->getData($datos['pdf_memo_solicitud']);

                //mover documento acta PROYECTO
                //el mover no se esta ejecutando: revisar extensiones y permisos
                if($doc->mover($pdf_memo_solicitud['extension'], 
                    'src/documentos/ascensos/',
                    $pdf_memo_solicitud['ruta_tmp'],
                    $pdf_memo_solicitud['original_name'],
                    array('pdf'))
                ){
                    
                    $doc_memo_solicitud1=$pdo->prepare("INSERT INTO documento(
                        descripcion,
                        estatus,
                        id_persona,
                        id_tipo_documento) 
                        VALUES(
                            :descripcion,
                            :estatus, 
                            :id_persona,
                            (SELECT id_tipo_documento FROM tipo_documento WHERE descripcion=:tipo_documento) 
                            );");
                    $doc_memo_solicitud1->execute([
                        'descripcion'=>date('Y-m-d H:i:s').$pdf_memo_solicitud['original_name'],
                        'estatus'=>'Sin Verificar',
                        'id_persona'=>$datos['id_persona'],
                        'tipo_documento'=>$datos['tipo_documento']
                        ]);
                        
                    $memo_solicitud1=$pdo->prepare("INSERT INTO 
                        memo_solicitud(
                        id_documento, 
                        id_solicitud_ascenso,
                        estatus
                        ) VALUES(
                            (select id_documento from documento ORDER BY id_documento DESC LIMIT 1),
                            :id_solicitud_ascenso,
                            :estatus);");
                    $memo_solicitud1->execute([
                        'id_solicitud_ascenso'=>$datos['id_solicitud_ascenso'],
                        'estatus'=>'En Espera'
                        ]);
                        
                } 
            
                //4. consignas la transaccion (en caso de que no suceda ningun fallo)
                $pdo->commit();
                return true; 

        }catch(PDOException $e){
             //5. regresas a un estado anterior en caso de error
             $pdo->rollBack();
             //echo "Fallo: " . $e->getMessage();
            return false;
        }
    }

    public function getMemos($id_solicitud_ascenso, $id_tipo_documento){
        try{
            $items=[];
            $query=$this->db->connect()->prepare("SELECT 
            id_memo_solicitud,
            memo_solicitud.id_documento,
            documento.descripcion,
            documento.id_tipo_documento,
            tipo_documento.descripcion as tipo_documento,
            memo_solicitud.estatus
            from memo_solicitud,
            documento,
            tipo_documento
            where 
            memo_solicitud.id_documento=documento.id_documento
            and documento.id_tipo_documento=tipo_documento.id_tipo_documento
            and documento.id_tipo_documento=:id_tipo_documento
            and id_solicitud_ascenso=:id_solicitud_ascenso;");
            $query->execute([
                'id_tipo_documento'=>$id_tipo_documento,
                'id_solicitud_ascenso'=>$id_solicitud_ascenso
                ]);

                $item = new Estructura();

                while($row=$query->fetch()){
                    $item->id_memo_solicitud=$row['id_memo_solicitud'];
                    $item->id_documento=$row['id_documento'];
                    $item->descripcion=$row['descripcion'];
                    $item->id_tipo_documento=$row['id_tipo_documento'];
                    $item->tipo_documento=$row['tipo_documento'];
                    $item->estatus=$row['estatus'];
                }   
                return $item;

        }catch(PDOException $e){
            return[];
        }
    }

    public function updateEstatusMemo($datos){
        try{
            
            $query=$this->db->connect()->prepare("UPDATE 
            memo_solicitud as t1 
            set estatus=:estatus 
            from  documento as t3 
          
            where 
            t1.id_documento=t3.id_documento
            and t3.id_tipo_documento=:id_tipo_documento
            and t1.id_solicitud_ascenso=:id_solicitud_ascenso;");
            $query->execute([
                'estatus'=>$datos['estatus'],
                'id_solicitud_ascenso'=>$datos['id_solicitud_ascenso'],
                'id_tipo_documento'=>$datos['id_tipo_documento']

            ]);
            
            return true;

        }catch(PDOException $e){
            return false;
        }
    }

    public function insertMemoPtoCuenta($datos){
        try{
            
            
            //1. guardas el objeto pdo en una variable
            $pdo=$this->db->connect();

            //2. comienzas transaccion
            $pdo->beginTransaction();
            $doc=new Documento();
            $pdf_memo_solicitud2=$doc->getData($datos['pdf_memo_solicitud']);
            
            
            
                //mover documento acta PROYECTO
                //el mover no se esta ejecutando: revisar extensiones y permisos
                if($doc->mover($pdf_memo_solicitud2['extension'], 
                    'src/documentos/ascensos/',
                    $pdf_memo_solicitud2['ruta_tmp'],
                    $pdf_memo_solicitud2['original_name'],
                    array('pdf'))
                ){
                    
                    $doc_memo_solicitud2=$pdo->prepare("INSERT INTO documento(
                        descripcion,
                        estatus,
                        id_persona,
                        id_tipo_documento) 
                        VALUES(
                            :descripcion,
                            :estatus, 
                            :id_persona,
                            (SELECT id_tipo_documento FROM tipo_documento WHERE descripcion=:tipo_documento) 
                            );");
                    $doc_memo_solicitud2->execute([
                        'descripcion'=>date('Y-m-d H:i:s').$pdf_memo_solicitud2['original_name'],
                        'estatus'=>'Sin Verificar',
                        'id_persona'=>$datos['id_persona'],
                        'tipo_documento'=>$datos['tipo_documento1']
                        ]);
                    
                    $memo_solicitud2=$pdo->prepare("INSERT INTO 
                        memo_solicitud(
                        id_documento, 
                        id_solicitud_ascenso,
                        estatus
                        ) VALUES(
                            (select id_documento from documento ORDER BY id_documento DESC LIMIT 1),
                            :id_solicitud_ascenso,
                            :estatus);");

                    $memo_solicitud2->execute([
                        'id_solicitud_ascenso'=>$datos['id_solicitud_ascenso'],
                        'estatus'=>'En Espera'
                        ]);
                    
                } 
                $pto_cuenta_solicitud1=$doc->getData($datos['pdf_pto_cuenta_solicitud']);
                if($doc->mover($pto_cuenta_solicitud1['extension'], 
                    'src/documentos/ascensos/',
                    $pto_cuenta_solicitud1['ruta_tmp'],
                    $pto_cuenta_solicitud1['original_name'],
                    array('pdf'))
                ){
                    
                    $doc_pto_cuenta_solicitud1=$pdo->prepare("INSERT INTO documento(
                        descripcion,
                        estatus,
                        id_persona,
                        id_tipo_documento) 
                        VALUES(
                            :descripcion,
                            :estatus, 
                            :id_persona,
                            (SELECT id_tipo_documento FROM tipo_documento WHERE descripcion=:tipo_documento) 
                            );");
                    $doc_pto_cuenta_solicitud1->execute([
                        'descripcion'=>date('Y-m-d H:i:s').$pto_cuenta_solicitud1['original_name'],
                        'estatus'=>'Sin Verificar',
                        'id_persona'=>$datos['id_persona'],
                        'tipo_documento'=>$datos['tipo_documento2']

                        ]);
                   
                    $pto_cuenta_solicitud1=$pdo->prepare("INSERT INTO 
                        memo_solicitud(
                        id_documento, 
                        id_solicitud_ascenso,
                        estatus
                        ) VALUES(
                            (select id_documento from documento ORDER BY id_documento DESC LIMIT 1),
                            :id_solicitud_ascenso,
                            :estatus);");
                    $pto_cuenta_solicitud1->execute([
                        'id_solicitud_ascenso'=>$datos['id_solicitud_ascenso'],
                        'estatus'=>'En Espera'
                        ]);
                }
                
                //4. consignas la transaccion (en caso de que no suceda ningun fallo)
                $pdo->commit();
                return true; 

        }catch(PDOException $e){
             //5. regresas a un estado anterior en caso de error
             $pdo->rollBack();
             //echo "Fallo: " . $e->getMessage();
            return false;
        }
    }


    public function existeJurado($id_solicitud_ascenso){
        try{
    
            //var_dump($id_solicitud_ascenso);
            $sql=$this->db->connect()->prepare("SELECT * FROM propuesta_jurado WHERE id_solicitud_ascenso=:id_solicitud_ascenso;");
            $sql->execute(['id_solicitud_ascenso' =>$id_solicitud_ascenso]);
            $nombre=$sql->fetch();
            
            if($id_solicitud_ascenso==$nombre['id_solicitud_ascenso']){
                
                return $nombre['id_solicitud_ascenso'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }


        ///////////// FIN PROPUESTA DE JURADO ////////////////////

        public function updateArchivo($datos){
            try{
                

                
                 //1. guardas el objeto pdo en una variable
                $pdo=$this->db->connect();

                //2. comienzas transaccion
                $pdo->beginTransaction();
                $doc=new Documento();
                $file=$doc->getData($datos['file']);
                if($doc->mover($file['extension'], 
                'src/documentos/ascensos/',
                $file['ruta_tmp'],
                $file['original_name'],
                array('pdf'))
                ){
                   
                    $query=$pdo->prepare("UPDATE 
                    documento as t1 
                    set descripcion=:descripcion
                    from  memo_solicitud as t3 
                    where 
                    t1.id_documento=t3.id_documento
                    and t1.id_tipo_documento=:id_tipo_documento
                    and t3.id_solicitud_ascenso=:id_solicitud_ascenso;");

                    $query->execute([
                        'descripcion'=>date('Y-m-d H:i:s').$file['original_name'],
                        'id_tipo_documento'=>$datos['id_tipo_documento'],
                        'id_solicitud_ascenso'=>$datos['id_solicitud_ascenso']
                    ]);
                }
                //4. consignas la transaccion (en caso de que no suceda ningun fallo)
                $pdo->commit();
                return true;

            }catch(PDOException $e){
                //5. regresas a un estado anterior en caso de error
                $pdo->rollBack();
                //echo "Fallo: " . $e->getMessage();
                return false;
            }
        }



        //obtener coorreos
        public function getCorreoCoord($datos){
            
            try{

                $sql = $this->db->connect()->prepare("SELECT 
                id_usuario, 
                usuario, 
                t1.id_perfil,
                t2.descripcion as perfil,
                t1.id_persona,
                identificacion,
                primer_nombre,
                primer_apellido,
                correo
                
                
                from 
                usuario as t1,
                perfil as t2, 
                persona as t3,
                persona_nacionalidad as t4,
                persona_correo as t5

                where 
                t1.id_persona=t3.id_persona
                and t4.id_persona=t3.id_persona
                and t1.id_perfil=t2.id_perfil
                and t5.id_persona=t3.id_persona
                and t3.id_persona=(SELECT t1.id_persona from persona_centro_estudio as t1,
							persona as t2,
							docente as t3,
							usuario as t4
							where t1.id_persona=t2.id_persona
							and t2.id_persona=t3.id_persona
							and t4.id_persona=t2.id_persona
							and t3.id_centro_estudio=:id_centro_estudio 
							and t4.id_perfil=:id_perfil
							and t1.estatus=:estatus LIMIT 1);");

                $sql->execute([
                    'id_centro_estudio'=>$datos['id_centro_estudio'],
                    'id_perfil'=>$datos['id_perfil'],//PERFIL 2 ES EL PERFIL DE COORDINADOR
                    'estatus'=>'Activo'
                    ]);

                $item = new Estructura();
                while($row=$sql->fetch()){
                    $item->perfil=$row['perfil'];
                    $item->identificacion=$row['identificacion'];
                    $item->primer_nombre=$row['primer_nombre'];
                    $item->primer_apellido=$row['primer_apellido'];
                    $item->correo=$row['correo'];

                }
                return $item;
        
            } catch(PDOException $e){
                return false;
            }

        }

        public function insertDeclinar($datos){
            
            try{
                
                $query=$this->db->connect()->prepare("INSERT INTO solicitud_declinar(
                    descripcion, 
                    id_solicitud_ascenso,
                    id_tipo_declinar) VALUES(
                        :descripcion, 
                        :id_solicitud_ascenso,
                        :id_tipo_declinar
                    );
                ");
                $query->execute([
                    'descripcion'=>$datos['descripcion'],
                    'id_solicitud_ascenso'=>$datos['id_solicitud_ascenso'],
                    'id_tipo_declinar'=>$datos['id_tipo_declinar']
                ]);
    
                return true;

            } catch(PDOException $e){
                return false;
            }
        }


        //obtener motivo de declinacion
        public function getMotivoD($id){

            try{
                
                $sql = $this->db->connect()->prepare("SELECT * from solicitud_declinar 
                where id_solicitud_ascenso=:id_solicitud_ascenso;");
                $sql->execute(['id_solicitud_ascenso'=>$id]);
                $item = new Estructura();

                while($row=$sql->fetch()){
                    $item->id_solicitud_declinar=$row['id_solicitud_declinar'];
                    $item->id_solicitud_ascenso=$row['id_solicitud_ascenso'];
                    $item->descripcion=$row['descripcion'];
                }
                
                return $item;
            }catch(PDOExcepton $e){
                return null;
            }
        }


        //obtener correos de perfiles DGTA
        public function getUsersDGTA(){
            $items=[];
            try{
                $query=$this->db->connect()->query("SELECT id_usuario, 
                usuario, 
                t4.id_perfil,
                t5.descripcion as perfil,
                t2.id_persona,
                identificacion,
                primer_nombre,
                primer_apellido,
                correo from 
                persona as t2,
                docente as t3,
                usuario as t4,
                perfil as t5,
                persona_nacionalidad as t6,
                persona_correo as t7
                where 
                t2.id_persona=t3.id_persona
                and t4.id_persona=t2.id_persona
                and t4.id_perfil=t5.id_perfil
                and t4.id_perfil=t5.id_perfil
                and t6.id_persona=t2.id_persona
                and t7.id_persona=t2.id_persona
                and (t4.id_perfil=4 or t4.id_perfil=5);");
                

                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id_usuario=$row['id_usuario'];
                    $item->id_perfil=$row['id_perfil'];
                    $item->perfil=$row['perfil'];
                    $item->id_persona=$row['id_persona'];
                    $item->identificacion=$row['identificacion'];
                    $item->id_persona=$row['id_persona'];
                    $item->identificacion=$row['identificacion'];
                    $item->primer_nombre=$row['primer_nombre'];
                    $item->primer_apellido=$row['primer_apellido'];
                    $item->correo=$row['correo'];
                    array_push($items,$item);
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
          
        }

        //obtener correos de perfiles de Vicerrectorado
        public function getUsersVICE(){
            $items=[];
            try{
                $query=$this->db->connect()->query("SELECT id_usuario, 
                usuario, 
                t4.id_perfil,
                t5.descripcion as perfil,
                t2.id_persona,
                identificacion,
                primer_nombre,
                primer_apellido,
                correo from 
                persona as t2,
                docente as t3,
                usuario as t4,
                perfil as t5,
                persona_nacionalidad as t6,
                persona_correo as t7
                where 
                t2.id_persona=t3.id_persona
                and t4.id_persona=t2.id_persona
                and t4.id_perfil=t5.id_perfil
                and t4.id_perfil=t5.id_perfil
                and t6.id_persona=t2.id_persona
                and t7.id_persona=t2.id_persona
                and t4.id_perfil=6;");
                

                while($row=$query->fetch()){
                    $item=new Estructura();
                    $item->id_usuario=$row['id_usuario'];
                    $item->id_perfil=$row['id_perfil'];
                    $item->perfil=$row['perfil'];
                    $item->id_persona=$row['id_persona'];
                    $item->identificacion=$row['identificacion'];
                    $item->id_persona=$row['id_persona'];
                    $item->identificacion=$row['identificacion'];
                    $item->primer_nombre=$row['primer_nombre'];
                    $item->primer_apellido=$row['primer_apellido'];
                    $item->correo=$row['correo'];
                    array_push($items,$item);
                }
                return $items;
          
            }catch(PDOException $e){
                return[];
            }
          
        }

    }
?>