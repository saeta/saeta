<?php
include_once 'datosacademicos/turno.php';

class TurnoModel extends Model{
    public function __construct(){
        parent::__construct();
    }


    public function existe($turno){
        try{
    
            //var_dump($turno);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM turno WHERE descripcion=:descripcion");
            $sql->execute(['descripcion' =>$turno]);
            $nombre=$sql->fetch();
            
            if($turno==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }

    public function insert($datos){
        //echo "<br>insertar datos";
    
        try{
         
            $query=$this->db->connect()->prepare('INSERT INTO turno(descripcion) VALUES(:descripcion)');
    
                $query->execute([
                    'descripcion'=>$datos['descripcion'],
                ]);
    
                return true;
          
        }catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                 }
        
        
    }

    

    public function getTurno(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT * FROM turno");
            
            while($row=$query->fetch()){
                
                $item=new Turno_ent();
                $item->id_turno=$row['id_turno'];
                $item->descripcion=$row['descripcion'];
            
                array_push($items,$item);
            
            
            }
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }
      
    public function update($datos){
   //var_dump($datos);
        //var_dump($item['id_pais'],$item['estado'],$item['codigoine'],$item['id_estado']);
        $query=$this->db->connect()->prepare("UPDATE turno SET descripcion=:descripcion
        WHERE id_turno=:id_turno");
        
        try{
            $query->execute([
                'id_turno'=>$datos['id_turno'],
                'descripcion'=>$datos['descripcion'],         
                     ]);
         return true;

        }catch(PDOException $e){
             return false;
        }
         
    }

        public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM turno WHERE id_turno = :id_turno");
    
            try{
                $query->execute([
                'id_turno'=>$id
                ]);
                return true;
            }catch(PDOException $e){
                return false;
            }
        }
    }


    ?>