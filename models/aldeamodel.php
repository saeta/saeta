<?php
include_once 'models/estructura.php';
class AldeaModel extends Model{
    public function __construct(){
    parent::__construct();
    }
    public function existe($codigoaldea){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion,codigo_aldea FROM aldea WHERE codigo_aldea=:codigo_aldea");
            $sql->execute(['codigo_aldea' =>$codigoaldea]);
            $nombre=$sql->fetch();
            
            if($codigoaldea==$nombre['codigo_aldea']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
    //echo "<br>insertar datos";

  

    try{
     
            
           
            $query2=$this->db->connect()->prepare('INSERT INTO aldea (descripcion,codigo_sucre,codigo_aldea,estatus,id_aldea_tipo,id_ambiente) VALUES (:descripcion,:codigo_sucre,:codigo_aldea,:estatus,:id_aldea_tipo,:id_ambiente)');
            $query2->execute(['descripcion'=>$datos['aldea'],'codigo_sucre'=>$datos['codigosucre'],'codigo_aldea'=>$datos['codigoaldea'],'estatus'=>$datos['estatus'],'id_aldea_tipo'=>$datos['aldeatipo'],'id_ambiente'=>$datos['ambiente']]);
          
           /* $query3=$this->db->connect()->prepare(' INSERT INTO aldea_telefono (descripcion,id_telefono_codigo_area,id_aldea) select :descripcion,id_telefono_codigo_area,id_aldea from aldea, telefono_codigo_area order by id_aldea desc limit 1');
            $query3->execute(['descripcion'=>$datos['numero']]);*/
          


            return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }
    
    
            }

            public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT aldea.id_aldea,aldea.descripcion AS aldea,codigo_sucre,codigo_aldea,aldea.estatus  AS estatus_aldea,ambiente.id_ambiente,ambiente.descripcion AS ambiente,ambiente.estatus AS estatus_ambiente,aldea_tipo.id_aldea_tipo,aldea_tipo.descripcion AS aldea_tipo FROM aldea,ambiente,aldea_tipo WHERE  aldea.id_ambiente=ambiente.id_ambiente AND aldea.id_aldea_tipo=aldea_tipo.id_aldea_tipo");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_aldea=$row['id_aldea'];
              $item->aldea=$row['aldea'];
              $item->codigo_sucre=$row['codigo_sucre'];
              $item->codigo_aldea=$row['codigo_aldea'];          
              $item->estatus_aldea=$row['estatus_aldea'];
              $item->id_ambiente=$row['id_ambiente'];
              $item->ambiente=$row['ambiente'];

              $item->id_aldea_tipo=$row['id_aldea_tipo'];

              $item->aldea_tipo=$row['aldea_tipo'];
              $item->id_aldea_tipo=$row['id_aldea_tipo'];
              $item->estatus_ambiente=$row['estatus_ambiente'];
              
              
              
            
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }

              public function getAmbiente(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT id_ambiente,descripcion FROM ambiente WHERE estatus ='1'");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_ambiente=$row['id_ambiente'];
              $item->descripcion=$row['descripcion'];
            
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }

            public function getAldeaTipo(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT id_aldea_tipo,descripcion FROM aldea_tipo");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_aldea_tipo=$row['id_aldea_tipo'];
              $item->descripcion=$row['descripcion'];
            
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }

 


              public function update($item){
     

         //  var_dump($item['parroquia'],$item['codigoine'],$item['id_municipio'],$item['id_eje_municipal'],$item['id_parroquia']);
              $query=$this->db->connect()->prepare("UPDATE aldea SET descripcion= :descripcion,codigo_sucre= :codigo_sucre,codigo_aldea=:codigo_aldea,estatus=:estatus,id_aldea_tipo=:id_aldea_tipo,id_ambiente=:id_ambiente  WHERE id_aldea= :id_aldea");
            try{
                        $query->execute([
                        'descripcion'=>$item['aldea'],
                        'codigo_sucre'=>$item['codigosucre'],                        
                        'codigo_aldea'=>$item['codigoaldea'],
                        'estatus'=>$item['estatus'],

                        'id_aldea_tipo'=>$item['aldeatipo'],
                        'id_ambiente'=>$item['ambiente'],
  
                        'id_aldea'=>$item['id_aldea'],
                        
                        ]);
            return true;
                       
                        
            }catch(PDOException $e){
                return false;
                 }
            
                }





    }

    ?>