<?php
include_once 'datosacademicos/nucleo_academico.php';
include_once 'datosacademicos/centro_estudio.php';

class NucleoacademicoModel extends Model{
    public function __construct(){
        parent::__construct();
    }

    public function existe($nucleo_academico){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM nucleo_academico WHERE  descripcion=:descripcion");
            $sql->execute(['descripcion' =>$nucleo_academico]);
            $nombre=$sql->fetch();
            
            if($nucleo_academico==$nombre['descripcion']){
                return $nombre['descripcion'];
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
        //echo "<br>insertar datos";
        try{
            
            $query=$this->db->connect()->prepare('INSERT INTO nucleo_academico(
                descripcion,
                id_centro_estudio) VALUES(:descripcion, :id_centro_estudio)');
           
            $query->execute([
                'descripcion'=>$datos['descripcion'],
                'id_centro_estudio'=>$datos['id_centro_estudio']
                ]);
            
            return true;
       
        }
        catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                
        }
  
    }

    public function getCentro(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT id_centro_estudio, descripcion, codigo,id_area_conocimiento_opsu,estatus  FROM centro_estudio");
            
            while($row=$query->fetch()){
                
                $item=new Centro();
                $item->id_centro_estudio=$row['id_centro_estudio'];
                $item->descripcion=$row['descripcion'];
                $item->codigo=$row['codigo'];
                $item->id_area_conocimiento_opsu=$row['id_area_conocimiento_opsu'];
                $item->estatus=$row['estatus'];

                array_push($items,$item);
            
            
            }
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }

    public function getNucleo(){
            $items=[];
            try{
                $query=$this->db->connect()->query("SELECT id_nucleo_academico as id_nucleo, nucleo_academico.descripcion AS nucleo,  nucleo_academico.id_centro_estudio as id_centro, centro_estudio.descripcion AS centro FROM nucleo_academico, centro_estudio WHERE  nucleo_academico.id_centro_estudio=centro_estudio.id_centro_estudio");
                
                while($row=$query->fetch()){
                    
                    $item=new Nucleo();
                    $item->id_nucleo_academico=$row['id_nucleo'];
                    $item->descripcion=$row['nucleo'];
                    $item->id_centro_estudio=$row['id_centro'];
                    $item->descripcion_centro=$row['centro'];
                
                    array_push($items,$item);
                
                
                }
                return $items;
          
            }catch(PDOException $e){
            return[];
            }
          
    }

    public function update($datos){
            //var_dump($datos);
                  //var_dump($item['id_pais'],$item['estado'],$item['codigoine'],$item['id_estado']);
                  $query=$this->db->connect()->prepare("UPDATE nucleo_academico SET descripcion=:descripcion, 
                  id_centro_estudio=:id_centro_estudio WHERE id_nucleo_academico=:id_nucleo_academico");
                  
                  try{
                      $query->execute([
                          'id_nucleo_academico'=>$datos['id_nucleo_academico'],
                          'descripcion'=>$datos['descripcion'],
                          'id_centro_estudio'=>$datos['id_centro_estudio'],
                                   
                               ]);
                   return true;
          
                  }catch(PDOException $e){
                       return false;
                  }
                   
    }

    public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM nucleo_academico WHERE id_nucleo_academico = :id_nucleo_academico");
        
                try{
                $query->execute([
                    'id_nucleo_academico'=>$id,
                    ]);
                        return true;
        
                }catch(PDOException $e){
        return false;
    }
           
  }

}

?>