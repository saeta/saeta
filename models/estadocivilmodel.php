<?php
include_once 'models/persona.php';
class EstadoCivilModel extends Model{
    public function __construct(){
    parent::__construct();
    }
    public function existe($estadocivil){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM estado_civil WHERE  descripcion=:descripcion");
            $sql->execute(['descripcion' =>$estadocivil]);
            $nombre=$sql->fetch();
            
            if($estadocivil==$nombre['descripcion']){
                return $nombre['descripcion'];
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
    //echo "<br>insertar datos";

    try{
   //  var_dump($datos['estadocivil'],$datos['codigo'],$datos['estado']);


             $query=$this->db->connect()->prepare('INSERT INTO estado_civil(descripcion, codigo) VALUES (:descripcion, :codigo)');

            $query->execute(['descripcion'=>$datos['estadocivil'],'codigo'=>$datos['codigo']]);

            return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }
    
    
            }

            public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT * FROM estado_civil WHERE id_estado_civil = id_estado_civil");
              
              while($row=$query->fetch()){
              $item=new Persona();
              $item->id_estado_civil=$row['id_estado_civil'];
              $item->descripcion=$row['descripcion'];
              $item->codigo=$row['codigo'];
         
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }
             
/*
public function getbyId($id){
    $item = new Publicacion();

    $query = $this->db->connect()->prepare("SELECT * FROM publicacion WHERE id_publicacion = :id_publicacion");
    
    try{
        $query ->execute(['id_publicacion'=>$id]);

        while($row = $query->fetch()){
            $item->titulo = $row['titulo'];
            $item->extracto = $row['extracto'];
            $item->categoria = $row['categoria'];

        }
        return $item;
    }catch(PDOExcepton $e){
        return null;
    }
}*/


public function update($item){
   
    //var_dump($item['id_pais'],$item['estado'],$item['codigo'],$item['id_estado']);

          

         $query=$this->db->connect()->prepare("UPDATE estado_civil SET descripcion=:descripcion , codigo=:codigo WHERE id_estado_civil=:id_estado_civil");
     try{
                 $query->execute([
                 'descripcion'=>$item['estadocivil'],
                 'codigo'=>$item['codigo'],          
                 
                 'id_estado_civil'=>$item['id_estado_civil'],
                 
                 ]);
     return true;
                
                 
     }catch(PDOException $e){
         return false;
          }
     
         }

         public function delete($id_estado_civil){
            $query=$this->db->connect()->prepare("DELETE FROM estado_civil WHERE id_estado_civil=:id_estado_civil");
        
                try{
                $query->execute([
                    'id_estado_civil'=>$id_estado_civil,
                    ]);
                        return true;
        
                }catch(PDOException $e){
        return false;
        }
        
        
            }








    }

    ?>