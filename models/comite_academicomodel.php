<?php
include_once 'models/estructura.php';
class comite_academicoModel extends Model{
    public function __construct(){
    parent::__construct();
    }
    public function existe($id_programa,$id_comite_tipo){
        try{
    // se valida que un programa y un mismo tipo se registren de forma duplicada en bd
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT programa.id_programa AS id_programa,programa.descripcion AS programa ,comite_tipo.id_comite_tipo,comite_tipo.descripcion AS comite_tipo FROM comitepfa_integrante,programa,comite_tipo WHERE comitepfa_integrante.id_programa=programa.id_programa AND comitepfa_integrante.id_comite_tipo=comite_tipo.id_comite_tipo AND programa.id_programa=:id_programa  AND comite_tipo.id_comite_tipo=:id_comite_tipo");
           
           
            $sql->execute(['id_programa' =>$id_programa,'id_comite_tipo' =>$id_comite_tipo]);
            $nombre=$sql->fetch();
            
           // var_dump($nombre['id_programa'],$nombre['id_comite_tipo']);
            if($id_programa==$nombre['id_programa'] && $id_comite_tipo==$nombre['id_comite_tipo']){
                
                return $nombre['programa']. "</b> con tipo de comite : <b>" .$nombre['comite_tipo'];
    
            }
        
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function existeDocente($docentes,$nombre_comite){
        try{
    // se valida que un programa y un mismo tipo se registren de forma duplicada en bd
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT comitepfa_integrante.id_docente,primer_nombre,primer_apellido,comitepfa_integrante.descripcion FROM comitepfa_integrante,docente,persona WHERE comitepfa_integrante.id_docente=docente.id_docente AND  docente.id_persona=persona.id_persona AND comitepfa_integrante.id_docente=:id_docente AND comitepfa_integrante.descripcion=:descripcion");
           
           
            $sql->execute(['id_docente' =>$docentes,'descripcion' =>$nombre_comite]);
            $nombre=$sql->fetch();
            
           // var_dump($nombre['id_programa'],$nombre['id_comite_tipo']);
            if($docentes==$nombre['id_docente'] && $nombre_comite==$nombre['descripcion']){
                
                return $nombre['primer_nombre']. " " .$nombre['primer_apellido'];
    
            }
        
            return false;
        } catch(PDOException $e){
            return false;
        }
    }

    public function insert($datos){
    //echo "<br>insertar datos";
 
    try{



            /*if($datos['estatus']=='1'){
            $fecha_inicio=date("Y-m-d");
           // $fecha_fin="";
            }else if($datos['estatus']=='0'){
            $fecha_inicio=date("Y-m-d");
            $fecha_fin=date("Y-m-d");
            }*/
            
            $fecha_inicio=date("Y-m-d");

            $docentes=$datos['docentes'];

           // var_dump($docentes,$fecha_inicio,$fecha_fin,$datos['estatus'],$datos['id_programa'],$datos['id_comite_tipo']);

            $query2=$this->db->connect()->prepare('INSERT INTO comitepfa_integrante (fecha_inicio,fecha_fin,estatus,id_programa,id_docente,id_comite_tipo,descripcion) VALUES (:fecha_inicio,:fecha_fin,:estatus,:id_programa,:id_docente,:id_comite_tipo,:descripcion)');
          
           for ($i=0;$i<count($docentes);$i++)    
              {  
            $query2->execute(['fecha_inicio'=>$fecha_inicio,'fecha_fin'=>$fecha_fin,'estatus'=>$datos['estatus'],'id_programa'=>$datos['id_programa'],'id_docente'=>$docentes[$i],'id_comite_tipo'=>$datos['id_comite_tipo'],'descripcion'=>$datos['nombre_comite']]);
        
             }
             
    
            return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }   
    
            }



            public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT DISTINCT comite_tipo.id_comite_tipo,programa.id_programa,programa.descripcion AS programa,comite_tipo.descripcion AS comite_tipo,comitepfa_integrante.descripcion AS nombre_comite FROM comitepfa_integrante,comite_tipo,programa WHERE comitepfa_integrante.id_comite_tipo=comite_tipo.id_comite_tipo AND comitepfa_integrante.id_programa=programa.id_programa");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_programa=$row['id_programa'];
              $item->id_comite_tipo=$row['id_comite_tipo'];            
              $item->programa=$row['programa'];          
              $item->comite_tipo=$row['comite_tipo'];
              $item->nombre_comite=$row['nombre_comite'];
              
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }
              public function getHistory(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT DISTINCT comite_nombre,fecha_registro,comite_programa,comite_tipo FROM history_comitepfa_integrante ORDER BY fecha_registro DESC ");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              
            // $item->id_comitepfa_integrante=$row['id_comitepfa_integrante'];
              $item->comite_nombre=$row['comite_nombre'];
              $item->fecha_registro=$row['fecha_registro'];            
              $item->comite_programa=$row['comite_programa'];          
              $item->comite_tipo=$row['comite_tipo'];
       
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }



              public function getPFA(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT id_programa,descripcion FROM Programa WHERE estatus='1'");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_programa=$row['id_programa'];
              $item->descripcion=$row['descripcion'];
        
            
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }


              public function getDocente(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT docente.id_docente,identificacion,primer_nombre,primer_apellido FROM docente,persona,persona_nacionalidad WHERE docente.id_persona=persona.id_persona AND persona.id_persona=persona_nacionalidad.id_persona ORDER BY persona.id_persona DESC");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_docente=$row['id_docente'];
              $item->identificacion=$row['identificacion'];
              $item->primer_nombre=$row['primer_nombre'];
              $item->primer_apellido=$row['primer_apellido'];
            
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }





            public function getComiteTipo(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT id_comite_tipo,descripcion FROM comite_tipo WHERE estatus ='1'");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_comite_tipo=$row['id_comite_tipo'];
              $item->descripcion=$row['descripcion'];

              array_push($items,$item);
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }


//comite detalle
///////////////////////////////////////////////////////////////////////////////////
            public function getbyId($nombre_comite){
                $items=[];

                $query = $this->db->connect()->prepare("SELECT comitepfa_integrante.id_comitepfa_integrante,fecha_inicio,fecha_fin,comitepfa_integrante.estatus,tipo_docente,primer_nombre,primer_apellido,identificacion,comitepfa_integrante.descripcion AS nombre_comite,programa.descripcion AS docente_programa FROM comitepfa_integrante,docente,persona,persona_nacionalidad,programa  WHERE comitepfa_integrante.id_docente=docente.id_docente AND docente.id_persona=persona.id_persona AND persona_nacionalidad.id_persona=persona.id_persona AND programa.id_programa=docente.id_programa AND comitepfa_integrante.descripcion=:descripcion");
                
                try{
                    $query ->execute(['descripcion'=>$nombre_comite]);

                    while($row=$query->fetch()){
                        $item=new Estructura();
                        $item->id_comitepfa_integrante=$row['id_comitepfa_integrante'];
                        $item->fecha_inicio=$row['fecha_inicio'];
                        $item->fecha_fin=$row['fecha_fin'];
                        $item->estatus=$row['estatus'];
                        $item->tipo_docente=$row['tipo_docente'];
                        $item->primer_nombre=$row['primer_nombre'];
                        $item->primer_apellido=$row['primer_apellido'];
                        $item->identificacion=$row['identificacion'];
                        $item->nombre_comite=$row['nombre_comite'];
                        $item->docente_programa=$row['docente_programa'];
                        
          
                        array_push($items,$item);
                        
                        }


                        return $items;
                }catch(PDOExcepton $e){
                    return null;
                }
            }
            

            public function getComites($nombre_comite){
                $item=new Estructura();

                $query = $this->db->connect()->prepare("SELECT DISTINCT comite_tipo.id_comite_tipo,programa.id_programa,programa.descripcion AS programa,comite_tipo.descripcion AS comite_tipo,comitepfa_integrante.descripcion AS nombre_comite FROM comitepfa_integrante,comite_tipo,programa WHERE comitepfa_integrante.id_comite_tipo=comite_tipo.id_comite_tipo AND comitepfa_integrante.id_programa=programa.id_programa AND comitepfa_integrante.descripcion=:descripcion");
             //   var_dump($nombre_comite);
                try{
                    $query ->execute(['descripcion'=>$nombre_comite]);

                    while($row=$query->fetch()){
                     
                        $item->id_programa=$row['id_programa'];
                        $item->id_comite_tipo=$row['id_comite_tipo'];            
                        $item->programa=$row['programa'];          
                        $item->comite_tipo=$row['comite_tipo'];
                        $item->nombre_comite=$row['nombre_comite'];
                        
                        }

                    //var_dump($item);
                        return $item;
                }catch(PDOExcepton $e){
                    return null;
                }
            }


            public function getbyIdHistoricos($nombre_comite,$fecha_registro){
                $items=[];

                $query = $this->db->connect()->prepare("SELECT * FROM history_comitepfa_integrante WHERE comite_nombre=:comite_nombre AND fecha_registro=:fecha_registro");
                
                try{
                    $query ->execute(['comite_nombre'=>$nombre_comite,'fecha_registro'=>$fecha_registro]);

                    while($row=$query->fetch()){
                        $item=new Estructura();


                        $item->id_comitepfa_integrante=$row['id_comitepfa_integrante'];
                        $item->fecha_inicio=$row['fecha_inicio'];
                        $item->fecha_fin=$row['fecha_fin'];
                        $item->estatus_docente=$row['estatus_docente'];
                        $item->tipo_docente=$row['tipo_docente'];
                        $item->primer_nombre=$row['primer_nombre'];
                        $item->primer_apellido=$row['primer_apellido'];
                        $item->identificacion=$row['identificacion'];
                        $item->comite_nombre=$row['nombre_comite'];
                        $item->docente_programa=$row['docente_programa'];
                        $item->fecha_registro=$row['fecha_registro'];
                        $item->comite_programa=$row['comite_programa'];
                        $item->comite_tipo=$row['comite_tipo'];
                        
          
                        array_push($items,$item);
                        
                        }


                        return $items;
                }catch(PDOExcepton $e){
                    return null;
                }
            }


       


            public function getComitesHistory($nombre_comite,$fecha_registro){
                $item=new Estructura();

                $query = $this->db->connect()->prepare("SELECT DISTINCT comite_nombre,fecha_registro,comite_programa,comite_tipo FROM history_comitepfa_integrante WHERE comite_nombre=:comite_nombre");
             //   var_dump($nombre_comite);
                try{
                    $query ->execute(['comite_nombre'=>$nombre_comite]);

                    while($row=$query->fetch()){
                     
                         $item->comite_nombre=$row['comite_nombre'];
                         $item->fecha_registro=$row['fecha_registro'];
                        $item->comite_programa=$row['comite_programa'];
                        $item->comite_tipo=$row['comite_tipo'];

                        }

                    //var_dump($item);
                        return $item;
                }catch(PDOExcepton $e){
                    return null;
                }
            }

        
            
///////////////////////////////////////////////////////////////////////////////////

        //actualizar estatus de docente perteneciente a un pfa  
            public function updateÉstatus($id_comite){
                //  var_dump($item['parroquia'],$item['codigoine'],$item['id_municipio'],$item['id_eje_municipal'],$item['id_parroquia']);
                  //var_dump($id_comite);
               
                $query2=$this->db->connect()->prepare("SELECT estatus FROM comitepfa_integrante WHERE id_comitepfa_integrante=:id_comitepfa_integrante");
                $query2->execute(['id_comitepfa_integrante'=>$id_comite,]);
                $comite=$query2->fetch();
                $comite['estatus'];

               // var_dump( $comite['estatus']);
            

                    try{

                        // se valida el estatus segun las fecha de actualizacion contra el sistema
                        if( $comite['estatus']==1){
                 
                            $estatus=0;
                            $fecha_fin=(date("Y-m-d"));
        
                         //   var_dump($estatus,$fecha_fin,$id_comite);
                            $query=$this->db->connect()->prepare("UPDATE comitepfa_integrante SET estatus= :estatus,fecha_fin=:fecha_fin WHERE id_comitepfa_integrante= :id_comitepfa_integrante");
                            $query->execute(['estatus'=> $estatus,'fecha_fin'=>$fecha_fin,'id_comitepfa_integrante'=>$id_comite]);
        
                            
        
                        }else if($comite['estatus']==0){
                           // echo"auiqqqqqq2";
                            $estatus=1;
                            $fecha_inicio=(date("Y-m-d"));
                            //pasar fecha vacia aqui
                            //$fecha_fin="";
                            $query=$this->db->connect()->prepare("UPDATE comitepfa_integrante SET estatus= :estatus,fecha_inicio=:fecha_inicio,fecha_fin=:fecha_fin WHERE id_comitepfa_integrante= :id_comitepfa_integrante");
                            $query->execute(['estatus'=> $estatus,'fecha_inicio'=>$fecha_inicio,'fecha_fin'=>$fecha_fin,'id_comitepfa_integrante'=>$id_comite]);
        
        
                            
        
                        }

                   return true;
                              
                               
                   }catch(PDOException $e){
                       return false;
                        }
                   
                       }


///////////////////////////////////////////////////////////////////////////////////

          //actualizar comite academico PFA

              public function update($item){
         // var_dump($item['programa'],$item['comite_tipo'],$item['comite_academico'],$item['id_para_comite_academico'],$item['id_parroquia']);
        //var_dump($item['id_para_comite_academico']);
              $query=$this->db->connect()->prepare("UPDATE comitepfa_integrante SET id_programa= :id_programa,id_comite_tipo= :id_comite_tipo,descripcion=:descripcion  WHERE descripcion= :id_para_comite_academico");
            try{
                        $query->execute([                        
                        'id_programa'=>$item['programa'],     
                        'id_comite_tipo'=>$item['comite_tipo'],  
                        'descripcion'=>$item['comite_academico'],                 
                        'id_para_comite_academico'=>$item['id_para_comite_academico'],
                        ]);
            return true;
                       
                        
            }catch(PDOException $e){
                return false;
                 }
            
                }





    }

    ?>