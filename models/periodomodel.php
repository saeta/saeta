<?php
include_once 'models/estructura.php';
class PeriodoModel extends Model{
    public function __construct(){
    parent::__construct();
    }
    public function existe($periodo){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM periodo_academico WHERE descripcion=:descripcion");
            $sql->execute(['descripcion' =>$periodo]);
            $nombre=$sql->fetch();
            
            if($periodo==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }
    public function insert($datos){
    //echo "<br>insertar datos";
    

    try{
     
 

   $fecha_actual = strtotime(date("Y"));
// se separa la fecha para obtener solo el año en curso y realizar la conparacion
  $fecha = $datos['calendario'];
list($ano, $mes, $dia) = split('[/.-]', $fecha);
////////////////////////////////////////////////////////////////////////////////

   $fecha_entrada = strtotime($ano);
       
   if($fecha_actual >= $fecha_entrada)
       {
       $estatus=1;
       }else
           {
      $estatus=0;
     }
          
             $query=$this->db->connect()->prepare('INSERT INTO periodo_academico(descripcion, estatus, calendario_academico) VALUES (:descripcion, :estatus, :calendario_academico)');
  
        
             $query->execute(['descripcion'=>$datos['periodo'],'estatus'=>$estatus,'calendario_academico'=>$fecha]);
               
                        return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }
    
    
            }
            
          //GET Periodo Academico
            public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT * FROM periodo_academico");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_periodo_academico=$row['id_periodo_academico'];
              $item->descripcion=$row['descripcion'];
              $item->estatus=$row['estatus'];
              $item->calendario_academico=$row['calendario_academico'];
           
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }
              public function getAldea(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT * FROM aldea");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_aldea=$row['id_aldea'];
              $item->descripcion=$row['descripcion'];
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }




 


              public function update($item){
                  
             
               // var_dump($item['calendario']);    
          $query=$this->db->connect()->prepare("UPDATE periodo_academico SET id_periodo_academico= :id_periodo_academico,descripcion= :descripcion,estatus=:estatus,calendario_academico=:calendario_academico WHERE id_periodo_academico= :id_periodo_academico");
            try{

              
                        $query->execute([
                        'descripcion'=>$item['periodo'],
                        'estatus'=>$item['estatus'],                        
                        'calendario_academico'=>$item['calendario'],
                        'id_periodo_academico'=>$item['id_periodo_academico'], 
                        
                        ]);
                   

            return true;
                       
                        
            }catch(PDOException $e){
                return false;
                 }
            
                }


    }

    ?>