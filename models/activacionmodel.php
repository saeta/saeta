<?php
include_once 'models/estructura.php';
class ActivacionModel extends Model{
    public function __construct(){
    parent::__construct();
    }
    public function existe($motivo){
        try{
    
            //var_dump($cedula);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM modulo_activacion WHERE descripcion=:descripcion");
            $sql->execute(['descripcion' =>$motivo]);
            $nombre=$sql->fetch();
            
            if($motivo==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }


    public function existe_ascenso($datos){
        try{

            $sql = $this->db->connect()->prepare("SELECT 
            periodo_lectivo,
            descripcion, 
            fecha_inicio, 
            fecha_fin, 
            id_tipo_activacion 
            FROM activacion 
            WHERE periodo_lectivo=:periodo_lectivo 
            
            AND id_tipo_activacion=:id_tipo_activacion
            AND fecha_inicio<=:fecha_inicio
            AND fecha_fin>=:fecha_fin;");
            $sql->execute([
                'periodo_lectivo'=>$datos['periodo_lectivo'],
                'fecha_inicio'=>date('Y-m-d'),
                'fecha_fin'=>date('Y-m-d'),
                'id_tipo_activacion'=>$datos['id_tipo_activacion']
                ]);
            $nombre=$sql->fetch(PDO::FETCH_ASSOC);
            if($datos['periodo_lectivo']==$nombre['periodo_lectivo'] 
                        || $datos['id_tipo_activacion']==$nombre['id_tipo_activacion']){
                
                return true;
    
            } 
        } catch(PDOException $e){
            return false;
        }
    }

    public function insert($datos){
    
        //var_dump($datos);
        try{
     
        $ofertas=$datos['ofertas'];


             $query=$this->db->connect()->prepare('INSERT INTO modulo_activacion(fecha_inicio, fecha_fin, id_oferta_academica, tipo_activacion,id_aldea,descripcion) VALUES
             (:fecha_inicio, :fecha_fin, :id_oferta_academica,:tipo_activacion,:id_aldea,:descripcion)');
  
  
              //Recorrer el arreglo de ofertas academicas
                for ($i=0;$i<count($ofertas);$i++)    
                {       
                        $query->execute(['fecha_inicio'=>$datos['fecha_inicio'],'fecha_fin'=>$datos['fecha_fin'],'id_oferta_academica'=>$ofertas[$i],
                        'tipo_activacion'=>$datos['modulo'], 'id_aldea'=>$datos['aldea'], 'descripcion'=>$datos['motivo']]);
                } 
                        return true;
      
    }catch(PDOException $e){
        //echo "Matricula duplicada";
        return false;
             }
    
    
            }
            
          //GET Modulo de activacion
            public function get(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT modulo_activacion.id_modulo_activacion, fecha_inicio,fecha_fin,oferta_academica.id_oferta_academica,oferta_academica.descripcion AS oferta_academica,tipo_activacion,modulo_activacion.descripcion AS modulo_activacion,aldea.id_aldea,aldea.descripcion AS aldea,aldea.estatus AS estatus_aldea, aldea.codigo_sucre,aldea.codigo_aldea,periodo_academico.id_periodo_academico,periodo_academico.descripcion AS periodo_academico,periodo_academico.estatus AS periodo_academico_estatus,calendario_academico,estatus_carga_notas,aldea_malla_curricula.id_aldea_malla_curricula,aldea_malla_curricula.estatus AS aldea_malla_curricula_estatus,turno.id_turno,turno.descripcion AS turno,aldea_tipo.id_aldea_tipo,aldea_tipo.descripcion AS aldea_tipo,ambiente.id_ambiente, ambiente.descripcion AS ambiente,ambiente.codigo AS codigo_ambiente,ambiente.direccion AS ambiente_direccion, coordenada_latitud,coordenada_longitd,ambiente.estatus AS ambiente_estatus,parroquia.id_parroquia,parroquia.descripcion AS parroquia  FROM modulo_activacion,oferta_academica,aldea,periodo_academico,aldea_malla_curricula,turno,aldea_tipo,ambiente,parroquia WHERE modulo_activacion.id_oferta_academica=oferta_academica.id_oferta_academica AND modulo_activacion.id_aldea=aldea.id_aldea AND oferta_academica.id_aldea_malla_curricula=aldea_malla_curricula.id_aldea_malla_curricula AND aldea_malla_curricula.id_turno = turno.id_turno  AND oferta_academica.id_periodo_academico=periodo_academico.id_periodo_academico AND aldea.id_aldea_tipo=aldea_tipo.id_aldea_tipo AND aldea.id_ambiente =ambiente.id_ambiente AND ambiente.id_parroquia=parroquia.id_parroquia");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_modulo_activacion=$row['id_modulo_activacion'];
              $item->fecha_inicio=$row['fecha_inicio'];
              $item->fecha_fin=$row['fecha_fin'];
              $item->tipo_activacion=$row['tipo_activacion'];

              $item->id_oferta_academica=$row['id_oferta_academica'];
              $item->oferta_academica=$row['oferta_academica'];
              
              $item->modulo_activacion=$row['modulo_activacion']; 
              $item->id_aldea=$row['id_aldea'];             
              $item->aldea=$row['aldea'];
              $item->estatus_aldea=$row['estatus_aldea'];
              $item->codigo_sucre=$row['codigo_sucre'];
              $item->codigo_aldea=$row['codigo_aldea'];
              $item->periodo_academico=$row['periodo_academico'];

              
              $item->periodo_academico_estatus=$row['periodo_academico_estatus'];
              $item->calendario_academico=$row['calendario_academico'];
              $item->estatus_carga_notas=$row['estatus_carga_notas'];


              $item->aldea_malla_curricula_estatus=$row['aldea_malla_curricula_estatus'];
              $item->turno=$row['turno'];
              $item->aldea_tipo=$row['aldea_tipo'];

              $item->ambiente=$row['ambiente'];
              $item->codigo_ambiente=$row['codigo_ambiente'];
              $item->ambiente_direccion=$row['ambiente_direccion'];

              $item->coordenada_latitud=$row['coordenada_latitud'];
              $item->coordenada_longitd=$row['coordenada_longitd'];

              $item->ambiente_estatus=$row['ambiente_estatus'];
              $item->parroquia=$row['parroquia'];           
                            
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }
              
            public function getAldea(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT * FROM aldea");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_aldea=$row['id_aldea'];
              $item->descripcion=$row['descripcion'];
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
            }



              public function getOfertaAcademica(){
                $items=[];
               try{
              $query=$this->db->connect()->query("SELECT * FROM oferta_academica");
              
              while($row=$query->fetch()){
              $item=new Estructura();
              $item->id_oferta_academica=$row['id_oferta_academica'];
              $item->descripcion=$row['descripcion'];
              
              array_push($items,$item);
              
              
              }
              return $items;
              
              }catch(PDOException $e){
              return[];
              }
              
              }

 


              public function update($item){
                  
                
               // var_dump($item['ofertas']);    
          $query=$this->db->connect()->prepare("UPDATE modulo_activacion SET fecha_inicio= :fecha_inicio,fecha_fin= :fecha_fin,id_oferta_academica=:id_oferta_academica,tipo_activacion=:tipo_activacion,id_aldea=:id_aldea,descripcion=:descripcion WHERE id_modulo_activacion= :id_modulo_activacion");
            try{

              
                        $query->execute([
                        'fecha_inicio'=>$item['fecha_inicio'],
                        'fecha_fin'=>$item['fecha_fin'],                        
                        'id_oferta_academica'=>$item['ofertas'],
                        'tipo_activacion'=>$item['modulo'],
                        'id_aldea'=>$item['aldea'],                        
                        'descripcion'=>$item['motivo'], 
                        'id_modulo_activacion'=>$item['id_modulo_activacion'], 
                        
                        ]);
                   

            return true;
                       
                        
            }catch(PDOException $e){
                return false;
                 }
            
                }


                //obtener tipos de activacion de SAETA
                public function getTipoActivacion(){
                    
                    $items=[];
                    try{
                        $query=$this->db->connect()->query("SELECT * FROM tipo_activacion");
                  
                    while($row=$query->fetch()){

                        $item=new Estructura();
                        $item->id_tipo_activacion=$row['id_tipo_activacion'];
                        $item->descripcion=$row['descripcion'];
                    
                        array_push($items,$item);

                    }
                    return $items;
                  
                }catch(PDOException $e){
                    return [];
                }
                  
            }



            public function insert_ascenso($datos){
    
                //var_dump($datos);
                try{
             
                    $query=$this->db->connect()->prepare("INSERT INTO activacion(
                        periodo_lectivo, 
                        fecha_inicio,
                        fecha_fin,
                        id_tipo_activacion,
                        descripcion) 
                        VALUES(
                        :periodo,
                        :fecha_inicio,
                        :fecha_fin,
                        :id_tipo_activacion,
                        :descripcion
                        );");
                    $query->execute([
                        'periodo'=>$datos['periodo'],
                        'fecha_inicio'=>$datos['fecha_inicio'],
                        'fecha_fin'=>$datos['fecha_fin'],
                        'id_tipo_activacion'=>$datos['tipo_activacion'],
                        'descripcion'=>$datos['descripcion']
                        ]);

                        
                    return true;
              
                }catch(PDOException $e){
                    //echo "Matricula duplicada";
                    return false;
                }
            
            
            }
                //obtener tipos de activacion de SAETA
                public function getAscensos(){
                    
                    $items=[];
                    try{
                        $query=$this->db->connect()->query("SELECT 
                        id_activacion,
                        periodo_lectivo, 
                        fecha_inicio,
                        fecha_fin,
                        activacion.id_tipo_activacion,
                        activacion.descripcion as motivo_activacion,
                        tipo_activacion.descripcion as tipo_activacion

                        FROM activacion, tipo_activacion WHERE activacion.id_tipo_activacion=tipo_activacion.id_tipo_activacion;");
                  
                    while($row=$query->fetch()){

                        $item=new Estructura();
                        $item->id_activacion=$row['id_activacion'];
                        $item->periodo_lectivo=$row['periodo_lectivo'];
                        $item->fecha_inicio=$row['fecha_inicio'];
                        $item->fecha_fin=$row['fecha_fin'];
                        $item->id_tipo_activacion=$row['id_tipo_activacion'];
                        $item->motivo_activacion=$row['motivo_activacion'];
                        $item->tipo_activacion=$row['tipo_activacion'];

                        array_push($items,$item);

                    }
                    return $items;
                  
                }catch(PDOException $e){
                    return [];
                }
                  
            }



                //update ascensos SAETA
                public function update_ascenso($datos){
                    
                        try{

                            $sql=$this->db->connect()->prepare("UPDATE activacion SET 
                            periodo_lectivo=:periodo_lectivo,
                            fecha_inicio=:fecha_inicio,
                            fecha_fin=:fecha_fin,
                            id_tipo_activacion=:id_tipo_activacion,
                            descripcion=:descripcion WHERE id_activacion=:id_activacion;");
                            $sql->execute([
                                'id_activacion'=>$datos['id_activacion'],
                                'periodo_lectivo'=>$datos['periodo_lectivo'],
                                'fecha_inicio'=>$datos['fecha_inicio'],
                                'fecha_fin'=>$datos['fecha_fin'],
                                'id_tipo_activacion'=>$datos['id_tipo_activacion'],
                                'descripcion'=>$datos['descripcion']
                                ]);
                            return true;
                  
                          }catch(PDOException $e){
                               return false;
                          }
                           
            }

            //consultar ascensos
        public function getModAscenso(){
            //var_dump($fecha_actual);
                $items=[];
                try{
                    $query=$this->db->connect()->query("SELECT 
                    id_activacion,
                    periodo_lectivo, 
                    fecha_inicio,
                    fecha_fin,
                    activacion.id_tipo_activacion,
                    activacion.descripcion as motivo_activacion,
                    tipo_activacion.descripcion as tipo_activacion

                    FROM 
                    activacion, 
                    tipo_activacion 
                    WHERE 
                    activacion.id_tipo_activacion=tipo_activacion.id_tipo_activacion
                    and activacion.id_tipo_activacion=1 ORDER BY id_activacion DESC;");
                                
                    $row=$query->fetch(PDO::FETCH_ASSOC);
                    return $row;
    
                   
    
                } catch(PDOException $e){
                    return false;
                }
    
            }


            public function getbyID($id){
                try{

                    $item = new Estructura();
                    $sql = $this->db->connect()->prepare("SELECT 
                    id_activacion,
                    periodo_lectivo,
                    fecha_inicio,
                    fecha_fin,
                    t1.descripcion as activacion,
                    t1.id_tipo_activacion,
                    t2.descripcion as tipo_activacion

                     from 
                     activacion as t1, 
                     tipo_activacion as t2
                     where 
                     t1.id_tipo_activacion=t2.id_tipo_activacion
                     and id_activacion=:id_activacion;");
                    $sql->execute(['id_activacion'=>$id]);
                    while($row=$sql->fetch()){
                        $item->periodo_lectivo=$row['periodo_lectivo'];
                        $item->fecha_inicio=date("m-d-Y", strtotime($row['fecha_inicio']));
                        $item->fecha_fin=date("m-d-Y", strtotime($row['fecha_fin']));
                        $item->id_tipo_activacion=$row['id_tipo_activacion'];
                        $item->tipo_activacion=$row['tipo_activacion'];

                        $item->activacion=$row['activacion'];


                    }
                    return $item;

                }catch(PDOExcepton $e){
                    return null;
                }
            }

    }

    ?>