<?php
include_once 'datosacademicos/convenio.php';

class ConvenioModel extends Model{
    public function __construct(){
        parent::__construct();
    }


    public function existe($convenio){
        try{
    
            //var_dump($convenio);
            $sql = $this->db->connect()->prepare("SELECT descripcion FROM convenio WHERE descripcion=:descripcion");
            $sql->execute(['descripcion'=>$convenio]);
            $nombre=$sql->fetch();
            
            if($convenio==$nombre['descripcion']){
                
                return $nombre['descripcion'];
    
            } 
            return false;
        } catch(PDOException $e){
            return false;
        }
    }

    public function insert($datos){
        //echo "<br>insertar datos";
    
        try{
         //var_dump($datos);
            $query=$this->db->connect()->prepare('INSERT INTO convenio(
                descripcion,
                institucion_convenio,
                fecha_convenio) VALUES(
                    :descripcion,
                    :institucion_convenio,
                    :fecha_convenio
                    )');
    
                $query->execute([
                    'descripcion'=>$datos['descripcion'],
                    'institucion_convenio'=>$datos['institucion_convenio'],
                    'fecha_convenio'=>$datos['fecha_convenio']
                ]);
    
                return true;
          
        }catch(PDOException $e){
            //echo "Matricula duplicada";
            return false;
                 }
        
        
    }

    

    public function get(){
        $items=[];
        try{
            $query=$this->db->connect()->query("SELECT * FROM convenio");
            
            while($row=$query->fetch()){
                
                $item=new ConvenioI();
                $item->id_convenio=$row['id_convenio'];
                $item->descripcion=$row['descripcion'];
                $item->institucion_convenio=$row['institucion_convenio'];
                $item->fecha_convenio=$row['fecha_convenio'];

                array_push($items,$item);
            
            
            }
            return $items;
      
        }catch(PDOException $e){
        return[];
        }
      
    }
      
    public function update($datos){
        //var_dump($datos);
        //var_dump($item['id_pais'],$item['estado'],$item['codigoine'],$item['id_estado']);
        $query=$this->db->connect()->prepare("UPDATE convenio SET 
        descripcion=:descripcion,
        institucion_convenio=:institucion_convenio,
        fecha_convenio=:fecha_convenio
        WHERE id_convenio=:id_convenio");
        
        try{
            $query->execute([
                'id_convenio'=>$datos['id_convenio'],
                'descripcion'=>$datos['descripcion'],
                'institucion_convenio'=>$datos['institucion_convenio'],
                'fecha_convenio'=>$datos['fecha_convenio'],
                 ]);
         return true;

        }catch(PDOException $e){
             return false;
        }
         
    }

        public function delete($id){
            $query=$this->db->connect()->prepare("DELETE FROM convenio WHERE id_convenio = :id_convenio");
    
            try{
                $query->execute([
                'id_convenio'=>$id
                ]);
                return true;
            }catch(PDOException $e){
                return false;
            }
        }
    }


    ?>