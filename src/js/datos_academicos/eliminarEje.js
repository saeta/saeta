

const botones=document.querySelectorAll(".bEliminar");

    botones.forEach(boton=>{
    boton.addEventListener("click",function(){
        const id=this.dataset.id;
        var local=window.location.origin;
        //alert(local);
        const confirm=window.confirm("¿Desea Eliminar el Eje de Formación "+id+"?");
        if(confirm){
            //Solicitud AJAX
            httpRequest(local+"/modulorojo/eje_formacion/removerEje/"+id, function(){
            console.log(this.responseText);
            
            document.querySelector("#respuesta").innerHTML = this.responseText;
            const tbody = document.querySelector("#tbody-ejes");
            const fila =  document.querySelector("#fila-"+id);
            //alert(fila);

            tbody.removeChild(fila);


        })
        }
    });
});

function httpRequest(url,callback){
    var http=new XMLHttpRequest();
    http.open("GET", url);
    http.send();

    http.onreadystatechange=function(){
        if(this.readyState == 4 && 200){
            callback.apply(http);
        }
    }
}

