--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.24
-- Dumped by pg_dump version 9.5.24

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: h_aldeaprograma(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.h_aldeaprograma() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin

 

insert into history_aldea_programa SELECT          
old.id_aldea_programa,
old.codigo_opsu,
aldea_programa.id_aldea,
aldea.descripcion AS descripcion_aldea,
aldea_programa.id_programa as id_programa,
programa.descripcion AS descripcion_programa,
aldea_programa.estatus,
current_timestamp
FROM aldea_programa, aldea, programa 
WHERE aldea_programa.id_aldea=aldea.id_aldea;

return new;

End
$$;


ALTER FUNCTION public.h_aldeaprograma() OWNER TO postgres;

--
-- Name: h_comitepfa_integrante(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.h_comitepfa_integrante() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin 





insert into history_comitepfa_integrante SELECT comitepfa_integrante.id_comitepfa_integrante,fecha_inicio,
fecha_fin,comitepfa_integrante.estatus,tipo_docente,primer_nombre,primer_apellido,identificacion,
comitepfa_integrante.descripcion,programa.descripcion,current_timestamp,(SELECT DISTINCT programa.descripcion from comitepfa_integrante,programa where comitepfa_integrante.id_programa=programa.id_programa AND comitepfa_integrante.id_comitepfa_integrante=old.id_comitepfa_integrante),(SELECT DISTINCT comite_tipo.descripcion from comitepfa_integrante,comite_tipo where comitepfa_integrante.id_comite_tipo=comite_tipo.id_comite_tipo AND comitepfa_integrante.id_comitepfa_integrante=old.id_comitepfa_integrante) FROM comitepfa_integrante,docente,persona,persona_nacionalidad,programa  WHERE 
comitepfa_integrante.id_docente=docente.id_docente AND docente.id_persona=persona.id_persona AND persona_nacionalidad.id_persona=persona.id_persona AND 
programa.id_programa=docente.id_programa AND comitepfa_integrante.id_comitepfa_integrante=old.id_comitepfa_integrante;




return new;

End
$$;


ALTER FUNCTION public.h_comitepfa_integrante() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: activacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.activacion (
    id_activacion integer NOT NULL,
    periodo_lectivo character varying(45) NOT NULL,
    fecha_inicio date NOT NULL,
    fecha_fin date NOT NULL,
    id_tipo_activacion integer NOT NULL,
    descripcion character varying(80) NOT NULL
);


ALTER TABLE public.activacion OWNER TO postgres;

--
-- Name: activacion_id_activacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.activacion_id_activacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.activacion_id_activacion_seq OWNER TO postgres;

--
-- Name: activacion_id_activacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.activacion_id_activacion_seq OWNED BY public.activacion.id_activacion;


--
-- Name: aldea; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.aldea (
    id_aldea integer NOT NULL,
    descripcion character varying(70) NOT NULL,
    codigo_sucre character varying(15) NOT NULL,
    codigo_aldea character varying(15) NOT NULL,
    estatus character varying(2) NOT NULL,
    id_aldea_tipo integer NOT NULL,
    id_ambiente integer NOT NULL
);


ALTER TABLE public.aldea OWNER TO postgres;

--
-- Name: aldea_id_aldea_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.aldea_id_aldea_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aldea_id_aldea_seq OWNER TO postgres;

--
-- Name: aldea_id_aldea_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.aldea_id_aldea_seq OWNED BY public.aldea.id_aldea;


--
-- Name: aldea_malla_curricula; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.aldea_malla_curricula (
    id_aldea_malla_curricula integer NOT NULL,
    estatus character(2) NOT NULL,
    id_aldea integer NOT NULL,
    id_malla_curricular integer NOT NULL,
    id_turno integer NOT NULL
);


ALTER TABLE public.aldea_malla_curricula OWNER TO postgres;

--
-- Name: aldea_malla_curricula_id_aldea_malla_curricula_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.aldea_malla_curricula_id_aldea_malla_curricula_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aldea_malla_curricula_id_aldea_malla_curricula_seq OWNER TO postgres;

--
-- Name: aldea_malla_curricula_id_aldea_malla_curricula_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.aldea_malla_curricula_id_aldea_malla_curricula_seq OWNED BY public.aldea_malla_curricula.id_aldea_malla_curricula;


--
-- Name: aldea_programa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.aldea_programa (
    id_aldea_programa integer NOT NULL,
    codigo_opsu integer,
    id_aldea integer NOT NULL,
    id_programa integer NOT NULL,
    estatus character varying(45)
);


ALTER TABLE public.aldea_programa OWNER TO postgres;

--
-- Name: aldea_programa_id_aldea_programa_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.aldea_programa_id_aldea_programa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aldea_programa_id_aldea_programa_seq OWNER TO postgres;

--
-- Name: aldea_programa_id_aldea_programa_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.aldea_programa_id_aldea_programa_seq OWNED BY public.aldea_programa.id_aldea_programa;


--
-- Name: aldea_telefono; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.aldea_telefono (
    id_aldea_telefono integer NOT NULL,
    descripcion character varying(45) NOT NULL,
    id_telefono_codigo_area integer NOT NULL,
    id_aldea integer NOT NULL
);


ALTER TABLE public.aldea_telefono OWNER TO postgres;

--
-- Name: aldea_telefono_id_aldea_telefono_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.aldea_telefono_id_aldea_telefono_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aldea_telefono_id_aldea_telefono_seq OWNER TO postgres;

--
-- Name: aldea_telefono_id_aldea_telefono_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.aldea_telefono_id_aldea_telefono_seq OWNED BY public.aldea_telefono.id_aldea_telefono;


--
-- Name: aldea_tipo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.aldea_tipo (
    id_aldea_tipo integer NOT NULL,
    descripcion character varying(100) NOT NULL
);


ALTER TABLE public.aldea_tipo OWNER TO postgres;

--
-- Name: aldea_tipo_id_aldea_tipo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.aldea_tipo_id_aldea_tipo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aldea_tipo_id_aldea_tipo_seq OWNER TO postgres;

--
-- Name: aldea_tipo_id_aldea_tipo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.aldea_tipo_id_aldea_tipo_seq OWNED BY public.aldea_tipo.id_aldea_tipo;


--
-- Name: aldea_ubv; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.aldea_ubv (
    id_aldea_ubv integer NOT NULL,
    descripcion character varying(200) NOT NULL
);


ALTER TABLE public.aldea_ubv OWNER TO postgres;

--
-- Name: aldea_ubv_id_aldea_ubv_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.aldea_ubv_id_aldea_ubv_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aldea_ubv_id_aldea_ubv_seq OWNER TO postgres;

--
-- Name: aldea_ubv_id_aldea_ubv_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.aldea_ubv_id_aldea_ubv_seq OWNED BY public.aldea_ubv.id_aldea_ubv;


--
-- Name: ambiente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ambiente (
    id_ambiente integer NOT NULL,
    descripcion character varying(70) NOT NULL,
    codigo character varying(6) NOT NULL,
    direccion character varying(255) NOT NULL,
    coordenada_latitud text,
    coordenada_longitd text,
    id_parroquia integer NOT NULL,
    estatus character varying DEFAULT false
);


ALTER TABLE public.ambiente OWNER TO postgres;

--
-- Name: ambiente_detalle; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ambiente_detalle (
    id_ambiente_detalle integer NOT NULL,
    descripcion character varying(70) NOT NULL,
    estatus character varying(4) NOT NULL,
    id_ambiente integer NOT NULL
);


ALTER TABLE public.ambiente_detalle OWNER TO postgres;

--
-- Name: ambiente_detalle_id_ambiente_detalle_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ambiente_detalle_id_ambiente_detalle_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ambiente_detalle_id_ambiente_detalle_seq OWNER TO postgres;

--
-- Name: ambiente_detalle_id_ambiente_detalle_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ambiente_detalle_id_ambiente_detalle_seq OWNED BY public.ambiente_detalle.id_ambiente_detalle;


--
-- Name: ambiente_espacio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ambiente_espacio (
    id_ambiente_espacio integer NOT NULL,
    descripcion character varying(70) NOT NULL,
    capacidad integer NOT NULL,
    id_espacio_tipo integer NOT NULL,
    id_ambiente_detalle integer NOT NULL,
    estatus character varying(45) DEFAULT false
);


ALTER TABLE public.ambiente_espacio OWNER TO postgres;

--
-- Name: ambiente_espacio_id_ambiente_espacio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ambiente_espacio_id_ambiente_espacio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ambiente_espacio_id_ambiente_espacio_seq OWNER TO postgres;

--
-- Name: ambiente_espacio_id_ambiente_espacio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ambiente_espacio_id_ambiente_espacio_seq OWNED BY public.ambiente_espacio.id_ambiente_espacio;


--
-- Name: ambiente_id_ambiente_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ambiente_id_ambiente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ambiente_id_ambiente_seq OWNER TO postgres;

--
-- Name: ambiente_id_ambiente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ambiente_id_ambiente_seq OWNED BY public.ambiente.id_ambiente;


--
-- Name: antropometrico; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.antropometrico (
    id_antropometrico integer NOT NULL,
    peso character varying(45) NOT NULL,
    talla_pantalon character varying(45) NOT NULL,
    talla_camisa character varying(45) NOT NULL,
    talla_calzado character varying(45) NOT NULL,
    estatura character varying(45) NOT NULL,
    id_persona integer NOT NULL
);


ALTER TABLE public.antropometrico OWNER TO postgres;

--
-- Name: antropometrico_id_antropometrico_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.antropometrico_id_antropometrico_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.antropometrico_id_antropometrico_seq OWNER TO postgres;

--
-- Name: antropometrico_id_antropometrico_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.antropometrico_id_antropometrico_seq OWNED BY public.antropometrico.id_antropometrico;


--
-- Name: arancel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.arancel (
    id_arancel integer NOT NULL,
    descripcion character varying(120) NOT NULL,
    estatus boolean DEFAULT true,
    modo character varying(45),
    codigo character varying(10),
    cantidad_ut character varying(45),
    fecha_inicio character varying(45),
    fecha_fin character varying(45)
);


ALTER TABLE public.arancel OWNER TO postgres;

--
-- Name: arancel_id_arancel_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.arancel_id_arancel_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.arancel_id_arancel_seq OWNER TO postgres;

--
-- Name: arancel_id_arancel_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.arancel_id_arancel_seq OWNED BY public.arancel.id_arancel;


--
-- Name: area_conocimiento_opsu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.area_conocimiento_opsu (
    id_area_conocimiento_opsu integer NOT NULL,
    descripcion character varying(45) NOT NULL,
    codigo character varying(5) NOT NULL
);


ALTER TABLE public.area_conocimiento_opsu OWNER TO postgres;

--
-- Name: area_conocimiento_opsu_id_area_conocimiento_opsu_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.area_conocimiento_opsu_id_area_conocimiento_opsu_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.area_conocimiento_opsu_id_area_conocimiento_opsu_seq OWNER TO postgres;

--
-- Name: area_conocimiento_opsu_id_area_conocimiento_opsu_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.area_conocimiento_opsu_id_area_conocimiento_opsu_seq OWNED BY public.area_conocimiento_opsu.id_area_conocimiento_opsu;


--
-- Name: area_conocimiento_ubv; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.area_conocimiento_ubv (
    id_area_conocimiento_ubv integer NOT NULL,
    descripcion character varying(45) NOT NULL,
    codigo character varying(5) NOT NULL,
    estatus boolean DEFAULT false,
    id_area_conocimiento_opsu integer NOT NULL
);


ALTER TABLE public.area_conocimiento_ubv OWNER TO postgres;

--
-- Name: area_conocimiento_ubv_id_area_conocimiento_ubv_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.area_conocimiento_ubv_id_area_conocimiento_ubv_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.area_conocimiento_ubv_id_area_conocimiento_ubv_seq OWNER TO postgres;

--
-- Name: area_conocimiento_ubv_id_area_conocimiento_ubv_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.area_conocimiento_ubv_id_area_conocimiento_ubv_seq OWNED BY public.area_conocimiento_ubv.id_area_conocimiento_ubv;


--
-- Name: arte_software; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.arte_software (
    id_arte_software integer NOT NULL,
    nombre_arte character varying(250) NOT NULL,
    ano_arte integer NOT NULL,
    entidad_promotora character varying(100) NOT NULL,
    url_otro character varying(300) NOT NULL,
    id_docente integer NOT NULL,
    estatus_verificacion character varying(45) NOT NULL
);


ALTER TABLE public.arte_software OWNER TO postgres;

--
-- Name: arte_software_id_arte_software_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.arte_software_id_arte_software_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.arte_software_id_arte_software_seq OWNER TO postgres;

--
-- Name: arte_software_id_arte_software_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.arte_software_id_arte_software_seq OWNED BY public.arte_software.id_arte_software;


--
-- Name: articulo_revista; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.articulo_revista (
    id_articulo_revista integer NOT NULL,
    nombre_revista character varying(80) NOT NULL,
    titulo_articulo character varying(150) NOT NULL,
    issn_revista double precision NOT NULL,
    ano_revista integer NOT NULL,
    nro_revista integer NOT NULL,
    nro_pag_inicial integer NOT NULL,
    url_revista character varying(45) NOT NULL,
    id_docente integer NOT NULL,
    estatus_verificacion character varying(45) NOT NULL
);


ALTER TABLE public.articulo_revista OWNER TO postgres;

--
-- Name: articulo_revista_id_articulo_revista_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.articulo_revista_id_articulo_revista_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.articulo_revista_id_articulo_revista_seq OWNER TO postgres;

--
-- Name: articulo_revista_id_articulo_revista_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.articulo_revista_id_articulo_revista_seq OWNED BY public.articulo_revista.id_articulo_revista;


--
-- Name: ascenso_titular; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ascenso_titular (
    id_ascenso_titular integer NOT NULL,
    id_solicitud_ascenso integer NOT NULL,
    id_estudio_conducente integer NOT NULL
);


ALTER TABLE public.ascenso_titular OWNER TO postgres;

--
-- Name: ascenso_titular_id_ascenso_titular_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ascenso_titular_id_ascenso_titular_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ascenso_titular_id_ascenso_titular_seq OWNER TO postgres;

--
-- Name: ascenso_titular_id_ascenso_titular_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ascenso_titular_id_ascenso_titular_seq OWNED BY public.ascenso_titular.id_ascenso_titular;


--
-- Name: aspirante; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.aspirante (
    id_aspirante integer NOT NULL,
    promedio_nota double precision,
    "año_grado" integer,
    periodo_asignado character varying(10),
    codigo_opsu character varying(15),
    id_persona integer NOT NULL,
    id_aspirante_tipo integer NOT NULL,
    id_grado_academico integer NOT NULL,
    id_aldea integer NOT NULL,
    id_programa integer NOT NULL,
    id_aspirante_estatus integer NOT NULL,
    institucion_tipo character varying(70),
    institucion character varying(70),
    trabajo character varying(45)
);


ALTER TABLE public.aspirante OWNER TO postgres;

--
-- Name: aspirante_estatus; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.aspirante_estatus (
    id_aspirante_estatus integer NOT NULL,
    desripcion character varying(45) NOT NULL
);


ALTER TABLE public.aspirante_estatus OWNER TO postgres;

--
-- Name: aspirante_estatus_id_aspirante_estatus_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.aspirante_estatus_id_aspirante_estatus_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aspirante_estatus_id_aspirante_estatus_seq OWNER TO postgres;

--
-- Name: aspirante_estatus_id_aspirante_estatus_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.aspirante_estatus_id_aspirante_estatus_seq OWNED BY public.aspirante_estatus.id_aspirante_estatus;


--
-- Name: aspirante_id_aspirante_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.aspirante_id_aspirante_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aspirante_id_aspirante_seq OWNER TO postgres;

--
-- Name: aspirante_id_aspirante_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.aspirante_id_aspirante_seq OWNED BY public.aspirante.id_aspirante;


--
-- Name: aspirante_tipo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.aspirante_tipo (
    id_aspirante_tipo integer NOT NULL,
    descripcion character varying(45) NOT NULL
);


ALTER TABLE public.aspirante_tipo OWNER TO postgres;

--
-- Name: aspirante_tipo_id_aspirante_tipo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.aspirante_tipo_id_aspirante_tipo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aspirante_tipo_id_aspirante_tipo_seq OWNER TO postgres;

--
-- Name: aspirante_tipo_id_aspirante_tipo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.aspirante_tipo_id_aspirante_tipo_seq OWNED BY public.aspirante_tipo.id_aspirante_tipo;


--
-- Name: banco; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.banco (
    id_banco integer NOT NULL,
    descripcion character varying(150) NOT NULL,
    codigo character varying(15)
);


ALTER TABLE public.banco OWNER TO postgres;

--
-- Name: banco_id_banco_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.banco_id_banco_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.banco_id_banco_seq OWNER TO postgres;

--
-- Name: banco_id_banco_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.banco_id_banco_seq OWNED BY public.banco.id_banco;


--
-- Name: cambiar_oferta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cambiar_oferta (
    id_cambiar_oferta integer NOT NULL,
    fecha date NOT NULL,
    codigo_solicitud character varying(45) NOT NULL,
    estatus character varying(45) NOT NULL,
    aldea_ character varying(45) NOT NULL,
    perograma character varying(45) NOT NULL,
    turno character varying(45) NOT NULL,
    secciones character varying(45) NOT NULL,
    uc character varying(45) NOT NULL,
    id_oferta_academica integer NOT NULL,
    id_persona integer NOT NULL
);


ALTER TABLE public.cambiar_oferta OWNER TO postgres;

--
-- Name: cambiar_oferta_id_cambiar_oferta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cambiar_oferta_id_cambiar_oferta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cambiar_oferta_id_cambiar_oferta_seq OWNER TO postgres;

--
-- Name: cambiar_oferta_id_cambiar_oferta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cambiar_oferta_id_cambiar_oferta_seq OWNED BY public.cambiar_oferta.id_cambiar_oferta;


--
-- Name: carga_academica; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.carga_academica (
    id_carga_academica integer NOT NULL,
    id_oferta_academica_detalle integer NOT NULL,
    id_ambiente_espacio integer NOT NULL,
    id_docente integer NOT NULL
);


ALTER TABLE public.carga_academica OWNER TO postgres;

--
-- Name: carga_academica_id_carga_academica_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.carga_academica_id_carga_academica_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.carga_academica_id_carga_academica_seq OWNER TO postgres;

--
-- Name: carga_academica_id_carga_academica_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.carga_academica_id_carga_academica_seq OWNED BY public.carga_academica.id_carga_academica;


--
-- Name: cargo_experiencia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cargo_experiencia (
    id_cargo_experiencia integer NOT NULL,
    descripcion character varying(200) NOT NULL
);


ALTER TABLE public.cargo_experiencia OWNER TO postgres;

--
-- Name: cargo_experiencia_id_cargo_experiencia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cargo_experiencia_id_cargo_experiencia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cargo_experiencia_id_cargo_experiencia_seq OWNER TO postgres;

--
-- Name: cargo_experiencia_id_cargo_experiencia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cargo_experiencia_id_cargo_experiencia_seq OWNED BY public.cargo_experiencia.id_cargo_experiencia;


--
-- Name: censo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.censo (
    id_censo integer NOT NULL,
    estatus character varying(2),
    descripcion text,
    id_persona integer
);


ALTER TABLE public.censo OWNER TO postgres;

--
-- Name: censo_id_censo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.censo_id_censo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.censo_id_censo_seq OWNER TO postgres;

--
-- Name: censo_id_censo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.censo_id_censo_seq OWNED BY public.censo.id_censo;


--
-- Name: centro_estudio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.centro_estudio (
    id_centro_estudio integer NOT NULL,
    descripcion character varying(70) NOT NULL,
    codigo character varying(20) NOT NULL,
    id_area_conocimiento_opsu integer NOT NULL,
    estatus boolean DEFAULT false
);


ALTER TABLE public.centro_estudio OWNER TO postgres;

--
-- Name: centro_estudio_id_centro_estudio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.centro_estudio_id_centro_estudio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.centro_estudio_id_centro_estudio_seq OWNER TO postgres;

--
-- Name: centro_estudio_id_centro_estudio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.centro_estudio_id_centro_estudio_seq OWNED BY public.centro_estudio.id_centro_estudio;


--
-- Name: certificado_titulo_estudio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.certificado_titulo_estudio (
    id_certificado_titulo_estudio integer NOT NULL,
    id_estudio integer NOT NULL,
    id_documento integer NOT NULL
);


ALTER TABLE public.certificado_titulo_estudio OWNER TO postgres;

--
-- Name: certificado_titulo_estudio_id_certificado_titulo_estudio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.certificado_titulo_estudio_id_certificado_titulo_estudio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.certificado_titulo_estudio_id_certificado_titulo_estudio_seq OWNER TO postgres;

--
-- Name: certificado_titulo_estudio_id_certificado_titulo_estudio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.certificado_titulo_estudio_id_certificado_titulo_estudio_seq OWNED BY public.certificado_titulo_estudio.id_certificado_titulo_estudio;


--
-- Name: clasificacion_docente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.clasificacion_docente (
    id_clasificacion_docente integer NOT NULL,
    descripcion character varying(45) NOT NULL
);


ALTER TABLE public.clasificacion_docente OWNER TO postgres;

--
-- Name: clasificacion_docente_id_clasificacion_docente_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.clasificacion_docente_id_clasificacion_docente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clasificacion_docente_id_clasificacion_docente_seq OWNER TO postgres;

--
-- Name: clasificacion_docente_id_clasificacion_docente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.clasificacion_docente_id_clasificacion_docente_seq OWNED BY public.clasificacion_docente.id_clasificacion_docente;


--
-- Name: comision_excedencia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.comision_excedencia (
    id_comision_excedencia integer NOT NULL,
    id_tipo_info integer NOT NULL,
    lugar text NOT NULL,
    desde date NOT NULL,
    hasta date NOT NULL,
    designacion_funcion text NOT NULL,
    causa text NOT NULL,
    aprobacion character varying(2) NOT NULL,
    id_docente integer NOT NULL,
    estatus_verificacion character varying(45) NOT NULL
);


ALTER TABLE public.comision_excedencia OWNER TO postgres;

--
-- Name: comision_excedencia_id_comision_excedencia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.comision_excedencia_id_comision_excedencia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comision_excedencia_id_comision_excedencia_seq OWNER TO postgres;

--
-- Name: comision_excedencia_id_comision_excedencia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.comision_excedencia_id_comision_excedencia_seq OWNED BY public.comision_excedencia.id_comision_excedencia;


--
-- Name: comision_gubernamental; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.comision_gubernamental (
    id_comision_gubernamental integer NOT NULL,
    lugar character varying(150) NOT NULL,
    id_experiencia_docente integer NOT NULL
);


ALTER TABLE public.comision_gubernamental OWNER TO postgres;

--
-- Name: comision_gubernamental_id_comision_gubernamental_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.comision_gubernamental_id_comision_gubernamental_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comision_gubernamental_id_comision_gubernamental_seq OWNER TO postgres;

--
-- Name: comision_gubernamental_id_comision_gubernamental_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.comision_gubernamental_id_comision_gubernamental_seq OWNED BY public.comision_gubernamental.id_comision_gubernamental;


--
-- Name: comite_tipo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.comite_tipo (
    id_comite_tipo integer NOT NULL,
    descripcion character varying(70) NOT NULL,
    estatus character varying(6)
);


ALTER TABLE public.comite_tipo OWNER TO postgres;

--
-- Name: comite_tipo_id_comite_tipo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.comite_tipo_id_comite_tipo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comite_tipo_id_comite_tipo_seq OWNER TO postgres;

--
-- Name: comite_tipo_id_comite_tipo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.comite_tipo_id_comite_tipo_seq OWNED BY public.comite_tipo.id_comite_tipo;


--
-- Name: comitepfa_integrante; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.comitepfa_integrante (
    id_comitepfa_integrante integer NOT NULL,
    fecha_inicio date NOT NULL,
    fecha_fin date,
    estatus character varying(6) DEFAULT false,
    id_programa integer NOT NULL,
    id_docente integer NOT NULL,
    id_comite_tipo integer NOT NULL,
    descripcion character varying(70)
);


ALTER TABLE public.comitepfa_integrante OWNER TO postgres;

--
-- Name: comitepfa_integrante_historico; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.comitepfa_integrante_historico (
    id_comitepfa_integrante_historico integer NOT NULL,
    fecha_inicio date,
    fecha_fin date,
    estatus character varying(6),
    id_programa integer,
    id_docente integer,
    id_comite_tipo integer,
    descripcion character varying(70)
);


ALTER TABLE public.comitepfa_integrante_historico OWNER TO postgres;

--
-- Name: comitepfa_integrante_historic_id_comitepfa_integrante_histo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.comitepfa_integrante_historic_id_comitepfa_integrante_histo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comitepfa_integrante_historic_id_comitepfa_integrante_histo_seq OWNER TO postgres;

--
-- Name: comitepfa_integrante_historic_id_comitepfa_integrante_histo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.comitepfa_integrante_historic_id_comitepfa_integrante_histo_seq OWNED BY public.comitepfa_integrante_historico.id_comitepfa_integrante_historico;


--
-- Name: comitepfa_integrante_id_comitepfa_integrante_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.comitepfa_integrante_id_comitepfa_integrante_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comitepfa_integrante_id_comitepfa_integrante_seq OWNER TO postgres;

--
-- Name: comitepfa_integrante_id_comitepfa_integrante_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.comitepfa_integrante_id_comitepfa_integrante_seq OWNED BY public.comitepfa_integrante.id_comitepfa_integrante;


--
-- Name: concurso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.concurso (
    id_concurso integer NOT NULL,
    fecha_concurso date NOT NULL,
    id_escalafon integer NOT NULL,
    id_docente integer NOT NULL,
    id_documento integer NOT NULL
);


ALTER TABLE public.concurso OWNER TO postgres;

--
-- Name: concurso_id_concurso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.concurso_id_concurso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.concurso_id_concurso_seq OWNER TO postgres;

--
-- Name: concurso_id_concurso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.concurso_id_concurso_seq OWNED BY public.concurso.id_concurso;


--
-- Name: constancia_administrativo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.constancia_administrativo (
    id_constancia_administrativo integer NOT NULL,
    id_trabajo_administrativo integer NOT NULL,
    id_documento integer NOT NULL
);


ALTER TABLE public.constancia_administrativo OWNER TO postgres;

--
-- Name: constancia_administrativo_id_constancia_administrativo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.constancia_administrativo_id_constancia_administrativo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.constancia_administrativo_id_constancia_administrativo_seq OWNER TO postgres;

--
-- Name: constancia_administrativo_id_constancia_administrativo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.constancia_administrativo_id_constancia_administrativo_seq OWNED BY public.constancia_administrativo.id_constancia_administrativo;


--
-- Name: correo_tipo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.correo_tipo (
    id_correo_tipo integer NOT NULL,
    descripcion character varying(20) NOT NULL
);


ALTER TABLE public.correo_tipo OWNER TO postgres;

--
-- Name: correo_tipo_id_correo_tipo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.correo_tipo_id_correo_tipo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.correo_tipo_id_correo_tipo_seq OWNER TO postgres;

--
-- Name: correo_tipo_id_correo_tipo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.correo_tipo_id_correo_tipo_seq OWNED BY public.correo_tipo.id_correo_tipo;


--
-- Name: cuenta_bancaria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cuenta_bancaria (
    id_cuenta_bancaria integer NOT NULL,
    descripcion character varying(45) NOT NULL,
    id_cuenta_bancaria_tipo integer NOT NULL,
    id_banco integer NOT NULL,
    estatus character varying(45)
);


ALTER TABLE public.cuenta_bancaria OWNER TO postgres;

--
-- Name: cuenta_bancaria_id_cuenta_bancaria_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cuenta_bancaria_id_cuenta_bancaria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cuenta_bancaria_id_cuenta_bancaria_seq OWNER TO postgres;

--
-- Name: cuenta_bancaria_id_cuenta_bancaria_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cuenta_bancaria_id_cuenta_bancaria_seq OWNED BY public.cuenta_bancaria.id_cuenta_bancaria;


--
-- Name: cuenta_bancaria_tipo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cuenta_bancaria_tipo (
    id_cuenta_bancaria_tipo integer NOT NULL,
    descripcion character varying(15) NOT NULL
);


ALTER TABLE public.cuenta_bancaria_tipo OWNER TO postgres;

--
-- Name: cuenta_bancaria_tipo_id_cuenta_bancaria_tipo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cuenta_bancaria_tipo_id_cuenta_bancaria_tipo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cuenta_bancaria_tipo_id_cuenta_bancaria_tipo_seq OWNER TO postgres;

--
-- Name: cuenta_bancaria_tipo_id_cuenta_bancaria_tipo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cuenta_bancaria_tipo_id_cuenta_bancaria_tipo_seq OWNED BY public.cuenta_bancaria_tipo.id_cuenta_bancaria_tipo;


--
-- Name: deposito; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.deposito (
    id_deposito integer NOT NULL,
    num_deposito integer NOT NULL,
    monto double precision NOT NULL,
    id_arancel integer NOT NULL,
    id_cuenta_bancaria integer NOT NULL,
    id_persona integer NOT NULL
);


ALTER TABLE public.deposito OWNER TO postgres;

--
-- Name: deposito_id_deposito_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.deposito_id_deposito_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.deposito_id_deposito_seq OWNER TO postgres;

--
-- Name: deposito_id_deposito_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.deposito_id_deposito_seq OWNED BY public.deposito.id_deposito;


--
-- Name: dia_semana; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.dia_semana (
    id_dia_semana integer NOT NULL,
    descripcion character varying(10) NOT NULL
);


ALTER TABLE public.dia_semana OWNER TO postgres;

--
-- Name: dia_semana_id_dia_semana_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.dia_semana_id_dia_semana_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dia_semana_id_dia_semana_seq OWNER TO postgres;

--
-- Name: dia_semana_id_dia_semana_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.dia_semana_id_dia_semana_seq OWNED BY public.dia_semana.id_dia_semana;


--
-- Name: discapacidad; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.discapacidad (
    id_discapacidad integer NOT NULL,
    descripcion character varying(60) NOT NULL,
    id_tipo_discapacidad integer NOT NULL
);


ALTER TABLE public.discapacidad OWNER TO postgres;

--
-- Name: discapacidad_id_discapacidad_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.discapacidad_id_discapacidad_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.discapacidad_id_discapacidad_seq OWNER TO postgres;

--
-- Name: discapacidad_id_discapacidad_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.discapacidad_id_discapacidad_seq OWNED BY public.discapacidad.id_discapacidad;


--
-- Name: diseno_uc_docente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.diseno_uc_docente (
    id_diseno_uc integer NOT NULL,
    nombre character varying(255) NOT NULL,
    ano_creacion integer NOT NULL,
    id_tramo integer NOT NULL,
    id_unidad_curricular_tipo integer NOT NULL,
    id_programa integer NOT NULL,
    id_area_conocimiento_ubv integer NOT NULL,
    id_documento integer NOT NULL,
    id_docente integer NOT NULL,
    estatus_verificacion character varying(45) NOT NULL
);


ALTER TABLE public.diseno_uc_docente OWNER TO postgres;

--
-- Name: diseno_uc_docente_id_diseno_uc_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.diseno_uc_docente_id_diseno_uc_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.diseno_uc_docente_id_diseno_uc_seq OWNER TO postgres;

--
-- Name: diseno_uc_docente_id_diseno_uc_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.diseno_uc_docente_id_diseno_uc_seq OWNED BY public.diseno_uc_docente.id_diseno_uc;


--
-- Name: docencia_previa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.docencia_previa (
    id_docencia_previa integer NOT NULL,
    materia character varying(80) NOT NULL,
    id_nivel_academico integer NOT NULL,
    id_modalidad_estudio integer NOT NULL,
    semestre character varying(45) NOT NULL,
    periodo_lectivo character varying(45) NOT NULL,
    naturaleza character varying(45) NOT NULL,
    horas_semanales integer NOT NULL,
    estudiantes_atendidos character varying(45) NOT NULL,
    id_experiencia_docente integer NOT NULL
);


ALTER TABLE public.docencia_previa OWNER TO postgres;

--
-- Name: docencia_previa_id_docencia_previa_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.docencia_previa_id_docencia_previa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.docencia_previa_id_docencia_previa_seq OWNER TO postgres;

--
-- Name: docencia_previa_id_docencia_previa_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.docencia_previa_id_docencia_previa_seq OWNED BY public.docencia_previa.id_docencia_previa;


--
-- Name: docencia_previa_ubv; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.docencia_previa_ubv (
    id_docencia_previa_ubv integer NOT NULL,
    id_area_conocimiento_ubv integer NOT NULL,
    id_programa integer NOT NULL,
    id_eje_regional integer NOT NULL,
    id_eje_municipal integer NOT NULL,
    id_aldea integer NOT NULL,
    id_centro_estudio integer NOT NULL,
    fecha_ingreso date NOT NULL,
    estatus_verificacion character varying(45) NOT NULL,
    id_docente integer NOT NULL
);


ALTER TABLE public.docencia_previa_ubv OWNER TO postgres;

--
-- Name: docencia_previa_ubv_id_docencia_previa_ubv_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.docencia_previa_ubv_id_docencia_previa_ubv_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.docencia_previa_ubv_id_docencia_previa_ubv_seq OWNER TO postgres;

--
-- Name: docencia_previa_ubv_id_docencia_previa_ubv_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.docencia_previa_ubv_id_docencia_previa_ubv_seq OWNED BY public.docencia_previa_ubv.id_docencia_previa_ubv;


--
-- Name: docente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.docente (
    id_docente integer NOT NULL,
    id_centro_estudio integer NOT NULL,
    id_persona integer NOT NULL,
    id_eje_municipal integer NOT NULL,
    id_docente_dedicacion integer NOT NULL,
    id_docente_estatus integer NOT NULL,
    tipo_docente character varying(45) NOT NULL,
    id_clasificacion_docente integer NOT NULL,
    id_escalafon integer NOT NULL
);


ALTER TABLE public.docente OWNER TO postgres;

--
-- Name: docente_aldea; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.docente_aldea (
    id_docente_aldea integer NOT NULL,
    id_docente integer NOT NULL,
    id_aldea_ubv integer NOT NULL
);


ALTER TABLE public.docente_aldea OWNER TO postgres;

--
-- Name: docente_aldea_id_docente_alde_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.docente_aldea_id_docente_alde_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.docente_aldea_id_docente_alde_seq OWNER TO postgres;

--
-- Name: docente_aldea_id_docente_alde_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.docente_aldea_id_docente_alde_seq OWNED BY public.docente_aldea.id_docente_aldea;


--
-- Name: docente_dedicacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.docente_dedicacion (
    id_docente_dedicacion integer NOT NULL,
    descripcion character varying(45) NOT NULL,
    total_horas character varying(45) NOT NULL,
    hora_docencia_minima character varying(45) NOT NULL,
    hora_docencia_maxima character varying(45) NOT NULL
);


ALTER TABLE public.docente_dedicacion OWNER TO postgres;

--
-- Name: docente_dedicacion_id_docente_dedicacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.docente_dedicacion_id_docente_dedicacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.docente_dedicacion_id_docente_dedicacion_seq OWNER TO postgres;

--
-- Name: docente_dedicacion_id_docente_dedicacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.docente_dedicacion_id_docente_dedicacion_seq OWNED BY public.docente_dedicacion.id_docente_dedicacion;


--
-- Name: docente_estatus; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.docente_estatus (
    id_docente_estatus integer NOT NULL,
    descripcion character varying(45) NOT NULL
);


ALTER TABLE public.docente_estatus OWNER TO postgres;

--
-- Name: docente_estatus_id_docente_estatus_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.docente_estatus_id_docente_estatus_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.docente_estatus_id_docente_estatus_seq OWNER TO postgres;

--
-- Name: docente_estatus_id_docente_estatus_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.docente_estatus_id_docente_estatus_seq OWNED BY public.docente_estatus.id_docente_estatus;


--
-- Name: docente_id_docente_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.docente_id_docente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.docente_id_docente_seq OWNER TO postgres;

--
-- Name: docente_id_docente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.docente_id_docente_seq OWNED BY public.docente.id_docente;


--
-- Name: docente_programa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.docente_programa (
    id_docente_programa integer NOT NULL,
    id_docente integer NOT NULL,
    id_programa integer NOT NULL
);


ALTER TABLE public.docente_programa OWNER TO postgres;

--
-- Name: docente_programa_id_docente_programa_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.docente_programa_id_docente_programa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.docente_programa_id_docente_programa_seq OWNER TO postgres;

--
-- Name: docente_programa_id_docente_programa_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.docente_programa_id_docente_programa_seq OWNED BY public.docente_programa.id_docente_programa;


--
-- Name: documento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.documento (
    id_documento integer NOT NULL,
    descripcion text NOT NULL,
    estatus character varying(45) NOT NULL,
    id_persona integer NOT NULL,
    id_tipo_documento integer NOT NULL
);


ALTER TABLE public.documento OWNER TO postgres;

--
-- Name: documento_id_documento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.documento_id_documento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.documento_id_documento_seq OWNER TO postgres;

--
-- Name: documento_id_documento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.documento_id_documento_seq OWNED BY public.documento.id_documento;


--
-- Name: documento_identidad_tipo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.documento_identidad_tipo (
    id_documento_identidad_tipo integer NOT NULL,
    descripcion character varying(45)
);


ALTER TABLE public.documento_identidad_tipo OWNER TO postgres;

--
-- Name: documento_identidad_tipo_id_documento_identidad_tipo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.documento_identidad_tipo_id_documento_identidad_tipo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.documento_identidad_tipo_id_documento_identidad_tipo_seq OWNER TO postgres;

--
-- Name: documento_identidad_tipo_id_documento_identidad_tipo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.documento_identidad_tipo_id_documento_identidad_tipo_seq OWNED BY public.documento_identidad_tipo.id_documento_identidad_tipo;


--
-- Name: documento_solicitud; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.documento_solicitud (
    id_documento_solicitud integer NOT NULL,
    id_solicitud_ascenso integer NOT NULL,
    id_documento integer NOT NULL
);


ALTER TABLE public.documento_solicitud OWNER TO postgres;

--
-- Name: documento_solicitud_id_documento_solicitud_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.documento_solicitud_id_documento_solicitud_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.documento_solicitud_id_documento_solicitud_seq OWNER TO postgres;

--
-- Name: documento_solicitud_id_documento_solicitud_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.documento_solicitud_id_documento_solicitud_seq OWNED BY public.documento_solicitud.id_documento_solicitud;


--
-- Name: domicilio_detalle_tipo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.domicilio_detalle_tipo (
    id_domicilio_detalle_tipo integer NOT NULL,
    descripcion character varying(45) NOT NULL
);


ALTER TABLE public.domicilio_detalle_tipo OWNER TO postgres;

--
-- Name: domicilio_detalle_tipo_id_domicilio_detalle_tipo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.domicilio_detalle_tipo_id_domicilio_detalle_tipo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.domicilio_detalle_tipo_id_domicilio_detalle_tipo_seq OWNER TO postgres;

--
-- Name: domicilio_detalle_tipo_id_domicilio_detalle_tipo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.domicilio_detalle_tipo_id_domicilio_detalle_tipo_seq OWNED BY public.domicilio_detalle_tipo.id_domicilio_detalle_tipo;


--
-- Name: domicilio_persona; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.domicilio_persona (
    id_domicilio_persona integer NOT NULL,
    id_parroquia integer NOT NULL,
    id_persona integer NOT NULL,
    domicilio_detalle character varying(150),
    id_domicilio_detalle_tipo integer
);


ALTER TABLE public.domicilio_persona OWNER TO postgres;

--
-- Name: domicilio_persona_id_domicilio_persona_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.domicilio_persona_id_domicilio_persona_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.domicilio_persona_id_domicilio_persona_seq OWNER TO postgres;

--
-- Name: domicilio_persona_id_domicilio_persona_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.domicilio_persona_id_domicilio_persona_seq OWNED BY public.domicilio_persona.id_domicilio_persona;


--
-- Name: eje_formacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.eje_formacion (
    id_eje_formacion integer NOT NULL,
    descripcion character varying(70) NOT NULL,
    codigo character varying(5) NOT NULL
);


ALTER TABLE public.eje_formacion OWNER TO postgres;

--
-- Name: eje_formacion_id_eje_formacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.eje_formacion_id_eje_formacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.eje_formacion_id_eje_formacion_seq OWNER TO postgres;

--
-- Name: eje_formacion_id_eje_formacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.eje_formacion_id_eje_formacion_seq OWNED BY public.eje_formacion.id_eje_formacion;


--
-- Name: eje_municipal; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.eje_municipal (
    id_eje_municipal integer NOT NULL,
    descripcion character varying(70) NOT NULL,
    codigo character varying(15) NOT NULL,
    id_eje_regional integer NOT NULL,
    estatus character varying DEFAULT false
);


ALTER TABLE public.eje_municipal OWNER TO postgres;

--
-- Name: eje_municipal_id_eje_municipal_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.eje_municipal_id_eje_municipal_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.eje_municipal_id_eje_municipal_seq OWNER TO postgres;

--
-- Name: eje_municipal_id_eje_municipal_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.eje_municipal_id_eje_municipal_seq OWNED BY public.eje_municipal.id_eje_municipal;


--
-- Name: eje_regional; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.eje_regional (
    id_eje_regional integer NOT NULL,
    descripcion character varying(70) NOT NULL,
    codigo character varying(15) NOT NULL,
    estatus character varying DEFAULT false
);


ALTER TABLE public.eje_regional OWNER TO postgres;

--
-- Name: eje_regional_id_eje_regional_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.eje_regional_id_eje_regional_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.eje_regional_id_eje_regional_seq OWNER TO postgres;

--
-- Name: eje_regional_id_eje_regional_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.eje_regional_id_eje_regional_seq OWNED BY public.eje_regional.id_eje_regional;


--
-- Name: escalafon; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.escalafon (
    id_escalafon integer NOT NULL,
    descripcion character varying(45) NOT NULL
);


ALTER TABLE public.escalafon OWNER TO postgres;

--
-- Name: escalafon_id_escalafon_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.escalafon_id_escalafon_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.escalafon_id_escalafon_seq OWNER TO postgres;

--
-- Name: escalafon_id_escalafon_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.escalafon_id_escalafon_seq OWNED BY public.escalafon.id_escalafon;


--
-- Name: espacio_tipo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.espacio_tipo (
    id_espacio_tipo integer NOT NULL,
    descripcion character varying(70) NOT NULL
);


ALTER TABLE public.espacio_tipo OWNER TO postgres;

--
-- Name: espacio_tipo_id_espacio_tipo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.espacio_tipo_id_espacio_tipo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.espacio_tipo_id_espacio_tipo_seq OWNER TO postgres;

--
-- Name: espacio_tipo_id_espacio_tipo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.espacio_tipo_id_espacio_tipo_seq OWNED BY public.espacio_tipo.id_espacio_tipo;


--
-- Name: estado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.estado (
    id_estado integer NOT NULL,
    descripcion character varying(50) NOT NULL,
    codigo character varying(6) NOT NULL,
    id_pais integer NOT NULL
);


ALTER TABLE public.estado OWNER TO postgres;

--
-- Name: estado_civil; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.estado_civil (
    id_estado_civil integer NOT NULL,
    descripcion character varying(20) NOT NULL,
    codigo character(5) NOT NULL
);


ALTER TABLE public.estado_civil OWNER TO postgres;

--
-- Name: estado_civil_id_estado_civil_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.estado_civil_id_estado_civil_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.estado_civil_id_estado_civil_seq OWNER TO postgres;

--
-- Name: estado_civil_id_estado_civil_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.estado_civil_id_estado_civil_seq OWNED BY public.estado_civil.id_estado_civil;


--
-- Name: estado_id_estado_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.estado_id_estado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.estado_id_estado_seq OWNER TO postgres;

--
-- Name: estado_id_estado_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.estado_id_estado_seq OWNED BY public.estado.id_estado;


--
-- Name: estatus_ascenso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.estatus_ascenso (
    id_estatus_ascenso integer NOT NULL,
    descripcion character varying(45) NOT NULL
);


ALTER TABLE public.estatus_ascenso OWNER TO postgres;

--
-- Name: estatus_ascenso_id_estatus_ascenso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.estatus_ascenso_id_estatus_ascenso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.estatus_ascenso_id_estatus_ascenso_seq OWNER TO postgres;

--
-- Name: estatus_ascenso_id_estatus_ascenso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.estatus_ascenso_id_estatus_ascenso_seq OWNED BY public.estatus_ascenso.id_estatus_ascenso;


--
-- Name: estudiante; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.estudiante (
    id_estudiante integer NOT NULL,
    estatus character varying(45),
    id_persona integer NOT NULL,
    id_aspirante_tipo integer NOT NULL
);


ALTER TABLE public.estudiante OWNER TO postgres;

--
-- Name: estudiante_detalle; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.estudiante_detalle (
    id_estudiante_detalle integer NOT NULL,
    estatus character(2) NOT NULL,
    fecha_ingreso date NOT NULL,
    numero_expediente character varying(15) NOT NULL,
    id_estudiante integer NOT NULL,
    id_aldea_malla_curricula integer NOT NULL,
    id_turno integer NOT NULL
);


ALTER TABLE public.estudiante_detalle OWNER TO postgres;

--
-- Name: estudiante_detalle_id_estudiante_detalle_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.estudiante_detalle_id_estudiante_detalle_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.estudiante_detalle_id_estudiante_detalle_seq OWNER TO postgres;

--
-- Name: estudiante_detalle_id_estudiante_detalle_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.estudiante_detalle_id_estudiante_detalle_seq OWNED BY public.estudiante_detalle.id_estudiante_detalle;


--
-- Name: estudiante_documento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.estudiante_documento (
    id_estudiante_documento integer NOT NULL,
    ruta_documento character varying(500),
    id_requisito_inscripcion integer NOT NULL,
    id_persona integer NOT NULL,
    id_tipo_archivo integer NOT NULL,
    estatus_documento character varying(45) NOT NULL,
    descripcion_estatus_documento character varying(200) NOT NULL
);


ALTER TABLE public.estudiante_documento OWNER TO postgres;

--
-- Name: estudiante_documento_id_estudiante_documento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.estudiante_documento_id_estudiante_documento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.estudiante_documento_id_estudiante_documento_seq OWNER TO postgres;

--
-- Name: estudiante_documento_id_estudiante_documento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.estudiante_documento_id_estudiante_documento_seq OWNED BY public.estudiante_documento.id_estudiante_documento;


--
-- Name: estudiante_id_estudiante_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.estudiante_id_estudiante_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.estudiante_id_estudiante_seq OWNER TO postgres;

--
-- Name: estudiante_id_estudiante_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.estudiante_id_estudiante_seq OWNED BY public.estudiante.id_estudiante;


--
-- Name: estudio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.estudio (
    id_estudio integer NOT NULL,
    estudio character varying(45) NOT NULL,
    id_tipo_estudio integer NOT NULL,
    estatus character varying(45) NOT NULL,
    id_pais integer NOT NULL,
    id_modalidad_estudio integer NOT NULL,
    ano_estudio integer NOT NULL,
    id_institucion integer NOT NULL,
    id_docente integer NOT NULL,
    estatus_verificacion character varying(20) NOT NULL
);


ALTER TABLE public.estudio OWNER TO postgres;

--
-- Name: estudio_conducente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.estudio_conducente (
    id_estudio_conducente integer NOT NULL,
    titulo_obtenido character varying(100) NOT NULL,
    id_estudio integer NOT NULL
);


ALTER TABLE public.estudio_conducente OWNER TO postgres;

--
-- Name: estudio_conducente_id_estudio_conducente_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.estudio_conducente_id_estudio_conducente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.estudio_conducente_id_estudio_conducente_seq OWNER TO postgres;

--
-- Name: estudio_conducente_id_estudio_conducente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.estudio_conducente_id_estudio_conducente_seq OWNED BY public.estudio_conducente.id_estudio_conducente;


--
-- Name: estudio_id_estudio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.estudio_id_estudio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.estudio_id_estudio_seq OWNER TO postgres;

--
-- Name: estudio_id_estudio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.estudio_id_estudio_seq OWNED BY public.estudio.id_estudio;


--
-- Name: estudio_idioma; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.estudio_idioma (
    id_estudio_idioma integer NOT NULL,
    nivel_lectura character varying(45) NOT NULL,
    nivel_escritura character varying(45) NOT NULL,
    nivel_habla character varying(45) NOT NULL,
    nivel_comprende character varying(45) NOT NULL,
    id_estudio integer NOT NULL,
    id_idioma integer NOT NULL
);


ALTER TABLE public.estudio_idioma OWNER TO postgres;

--
-- Name: estudio_nivel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.estudio_nivel (
    id_estudio_nivel integer NOT NULL,
    id_estudio integer NOT NULL,
    id_nivel_academico integer NOT NULL,
    id_programa_nivel integer NOT NULL
);


ALTER TABLE public.estudio_nivel OWNER TO postgres;

--
-- Name: estudio_nivel_id_estudio_nivel_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.estudio_nivel_id_estudio_nivel_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.estudio_nivel_id_estudio_nivel_seq OWNER TO postgres;

--
-- Name: estudio_nivel_id_estudio_nivel_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.estudio_nivel_id_estudio_nivel_seq OWNED BY public.estudio_nivel.id_estudio_nivel;


--
-- Name: etnia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.etnia (
    id_etnia integer NOT NULL,
    descripcion character varying(35) NOT NULL
);


ALTER TABLE public.etnia OWNER TO postgres;

--
-- Name: etnia_id_etnia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.etnia_id_etnia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.etnia_id_etnia_seq OWNER TO postgres;

--
-- Name: etnia_id_etnia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.etnia_id_etnia_seq OWNED BY public.etnia.id_etnia;


--
-- Name: expediente_documento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.expediente_documento (
    id_expediente_documento integer NOT NULL,
    id_estudiante_documento integer NOT NULL,
    id_estudiante_detalle integer NOT NULL
);


ALTER TABLE public.expediente_documento OWNER TO postgres;

--
-- Name: expediente_documento_id_expediente_documento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.expediente_documento_id_expediente_documento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.expediente_documento_id_expediente_documento_seq OWNER TO postgres;

--
-- Name: expediente_documento_id_expediente_documento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.expediente_documento_id_expediente_documento_seq OWNED BY public.expediente_documento.id_expediente_documento;


--
-- Name: experiencia_docente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.experiencia_docente (
    id_experiencia_docente integer NOT NULL,
    id_tipo_experiencia integer NOT NULL,
    id_cargo_experiencia integer NOT NULL,
    fecha_ingreso date NOT NULL,
    fecha_egreso date NOT NULL,
    id_estado integer NOT NULL,
    id_institucion integer NOT NULL,
    id_docente integer NOT NULL,
    ano_experiencia integer NOT NULL,
    estatus_verificacion character varying(45) NOT NULL
);


ALTER TABLE public.experiencia_docente OWNER TO postgres;

--
-- Name: experiencia_docente_id_experiencia_docente_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.experiencia_docente_id_experiencia_docente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.experiencia_docente_id_experiencia_docente_seq OWNER TO postgres;

--
-- Name: experiencia_docente_id_experiencia_docente_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.experiencia_docente_id_experiencia_docente_seq OWNED BY public.experiencia_docente.id_experiencia_docente;


--
-- Name: fase_ii; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fase_ii (
    id_fase_ii integer NOT NULL,
    acta_defensa character varying(45) NOT NULL,
    id_solicitud_ascenso integer NOT NULL,
    memo_acta_1 character varying(45) NOT NULL,
    memo_acta_2 character varying(45) NOT NULL,
    pto_cuenta character varying(45) NOT NULL
);


ALTER TABLE public.fase_ii OWNER TO postgres;

--
-- Name: fase_ii_id_fase_ii_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.fase_ii_id_fase_ii_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fase_ii_id_fase_ii_seq OWNER TO postgres;

--
-- Name: fase_ii_id_fase_ii_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.fase_ii_id_fase_ii_seq OWNED BY public.fase_ii.id_fase_ii;


--
-- Name: genero; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.genero (
    id_genero integer NOT NULL,
    descripcion character varying(10) NOT NULL,
    codigo character(5) NOT NULL
);


ALTER TABLE public.genero OWNER TO postgres;

--
-- Name: genero_id_genero_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.genero_id_genero_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.genero_id_genero_seq OWNER TO postgres;

--
-- Name: genero_id_genero_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.genero_id_genero_seq OWNED BY public.genero.id_genero;


--
-- Name: grado_academico; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.grado_academico (
    id_grado_academico integer NOT NULL,
    descripcion character varying(70),
    id_nivel_academico integer NOT NULL,
    codigo_ine character varying(5) NOT NULL
);


ALTER TABLE public.grado_academico OWNER TO postgres;

--
-- Name: grado_academico_id_grado_academico_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.grado_academico_id_grado_academico_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grado_academico_id_grado_academico_seq OWNER TO postgres;

--
-- Name: grado_academico_id_grado_academico_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.grado_academico_id_grado_academico_seq OWNED BY public.grado_academico.id_grado_academico;


--
-- Name: historia_ascenso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.historia_ascenso (
    id_historia_ascenso integer NOT NULL,
    fecha_ascenso date NOT NULL,
    id_escalafon integer NOT NULL,
    id_docente integer NOT NULL
);


ALTER TABLE public.historia_ascenso OWNER TO postgres;

--
-- Name: historia_ascenso_id_historia_ascenso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.historia_ascenso_id_historia_ascenso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.historia_ascenso_id_historia_ascenso_seq OWNER TO postgres;

--
-- Name: historia_ascenso_id_historia_ascenso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.historia_ascenso_id_historia_ascenso_seq OWNED BY public.historia_ascenso.id_historia_ascenso;


--
-- Name: historia_jurado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.historia_jurado (
    id_historia_jurado integer NOT NULL,
    jurado_id_jurado integer NOT NULL,
    id_solicitud_ascenso integer NOT NULL,
    fecha_jurado date NOT NULL,
    id_propuesta_jurado integer NOT NULL
);


ALTER TABLE public.historia_jurado OWNER TO postgres;

--
-- Name: historia_jurado_id_historia_jurado_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.historia_jurado_id_historia_jurado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.historia_jurado_id_historia_jurado_seq OWNER TO postgres;

--
-- Name: historia_jurado_id_historia_jurado_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.historia_jurado_id_historia_jurado_seq OWNED BY public.historia_jurado.id_historia_jurado;


--
-- Name: history_aldea_programa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.history_aldea_programa (
    id_aldea_programa integer NOT NULL,
    codigo_opsu integer,
    id_aldea integer NOT NULL,
    descripcion_aldea character varying(150),
    id_programa integer NOT NULL,
    descripcion_programa character varying(150),
    estatus character varying(45),
    hora timestamp without time zone
);


ALTER TABLE public.history_aldea_programa OWNER TO postgres;

--
-- Name: history_comitepfa_integrante; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.history_comitepfa_integrante (
    id_comitepfa_integrante integer NOT NULL,
    fecha_inicio date NOT NULL,
    fecha_fin date,
    estatus_docente character varying(6),
    tipo_docente character varying(45),
    primer_nombre character varying(30),
    primer_apellido character varying(30),
    identificacion integer NOT NULL,
    comite_nombre character varying(45),
    docente_programa character varying(70),
    fecha_registro date,
    comite_programa character varying(45),
    comite_tipo character varying(45)
);


ALTER TABLE public.history_comitepfa_integrante OWNER TO postgres;

--
-- Name: horario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.horario (
    id_horario integer NOT NULL,
    bloque_inicio character varying(45),
    bloque_fin character varying(45),
    id_dia_semana integer NOT NULL,
    id_carga_academica integer NOT NULL
);


ALTER TABLE public.horario OWNER TO postgres;

--
-- Name: horario_id_horario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.horario_id_horario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.horario_id_horario_seq OWNER TO postgres;

--
-- Name: horario_id_horario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.horario_id_horario_seq OWNED BY public.horario.id_horario;


--
-- Name: idioma; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.idioma (
    id_idioma integer NOT NULL,
    code character varying(10) NOT NULL,
    name character varying(80) NOT NULL,
    nativename character varying(57)
);


ALTER TABLE public.idioma OWNER TO postgres;

--
-- Name: idioma_id_idioma_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.idioma_id_idioma_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.idioma_id_idioma_seq OWNER TO postgres;

--
-- Name: idioma_id_idioma_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.idioma_id_idioma_seq OWNED BY public.estudio_idioma.id_estudio_idioma;


--
-- Name: idioma_id_idioma_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.idioma_id_idioma_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.idioma_id_idioma_seq1 OWNER TO postgres;

--
-- Name: idioma_id_idioma_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.idioma_id_idioma_seq1 OWNED BY public.idioma.id_idioma;


--
-- Name: ins_uni_curricular; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ins_uni_curricular (
    id_ins_uni_curricular integer NOT NULL,
    id_inscripcion integer NOT NULL,
    id_malla_curricular integer NOT NULL,
    nota character varying(45)
);


ALTER TABLE public.ins_uni_curricular OWNER TO postgres;

--
-- Name: ins_uni_curricular_id_ins_uni_curricular_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ins_uni_curricular_id_ins_uni_curricular_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ins_uni_curricular_id_ins_uni_curricular_seq OWNER TO postgres;

--
-- Name: ins_uni_curricular_id_ins_uni_curricular_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ins_uni_curricular_id_ins_uni_curricular_seq OWNED BY public.ins_uni_curricular.id_ins_uni_curricular;


--
-- Name: inscripcion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.inscripcion (
    id_inscripcion integer NOT NULL,
    estudiante_id integer NOT NULL
);


ALTER TABLE public.inscripcion OWNER TO postgres;

--
-- Name: inscripcion_detalle; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.inscripcion_detalle (
    id_inscripcion_detalle integer NOT NULL,
    codigo character varying(6) NOT NULL,
    estatus character(1) NOT NULL,
    fecha_ingreso date NOT NULL,
    id_inscripcion integer NOT NULL,
    id_carga_academica integer NOT NULL
);


ALTER TABLE public.inscripcion_detalle OWNER TO postgres;

--
-- Name: inscripcion_detalle_id_inscripcion_detalle_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.inscripcion_detalle_id_inscripcion_detalle_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.inscripcion_detalle_id_inscripcion_detalle_seq OWNER TO postgres;

--
-- Name: inscripcion_detalle_id_inscripcion_detalle_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.inscripcion_detalle_id_inscripcion_detalle_seq OWNED BY public.inscripcion_detalle.id_inscripcion_detalle;


--
-- Name: inscripcion_id_inscripcion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.inscripcion_id_inscripcion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.inscripcion_id_inscripcion_seq OWNER TO postgres;

--
-- Name: inscripcion_id_inscripcion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.inscripcion_id_inscripcion_seq OWNED BY public.inscripcion.id_inscripcion;


--
-- Name: institucion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.institucion (
    id_institucion integer NOT NULL,
    descripcion character varying(45) NOT NULL,
    id_pais integer NOT NULL,
    id_institucion_tipo integer NOT NULL
);


ALTER TABLE public.institucion OWNER TO postgres;

--
-- Name: institucion_educativa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.institucion_educativa (
    id_institucion_educativa integer NOT NULL,
    descripcion character varying(45) NOT NULL
);


ALTER TABLE public.institucion_educativa OWNER TO postgres;

--
-- Name: institucion_educativa_id_institucion_educativa_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.institucion_educativa_id_institucion_educativa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.institucion_educativa_id_institucion_educativa_seq OWNER TO postgres;

--
-- Name: institucion_educativa_id_institucion_educativa_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.institucion_educativa_id_institucion_educativa_seq OWNED BY public.institucion_educativa.id_institucion_educativa;


--
-- Name: institucion_id_institucion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.institucion_id_institucion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.institucion_id_institucion_seq OWNER TO postgres;

--
-- Name: institucion_id_institucion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.institucion_id_institucion_seq OWNED BY public.institucion.id_institucion;


--
-- Name: institucion_tipo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.institucion_tipo (
    id_institucion_tipo integer NOT NULL,
    descripcion character varying(45) NOT NULL
);


ALTER TABLE public.institucion_tipo OWNER TO postgres;

--
-- Name: institucion_tipo_id_institucion_tipo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.institucion_tipo_id_institucion_tipo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.institucion_tipo_id_institucion_tipo_seq OWNER TO postgres;

--
-- Name: institucion_tipo_id_institucion_tipo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.institucion_tipo_id_institucion_tipo_seq OWNED BY public.institucion_tipo.id_institucion_tipo;


--
-- Name: jurado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jurado (
    id_jurado integer NOT NULL,
    id_propuesta_jurado integer NOT NULL,
    id_tipo_jurado integer NOT NULL,
    id_docente integer NOT NULL
);


ALTER TABLE public.jurado OWNER TO postgres;

--
-- Name: jurado_id_jurado_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jurado_id_jurado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jurado_id_jurado_seq OWNER TO postgres;

--
-- Name: jurado_id_jurado_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.jurado_id_jurado_seq OWNED BY public.jurado.id_jurado;


--
-- Name: libro; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.libro (
    id_libro integer NOT NULL,
    nombre_libro character varying(80) NOT NULL,
    editorial character varying(80) NOT NULL,
    isbn_libro character varying(45) NOT NULL,
    ano_publicacion integer NOT NULL,
    ciudad_publicacion character varying(80) NOT NULL,
    volumenes integer NOT NULL,
    nro_paginas integer NOT NULL,
    url_libro character varying(80) NOT NULL,
    id_docente integer NOT NULL,
    estatus_verificacion character varying(45) NOT NULL
);


ALTER TABLE public.libro OWNER TO postgres;

--
-- Name: libro_id_libro_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.libro_id_libro_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.libro_id_libro_seq OWNER TO postgres;

--
-- Name: libro_id_libro_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.libro_id_libro_seq OWNED BY public.libro.id_libro;


--
-- Name: linea_investigacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.linea_investigacion (
    id_linea_investigacion integer NOT NULL,
    descripcion character varying(45) NOT NULL,
    codigo character varying(5) NOT NULL,
    especificacion character varying(255) NOT NULL,
    id_nucleo_academico integer NOT NULL
);


ALTER TABLE public.linea_investigacion OWNER TO postgres;

--
-- Name: linea_investigacion_id_linea_investigacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.linea_investigacion_id_linea_investigacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.linea_investigacion_id_linea_investigacion_seq OWNER TO postgres;

--
-- Name: linea_investigacion_id_linea_investigacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.linea_investigacion_id_linea_investigacion_seq OWNED BY public.linea_investigacion.id_linea_investigacion;


--
-- Name: malla_curricular; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.malla_curricular (
    id_malla_curricular integer NOT NULL,
    descripcion character varying(150) NOT NULL,
    codigo character varying(15) NOT NULL,
    salida_intermedia boolean DEFAULT false,
    id_modalidad_malla integer NOT NULL,
    id_programa integer NOT NULL,
    estatus boolean DEFAULT false
);


ALTER TABLE public.malla_curricular OWNER TO postgres;

--
-- Name: malla_curricular_detalle; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.malla_curricular_detalle (
    id_malla_curricular_detalle integer NOT NULL,
    cantidad_tramo integer,
    id_tramo integer NOT NULL,
    id_malla_curricular integer NOT NULL,
    id_trayecto integer NOT NULL,
    id_unidad_curricular_detalle integer NOT NULL
);


ALTER TABLE public.malla_curricular_detalle OWNER TO postgres;

--
-- Name: malla_curricular_detalle_id_malla_curricular_detalle_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.malla_curricular_detalle_id_malla_curricular_detalle_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.malla_curricular_detalle_id_malla_curricular_detalle_seq OWNER TO postgres;

--
-- Name: malla_curricular_detalle_id_malla_curricular_detalle_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.malla_curricular_detalle_id_malla_curricular_detalle_seq OWNED BY public.malla_curricular_detalle.id_malla_curricular_detalle;


--
-- Name: malla_curricular_id_malla_curricular_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.malla_curricular_id_malla_curricular_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.malla_curricular_id_malla_curricular_seq OWNER TO postgres;

--
-- Name: malla_curricular_id_malla_curricular_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.malla_curricular_id_malla_curricular_seq OWNED BY public.malla_curricular.id_malla_curricular;


--
-- Name: memo_solicitud; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.memo_solicitud (
    id_memo_solicitud integer NOT NULL,
    id_documento integer NOT NULL,
    id_solicitud_ascenso integer NOT NULL,
    estatus character varying(45) NOT NULL
);


ALTER TABLE public.memo_solicitud OWNER TO postgres;

--
-- Name: memo_solicitud_id_memo_solicitud_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.memo_solicitud_id_memo_solicitud_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.memo_solicitud_id_memo_solicitud_seq OWNER TO postgres;

--
-- Name: memo_solicitud_id_memo_solicitud_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.memo_solicitud_id_memo_solicitud_seq OWNED BY public.memo_solicitud.id_memo_solicitud;


--
-- Name: metodologia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.metodologia (
    id_metodologia integer NOT NULL,
    descripcion character varying(45) NOT NULL,
    id_tipo_metodologia integer NOT NULL
);


ALTER TABLE public.metodologia OWNER TO postgres;

--
-- Name: metodologia_id_metodologia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.metodologia_id_metodologia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.metodologia_id_metodologia_seq OWNER TO postgres;

--
-- Name: metodologia_id_metodologia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.metodologia_id_metodologia_seq OWNED BY public.metodologia.id_metodologia;


--
-- Name: modalidad_estudio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.modalidad_estudio (
    id_modalidad_estudio integer NOT NULL,
    descripcion character varying(70) NOT NULL
);


ALTER TABLE public.modalidad_estudio OWNER TO postgres;

--
-- Name: modalidad_estudio_id_modalidad_estudio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.modalidad_estudio_id_modalidad_estudio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modalidad_estudio_id_modalidad_estudio_seq OWNER TO postgres;

--
-- Name: modalidad_estudio_id_modalidad_estudio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.modalidad_estudio_id_modalidad_estudio_seq OWNED BY public.modalidad_estudio.id_modalidad_estudio;


--
-- Name: modalidad_malla; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.modalidad_malla (
    id_modalidad_malla integer NOT NULL,
    descripcion character varying(45) NOT NULL
);


ALTER TABLE public.modalidad_malla OWNER TO postgres;

--
-- Name: modalidad_malla_id_modalidad_malla_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.modalidad_malla_id_modalidad_malla_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modalidad_malla_id_modalidad_malla_seq OWNER TO postgres;

--
-- Name: modalidad_malla_id_modalidad_malla_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.modalidad_malla_id_modalidad_malla_seq OWNED BY public.modalidad_malla.id_modalidad_malla;


--
-- Name: modulo_activacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.modulo_activacion (
    id_modulo_activacion integer NOT NULL,
    fecha_inicio date NOT NULL,
    fecha_fin date NOT NULL,
    id_oferta_academica integer NOT NULL,
    tipo_activacion character varying(45) NOT NULL,
    id_aldea integer NOT NULL,
    descripcion character varying(70)
);


ALTER TABLE public.modulo_activacion OWNER TO postgres;

--
-- Name: modulo_activacion_ingreso_id_modulo_activacion_ingreso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.modulo_activacion_ingreso_id_modulo_activacion_ingreso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.modulo_activacion_ingreso_id_modulo_activacion_ingreso_seq OWNER TO postgres;

--
-- Name: modulo_activacion_ingreso_id_modulo_activacion_ingreso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.modulo_activacion_ingreso_id_modulo_activacion_ingreso_seq OWNED BY public.modulo_activacion.id_modulo_activacion;


--
-- Name: municipio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.municipio (
    id_municipio integer NOT NULL,
    descripcion character varying(45) NOT NULL,
    codigo character varying(6) NOT NULL,
    id_estado integer NOT NULL
);


ALTER TABLE public.municipio OWNER TO postgres;

--
-- Name: municipio_id_municipio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.municipio_id_municipio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.municipio_id_municipio_seq OWNER TO postgres;

--
-- Name: municipio_id_municipio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.municipio_id_municipio_seq OWNED BY public.municipio.id_municipio;


--
-- Name: nacionalidad; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.nacionalidad (
    id_nacionalidad integer NOT NULL,
    descripcion character varying(45) NOT NULL,
    id_pais integer NOT NULL
);


ALTER TABLE public.nacionalidad OWNER TO postgres;

--
-- Name: nacionalidad_id_nacionalidad_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.nacionalidad_id_nacionalidad_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nacionalidad_id_nacionalidad_seq OWNER TO postgres;

--
-- Name: nacionalidad_id_nacionalidad_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.nacionalidad_id_nacionalidad_seq OWNED BY public.nacionalidad.id_nacionalidad;


--
-- Name: nivel_academico; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.nivel_academico (
    id_nivel_academico integer NOT NULL,
    descripcion character varying(70) NOT NULL,
    codigo_ine character varying(45) NOT NULL
);


ALTER TABLE public.nivel_academico OWNER TO postgres;

--
-- Name: nivel_academico_id_nivel_academico_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.nivel_academico_id_nivel_academico_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nivel_academico_id_nivel_academico_seq OWNER TO postgres;

--
-- Name: nivel_academico_id_nivel_academico_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.nivel_academico_id_nivel_academico_seq OWNED BY public.nivel_academico.id_nivel_academico;


--
-- Name: nomina_docente_sigad; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.nomina_docente_sigad (
    id_nomina_docente_sigad integer NOT NULL,
    cedula integer NOT NULL,
    nombres character varying(150) NOT NULL,
    nacionalidad character varying(25) NOT NULL,
    fecha_nacimiento date NOT NULL,
    pais_nacimiento character varying(80),
    estado_nacimiento character varying(80),
    genero character varying(30) NOT NULL,
    estado_civil character varying(30) NOT NULL,
    telefono character varying(30),
    correo_electronico character varying(50),
    direccion character varying(255) NOT NULL,
    numero_hijos integer NOT NULL,
    cargo character varying(50) NOT NULL,
    clasificacion_docente character varying(50) NOT NULL,
    fecha_ingreso date NOT NULL,
    dedicacion character varying(80) NOT NULL,
    estatus character varying(20) NOT NULL
);


ALTER TABLE public.nomina_docente_sigad OWNER TO postgres;

--
-- Name: nomina_docente_sigad_id_nomina_docente_sigad_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.nomina_docente_sigad_id_nomina_docente_sigad_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nomina_docente_sigad_id_nomina_docente_sigad_seq OWNER TO postgres;

--
-- Name: nomina_docente_sigad_id_nomina_docente_sigad_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.nomina_docente_sigad_id_nomina_docente_sigad_seq OWNED BY public.nomina_docente_sigad.id_nomina_docente_sigad;


--
-- Name: noticia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.noticia (
    id_noticia integer NOT NULL,
    contenido text,
    nom_arch character varying(100),
    titulo character varying(150),
    fecha date
);


ALTER TABLE public.noticia OWNER TO postgres;

--
-- Name: noticia_id_noticia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.noticia_id_noticia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.noticia_id_noticia_seq OWNER TO postgres;

--
-- Name: noticia_id_noticia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.noticia_id_noticia_seq OWNED BY public.noticia.id_noticia;


--
-- Name: nucleo_academico; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.nucleo_academico (
    id_nucleo_academico integer NOT NULL,
    descripcion character varying(45) NOT NULL,
    id_centro_estudio integer NOT NULL
);


ALTER TABLE public.nucleo_academico OWNER TO postgres;

--
-- Name: nucleo_academico_id_nucleo_academico_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.nucleo_academico_id_nucleo_academico_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nucleo_academico_id_nucleo_academico_seq OWNER TO postgres;

--
-- Name: nucleo_academico_id_nucleo_academico_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.nucleo_academico_id_nucleo_academico_seq OWNED BY public.nucleo_academico.id_nucleo_academico;


--
-- Name: oferta_academica; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oferta_academica (
    id_oferta_academica integer NOT NULL,
    estatus character varying(45) NOT NULL,
    id_periodo_academico integer NOT NULL,
    id_aldea_malla_curricula integer NOT NULL,
    descripcion character varying(70)
);


ALTER TABLE public.oferta_academica OWNER TO postgres;

--
-- Name: oferta_academica_detalle; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.oferta_academica_detalle (
    id_oferta_academica_detalle integer NOT NULL,
    id_oferta_academica integer NOT NULL,
    id_malla_curricular_detalle integer NOT NULL,
    id_seccion integer NOT NULL
);


ALTER TABLE public.oferta_academica_detalle OWNER TO postgres;

--
-- Name: oferta_academica_detalle_id_oferta_academica_detalle_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.oferta_academica_detalle_id_oferta_academica_detalle_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oferta_academica_detalle_id_oferta_academica_detalle_seq OWNER TO postgres;

--
-- Name: oferta_academica_detalle_id_oferta_academica_detalle_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.oferta_academica_detalle_id_oferta_academica_detalle_seq OWNED BY public.oferta_academica_detalle.id_oferta_academica_detalle;


--
-- Name: oferta_academica_id_oferta_academica_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.oferta_academica_id_oferta_academica_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.oferta_academica_id_oferta_academica_seq OWNER TO postgres;

--
-- Name: oferta_academica_id_oferta_academica_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.oferta_academica_id_oferta_academica_seq OWNED BY public.oferta_academica.id_oferta_academica;


--
-- Name: pais; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pais (
    id_pais integer NOT NULL,
    descripcion character varying(70) NOT NULL,
    codigo character varying(45),
    estatus character varying DEFAULT false
);


ALTER TABLE public.pais OWNER TO postgres;

--
-- Name: pais_id_pais_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pais_id_pais_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pais_id_pais_seq OWNER TO postgres;

--
-- Name: pais_id_pais_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pais_id_pais_seq OWNED BY public.pais.id_pais;


--
-- Name: parroquia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.parroquia (
    id_parroquia integer NOT NULL,
    descripcion character varying(50) NOT NULL,
    codigo character varying(6) NOT NULL,
    id_municipio integer NOT NULL,
    id_eje_municipal integer NOT NULL
);


ALTER TABLE public.parroquia OWNER TO postgres;

--
-- Name: parroquia_id_parroquia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.parroquia_id_parroquia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.parroquia_id_parroquia_seq OWNER TO postgres;

--
-- Name: parroquia_id_parroquia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.parroquia_id_parroquia_seq OWNED BY public.parroquia.id_parroquia;


--
-- Name: perfil; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.perfil (
    id_perfil integer NOT NULL,
    descripcion character varying(45) NOT NULL
);


ALTER TABLE public.perfil OWNER TO postgres;

--
-- Name: perfil_id_perfil_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.perfil_id_perfil_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.perfil_id_perfil_seq OWNER TO postgres;

--
-- Name: perfil_id_perfil_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.perfil_id_perfil_seq OWNED BY public.perfil.id_perfil;


--
-- Name: periodo_academico; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.periodo_academico (
    id_periodo_academico integer NOT NULL,
    descripcion character varying(70) NOT NULL,
    estatus character varying(6) DEFAULT false,
    calendario_academico character varying(45) NOT NULL
);


ALTER TABLE public.periodo_academico OWNER TO postgres;

--
-- Name: periodo_academico_id_periodo_academico_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.periodo_academico_id_periodo_academico_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.periodo_academico_id_periodo_academico_seq OWNER TO postgres;

--
-- Name: periodo_academico_id_periodo_academico_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.periodo_academico_id_periodo_academico_seq OWNED BY public.periodo_academico.id_periodo_academico;


--
-- Name: persona; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.persona (
    id_persona integer NOT NULL,
    primer_nombre character varying(30) NOT NULL,
    segundo_nombre character varying(30),
    primer_apellido character varying(30) NOT NULL,
    segundo_apellido character varying(30),
    fecha_nacimiento date NOT NULL,
    id_estado_civil integer NOT NULL,
    id_genero integer NOT NULL,
    id_pais integer NOT NULL
);


ALTER TABLE public.persona OWNER TO postgres;

--
-- Name: persona_correo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.persona_correo (
    id_persona_correo integer NOT NULL,
    correo character varying(50) NOT NULL,
    id_persona integer NOT NULL,
    id_correo_tipo integer NOT NULL
);


ALTER TABLE public.persona_correo OWNER TO postgres;

--
-- Name: persona_correo_id_persona_correo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.persona_correo_id_persona_correo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.persona_correo_id_persona_correo_seq OWNER TO postgres;

--
-- Name: persona_correo_id_persona_correo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.persona_correo_id_persona_correo_seq OWNED BY public.persona_correo.id_persona_correo;


--
-- Name: persona_discapacidad; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.persona_discapacidad (
    id_persona_discapacidad integer NOT NULL,
    id_persona integer NOT NULL,
    id_discapacidad integer NOT NULL,
    codigo_conapdis character varying(10) NOT NULL,
    observacion character varying(200)
);


ALTER TABLE public.persona_discapacidad OWNER TO postgres;

--
-- Name: persona_discapacidad_id_persona_discapacidad_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.persona_discapacidad_id_persona_discapacidad_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.persona_discapacidad_id_persona_discapacidad_seq OWNER TO postgres;

--
-- Name: persona_discapacidad_id_persona_discapacidad_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.persona_discapacidad_id_persona_discapacidad_seq OWNED BY public.persona_discapacidad.id_persona_discapacidad;


--
-- Name: persona_etnia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.persona_etnia (
    id_persona_etnia integer NOT NULL,
    id_persona integer NOT NULL,
    id_etnia integer NOT NULL
);


ALTER TABLE public.persona_etnia OWNER TO postgres;

--
-- Name: persona_etnia_id_persona_etnia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.persona_etnia_id_persona_etnia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.persona_etnia_id_persona_etnia_seq OWNER TO postgres;

--
-- Name: persona_etnia_id_persona_etnia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.persona_etnia_id_persona_etnia_seq OWNED BY public.persona_etnia.id_persona_etnia;


--
-- Name: persona_fecha_ingreso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.persona_fecha_ingreso (
    id_persona_fecha_ingreso integer NOT NULL,
    fecha date NOT NULL,
    id_persona integer NOT NULL
);


ALTER TABLE public.persona_fecha_ingreso OWNER TO postgres;

--
-- Name: persona_fecha_ingreso_id_persona_fecha_ingreso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.persona_fecha_ingreso_id_persona_fecha_ingreso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.persona_fecha_ingreso_id_persona_fecha_ingreso_seq OWNER TO postgres;

--
-- Name: persona_fecha_ingreso_id_persona_fecha_ingreso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.persona_fecha_ingreso_id_persona_fecha_ingreso_seq OWNED BY public.persona_fecha_ingreso.id_persona_fecha_ingreso;


--
-- Name: persona_hijo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.persona_hijo (
    id_persona_hijo integer NOT NULL,
    descripcion integer NOT NULL,
    id_persona integer NOT NULL
);


ALTER TABLE public.persona_hijo OWNER TO postgres;

--
-- Name: persona_hijo_id_persona_hijo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.persona_hijo_id_persona_hijo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.persona_hijo_id_persona_hijo_seq OWNER TO postgres;

--
-- Name: persona_hijo_id_persona_hijo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.persona_hijo_id_persona_hijo_seq OWNED BY public.persona_hijo.id_persona_hijo;


--
-- Name: persona_id_persona_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.persona_id_persona_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.persona_id_persona_seq OWNER TO postgres;

--
-- Name: persona_id_persona_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.persona_id_persona_seq OWNED BY public.persona.id_persona;


--
-- Name: persona_nacionalidad; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.persona_nacionalidad (
    id_persona_nacionalidad integer NOT NULL,
    identificacion integer NOT NULL,
    id_nacionalidad integer NOT NULL,
    id_documento_identidad_tipo integer NOT NULL,
    id_persona integer NOT NULL
);


ALTER TABLE public.persona_nacionalidad OWNER TO postgres;

--
-- Name: persona_nacionalidad_id_persona_nacionalidad_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.persona_nacionalidad_id_persona_nacionalidad_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.persona_nacionalidad_id_persona_nacionalidad_seq OWNER TO postgres;

--
-- Name: persona_nacionalidad_id_persona_nacionalidad_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.persona_nacionalidad_id_persona_nacionalidad_seq OWNED BY public.persona_nacionalidad.id_persona_nacionalidad;


--
-- Name: persona_telefono; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.persona_telefono (
    id_persona_telefono integer NOT NULL,
    telefono character varying(7) NOT NULL,
    id_telefono_codigo_area integer NOT NULL,
    id_persona integer NOT NULL,
    id_telefono_tipo integer NOT NULL
);


ALTER TABLE public.persona_telefono OWNER TO postgres;

--
-- Name: persona_telefono_id_persona_telefono_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.persona_telefono_id_persona_telefono_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.persona_telefono_id_persona_telefono_seq OWNER TO postgres;

--
-- Name: persona_telefono_id_persona_telefono_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.persona_telefono_id_persona_telefono_seq OWNED BY public.persona_telefono.id_persona_telefono;


--
-- Name: personal_administrativo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.personal_administrativo (
    id_personal_administrativo integer NOT NULL,
    id_persona integer NOT NULL,
    id_unidad_administrativa integer NOT NULL
);


ALTER TABLE public.personal_administrativo OWNER TO postgres;

--
-- Name: personal_administrativo_id_personal_administrativo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.personal_administrativo_id_personal_administrativo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.personal_administrativo_id_personal_administrativo_seq OWNER TO postgres;

--
-- Name: personal_administrativo_id_personal_administrativo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.personal_administrativo_id_personal_administrativo_seq OWNED BY public.personal_administrativo.id_personal_administrativo;


--
-- Name: ponencia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ponencia (
    id_ponencia integer NOT NULL,
    nombre_ponencia character varying(100) NOT NULL,
    nombre_evento character varying(100) NOT NULL,
    ano_ponencia integer NOT NULL,
    ciudad character varying(80) NOT NULL,
    titulo_memoria character varying(150) NOT NULL,
    volumen integer NOT NULL,
    identificador character varying(45) NOT NULL,
    pag_inicial integer NOT NULL,
    formato character varying(45) NOT NULL,
    url_ponencia character varying(300) NOT NULL,
    id_docente integer NOT NULL,
    estatus_verificacion character varying(45) NOT NULL
);


ALTER TABLE public.ponencia OWNER TO postgres;

--
-- Name: ponencia_id_ponencia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ponencia_id_ponencia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ponencia_id_ponencia_seq OWNER TO postgres;

--
-- Name: ponencia_id_ponencia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ponencia_id_ponencia_seq OWNED BY public.ponencia.id_ponencia;


--
-- Name: pregunta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pregunta (
    id_pregunta integer NOT NULL,
    descripcion character varying(80) NOT NULL
);


ALTER TABLE public.pregunta OWNER TO postgres;

--
-- Name: pregunta_id_pregunta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pregunta_id_pregunta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pregunta_id_pregunta_seq OWNER TO postgres;

--
-- Name: pregunta_id_pregunta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pregunta_id_pregunta_seq OWNED BY public.pregunta.id_pregunta;


--
-- Name: programa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.programa (
    id_programa integer NOT NULL,
    descripcion character varying(70) NOT NULL,
    codigo character varying(15),
    id_centro_estudio integer NOT NULL,
    id_programa_tipo integer NOT NULL,
    id_nivel_academico integer NOT NULL,
    id_linea_investigacion integer NOT NULL,
    estatus character varying(6) DEFAULT false
);


ALTER TABLE public.programa OWNER TO postgres;

--
-- Name: programa_id_programa_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.programa_id_programa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.programa_id_programa_seq OWNER TO postgres;

--
-- Name: programa_id_programa_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.programa_id_programa_seq OWNED BY public.programa.id_programa;


--
-- Name: programa_nivel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.programa_nivel (
    id_programa_nivel integer NOT NULL,
    descripcion character varying(45) NOT NULL,
    estatus boolean DEFAULT false,
    codigo character varying(45)
);


ALTER TABLE public.programa_nivel OWNER TO postgres;

--
-- Name: programa_nivel_id_programa_nivel_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.programa_nivel_id_programa_nivel_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.programa_nivel_id_programa_nivel_seq OWNER TO postgres;

--
-- Name: programa_nivel_id_programa_nivel_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.programa_nivel_id_programa_nivel_seq OWNED BY public.programa_nivel.id_programa_nivel;


--
-- Name: programa_tipo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.programa_tipo (
    id_programa_tipo integer NOT NULL,
    descripcion character varying(70) NOT NULL,
    codigo character varying(15) NOT NULL,
    id_programa_nivel integer NOT NULL
);


ALTER TABLE public.programa_tipo OWNER TO postgres;

--
-- Name: programa_tipo_id_programa_tipo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.programa_tipo_id_programa_tipo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.programa_tipo_id_programa_tipo_seq OWNER TO postgres;

--
-- Name: programa_tipo_id_programa_tipo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.programa_tipo_id_programa_tipo_seq OWNED BY public.programa_tipo.id_programa_tipo;


--
-- Name: propuesta_investigacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.propuesta_investigacion (
    id_propuesta_investigacion integer NOT NULL,
    descripcion character varying(45) NOT NULL,
    estatus_propuesta character varying(45) NOT NULL,
    observacion character varying(500) NOT NULL,
    id_comitepfa_integrante integer NOT NULL,
    objetivo character varying(100) NOT NULL,
    id_linea_investigacion integer NOT NULL,
    id_tipo_archivo integer NOT NULL,
    estatus_registro character varying(45) NOT NULL
);


ALTER TABLE public.propuesta_investigacion OWNER TO postgres;

--
-- Name: propuesta_investigacion_id_propuesta_investigacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.propuesta_investigacion_id_propuesta_investigacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.propuesta_investigacion_id_propuesta_investigacion_seq OWNER TO postgres;

--
-- Name: propuesta_investigacion_id_propuesta_investigacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.propuesta_investigacion_id_propuesta_investigacion_seq OWNED BY public.propuesta_investigacion.id_propuesta_investigacion;


--
-- Name: propuesta_jurado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.propuesta_jurado (
    id_propuesta_jurado integer NOT NULL,
    id_docente integer NOT NULL,
    estatus character varying(45) NOT NULL,
    id_solicitud_ascenso integer NOT NULL
);


ALTER TABLE public.propuesta_jurado OWNER TO postgres;

--
-- Name: propuesta_jurado_id_propuesta_jurado_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.propuesta_jurado_id_propuesta_jurado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.propuesta_jurado_id_propuesta_jurado_seq OWNER TO postgres;

--
-- Name: propuesta_jurado_id_propuesta_jurado_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.propuesta_jurado_id_propuesta_jurado_seq OWNED BY public.propuesta_jurado.id_propuesta_jurado;


--
-- Name: proyecto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.proyecto (
    id_proyecto integer NOT NULL,
    titulo character varying(250) NOT NULL,
    objetivo_general character varying(350) NOT NULL,
    descripcion character varying(1500) NOT NULL,
    metodologia character varying(45) NOT NULL,
    estatus character varying(45) NOT NULL,
    direccion character varying(45) NOT NULL,
    id_parroquia integer NOT NULL,
    id_estudiante integer NOT NULL,
    id_tutor_estudios integer NOT NULL,
    id_metodologia integer NOT NULL
);


ALTER TABLE public.proyecto OWNER TO postgres;

--
-- Name: proyecto_id_proyecto_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.proyecto_id_proyecto_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proyecto_id_proyecto_seq OWNER TO postgres;

--
-- Name: proyecto_id_proyecto_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.proyecto_id_proyecto_seq OWNED BY public.proyecto.id_proyecto;


--
-- Name: reconocimiento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reconocimiento (
    id_reconocimiento integer NOT NULL,
    id_tipo_reconocimiento integer NOT NULL,
    descripcion character varying(45) NOT NULL,
    entidad_promotora character varying(200) NOT NULL,
    ano_reconocimiento integer NOT NULL,
    caracter character varying(45) NOT NULL,
    id_documento integer NOT NULL,
    id_docente integer NOT NULL,
    estatus_verificacion character varying(45) NOT NULL
);


ALTER TABLE public.reconocimiento OWNER TO postgres;

--
-- Name: reconocimiento_id_reconocimiento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.reconocimiento_id_reconocimiento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reconocimiento_id_reconocimiento_seq OWNER TO postgres;

--
-- Name: reconocimiento_id_reconocimiento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.reconocimiento_id_reconocimiento_seq OWNED BY public.reconocimiento.id_reconocimiento;


--
-- Name: reingreso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reingreso (
    id_reingreso integer NOT NULL,
    numero_solicitud character varying(45) NOT NULL,
    motivo character varying(45) NOT NULL,
    estatus character varying(45) NOT NULL,
    id_estudiante integer NOT NULL
);


ALTER TABLE public.reingreso OWNER TO postgres;

--
-- Name: reingreso_id_reingreso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.reingreso_id_reingreso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reingreso_id_reingreso_seq OWNER TO postgres;

--
-- Name: reingreso_id_reingreso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.reingreso_id_reingreso_seq OWNED BY public.reingreso.id_reingreso;


--
-- Name: requisito_grado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.requisito_grado (
    id_requisito_grado integer NOT NULL,
    descripcion_titulo character varying(70) NOT NULL,
    id_malla_curricular integer NOT NULL,
    id_trayecto integer NOT NULL,
    id_tramo integer NOT NULL,
    id_grado_academico integer NOT NULL
);


ALTER TABLE public.requisito_grado OWNER TO postgres;

--
-- Name: requisito_grado_id_requisito_grado_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.requisito_grado_id_requisito_grado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.requisito_grado_id_requisito_grado_seq OWNER TO postgres;

--
-- Name: requisito_grado_id_requisito_grado_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.requisito_grado_id_requisito_grado_seq OWNED BY public.requisito_grado.id_requisito_grado;


--
-- Name: requisito_inscripcion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.requisito_inscripcion (
    id_requisito_inscripcion integer NOT NULL,
    descripcion character varying(45) NOT NULL,
    estatus character varying(45),
    id_programa integer NOT NULL
);


ALTER TABLE public.requisito_inscripcion OWNER TO postgres;

--
-- Name: requisito_inscripcion_id_requisito_inscripcion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.requisito_inscripcion_id_requisito_inscripcion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.requisito_inscripcion_id_requisito_inscripcion_seq OWNER TO postgres;

--
-- Name: requisito_inscripcion_id_requisito_inscripcion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.requisito_inscripcion_id_requisito_inscripcion_seq OWNED BY public.requisito_inscripcion.id_requisito_inscripcion;


--
-- Name: rol; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.rol (
    id_rol integer NOT NULL,
    descripcion character varying(45) NOT NULL
);


ALTER TABLE public.rol OWNER TO postgres;

--
-- Name: rol_id_rol_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.rol_id_rol_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rol_id_rol_seq OWNER TO postgres;

--
-- Name: rol_id_rol_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.rol_id_rol_seq OWNED BY public.rol.id_rol;


--
-- Name: seccion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.seccion (
    id_seccion integer NOT NULL,
    descripcion character varying(45) NOT NULL,
    id_trayecto integer NOT NULL,
    id_tramo integer NOT NULL,
    id_oferta_academica integer NOT NULL,
    cupo integer NOT NULL
);


ALTER TABLE public.seccion OWNER TO postgres;

--
-- Name: seccion_id_seccion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.seccion_id_seccion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seccion_id_seccion_seq OWNER TO postgres;

--
-- Name: seccion_id_seccion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.seccion_id_seccion_seq OWNED BY public.seccion.id_seccion;


--
-- Name: sno_familia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sno_familia (
    id_sno_familia integer NOT NULL,
    nacionalidad character varying(15) NOT NULL,
    pnombre character varying(20) NOT NULL,
    snombre character varying(20),
    papellido character varying(20) NOT NULL,
    sapellido character varying(20),
    parentesco character varying(20) NOT NULL,
    fecha_nac date NOT NULL,
    sexfam character varying(1),
    estado_civil character varying(10),
    codest character varying(3) NOT NULL,
    ciudad_residencia character varying(50),
    direccion text,
    telefono character varying(20),
    correo character varying(100),
    cedper character varying(10) NOT NULL,
    tipo_trabajador character varying(100) NOT NULL,
    cedfam integer NOT NULL,
    estatus character varying(3),
    observacion character varying(255)
);


ALTER TABLE public.sno_familia OWNER TO postgres;

--
-- Name: sno_familia_id_sno_familia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sno_familia_id_sno_familia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sno_familia_id_sno_familia_seq OWNER TO postgres;

--
-- Name: sno_familia_id_sno_familia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sno_familia_id_sno_familia_seq OWNED BY public.sno_familia.id_sno_familia;


--
-- Name: solicitud_acta_pida; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.solicitud_acta_pida (
    id_solicitud_acta_pida integer NOT NULL,
    id_documento integer NOT NULL
);


ALTER TABLE public.solicitud_acta_pida OWNER TO postgres;

--
-- Name: solicitud_acta_pida_id_solicitud_acta_pida_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.solicitud_acta_pida_id_solicitud_acta_pida_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.solicitud_acta_pida_id_solicitud_acta_pida_seq OWNER TO postgres;

--
-- Name: solicitud_acta_pida_id_solicitud_acta_pida_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.solicitud_acta_pida_id_solicitud_acta_pida_seq OWNED BY public.solicitud_acta_pida.id_solicitud_acta_pida;


--
-- Name: solicitud_acta_proyecto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.solicitud_acta_proyecto (
    id_solicitud_acta_proyecto integer NOT NULL,
    id_documento integer NOT NULL
);


ALTER TABLE public.solicitud_acta_proyecto OWNER TO postgres;

--
-- Name: solicitud_acta_proyecto_id_solicitud_acta_proyecto_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.solicitud_acta_proyecto_id_solicitud_acta_proyecto_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.solicitud_acta_proyecto_id_solicitud_acta_proyecto_seq OWNER TO postgres;

--
-- Name: solicitud_acta_proyecto_id_solicitud_acta_proyecto_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.solicitud_acta_proyecto_id_solicitud_acta_proyecto_seq OWNED BY public.solicitud_acta_proyecto.id_solicitud_acta_proyecto;


--
-- Name: solicitud_ascenso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.solicitud_ascenso (
    id_solicitud_ascenso integer NOT NULL,
    id_escalafon integer NOT NULL,
    id_estatus_ascenso integer NOT NULL,
    id_docente integer NOT NULL,
    id_trabajo_ascenso integer NOT NULL,
    fecha_solicitud date NOT NULL,
    id_concurso integer NOT NULL,
    id_solicitud_acta_pida integer NOT NULL,
    id_solicitud_acta_proyecto integer NOT NULL,
    ciudad character varying(80) NOT NULL,
    fecha_consulta date NOT NULL
);


ALTER TABLE public.solicitud_ascenso OWNER TO postgres;

--
-- Name: solicitud_ascenso_id_solicitud_ascenso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.solicitud_ascenso_id_solicitud_ascenso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.solicitud_ascenso_id_solicitud_ascenso_seq OWNER TO postgres;

--
-- Name: solicitud_ascenso_id_solicitud_ascenso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.solicitud_ascenso_id_solicitud_ascenso_seq OWNED BY public.solicitud_ascenso.id_solicitud_ascenso;


--
-- Name: solicitud_escalafon_siguiente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.solicitud_escalafon_siguiente (
    id_solicitud_escalafon_siguiente integer NOT NULL,
    id_solicitud_ascenso integer NOT NULL,
    id_escalafon integer NOT NULL
);


ALTER TABLE public.solicitud_escalafon_siguiente OWNER TO postgres;

--
-- Name: solicitud_escalafon_siguiente_id_solicitud_escalafon_siguie_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.solicitud_escalafon_siguiente_id_solicitud_escalafon_siguie_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.solicitud_escalafon_siguiente_id_solicitud_escalafon_siguie_seq OWNER TO postgres;

--
-- Name: solicitud_escalafon_siguiente_id_solicitud_escalafon_siguie_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.solicitud_escalafon_siguiente_id_solicitud_escalafon_siguie_seq OWNED BY public.solicitud_escalafon_siguiente.id_solicitud_escalafon_siguiente;


--
-- Name: solicitud_resolucion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.solicitud_resolucion (
    id_solicitud_resolucion integer NOT NULL,
    id_solicitud_ascenso integer NOT NULL,
    id_documento integer NOT NULL
);


ALTER TABLE public.solicitud_resolucion OWNER TO postgres;

--
-- Name: solicitud_resolucion_id_solicitud_resolucion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.solicitud_resolucion_id_solicitud_resolucion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.solicitud_resolucion_id_solicitud_resolucion_seq OWNER TO postgres;

--
-- Name: solicitud_resolucion_id_solicitud_resolucion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.solicitud_resolucion_id_solicitud_resolucion_seq OWNED BY public.solicitud_resolucion.id_solicitud_resolucion;


--
-- Name: telefono_codigo_area; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.telefono_codigo_area (
    id_telefono_codigo_area integer NOT NULL,
    descripcion character varying(10) NOT NULL
);


ALTER TABLE public.telefono_codigo_area OWNER TO postgres;

--
-- Name: telefono_codigo_area_id_telefono_codigo_area_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.telefono_codigo_area_id_telefono_codigo_area_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.telefono_codigo_area_id_telefono_codigo_area_seq OWNER TO postgres;

--
-- Name: telefono_codigo_area_id_telefono_codigo_area_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.telefono_codigo_area_id_telefono_codigo_area_seq OWNED BY public.telefono_codigo_area.id_telefono_codigo_area;


--
-- Name: telefono_tipo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.telefono_tipo (
    id_telefono_tipo integer NOT NULL,
    descripcion character varying(10) NOT NULL
);


ALTER TABLE public.telefono_tipo OWNER TO postgres;

--
-- Name: telefono_tipo_id_telefono_tipo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.telefono_tipo_id_telefono_tipo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.telefono_tipo_id_telefono_tipo_seq OWNER TO postgres;

--
-- Name: telefono_tipo_id_telefono_tipo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.telefono_tipo_id_telefono_tipo_seq OWNED BY public.telefono_tipo.id_telefono_tipo;


--
-- Name: tipo_activacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_activacion (
    id_tipo_activacion integer NOT NULL,
    descripcion character varying(45) NOT NULL
);


ALTER TABLE public.tipo_activacion OWNER TO postgres;

--
-- Name: tipo_activacion_id_tipo_activacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_activacion_id_tipo_activacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_activacion_id_tipo_activacion_seq OWNER TO postgres;

--
-- Name: tipo_activacion_id_tipo_activacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_activacion_id_tipo_activacion_seq OWNED BY public.tipo_activacion.id_tipo_activacion;


--
-- Name: tipo_archivo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_archivo (
    id_tipo_archivo integer NOT NULL,
    descripcion character varying(45) NOT NULL
);


ALTER TABLE public.tipo_archivo OWNER TO postgres;

--
-- Name: tipo_archivo_id_tipo_archivo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_archivo_id_tipo_archivo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_archivo_id_tipo_archivo_seq OWNER TO postgres;

--
-- Name: tipo_archivo_id_tipo_archivo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_archivo_id_tipo_archivo_seq OWNED BY public.tipo_archivo.id_tipo_archivo;


--
-- Name: tipo_discapacidad; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_discapacidad (
    id_tipo_discapacidad integer NOT NULL,
    descripcion character varying(45)
);


ALTER TABLE public.tipo_discapacidad OWNER TO postgres;

--
-- Name: tipo_discapacidad_id_tipo_discapacidad_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_discapacidad_id_tipo_discapacidad_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_discapacidad_id_tipo_discapacidad_seq OWNER TO postgres;

--
-- Name: tipo_discapacidad_id_tipo_discapacidad_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_discapacidad_id_tipo_discapacidad_seq OWNED BY public.tipo_discapacidad.id_tipo_discapacidad;


--
-- Name: tipo_documento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_documento (
    id_tipo_documento integer NOT NULL,
    descripcion character varying(45) NOT NULL
);


ALTER TABLE public.tipo_documento OWNER TO postgres;

--
-- Name: tipo_documento_id_tipo_documento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_documento_id_tipo_documento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_documento_id_tipo_documento_seq OWNER TO postgres;

--
-- Name: tipo_documento_id_tipo_documento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_documento_id_tipo_documento_seq OWNED BY public.tipo_documento.id_tipo_documento;


--
-- Name: tipo_estudio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_estudio (
    id_tipo_estudio integer NOT NULL,
    descripcion character varying(45) NOT NULL
);


ALTER TABLE public.tipo_estudio OWNER TO postgres;

--
-- Name: tipo_estudio_id_tipo_estudio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_estudio_id_tipo_estudio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_estudio_id_tipo_estudio_seq OWNER TO postgres;

--
-- Name: tipo_estudio_id_tipo_estudio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_estudio_id_tipo_estudio_seq OWNED BY public.tipo_estudio.id_tipo_estudio;


--
-- Name: tipo_experiencia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_experiencia (
    id_tipo_experiencia integer NOT NULL,
    descripcion character varying(45) NOT NULL
);


ALTER TABLE public.tipo_experiencia OWNER TO postgres;

--
-- Name: tipo_experiencia_id_tipo_experiencia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_experiencia_id_tipo_experiencia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_experiencia_id_tipo_experiencia_seq OWNER TO postgres;

--
-- Name: tipo_experiencia_id_tipo_experiencia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_experiencia_id_tipo_experiencia_seq OWNED BY public.tipo_experiencia.id_tipo_experiencia;


--
-- Name: tipo_info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_info (
    id_tipo_info integer NOT NULL,
    descripcion character varying(45) NOT NULL
);


ALTER TABLE public.tipo_info OWNER TO postgres;

--
-- Name: tipo_info_id_tipo_info_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_info_id_tipo_info_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_info_id_tipo_info_seq OWNER TO postgres;

--
-- Name: tipo_info_id_tipo_info_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_info_id_tipo_info_seq OWNED BY public.tipo_info.id_tipo_info;


--
-- Name: tipo_jurado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_jurado (
    id_tipo_jurado integer NOT NULL,
    descripcion character varying(45) NOT NULL
);


ALTER TABLE public.tipo_jurado OWNER TO postgres;

--
-- Name: tipo_jurado_id_tipo_jurado_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_jurado_id_tipo_jurado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_jurado_id_tipo_jurado_seq OWNER TO postgres;

--
-- Name: tipo_jurado_id_tipo_jurado_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_jurado_id_tipo_jurado_seq OWNED BY public.tipo_jurado.id_tipo_jurado;


--
-- Name: tipo_metodologia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_metodologia (
    id_tipo_metodologia integer NOT NULL,
    descripcion character varying(45) NOT NULL
);


ALTER TABLE public.tipo_metodologia OWNER TO postgres;

--
-- Name: tipo_metodologia_id_tipo_metodologia_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_metodologia_id_tipo_metodologia_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_metodologia_id_tipo_metodologia_seq OWNER TO postgres;

--
-- Name: tipo_metodologia_id_tipo_metodologia_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_metodologia_id_tipo_metodologia_seq OWNED BY public.tipo_metodologia.id_tipo_metodologia;


--
-- Name: tipo_reconocimiento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_reconocimiento (
    id_tipo_reconocimiento integer NOT NULL,
    descripcion character varying(80) NOT NULL
);


ALTER TABLE public.tipo_reconocimiento OWNER TO postgres;

--
-- Name: tipo_reconocimiento_id_tipo_reconocimiento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_reconocimiento_id_tipo_reconocimiento_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_reconocimiento_id_tipo_reconocimiento_seq OWNER TO postgres;

--
-- Name: tipo_reconocimiento_id_tipo_reconocimiento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_reconocimiento_id_tipo_reconocimiento_seq OWNED BY public.tipo_reconocimiento.id_tipo_reconocimiento;


--
-- Name: trabajo_administrativo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.trabajo_administrativo (
    id_trabajo_administrativo integer NOT NULL,
    periodo character varying(45) NOT NULL,
    id_experiencia_docente integer NOT NULL
);


ALTER TABLE public.trabajo_administrativo OWNER TO postgres;

--
-- Name: trabajo_administrativo_id_trabajo_administrativo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.trabajo_administrativo_id_trabajo_administrativo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.trabajo_administrativo_id_trabajo_administrativo_seq OWNER TO postgres;

--
-- Name: trabajo_administrativo_id_trabajo_administrativo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.trabajo_administrativo_id_trabajo_administrativo_seq OWNED BY public.trabajo_administrativo.id_trabajo_administrativo;


--
-- Name: trabajo_ascenso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.trabajo_ascenso (
    id_trabajo_ascenso integer NOT NULL,
    titulo text NOT NULL,
    id_documento integer NOT NULL
);


ALTER TABLE public.trabajo_ascenso OWNER TO postgres;

--
-- Name: trabajo_ascenso_id_trabajo_ascenso_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.trabajo_ascenso_id_trabajo_ascenso_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.trabajo_ascenso_id_trabajo_ascenso_seq OWNER TO postgres;

--
-- Name: trabajo_ascenso_id_trabajo_ascenso_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.trabajo_ascenso_id_trabajo_ascenso_seq OWNED BY public.trabajo_ascenso.id_trabajo_ascenso;


--
-- Name: trabajo_grado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.trabajo_grado (
    id_trabajo_grado integer NOT NULL,
    titulo character varying(120) NOT NULL,
    resumen text NOT NULL,
    id_estudio_conducente integer NOT NULL
);


ALTER TABLE public.trabajo_grado OWNER TO postgres;

--
-- Name: trabajo_grado_id_trabajo_grado_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.trabajo_grado_id_trabajo_grado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.trabajo_grado_id_trabajo_grado_seq OWNER TO postgres;

--
-- Name: trabajo_grado_id_trabajo_grado_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.trabajo_grado_id_trabajo_grado_seq OWNED BY public.trabajo_grado.id_trabajo_grado;


--
-- Name: tramo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tramo (
    id_tramo integer NOT NULL,
    descripcion character varying(70) NOT NULL,
    codigo character varying(15) NOT NULL
);


ALTER TABLE public.tramo OWNER TO postgres;

--
-- Name: tramo_id_tramo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tramo_id_tramo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tramo_id_tramo_seq OWNER TO postgres;

--
-- Name: tramo_id_tramo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tramo_id_tramo_seq OWNED BY public.tramo.id_tramo;


--
-- Name: trayecto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.trayecto (
    id_trayecto integer NOT NULL,
    descripcion character varying(70) NOT NULL,
    codigo character varying(15) NOT NULL
);


ALTER TABLE public.trayecto OWNER TO postgres;

--
-- Name: trayecto_id_trayecto_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.trayecto_id_trayecto_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.trayecto_id_trayecto_seq OWNER TO postgres;

--
-- Name: trayecto_id_trayecto_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.trayecto_id_trayecto_seq OWNED BY public.trayecto.id_trayecto;


--
-- Name: turno; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.turno (
    id_turno integer NOT NULL,
    descripcion character varying(70) NOT NULL
);


ALTER TABLE public.turno OWNER TO postgres;

--
-- Name: turno_id_turno_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.turno_id_turno_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.turno_id_turno_seq OWNER TO postgres;

--
-- Name: turno_id_turno_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.turno_id_turno_seq OWNED BY public.turno.id_turno;


--
-- Name: tutor_estudios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tutor_estudios (
    id_tutor_estudios integer NOT NULL,
    "año_grado" character varying(45) NOT NULL,
    id_institucion_educativa integer NOT NULL,
    id_grado_academico integer NOT NULL,
    tutor_tipo character varying(45) NOT NULL
);


ALTER TABLE public.tutor_estudios OWNER TO postgres;

--
-- Name: tutor_estudios_id_tutor_estudios_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tutor_estudios_id_tutor_estudios_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tutor_estudios_id_tutor_estudios_seq OWNER TO postgres;

--
-- Name: tutor_estudios_id_tutor_estudios_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tutor_estudios_id_tutor_estudios_seq OWNED BY public.tutor_estudios.id_tutor_estudios;


--
-- Name: tutoria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tutoria (
    id_tutoria integer NOT NULL,
    ano_tutoria integer NOT NULL,
    periodo_lectivo character varying(45) NOT NULL,
    estatus_verificacion character varying(45) NOT NULL,
    id_docente integer NOT NULL,
    id_area_conocimiento_ubv integer NOT NULL,
    id_linea_investigacion integer NOT NULL,
    id_nucleo_academico integer NOT NULL,
    id_centro_estudio integer NOT NULL,
    id_programa integer NOT NULL,
    id_eje_regional integer NOT NULL,
    nro_estudiantes integer NOT NULL,
    descripcion character varying(20) NOT NULL
);


ALTER TABLE public.tutoria OWNER TO postgres;

--
-- Name: tutoria_id_tutoria_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tutoria_id_tutoria_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tutoria_id_tutoria_seq OWNER TO postgres;

--
-- Name: tutoria_id_tutoria_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tutoria_id_tutoria_seq OWNED BY public.tutoria.id_tutoria;


--
-- Name: unidad_administrativa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.unidad_administrativa (
    id_unidad_administrativa integer NOT NULL,
    descripcion character varying(70) NOT NULL,
    id_aldea integer NOT NULL
);


ALTER TABLE public.unidad_administrativa OWNER TO postgres;

--
-- Name: unidad_administrativa_id_unidad_administrativa_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.unidad_administrativa_id_unidad_administrativa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.unidad_administrativa_id_unidad_administrativa_seq OWNER TO postgres;

--
-- Name: unidad_administrativa_id_unidad_administrativa_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.unidad_administrativa_id_unidad_administrativa_seq OWNED BY public.unidad_administrativa.id_unidad_administrativa;


--
-- Name: unidad_administrativa_telefono; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.unidad_administrativa_telefono (
    id_unidad_administrativa_telefono integer NOT NULL,
    descripcion character varying(45) NOT NULL,
    id_unidad_administrativa integer NOT NULL,
    id_telefono_codigo_area integer NOT NULL
);


ALTER TABLE public.unidad_administrativa_telefono OWNER TO postgres;

--
-- Name: unidad_administrativa_telefon_id_unidad_administrativa_tele_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.unidad_administrativa_telefon_id_unidad_administrativa_tele_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.unidad_administrativa_telefon_id_unidad_administrativa_tele_seq OWNER TO postgres;

--
-- Name: unidad_administrativa_telefon_id_unidad_administrativa_tele_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.unidad_administrativa_telefon_id_unidad_administrativa_tele_seq OWNED BY public.unidad_administrativa_telefono.id_unidad_administrativa_telefono;


--
-- Name: unidad_curricular; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.unidad_curricular (
    id_unidad_curricular integer NOT NULL,
    descripcion character varying(70) NOT NULL,
    estatus character varying(20) NOT NULL
);


ALTER TABLE public.unidad_curricular OWNER TO postgres;

--
-- Name: unidad_curricular_detalle; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.unidad_curricular_detalle (
    id_unidad_curricular_detalle integer NOT NULL,
    num_horas_aula integer NOT NULL,
    num_horas_lab integer,
    unidades_credito integer,
    id_unidad_curricular integer NOT NULL,
    id_unidad_curricular_tipo integer NOT NULL,
    id_eje_formacion integer NOT NULL,
    id_frecuencia integer NOT NULL,
    codigo character varying(5) NOT NULL
);


ALTER TABLE public.unidad_curricular_detalle OWNER TO postgres;

--
-- Name: unidad_curricular_detalle_id_unidad_curricular_detalle_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.unidad_curricular_detalle_id_unidad_curricular_detalle_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.unidad_curricular_detalle_id_unidad_curricular_detalle_seq OWNER TO postgres;

--
-- Name: unidad_curricular_detalle_id_unidad_curricular_detalle_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.unidad_curricular_detalle_id_unidad_curricular_detalle_seq OWNED BY public.unidad_curricular_detalle.id_unidad_curricular_detalle;


--
-- Name: unidad_curricular_id_unidad_curricular_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.unidad_curricular_id_unidad_curricular_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.unidad_curricular_id_unidad_curricular_seq OWNER TO postgres;

--
-- Name: unidad_curricular_id_unidad_curricular_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.unidad_curricular_id_unidad_curricular_seq OWNED BY public.unidad_curricular.id_unidad_curricular;


--
-- Name: unidad_curricular_tipo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.unidad_curricular_tipo (
    id_unidad_curricular_tipo integer NOT NULL,
    descripcion character varying(70) NOT NULL
);


ALTER TABLE public.unidad_curricular_tipo OWNER TO postgres;

--
-- Name: unidad_curricular_tipo_id_unidad_curricular_tipo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.unidad_curricular_tipo_id_unidad_curricular_tipo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.unidad_curricular_tipo_id_unidad_curricular_tipo_seq OWNER TO postgres;

--
-- Name: unidad_curricular_tipo_id_unidad_curricular_tipo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.unidad_curricular_tipo_id_unidad_curricular_tipo_seq OWNED BY public.unidad_curricular_tipo.id_unidad_curricular_tipo;


--
-- Name: unidad_pago; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.unidad_pago (
    id_unidad_pago integer NOT NULL,
    monto character varying(100),
    estatus character varying(2),
    fecha_registro character varying(45),
    descripcion character varying(45),
    hora_registro character varying(45)
);


ALTER TABLE public.unidad_pago OWNER TO postgres;

--
-- Name: unidad_de_pago_id_unidad_de_pago_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.unidad_de_pago_id_unidad_de_pago_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.unidad_de_pago_id_unidad_de_pago_seq OWNER TO postgres;

--
-- Name: unidad_de_pago_id_unidad_de_pago_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.unidad_de_pago_id_unidad_de_pago_seq OWNED BY public.unidad_pago.id_unidad_pago;


--
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuario (
    id_usuario integer NOT NULL,
    id_persona integer NOT NULL,
    usuario character varying(45),
    password character varying(45),
    fecha date,
    estatus character varying(45),
    id_perfil integer NOT NULL
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- Name: usuario_id_usuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuario_id_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_id_usuario_seq OWNER TO postgres;

--
-- Name: usuario_id_usuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuario_id_usuario_seq OWNED BY public.usuario.id_usuario;


--
-- Name: usuario_pregunta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuario_pregunta (
    id_usuario_pregunta integer NOT NULL,
    id_pregunta integer NOT NULL,
    respuesta character varying(80),
    id_usuario integer NOT NULL
);


ALTER TABLE public.usuario_pregunta OWNER TO postgres;

--
-- Name: usuario_pregunta_id_usuario_pregunta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuario_pregunta_id_usuario_pregunta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_pregunta_id_usuario_pregunta_seq OWNER TO postgres;

--
-- Name: usuario_pregunta_id_usuario_pregunta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuario_pregunta_id_usuario_pregunta_seq OWNED BY public.usuario_pregunta.id_usuario_pregunta;


--
-- Name: usuario_rol; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuario_rol (
    id_usuario_rol integer NOT NULL,
    id_usuario integer NOT NULL,
    id_rol integer NOT NULL
);


ALTER TABLE public.usuario_rol OWNER TO postgres;

--
-- Name: usuario_rol_id_usuario_rol_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuario_rol_id_usuario_rol_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_rol_id_usuario_rol_seq OWNER TO postgres;

--
-- Name: usuario_rol_id_usuario_rol_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuario_rol_id_usuario_rol_seq OWNED BY public.usuario_rol.id_usuario_rol;


--
-- Name: id_activacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.activacion ALTER COLUMN id_activacion SET DEFAULT nextval('public.activacion_id_activacion_seq'::regclass);


--
-- Name: id_aldea; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aldea ALTER COLUMN id_aldea SET DEFAULT nextval('public.aldea_id_aldea_seq'::regclass);


--
-- Name: id_aldea_malla_curricula; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aldea_malla_curricula ALTER COLUMN id_aldea_malla_curricula SET DEFAULT nextval('public.aldea_malla_curricula_id_aldea_malla_curricula_seq'::regclass);


--
-- Name: id_aldea_programa; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aldea_programa ALTER COLUMN id_aldea_programa SET DEFAULT nextval('public.aldea_programa_id_aldea_programa_seq'::regclass);


--
-- Name: id_aldea_telefono; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aldea_telefono ALTER COLUMN id_aldea_telefono SET DEFAULT nextval('public.aldea_telefono_id_aldea_telefono_seq'::regclass);


--
-- Name: id_aldea_tipo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aldea_tipo ALTER COLUMN id_aldea_tipo SET DEFAULT nextval('public.aldea_tipo_id_aldea_tipo_seq'::regclass);


--
-- Name: id_aldea_ubv; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aldea_ubv ALTER COLUMN id_aldea_ubv SET DEFAULT nextval('public.aldea_ubv_id_aldea_ubv_seq'::regclass);


--
-- Name: id_ambiente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ambiente ALTER COLUMN id_ambiente SET DEFAULT nextval('public.ambiente_id_ambiente_seq'::regclass);


--
-- Name: id_ambiente_detalle; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ambiente_detalle ALTER COLUMN id_ambiente_detalle SET DEFAULT nextval('public.ambiente_detalle_id_ambiente_detalle_seq'::regclass);


--
-- Name: id_ambiente_espacio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ambiente_espacio ALTER COLUMN id_ambiente_espacio SET DEFAULT nextval('public.ambiente_espacio_id_ambiente_espacio_seq'::regclass);


--
-- Name: id_antropometrico; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.antropometrico ALTER COLUMN id_antropometrico SET DEFAULT nextval('public.antropometrico_id_antropometrico_seq'::regclass);


--
-- Name: id_arancel; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arancel ALTER COLUMN id_arancel SET DEFAULT nextval('public.arancel_id_arancel_seq'::regclass);


--
-- Name: id_area_conocimiento_opsu; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.area_conocimiento_opsu ALTER COLUMN id_area_conocimiento_opsu SET DEFAULT nextval('public.area_conocimiento_opsu_id_area_conocimiento_opsu_seq'::regclass);


--
-- Name: id_area_conocimiento_ubv; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.area_conocimiento_ubv ALTER COLUMN id_area_conocimiento_ubv SET DEFAULT nextval('public.area_conocimiento_ubv_id_area_conocimiento_ubv_seq'::regclass);


--
-- Name: id_arte_software; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arte_software ALTER COLUMN id_arte_software SET DEFAULT nextval('public.arte_software_id_arte_software_seq'::regclass);


--
-- Name: id_articulo_revista; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.articulo_revista ALTER COLUMN id_articulo_revista SET DEFAULT nextval('public.articulo_revista_id_articulo_revista_seq'::regclass);


--
-- Name: id_ascenso_titular; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ascenso_titular ALTER COLUMN id_ascenso_titular SET DEFAULT nextval('public.ascenso_titular_id_ascenso_titular_seq'::regclass);


--
-- Name: id_aspirante; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aspirante ALTER COLUMN id_aspirante SET DEFAULT nextval('public.aspirante_id_aspirante_seq'::regclass);


--
-- Name: id_aspirante_estatus; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aspirante_estatus ALTER COLUMN id_aspirante_estatus SET DEFAULT nextval('public.aspirante_estatus_id_aspirante_estatus_seq'::regclass);


--
-- Name: id_aspirante_tipo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aspirante_tipo ALTER COLUMN id_aspirante_tipo SET DEFAULT nextval('public.aspirante_tipo_id_aspirante_tipo_seq'::regclass);


--
-- Name: id_banco; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.banco ALTER COLUMN id_banco SET DEFAULT nextval('public.banco_id_banco_seq'::regclass);


--
-- Name: id_cambiar_oferta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cambiar_oferta ALTER COLUMN id_cambiar_oferta SET DEFAULT nextval('public.cambiar_oferta_id_cambiar_oferta_seq'::regclass);


--
-- Name: id_carga_academica; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.carga_academica ALTER COLUMN id_carga_academica SET DEFAULT nextval('public.carga_academica_id_carga_academica_seq'::regclass);


--
-- Name: id_cargo_experiencia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cargo_experiencia ALTER COLUMN id_cargo_experiencia SET DEFAULT nextval('public.cargo_experiencia_id_cargo_experiencia_seq'::regclass);


--
-- Name: id_censo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.censo ALTER COLUMN id_censo SET DEFAULT nextval('public.censo_id_censo_seq'::regclass);


--
-- Name: id_centro_estudio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.centro_estudio ALTER COLUMN id_centro_estudio SET DEFAULT nextval('public.centro_estudio_id_centro_estudio_seq'::regclass);


--
-- Name: id_certificado_titulo_estudio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.certificado_titulo_estudio ALTER COLUMN id_certificado_titulo_estudio SET DEFAULT nextval('public.certificado_titulo_estudio_id_certificado_titulo_estudio_seq'::regclass);


--
-- Name: id_clasificacion_docente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clasificacion_docente ALTER COLUMN id_clasificacion_docente SET DEFAULT nextval('public.clasificacion_docente_id_clasificacion_docente_seq'::regclass);


--
-- Name: id_comision_excedencia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comision_excedencia ALTER COLUMN id_comision_excedencia SET DEFAULT nextval('public.comision_excedencia_id_comision_excedencia_seq'::regclass);


--
-- Name: id_comision_gubernamental; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comision_gubernamental ALTER COLUMN id_comision_gubernamental SET DEFAULT nextval('public.comision_gubernamental_id_comision_gubernamental_seq'::regclass);


--
-- Name: id_comite_tipo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comite_tipo ALTER COLUMN id_comite_tipo SET DEFAULT nextval('public.comite_tipo_id_comite_tipo_seq'::regclass);


--
-- Name: id_comitepfa_integrante; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comitepfa_integrante ALTER COLUMN id_comitepfa_integrante SET DEFAULT nextval('public.comitepfa_integrante_id_comitepfa_integrante_seq'::regclass);


--
-- Name: id_comitepfa_integrante_historico; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comitepfa_integrante_historico ALTER COLUMN id_comitepfa_integrante_historico SET DEFAULT nextval('public.comitepfa_integrante_historic_id_comitepfa_integrante_histo_seq'::regclass);


--
-- Name: id_concurso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.concurso ALTER COLUMN id_concurso SET DEFAULT nextval('public.concurso_id_concurso_seq'::regclass);


--
-- Name: id_constancia_administrativo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.constancia_administrativo ALTER COLUMN id_constancia_administrativo SET DEFAULT nextval('public.constancia_administrativo_id_constancia_administrativo_seq'::regclass);


--
-- Name: id_correo_tipo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.correo_tipo ALTER COLUMN id_correo_tipo SET DEFAULT nextval('public.correo_tipo_id_correo_tipo_seq'::regclass);


--
-- Name: id_cuenta_bancaria; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cuenta_bancaria ALTER COLUMN id_cuenta_bancaria SET DEFAULT nextval('public.cuenta_bancaria_id_cuenta_bancaria_seq'::regclass);


--
-- Name: id_cuenta_bancaria_tipo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cuenta_bancaria_tipo ALTER COLUMN id_cuenta_bancaria_tipo SET DEFAULT nextval('public.cuenta_bancaria_tipo_id_cuenta_bancaria_tipo_seq'::regclass);


--
-- Name: id_deposito; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.deposito ALTER COLUMN id_deposito SET DEFAULT nextval('public.deposito_id_deposito_seq'::regclass);


--
-- Name: id_dia_semana; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dia_semana ALTER COLUMN id_dia_semana SET DEFAULT nextval('public.dia_semana_id_dia_semana_seq'::regclass);


--
-- Name: id_discapacidad; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discapacidad ALTER COLUMN id_discapacidad SET DEFAULT nextval('public.discapacidad_id_discapacidad_seq'::regclass);


--
-- Name: id_diseno_uc; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.diseno_uc_docente ALTER COLUMN id_diseno_uc SET DEFAULT nextval('public.diseno_uc_docente_id_diseno_uc_seq'::regclass);


--
-- Name: id_docencia_previa; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docencia_previa ALTER COLUMN id_docencia_previa SET DEFAULT nextval('public.docencia_previa_id_docencia_previa_seq'::regclass);


--
-- Name: id_docencia_previa_ubv; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docencia_previa_ubv ALTER COLUMN id_docencia_previa_ubv SET DEFAULT nextval('public.docencia_previa_ubv_id_docencia_previa_ubv_seq'::regclass);


--
-- Name: id_docente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docente ALTER COLUMN id_docente SET DEFAULT nextval('public.docente_id_docente_seq'::regclass);


--
-- Name: id_docente_aldea; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docente_aldea ALTER COLUMN id_docente_aldea SET DEFAULT nextval('public.docente_aldea_id_docente_alde_seq'::regclass);


--
-- Name: id_docente_dedicacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docente_dedicacion ALTER COLUMN id_docente_dedicacion SET DEFAULT nextval('public.docente_dedicacion_id_docente_dedicacion_seq'::regclass);


--
-- Name: id_docente_estatus; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docente_estatus ALTER COLUMN id_docente_estatus SET DEFAULT nextval('public.docente_estatus_id_docente_estatus_seq'::regclass);


--
-- Name: id_docente_programa; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docente_programa ALTER COLUMN id_docente_programa SET DEFAULT nextval('public.docente_programa_id_docente_programa_seq'::regclass);


--
-- Name: id_documento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documento ALTER COLUMN id_documento SET DEFAULT nextval('public.documento_id_documento_seq'::regclass);


--
-- Name: id_documento_identidad_tipo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documento_identidad_tipo ALTER COLUMN id_documento_identidad_tipo SET DEFAULT nextval('public.documento_identidad_tipo_id_documento_identidad_tipo_seq'::regclass);


--
-- Name: id_documento_solicitud; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documento_solicitud ALTER COLUMN id_documento_solicitud SET DEFAULT nextval('public.documento_solicitud_id_documento_solicitud_seq'::regclass);


--
-- Name: id_domicilio_detalle_tipo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.domicilio_detalle_tipo ALTER COLUMN id_domicilio_detalle_tipo SET DEFAULT nextval('public.domicilio_detalle_tipo_id_domicilio_detalle_tipo_seq'::regclass);


--
-- Name: id_domicilio_persona; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.domicilio_persona ALTER COLUMN id_domicilio_persona SET DEFAULT nextval('public.domicilio_persona_id_domicilio_persona_seq'::regclass);


--
-- Name: id_eje_formacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.eje_formacion ALTER COLUMN id_eje_formacion SET DEFAULT nextval('public.eje_formacion_id_eje_formacion_seq'::regclass);


--
-- Name: id_eje_municipal; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.eje_municipal ALTER COLUMN id_eje_municipal SET DEFAULT nextval('public.eje_municipal_id_eje_municipal_seq'::regclass);


--
-- Name: id_eje_regional; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.eje_regional ALTER COLUMN id_eje_regional SET DEFAULT nextval('public.eje_regional_id_eje_regional_seq'::regclass);


--
-- Name: id_escalafon; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.escalafon ALTER COLUMN id_escalafon SET DEFAULT nextval('public.escalafon_id_escalafon_seq'::regclass);


--
-- Name: id_espacio_tipo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.espacio_tipo ALTER COLUMN id_espacio_tipo SET DEFAULT nextval('public.espacio_tipo_id_espacio_tipo_seq'::regclass);


--
-- Name: id_estado; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estado ALTER COLUMN id_estado SET DEFAULT nextval('public.estado_id_estado_seq'::regclass);


--
-- Name: id_estado_civil; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estado_civil ALTER COLUMN id_estado_civil SET DEFAULT nextval('public.estado_civil_id_estado_civil_seq'::regclass);


--
-- Name: id_estatus_ascenso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estatus_ascenso ALTER COLUMN id_estatus_ascenso SET DEFAULT nextval('public.estatus_ascenso_id_estatus_ascenso_seq'::regclass);


--
-- Name: id_estudiante; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudiante ALTER COLUMN id_estudiante SET DEFAULT nextval('public.estudiante_id_estudiante_seq'::regclass);


--
-- Name: id_estudiante_detalle; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudiante_detalle ALTER COLUMN id_estudiante_detalle SET DEFAULT nextval('public.estudiante_detalle_id_estudiante_detalle_seq'::regclass);


--
-- Name: id_estudiante_documento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudiante_documento ALTER COLUMN id_estudiante_documento SET DEFAULT nextval('public.estudiante_documento_id_estudiante_documento_seq'::regclass);


--
-- Name: id_estudio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudio ALTER COLUMN id_estudio SET DEFAULT nextval('public.estudio_id_estudio_seq'::regclass);


--
-- Name: id_estudio_conducente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudio_conducente ALTER COLUMN id_estudio_conducente SET DEFAULT nextval('public.estudio_conducente_id_estudio_conducente_seq'::regclass);


--
-- Name: id_estudio_idioma; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudio_idioma ALTER COLUMN id_estudio_idioma SET DEFAULT nextval('public.idioma_id_idioma_seq'::regclass);


--
-- Name: id_estudio_nivel; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudio_nivel ALTER COLUMN id_estudio_nivel SET DEFAULT nextval('public.estudio_nivel_id_estudio_nivel_seq'::regclass);


--
-- Name: id_etnia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.etnia ALTER COLUMN id_etnia SET DEFAULT nextval('public.etnia_id_etnia_seq'::regclass);


--
-- Name: id_expediente_documento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.expediente_documento ALTER COLUMN id_expediente_documento SET DEFAULT nextval('public.expediente_documento_id_expediente_documento_seq'::regclass);


--
-- Name: id_experiencia_docente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.experiencia_docente ALTER COLUMN id_experiencia_docente SET DEFAULT nextval('public.experiencia_docente_id_experiencia_docente_seq'::regclass);


--
-- Name: id_fase_ii; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fase_ii ALTER COLUMN id_fase_ii SET DEFAULT nextval('public.fase_ii_id_fase_ii_seq'::regclass);


--
-- Name: id_genero; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.genero ALTER COLUMN id_genero SET DEFAULT nextval('public.genero_id_genero_seq'::regclass);


--
-- Name: id_grado_academico; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grado_academico ALTER COLUMN id_grado_academico SET DEFAULT nextval('public.grado_academico_id_grado_academico_seq'::regclass);


--
-- Name: id_historia_ascenso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historia_ascenso ALTER COLUMN id_historia_ascenso SET DEFAULT nextval('public.historia_ascenso_id_historia_ascenso_seq'::regclass);


--
-- Name: id_historia_jurado; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historia_jurado ALTER COLUMN id_historia_jurado SET DEFAULT nextval('public.historia_jurado_id_historia_jurado_seq'::regclass);


--
-- Name: id_horario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.horario ALTER COLUMN id_horario SET DEFAULT nextval('public.horario_id_horario_seq'::regclass);


--
-- Name: id_idioma; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.idioma ALTER COLUMN id_idioma SET DEFAULT nextval('public.idioma_id_idioma_seq1'::regclass);


--
-- Name: id_ins_uni_curricular; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ins_uni_curricular ALTER COLUMN id_ins_uni_curricular SET DEFAULT nextval('public.ins_uni_curricular_id_ins_uni_curricular_seq'::regclass);


--
-- Name: id_inscripcion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inscripcion ALTER COLUMN id_inscripcion SET DEFAULT nextval('public.inscripcion_id_inscripcion_seq'::regclass);


--
-- Name: id_inscripcion_detalle; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inscripcion_detalle ALTER COLUMN id_inscripcion_detalle SET DEFAULT nextval('public.inscripcion_detalle_id_inscripcion_detalle_seq'::regclass);


--
-- Name: id_institucion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.institucion ALTER COLUMN id_institucion SET DEFAULT nextval('public.institucion_id_institucion_seq'::regclass);


--
-- Name: id_institucion_educativa; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.institucion_educativa ALTER COLUMN id_institucion_educativa SET DEFAULT nextval('public.institucion_educativa_id_institucion_educativa_seq'::regclass);


--
-- Name: id_institucion_tipo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.institucion_tipo ALTER COLUMN id_institucion_tipo SET DEFAULT nextval('public.institucion_tipo_id_institucion_tipo_seq'::regclass);


--
-- Name: id_jurado; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jurado ALTER COLUMN id_jurado SET DEFAULT nextval('public.jurado_id_jurado_seq'::regclass);


--
-- Name: id_libro; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.libro ALTER COLUMN id_libro SET DEFAULT nextval('public.libro_id_libro_seq'::regclass);


--
-- Name: id_linea_investigacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.linea_investigacion ALTER COLUMN id_linea_investigacion SET DEFAULT nextval('public.linea_investigacion_id_linea_investigacion_seq'::regclass);


--
-- Name: id_malla_curricular; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.malla_curricular ALTER COLUMN id_malla_curricular SET DEFAULT nextval('public.malla_curricular_id_malla_curricular_seq'::regclass);


--
-- Name: id_malla_curricular_detalle; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.malla_curricular_detalle ALTER COLUMN id_malla_curricular_detalle SET DEFAULT nextval('public.malla_curricular_detalle_id_malla_curricular_detalle_seq'::regclass);


--
-- Name: id_memo_solicitud; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.memo_solicitud ALTER COLUMN id_memo_solicitud SET DEFAULT nextval('public.memo_solicitud_id_memo_solicitud_seq'::regclass);


--
-- Name: id_metodologia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.metodologia ALTER COLUMN id_metodologia SET DEFAULT nextval('public.metodologia_id_metodologia_seq'::regclass);


--
-- Name: id_modalidad_estudio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.modalidad_estudio ALTER COLUMN id_modalidad_estudio SET DEFAULT nextval('public.modalidad_estudio_id_modalidad_estudio_seq'::regclass);


--
-- Name: id_modalidad_malla; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.modalidad_malla ALTER COLUMN id_modalidad_malla SET DEFAULT nextval('public.modalidad_malla_id_modalidad_malla_seq'::regclass);


--
-- Name: id_modulo_activacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.modulo_activacion ALTER COLUMN id_modulo_activacion SET DEFAULT nextval('public.modulo_activacion_ingreso_id_modulo_activacion_ingreso_seq'::regclass);


--
-- Name: id_municipio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.municipio ALTER COLUMN id_municipio SET DEFAULT nextval('public.municipio_id_municipio_seq'::regclass);


--
-- Name: id_nacionalidad; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nacionalidad ALTER COLUMN id_nacionalidad SET DEFAULT nextval('public.nacionalidad_id_nacionalidad_seq'::regclass);


--
-- Name: id_nivel_academico; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nivel_academico ALTER COLUMN id_nivel_academico SET DEFAULT nextval('public.nivel_academico_id_nivel_academico_seq'::regclass);


--
-- Name: id_nomina_docente_sigad; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nomina_docente_sigad ALTER COLUMN id_nomina_docente_sigad SET DEFAULT nextval('public.nomina_docente_sigad_id_nomina_docente_sigad_seq'::regclass);


--
-- Name: id_noticia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.noticia ALTER COLUMN id_noticia SET DEFAULT nextval('public.noticia_id_noticia_seq'::regclass);


--
-- Name: id_nucleo_academico; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nucleo_academico ALTER COLUMN id_nucleo_academico SET DEFAULT nextval('public.nucleo_academico_id_nucleo_academico_seq'::regclass);


--
-- Name: id_oferta_academica; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oferta_academica ALTER COLUMN id_oferta_academica SET DEFAULT nextval('public.oferta_academica_id_oferta_academica_seq'::regclass);


--
-- Name: id_oferta_academica_detalle; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oferta_academica_detalle ALTER COLUMN id_oferta_academica_detalle SET DEFAULT nextval('public.oferta_academica_detalle_id_oferta_academica_detalle_seq'::regclass);


--
-- Name: id_pais; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pais ALTER COLUMN id_pais SET DEFAULT nextval('public.pais_id_pais_seq'::regclass);


--
-- Name: id_parroquia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parroquia ALTER COLUMN id_parroquia SET DEFAULT nextval('public.parroquia_id_parroquia_seq'::regclass);


--
-- Name: id_perfil; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.perfil ALTER COLUMN id_perfil SET DEFAULT nextval('public.perfil_id_perfil_seq'::regclass);


--
-- Name: id_periodo_academico; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.periodo_academico ALTER COLUMN id_periodo_academico SET DEFAULT nextval('public.periodo_academico_id_periodo_academico_seq'::regclass);


--
-- Name: id_persona; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona ALTER COLUMN id_persona SET DEFAULT nextval('public.persona_id_persona_seq'::regclass);


--
-- Name: id_persona_correo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_correo ALTER COLUMN id_persona_correo SET DEFAULT nextval('public.persona_correo_id_persona_correo_seq'::regclass);


--
-- Name: id_persona_discapacidad; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_discapacidad ALTER COLUMN id_persona_discapacidad SET DEFAULT nextval('public.persona_discapacidad_id_persona_discapacidad_seq'::regclass);


--
-- Name: id_persona_etnia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_etnia ALTER COLUMN id_persona_etnia SET DEFAULT nextval('public.persona_etnia_id_persona_etnia_seq'::regclass);


--
-- Name: id_persona_fecha_ingreso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_fecha_ingreso ALTER COLUMN id_persona_fecha_ingreso SET DEFAULT nextval('public.persona_fecha_ingreso_id_persona_fecha_ingreso_seq'::regclass);


--
-- Name: id_persona_hijo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_hijo ALTER COLUMN id_persona_hijo SET DEFAULT nextval('public.persona_hijo_id_persona_hijo_seq'::regclass);


--
-- Name: id_persona_nacionalidad; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_nacionalidad ALTER COLUMN id_persona_nacionalidad SET DEFAULT nextval('public.persona_nacionalidad_id_persona_nacionalidad_seq'::regclass);


--
-- Name: id_persona_telefono; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_telefono ALTER COLUMN id_persona_telefono SET DEFAULT nextval('public.persona_telefono_id_persona_telefono_seq'::regclass);


--
-- Name: id_personal_administrativo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.personal_administrativo ALTER COLUMN id_personal_administrativo SET DEFAULT nextval('public.personal_administrativo_id_personal_administrativo_seq'::regclass);


--
-- Name: id_ponencia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ponencia ALTER COLUMN id_ponencia SET DEFAULT nextval('public.ponencia_id_ponencia_seq'::regclass);


--
-- Name: id_pregunta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pregunta ALTER COLUMN id_pregunta SET DEFAULT nextval('public.pregunta_id_pregunta_seq'::regclass);


--
-- Name: id_programa; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.programa ALTER COLUMN id_programa SET DEFAULT nextval('public.programa_id_programa_seq'::regclass);


--
-- Name: id_programa_nivel; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.programa_nivel ALTER COLUMN id_programa_nivel SET DEFAULT nextval('public.programa_nivel_id_programa_nivel_seq'::regclass);


--
-- Name: id_programa_tipo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.programa_tipo ALTER COLUMN id_programa_tipo SET DEFAULT nextval('public.programa_tipo_id_programa_tipo_seq'::regclass);


--
-- Name: id_propuesta_investigacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.propuesta_investigacion ALTER COLUMN id_propuesta_investigacion SET DEFAULT nextval('public.propuesta_investigacion_id_propuesta_investigacion_seq'::regclass);


--
-- Name: id_propuesta_jurado; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.propuesta_jurado ALTER COLUMN id_propuesta_jurado SET DEFAULT nextval('public.propuesta_jurado_id_propuesta_jurado_seq'::regclass);


--
-- Name: id_proyecto; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proyecto ALTER COLUMN id_proyecto SET DEFAULT nextval('public.proyecto_id_proyecto_seq'::regclass);


--
-- Name: id_reconocimiento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reconocimiento ALTER COLUMN id_reconocimiento SET DEFAULT nextval('public.reconocimiento_id_reconocimiento_seq'::regclass);


--
-- Name: id_reingreso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reingreso ALTER COLUMN id_reingreso SET DEFAULT nextval('public.reingreso_id_reingreso_seq'::regclass);


--
-- Name: id_requisito_grado; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.requisito_grado ALTER COLUMN id_requisito_grado SET DEFAULT nextval('public.requisito_grado_id_requisito_grado_seq'::regclass);


--
-- Name: id_requisito_inscripcion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.requisito_inscripcion ALTER COLUMN id_requisito_inscripcion SET DEFAULT nextval('public.requisito_inscripcion_id_requisito_inscripcion_seq'::regclass);


--
-- Name: id_rol; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rol ALTER COLUMN id_rol SET DEFAULT nextval('public.rol_id_rol_seq'::regclass);


--
-- Name: id_seccion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seccion ALTER COLUMN id_seccion SET DEFAULT nextval('public.seccion_id_seccion_seq'::regclass);


--
-- Name: id_sno_familia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sno_familia ALTER COLUMN id_sno_familia SET DEFAULT nextval('public.sno_familia_id_sno_familia_seq'::regclass);


--
-- Name: id_solicitud_acta_pida; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_acta_pida ALTER COLUMN id_solicitud_acta_pida SET DEFAULT nextval('public.solicitud_acta_pida_id_solicitud_acta_pida_seq'::regclass);


--
-- Name: id_solicitud_acta_proyecto; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_acta_proyecto ALTER COLUMN id_solicitud_acta_proyecto SET DEFAULT nextval('public.solicitud_acta_proyecto_id_solicitud_acta_proyecto_seq'::regclass);


--
-- Name: id_solicitud_ascenso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_ascenso ALTER COLUMN id_solicitud_ascenso SET DEFAULT nextval('public.solicitud_ascenso_id_solicitud_ascenso_seq'::regclass);


--
-- Name: id_solicitud_escalafon_siguiente; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_escalafon_siguiente ALTER COLUMN id_solicitud_escalafon_siguiente SET DEFAULT nextval('public.solicitud_escalafon_siguiente_id_solicitud_escalafon_siguie_seq'::regclass);


--
-- Name: id_solicitud_resolucion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_resolucion ALTER COLUMN id_solicitud_resolucion SET DEFAULT nextval('public.solicitud_resolucion_id_solicitud_resolucion_seq'::regclass);


--
-- Name: id_telefono_codigo_area; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.telefono_codigo_area ALTER COLUMN id_telefono_codigo_area SET DEFAULT nextval('public.telefono_codigo_area_id_telefono_codigo_area_seq'::regclass);


--
-- Name: id_telefono_tipo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.telefono_tipo ALTER COLUMN id_telefono_tipo SET DEFAULT nextval('public.telefono_tipo_id_telefono_tipo_seq'::regclass);


--
-- Name: id_tipo_activacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_activacion ALTER COLUMN id_tipo_activacion SET DEFAULT nextval('public.tipo_activacion_id_tipo_activacion_seq'::regclass);


--
-- Name: id_tipo_archivo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_archivo ALTER COLUMN id_tipo_archivo SET DEFAULT nextval('public.tipo_archivo_id_tipo_archivo_seq'::regclass);


--
-- Name: id_tipo_discapacidad; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_discapacidad ALTER COLUMN id_tipo_discapacidad SET DEFAULT nextval('public.tipo_discapacidad_id_tipo_discapacidad_seq'::regclass);


--
-- Name: id_tipo_documento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_documento ALTER COLUMN id_tipo_documento SET DEFAULT nextval('public.tipo_documento_id_tipo_documento_seq'::regclass);


--
-- Name: id_tipo_estudio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_estudio ALTER COLUMN id_tipo_estudio SET DEFAULT nextval('public.tipo_estudio_id_tipo_estudio_seq'::regclass);


--
-- Name: id_tipo_experiencia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_experiencia ALTER COLUMN id_tipo_experiencia SET DEFAULT nextval('public.tipo_experiencia_id_tipo_experiencia_seq'::regclass);


--
-- Name: id_tipo_info; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_info ALTER COLUMN id_tipo_info SET DEFAULT nextval('public.tipo_info_id_tipo_info_seq'::regclass);


--
-- Name: id_tipo_jurado; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_jurado ALTER COLUMN id_tipo_jurado SET DEFAULT nextval('public.tipo_jurado_id_tipo_jurado_seq'::regclass);


--
-- Name: id_tipo_metodologia; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_metodologia ALTER COLUMN id_tipo_metodologia SET DEFAULT nextval('public.tipo_metodologia_id_tipo_metodologia_seq'::regclass);


--
-- Name: id_tipo_reconocimiento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_reconocimiento ALTER COLUMN id_tipo_reconocimiento SET DEFAULT nextval('public.tipo_reconocimiento_id_tipo_reconocimiento_seq'::regclass);


--
-- Name: id_trabajo_administrativo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trabajo_administrativo ALTER COLUMN id_trabajo_administrativo SET DEFAULT nextval('public.trabajo_administrativo_id_trabajo_administrativo_seq'::regclass);


--
-- Name: id_trabajo_ascenso; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trabajo_ascenso ALTER COLUMN id_trabajo_ascenso SET DEFAULT nextval('public.trabajo_ascenso_id_trabajo_ascenso_seq'::regclass);


--
-- Name: id_trabajo_grado; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trabajo_grado ALTER COLUMN id_trabajo_grado SET DEFAULT nextval('public.trabajo_grado_id_trabajo_grado_seq'::regclass);


--
-- Name: id_tramo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tramo ALTER COLUMN id_tramo SET DEFAULT nextval('public.tramo_id_tramo_seq'::regclass);


--
-- Name: id_trayecto; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trayecto ALTER COLUMN id_trayecto SET DEFAULT nextval('public.trayecto_id_trayecto_seq'::regclass);


--
-- Name: id_turno; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.turno ALTER COLUMN id_turno SET DEFAULT nextval('public.turno_id_turno_seq'::regclass);


--
-- Name: id_tutor_estudios; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutor_estudios ALTER COLUMN id_tutor_estudios SET DEFAULT nextval('public.tutor_estudios_id_tutor_estudios_seq'::regclass);


--
-- Name: id_tutoria; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutoria ALTER COLUMN id_tutoria SET DEFAULT nextval('public.tutoria_id_tutoria_seq'::regclass);


--
-- Name: id_unidad_administrativa; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidad_administrativa ALTER COLUMN id_unidad_administrativa SET DEFAULT nextval('public.unidad_administrativa_id_unidad_administrativa_seq'::regclass);


--
-- Name: id_unidad_administrativa_telefono; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidad_administrativa_telefono ALTER COLUMN id_unidad_administrativa_telefono SET DEFAULT nextval('public.unidad_administrativa_telefon_id_unidad_administrativa_tele_seq'::regclass);


--
-- Name: id_unidad_curricular; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidad_curricular ALTER COLUMN id_unidad_curricular SET DEFAULT nextval('public.unidad_curricular_id_unidad_curricular_seq'::regclass);


--
-- Name: id_unidad_curricular_detalle; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidad_curricular_detalle ALTER COLUMN id_unidad_curricular_detalle SET DEFAULT nextval('public.unidad_curricular_detalle_id_unidad_curricular_detalle_seq'::regclass);


--
-- Name: id_unidad_curricular_tipo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidad_curricular_tipo ALTER COLUMN id_unidad_curricular_tipo SET DEFAULT nextval('public.unidad_curricular_tipo_id_unidad_curricular_tipo_seq'::regclass);


--
-- Name: id_unidad_pago; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidad_pago ALTER COLUMN id_unidad_pago SET DEFAULT nextval('public.unidad_de_pago_id_unidad_de_pago_seq'::regclass);


--
-- Name: id_usuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario ALTER COLUMN id_usuario SET DEFAULT nextval('public.usuario_id_usuario_seq'::regclass);


--
-- Name: id_usuario_pregunta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario_pregunta ALTER COLUMN id_usuario_pregunta SET DEFAULT nextval('public.usuario_pregunta_id_usuario_pregunta_seq'::regclass);


--
-- Name: id_usuario_rol; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario_rol ALTER COLUMN id_usuario_rol SET DEFAULT nextval('public.usuario_rol_id_usuario_rol_seq'::regclass);


--
-- Data for Name: activacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.activacion (id_activacion, periodo_lectivo, fecha_inicio, fecha_fin, id_tipo_activacion, descripcion) FROM stdin;
115	I	2020-01-23	2020-02-29	1	PRUEBA DOS
\.


--
-- Name: activacion_id_activacion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.activacion_id_activacion_seq', 115, true);


--
-- Data for Name: aldea; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.aldea (id_aldea, descripcion, codigo_sucre, codigo_aldea, estatus, id_aldea_tipo, id_ambiente) FROM stdin;
1	palo verde	1232	548	1	4	1
\.


--
-- Name: aldea_id_aldea_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.aldea_id_aldea_seq', 1, true);


--
-- Data for Name: aldea_malla_curricula; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.aldea_malla_curricula (id_aldea_malla_curricula, estatus, id_aldea, id_malla_curricular, id_turno) FROM stdin;
\.


--
-- Name: aldea_malla_curricula_id_aldea_malla_curricula_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.aldea_malla_curricula_id_aldea_malla_curricula_seq', 1, false);


--
-- Data for Name: aldea_programa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.aldea_programa (id_aldea_programa, codigo_opsu, id_aldea, id_programa, estatus) FROM stdin;
\.


--
-- Name: aldea_programa_id_aldea_programa_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.aldea_programa_id_aldea_programa_seq', 1, false);


--
-- Data for Name: aldea_telefono; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.aldea_telefono (id_aldea_telefono, descripcion, id_telefono_codigo_area, id_aldea) FROM stdin;
\.


--
-- Name: aldea_telefono_id_aldea_telefono_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.aldea_telefono_id_aldea_telefono_seq', 1, false);


--
-- Data for Name: aldea_tipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.aldea_tipo (id_aldea_tipo, descripcion) FROM stdin;
4	Sede
5	Aldea
6	Sede Administrativa
\.


--
-- Name: aldea_tipo_id_aldea_tipo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.aldea_tipo_id_aldea_tipo_seq', 6, true);


--
-- Data for Name: aldea_ubv; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.aldea_ubv (id_aldea_ubv, descripcion) FROM stdin;
1	Aldea 1
2	Aldea 2
3	Aldea 3
4	Aldea 4
5	Aldea 5
6	Aldea 6
7	Aldea 7
8	Aldea 10
9	Aldea 11
\.


--
-- Name: aldea_ubv_id_aldea_ubv_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.aldea_ubv_id_aldea_ubv_seq', 9, true);


--
-- Data for Name: ambiente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ambiente (id_ambiente, descripcion, codigo, direccion, coordenada_latitud, coordenada_longitd, id_parroquia, estatus) FROM stdin;
1	ambiente uno	3234	los altos de san antonio	56	97	1	1
\.


--
-- Data for Name: ambiente_detalle; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ambiente_detalle (id_ambiente_detalle, descripcion, estatus, id_ambiente) FROM stdin;
\.


--
-- Name: ambiente_detalle_id_ambiente_detalle_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ambiente_detalle_id_ambiente_detalle_seq', 1, false);


--
-- Data for Name: ambiente_espacio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ambiente_espacio (id_ambiente_espacio, descripcion, capacidad, id_espacio_tipo, id_ambiente_detalle, estatus) FROM stdin;
\.


--
-- Name: ambiente_espacio_id_ambiente_espacio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ambiente_espacio_id_ambiente_espacio_seq', 1, false);


--
-- Name: ambiente_id_ambiente_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ambiente_id_ambiente_seq', 1, true);


--
-- Data for Name: antropometrico; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.antropometrico (id_antropometrico, peso, talla_pantalon, talla_camisa, talla_calzado, estatura, id_persona) FROM stdin;
\.


--
-- Name: antropometrico_id_antropometrico_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.antropometrico_id_antropometrico_seq', 1, false);


--
-- Data for Name: arancel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.arancel (id_arancel, descripcion, estatus, modo, codigo, cantidad_ut, fecha_inicio, fecha_fin) FROM stdin;
\.


--
-- Name: arancel_id_arancel_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.arancel_id_arancel_seq', 1, false);


--
-- Data for Name: area_conocimiento_opsu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.area_conocimiento_opsu (id_area_conocimiento_opsu, descripcion, codigo) FROM stdin;
17	Ciencias Básicas y Naturales	01
18	Ciencias de la Educación	02
19	Ciencias de la Salud	03
20	Ciencias del Agro y del Mar	04
21	Ciencias Sociales y Económicas	05
22	Ciencias y Artes Militares	06
23	Humanidades, Letras y Artes	07
24	Ingeniería, Arquitectura y Tecnología	08
25	Ninguno	00
\.


--
-- Name: area_conocimiento_opsu_id_area_conocimiento_opsu_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.area_conocimiento_opsu_id_area_conocimiento_opsu_seq', 25, true);


--
-- Data for Name: area_conocimiento_ubv; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.area_conocimiento_ubv (id_area_conocimiento_ubv, descripcion, codigo, estatus, id_area_conocimiento_opsu) FROM stdin;
1	area ubv 1	053	t	17
2	area ubv 2	456	f	18
\.


--
-- Name: area_conocimiento_ubv_id_area_conocimiento_ubv_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.area_conocimiento_ubv_id_area_conocimiento_ubv_seq', 2, true);


--
-- Data for Name: arte_software; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.arte_software (id_arte_software, nombre_arte, ano_arte, entidad_promotora, url_otro, id_docente, estatus_verificacion) FROM stdin;
\.


--
-- Name: arte_software_id_arte_software_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.arte_software_id_arte_software_seq', 4, true);


--
-- Data for Name: articulo_revista; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.articulo_revista (id_articulo_revista, nombre_revista, titulo_articulo, issn_revista, ano_revista, nro_revista, nro_pag_inicial, url_revista, id_docente, estatus_verificacion) FROM stdin;
\.


--
-- Name: articulo_revista_id_articulo_revista_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.articulo_revista_id_articulo_revista_seq', 6, true);


--
-- Data for Name: ascenso_titular; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ascenso_titular (id_ascenso_titular, id_solicitud_ascenso, id_estudio_conducente) FROM stdin;
\.


--
-- Name: ascenso_titular_id_ascenso_titular_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ascenso_titular_id_ascenso_titular_seq', 1, false);


--
-- Data for Name: aspirante; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.aspirante (id_aspirante, promedio_nota, "año_grado", periodo_asignado, codigo_opsu, id_persona, id_aspirante_tipo, id_grado_academico, id_aldea, id_programa, id_aspirante_estatus, institucion_tipo, institucion, trabajo) FROM stdin;
\.


--
-- Data for Name: aspirante_estatus; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.aspirante_estatus (id_aspirante_estatus, desripcion) FROM stdin;
\.


--
-- Name: aspirante_estatus_id_aspirante_estatus_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.aspirante_estatus_id_aspirante_estatus_seq', 1, false);


--
-- Name: aspirante_id_aspirante_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.aspirante_id_aspirante_seq', 1, false);


--
-- Data for Name: aspirante_tipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.aspirante_tipo (id_aspirante_tipo, descripcion) FROM stdin;
11	OPSU
12	POST-GRADO
13	COMUNIDAD
14	CONVENIO
15	POSTULACION
\.


--
-- Name: aspirante_tipo_id_aspirante_tipo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.aspirante_tipo_id_aspirante_tipo_seq', 15, true);


--
-- Data for Name: banco; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.banco (id_banco, descripcion, codigo) FROM stdin;
63	BANCO CENTRAL DE VENEZUELA	0001
64	BANCO DE VENEZUELA S.A.C.A. BANCO UNIVERSAL	0102
65	VENEZOLANO DE CRÉDITO, S.A. BANCO UNIVERSAL	0104
66	BANCO MERCANTIL, C.A. S.A.C.A. BANCO UNIVERSAL	0105
67	BANCO PROVINCIAL, S.A. BANCO UNIVERSAL	0108
68	BANCO DEL CARIBE, C.A. BANCO UNIVERSAL	0114
69	BANCO EXTERIOR, C.A. BANCO UNIVERSAL	0115
70	BANCO OCCIDENTAL DE DESCUENTO BANCO UNIVERSAL, C.A.	0116
71	BANCO CARONI, C.A. BANCO UNIVERSAL	0128
72	BANESCO BANCO UNIVERSAL S.A.C.A.	0134
73	BANCO SOFITASA BANCO UNIVERSAL, C.A	0137
74	BANCO PLAZA, BANCO UNIVERSAL C.A	0138
75	BANCO DE LA GENTE EMPRENDEDORA BANGENTE, C.A	0146
76	BANCO DEL PUEBLO SOBERANO, BANCO DE DESARROLLO	0149
77	BFC BANCO FONDO COMUN C.A. BANCO UNIVERSAL	0151
78	100% BANCO, BANCO COMERCIAL, C.A.	0156
79	DELSUR BANCO UNIVERSAL, C.A.	0157
80	BANCO DEL TESORO, C.A. BANCO UNIVERSAL	0163
81	BANCO AGRICOLA DE VENEZUELA, C.A. BANCO UNIVERSAL	0166
82	BANCRECER S.A. BANCO DE DESARROLLO	0168
83	MI BANCO, BANCO MICROFINANCIERO, C.A.	0169
84	BANCO ACTIVO, C.A. BANCO UNIVERSAL	0171
85	BANCAMIGA BANCO MICROFINANCIERO, C.A.	0172
86	BANCO INTERNACIONAL DE DESARROLLO, C.A. BANCO UNIVERSAL	0173
87	BANPLUS BANCO UNIVERAL, C.A.	0174
88	BANCO BICENTENARIO BANCO UNIVERSAL, C.A.	0175
89	NOVO BANCO, S.A. SUCURSAL VENEZUELA BANCO UNIVERSAL	0176
90	BANCO DE LA FUERZA ARMADA NACIONAL BOLIVARIANA, B.U.	0177
91	CITIBANK N.A.	0190
92	BANCO NACIONAL CRÉDITO, C.A. BANCO UNIVERSAL	0191
93	INSTITUTO MUNICIPAL DE CRÉDITO POPULAR	0601
\.


--
-- Name: banco_id_banco_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.banco_id_banco_seq', 93, true);


--
-- Data for Name: cambiar_oferta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cambiar_oferta (id_cambiar_oferta, fecha, codigo_solicitud, estatus, aldea_, perograma, turno, secciones, uc, id_oferta_academica, id_persona) FROM stdin;
\.


--
-- Name: cambiar_oferta_id_cambiar_oferta_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cambiar_oferta_id_cambiar_oferta_seq', 1, false);


--
-- Data for Name: carga_academica; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.carga_academica (id_carga_academica, id_oferta_academica_detalle, id_ambiente_espacio, id_docente) FROM stdin;
\.


--
-- Name: carga_academica_id_carga_academica_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.carga_academica_id_carga_academica_seq', 1, false);


--
-- Data for Name: cargo_experiencia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cargo_experiencia (id_cargo_experiencia, descripcion) FROM stdin;
\.


--
-- Name: cargo_experiencia_id_cargo_experiencia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cargo_experiencia_id_cargo_experiencia_seq', 25, true);


--
-- Data for Name: censo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.censo (id_censo, estatus, descripcion, id_persona) FROM stdin;
2	1		232
3	1		272
1	1		206
\.


--
-- Name: censo_id_censo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.censo_id_censo_seq', 3, true);


--
-- Data for Name: centro_estudio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.centro_estudio (id_centro_estudio, descripcion, codigo, id_area_conocimiento_opsu, estatus) FROM stdin;
1	CENTRO DE ESTUDIOS AMBIENTALES	CEA	17	t
2	CENTRO DE ESTUDIOS EN CIENCIA DE LA ENERGÍA	CEP	24	t
3	Centro de Estudios para la Salud Colectiva y el Derecho a la Vida	CESACODEVI	19	t
4	CENTRO DE ESTUDIO DE EDUCACIÓN EMANCIPADORA Y PEDAGOGÍA CRÍTICA	CEPEC	18	t
6	CENTRO DE ESTUDIOS DE LA COMUNICACIÓN SOCIAL	CECSO	21	t
7	CENTRO DE ESTUDIOS PUEBLOS Y CULTURA INDIGENAS	CEPyCI	23	t
5	CENTRO DE ESTUDIOS SOCIALES Y CULTURALES	CESyC	23	t
8	CENTRO DE ESTUDIOS ECONOMÍA POLÍTICA	CEEP	21	t
9	Ninguno	00	25	t
\.


--
-- Name: centro_estudio_id_centro_estudio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.centro_estudio_id_centro_estudio_seq', 9, true);


--
-- Data for Name: certificado_titulo_estudio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.certificado_titulo_estudio (id_certificado_titulo_estudio, id_estudio, id_documento) FROM stdin;
44	155	502
45	156	503
\.


--
-- Name: certificado_titulo_estudio_id_certificado_titulo_estudio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.certificado_titulo_estudio_id_certificado_titulo_estudio_seq', 45, true);


--
-- Data for Name: clasificacion_docente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.clasificacion_docente (id_clasificacion_docente, descripcion) FROM stdin;
1	DOCENTE
2	CONTRATADO
3	ALTO NIVEL
4	JUBILADOS
5	PENSIONADOS
\.


--
-- Name: clasificacion_docente_id_clasificacion_docente_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.clasificacion_docente_id_clasificacion_docente_seq', 5, true);


--
-- Data for Name: comision_excedencia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.comision_excedencia (id_comision_excedencia, id_tipo_info, lugar, desde, hasta, designacion_funcion, causa, aprobacion, id_docente, estatus_verificacion) FROM stdin;
\.


--
-- Name: comision_excedencia_id_comision_excedencia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.comision_excedencia_id_comision_excedencia_seq', 5, true);


--
-- Data for Name: comision_gubernamental; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.comision_gubernamental (id_comision_gubernamental, lugar, id_experiencia_docente) FROM stdin;
\.


--
-- Name: comision_gubernamental_id_comision_gubernamental_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.comision_gubernamental_id_comision_gubernamental_seq', 4, true);


--
-- Data for Name: comite_tipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.comite_tipo (id_comite_tipo, descripcion, estatus) FROM stdin;
\.


--
-- Name: comite_tipo_id_comite_tipo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.comite_tipo_id_comite_tipo_seq', 3, true);


--
-- Data for Name: comitepfa_integrante; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.comitepfa_integrante (id_comitepfa_integrante, fecha_inicio, fecha_fin, estatus, id_programa, id_docente, id_comite_tipo, descripcion) FROM stdin;
\.


--
-- Name: comitepfa_integrante_historic_id_comitepfa_integrante_histo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.comitepfa_integrante_historic_id_comitepfa_integrante_histo_seq', 1, false);


--
-- Data for Name: comitepfa_integrante_historico; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.comitepfa_integrante_historico (id_comitepfa_integrante_historico, fecha_inicio, fecha_fin, estatus, id_programa, id_docente, id_comite_tipo, descripcion) FROM stdin;
\.


--
-- Name: comitepfa_integrante_id_comitepfa_integrante_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.comitepfa_integrante_id_comitepfa_integrante_seq', 2, true);


--
-- Data for Name: concurso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.concurso (id_concurso, fecha_concurso, id_escalafon, id_docente, id_documento) FROM stdin;
\.


--
-- Name: concurso_id_concurso_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.concurso_id_concurso_seq', 23, true);


--
-- Data for Name: constancia_administrativo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.constancia_administrativo (id_constancia_administrativo, id_trabajo_administrativo, id_documento) FROM stdin;
\.


--
-- Name: constancia_administrativo_id_constancia_administrativo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.constancia_administrativo_id_constancia_administrativo_seq', 10, true);


--
-- Data for Name: correo_tipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.correo_tipo (id_correo_tipo, descripcion) FROM stdin;
5	Personal
6	Institucional
\.


--
-- Name: correo_tipo_id_correo_tipo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.correo_tipo_id_correo_tipo_seq', 6, true);


--
-- Data for Name: cuenta_bancaria; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cuenta_bancaria (id_cuenta_bancaria, descripcion, id_cuenta_bancaria_tipo, id_banco, estatus) FROM stdin;
\.


--
-- Name: cuenta_bancaria_id_cuenta_bancaria_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cuenta_bancaria_id_cuenta_bancaria_seq', 1, false);


--
-- Data for Name: cuenta_bancaria_tipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cuenta_bancaria_tipo (id_cuenta_bancaria_tipo, descripcion) FROM stdin;
4	Corriente
5	Ahorro
6	Virtual
\.


--
-- Name: cuenta_bancaria_tipo_id_cuenta_bancaria_tipo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cuenta_bancaria_tipo_id_cuenta_bancaria_tipo_seq', 6, true);


--
-- Data for Name: deposito; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.deposito (id_deposito, num_deposito, monto, id_arancel, id_cuenta_bancaria, id_persona) FROM stdin;
\.


--
-- Name: deposito_id_deposito_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.deposito_id_deposito_seq', 1, false);


--
-- Data for Name: dia_semana; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.dia_semana (id_dia_semana, descripcion) FROM stdin;
8	Lunes
9	Martes
10	Miercoles
11	Jueves
12	Viernes
13	Sabado
14	Domingo
\.


--
-- Name: dia_semana_id_dia_semana_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.dia_semana_id_dia_semana_seq', 14, true);


--
-- Data for Name: discapacidad; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.discapacidad (id_discapacidad, descripcion, id_tipo_discapacidad) FROM stdin;
1	invidencia	5
\.


--
-- Name: discapacidad_id_discapacidad_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.discapacidad_id_discapacidad_seq', 1, true);


--
-- Data for Name: diseno_uc_docente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.diseno_uc_docente (id_diseno_uc, nombre, ano_creacion, id_tramo, id_unidad_curricular_tipo, id_programa, id_area_conocimiento_ubv, id_documento, id_docente, estatus_verificacion) FROM stdin;
\.


--
-- Name: diseno_uc_docente_id_diseno_uc_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.diseno_uc_docente_id_diseno_uc_seq', 16, true);


--
-- Data for Name: docencia_previa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.docencia_previa (id_docencia_previa, materia, id_nivel_academico, id_modalidad_estudio, semestre, periodo_lectivo, naturaleza, horas_semanales, estudiantes_atendidos, id_experiencia_docente) FROM stdin;
\.


--
-- Name: docencia_previa_id_docencia_previa_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.docencia_previa_id_docencia_previa_seq', 6, true);


--
-- Data for Name: docencia_previa_ubv; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.docencia_previa_ubv (id_docencia_previa_ubv, id_area_conocimiento_ubv, id_programa, id_eje_regional, id_eje_municipal, id_aldea, id_centro_estudio, fecha_ingreso, estatus_verificacion, id_docente) FROM stdin;
\.


--
-- Name: docencia_previa_ubv_id_docencia_previa_ubv_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.docencia_previa_ubv_id_docencia_previa_ubv_seq', 4, true);


--
-- Data for Name: docente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.docente (id_docente, id_centro_estudio, id_persona, id_eje_municipal, id_docente_dedicacion, id_docente_estatus, tipo_docente, id_clasificacion_docente, id_escalafon) FROM stdin;
137	1	207	5	1	1	SAETA	1	7
138	1	208	6	1	1	SAETA	1	1
139	1	210	3	1	2	SAETA	1	2
140	1	211	7	1	2	SAETA	1	3
141	1	212	6	1	1	SAETA	1	2
142	1	213	7	1	2	SAETA	1	1
145	1	233	7	1	1	SAETA	2	4
146	1	235	3	1	1	SAETA	1	1
147	1	236	5	1	1	SAETA	1	1
148	1	237	5	1	1	SAETA	1	3
149	1	244	7	1	1	SAETA	1	1
151	1	246	7	1	1	SAETA	1	1
152	1	248	6	1	1	SAETA	1	1
153	1	249	3	1	1	SAETA	1	1
150	1	245	1	1	1	SAETA	2	5
155	1	251	11	1	1	SIDTA	1	1
157	1	258	9	1	1	SIDTA	2	3
156	4	252	8	1	1	SIDTA	1	1
158	3	271	18	1	1	SIDTA	1	2
144	1	232	6	1	1	SAETA	2	4
160	1	273	8	1	1	SIDTA	1	1
161	3	274	8	1	1	SIDTA	2	5
159	3	272	12	1	1	SIDTA	2	3
154	1	250	1	1	1	SAETA	1	3
136	1	206	5	1	1	SAETA	1	7
\.


--
-- Data for Name: docente_aldea; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.docente_aldea (id_docente_aldea, id_docente, id_aldea_ubv) FROM stdin;
1	156	2
2	157	3
6	144	2
5	159	9
4	154	7
3	136	5
\.


--
-- Name: docente_aldea_id_docente_alde_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.docente_aldea_id_docente_alde_seq', 6, true);


--
-- Data for Name: docente_dedicacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.docente_dedicacion (id_docente_dedicacion, descripcion, total_horas, hora_docencia_minima, hora_docencia_maxima) FROM stdin;
1	DOCENTE DEDICACION EXCLUSIVA	40	12	16
2	DOCENTE TIEMPO COMPLETO	36	8	12
3	DOCENTE MEDIO TIEMPO	18	4	6
4	DOCENTE TIEMPO CONVENCIONAL	7	7	7
\.


--
-- Name: docente_dedicacion_id_docente_dedicacion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.docente_dedicacion_id_docente_dedicacion_seq', 4, true);


--
-- Data for Name: docente_estatus; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.docente_estatus (id_docente_estatus, descripcion) FROM stdin;
1	ACTIVO
2	SUSPENDIDO
3	PASIVO
\.


--
-- Name: docente_estatus_id_docente_estatus_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.docente_estatus_id_docente_estatus_seq', 3, true);


--
-- Name: docente_id_docente_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.docente_id_docente_seq', 161, true);


--
-- Data for Name: docente_programa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.docente_programa (id_docente_programa, id_docente, id_programa) FROM stdin;
2	160	1
3	160	4
18	161	1
19	161	4
20	161	3
35	159	1
36	159	4
37	159	3
40	154	1
41	154	4
42	136	1
43	136	4
\.


--
-- Name: docente_programa_id_docente_programa_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.docente_programa_id_docente_programa_seq', 43, true);


--
-- Data for Name: documento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.documento (id_documento, descripcion, estatus, id_persona, id_tipo_documento) FROM stdin;
502	2020-02-27 19:25:28Presentación_Metodología.pdf	Sin Verificar	206	1
503	2020-02-27 21:55:10Presentación_Metodología.pdf	Sin Verificar	206	1
\.


--
-- Name: documento_id_documento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.documento_id_documento_seq', 503, true);


--
-- Data for Name: documento_identidad_tipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.documento_identidad_tipo (id_documento_identidad_tipo, descripcion) FROM stdin;
7	Venezolano
8	Extranjero
9	Pasaporte
\.


--
-- Name: documento_identidad_tipo_id_documento_identidad_tipo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.documento_identidad_tipo_id_documento_identidad_tipo_seq', 9, true);


--
-- Data for Name: documento_solicitud; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.documento_solicitud (id_documento_solicitud, id_solicitud_ascenso, id_documento) FROM stdin;
\.


--
-- Name: documento_solicitud_id_documento_solicitud_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.documento_solicitud_id_documento_solicitud_seq', 1, false);


--
-- Data for Name: domicilio_detalle_tipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.domicilio_detalle_tipo (id_domicilio_detalle_tipo, descripcion) FROM stdin;
1	Casa
2	Departamento
\.


--
-- Name: domicilio_detalle_tipo_id_domicilio_detalle_tipo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.domicilio_detalle_tipo_id_domicilio_detalle_tipo_seq', 2, true);


--
-- Data for Name: domicilio_persona; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.domicilio_persona (id_domicilio_persona, id_parroquia, id_persona, domicilio_detalle, id_domicilio_detalle_tipo) FROM stdin;
60	636	208	PALO VERDE AV CENTRAL EDIF EL REMANSO	1
61	634	209	palo verde	2
63	16	211	EL GUARATARO 	1
64	634	212	PALO VERDE	1
65	634	213	santa teresa	1
66	630	214	ADMIN ADMIN	1
67	634	215	san agustin	1
69	634	217	direccion de alta	1
62	634	210	LA BOMBILLA	1
71	634	233	AV. MARIA TERESA TORO, N 17. URB. LAS ACALCIAS 	1
72	636	235	SECTRO ARAMARE PUERTO AYACUCHO ESTADO AMAZONAS 	1
73	634	236	CARACAS 	1
74	634	237	CALLE MISTER WILLIAM HACKETT - VIA CANDELARIA - SECTOR CRUZ VERDE N 1-28 TRUJILLO 	1
75	634	244	EL ENCANTO. CALLE BERTORELLI. EDI. CARAO. APTO 12D-1.PISO 12 	1
76	634	245	CARACAS 	1
77	637	246	LOS CORRALES. 1ERA AV. CASA 34 	1
78	593	248	FIONAL CALLE PPAL N# 72, PEDRAZA LA VIEJA. MUNICIPIO EZEQUIEL ZAMORA 	1
79	181	249	AV JESUS SOTO CONJUBNTO RES. ANGOSTURA EDIF A-1 APTO N 121 	1
92	589	274	AV,PRINCIPAL DE BELLA VISTA, CASA CINARAL 	1
90	634	272	UD 4, CONJUNTO RESIDENCIAL MUCURITAS BL.8 P.14 APTO. 1401 	1
80	636	250	PROPATRIA LOS MAGALLANES	1
58	121	206	PALO VERDE 	1
81	634	251	URB. EZEQUIEL ZAMORA CASA N 47	2
88	634	258	URB PALMA REAL CONJ RES AGUA DE CANTO N19	1
82	634	252	APTO 1-D LA CANDELARIA, PARQUE CARABOBO	1
89	634	271	CARACAS 	1
59	636	207	PALO VERDE	1
70	634	232	CALLE CUCHIVERO,QTA MORICHAL URB.PIEDRA AZUL BARUTA 	1
91	635	273	URB. LAS CAYENAS, MANZANA 01 N 32. VIA LA CRUZ.	1
\.


--
-- Name: domicilio_persona_id_domicilio_persona_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.domicilio_persona_id_domicilio_persona_seq', 92, true);


--
-- Data for Name: eje_formacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.eje_formacion (id_eje_formacion, descripcion, codigo) FROM stdin;
\.


--
-- Name: eje_formacion_id_eje_formacion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.eje_formacion_id_eje_formacion_seq', 1, false);


--
-- Data for Name: eje_municipal; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.eje_municipal (id_eje_municipal, descripcion, codigo, id_eje_regional, estatus) FROM stdin;
1	EM CACIQUE CATIA	0101	1	false
2	EM CACIQUE TAMANACO	0102	1	false
3	EM CACIQUE TIUNA	0103	1	false
4	EM DOCTOR JOSE MARIA VARGAS	0104	1	false
5	EM GENERAL EZEQUIEL ZAMORA	0105	1	false
6	EM HEROINA EULALIA RAMOS	0106	1	false
7	EM LIBERTADOR SIMON BOLIVAR	0107	1	false
8	EM CAMPO DE CARABOBO	0201	2	false
9	EM CAPITAN ANTONIO RICAURTE	0202	2	false
10	EM LOS MORROS DE SAN JUAN	0203	2	false
11	EM GRAN ESTADO DEL NORTE DE OCCIDENTE	0301	3	false
12	EM HEROINA JOSEFA CAMEJO	0302	3	false
13	EM INDIOS JIRAJARAS	0303	3	false
14	EM CACIQUE PITIJOC	0401	4	false
15	EM INDIOS CAQUETIOS	0402	4	false
16	EM MAARA-IWO	0403	4	false
17	EM RELAMPAGOS DEL CATATUMBO	0404	4	false
18	EM CACIQUE MURACHI	0501	5	false
19	EM INDIOS HUMOGRIAS	0502	5	false
20	EM LOS COMUNEROS DE LOS ANDES	0503	5	false
21	EM PARAMO EL TAMA	0504	5	false
22	EM PICO BOLIVAR	0505	5	false
23	EM RIOS ARAUCA-URIBANTE	0506	5	false
24	EM CACIQUE COROMOTO	0601	6	false
25	EM GENERAL JOSE LAURENCIO SILVA	0602	6	false
26	EM INDIOS VARYNA	0603	6	false
27	EM RIO GUANAGUANARE	0604	6	false
28	EM CACIQUE ARAMARE	0701	7	false
29	EM CORONEL ANDRES ELORZA	0702	7	false
30	EM PICO LA NEBLINA	0703	7	false
31	EM VUELVAN CARAS	0704	7	false
32	EM FAJA DEL ORINOCO	0801	8	false
33	EM INDIOS PEMON	0802	8	false
34	EM RIO CARONI	0803	8	false
35	EM RIO CAURA	0804	8	false
36	EM SIERRA DE IMATACA	0804	8	false
37	EM CARIBANA	0901	9	false
38	EM INDIA GUANIPA	0902	9	false
39	EM INDIOS WARAO	0903	9	false
40	EM INDIOS GUAIQUERIES	1001	10	false
41	EM PUEBLOS DE LA MAR	1002	10	false
\.


--
-- Name: eje_municipal_id_eje_municipal_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.eje_municipal_id_eje_municipal_seq', 41, true);


--
-- Data for Name: eje_regional; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.eje_regional (id_eje_regional, descripcion, codigo, estatus) FROM stdin;
2	EGR GENERAL JOSE FELIX RIBAS	02	true
3	EGR PRECURSOR JOSE LEONARDO CHIRINOS	03	true
4	EGR CACIQUE MARA	04	true
5	EGR GENERAL CIPRIANO CASTRO	05	true
6	EGR GUERRILLERO MAISANTA	06	true
7	EGR RIOS ORINOCO APURE	07	true
8	EGR SALTO KEREPAKUPAI VENA	08	true
9	EGR HEROINA JUANA LA AVANZADORA	09	true
10	EGR GENERAL ANTONIO JOSE DE SUCRE	10	true
1	EGR CACIQUE GUAICAIPURO	01	true
\.


--
-- Name: eje_regional_id_eje_regional_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.eje_regional_id_eje_regional_seq', 10, true);


--
-- Data for Name: escalafon; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.escalafon (id_escalafon, descripcion) FROM stdin;
1	INSTRUCTOR
2	ASISTENTE
3	AGREGADO
4	ASOCIADO
5	TITULAR
6	EMÉRITO
7	AUXILIAR I
8	AUXILIAR II
9	AUXILIAR III
\.


--
-- Name: escalafon_id_escalafon_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.escalafon_id_escalafon_seq', 29, true);


--
-- Data for Name: espacio_tipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.espacio_tipo (id_espacio_tipo, descripcion) FROM stdin;
15	Aula
16	Laboratorio
17	Oficina
18	Usos Multiples
19	Lobby
20	Estacionamiento
21	Cancha Deportiva
\.


--
-- Name: espacio_tipo_id_espacio_tipo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.espacio_tipo_id_espacio_tipo_seq', 21, true);


--
-- Data for Name: estado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.estado (id_estado, descripcion, codigo, id_pais) FROM stdin;
1	Distrito Capital	01	242
2	Amazonas	02	242
3	Anzoategui	03	242
4	Apure	04	242
5	Aragua	05	242
6	Barinas	06	242
7	Bolivar	07	242
8	Carabobo	08	242
9	Cojedes	09	242
10	Delta Amacuro	10	242
11	Falcon	11	242
12	Guarico	12	242
13	Lara	13	242
14	Merida	14	242
15	Miranda	15	242
16	Monagas	16	242
17	Nueva Esparta	17	242
18	Portuguesa	18	242
19	Sucre	19	242
20	Tachira	20	242
21	Trujillo	21	242
22	Yaracuy	22	242
23	Zulia	23	242
24	Vargas	24	242
\.


--
-- Data for Name: estado_civil; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.estado_civil (id_estado_civil, descripcion, codigo) FROM stdin;
9	Soltero	1    
10	Casado	2    
12	Viudo	4    
11	Divorciado	3    
\.


--
-- Name: estado_civil_id_estado_civil_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.estado_civil_id_estado_civil_seq', 12, true);


--
-- Name: estado_id_estado_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.estado_id_estado_seq', 24, true);


--
-- Data for Name: estatus_ascenso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.estatus_ascenso (id_estatus_ascenso, descripcion) FROM stdin;
1	En Proceso
2	Fase I
3	Fase II
4	Culminada
5	Rechazada
\.


--
-- Name: estatus_ascenso_id_estatus_ascenso_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.estatus_ascenso_id_estatus_ascenso_seq', 5, true);


--
-- Data for Name: estudiante; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.estudiante (id_estudiante, estatus, id_persona, id_aspirante_tipo) FROM stdin;
\.


--
-- Data for Name: estudiante_detalle; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.estudiante_detalle (id_estudiante_detalle, estatus, fecha_ingreso, numero_expediente, id_estudiante, id_aldea_malla_curricula, id_turno) FROM stdin;
\.


--
-- Name: estudiante_detalle_id_estudiante_detalle_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.estudiante_detalle_id_estudiante_detalle_seq', 1, false);


--
-- Data for Name: estudiante_documento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.estudiante_documento (id_estudiante_documento, ruta_documento, id_requisito_inscripcion, id_persona, id_tipo_archivo, estatus_documento, descripcion_estatus_documento) FROM stdin;
\.


--
-- Name: estudiante_documento_id_estudiante_documento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.estudiante_documento_id_estudiante_documento_seq', 1, false);


--
-- Name: estudiante_id_estudiante_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.estudiante_id_estudiante_seq', 1, false);


--
-- Data for Name: estudio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.estudio (id_estudio, estudio, id_tipo_estudio, estatus, id_pais, id_modalidad_estudio, ano_estudio, id_institucion, id_docente, estatus_verificacion) FROM stdin;
154	estudio onducente no concluido	1	en curso	242	10	2013	1196	136	Sin Verificar
156	Dicotomia Fisico Quirurgico	1	concluido	242	10	2013	1198	136	Verificado
155	nombre de estudio no conducente 	2	concluido	242	10	1999	1197	136	Verificado
\.


--
-- Data for Name: estudio_conducente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.estudio_conducente (id_estudio_conducente, titulo_obtenido, id_estudio) FROM stdin;
91	estudio conducente nro 1 	154
92	estudio no conducente nro uno	155
93	Especialista fisico quirurgico	156
\.


--
-- Name: estudio_conducente_id_estudio_conducente_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.estudio_conducente_id_estudio_conducente_seq', 93, true);


--
-- Name: estudio_id_estudio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.estudio_id_estudio_seq', 156, true);


--
-- Data for Name: estudio_idioma; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.estudio_idioma (id_estudio_idioma, nivel_lectura, nivel_escritura, nivel_habla, nivel_comprende, id_estudio, id_idioma) FROM stdin;
\.


--
-- Data for Name: estudio_nivel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.estudio_nivel (id_estudio_nivel, id_estudio, id_nivel_academico, id_programa_nivel) FROM stdin;
5	154	19	1
6	156	24	1
\.


--
-- Name: estudio_nivel_id_estudio_nivel_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.estudio_nivel_id_estudio_nivel_seq', 6, true);


--
-- Data for Name: etnia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.etnia (id_etnia, descripcion) FROM stdin;
45	Baniva
46	Baré
47	Kubeo
48	Jiwi
49	Hoti
50	Curripako
51	Piacoco
52	Puninave
53	Sáliva
54	Sanemá/Shirianá
55	Wotjuja/Piaroa
56	Yanomami
57	Warekena
58	Yabarana/Mako
59	Ñengatú/Yeral
60	Kariña
61	Cumanagoto
62	Pumé
63	Kuiba
64	Uruak/Arutani
65	Arawako
66	Arawak
67	Eñepa
68	Pemón
69	Sape
70	Wanaí/Mapoyo
71	Warao
72	Chaima
73	Wayuu
74	Añú
75	Barí
76	Yukpa
77	Japreira
78	Ayamán
79	Amorua
80	Inga
81	Ye Kwana
82	Quinaroe
83	Guazabara
84	Gayón
85	Camentza
86	Guanono
87	Timotes
88	Mako
\.


--
-- Name: etnia_id_etnia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.etnia_id_etnia_seq', 88, true);


--
-- Data for Name: expediente_documento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.expediente_documento (id_expediente_documento, id_estudiante_documento, id_estudiante_detalle) FROM stdin;
\.


--
-- Name: expediente_documento_id_expediente_documento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.expediente_documento_id_expediente_documento_seq', 1, false);


--
-- Data for Name: experiencia_docente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.experiencia_docente (id_experiencia_docente, id_tipo_experiencia, id_cargo_experiencia, fecha_ingreso, fecha_egreso, id_estado, id_institucion, id_docente, ano_experiencia, estatus_verificacion) FROM stdin;
\.


--
-- Name: experiencia_docente_id_experiencia_docente_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.experiencia_docente_id_experiencia_docente_seq', 37, true);


--
-- Data for Name: fase_ii; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fase_ii (id_fase_ii, acta_defensa, id_solicitud_ascenso, memo_acta_1, memo_acta_2, pto_cuenta) FROM stdin;
\.


--
-- Name: fase_ii_id_fase_ii_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.fase_ii_id_fase_ii_seq', 1, false);


--
-- Data for Name: genero; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.genero (id_genero, descripcion, codigo) FROM stdin;
3	Masculino	M    
4	Femenino	F    
\.


--
-- Name: genero_id_genero_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.genero_id_genero_seq', 4, true);


--
-- Data for Name: grado_academico; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.grado_academico (id_grado_academico, descripcion, id_nivel_academico, codigo_ine) FROM stdin;
\.


--
-- Name: grado_academico_id_grado_academico_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.grado_academico_id_grado_academico_seq', 1, false);


--
-- Data for Name: historia_ascenso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.historia_ascenso (id_historia_ascenso, fecha_ascenso, id_escalafon, id_docente) FROM stdin;
\.


--
-- Name: historia_ascenso_id_historia_ascenso_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.historia_ascenso_id_historia_ascenso_seq', 1, false);


--
-- Data for Name: historia_jurado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.historia_jurado (id_historia_jurado, jurado_id_jurado, id_solicitud_ascenso, fecha_jurado, id_propuesta_jurado) FROM stdin;
\.


--
-- Name: historia_jurado_id_historia_jurado_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.historia_jurado_id_historia_jurado_seq', 1, false);


--
-- Data for Name: history_aldea_programa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.history_aldea_programa (id_aldea_programa, codigo_opsu, id_aldea, descripcion_aldea, id_programa, descripcion_programa, estatus, hora) FROM stdin;
\.


--
-- Data for Name: history_comitepfa_integrante; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.history_comitepfa_integrante (id_comitepfa_integrante, fecha_inicio, fecha_fin, estatus_docente, tipo_docente, primer_nombre, primer_apellido, identificacion, comite_nombre, docente_programa, fecha_registro, comite_programa, comite_tipo) FROM stdin;
\.


--
-- Data for Name: horario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.horario (id_horario, bloque_inicio, bloque_fin, id_dia_semana, id_carga_academica) FROM stdin;
\.


--
-- Name: horario_id_horario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.horario_id_horario_seq', 1, false);


--
-- Data for Name: idioma; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.idioma (id_idioma, code, name, nativename) FROM stdin;
278	ab	Abkhaz	аҧсуа
279	aa	Afar	Afaraf
280	af	Afrikaans	Afrikaans
281	ak	Akan	Akan
282	sq	Albanian	Shqip
283	am	Amharic	አማርኛ
284	ar	Arabic	العربية
285	an	Aragonese	Aragonés
286	hy	Armenian	Հայերեն
287	as	Assamese	অসমীয়া
288	av	Avaric	авар мацӀ, магӀарул мацӀ
289	ae	Avestan	avesta
290	ay	Aymara	aymar aru
291	az	Azerbaijani	azərbaycan dili
292	bm	Bambara	bamanankan
293	ba	Bashkir	башҡорт теле
294	eu	Basque	euskara, euskera
295	be	Belarusian	Беларуская
296	bn	Bengali	বাংলা
297	bh	Bihari	भोजपुरी
298	bi	Bislama	Bislama
299	bs	Bosnian	bosanski jezik
300	br	Breton	brezhoneg
301	bg	Bulgarian	български език
302	my	Burmese	ဗမာစာ
303	ca	Catalan; Valencian	Català
304	ch	Chamorro	Chamoru
305	ce	Chechen	нохчийн мотт
306	ny	Chichewa; Chewa; Nyanja	chiCheŵa, chinyanja
307	zh	Chinese	中文 (Zhōngwén), 汉语, 漢語
308	cv	Chuvash	чӑваш чӗлхи
309	kw	Cornish	Kernewek
310	co	Corsican	corsu, lingua corsa
311	cr	Cree	ᓀᐦᐃᔭᐍᐏᐣ
312	hr	Croatian	hrvatski
313	cs	Czech	česky, čeština
314	da	Danish	dansk
315	dv	Divehi; Dhivehi; Maldivian;	ދިވެހި
316	nl	Dutch	Nederlands, Vlaams
317	en	English	English
318	eo	Esperanto	Esperanto
319	et	Estonian	eesti, eesti keel
320	ee	Ewe	Eʋegbe
321	fo	Faroese	føroyskt
322	fj	Fijian	vosa Vakaviti
323	fi	Finnish	suomi, suomen kieli
324	fr	French	français, langue française
325	ff	Fula; Fulah; Pulaar; Pular	Fulfulde, Pulaar, Pular
326	gl	Galician	Galego
327	ka	Georgian	ქართული
328	de	German	Deutsch
329	el	Greek, Modern	Ελληνικά
330	gn	Guaraní	Avañeẽ
331	gu	Gujarati	ગુજરાતી
332	ht	Haitian; Haitian Creole	Kreyòl ayisyen
333	ha	Hausa	Hausa, هَوُسَ
334	he	Hebrew (modern)	עברית
335	hz	Herero	Otjiherero
336	hi	Hindi	हिन्दी, हिंदी
337	ho	Hiri Motu	Hiri Motu
338	hu	Hungarian	Magyar
339	ia	Interlingua	Interlingua
340	id	Indonesian	Bahasa Indonesia
341	ie	Interlingue	Originally called Occidental; then Interlingue after WWII
342	ga	Irish	Gaeilge
343	ig	Igbo	Asụsụ Igbo
344	ik	Inupiaq	Iñupiaq, Iñupiatun
345	io	Ido	Ido
346	is	Icelandic	Íslenska
347	it	Italian	Italiano
348	iu	Inuktitut	ᐃᓄᒃᑎᑐᑦ
349	ja	Japanese	日本語 (にほんご／にっぽんご)
350	jv	Javanese	basa Jawa
351	kl	Kalaallisut, Greenlandic	kalaallisut, kalaallit oqaasii
352	kn	Kannada	ಕನ್ನಡ
353	kr	Kanuri	Kanuri
354	ks	Kashmiri	कश्मीरी, كشميري‎
355	kk	Kazakh	Қазақ тілі
356	km	Khmer	ភាសាខ្មែរ
357	ki	Kikuyu, Gikuyu	Gĩkũyũ
358	rw	Kinyarwanda	Ikinyarwanda
359	ky	Kirghiz, Kyrgyz	кыргыз тили
360	kv	Komi	коми кыв
361	kg	Kongo	KiKongo
362	ko	Korean	한국어 (韓國語), 조선말 (朝鮮語)
363	ku	Kurdish	Kurdî, كوردی‎
364	kj	Kwanyama, Kuanyama	Kuanyama
365	la	Latin	latine, lingua latina
366	lb	Luxembourgish, Letzeburgesch	Lëtzebuergesch
367	lg	Luganda	Luganda
368	li	Limburgish, Limburgan, Limburger	Limburgs
369	ln	Lingala	Lingála
370	lo	Lao	ພາສາລາວ
371	lt	Lithuanian	lietuvių kalba
372	lu	Luba-Katanga	\N
373	lv	Latvian	latviešu valoda
374	gv	Manx	Gaelg, Gailck
375	mk	Macedonian	македонски јазик
376	mg	Malagasy	Malagasy fiteny
377	ms	Malay	bahasa Melayu, بهاس ملايو‎
378	ml	Malayalam	മലയാളം
379	mt	Maltese	Malti
380	mi	Māori	te reo Māori
381	mr	Marathi (Marāṭhī)	मराठी
382	mh	Marshallese	Kajin M̧ajeļ
383	mn	Mongolian	монгол
384	na	Nauru	Ekakairũ Naoero
385	nv	Navajo, Navaho	Diné bizaad, Dinékʼehǰí
386	nb	Norwegian Bokmål	Norsk bokmål
387	nd	North Ndebele	isiNdebele
388	ne	Nepali	नेपाली
389	ng	Ndonga	Owambo
390	nn	Norwegian Nynorsk	Norsk nynorsk
391	no	Norwegian	Norsk
392	ii	Nuosu	ꆈꌠ꒿ Nuosuhxop
393	nr	South Ndebele	isiNdebele
394	oc	Occitan	Occitan
395	oj	Ojibwe, Ojibwa	ᐊᓂᔑᓈᐯᒧᐎᓐ
396	cu	Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic	ѩзыкъ словѣньскъ
397	om	Oromo	Afaan Oromoo
398	or	Oriya	ଓଡ଼ିଆ
399	os	Ossetian, Ossetic	ирон æвзаг
400	pa	Panjabi, Punjabi	ਪੰਜਾਬੀ, پنجابی‎
401	pi	Pāli	पाऴि
402	fa	Persian	فارسی
403	pl	Polish	polski
404	ps	Pashto, Pushto	پښتو
405	pt	Portuguese	Português
406	qu	Quechua	Runa Simi, Kichwa
407	rm	Romansh	rumantsch grischun
408	rn	Kirundi	kiRundi
409	ro	Romanian, Moldavian, Moldovan	română
410	ru	Russian	русский язык
411	sa	Sanskrit (Saṁskṛta)	संस्कृतम्
412	sc	Sardinian	sardu
413	sd	Sindhi	सिन्धी, سنڌي، سندھی‎
414	se	Northern Sami	Davvisámegiella
415	sm	Samoan	gagana faa Samoa
416	sg	Sango	yângâ tî sängö
417	sr	Serbian	српски језик
418	gd	Scottish Gaelic; Gaelic	Gàidhlig
419	sn	Shona	chiShona
420	si	Sinhala, Sinhalese	සිංහල
421	sk	Slovak	slovenčina
422	sl	Slovene	slovenščina
423	so	Somali	Soomaaliga, af Soomaali
424	st	Southern Sotho	Sesotho
425	es	Spanish; Castilian	español, castellano
426	su	Sundanese	Basa Sunda
427	sw	Swahili	Kiswahili
428	ss	Swati	SiSwati
429	sv	Swedish	svenska
430	ta	Tamil	தமிழ்
431	te	Telugu	తెలుగు
432	tg	Tajik	тоҷикӣ, toğikī, تاجیکی‎
433	th	Thai	ไทย
434	ti	Tigrinya	ትግርኛ
435	bo	Tibetan Standard, Tibetan, Central	བོད་ཡིག
436	tk	Turkmen	Türkmen, Түркмен
437	tl	Tagalog	Wikang Tagalog, ᜏᜒᜃᜅ᜔ ᜆᜄᜎᜓᜄ᜔
438	tn	Tswana	Setswana
439	to	Tonga (Tonga Islands)	faka Tonga
440	tr	Turkish	Türkçe
441	ts	Tsonga	Xitsonga
442	tt	Tatar	татарча, tatarça, تاتارچا‎
443	tw	Twi	Twi
444	ty	Tahitian	Reo Tahiti
445	ug	Uighur, Uyghur	Uyƣurqə, ئۇيغۇرچە‎
446	uk	Ukrainian	українська
447	ur	Urdu	اردو
448	uz	Uzbek	zbek, Ўзбек, أۇزبېك‎
449	ve	Venda	Tshivenḓa
450	vi	Vietnamese	Tiếng Việt
451	vo	Volapük	Volapük
452	wa	Walloon	Walon
453	cy	Welsh	Cymraeg
454	wo	Wolof	Wollof
455	fy	Western Frisian	Frysk
456	xh	Xhosa	isiXhosa
457	yi	Yiddish	ייִדיש
458	yo	Yoruba	Yorùbá
459	za	Zhuang, Chuang	Saɯ cueŋƅ, Saw cuengh
\.


--
-- Name: idioma_id_idioma_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.idioma_id_idioma_seq', 21, true);


--
-- Name: idioma_id_idioma_seq1; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.idioma_id_idioma_seq1', 459, true);


--
-- Data for Name: ins_uni_curricular; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ins_uni_curricular (id_ins_uni_curricular, id_inscripcion, id_malla_curricular, nota) FROM stdin;
\.


--
-- Name: ins_uni_curricular_id_ins_uni_curricular_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ins_uni_curricular_id_ins_uni_curricular_seq', 1, false);


--
-- Data for Name: inscripcion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.inscripcion (id_inscripcion, estudiante_id) FROM stdin;
\.


--
-- Data for Name: inscripcion_detalle; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.inscripcion_detalle (id_inscripcion_detalle, codigo, estatus, fecha_ingreso, id_inscripcion, id_carga_academica) FROM stdin;
\.


--
-- Name: inscripcion_detalle_id_inscripcion_detalle_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.inscripcion_detalle_id_inscripcion_detalle_seq', 1, false);


--
-- Name: inscripcion_id_inscripcion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.inscripcion_id_inscripcion_seq', 1, false);


--
-- Data for Name: institucion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.institucion (id_institucion, descripcion, id_pais, id_institucion_tipo) FROM stdin;
1196	INSTITUTO UNIVERSITARIO DE CARACAS	242	1
1197	INSTITUTO DE CIENCIA Y BIOLOGIA MARINA	242	2
1198	UNIVERSIDAD SANTA MARIA	242	1
\.


--
-- Data for Name: institucion_educativa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.institucion_educativa (id_institucion_educativa, descripcion) FROM stdin;
\.


--
-- Name: institucion_educativa_id_institucion_educativa_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.institucion_educativa_id_institucion_educativa_seq', 1, false);


--
-- Name: institucion_id_institucion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.institucion_id_institucion_seq', 1198, true);


--
-- Data for Name: institucion_tipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.institucion_tipo (id_institucion_tipo, descripcion) FROM stdin;
1	Pública
2	Privada
\.


--
-- Name: institucion_tipo_id_institucion_tipo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.institucion_tipo_id_institucion_tipo_seq', 2, true);


--
-- Data for Name: jurado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jurado (id_jurado, id_propuesta_jurado, id_tipo_jurado, id_docente) FROM stdin;
\.


--
-- Name: jurado_id_jurado_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jurado_id_jurado_seq', 512, true);


--
-- Data for Name: libro; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.libro (id_libro, nombre_libro, editorial, isbn_libro, ano_publicacion, ciudad_publicacion, volumenes, nro_paginas, url_libro, id_docente, estatus_verificacion) FROM stdin;
\.


--
-- Name: libro_id_libro_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.libro_id_libro_seq', 4, true);


--
-- Data for Name: linea_investigacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.linea_investigacion (id_linea_investigacion, descripcion, codigo, especificacion, id_nucleo_academico) FROM stdin;
1	linea investigacion 1	569	especificacion 1	1
\.


--
-- Name: linea_investigacion_id_linea_investigacion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.linea_investigacion_id_linea_investigacion_seq', 1, true);


--
-- Data for Name: malla_curricular; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.malla_curricular (id_malla_curricular, descripcion, codigo, salida_intermedia, id_modalidad_malla, id_programa, estatus) FROM stdin;
\.


--
-- Data for Name: malla_curricular_detalle; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.malla_curricular_detalle (id_malla_curricular_detalle, cantidad_tramo, id_tramo, id_malla_curricular, id_trayecto, id_unidad_curricular_detalle) FROM stdin;
\.


--
-- Name: malla_curricular_detalle_id_malla_curricular_detalle_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.malla_curricular_detalle_id_malla_curricular_detalle_seq', 1, false);


--
-- Name: malla_curricular_id_malla_curricular_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.malla_curricular_id_malla_curricular_seq', 1, false);


--
-- Data for Name: memo_solicitud; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.memo_solicitud (id_memo_solicitud, id_documento, id_solicitud_ascenso, estatus) FROM stdin;
\.


--
-- Name: memo_solicitud_id_memo_solicitud_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.memo_solicitud_id_memo_solicitud_seq', 83, true);


--
-- Data for Name: metodologia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.metodologia (id_metodologia, descripcion, id_tipo_metodologia) FROM stdin;
\.


--
-- Name: metodologia_id_metodologia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.metodologia_id_metodologia_seq', 1, false);


--
-- Data for Name: modalidad_estudio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.modalidad_estudio (id_modalidad_estudio, descripcion) FROM stdin;
9	Presencial
10	Semi-Presencial
11	A Distancia
12	Virtual
\.


--
-- Name: modalidad_estudio_id_modalidad_estudio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.modalidad_estudio_id_modalidad_estudio_seq', 12, true);


--
-- Data for Name: modalidad_malla; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.modalidad_malla (id_modalidad_malla, descripcion) FROM stdin;
3	Diurna
4	Nocturna
\.


--
-- Name: modalidad_malla_id_modalidad_malla_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.modalidad_malla_id_modalidad_malla_seq', 4, true);


--
-- Data for Name: modulo_activacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.modulo_activacion (id_modulo_activacion, fecha_inicio, fecha_fin, id_oferta_academica, tipo_activacion, id_aldea, descripcion) FROM stdin;
\.


--
-- Name: modulo_activacion_ingreso_id_modulo_activacion_ingreso_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.modulo_activacion_ingreso_id_modulo_activacion_ingreso_seq', 1, false);


--
-- Data for Name: municipio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.municipio (id_municipio, descripcion, codigo, id_estado) FROM stdin;
1	Libertador	010100	1
2	Autónomo Alto Orinoco	020100	2
3	Autónomo Atabapo	020200	2
4	Autónomo Atures	020300	2
5	Autónomo Autana	020400	2
6	Autónomo Maroa	020500	2
7	Manapiare	020600	2
8	Río Negro	020700	2
9	Anaco	030100	3
10	Aragua	030200	3
11	Fernando de Peñalver	030300	3
12	Francisco del Carmen Carvajal	030400	3
13	Francisco de Miranda	030500	3
14	Guanta	030600	3
15	Independencia	030700	3
16	Juan Antonio Sotillo	030800	3
17	Juan Manuel Cajigal	030900	3
18	Municipio José Gregorio Monagas	031000	3
19	Libertad	031100	3
20	Municipio Manuel Ezequiel Bruzual	031200	3
21	Pedro María Fréites	031300	3
22	Píritu	031400	3
23	San José de Guanipa	031500	3
24	San Juan de Capistrano	031600	3
25	Santa Ana	031700	3
26	Simón Bolíva	031800	3
27	Simón Rodríguez	031900	3
28	Sir Arthur Mc Gregor	032000	3
29	Turístico Diego Bautista Urbaneja	032100	3
30	Achaguas	040100	4
31	Biruaca	040200	4
32	Muñoz	040300	4
33	Páez	040400	4
34	Pedro Camejo	040500	4
35	Rómulo Gallegos 1	040600	4
36	San Fernando	040700	4
37	Camatagua	050200	5
38	Girardot	050300	5
39	José Ángel Lamas	050400	5
40	José Félix Ribas	050500	5
41	José Rafael Revenga	050600	5
42	Libertador	050700	5
43	Mario Briceño Iragorry	050800	5
44	San Casimiro	050900	5
45	San Sebastián	051000	5
46	Santiago Mariño	051100	5
47	Santos Michelena	051200	5
48	Sucre	051300	5
49	Tovar	051400	5
50	Urdaneta	051500	5
51	Zamora	051600	5
52	Francisco Linares Alcántara	051700	5
53	Ocumare de la Costa de Oro	051800	5
54	Alberto Arvelo Torrealba	060100	6
55	Antonio José de Sucre	060200	6
56	Arismendi	060300	6
57	Barinas	060400	6
58	Bolívar	060500	6
59	Cruz Paredes	050600	6
60	Ezequiel Zamora	060700	6
61	Obispos	060800	6
62	Pedaza	060900	6
63	Rojas	061000	6
64	Sosa	061100	6
65	Andrés Eloy Blanco	061200	6
66	Caroní	070100	7
67	Cedeño	070200	7
68	El Callao	070300	7
69	Gran Sabana	070401	7
70	Heres	070400	7
71	Piar	070500	7
72	Bolivariano Angostura	070600	7
73	Roscio	070700	7
74	Sifontes	070800	7
75	Sucre	070900	7
76	Parede Pedro Chien	071000	7
77	Bejuma	080100	8
78	Carlos Aarvlo	080200	8
79	Diego Ibarra	080300	8
80	Guacara	080401	8
81	Juan Jóse Mora	080400	8
82	Libertador	080500	8
83	Los Guayos	080600	8
84	Miranda	080700	8
85	Montalbán	080800	8
86	Naguanagua	080900	8
87	San Diego	081100	8
88	San Joaquín	081200	8
89	Valencia	081300	8
90	Anzoátegui	090100	9
91	Tinaquillo	090200	9
92	Girardot	090300	9
93	Lima Blanco	090400	9
94	Pao de San Juan Bautista	090500	9
95	Ricaurte	090600	9
96	Rómulo Gallegos	090700	9
97	Ezequiel Zamora	090800	9
98	Tinaco	090900	9
99	Antonio Diaz	100100	10
100	Casacoima	100200	10
101	Pedernales	100300	10
102	Tucupita	100400	10
103	Acosta	110100	11
104	Bolívar	110200	11
105	Buchivacoa	110300	11
106	Cacique Manaure	110400	11
107	Carirubana	110500	11
108	Colina	110600	11
109	Dabajuro	110700	11
110	Democracia	110800	11
111	Falcón	110900	11
112	Federación	111000	11
113	Jacura	111100	11
114	Los Taques	111200	11
115	Mauroa	111300	11
116	Miranda	111400	11
117	Monseñor Iturriza	111500	11
118	Palmasola	111600	11
119	Petit	111700	11
120	Píritu	111800	11
121	San Francisco	111900	11
122	silva	111200	11
123	Sucre	112100	11
124	Tocópero	112200	11
125	Unión	112300	11
126	Urumaco	112400	11
127	Zamora	112500	11
128	Camaguán	120100	12
129	Chaguaramas	120200	12
130	El Socorro	120300	12
131	San Gerónimo de Guayabal	120400	12
132	Leonardo Infante	120500	12
133	Las Mercedes	120600	12
134	Julián Mellado	120700	12
135	Francisco de Miranda	120800	12
136	José Tadeo Monagas	120900	12
137	Ortiz	121000	12
138	José Félix Ribas 	121100	12
139	Germán Roscio	121200	12
140	San José de Guaribe	121300	12
141	Santa María de Ipire	121400	12
142	Pdero Zaraza	121500	12
143	Andrés Eloy Blanco	130100	13
144	Crespo	131200	13
145	Iribarren	130300	13
146	Jiménez	130400	13
147	Morán	130500	13
148	Palavecino	130600	13
149	Simón Planas	130700	13
150	Torres	130800	13
151	Urdaneta	130900	13
152	Alberto Adriani	140100	14
153	Andés Bello	140200	14
154	Antonio Pinto Salinas	140300	14
155	Aricagua	140400	14
156	Arzobispo Chacón	140500	14
157	Campo Elías	140600	14
158	Caracciolo Parra Olmedo	140700	14
159	Cardenal Quintero	140800	14
160	Guaraque Guaraque	140900	14
161	Julio César Salas	141000	14
162	Justo Briceño	141100	14
163	Libertador	141200	14
164	Miranda	141300	14
165	Obispo Ramos Lora	141400	14
166	Padre Noguera	141500	14
167	Pueblo Llano	141600	14
168	Rangel	141700	14
169	Rivas Dávila	141800	14
170	Santos Marquina	141900	14
171	Sucre	142000	14
172	Tovar	142100	14
173	Tulio Febres Cordero	142200	14
174	Zea	142300	14
175	Acevedo	150100	15
176	Andrés Bello	150200	15
177	Baruta	150300	15
178	Brión	150400	15
179	Buroz	150500	15
180	Carrizal	150600	15
181	Chacao	150700	15
182	Cristóbal Rojas	150800	15
183	El Hatillo	150900	15
184	Bolivariano Guaicaipuro	151000	15
185	Independencia	151100	15
186	Lander	151200	15
187	Los Salias	151300	15
188	Páez	151400	15
189	Paz Castillo	151500	15
190	Pedro Gual	151600	15
191	Plaza	151700	15
192	Simón Bolivar	151800	15
193	Sucre	151900	15
194	Urdaneta	152000	15
195	Zamora	152100	15
196	Acosta	160100	16
197	Aguasay	160200	16
198	Bolívar	160300	16
199	Caripe	160400	16
200	Cedeño	160500	16
201	Ezequiel Zamora	160600	16
202	Libertador	160700	16
203	Maturín	160800	16
204	Piar	160900	16
205	Punceres	161000	16
206	Santa Barbára	161100	16
207	Sotillo	161200	16
208	Uracoa	161300	16
209	Antolín del Campo	170100	17
210	Arismendi	170200	17
211	Díaz	170300	17
212	García	170400	1
213	Gómez	170500	1
214	Maneiro	170500	17
215	Marcano	170600	17
216	Mariño	170700	17
217	Península de Marcano	170800	17
218	Tubores	170900	17
219	Villalba	171000	17
220	Agua Blanca	180100	18
221	Araure	180200	18
222	Esteller	180300	18
223	Guanare	180400	18
224	Guanarito	180500	18
225	Monseñor José Vicente de Unda	180600	18
226	Ospino	180700	18
227	Páez	180800	18
228	Papelón	180900	18
229	San Genaro de Boconoito	181000	18
230	San Rafael de Onoto	181100	18
231	Santa Rosalía	181200	18
232	Sucre	181300	18
233	Turén	181400	18
234	Andrés Eloy Blanco	190100	19
235	Andrés Mata	190200	19
236	Arismendi	190300	19
237	Benítez	190400	19
238	Bermúdez	190500	19
239	Bolívar	190600	19
240	Cajigal	190700	19
241	Cruz Salmerón Acosta	190800	19
242	Libertador	190900	19
243	Mariño	191000	19
244	Mejías	191100	19
245	Montes	191200	19
246	Ribero	191300	19
247	Sucre	191400	19
248	Valdez	191500	19
249	Andres Bello	200100	20
250	Antonio Rómulo Acosta	200200	20
251	Ayacucho	200300	20
252	Bolívar	200400	20
253	Cárdenas	200500	20
254	Córdoba	200600	20
255	Fernádez Feo	200700	20
256	Francisco de Miranda	200800	20
257	García de Hevia	200900	20
258	Guásimos	201000	20
259	Independencia	201100	20
260	Jáuregui	201200	20
261	José María Vargas	201300	20
262	Juanín	201400	20
263	Libertad	201500	20
264	Libertador	201600	20
265	Lobatera	201700	20
266	Michelena	201800	20
267	Panamericano	201900	20
268	Pedro María Ureña	202000	20
269	Rafael Urdaneta	202100	20
270	Samuel Darío Maldonado	202200	20
271	San Cristóbal	202300	20
272	Seboruco	202400	20
273	Simón Rodríguez	202500	20
274	Sucre	202600	20
275	Torbes	202700	20
276	Uribante	202800	20
277	San Judas Tadeo	202900	20
278	Andés Bello	210100	21
279	Boconó	210200	21
280	Bolívar	210300	21
281	Candelaria	210400	21
282	Carache	210500	21
283	Escuque	210600	21
284	José Felipe Márquez Cañizales	210700	21
285	Juan Vicente Campo Elías	210800	21
286	La ceiba	210900	21
287	Miranda	212000	21
288	Monte Carmlo	212100	21
289	Motatán	212200	21
290	Pámpan	212300	21
291	Pampanito	212400	21
292	Rafael Rangel	212500	21
293	San Rafael de Carvajal	212700	21
294	Sucre	212800	21
295	Trujillo	212900	21
296	Urdaneta	213000	21
297	Valera	213100	21
298	Arístides Bastidas	220100	22
299	Bolívar	220200	22
300	Bruzual	220300	22
301	Independencia	220500	22
302	José Antonio Páez	220600	22
303	Trinidad	220700	22
304	Manuel Monge	220800	22
305	Nirgua	220900	22
306	Peña	221000	22
307	San Felipe	221100	22
308	Sucre	221200	22
309	Urachiche	221300	22
310	Veroes	221400	22
311	Almirante Padilla	230100	23
312	Baralt	230200	23
313	Cabimas	230300	23
314	Catatumbo	230400	23
315	Colón	230500	23
316	Francisco Javier Pulgar	230600	23
317	Jesús Enrique Lossada	230700	23
318	José María Semprúm	230800	23
319	La Cañada de Urdaneta	230900	23
320	Lagunillas	231000	23
321	Machiques de Perijá	231100	23
322	Mara	231200	23
323	Maracaibo	231300	23
324	Miranda	231400	23
325	Indígena Bolivariano Guajira	231500	23
326	Rosario de Perijá	231600	23
327	San Francisco	231700	23
328	Santa Rita	231800	23
329	Simón Bolívar	231900	23
330	Sucre	232000	23
331	Valmore Rodríguez	232100	23
332	Vargas	240100	24
\.


--
-- Name: municipio_id_municipio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.municipio_id_municipio_seq', 332, true);


--
-- Data for Name: nacionalidad; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.nacionalidad (id_nacionalidad, descripcion, id_pais) FROM stdin;
1	Venezolano	242
2	Colombiano	45
\.


--
-- Name: nacionalidad_id_nacionalidad_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.nacionalidad_id_nacionalidad_seq', 2, true);


--
-- Data for Name: nivel_academico; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.nivel_academico (id_nivel_academico, descripcion, codigo_ine) FROM stdin;
19	Educación Inicial	10
20	Educación Primaria	2122
21	Desarrollo Personal y Laboral no profesional	30
22	Educación Media	4143
23	Educación Técnica Superior	50
24	Educación Profesional Universitaria	60
25	Maestrías	70
26	Especialización	80
27	Doctorados	90
\.


--
-- Name: nivel_academico_id_nivel_academico_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.nivel_academico_id_nivel_academico_seq', 27, true);


--
-- Data for Name: nomina_docente_sigad; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.nomina_docente_sigad (id_nomina_docente_sigad, cedula, nombres, nacionalidad, fecha_nacimiento, pais_nacimiento, estado_nacimiento, genero, estado_civil, telefono, correo_electronico, direccion, numero_hijos, cargo, clasificacion_docente, fecha_ingreso, dedicacion, estatus) FROM stdin;
1	26334542	anthony de jesus, frankis escalona	Venezolano	1998-09-21	Venezuela	Miranda	Masculino	Soltero	4142425931	anthony26334@gmail.com	PALO VERDE	0	AUXILIAR I	DOCENTE	2019-03-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
2	1488895	REQUENA, MIGUEL ANTONIO	Venezolano 	1940-05-09	\N	\N	Masculino 	Soltero 	\N	requenami@gmail.com 	CALLE CUCHIVERO,QTA MORICHAL URB.PIEDRA AZUL BARUTA 	0	ASOCIADO	CONTRATADO	2018-02-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
3	1526247	PADRON RIVAS, MIGUEL ANGEL	Venezolano 	1937-04-14	\N	\N	Masculino 	Soltero 	\N	mapadron@ubv.edu.ve 	AV. MARIA TERESA TORO, N 17. URB. LAS ACALCIAS 	0	ASOCIADO	CONTRATADO	2015-09-17	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
5	1566387	OROZCO DE RAMOS, CARMEN AMADA	Venezolano 	1953-12-07	\N	\N	Femenino 	Soltero 	\N	corozco@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2010-05-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
6	1928728	HACKETT BRICEÑO, EVELYN ELIZABETH	Venezolano 	1943-10-21	\N	\N	Femenino 	Soltero 	\N	ehackett@ubv.edu.ve 	CALLE MISTER WILLIAM HACKETT - VIA CANDELARIA - SECTOR CRUZ VERDE N 1-28 TRUJILLO 	0	AGREGADO	DOCENTE	2006-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
7	2073443	ESCALONA  PEREZ, MARIANO  DE LA TRINIDA	Venezolano 	1943-10-16	\N	\N	Masculino 	Soltero 	\N	mescalona@ubv.edu.ve 	EL ENCANTO. CALLE BERTORELLI. EDI. CARAO. APTO 12D-1.PISO 12 	1	INSTRUCTOR	DOCENTE	2005-11-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
8	2140931	GALLEGOS DE LOSADA, ALICIA MARGARITA	Venezolano 	1941-11-11	\N	\N	Femenino 	Casado 	\N	agallegos@ubv.edu.ve 	URB. LA FLORIDA, AV. LOS SAMANES, RES. AKARANTAY, 8-A 	0	ASOCIADO	CONTRATADO	2015-09-17	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
9	2359988	HERNANDEZ FLORES, MAXIMO ANTONIO	Venezolano 	1941-03-22	\N	\N	Masculino 	Soltero 	\N	mahernandezf@ubv.edu.ve 	CARACAS 	0	TITULAR	CONTRATADO	2013-10-08	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
10	2476385	SANTAELLA, VICTORIA YUSMIL	Venezolano 	1954-11-28	\N	\N	Femenino 	Soltero 	\N	vsantaella@ubv.edu.ve 	LOS CORRALES. 1ERA AV. CASA 34 	0	INSTRUCTOR	DOCENTE	2007-10-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
11	2476777	RAMOS ARAQUE, JUAN ALEXIS	Venezolano 	1952-12-17	\N	\N	Masculino 	Soltero 	\N	jaramos@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2006-07-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
12	2522605	UZCATEGUI, JESÚS ORLANDO	Venezolano 	1954-10-04	\N	\N	Masculino 	Divorciado 	\N	juzcategui@ubv.edu.ve 	SECTOR2, CALLE 4, CASA 4, URB. VALLE LINDO 	0	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
13	2554127	RIVAS VIVAS, MIZAEL MARTIN	Venezolano 	1953-12-15	\N	\N	Masculino 	Soltero 	\N	mmrivas@ubv.edu.ve 	EDIFICIO 6 APRO 9 VEREDA N8 MONTEREY SECTOR LA GUAYANA 	0	COORDINADOR PFG INSTRUCTOR	DOCENTE	2010-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
14	2767570	DURAN VIVAS, JOSE HERMES	Venezolano 	1957-06-11	\N	\N	Masculino 	Soltero 	\N	jhduran@ubv.edu.ve 	FINAL SEGUNDA AV.DE ARTIGAS CASA N 10.SAN JUAN 	1	INSTRUCTOR	DOCENTE	2012-02-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
15	2864485	RAMOS RODRIGUEZ, MARY REINA	Venezolano 	1951-10-16	\N	\N	Femenino 	Soltero 	\N	mrramos@ubv.edu.ve 	EDF. STA. ROSA, PISO 1,. APTO1-D, AV. ROMULO GALLEGO 	0	AGREGADO	DOCENTE	2005-04-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
16	2906327	BENITEZ RONDON, ORLANDO DE JESÚS	Venezolano 	1945-02-05	\N	\N	Masculino 	Divorciado 	\N	obenitez@ubv.edu.ve 	AV JESUS SOTO CONJUBNTO RES. ANGOSTURA EDIF A-1 APTO N 121 	0	INSTRUCTOR	DOCENTE	2007-05-21	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
17	2956814	ROMERO ROSA, LENIN ROBERTO	Venezolano 	1945-02-11	\N	\N	Masculino 	Soltero 	\N	lrromero@ubv.edu.ve 	URB. EL BOSQUE. AV. LIBERTADOR. RES. IMATACA. PISO 1-11 	0	AGREGADO	DOCENTE	2007-04-23	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
18	3026619	FIGUEROA DE QUINTERO, ROSA DOMINGA	Venezolano 	1947-08-30	\N	\N	Femenino 	Soltero 	\N	rfigueroa@ubv.edu.ve 	CARACAS 	0	TITULAR	CONTRATADO	2011-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
19	3046277	SAGARAY MERCHAN, JUAN OSWALDO	Venezolano 	1950-07-16	\N	\N	Masculino 	Casado 	\N	jsagaray@ubv.edu.ve 	URB. EZEQUIEL ZAMORA CASA N 47 	0	INSTRUCTOR	DOCENTE	2007-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
20	3088118	RAMÍREZ HERRERA, OSWALDO ANTONIO	Venezolano 	1945-08-17	\N	\N	Masculino 	Soltero 	\N	oramirez@ubv.edu.ve 	APTO 1-D LA CANDELARIA, PARQUE CARABOBO 	0	INSTRUCTOR	DOCENTE	2005-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
21	3130439	QUINTERO LOPEZ, ANULFO DEL VALLE	Venezolano 	1949-11-19	\N	\N	Masculino 	Soltero 	\N	aquintero@ubv.edu.ve 	UBR. PEDRO BRICEÑO MENDEZ CALLE 15-17 	4	COORDINADOR PFG ASISTENTE	DOCENTE	2006-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
22	3158060	BRION ESPINA, WIENFRIED APOLINAR 	Venezolano 	1944-07-23	\N	\N	Masculino 	Divorciado 	\N	wbrion@ubv.edu.ve 	URB PALMA REAL CONJ RES AGUA DE CANTO N19 	0	AGREGADO	CONTRATADO	2014-02-17	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
23	3177244	PARAVISINI GARCIA, DAVID RAFAEL	Venezolano 	1948-10-23	\N	\N	Masculino 	Soltero 	\N	dparavisini@ubv.edu.ve 	CARACAS 	1	AGREGADO	DOCENTE	2009-10-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
24	3246979	UGARTE CAMPOY, FRANCISCO RAMÓN	Venezolano 	1949-01-14	\N	\N	Masculino 	Soltero 	\N	fugarte@ubv.edu.ve 	EL PARAISO CALLE MACHADO EDIF KYOTO CASA 113 	0	INSTRUCTOR	DOCENTE	2007-11-27	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
25	3253693	TOLEDO CASTRO, ALEXIS JOSE	Venezolano 	1949-11-29	\N	\N	Masculino 	Soltero 	\N	atoledo@ubv.edu.ve 	URB.SOLIDARIDAD, CASA 43, PARROQUIA URIMARE, EDO.VARGAS 	0	INSTRUCTOR	CONTRATADO	2014-01-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
26	3449814	MÁRQUEZ ESCALONA, PEDRO ARMENDUL	Venezolano 	1953-06-07	\N	\N	Masculino 	Soltero 	\N	 	FIONAL CALLE PPAL N# 72, PEDRAZA LA VIEJA. MUNICIPIO EZEQUIEL ZAMORA 	0	INSTRUCTOR	DOCENTE	2006-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
27	3548732	GONCALVES VERDU, FREDDY	Venezolano 	1947-12-27	\N	\N	Masculino 	Soltero 	\N	fgoncalves@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2010-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
28	3551474	MEDINA VALLADOLID, FERNANDO RAMON	Venezolano 	1951-04-13	\N	\N	Masculino 	Casado 	\N	frmedina@ubv.edu.ve 	AV JOSE ANGEL LAMAS #17 PARROQUIA SAN JUAN 	0	ASISTENTE	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
29	3654318	HERNÁNDEZ JARAMILLO, TERESA DE JESÚS	Venezolano 	1952-10-03	\N	\N	Femenino 	Soltero 	\N	thernandez@ubv.edu.ve 	URBANIZACION VILA ASIA, CALLE CAMBOYA, MAZ. 10, CASA NUM. 24, PTO ORDAZ 	0	INSTRUCTOR	DOCENTE	2007-10-29	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
30	3662320	SEPULVEDA BRITO, ROSA LILA	Venezolano 	1949-06-11	\N	\N	Femenino 	Soltero 	\N	rsepulveda@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2011-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
31	3694187	WEKY MARCHAN, ELY JOSE	Venezolano 	1947-01-23	\N	\N	Masculino 	Soltero 	\N	 	LA FLORESTA CALLE 5 NUMERO 50 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
32	3700266	BECERRA MOROCOIMA, OSMERY DEL VALLE	Venezolano 	1951-08-04	\N	\N	Femenino 	Soltero 	\N	odbecerra@ubv.edu.ve 	URB. ALBERTO RACEL 	0	TITULAR	DOCENTE	2008-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
33	3708032	TORREALBA ALEJOS, JOSE FRANCISCO	Venezolano 	1950-09-02	\N	\N	Masculino 	Soltero 	\N	jftorrealba@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2010-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
34	3713002	GONZALEZ, HERNÁN FRANCISCO	Venezolano 	1949-10-10	\N	\N	Masculino 	Soltero 	\N	hegonzalez@ubv.edu.ve 	EL PARAISO AVDA PAEZ EDIF VILLA PAEZ 	0	INSTRUCTOR	DOCENTE	2008-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1637	9586503	RUIZ ATACHO, ARGENIS JESÚS	Venezolano 	1967-08-02	\N	\N	Masculino 	Soltero 	\N	ajruiz@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2005-05-16	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
199	6132548	BORREGO DE GAMEZ, TRINA MARIA 	Venezolano 	1963-02-26	\N	\N	Femenino 	Casado 	\N	tborrego@ubv.edu.ve 	EL PARAISO 	0	INSTRUCTOR	CONTRATADO	2012-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
35	3716305	GOUVEIA DE SOUSA, MARÍA DE LA CONCEPCION	Venezolano 	1948-09-14	\N	\N	Femenino 	Soltero 	\N	mgouveia@ubv.edu.ve 	URBANIZACION LLANO ALTO CONJUNTO RESIDENCIAL CARMEL CALLE E QUINTA E-23 	0	INSTRUCTOR	DOCENTE	2008-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
36	3719858	GUEDEZ GONZALEZ, PEDRO ANTONIO	Venezolano 	1949-06-15	\N	\N	Masculino 	Casado 	\N	paguedez@ubv.edu.ve 	NUEVA CASARAPA RESD CAÑAVERAL PISO 3 APTO G-44 	0	ASISTENTE	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
37	3751154	SANCHEZ  HERNANDEZ, BEATRIZ ELENA	Venezolano 	1950-01-07	\N	\N	Femenino 	Soltero 	\N	besanchez@ubv.edu.ve 	CARACAS 	1	ASISTENTE	DOCENTE	2009-05-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
38	3778207	CORONADO FERNÁNDEZ, CARMEN AURORA	Venezolano 	1950-12-16	\N	\N	Femenino 	Soltero 	\N	ccoronado@ubv.edu.ve 	SECTOR PUERTO RICO CALLE 62 N31-64 	1	INSTRUCTOR	DOCENTE	2007-09-17	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
39	3820785	RUIZ HERNANDEZ, MIGDALIA JOSEFINA	Venezolano 	1951-06-07	\N	\N	Femenino 	Soltero 	\N	mruiz@ubv.edu.ve 	URBANIZACION REFUGIO SANTA BARBARA, CASA 108, SECTOR RETEN ABAJO, 	0	ASISTENTE	DOCENTE	2006-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
40	3881078	CENTENO DE FUENTES, DEYCI AMELIA	Venezolano 	1949-05-26	\N	\N	Femenino 	Casado 	\N	dcenteno@ubv.edu.ve 	BLOQUE 54-E-208 SIERRA MAESTRA 23 DE ENERO 	0	INSTRUCTOR	CONTRATADO	2016-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
41	3881319	LUNA, OSCAR ALBERTO	Venezolano 	1951-12-07	\N	\N	Masculino 	Soltero 	\N	oluna@ubv.edu.ve 	CCS 	0	INSTRUCTOR	DOCENTE	2011-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
42	3891039	FIGUERA DE SERRANO, XIOMARA RAFAELA	Venezolano 	1950-10-02	\N	\N	Femenino 	Casado 	\N	 	CALLE VIEJA CRUCE CON CALLE EL GRUPO SAN ANTONIO 	0	INSTRUCTOR	CONTRATADO	2018-01-22	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
43	3892052	GUZMAN SANTANA, LUIS GAUTAMA	Venezolano 	1953-08-25	\N	\N	Masculino 	Soltero 	\N	lguzman@ubv.edu.ve 	AV. TAMANACO ENTRE CALLE CARIBE YPARAMACAY, EDIF CHISTIAN, PISO 4, APTO # 16 UR, EL LLANITO 	0	INSTRUCTOR	DOCENTE	2007-10-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
44	3895647	RUIZ, EGLEE	Venezolano 	1952-03-29	\N	\N	Femenino 	Soltero 	\N	eruiz@ubv.edu.ve 	CALLE LA GOMERA RES VILLA DEL CARMEN, EL AMARILLO, SAN ANTONIO DE LOS ALTOS 	0	ASISTENTE	DOCENTE	2005-11-28	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
45	3916982	MARTÍNEZ CARICOTE, WILFREDO JESÚS	Venezolano 	1951-11-03	\N	\N	Masculino 	Casado 	\N	wjmartinezc@ubv.edu.ve 	URB. LA ISABELICA, SECTOR 01, VEREDA 08, CASA N 08 	0	INSTRUCTOR	DOCENTE	2007-09-17	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
46	3928928	VIRLA ARAMBULO, HENRY JESÚS	Venezolano 	1952-01-22	\N	\N	Masculino 	Soltero 	\N	hvirla@ubv.edu.ve 	CALLE 105 A N 19 G-165 SECTOR POMONA 	1	ASISTENTE	DOCENTE	2005-04-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
47	3943141	ACOSTA SANCHEZ, LUIS BELTRAN	Venezolano 	1952-10-10	\N	\N	Masculino 	Casado 	\N	lbacosta@ubv.edu.ve 	AV.JOSE ANTONIA PAEZ EL PARAISO, EDIF.LOS FRAILES PISO 11 APTO 11-A. 	0	INSTRUCTOR	CONTRATADO	2016-01-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
48	3944945	MARTÍNEZ GARCIA, JACINTO ANTONIO	Venezolano 	1950-08-17	\N	\N	Masculino 	Soltero 	\N	jamartinez@ubv.edu.ve 	PINEDA A PARAISO CENTRO RESIDENCIAL PARAISO ALTAGRACIA CARACAS 	1	INSTRUCTOR	DOCENTE	2005-10-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
49	3955733	MAROT, JUAN	Venezolano 	1953-06-24	\N	\N	Masculino 	Soltero 	\N	jumarot@ubv.edu.ve 	CALLE TUCUPITA # 74 TUCUPITA - DELTA AMACURO 	0	INSTRUCTOR	DOCENTE	2006-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
50	3981108	ZAVARCE, CARLOS	Venezolano 	1953-10-16	\N	\N	Masculino 	Soltero 	\N	czavarce@ubv.edu.ve 	EDIF.. MONTECRISTO PISO 2 APTO, 3 LA CANDELARIA CARACAS 	0	INSTRUCTOR	DOCENTE	2005-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
51	4023903	REYES OLIMPIO, TOMASA DEL CARMEN	Venezolano 	1953-07-16	\N	\N	Femenino 	Soltero 	\N	tdreyes@ubv.edu.ve 	URB. LAS CAYENAS, MANZANA 01 N 32. VIA LA CRUZ. 	0	INSTRUCTOR	DOCENTE	2007-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
52	4026576	RAUSSEO RINCONES, CARLOS MANUEL	Venezolano 	1951-11-18	\N	\N	Masculino 	Divorciado 	\N	crausseo@ubv.edu.ve 	CARRETERA 6, ANTIGUA CALLE BERMUDEZ, N 4. MATURIN EDO. MONAGAS 	1	COORDINADOR NACIONAL AGREGADO	DOCENTE	2005-05-02	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
53	4027699	ROMERO SOLEDAD, HÉCTOR JOSÉ	Venezolano 	1948-03-02	\N	\N	Masculino 	Soltero 	\N	hromero@ubv.edu.ve 	AV. GUASIMA DELTA AMACURO 	2	ASISTENTE	DOCENTE	2006-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
54	4028058	CEDEÑO CAMPO, JOSE ANTONIO	Venezolano 	1954-06-20	\N	\N	Masculino 	Soltero 	\N	jacedeno@ubv.edu.ve 	URB LAS TRINITARIAS CALLE 4 N 172 	1	INSTRUCTOR	CONTRATADO	2015-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
55	4045344	QUIJADA SUNIAGA, ALBERTO ENRIQUE	Venezolano 	1953-05-24	\N	\N	Masculino 	Casado 	\N	aequijada@ubv.edu.ve 	URB. RAUL LEONI 01 CALLE# 5 CASA# 8-10 SAN RAFAEL TUCUPITA DELTA AMACURO 	2	ASISTENTE	DOCENTE	2006-07-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
56	4061710	BRICEÑO DE OSORIO, ALBA ROSA	Venezolano 	1953-05-05	\N	\N	Femenino 	Casado 	\N	abriceno@ubv.edu.ve 	AV. RAFAEL LEONI -  EDIF. LUIGI-APTO B-2, URB. CASANOVA 	0	ASOCIADO	DOCENTE	2006-11-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
57	4075106	PADRON  CERRADES, ROSA COROMOTO	Venezolano 	1952-12-31	\N	\N	Femenino 	Soltero 	\N	rpadron@ubv.edu.ve 	PARROQUIA ALTAGRACIA, AV. NORTE 12, CALLE CEIBA A DELICIAS, RESIDENCI CEIBA, PISO 3, APTO. 3D 	0	ASISTENTE	DOCENTE	2005-10-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
58	4082754	HANSON FLORES, MARYANN DEL CARMEN	Venezolano 	1951-05-10	\N	\N	Femenino 	Soltero 	\N	mhanson@ubv.edu.ve 	LA PASTORA 	0	ASISTENTE	CONTRATADO	2018-04-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
59	4087466	GONZALEZ NIETO, GUSTAVO JOSE	Venezolano 	1965-06-16	\N	\N	Masculino 	Soltero 	\N	gjgonzalez@ubv.edu.ve 	AV,PRINCIPAL DE BELLA VISTA, CASA CINARAL 	0	TITULAR	CONTRATADO	2017-01-09	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
60	4090596	BELLAZZINI CECCONI, GUIDO	Venezolano 	1956-05-03	\N	\N	Masculino 	Casado 	\N	gbellazzini@ubv.edu.ve 	PIRINEOS I LOTE H URB RIO ZUNIGA CALLE N 5 CASA Z-05 	0	INSTRUCTOR	DOCENTE	2011-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
61	4121612	FIGUERA, EDGAR APOLINAR	Venezolano 	1955-01-08	\N	\N	Masculino 	Soltero 	\N	edfiguera@ubv.edu.ve 	PARQUE CENTRAL EDF TAJAMAR PISO 16 -J EL CONDE MUNICIPIO LIBERTADOR 	1	ASOCIADO	DOCENTE	2004-03-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
62	4129419	HERRERA MORENO, FERNANDO ANTONIO	Venezolano 	1951-11-22	\N	\N	Masculino 	Soltero 	\N	fherrera@ubv.edu.ve 	CARACAS 	0	ASISTENTE	DOCENTE	2011-09-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
63	4138909	LUGO DE  GUEVARA, EMILIA SOBELLA	Venezolano 	1956-09-13	\N	\N	Femenino 	Soltero 	\N	eslugo@ubv.edu.ve 	CASCO CENTRAL PAEZ  CASA N43 	0	INSTRUCTOR	DOCENTE	2007-11-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
64	4145112	RAYDAN, CARMELO	Venezolano 	1952-03-07	\N	\N	Masculino 	Soltero 	\N	craydan@ubv.edu.ve 	CARACAS 	0	AGREGADO	DOCENTE	2004-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
65	4165384	VIVAS SUAREZ, ALVARO	Venezolano 	1950-07-26	\N	\N	Masculino 	Soltero 	\N	avivas@ubv.edu.ve 	AV. PRINCIPAL PLAYA GRANDE, QTA. SANTA BARBARA 419 	0	INSTRUCTOR	DOCENTE	2007-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
66	4169561	EGUI CACHUT, MARÍA EUGENIA	Venezolano 	1953-09-12	\N	\N	Femenino 	Soltero 	\N	megui@ubv.edu.ve 	SEGUNDA CDALLE SABANA DEL BANCO # 20 LA PASTORA 	0	ASISTENTE	DOCENTE	2004-05-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
67	4173168	LÓPEZ, AMALIO RAFAEL	Venezolano 	1951-12-14	\N	\N	Masculino 	Soltero 	\N	alopez@ubv.edu.ve 	CLIONAS DE BELLO MONTE VOLTAIRE EDF JARDIN BELLO MONTE 	0	ASISTENTE	DOCENTE	2005-02-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
68	4180275	GÓMEZ LUGO, ARGENIS REGINO	Venezolano 	1954-12-31	\N	\N	Masculino 	Divorciado 	\N	argomez@ubv.edu.ve 	COMUNIDAD CARDON AV. 13, CASA 8- 214 	1	INSTRUCTOR	DOCENTE	2007-04-30	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
69	4195121	RODRÍGUEZ CASTILLO, OSCAR ANTONIO	Venezolano 	1955-12-02	\N	\N	Masculino 	Soltero 	\N	oarodriguezc@ubv.edu.ve 	URB. MERECURE AV.1 N 2 BIRUACA 	0	INSTRUCTOR	DOCENTE	2008-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
70	4216636	PAREJO ABREU, EINSTEIN RAMON	Venezolano 	1956-09-19	\N	\N	Masculino 	Soltero 	\N	eparejo@ubv.edu.ve 	CARACAS 	0	ASOCIADO	DOCENTE	2009-10-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
71	4270868	LEDEZMA PALACIOS, MIRIAM VICTORIA	Venezolano 	1953-11-17	\N	\N	Femenino 	Soltero 	\N	mvledezma@ubv.edu.ve 	PUENTE YANEZ A SOCCORRO, EDF. MONTECRISTO PISO 2 APTO. 3 LA CANDELARIA 	0	ASISTENTE	DOCENTE	2005-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
72	4277990	RODRIGUEZ PEÑA, VICTOR MANUEL	Venezolano 	1953-02-25	\N	\N	Masculino 	Soltero 	\N	vrodriguez@ubv.edu.ve 	URB.JOSE DE SAN MARTIN, UDI, SECTOR 3, CALLE 4, N 16-CUA 	0	COORDINADOR DE SEDE INSTRUCTOR	CONTRATADO	2016-01-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
73	4279766	DELFIN TORRES, ARGENIS	Venezolano 	1953-05-13	\N	\N	Masculino 	Soltero 	\N	adelfin@ubv.edu.ve 	ANTIMANO, DISTRIBUIDOR PARATE BUENO 	1	INSTRUCTOR	DOCENTE	2007-01-09	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
74	4303977	TORREALBA MARTINEZ, ELECTO DE JESUS	Venezolano 	1956-07-31	\N	\N	Masculino 	Soltero 	\N	etorrealba@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2011-01-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
75	4351430	NASS CARRANZA, PEDRO ENRIQUE	Venezolano 	1956-03-03	\N	\N	Masculino 	Casado 	\N	pnass@ubv.edu.ve 	ROBLEDAL SECTOR GUIRIGUIRE CARRETERA VIA EL FARO, CASA EL PITIGUEY 	0	AGREGADO	DOCENTE	2006-04-17	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
76	4351904	CAMPOS BARRIOS, ZULAY ELENA	Venezolano 	1952-05-05	\N	\N	Femenino 	Soltero 	\N	zecampos@ubv.edu.ve 	AV. SANTIAGO MARIÑO RES. MARGARITA PLAZA PISO 11 APTO 11 	0	INSTRUCTOR	DOCENTE	2008-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
77	4351925	PIÑANGO PIÑANGO, MARÍA MAGDALENA	Venezolano 	1955-11-30	\N	\N	Femenino 	Soltero 	\N	mpinango@ubv.edu.ve 	LOS JARDINES DEL VALLE, ES. VALLE ALTO 	0	AGREGADO	DOCENTE	2005-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
78	4361219	MENDOZA MONTENEGRO, NERIDA JOSEFINA	Venezolano 	1954-07-27	\N	\N	Femenino 	Soltero 	\N	njmendoza@ubv.edu.ve 	CARACAS 	0	ASISTENTE	DOCENTE	2011-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
79	4419759	VIRGUEZ DE OLIVO, ROSA ARELIS	Venezolano 	1955-06-15	\N	\N	Femenino 	Casado 	\N	rvirguez@ubv.edu.ve 	UD 4, CONJUNTO RESIDENCIAL MUCURITAS BL.8 P.14 APTO. 1401 	0	AGREGADO	CONTRATADO	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
80	4434003	HERRERA CONRADO, RAMÓN ROBERTO	Venezolano 	1956-05-01	\N	\N	Masculino 	Soltero 	\N	rrherrera@ubv.edu.ve 	AV LA ESTRELLA  EDF ULISES PISO2 APTO 4 SA N BERNADINO, CARACAS 	0	COORDINADOR DE SEDE ASISTENTE	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
81	4479329	CIANCI FONSECA, SEBASTIAN	Venezolano 	1954-01-07	\N	\N	Masculino 	Soltero 	\N	scianci@ubv.edu.ve 	CALLE 12 ENTRE AVENIDA 6 Y 7 CASA S/N SECTOR EL MANGUITO 	0	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
82	4492554	BAUTISTA DE CLOTHIER, FABIOLA INMACULADA COROMOTO	Venezolano 	1958-03-15	\N	\N	Femenino 	Soltero 	\N	fbautista@ubv.edu.ve 	URB. SANTA MARIA NORTE, AV LOS JABILLOS. QTA EL SEBASTIAN-N 0-81 	0	ASOCIADO	DOCENTE	2005-02-21	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
83	4495655	ESCALA CARDOZO, GEOVANNY RAMON	Venezolano 	1954-06-18	\N	\N	Masculino 	Soltero 	\N	gescala@ubv.edu.ve 	CALLE BERMUDEZ, N 25 SECTOR NEGRO PRIMERO CIUDAD BOLIVAR. 	1	INSTRUCTOR	DOCENTE	2004-03-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
84	4498928	NOVOSEL RODRIGUEZ, ORLANDO JOSÉ	Venezolano 	1957-09-18	\N	\N	Masculino 	Soltero 	\N	onovosel@ubv.edu.ve 	URB. GALLO VERDE, APTO. # 6 BLOQUE H-12 MARACAIBO 	3	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
85	4521827	GONZALEZ, ELPIDIO JOSÉ	Venezolano 	1954-01-01	\N	\N	Masculino 	Soltero 	\N	 	URB SAN FELOPE SECTOR 5 VEREDA 37 # 17 	0	AGREGADO	DOCENTE	2004-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
86	4567657	RIVAS MATUTE, HÉCTOR JOSÉ	Venezolano 	1956-10-04	\N	\N	Masculino 	Casado 	\N	hrivas@ubv.edu.ve 	URB. BASE LIBERTADOR SECTOR B CASA B-033C. PALO NEGRO 	0	ASISTENTE	DOCENTE	2006-11-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
87	4570099	SERRANO JIMENEZ, RUBÉN LUPERCIO	Venezolano 	1956-10-30	\N	\N	Masculino 	Soltero 	\N	rlserrano@ubv.edu.ve 	C/R Los Mangos Torre ?D?. Apartamento 162. Av. Constitución 	0	INSTRUCTOR	DOCENTE	2006-09-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
88	4576118	REINOSO RATJES, RUBEN DARIO	Venezolano 	1955-07-13	\N	\N	Masculino 	Soltero 	\N	rreinoso@ubv.edu.ve 	CARACAS 	0	ASOCIADO	DOCENTE	2002-03-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
89	4578204	DIAZ BLANCO, ISAAC RAMON	Venezolano 	1956-09-26	\N	\N	Masculino 	Divorciado 	\N	idiaz@ubv.edu.ve 	AV MARIA AUXILIADORA, CONJUNTO RESIDENCIAL BAMBUSAL TORRE I, PISO 6, APARTAMENTO 1-A 	3	INSTRUCTOR	CONTRATADO	2015-09-17	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
90	4579242	LUNKS HAHNER, JUAN SIMON	Venezolano 	1955-05-05	\N	\N	Masculino 	Soltero 	\N	jlunks@ubv.edu.ve 	AV. MARACAY .QTA. NO.4. URB, LAS PALMAS. 	0	ASISTENTE	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
91	4581096	ESCALANTE IBARRA, JOSÉ FRANCISCO	Venezolano 	1955-11-10	\N	\N	Masculino 	Soltero 	\N	jescalante@ubv.edu.ve 	CALLE REAL COTIZA. CASA 14. SAN JOSE COTIZA 	0	ASISTENTE	DOCENTE	2005-03-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
92	4581465	NUÑEZ NIEBLA, JUAN FERNANDO DE NAZARET	Venezolano 	1955-10-09	\N	\N	Masculino 	Soltero 	\N	jfnunez@ubv.edu.ve 	URB.PALO VERDE CALLE 3, QUINTA RIVON #16-A PETARE. FILAS DE MARICHE 	0	AUXILIAR II	DOCENTE	2006-08-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
93	4587946	CEDEÑO MIJARES, LUIS RAMON	Venezolano 	1954-07-14	\N	\N	Masculino 	Soltero 	\N	lcedeno@ubv.edu.ve 	PETARE. CASA SANTA BARBARA. 	0	INSTRUCTOR	DOCENTE	2008-02-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
94	4601274	RON COFFIL, EDUARDO RAFAEL	Venezolano 	1956-09-07	\N	\N	Masculino 	Casado 	\N	eronc@ubv.edu.ve 	AV.CASANOVA. EDIF.CEDIAZ. TORRE OESTE, PISO 2 APTO 22 	0	INSTRUCTOR	CONTRATADO	2017-01-09	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
95	4613979	RODRÍGUEZ AMUNDARAY, ROSA ELENA	Venezolano 	1955-12-27	\N	\N	Femenino 	Soltero 	\N	rrodriguez@ubv.edu.ve 	CALLE 7-Q N 37 SECTOR LA MANGA 	0	INSTRUCTOR	DOCENTE	2006-06-02	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
96	4615953	IBARRA FEBRES, BELITZA	Venezolano 	1958-10-08	\N	\N	Femenino 	Divorciado 	\N	bibarra@ubv.edu.ve 	LA MURALLA CARRERA 3 CASA N1 	1	ASISTENTE	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
97	4623279	BELLO RIVAS, BIENVENIDA DEL VALLE	Venezolano 	1959-11-17	\N	\N	Femenino 	Soltero 	\N	bbello@ubv.edu.ve 	CALLE JUNCAL NO25 GÜIRIA EDO SUCRE 	0	ASISTENTE	DOCENTE	2006-11-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
98	4624609	MARFISIS DE JURADO, INÉS MATILDE	Venezolano 	1956-03-14	\N	\N	Femenino 	Casado 	\N	imarfisis@ubv.edu.ve 	BELLA VISTA MZ 34 CASA N 01  QUINTA VILLA DE FATIMA 	0	INSTRUCTOR	DOCENTE	2007-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
194	6099603	FRANQUIZ, ENRIQUE RAMON	Venezolano 	1960-01-12	\N	\N	Masculino 	Soltero 	\N	efranquiz@ubv.edu.ve 	UBV 	0	INSTRUCTOR	CONTRATADO	2017-10-23	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
131	5245781	SANCHEZ, YADIRA SOLEDAD	Venezolano 	1960-05-12	\N	\N	Femenino 	Soltero 	\N	ysanchez@ubv.edu.ve 	URBANIZACION LINDA BARINAS CALLE 3 CASA N109 	0	INSTRUCTOR	DOCENTE	2006-05-02	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
987	14036751	YEGUEZ, ROSIRIS COROMOTO	Venezolano 	1978-04-30	\N	\N	Femenino 	Soltero 	\N	ryeguez@ubv.edu.ve 	URB JUANA LA AVANZADORA J-27 CASA N 12 	2	AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
99	4666608	GONZALEZ DE SALAS, ANA MAGALY	Venezolano 	1956-04-09	\N	\N	Femenino 	Casado 	\N	amgonzalez@ubv.edu.ve 	AV. PRINCIPAL DE LA PARROQUIA CAMPO ALEGRE CASA 127 MUNICIPIO VSAN RAFAELDE CARVAJAL 	0	INSTRUCTOR	CONTRATADO	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
100	4702026	LEON PINEDA, RUBÉN DARIO	Venezolano 	1956-01-08	\N	\N	Masculino 	Soltero 	\N	rleon@ubv.edu.ve 	BLOQUES DE LA PARAGUA SECTOR 4 BLOQUE 3 EDIFICIO 4-2C 	1	ASISTENTE	DOCENTE	2007-09-17	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
101	4718082	BETANCOURT DE MUJICA , ISAIRA RAMONA 	Venezolano 	1958-08-31	\N	\N	Femenino 	Casado 	\N	irbetancourt@ubv.edu.ve 	URB LA FLORESTA CARRERA 1 CON CALLE 6 CASA LAS DELICIAS 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
102	4746131	JIMÉNEZ SALCEDO, CARMEN REGINA	Venezolano 	1953-10-23	\N	\N	Femenino 	Soltero 	\N	crjimenez@ubv.edu.ve 	AV. 14 CON CALLE  19 # 19-08,SIERRA MAESTRA MARACAIBO 	0	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
103	4750114	CARABALLO HURTADO, MARCIAL SEGUNDO	Venezolano 	1953-05-06	\N	\N	Masculino 	Soltero 	\N	mscaraballo@ubv.edu.ve 	PUEBLO NUEVO DE BONGO CARRETERA PRINCIPAL CASA N1 	0	INSTRUCTOR	DOCENTE	2007-10-22	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
104	4774913	ANCHUSTEGUI SAAVEDRA, BEGOÑA	Venezolano 	1958-09-03	\N	\N	Femenino 	Soltero 	\N	banchustegui@ubv.edu.ve 	URB LA FLORIDA AV LOS MANGOS RES NAIQUATA APTO 10/C PISO 10 CCS 	0	INSTRUCTOR	DOCENTE	2007-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
105	4780953	ALVAREZ, NILDA MILAGROS	Venezolano 	1959-08-19	\N	\N	Femenino 	Soltero 	\N	nialvarez@ubv.edu.ve 	URB. GUAICAIPURO I. 2DA CALLE. CASA 1217. 	1	INSTRUCTOR	DOCENTE	2007-10-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
106	4810852	PÉREZ PEREZ, WILLIAMS	Venezolano 	1955-10-09	\N	\N	Masculino 	Soltero 	\N	wperezp@ubv.edu.ve 	URB. SAN RAFAEL EN MUNICIPIO CARIRUBANA  MANZANA 1 	0	INSTRUCTOR	DOCENTE	2005-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
107	4820087	MENDEZ COA, ISNARDY JOSE	Venezolano 	1956-11-13	\N	\N	Masculino 	Soltero 	\N	imendez@ubv.edu.ve 	CARACAS 	1	INSTRUCTOR	DOCENTE	2010-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
108	4853036	MÁRQUEZ CONTRERAS, MANUEL ENRIQUE	Venezolano 	1956-05-20	\N	\N	Masculino 	Soltero 	\N	mmarquez@ubv.edu.ve 	URB. JUAN PABLO II MANZANA K-1#11 BARINAS EDO. BARINAS 	0	ASISTENTE	DOCENTE	2006-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
109	4924356	SALAS LAMOS, LEOVALDO ANTONIO	Venezolano 	1959-04-05	\N	\N	Masculino 	Soltero 	\N	lsalas@ubv.edu.ve 	URBANIZACION BARRIO EL CENTRO AV.SUCRE CASA 126 	0	COORDINADOR DE SEDE AGREGADO	DOCENTE	2006-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
110	4930215	ESCORCHA MARIN, YOLANDA COROMOTO	Venezolano 	1960-10-10	\N	\N	Femenino 	Soltero 	\N	yescorcha@ubv.edu.ve 	CARACAS 	0	ASISTENTE	DOCENTE	2010-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
111	4952045	MALAVE, DIOCELIS MARIA	Venezolano 	1958-10-30	\N	\N	Femenino 	Divorciado 	\N	dmalave@ubv.edu.ve 	GUARITOS I VEREDA 01 CASA 09 	0	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
112	4961711	HERNANDEZ GARCIA, CLEMENTE JOSÉ	Venezolano 	1959-11-14	\N	\N	Masculino 	Soltero 	\N	cjhernandezg@ubv.edu.ve 	GRAN SABANA CAA BLANCA CALLEJON PPAL. CASA # 4 	1	ASISTENTE	DOCENTE	2006-07-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
113	4984171	GONZALEZ DE LARA, YOHANNA JOSEFINA	Venezolano 	1956-02-14	\N	\N	Femenino 	Soltero 	\N	yjgonzalezd@ubv.edu.ve 	URB EL PERU SECTOR VEREDA # 09 CASA # 03 	0	ASISTENTE	DOCENTE	2007-04-23	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
114	4984191	MERCADO TOMASSINI, JOSÉ GREGORIO	Venezolano 	1957-03-23	\N	\N	Masculino 	Soltero 	\N	jgmercado@ubv.edu.ve 	CALLE MATURIN N 14 LA ALAMEDA PARROQUIA CATEDRAL CIUDAD BOLIVAR 	1	AGREGADO	DOCENTE	2005-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
115	4994327	PÉREZ NAVA, LUIS ADOLFO	Venezolano 	1957-07-26	\N	\N	Masculino 	Soltero 	\N	laperezn@ubv.edu.ve 	AV. 11 # 16-51 SIERRA MAESTRA ,MARACAIBO,VENEZUELA 	0	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
116	4994850	FERNÁNDEZ LOPEZ, BERNARDO ANTONIO	Venezolano 	1956-03-12	\N	\N	Masculino 	Soltero 	\N	bafernandez@ubv.edu.ve 	MARACAIBO ESTADO ZULIA 	0	INSTRUCTOR	DOCENTE	2007-01-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
117	5017596	BETANCOURT  MUJICA, JOEL EDUARDO	Venezolano 	1958-11-01	\N	\N	Masculino 	Casado 	\N	jebetancourt@ubv.edu.ve 	CONJ. RES. CHAGUARAMAL II CALEE LAS FLORES AGUA SALADA 	0	INSTRUCTOR	DOCENTE	2005-10-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
118	5028260	PARADA VALERO, FREDDY ALBERTO	Venezolano 	1958-05-31	\N	\N	Masculino 	Casado 	\N	fparada@ubv.edu.ve 	VIA CHORRO DEL INDIO, URBA, VILLA EL EDUCADOR, CALLE N 4 CASA N 118 	0	INSTRUCTOR	DOCENTE	2010-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
119	5038237	CHOURIO, RAIZA LISBET	Venezolano 	1955-12-14	\N	\N	Femenino 	Soltero 	\N	rchourio@ubv.edu.ve 	URB.EL TRBOL. CIRCONV. N 2. ARAGUA. APTO 2B. 	0	ASISTENTE	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
120	5047090	BARRETO GONZALEZ, DIGNORIS COROMOTO	Venezolano 	1957-10-13	\N	\N	Femenino 	Soltero 	\N	dbarreto@ubv.edu.ve 	AV. BELLA VISTA CON CALLE 81. EDIF. METROPOLITANO.APTO 5-13 	0	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
121	5068764	ACOSTA RAMIREZ, ALEXANDER	Venezolano 	1958-09-08	\N	\N	Masculino 	Soltero 	\N	aacosta@ubv.edu.ve 	URBANIZACION SABAÑERA CALLEJON EL CALBARIO MARACAIBO 	0	AGREGADO	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
122	5073396	MURIA DE MADRID, BEATRIZ ZULEIKA	Venezolano 	1959-04-11	\N	\N	Femenino 	Casado 	\N	bmuria@ubv.edu.ve 	CARACAS 	2	INSTRUCTOR	CONTRATADO	2016-01-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
123	5082255	TOLEDO NUNEZ, JESÚS RAFAEL	Venezolano 	1957-10-27	\N	\N	Masculino 	Soltero 	\N	jrtoledo@ubv.edu.ve 	CAÑADA HONDA CALLE LOS POSTES NEGROS EDF. VISOCA 	0	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
124	5082362	CORDOVA CAMPOS, JAIME JOSE	Venezolano 	1958-03-21	\N	\N	Masculino 	Soltero 	\N	jjcordova@ubv.edu.ve 	URBANIZACION LOS SAUCES, CALLE 2, CASA C-8, SECTOR TIPURO II 	1	INSTRUCTOR	CONTRATADO	2015-11-24	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
125	5091883	GUTIÉRREZ , MARÍA JOSEFA	Venezolano 	1959-06-10	\N	\N	Femenino 	Casado 	\N	mjgutierrez@ubv.edu.ve 	LAGUNA OM VIA EL CERRITO CALLE PRINCIPAL CASA UN REGALO DE DIOS 	0	ASISTENTE	DOCENTE	2007-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
126	5113029	CHIRINOS MORILLO, ISMAEL JESÚS	Venezolano 	1956-06-17	\N	\N	Masculino 	Soltero 	\N	ichirinos@ubv.edu.ve 	URBANIZACION 4 	0	ASISTENTE	DOCENTE	2006-10-23	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
127	5124438	SANCHEZ BERNAL, FRANKLIN JOSÉ	Venezolano 	1957-11-07	\N	\N	Masculino 	Soltero 	\N	fjsanchez@ubv.edu.ve 	ALTAGRACIA AV.OESTE CON 3 CALLE NORTE 10 PARAISO A DR. GONZALEZ RES. TAKE APTO 	0	INSTRUCTOR	DOCENTE	2008-02-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
128	5132477	LINARES, JOSE GREGORIO	Venezolano 	1958-03-23	\N	\N	Masculino 	Soltero 	\N	jolinares@ubv.edu.ve 	LOS CHAGUARAMOS 	2	INSTRUCTOR	DOCENTE	2011-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
129	5185582	SUCRE, YELICZA ALEJANDRINA	Venezolano 	1957-11-25	\N	\N	Femenino 	Soltero 	\N	ysucre@ubv.edu.ve 	CALLE COLOMBINA, CASA N 19 	0	INSTRUCTOR	DOCENTE	2007-09-23	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
130	5233246	RIVERO ESPAÑOL, GUMERSINDA MERCEDES	Venezolano 	1955-09-15	\N	\N	Femenino 	Soltero 	\N	grivero@ubv.edu.ve 	AV. PRINCIPAL C/SAN JOSE CASA N 28 SECTOR ZAJONTE 	0	ASISTENTE	DOCENTE	2007-05-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
132	5263578	OVIEDO VASQUEZ, MIRNA COROMOTO	Venezolano 	1958-09-03	\N	\N	Femenino 	Casado 	\N	moviedo@ubv.edu.ve 	CALLE BARINAS N 34. URB. AQUILES NAZOA 	0	INSTRUCTOR	DOCENTE	2007-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
133	5303980	DÁVILA, ANDRÉS	Venezolano 	1960-07-30	\N	\N	Masculino 	Soltero 	\N	adavila@ubv.edu.ve 	CALLE LAS DELICIAS, SECTOR LAS FLORES, CASA #2, PARROQUIA AGUA SALADA 	0	INSTRUCTOR	DOCENTE	2005-10-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
134	5307927	MUÑOZ PLAZA, EDUARDO JOSÉ	Venezolano 	1961-05-16	\N	\N	Masculino 	Soltero 	\N	emunoz@ubv.edu.ve 	RES.EL LAGO E-2 PISO 8. APTO 91.CALLE EL LAGO - EV. FRANCISCO DE MIRANDA. CALIFORNIA NORTE 	3	INSTRUCTOR	DOCENTE	2005-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
135	5318045	ALVAREZ MOTA, OMERO ENRIQUE	Venezolano 	1958-12-14	\N	\N	Masculino 	Soltero 	\N	oalvarez@ubv.edu.ve 	URB. VILLA DON CARLOS, CALLE MERCURIOO N 54 	0	AGREGADO	DOCENTE	2004-09-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
136	5330568	ANDRADE DE VARGAS, BERLA ESPERANZA	Venezolano 	1957-12-17	\N	\N	Femenino 	Casado 	\N	bandrade@ubv.edu.ve 	URBANIZACION DE SAN ANTONIO AVENIDA EL LIMON N 60 	0	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
137	5337986	RUIZ CARVAJAL, MAGYOLIS FLORENTINA	Venezolano 	1960-10-16	\N	\N	Femenino 	Divorciado 	\N	mfruiz@ubv.edu.ve 	CALLE PRINCIPALSAN RAFAEL S/N 	1	INSTRUCTOR	DOCENTE	2007-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
138	5360134	MIRABAL DE DIAZ, DILA MARIA	Venezolano 	1957-02-02	\N	\N	Femenino 	Soltero 	\N	dmirabal@ubv.edu.ve 	URB. EL NAZARENO VEREDA 16, CASA N 03 	0	INSTRUCTOR	DOCENTE	2007-10-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
139	5362115	BORJAS FLORES, NILYA YAMELI	Venezolano 	1959-02-01	\N	\N	Femenino 	Soltero 	\N	nborjas@ubv.edu.ve 	TAMARINDO SECTOR 1- CALLE 1 CASA 4 	1	INSTRUCTOR	DOCENTE	2006-05-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
140	5363936	ABREU FLORES, CARMEN AYDEES	Venezolano 	1957-05-06	\N	\N	Femenino 	Soltero 	\N	caabreu@ubv.edu.ve 	URB. 9 DE MARZO, CALLE 10, CASA # 14614 EDO. PORTUGUESA - AGUA BLANCA 	0	INSTRUCTOR	DOCENTE	2006-05-02	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
141	5387206	SIMANCA, REINA ARACELIS	Venezolano 	1959-08-25	\N	\N	Femenino 	Divorciado 	\N	 	URB LA ISABELICA SECTOR 10 VEREDA 20 CASA 04 	0	INSTRUCTOR	CONTRATADO	2019-05-31	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
142	5395841	BECERRA MOROCOIMA, ANACELYS JOSEFINA	Venezolano 	1958-07-28	\N	\N	Femenino 	Soltero 	\N	ajbecerra@ubv.edu.ve 	URB. LOS TAPIALES 1 2DA CALLE N 26B MATURIN 	0	ASISTENTE	DOCENTE	2006-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
143	5398683	PINTO, ALIS RAFAEL	Venezolano 	1961-11-22	\N	\N	Masculino 	Soltero 	\N	alpinto@ubv.edu.ve 	CALLE JECOME CASTELLON #03 SECTOR ANDRES ELOY BLANCO 	2	ASISTENTE	DOCENTE	2004-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
144	5409670	GONZALEZ OSUNA, ALEX ANTONIO	Venezolano 	1957-05-06	\N	\N	Masculino 	Soltero 	\N	aagonzalezo@ubv.edu.ve 	CALLE SAN ANTONIO N 4, SECTOR LA COROMOTO AV. SAN MARTIN. PARROQUIA EL PARAISO 	2	ASISTENTE	DOCENTE	2005-03-28	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
145	5426844	VIVAS RINCON, INÉS ELISA	Venezolano 	1956-07-22	\N	\N	Femenino 	Soltero 	\N	ievivas@ubv.edu.ve 	AV. PANTEON EDF. RODA APTO. 2-C CARACAS 	0	ASOCIADO	DOCENTE	2005-10-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
146	5432789	PUENTES BAUTISTA, JOSE RAFAEL	Venezolano 	1958-11-13	\N	\N	Masculino 	Soltero 	\N	jpuentes@ubv.edu.ve 	CARACAS 	1	INSTRUCTOR	DOCENTE	2010-10-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
147	5472267	ARRIECHI FUENTES, ALEXIS RAFAEL	Venezolano 	1959-02-26	\N	\N	Masculino 	Soltero 	\N	aarriechi@ubv.edu.ve 	CAMPOS POLICIA CALLE 02 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
148	5486933	ATAGUA DE CASTRO, CARMEN CELESTINA	Venezolano 	1959-11-15	\N	\N	Femenino 	Soltero 	\N	catagua@ubv.edu.ve 	URB. MACARENA SUR, CALLE LA CAPILLA N 40 LOS TEQUES 	0	ASISTENTE	DOCENTE	2006-07-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
149	5546474	LISBOA, NELLY BEATRIZ	Venezolano 	1960-01-17	\N	\N	Femenino 	Soltero 	\N	nlisboa@ubv.edu.ve 	CIUDAD CASARAPA CALLE PRINCIPAL PARCELA 21 EDF. 12  APTO. 2F 	0	INSTRUCTOR	DOCENTE	2008-02-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
150	5548910	MOROCOIMA BARRETO, JUAN ROCI	Venezolano 	1960-05-23	\N	\N	Masculino 	Casado 	\N	jmorocoima@ubv.edu.ve 	URB. PALMITA, CALLE PRINCIPAL, AL LADO DE LA ESCUELA, 	0	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
151	5553888	CONTRERAS SANCHEZ, ZUNILDE DEL CARMEN	Venezolano 	1960-02-03	\N	\N	Femenino 	Soltero 	\N	zcontreras@ubv.edu.ve 	URBANIZACION VISTA HERMOSA II CALLE MANZ 25 CASA N°16 	0	ASISTENTE	DOCENTE	2005-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
152	5556227	LOZADA RODRIGUEZ, JOSÉ GREGORIO	Venezolano 	1957-11-10	\N	\N	Masculino 	Soltero 	\N	jglozadar@ubv.edu.ve 	CALLE ICABARU, CASA #S LA SABANITA ESTADO BOLIVAR 	1	AUXILIAR I	DOCENTE	2004-09-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
153	5566346	LUPERA CARRERO, ROSAURA	Venezolano 	1959-08-09	\N	\N	Femenino 	Soltero 	\N	rlupera@ubv.edu.ve 	CARACAS 	0	ASISTENTE	DOCENTE	2010-11-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
154	5581012	ESPINOZA, EDUARDO	Venezolano 	1961-03-16	\N	\N	Masculino 	Soltero 	\N	eespinoza@ubv.edu.ve 	BLOQUE18P,LETRA E APTO.148.LA CAÑADA.23 DE ENERO 	0	AGREGADO	DOCENTE	2004-10-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
155	5584512	VELÁSQUEZ GUTIERREZ, BLANCA MARGARITA	Venezolano 	1959-11-22	\N	\N	Femenino 	Casado 	\N	bmvelasquez@ubv.edu.ve 	CALLE LAS DELICIAS CASA #12 SECTOR CAJA DE AGUA PARROQUIA NORTE 	1	ASISTENTE	DOCENTE	2007-06-04	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
156	5655933	USECHE DE DUQUE, AYREN ENDRINA	Venezolano 	1960-06-19	\N	\N	Femenino 	Casado 	\N	auseche@ubv.edu.ve 	SAN CRISTOBAL, BARRIO PUENTE REAL, SECTOR 16 N Y-19 	0	COORDINADOR DE SEDE INSTRUCTOR	DOCENTE	2007-04-23	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
157	5675079	ROMERO MORA, ANA MATILDE	Venezolano 	1963-03-11	\N	\N	Femenino 	Soltero 	\N	amromero@ubv.edu.ve 	CARACAS 	0	ASISTENTE	DOCENTE	2006-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
158	5687119	MANTILLA , YORMAN GUILLERMO	Venezolano 	1962-01-27	\N	\N	Masculino 	Soltero 	\N	YORMANUNELLEZ2014@HOTMAIL.COM 	CALLE EL SAMAN AL FINAL N 3-18,PARTE ALTA SECTOR LAS MARIAS 	0	INSTRUCTOR	CONTRATADO	2018-01-22	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
159	5703245	FIORE PATIÑO, BELEN ARDIFONSO	Venezolano 	1961-01-10	\N	\N	Femenino 	Soltero 	\N	bafiore@ubv.edu.ve 	PALO ALTO CALLE F F 1-A 	1	INSTRUCTOR	DOCENTE	2008-02-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
160	5727830	GUANIPA MEDINA, XIOMARA ELEYDA	Venezolano 	1960-12-03	\N	\N	Femenino 	Soltero 	\N	xguanipa@ubv.edu.ve 	URB NUEVA DEMOCRACIA CALLE 46 	0	COORDINADOR DE SEDE AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
161	5739203	MENDOZA BAUTISTA, DORIS ANTONIA	Venezolano 	1956-11-02	\N	\N	Femenino 	Soltero 	\N	dmendoza@ubv.edu.ve 	AV. 6 CASA S/N URB CUMBRES ANDINAS 	0	ASISTENTE	DOCENTE	2006-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
162	5749891	GONZALEZ ZAPATA, LUIS ALBERTO	Venezolano 	1960-02-28	\N	\N	Masculino 	Casado 	\N	lagonzalezz@ubv.edu.ve 	URB. RIO CAURA I - SECTOR 01, MANZANA 06 - N 01 CALLE SIPAO C/C AIMARA 	1	INSTRUCTOR	DOCENTE	2006-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
163	5751425	CHASSAIGNE RICCIULLI, GERDI ELIZABETH	Venezolano 	1960-10-22	\N	\N	Femenino 	Soltero 	\N	gchassaigne@ubv.edu.ve 	URB. LA ESMERALDA MZNA A4  CASA NUM 30, 	1	AGREGADO	DOCENTE	2009-06-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
164	5752145	SANCHEZ DE MANZANARES, NOHELIA COROMOTO	Venezolano 	1958-10-11	\N	\N	Femenino 	Casado 	\N	nsanchez@ubv.edu.ve 	CALLE MONAGAS, PLANTA ALTA E COMEN LO BOTIJA 	0	AGREGADO	DOCENTE	2005-11-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
165	5761152	PAREDES DE FEREIRA, JANETH RAMONA	Venezolano 	1962-07-08	\N	\N	Femenino 	Soltero 	\N	jparedes@ubv.edu.ve 	URB LA LLANADA CARVAJAL AV PRINCIPA 	1	INSTRUCTOR	DOCENTE	2006-07-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
266	7246551	COVA, KAREN ELENA	Venezolano 	1966-11-04	\N	\N	Femenino 	Soltero 	\N	 	FINAL CALLE LOZADA N 19 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
166	5786785	VILORIA CARRILLO, ERNESTO JOSÉ	Venezolano 	1964-06-23	\N	\N	Masculino 	Soltero 	\N	ejviloria@ubv.edu.ve 	URB LA TRINIDAD CALLE 53B N 15N -66 	1	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
167	5786831	ZABALA ALBURJAS, JOSÉ GREGORIO	Venezolano 	1964-10-01	\N	\N	Masculino 	Soltero 	\N	jgzabala@ubv.edu.ve 	URB EL TREBOL EDF PINO PISO 3 APTO 11-D MCB 	2	AGREGADO	DOCENTE	2004-10-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
168	5795062	REA CASTILLO, TERESA RAMONA	Venezolano 	1960-05-26	\N	\N	Femenino 	Soltero 	\N	trrea@ubv.edu.ve 	CARACAS 	0	ASISTENTE	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
169	5805982	BRAVO GARCIA, JOSÉ GREGORIO	Venezolano 	1958-07-15	\N	\N	Masculino 	Soltero 	\N	josebravo@ubv.edu.ve 	URB. EL PARAISO DEL IVEA, CALLE 2, # 19, PUERTO LA CRUZ-ANZOATEGUI 	0	ASISTENTE	DOCENTE	2007-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
170	5824215	ACOSTA, YAMIRA	Venezolano 	1961-12-09	\N	\N	Femenino 	Soltero 	\N	yacosta@ubv.edu.ve 	BARRIO COUNTRY SUR CALLE 951 MARACAYBO 	0	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
171	5834827	RAMIREZ LAURENZ, REINALDO ALFONZO	Venezolano 	1957-12-14	\N	\N	Masculino 	Soltero 	\N	raramirez@ubv.edu.ve 	SECTOR RANCHERIA I AV 19 HATICOS POR ARRIBA CON AV 18F#17A-68 	1	INSTRUCTOR	DOCENTE	2008-01-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
172	5850741	BAPTISTA FUENMAYOR, MARÍA GREGORIA RAMONA	Venezolano 	1959-11-28	\N	\N	Femenino 	Soltero 	\N	mgbaptista@ubv.edu.ve 	CALLE KN7-76,BARRIO18 DE OCTUBRE PARROQUIA COQUIVACOA 	0	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
173	5851171	SANCHEZ PEREZ, JOSÉ GREGORIO	Venezolano 	1958-12-18	\N	\N	Masculino 	Soltero 	\N	jgsanchez@ubv.edu.ve 	AVENIDA3-C,N87-103,PARROQUIA SANTA LUCIA MUNICIPIO MARACAIBO 	0	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
174	5856992	MARCANO FIERRO, YBELISE MARGARITA	Venezolano 	1961-02-05	\N	\N	Femenino 	Soltero 	\N	ymarcano@ubv.edu.ve 	URB. BELLA VISTA, CARRERA 9, MANZANA 8 CASA # 10 MATURIN ESTADO MONAGAS 	2	AGREGADO	DOCENTE	2007-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
175	5884900	FIGUERAS LEMUS, RUTH NOEMI	Venezolano 	1965-10-19	\N	\N	Femenino 	Soltero 	\N	rfigueras@ubv.edu.ve 	URB.SAN ANDRESSAN ANDRES A. INTERCOMUNAL DEL VALLE RES. SAN ANDRES 	0	INSTRUCTOR	DOCENTE	2008-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
176	5886492	RAMOS PACHECO, WILBERTO	Venezolano 	1957-08-13	\N	\N	Masculino 	Casado 	\N	wramos@ubv.edu.ve 	RESIDENCIAS VILLA VICTORIA MANZANA N 2CASA N7 AV.ATLANTICO PUERTO ORDAZ ESTADO BOLIVAR 	0	INSTRUCTOR	DOCENTE	2005-04-28	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
177	5887680	CABEZAS SAEZ, JOSE LUIS	Venezolano 	1959-11-09	\N	\N	Masculino 	Soltero 	\N	jcabeza@ubv.edu.ve 	AV.LAS CIENCIAS EDIF.BALPECA PISO 2 APTO 23, LOS CHAGUARAMOS 	0	INSTRUCTOR	CONTRATADO	2017-10-23	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
178	5891010	FIGUEREDO BURGOS, MARÍA DE LOURDES	Venezolano 	1961-02-28	\N	\N	Femenino 	Soltero 	\N	mlfigueredo@ubv.edu.ve 	PRADO DE MARIA. CALLE LA CRUZ. CASA 18. SANTA EDUVIGIS 	0	AGREGADO	DOCENTE	2007-10-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
179	5914381	WEKY NESSI, FRANCISCO JAVIER	Venezolano 	1968-04-02	\N	\N	Masculino 	Soltero 	\N	fweky@ubv.edu.ve 	CALLE ACOSTA S/N. SAN ANTONIO A 100 METROS DE LA PLAZA BOLIVARMATURIN EDO MONAGAS 	0	INSTRUCTOR	DOCENTE	2007-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
180	5949417	RAMOS, ALFREDO ANTONIO	Venezolano 	1960-01-07	\N	\N	Masculino 	Soltero 	\N	alramos@ubv.edu.ve 	AV. INTERCOMUNAL EL VALLE SECTOR ZAMORA CASA # 183-1 	0	INSTRUCTOR	DOCENTE	2005-01-19	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
181	5971585	TORRES GONZALEZ, NIEVES	Venezolano 	1962-05-19	\N	\N	Femenino 	Soltero 	\N	torres.nieves@gmail.com 	CALLE LIBERTAD, SECTOR EL COPEY CASA 2-A NUEVA ESPARTA 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
182	5977680	TORRES SORANDO, LOURDES JOSEFINA	Venezolano 	1961-11-04	\N	\N	Femenino 	Casado 	\N	ljtorres@ubv.edu.ve 	ROBLEDAL, PENINSULA DE MACANAO NUEVA ESPARTAL. 	0	COORDINADOR DE SEDE AGREGADO	DOCENTE	2006-05-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
183	5992120	LEDEZMA, MATILDE DE JESÚS	Venezolano 	1964-01-09	\N	\N	Femenino 	Casado 	\N	mdledezma@ubv.edu.ve 	AV. LAS PALMERAS #23 MATHURIN ESTADO MONAGAS 	0	ASISTENTE	DOCENTE	2006-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
184	6005958	CUBILLAN TROCONIS, ALFREDO ALBERTO	Venezolano 	1959-10-29	\N	\N	Masculino 	Soltero 	\N	acubillan@ubv.edu.ve 	COLINAS LOS RUICES AV, SALTRON, RES. LOS RUISEÑORES, APTO B-5 	0	INSTRUCTOR	DOCENTE	2007-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
185	6020877	VARGAS FLORES, LEZY MAGYOLY	Venezolano 	1962-09-05	\N	\N	Femenino 	Soltero 	\N	lmvargas@ubv.edu.ve 	URB PALO VERDE EDF GRAN SASSO PISO 16 APT 61 	1	ASOCIADO	DOCENTE	2005-03-04	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
186	6024435	BELTRAN PRIETO, NIDIA MATILDE	Venezolano 	1960-09-13	\N	\N	Femenino 	Soltero 	\N	nmbeltran@ubv.edu.ve 	URB LOS PROCERES MANZANA 07, CALLE 07 CASA 43 	1	ASISTENTE	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
187	6036074	LADERA LADERA, FRANZ ENRIQUE	Venezolano 	1959-11-11	\N	\N	Masculino 	Soltero 	\N	fladera@ubv.edu.ve 	CHARALLAVE CALLE PERIMETRAL MANZANA 9 COLINAS BETANIA QUINTA  FOWN HOUSE 59 	1	AUXILIAR III	DOCENTE	2008-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
188	6037238	MIGUAL ANGEL, ARIZA	Venezolano 	1961-09-27	\N	\N	Masculino 	Soltero 	\N	MIGANGARI@HOTMAIL.COM 	COMUNIDAD MONTAÑA ALTA EDIF 1 PISO 6 APTO 16-3 	0	INSTRUCTOR	CONTRATADO	2017-06-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
189	6048728	CAMEJO GARCIA, DARIO JOSE	Venezolano 	1961-10-02	\N	\N	Masculino 	Soltero 	\N	dcamejo@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2010-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
190	6085264	GONZALEZ BERNAL, JHONNY ALEXANDER	Venezolano 	1962-03-17	\N	\N	Masculino 	Soltero 	\N	jhagonzalezb@ubv.edu.ve 	URB.ARAGUITA 2, SECT. 3, VEREDA 10, N 2 	1	AUXILIAR I	CONTRATADO	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
191	6088131	RIVAS FIGUERA, MARIA DEL CARMEN	Venezolano 	1961-12-07	\N	\N	Femenino 	Soltero 	\N	mcrivasf@ubv.edu.ve 	URBANIZACION BOYACA VEREDA 29 CASA NRO 09 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
192	6089708	HERRERA RIVAS, ROGER JOSE	Venezolano 	1962-06-15	\N	\N	Masculino 	Soltero 	\N	rjherrera@ubv.edu.ve 	CARACAS 	2	INSTRUCTOR	DOCENTE	2008-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
193	6096852	PÉREZ, ALEJANDRO ALBERTO	Venezolano 	1962-02-09	\N	\N	Masculino 	Soltero 	\N	alperez@ubv.edu.ve 	CALLE 3 MONTALBAN II RES. COSTAL DE MARFIL, PISO 5, APTO. 18 	1	COORDINADOR ACADEMICO ASISTENTE	DOCENTE	2007-05-02	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
195	6110675	LEZAMA CENTENO, PEDRO ANTONIO	Venezolano 	1963-10-18	\N	\N	Masculino 	Soltero 	\N	pedrolezama21@gmail.com 	URB FORTIN DE SAN PEDRO SECTOR CASTILLEJO 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
196	6114993	GONZALEZ TORRES, CARLOS EMILIO	Venezolano 	1963-11-09	\N	\N	Femenino 	Soltero 	\N	cgreforestacion16@gmail.com 	SECTOR SAN PEDRO CALLE CASAGRANDE, CASA 49 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
197	6119084	CEDILLO RAMOS, MERCEDES	Venezolano 	1965-01-21	\N	\N	Femenino 	Soltero 	\N	mcedillo@ubv.edu.ve 	23 DE ENERO LA CAÑADA BLOQUE 20 Y 21 	0	COORDINADOR NACIONAL INSTRUCTOR	DOCENTE	2008-02-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
198	6120699	ALVES, ANA FABIOLA	Venezolano 	1964-04-01	\N	\N	Femenino 	Soltero 	\N	aalves@ubv.edu.ve 	RESID.CUVERO PISO  11 APTO 11 A URB.LA  ROSALERA  SUR 	1	ASISTENTE	DOCENTE	2004-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
200	6134754	MARTÍNEZ SILVA, BELKIS RAFAELA	Venezolano 	1964-03-09	\N	\N	Femenino 	Soltero 	\N	brmartinez@ubv.edu.ve 	AV. SUCRE DE CATIA CALLE REAL DEL MANICOMIO, MOLINO A PEDRERA NRO 12-12 	0	INSTRUCTOR	DOCENTE	2004-03-22	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
201	6141966	LHAYA MECIA, VILMA BEATRIZ	Venezolano 	1963-01-24	\N	\N	Femenino 	Soltero 	\N	vblhaya@ubv.edu.ve 	HACIENDA EL FRUTAL, CARRETERA NACIONAL RIO CHICO EL GUAPO 	0	AGREGADO	DOCENTE	2006-03-27	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
202	6156035	ARAYA DE ROO, ORIETTA	Venezolano 	1948-08-12	\N	\N	Femenino 	Soltero 	\N	oaraya@ubv.edu.ve 	RESIDENCIA PARQUE 7 	0	INSTRUCTOR	DOCENTE	2005-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
203	6188331	MÁRQUEZ UGUETO, FLOR AURISTELA	Venezolano 	1967-09-13	\N	\N	Femenino 	Soltero 	\N	famarquez@ubv.edu.ve 	AV. LOS MORALITO CASA LAS NEGRAS, SECTOR EL AMARILLO, SAN ANTONIO DE LOS ALTOS 	0	ASISTENTE	DOCENTE	2006-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
204	6191060	MONTENEGRO DE ALFONZO, ANA YADIRA	Venezolano 	1966-10-10	\N	\N	Femenino 	Soltero 	\N	amontenegro@ubv.edu.ve 	URB FRANCISCO DE MIRANDA CASALTA I BLOQUE 5 PISO 3 APTO 07 PARROQUIA SUCRE 	2	ASISTENTE	DOCENTE	2004-04-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
205	6204901	GUZMAN GOMEZ, PEDRO RAFAEL	Venezolano 	1965-05-21	\N	\N	Masculino 	Soltero 	\N	prguzman@ubv.edu.ve 	AV.ALMA MATER EDIF. GIULIETA PISO 5 APTO 52 	0	INSTRUCTOR	CONTRATADO	2013-03-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
206	6223230	PADRINO RUDAS, MARTIN JOSÉ	Venezolano 	1965-08-21	\N	\N	Masculino 	Soltero 	\N	mpadrino@ubv.edu.ve 	AV. BARALT. ESQ TRUCO A GUANABANO. TORRE CASTEL GRANDE. PISO 8-E. EL SILENCIO 	2	ASISTENTE	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
207	6226614	GOMEZ MENDEZ, ODILIA MARIA	Venezolano 	1965-03-14	\N	\N	Femenino 	Soltero 	\N	ogomez@ubv.edu.ve 	URB. LAS ACACIAS AV. VICTORIA CALLE GUAYANA EDF. LAS PALMAS 3ER PISO APTO 31 	0	COORDINADOR PFG ASISTENTE	DOCENTE	2008-05-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
208	6230053	HERNÁNDEZ ACOSTA, ALEXIS ALFREDO	Venezolano 	1966-08-31	\N	\N	Masculino 	Soltero 	\N	aahernandez@ubv.edu.ve 	SACTOR UD.5 BLOQUE 9 EDIF 01 CARUCUAO CARACAS 	2	ASISTENTE	DOCENTE	2004-10-19	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
209	6230965	SOTILLO, JUAN CARLOS	Venezolano 	1966-06-23	\N	\N	Masculino 	Soltero 	\N	jusotillo@ubv.edu.ve 	CUMANACOA EDO.SUCRE 	0	AGREGADO	DOCENTE	2004-10-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
210	6245900	MÁRQUEZ RIVERO, RAIZA COROMOTO	Venezolano 	1967-08-16	\N	\N	Femenino 	Soltero 	\N	rcmarquez@ubv.edu.ve 	SECTOR MAGDALENA CALLE N 15 AV. 1, CASA N 9 	1	ASISTENTE	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
211	6268519	MORA DE BELLAZZINI, MYRIAN	Venezolano 	1954-12-31	\N	\N	Femenino 	Casado 	\N	mmorab@ubv.edu.ve 	CALLE 5 SECTOR RIO ZUÑIGA CASA Z-05 PIRINEOS I 	0	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
212	6293620	SUBERO, MAIDOLYS MILAGROS	Venezolano 	1966-12-17	\N	\N	Femenino 	Soltero 	\N	mmsubero@ubv.edu.ve 	SECTOR ANDRES ELOY BLANCO CALLE SAN JOSE CASA #470 	0	ASISTENTE	DOCENTE	2008-05-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
213	6300359	ARTIGAS ANDRADE, LUIS GERARDO	Venezolano 	1966-01-26	\N	\N	Masculino 	Soltero 	\N	lgartigas@ubv.edu.ve 	KM.38 CARRETERA COLONIA TOVAR,URB QUEBRADA DEL AGUA,CHALED AGUA LAS FLORES 	1	ASISTENTE	DOCENTE	2005-02-28	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
214	6310254	GARCIA LUNA, ANDRES RAFAEL	Venezolano 	1969-03-20	\N	\N	Masculino 	Soltero 	\N	argarcia@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2010-10-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
215	6313065	VERA MÁRQUEZ, LUIS RAMÓN	Venezolano 	1969-04-18	\N	\N	Masculino 	Soltero 	\N	vmluis18@gmail.com 	EDIF 14 PISO 17 APTO 02 SECTOR UD3 LA HACIENDA 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
216	6321194	MALDONADO PEÑA, VANESSA EVELIN	Venezolano 	1970-03-11	\N	\N	Femenino 	Soltero 	\N	vmaldonado@ubv.edu.ve 	URB. TERRAZAS DEL AVILA, CALLE 1, RESIDENCIAS DELTA, PISO 8, APTO 8B, CARACAS 	0	AGREGADO	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
217	6321923	BELLO TILLERO, ELIMAR	Venezolano 	1970-09-12	\N	\N	Femenino 	Soltero 	\N	ebello@ubv.edu.ve 	URB  LA FLORIDA CALLE LA NEGRIN CON PORVENIR EDF FABRICIO PISO 6 	2	AGREGADO	DOCENTE	2004-04-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
218	6331511	FARES ORTIZ, MILANYELA RAMONA 	Venezolano 	1969-02-09	\N	\N	Femenino 	Soltero 	\N	mfares@ubv.edu.ve 	URB LAS TRINITARIAS, CALLE 7 	1	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
219	6335598	GONZALEZ DE CARRASQUEL, DEYBI JOSEFINA	Venezolano 	1971-06-13	\N	\N	Femenino 	Soltero 	\N	deybigonzalez28@gmail.com 	SECTOR ARAGUITA AV PRINCIPAL CASA 63 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
220	6350331	MANAURE SANCHEZ, RAMÓN ESTEBAN	Venezolano 	1960-02-27	\N	\N	Masculino 	Soltero 	\N	remanaure@ubv.edu.ve 	AV. PRINCIPAL LA HACIENDA BLOQUE 15, ESCALERA 2 PISO 1 APTO 0103 CARICUAO  UD 5 	0	COORDINADOR DE SEDE AGREGADO	DOCENTE	2005-03-14	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
221	6386916	OSORIO ZAMBRANO, JORGE LUIS	Venezolano 	1962-09-12	\N	\N	Masculino 	Soltero 	\N	jlosorio@ubv.edu.ve 	MONAGAS 	1	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
222	6414379	RODRIGUEZ  ARTEAGA, LINDDYS DEL VALLE	Venezolano 	1965-01-14	\N	\N	Femenino 	Casado 	\N	lvrodrigueza@ubv.edu.ve 	PARCELA N 7, COLONIA MENDOZA, OCUMARE DEL TUY 	2	INSTRUCTOR	DOCENTE	2010-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
223	6447288	CAÑIZALEZ PARRA, ELISA JOSEFINA	Venezolano 	1966-04-21	\N	\N	Femenino 	Soltero 	\N	ecanizalez@ubv.edu.ve 	SECTOR Y RES. LOS BUDARES. LOMAS DE URQUIA. APTO 103- TORRE C. SAN ANTONIO DE LOS ALTOS 	1	ASISTENTE	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
224	6453481	MOTA  DE LAMEDA, BELQUYS JACQUELINE	Venezolano 	1963-10-18	\N	\N	Femenino 	Soltero 	\N	bjmota@ubv.edu.ve 	URB. EL RODEO   CALLE SAN BOSCO EDIF. EL RODEO TORRE C N 76 	0	INSTRUCTOR	DOCENTE	2008-02-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
225	6455717	BLANCO ARROYO, HÉCTOR RAFAEL	Venezolano 	1961-08-26	\N	\N	Masculino 	Soltero 	\N	hblanco@ubv.edu.ve 	LA MATICA VUELTA LARGA N 2-A 	1	ASISTENTE	DOCENTE	2008-02-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
226	6462514	VELASCO HERNANDEZ, AGUSTIN EVARISTO	Venezolano 	1962-12-24	\N	\N	Masculino 	Soltero 	\N	aevelasco@ubv.edu.ve 	URB COLINAS DE CARRIZAL AV AVP QUINTA COROMOTO T20 	0	INSTRUCTOR	CONTRATADO	2018-11-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
227	6464300	ESCOBAR ACOSTA, DARIO RAFAEL	Venezolano 	1963-12-19	\N	\N	Masculino 	Soltero 	\N	 	CALLE PRINCIPAL CASA NRO S/N SECTOR SANTA EULALIA LOS TEQUES 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
228	6465595	ARTEAGA PEREIRA, ALFREDO ANTONIO	Venezolano 	1960-05-07	\N	\N	Masculino 	Soltero 	\N	aaarteaga@ubv.edu.ve 	RES. PARQUE PARACOTOS. TORRE 7B. APTO PBA. PARACOTOS ESTADO MIRANDA 	1	AGREGADO	DOCENTE	2005-10-31	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
229	6468154	GUANCHEZ DE GUANCHEZ, FRANCISCA LEIDA	Venezolano 	1961-10-04	\N	\N	Femenino 	Soltero 	\N	fguanchez@ubv.edu.ve 	RESIDENCIAS VISTA MAR I. AV. LA COSTERA SECTOR CAMURI CHICO. URB. LA LLANADA CARABALLEDA ESTADO VARG 	0	ASISTENTE	DOCENTE	2006-04-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
230	6486306	NIÑO HERNANDEZ, EDGAR MARTIN	Venezolano 	1965-02-13	\N	\N	Masculino 	Soltero 	\N	enino@ubv.edu.ve 	VEREDA N7 CASA N18-01 CATIA LA MAR 	0	AUXILIAR I	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
231	6489645	BARRIOS FRANCO, DOUGLAS RAUL	Venezolano 	1966-06-26	\N	\N	Masculino 	Soltero 	\N	dbarrios@ubv.edu.ve 	AAV NEVERI, RESIDENCIA ORIENTE, EDF SUCRE PISO 1 APT A-3 UEBANIZACION LOS CHAGUARAMOS 	0	INSTRUCTOR	DOCENTE	2007-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
232	6520913	GONZALEZ RUIZ, LUIS MANUEL	Venezolano 	1963-03-08	\N	\N	Masculino 	Soltero 	\N	lmgonzalez@ubv.edu.ve 	AV. PROGRESO, CASA # 240 CASCO CENTRAL DE PUNTO FIJO 	2	ASOCIADO	DOCENTE	2004-09-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
233	6546928	VELÁSQUEZ CHARMELO, MILAGROS JOSEFINA	Venezolano 	1963-11-08	\N	\N	Femenino 	Soltero 	\N	mvelasquez@ubv.edu.ve 	EL VALLE AVENIDA INTERCOMUNAL CALLE 2 	0	ASISTENTE	DOCENTE	2004-04-28	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
234	6547833	VIZCAINO MORENO, DUILIO JOSÉ	Venezolano 	1963-06-04	\N	\N	Masculino 	Soltero 	\N	dvizcaino@ubv.edu.ve 	FINAL AV. SAN MARTIN CALLE SAN MARTIN LA FLORESTA EDF. MARGARITA 	0	INSTRUCTOR	DOCENTE	2008-02-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
235	6609080	LEON MARTINEZ, BRIZEIDA MARGARITA	Venezolano 	1961-12-11	\N	\N	Femenino 	Soltero 	\N	bmleon@ubv.edu.ve 	RIO CHICO 	1	ASISTENTE	DOCENTE	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
236	6719716	CELIS SERVIDEA, OCTAVIO JOSE	Venezolano 	1970-02-03	\N	\N	Masculino 	Soltero 	\N	ocelis@ubv.edu.ve 	AV. ROMULO GALLEGOS EDF.PEDERNALES.APTO.3-1,EL MARQUEZ 	0	ASISTENTE	DOCENTE	2004-06-28	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
237	6746537	ÁVILA FUENMAYOR, RICHARD MIGUEL	Venezolano 	1972-05-19	\N	\N	Masculino 	Soltero 	\N	rmavila@ubv.edu.ve 	URB.GODOFREDO CALLE 1 CON AV,.2 #15F 	2	INSTRUCTOR	DOCENTE	2005-05-02	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
238	6746563	ROMERO, ROSARIO	Venezolano 	1972-03-30	\N	\N	Femenino 	Soltero 	\N	rdcromero@ubv.edu.ve 	CARACAS 	2	ASISTENTE	DOCENTE	2004-09-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
239	6746742	BOLAÑO SALAS, HUBERTO SEGUNDO	Venezolano 	1971-04-21	\N	\N	Masculino 	Soltero 	\N	hbolano@ubv.edu.ve 	SECTOR ALTOS DE JALISCO, 	3	ASISTENTE	DOCENTE	2004-05-29	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
240	6747425	PIRELA REBONATTO, VERONICA ISABEL	Venezolano 	1971-02-23	\N	\N	Femenino 	Soltero 	\N	vpirela@ubv.edu.ve 	URB.AMALIA CALLE 65 	0	ASOCIADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
241	6867023	MOLINA PADRON, WALTER DANIEL	Venezolano 	1967-09-30	\N	\N	Masculino 	Soltero 	\N	wmolina@ubv.edu.ve 	BLOQUE 18 DE CARIGUAO 	0	ASISTENTE	DOCENTE	2004-04-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
242	6875222	ACOSTA NUCETTE, DIANA CAROLINA	Venezolano 	1966-12-25	\N	\N	Femenino 	Soltero 	\N	dacosta@ubv.edu.ve 	AV.3F CALLE,83-84,EDF.VILLAS FELICES,APTO.10B SECTOR VALLE FRIO. 	0	ASOCIADO	DOCENTE	2004-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
243	6877194	SALAS PEÑA, YASMIN DEL CARMEN	Venezolano 	1966-08-10	\N	\N	Femenino 	Soltero 	\N	ysalas@ubv.edu.ve 	CALLE CALDERON, S/N, EL VIGIA.LOS TEQUES ESTADO MIRANDA 	1	INSTRUCTOR	DOCENTE	2012-01-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
244	6879494	BUSTAMANTE AVENDANO, FLOR ANGEL	Venezolano 	1964-02-20	\N	\N	Femenino 	Soltero 	\N	fbustamante@ubv.edu.ve 	LOS ROSALES. 	0	ASISTENTE	DOCENTE	2004-04-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
245	6899125	DI GENARO, JOSÉ DOMENICO	Venezolano 	1966-11-09	\N	\N	Masculino 	Soltero 	\N	jdigenaro@ubv.edu.ve 	AV.PANTEON,EDF.VALDIANA,PISO I PTO.14 	0	ASISTENTE	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
246	6904012	ORSETTI RIVAS, LUIS MANUEL	Venezolano 	1963-10-31	\N	\N	Masculino 	Casado 	\N	lorsetti@ubv.edu.ve 	AV ESPAÑA C/C BELEN No, 272, CIUDAD BOLIVAR 	1	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
247	6906125	CONTRERAS COIZA, NANCY IVONNE	Venezolano 	1963-06-28	\N	\N	Femenino 	Soltero 	\N	nicontreras@ubv.edu.ve 	CARACAS 	1	INSTRUCTOR	DOCENTE	2011-01-31	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
248	6920150	MARQUEZ MARIN, JOSE ALEJANDRO	Venezolano 	1965-08-21	\N	\N	Masculino 	Soltero 	\N	jamm211@gmail.com 	CALLE 6 EDIF RESD LA TRINIDAD PISO 1. URB PLAYA GRANDE 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
249	6931915	CABALLERO GOLDING, GLORIA ESTRELLA	Venezolano 	1967-05-05	\N	\N	Femenino 	Soltero 	\N	gecaballero@ubv.edu.ve 	TIENDA HONDA A MERCEDES RE ALCE PISO 6 APT 61 PARROQUIA ALTAGRACIA 	0	AGREGADO	DOCENTE	2004-04-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
250	6954159	SANCHEZ CEDEÑO, OLLANTAY RAFAEL	Venezolano 	1967-09-06	\N	\N	Masculino 	Soltero 	\N	OLLANTAYS @GMAIL.COM 	CALLE 2 URBANIZACION VILLA ROSA BLOQUE 11 PB APTO 00-06 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
251	6964689	DA SILVA FERNANDEZ, MARIA SALOME	Venezolano 	1967-05-08	\N	\N	Femenino 	Soltero 	\N	msdasilvaf@ubv.edu.ve 	CARACAS 	0	ASISTENTE	DOCENTE	2010-04-20	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
252	6965029	GARCIA CALDERON, BELKYS CELESTE	Venezolano 	1967-06-11	\N	\N	Femenino 	Casado 	\N	bcgarciac@ubv.edu.ve 	URB. BELLO CAMPO CALLE M N 23 MATURIN 	1	ASISTENTE	DOCENTE	2006-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
253	6967020	URBINA DIAZ, TIBISAY COROMOTO	Venezolano 	1965-05-14	\N	\N	Femenino 	Divorciado 	\N	turbina@ubv.edu.ve 	URBANIZACION CARMEN ELINA VIA FLORES C/4 CASA D-39 SAN JUAN DE LOS MORROS 	0	ASISTENTE	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
254	6970614	PÉREZ HERNANDEZ, RODULFO HUMBERTO	Venezolano 	1965-06-20	\N	\N	Masculino 	Casado 	\N	 	URB. LA FUNDACION. ETAPA III. MANZANA 23. CASA 125-05-25 	0	INSTRUCTOR	DOCENTE	2006-10-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
255	6975559	CAPRILES VERGARA, MARISELA	Venezolano 	1966-12-10	\N	\N	Femenino 	Casado 	\N	 	BELLO MONTE CASA 5 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
256	6998152	VEGAS ECHEZURIA, WILLIAMS ROGELIO	Venezolano 	1969-10-05	\N	\N	Masculino 	Soltero 	\N	wvegas@ubv.edu.ve 	EL CALVARIO, CALLE CAMPOS ELIAS, CASA S/N OCUMARE 	2	INSTRUCTOR	DOCENTE	2005-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
257	7010559	PINEDA CASTILLO, WILFREDO RAMON 	Venezolano 	1961-04-15	\N	\N	Masculino 	Soltero 	\N	wpineda@ubv.edu.ve 	URBANIZACION LOMA LINDA AV PRINCIPAL A-52. GUACARA. CARABOBO 	0	INSTRUCTOR	DOCENTE	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
258	7017547	CARDENAS BENITEZ, JULIA JOSEFINA	Venezolano 	1961-03-16	\N	\N	Femenino 	Soltero 	\N	jcardenas@ubv.edu.ve 	URB EL PALOTAL SECTOR B VEREDA 12 N126 	0	INSTRUCTOR	DOCENTE	2008-01-14	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
259	7020631	TOVAR AGREDA, MISAEL	Venezolano 	1962-05-04	\N	\N	Masculino 	Casado 	\N	mtovar@ubv.edu.ve 	CALLE CASTAÑO N 11. GUACARA. EDO. CARABOBO 	0	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
260	7099374	CONTRERAS TREJO, IRIS ALIDA	Venezolano 	1965-03-07	\N	\N	Femenino 	Soltero 	\N	 	CALLE LOS ROSALES CASA N 189-30 TARAPIO 	0	INSTRUCTOR	CONTRATADO	2015-07-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
261	7155638	BOLÍVAR OJEDA, JULIO CESAR	Venezolano 	1961-08-06	\N	\N	Masculino 	Casado 	\N	jbolivar@ubv.edu.ve 	CONJ. RES. JUAN C. FALCON , EDIF. MANAURE 	3	ASOCIADO	DOCENTE	2005-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
262	7172322	TOVAR SALAS, ELBA GEOCONDA	Venezolano 	1963-11-15	\N	\N	Femenino 	Soltero 	\N	egtovar@ubv.edu.ve 	URB. SANTA CRUZ SECTOR 1,CALLE 1,VEREDA 20 CASA NUMERO 09.PUERTO CABELLO 	0	INSTRUCTOR	DOCENTE	2010-11-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
263	7199441	OSORIO ROJAS, JORGE HUMBERTO	Venezolano 	1960-05-26	\N	\N	Masculino 	Soltero 	\N	josorio@ubv.edu.ve 	CALLE  PRINCIPAL LA COOPERATIVA NORTE N30 MARACAY 	2	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
264	7209654	RODRIGUEZ ORELLANA, HECTOR HONORIO	Venezolano 	1961-06-16	\N	\N	Masculino 	Soltero 	\N	hhrodriguez@ubv.edu.ve 	CARRETERA TRAJANDINA, SECTOR SANARITO 	2	INSTRUCTOR	DOCENTE	2010-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
265	7237999	ABREU SELVIS, YANETH SORAYA	Venezolano 	1965-03-15	\N	\N	Femenino 	Soltero 	\N	yabreu@ubv.edu.ve 	CALLE EL ROSAL, PASAJE LAS PIÑAS N 6. BARRIO LA COOPERATIVA 	0	ASISTENTE	DOCENTE	2011-01-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
267	7264418	PACHECO MAICA, FERNANDO VICENTE	Venezolano 	1964-07-21	\N	\N	Masculino 	Soltero 	\N	fpacheco@ubv.edu.ve 	AV. BOLIVAR OESTE. SECTOR LA ROMANA. CASA N? 13 	2	INSTRUCTOR	DOCENTE	2011-06-09	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
268	7301346	RUIZ CUMARE, LISBETH ANTONIA	Venezolano 	1959-02-04	\N	\N	Femenino 	Casado 	\N	lruiz@ubv.edu.ve 	URBANIZACIÓN LOS OVEROS SUR. CASA N 34-17. LA ENCRUCIJADA. 	1	COORDINADOR PFG INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
269	7360388	VIERA NIEVES, CLAUDY FRANCISCO	Venezolano 	1964-03-05	\N	\N	Masculino 	Casado 	\N	cviera@ubv.edu.ve 	CALLE 60 ENTRE CARRERA 13-A Y 13-B 	3	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
270	7382824	JIMÉNEZ MARQUEZ, JUAN JULIAN	Venezolano 	1964-04-08	\N	\N	Masculino 	Soltero 	\N	jjjimenez@ubv.edu.ve 	KM 12 VIA DUACA,TAMACA VIA EL RETEN AL LADO DEL AMBULATORIO 	0	ASISTENTE	DOCENTE	2006-06-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
271	7421569	ALVAREZ DE RICO, YELITZA COROMOTO	Venezolano 	1968-12-19	\N	\N	Femenino 	Soltero 	\N	ycalvarez@ubv.edu.ve 	URB. LOS NARANJOS #49 CJTO RESIDENCIA PALMA REAL 	2	ASISTENTE	DOCENTE	2006-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
272	7429151	ANGEL MENDOZA, CARLOS JAVIER	Venezolano 	1970-11-17	\N	\N	Masculino 	Soltero 	\N	cjangel@ubv.edu.ve 	URBANIZACION LAS CAYENAS MANZANA 3 CASA N59 	1	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
273	7440595	PERALTA PEREZ, ISANIA PASTORA	Venezolano 	1969-06-01	\N	\N	Femenino 	Soltero 	\N	iperalta@ubv.edu.ve 	CARACAS 	3	INSTRUCTOR	DOCENTE	2010-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
274	7457884	RIOS MEDINA, NUBIA ELENA	Venezolano 	1960-12-25	\N	\N	Femenino 	Soltero 	\N	nrios@ubv.edu.ve 	AV, 1 URB. JOSE GIL FORTOUL BLOQUE 5, APTO C-5 PISO 3 	0	INSTRUCTOR	DOCENTE	2007-10-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
275	7492741	GUTIÉRREZ CHIRINOS, JOSÉ RAFAEL	Venezolano 	1960-10-24	\N	\N	Masculino 	Soltero 	\N	jrgutierrez@ubv.edu.ve 	URB. MONSEÑOR ITURRIZA AV. 2 NUM. 9 	2	AGREGADO	DOCENTE	2005-05-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
276	7498888	NAVAS CAMACHO, EDMUNDO MANUEL	Venezolano 	1963-11-20	\N	\N	Masculino 	Divorciado 	\N	enavas@ubv.edu.ve 	URB. CAMPO CLARO, CALLE 6, # 164 LA PUERTA MAVAREN 	3	AGREGADO	DOCENTE	2004-09-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
277	7566364	CALLES  NAVEDA, JOSÉ DE JESÚS	Venezolano 	1963-03-05	\N	\N	Masculino 	Casado 	\N	jcalles@ubv.edu.ve 	AV. E CON CALLE 5 URB. PEDRO MANUEL ARCAJE II ETAPA PUERTA MACARE 	3	ASISTENTE	DOCENTE	2005-10-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
278	7590250	ROBLES PARRA, YUVEIRA ZULMA	Venezolano 	1964-12-09	\N	\N	Femenino 	Soltero 	\N	yrobles@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2009-11-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
279	7606845	BORGES MOSQUERA, BELKIS DEL CARMEN	Venezolano 	1959-12-14	\N	\N	Femenino 	Soltero 	\N	bborges@ubv.edu.ve 	URBANIZACION LA TRINIDAD BLOQUE 12 AJUNTAS 	0	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
280	7607772	SUÁREZ LINARES, DULCE MARIA	Venezolano 	1960-04-10	\N	\N	Femenino 	Soltero 	\N	dmsuarez@ubv.edu.ve 	LOS CLAVELES AV. 41 333 96F-25 	1	ASISTENTE	DOCENTE	2004-08-20	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
281	7614821	FUENMAYOR FUENMAYOR, MARLYN DEL CARMEN	Venezolano 	1961-06-08	\N	\N	Femenino 	Soltero 	\N	mcfuenmayor@ubv.edu.ve 	URB.SANTA ISABEL.CALLE83-A.N74-31 	2	COORDINADOR PFG AGREGADO	DOCENTE	2004-10-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
282	7646996	ANDRADE DELGADO, LEONIDES JOSÉ	Venezolano 	1961-06-15	\N	\N	Masculino 	Soltero 	\N	landrade@ubv.edu.ve 	URB.LINDA BARINAS CALLE N8 CASA107-B 	0	INSTRUCTOR	DOCENTE	2007-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
283	7660585	MUJICA MACHADO, LILIANA MARGARITA	Venezolano 	1965-04-10	\N	\N	Femenino 	Soltero 	\N	lmujica@ubv.edu.ve 	CARACAS LA PASTORA MUNIPIO LIBERTADOR SET MANICOMIO 	1	ASISTENTE	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
284	7690115	VARGAS FARIA, CESAR EVELIO	Venezolano 	1963-04-02	\N	\N	Masculino 	Soltero 	\N	cvargas@ubv.edu.ve 	URB LA VICTORIA CALLE 67 N 78-52 IIETAPA MARACAIBO 	0	ASISTENTE	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
285	7694729	URDANETA VERA, YULEIDA JOSEFINA	Venezolano 	1964-09-30	\N	\N	Femenino 	Soltero 	\N	yjurdanetav@ubv.edu.ve 	CALLE 174- AV. 49. CASA # 173-80 EL CALLAO . MUNICIPIO SAN FRANCISCO 	0	ASISTENTE	DOCENTE	2004-05-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
286	7715550	PÉREZ SUAREZ, EVARISTO	Venezolano 	1961-09-11	\N	\N	Masculino 	Soltero 	\N	evperez@ubv.edu.ve 	URBANIZACION POMONA CALLE A NRO 14 	3	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
287	7738660	MERCADO GUEVARA, SANDRA LILIAM	Venezolano 	1961-09-20	\N	\N	Femenino 	Casado 	\N	smercado@ubv.edu.ve 	URB. PIEDRAS DEL SOL III CASA #634 VIA LA CONCEPCIÓN BARRIO RAFAEL  URDANETA 	2	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
288	7739901	PÉREZ SANCHEZ, ASCENCION NEREYDA	Venezolano 	1962-02-25	\N	\N	Femenino 	Soltero 	\N	anperezs@ubv.edu.ve 	CALLE 91B # 1C-36 SECTOR SANTA LUCIA 	1	INSTRUCTOR	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
289	7756746	MORALES FUENMAYOR, ZULY DEL CARMEN	Venezolano 	1963-02-26	\N	\N	Femenino 	Soltero 	\N	zdmorales@ubv.edu.ve 	URBANIZACION LA MARINA SECTOR 5 VEREDA 4 - N° 2 	1	ASISTENTE	DOCENTE	2005-04-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
290	7766917	RODRÍGUEZ SANCHEZ, ROXANA JOSEFINA	Venezolano 	1962-03-25	\N	\N	Femenino 	Soltero 	\N	rjrodriguez@ubv.edu.ve 	URBANIZACION, SAN JACINTO, SECTOR 4, TRANSVERSAL 4, N 05, MARACAIBO, EDO ZULIA 	1	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
291	7768773	PRIETO SANCHEZ, NORMAN CLARET	Venezolano 	1964-06-26	\N	\N	Femenino 	Soltero 	\N	ncprieto@ubv.edu.ve 	CALLE 44 N 15D-72 URB. CANAIMA, MARACAIBO 	1	ASISTENTE	DOCENTE	2004-05-29	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
292	7771407	FUENMAYOR, MIGUEL ANTONIO	Venezolano 	1963-01-18	\N	\N	Masculino 	Soltero 	\N	mafuenmayor@ubv.edu.ve 	AV. 8 SANTA RITA, SECTOR PUEBLO NUEVO, CALLE 59 N  9-126 (MARACAIBO ESTD. ZULIA 	0	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
293	7787540	CHÁVEZ MATHEUS, DORIS DEL CARMEN	Venezolano 	1963-08-12	\N	\N	Femenino 	Soltero 	\N	dchavez@ubv.edu.ve 	AV. 13 A, ENTRE CALLES 66A Y 67. N 66B-60 	1	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
294	7793727	VERDUN MADURO, CARLOS ALBERTO	Venezolano 	1961-05-10	\N	\N	Masculino 	Soltero 	\N	cverdun@ubv.edu.ve 	CALLE 4B CON 15 CASA N 47 -50 	0	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
295	7801949	BORREGO PORTILLO, MARÍA TERESA	Venezolano 	1964-12-11	\N	\N	Femenino 	Soltero 	\N	mtborrego@ubv.edu.ve 	URB. BELLA VISTA, CARRETERA #6, CALLE #18, CASA #17-26 	1	INSTRUCTOR	DOCENTE	2007-10-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
296	7810285	GRATEROL BARRETO, GREGORIO DE JESÚS	Venezolano 	1965-05-26	\N	\N	Masculino 	Soltero 	\N	ggraterol@ubv.edu.ve 	URBANIZACION LA TRINIDAD CALLE 52- A CASA N 15J - 36 	0	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
297	7813119	GARCIA HACCELVRINK, JOSÉ MARTIN	Venezolano 	1965-07-29	\N	\N	Masculino 	Soltero 	\N	jmgarcia@ubv.edu.ve 	AV. 3G ENTRE CALLES 67 Y 68 N 67 - 79 SECTOR BELLAS ARTES MARACAIBO EDO ZULIA 	1	AUXILIAR I	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
298	7820169	MEDINA CAMPOS, JESÚS NICOLAS	Venezolano 	1965-02-02	\N	\N	Masculino 	Soltero 	\N	jnmedina@ubv.edu.ve 	BARRIO CUATRICENTENARIO AV, 65B CASA N# 95C-39 	1	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
299	7828136	ZABALETA MORA, JOSÉ LUIS	Venezolano 	1965-03-14	\N	\N	Masculino 	Casado 	\N	jzabaleta@ubv.edu.ve 	UR. LAS TERRAZAS F CASA N3 	2	AGREGADO	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
300	7836612	GONZALEZ MARTINEZ, ASMERY GREGORIA	Venezolano 	1964-10-19	\N	\N	Femenino 	Soltero 	\N	aggonzalez@ubv.edu.ve 	CONJUNTO RESD. LAS PIRAMIDESS TORRE E APTO. 201 LA POMONA - MARACAIBO 	1	ASISTENTE	DOCENTE	2004-04-28	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
301	7841884	CARRASCO PIRELA, JUAN CARLOS	Venezolano 	1961-02-17	\N	\N	Masculino 	Soltero 	\N	jcarrasco@ubv.edu.ve 	CALLE LA LINEA CASA N8,SECTOR ISLA DE CUBA,PUERTO LA CRUZ 	0	INSTRUCTOR	DOCENTE	2007-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
302	7843614	MONTERO LEAL, JULIA VIRGINIA	Venezolano 	1963-01-31	\N	\N	Femenino 	Casado 	\N	jmontero@ubv.edu.ve 	URBANIZACION LAS PIEDRITAS LAS SABANITA CALLE UPATA CON CALLE NUEVA BOLIVAR 	2	INSTRUCTOR	DOCENTE	2005-04-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
303	7861010	BRICENO DE MENDEZ, DORA MARIA	Venezolano 	1966-04-04	\N	\N	Femenino 	Soltero 	\N	dmbriceno@ubv.edu.ve 	AVENIODA ANDRES BELLO, CALLE RANCHO GRNADE CASA # 07 SECTOR PUNTA ICOTEA CABIMAS ZULIA 	0	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
304	7886413	FUENMAYOR FUENMAYOR, JOSÉ ALBERTO	Venezolano 	1963-04-06	\N	\N	Masculino 	Soltero 	\N	jafuenmayor@ubv.edu.ve 	URB EL CAUJARO CALLE 199  49 H-2-18 ZONA POSTAL MARACAIBO 	1	AGREGADO	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
305	7890509	MORALES GUERRERO, MARÍA ISABEL	Venezolano 	1967-03-27	\N	\N	Femenino 	Casado 	\N	mmorales@ubv.edu.ve 	LOS SEMERUCOS CASA D1, CALLE MAITHIRUNIA MARAVEN 	1	ASISTENTE	DOCENTE	2007-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
306	7894834	VELARDE MORALES, ANAIS COROMOTO	Venezolano 	1966-02-17	\N	\N	Femenino 	Soltero 	\N	acvelarde@ubv.edu.ve 	URB CUMBRES DE MARACAIBO. RESD.LOMAS DE AMPORO CALLE 85 ENTRE AVENIDAS 62 Y63 	0	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
307	7908665	PARRA FIGUEROA, WALTER WILLIAN	Venezolano 	1967-10-06	\N	\N	Masculino 	Soltero 	\N	wparra@ubv.edu.ve 	CARACAS 	1	INSTRUCTOR	DOCENTE	2010-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
308	7923270	GARCIA BERMUDEZ, LUIS ENRIQUE	Venezolano 	1963-07-29	\N	\N	Masculino 	Soltero 	\N	legarcia@ubv.edu.ve 	BLOQUE 9 PISO 6 LETRA C APTO LOMAS DE URDANETA 	1	ASOCIADO	DOCENTE	2004-04-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
309	7947986	GARCIA CENTENO, MILY YOLANDA	Venezolano 	1972-07-12	\N	\N	Femenino 	Soltero 	\N	mygarcia@ubv.edu.ve 	SECTOR RAUL LEONI C/P BOQUERON 	3	INSTRUCTOR	DOCENTE	2010-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
310	7949450	ACOSTA PALMA, YVONNE AYARID	Venezolano 	1972-08-22	\N	\N	Femenino 	Soltero 	\N	yvonneacosta2010@hotmail.com 	URB LA MURALLA ETAPA 4 EL INGENIO CASA 13 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
311	7971137	OJEDA MORAN, MAIRA VERONICA	Venezolano 	1966-08-07	\N	\N	Femenino 	Soltero 	\N	mojeda@ubv.edu.ve 	RESD. VALLE CLARO SECTOR LAS LOMAS ED. CAROLINA APTO. 10A 	0	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
312	7973255	LAGUNA COELLO, DUVIS DEL CARMEN	Venezolano 	1965-10-20	\N	\N	Femenino 	Soltero 	\N	dlaguna@ubv.edu.ve 	SECTOR EL RECREO CALLE 95 	2	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
313	7979021	MEZA GODOY, WILMER ENRIQUE	Venezolano 	1968-02-23	\N	\N	Masculino 	Soltero 	\N	wmeza@ubv.edu.ve 	URB.EL CAUJARO CUARTO ESTACIONAMIENTO # 149G EDO. ZULIA 	1	AGREGADO	DOCENTE	2005-04-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
314	7990036	GARCÍA ORTIZ, ANA ROSA	Venezolano 	1967-05-20	\N	\N	Femenino 	Soltero 	\N	argarciao@ubv.edu.ve 	CALLEJON TACHIRA 4TA CALLE BARRIO AQUI ESTE CASA N 14 	1	INSTRUCTOR	CONTRATADO	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
315	7994519	CAñAS IZAGUIRRE, FLOR ERMELINDA	Venezolano 	1966-12-12	\N	\N	Femenino 	Soltero 	\N	fcanas@ubv.edu.ve 	PALO GORDO GULLERDIN VEREDA Y CASA 1-186 	1	COORDINADOR PFG AGREGADO	DOCENTE	2007-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
316	7994663	ONOREZ, TANIA	Venezolano 	1966-11-17	\N	\N	Femenino 	Soltero 	\N	tonorez@ubv.edu.ve 	URB. NUEVA CASARAPA , INTERCOMUNAL DE GUARENAS, CALLE ALAMBIQUE,  RESID. 6-C CASA- 33 	0	INSTRUCTOR	DOCENTE	2005-10-31	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
317	8025032	RIVAS URBINA, MARÍA CHIQUINQUIRÁ	Venezolano 	1962-09-22	\N	\N	Femenino 	Soltero 	\N	mcrivas@ubv.edu.ve 	JUAN PABLO II ,SECTOR FLORENTINO,Q 11 #6 BARINAS 	1	INSTRUCTOR	DOCENTE	2007-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
318	8025421	SALAZAR GUERRERO, CARINA EUNICE	Venezolano 	1963-04-07	\N	\N	Femenino 	Soltero 	\N	csalazar@ubv.edu.ve 	CALLE LAS GARCES, CONJUNTO RESIDENCIAL 450 AÑOS , EDF. EUCAPLITO. APTO. 73- E CORO - ESTADO FALCON 	1	ASOCIADO	DOCENTE	2004-09-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
319	8031335	MORA RONDÓN, JOSÉ NATIVIDAD	Venezolano 	1964-09-08	\N	\N	Masculino 	Soltero 	\N	jnmora@ubv.edu.ve 	URB. SANTA EDUVIGES CALLE PRINCIPAL ANEXO 1 CASA N* 1. PB 	0	INSTRUCTOR	DOCENTE	2012-07-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
320	8036623	PEÑA RANGEL, ANA JOSEFINA	Venezolano 	1965-11-05	\N	\N	Femenino 	Casado 	\N	ajpenar@ubv.edu.ve 	URBANIZACION LA LLOVIZNA CALLE NORTE CASA N41 	1	AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
321	8044253	BEUSES GALUE, REGINA ELIZABETH	Venezolano 	1968-02-21	\N	\N	Femenino 	Soltero 	\N	rbeuses@ubv.edu.ve 	JARDINES DE ALTO BARINAS. CONJUNTO RESIDENCIAL KARUACHI, N 33 	0	INSTRUCTOR	CONTRATADO	2015-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
322	8083287	PERNIA PERNIA, JESÚS ALIRIO	Venezolano 	1963-03-02	\N	\N	Masculino 	Soltero 	\N	japernia@ubv.edu.ve 	PARQUE CENTRAL EDUF. EL TEJAR, PISO 2 APTO. 2Q 	2	ASISTENTE	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
323	8098545	CASANOVA MORALES, MANUEL ANTONIO	Venezolano 	1964-04-11	\N	\N	Masculino 	Divorciado 	\N	mcasanova@ubv.edu.ve 	URBANIZACION EL PINAR, AV. 4, NUM. V4 - 20 SAN JUAN DE COLON, MUNICIPIO AYACUCHO DEL ESTADO TACHIRA 	3	ASISTENTE	DOCENTE	2006-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
324	8101895	TORRES ALVAREZ, LIGIA MARIA	Venezolano 	1966-03-11	\N	\N	Femenino 	Casado 	\N	ltorres@ubv.edu.ve 	AV. PRINCIPAL VILLAS DEL INGENIO. URB. VILLAS DEL INGENIO CASA 702 	2	INSTRUCTOR	CONTRATADO	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
325	8104992	MARINO BELEN, DIDIER ALFONSO	Venezolano 	1969-07-12	\N	\N	Masculino 	Soltero 	\N	damarinob@ubv.edu.ve 	AGUACATE, A SAN FRANCISQUITO, EDIFICIO EL CID PISO 7, APTT. 7-B, PARROQUIA SAN JUAN CARACAS 	2	ASISTENTE	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
326	8132210	VALETA RONDON, PABEL IVAN	Venezolano 	1962-02-14	\N	\N	Masculino 	Soltero 	\N	pvaleta@ubv.edu.ve 	URB. PRADOS DE ALTO BARINA CALLE 17 CASA N 57 	2	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
327	8142249	REGNAULT CARREÑO, LUISA MARGARITA	Venezolano 	1961-09-27	\N	\N	Femenino 	Divorciado 	\N	lregnault@ubv.edu.ve 	SECTOR STA.ROSA CALLE LUNA SOL N 850 	1	INSTRUCTOR	CONTRATADO	2013-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
328	8160691	VIÑA, JOSÉ ANGEL	Venezolano 	1957-08-02	\N	\N	Masculino 	Casado 	\N	jvina@ubv.edu.ve 	BARRIO RIO BLANCO, CALLE PLAZA N 51-B 	1	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
329	8177945	GABANTE ALVAREZ, ARGELIS	Venezolano 	1962-11-20	\N	\N	Femenino 	Soltero 	\N	agabante@ubv.edu.ve 	CONJUNTO RES. LA LAGUNA EDIFICIO V APTO. V-22 GUATIRE EDO. MIRANDA 	0	AGREGADO	DOCENTE	2004-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
330	8184541	GARRIDO, OSWALDO ANTONIO	Venezolano 	1964-06-13	\N	\N	Masculino 	Soltero 	\N	osgarrido@ubv.edu.ve 	AV.72 LA VICTORIE PRIMERA ETAPA RESD. VILLA LINDA CASA# 3 MARACAYBO 	0	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
331	8194637	VELASQUEZ FEBRES, GRACIELA AMELIA	Venezolano 	1964-12-21	\N	\N	Femenino 	Soltero 	\N	gavelasquez@ubv.edu.ve 	CALLE 5 UNRB DOÑA MENCA SECTOR II BOQUERON 	1	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
332	8229008	MARAPACUTO MOROCOIMA, EDUARDO JOSÉ	Venezolano 	1963-05-22	\N	\N	Masculino 	Casado 	\N	emarapacuto@ubv.edu.ve 	URB, VISTA HERMOSA CASA N 63 	1	INSTRUCTOR	DOCENTE	2007-05-02	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
333	8233053	RODRÍGUEZ RUIZ, YRIS MARGARITA	Venezolano 	1963-12-31	\N	\N	Femenino 	Soltero 	\N	ymrodriguezr@ubv.edu.ve 	COLINA DE SANTA MONICA RUTA NRO 1 EDIFICIO KARI- KARI PISO 5 APTO 51-A CARACAS 	0	ASISTENTE	DOCENTE	2005-10-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
334	8235021	BELISARIO SANTOYO, LAMBERTYS MARGARITA	Venezolano 	1966-09-17	\N	\N	Femenino 	Soltero 	\N	lbelisario@ubv.edu.ve 	CALLE CELIO ACOSTA EDIF PAMEGAL PISO 7 APTO 22 ENTRE LA AV. FCO DE MIRANDA Y CALLE SUCRE CHACAO 	0	COORDINADOR ACADEMICO AGREGADO	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
335	8268127	BETANCOURT PABIQUE, LISBEHT MARIA	Venezolano 	1972-11-19	\N	\N	Femenino 	Casado 	\N	lmbetancourt@ubv.edu.ve 	CALLE JUNCAL SUR 10-128 SECTOR DE GUAMACHITO 	0	ASISTENTE	DOCENTE	2007-09-24	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
336	8294973	GUILLEN PEDRIQUEZ, JHON WILLIAN	Venezolano 	1976-05-24	\N	\N	Masculino 	Soltero 	\N	jwguillen@ubv.edu.ve 	MONAGAS 	4	INSTRUCTOR	DOCENTE	2011-05-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
337	8295988	PULIDO MENDEZ, ALBERTO ILICH	Venezolano 	1975-11-01	\N	\N	Masculino 	Soltero 	\N	apulido@ubv.edu.ve 	URB. LA PARAGUA EDO. 1-17 C QPTON-12 SECTORA 	0	AGREGADO	DOCENTE	2004-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
338	8300818	GUERRA DE GONZALEZ, ELENA DEL VALLE	Venezolano 	1960-10-21	\N	\N	Femenino 	Casado 	\N	elenaguerra@ubv.edu.ve 	SECTOR D-3 N 113 OROPEZA CSTILLO PUERTO LA CRUZ 	1	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
339	8304138	ARQUIADEZ VERACIERTA, EUFRIS EZEQUIELA	Venezolano 	1962-04-13	\N	\N	Femenino 	Soltero 	\N	earquiadez@ubv.edu.ve 	CALLE LA LINEA NRO 118 URB LA CARAQUEÑA 	0	INSTRUCTOR	DOCENTE	2011-01-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
340	8318886	ARA DE RODRIGUEZ, LEIDA JOSEFINA	Venezolano 	1963-03-04	\N	\N	Femenino 	Casado 	\N	leidar@ubv.edu.ve 	CALLE GUAICAPURO NRO 35 SIERRA MAESTRA 	1	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
341	8341657	ARQUIADEZ VERACIERTA, CRUZ DEL MAR	Venezolano 	1967-05-01	\N	\N	Femenino 	Casado 	\N	cmarquiadez@ubv.edu.ve 	CALLE LA CRUZ N 21 SECTOR AGUA POTABLE POZUELOS 	1	INSTRUCTOR	DOCENTE	2011-05-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
342	8344002	RODRÍGUEZ DOMINGUEZ, LORENZO EMILIO	Venezolano 	1966-08-10	\N	\N	Masculino 	Soltero 	\N	lrodriguez@ubv.edu.ve 	CARACAS 	0	ASISTENTE	DOCENTE	2005-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
343	8358007	GARCIA, FRANKLIN YOEL	Venezolano 	1959-08-01	\N	\N	Masculino 	Soltero 	\N	 	URBANIZACION HUGO CHAVEZ II CALLE 3 CASA 21 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
344	8372438	MARIN MOTA, MIRNA DEL VALLE	Venezolano 	1962-11-17	\N	\N	Femenino 	Soltero 	\N	mmarin@ubv.edu.ve 	LA TOSCANA. AV. BOLIVAR. CASA S/N 	1	ASISTENTE	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
345	8394781	VALERIO, MAURY DEL CARMEN	Venezolano 	1963-09-22	\N	\N	Femenino 	Casado 	\N	mvalerio@ubv.edu.ve 	CUIDAD BOCA DEL RIO, CASA No. 14, CALLE 14 NUEVA ESPARTA 	0	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
346	8398093	SILVA SUNIAGA, JUAN CARLOS	Venezolano 	1964-11-02	\N	\N	Masculino 	Soltero 	\N	 	CCS 	0	ASISTENTE	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
347	8419942	AVILA GUTIERREZ, JESUS ENRRIQUE	Venezolano 	1966-06-20	\N	\N	Femenino 	Soltero 	\N	javila@ubv.edu.ve 	CALLE 5 DE JULIO CASA 530 CAMPO EL PORVENIR 	2	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
348	8442748	MAIZ , CRUZ MARIA	Venezolano 	1963-05-31	\N	\N	Femenino 	Divorciado 	\N	cmaiz@ubv.edu.ve 	URB INES ROMERO MANZ N 2 CASA N 5 CALLE N 3 	1	ASISTENTE	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
349	8444230	SERRANO SERRANO, MIGUEL EDUARDO	Venezolano 	1964-04-08	\N	\N	Masculino 	Soltero 	\N	miguelserrano64@hotmail.com 	URB.SUCRE AV. PRINCIPAL NRO 58 	0	INSTRUCTOR	CONTRATADO	2017-07-03	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
350	8447900	CORCEGA DE SIFONTES, EREDIS DIAMELYS	Venezolano 	1961-12-03	\N	\N	Femenino 	Soltero 	\N	edcorcega@ubv.edu.ve 	MIRAFLORES LA PANTALLA CASA N 2 	0	INSTRUCTOR	DOCENTE	2006-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
351	8453600	ACOSTA FRONTADO, LUISA DEL VALLE	Venezolano 	1965-05-10	\N	\N	Femenino 	Soltero 	\N	lacosta@ubv.edu.ve 	URB VILLA BLANCO LOS GUARITOS II 	2	INSTRUCTOR	DOCENTE	2007-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
352	8454227	SUBERO, ALFREDO RAFAEL	Venezolano 	1965-02-01	\N	\N	Masculino 	Soltero 	\N	asubero@ubv.edu.ve 	SECTOR LAS PALMITAS, CALLE CAMPO SUCRE NRO 14. 	1	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
353	8469166	SIFONTES MARCANO, NAIKA MARGARITA	Venezolano 	1964-08-17	\N	\N	Femenino 	Soltero 	\N	nsifontes@ubv.edu.ve 	CARRERA 2 A N 26 	0	INSTRUCTOR	DOCENTE	2007-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
354	8504361	JIMÉNEZ FERNANDEZ, RUTH MARY	Venezolano 	1967-07-11	\N	\N	Femenino 	Soltero 	\N	rmjimenez@ubv.edu.ve 	CALÑLE 65 EDIF. APTO 2-L SECTOR DON BOSCO- BELLA VISTA MARACAYBO 	2	ASOCIADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
355	8504566	ROJAS GIL, JESÚS DAVID	Venezolano 	1968-06-22	\N	\N	Masculino 	Soltero 	\N	jdrojasg@ubv.edu.ve 	URB, LAS GARZAS MANZ. 8 CASA 6 PUERTO ORDAZ, EDO. BOLIVAR 	1	INSTRUCTOR	DOCENTE	2004-03-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
356	8505029	PULIDO FRANCO, MAVIOLA DEL CARMEN	Venezolano 	1969-10-06	\N	\N	Femenino 	Soltero 	\N	mpulido@ubv.edu.ve 	URB LA PAAZ 1ERA ETAPA CALLE 97B AV 49 CASA N 97-90 	0	INSTRUCTOR	DOCENTE	2008-03-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
357	8505111	VILORIA PEREZ, VERONICA JOSEFINA	Venezolano 	1968-03-19	\N	\N	Femenino 	Soltero 	\N	vviloria@ubv.edu.ve 	CALLE 89 D. N 19C-59. SECTOR NUEVA - VIA 	2	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
358	8506727	BELTRÁN VILLALOBOS, MARÍA INES	Venezolano 	1969-03-25	\N	\N	Femenino 	Soltero 	\N	mbeltran@ubv.edu.ve 	AV. 3 ENTRE CALLES 19 Y20 EL MOJAN. 	2	ASOCIADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
359	8507360	PAZ CORREA, ANDRICKS LUZ	Venezolano 	1968-03-21	\N	\N	Femenino 	Soltero 	\N	alpazc@ubv.edu.ve 	URB. LA TRINIDAD CALLE 53B, 15N-66 	1	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
360	8508726	VILLALOBOS ORTEGAS, KARINA DEL CARMEN	Venezolano 	1970-10-13	\N	\N	Femenino 	Soltero 	\N	kvillalobos@ubv.edu.ve 	SECTOR PANAMERICANO. CALLE 71.  76A-09. 	1	AGREGADO	DOCENTE	2005-04-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
361	8540012	RIVAS FREITES, AGUSTIN ANTONIO	Venezolano 	1961-06-13	\N	\N	Masculino 	Soltero 	\N	aarivasf@ubv.edu.ve 	CARACAS 	2	ASISTENTE	DOCENTE	2010-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
362	8554183	SOLE PUERTA, NURIAN EMILIA	Venezolano 	1960-07-23	\N	\N	Femenino 	Soltero 	\N	nsole@ubv.edu.ve 	AV.MIRANDA,CASA .50-51.BARRIO ANTONIO JOSE DE SUCRE 	0	INSTRUCTOR	DOCENTE	2007-09-17	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
363	8592804	ALVAREZ MORALES, LUIS RAMON	Venezolano 	1964-09-26	\N	\N	Masculino 	Casado 	\N	lralvarez@ubv.edu.ve 	URB LA CANDELARIA CALLE LATINA EDIF LA REDOMA PISO 3 APTO 3-C 	1	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
364	8604383	PEÑA RUIZ, ROSA MERCEDES	Venezolano 	1967-09-16	\N	\N	Femenino 	Soltero 	\N	rmpena@ubv.edu.ve 	AV. 14 B ENTRE CALLES 83 Y 84 RES. NICOLE, AP. 1B 	2	ASOCIADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
365	8619675	ROJAS MARTÍNEZ, PEDRO LUCIANO	Venezolano 	1961-06-20	\N	\N	Masculino 	Soltero 	\N	plrojas@ubv.edu.ve 	URB. LAS ACACIAS, CALLE E, CASA #26 	0	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
366	8622864	FLAMES GONZALEZ, ABEL VICENTE	Venezolano 	1963-11-17	\N	\N	Masculino 	Soltero 	\N	aflames@ubv.edu.ve 	RES. AUTANA, APTO. 22, URB CENTRO ADMINISTRATITO 	4	ASOCIADO	DOCENTE	2006-05-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
367	8628832	MILANO RODRIGUEZ, ROSA DEUSDEDY	Venezolano 	1966-07-31	\N	\N	Femenino 	Soltero 	\N	rmilano@ubv.edu.ve 	URB. CIUDA CASARAPA, PARCELA 19, PARTE ALTA, EDF. # 8, APTO PB-E,GUARENAS, EDO. MIRANDA 	0	INSTRUCTOR	DOCENTE	2005-05-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
368	8661170	SANCHEZ SANCHEZ, SANDRA JANETH	Venezolano 	1967-05-15	\N	\N	Femenino 	Casado 	\N	sjsanchez@ubv.edu.ve 	URB VILLA BOLIVIA CALLE RESIDENCIAS TUMEREMO TORRE 1 PISO N 6 APTO N 3 	2	INSTRUCTOR	DOCENTE	2007-10-29	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
369	8663508	ARANGUREN, JOSE RAFAEL	Venezolano 	1965-12-10	\N	\N	Masculino 	Casado 	\N	josearangure@ubv.edu.ve 	URB. BOLÍVAR SUR, N 12 CALLE EL ESTADIOC N5 (3-10) LA VICTORIA 	1	ASISTENTE	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
370	8665206	AULAR QUIROZ, JOSÉ MIGUEL	Venezolano 	1966-01-26	\N	\N	Masculino 	Casado 	\N	jmaular@ubv.edu.ve 	CALLE MIRANDA C/C SUCRE. URB. LOS SAMANES II, CASA N 93-3 	2	AGREGADO	DOCENTE	2006-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
371	8683651	HENAO BERROTERAN, ANGÉLICA ROSA	Venezolano 	1969-10-02	\N	\N	Femenino 	Soltero 	\N	ahenao@ubv.edu.ve 	CALLE SAN PEDRO SECTOR SANTA EDUVIGIS LOS TEQUES MIRANDA 	2	ASOCIADO	DOCENTE	2004-09-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
372	8683888	MORALES CHAPELLIN, MEIBY	Venezolano 	1970-05-09	\N	\N	Femenino 	Soltero 	\N	meimorales@ubv.edu.ve 	URB.EL CEMENTERIO CALLE AYACUCHO 	1	INSTRUCTOR	DOCENTE	2008-02-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
373	8684086	BAZAN SEVILLA, FLOR	Venezolano 	1966-12-28	\N	\N	Femenino 	Soltero 	\N	fbazan@ubv.edu.ve 	SECTOR PUNTA BRAVA CALLE GUAICAIPURO RESD.MIRAFLORES 	0	INSTRUCTOR	DOCENTE	2008-02-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
374	8698688	PARRA BETANCOURT, JESÚS OSWALDO	Venezolano 	1967-07-18	\N	\N	Masculino 	Soltero 	\N	joparra@ubv.edu.ve 	BARRIO SIERRA MAESTRA CALLE # 9 ENTRE AV 15 Y  61 # 15A-34 	2	ASOCIADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
375	8737188	GALINDO MUNDARAY, HÉCTOR RAFAEL	Venezolano 	1965-07-26	\N	\N	Masculino 	Soltero 	\N	hgalindo@ubv.edu.ve 	CARACAS 	0	ASISTENTE	DOCENTE	2004-10-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
376	8758778	SEGOVIA TORRES, ZENITH DAVID	Venezolano 	1967-02-02	\N	\N	Masculino 	Soltero 	\N	zsegovia@ubv.edu.ve 	URB LA URBINANA  CALLE 7 RES MONTE RIO PISO 3 APTO 3-C CARACAS 	1	ASISTENTE	DOCENTE	2006-10-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
377	8764768	TORRES GONZALEZ, WINDY JOSEFINA	Venezolano 	1966-10-21	\N	\N	Femenino 	Soltero 	\N	 	URB.OROPEZA CASTILLO, RES,SUCRE PISO 2, APTO 0205 	0	INSTRUCTOR	CONTRATADO	2018-02-05	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
378	8768653	SILVA ALZURO, CARMEN ABEL	Venezolano 	1967-06-03	\N	\N	Masculino 	Soltero 	\N	csilva@ubv.edu.ve 	URB LOS POMELOS CALLE E # 147 BARINHAS EDO BARINAS 	2	ASISTENTE	DOCENTE	2006-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
379	8774249	RODRÍGUEZ FEO, JOSÉ BERNARDINO	Venezolano 	1970-06-04	\N	\N	Masculino 	Soltero 	\N	jbrodriguez@ubv.edu.ve 	PARROQUIA SANTA ROSALIA EDIF DON  JUIO 1 TORRE D APTO 41 	0	ASOCIADO	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
380	8776015	ALVAREZ, JESÚS ALEXANDER	Venezolano 	1972-07-18	\N	\N	Masculino 	Casado 	\N	jalalvarez@ubv.edu.ve 	CALLEJON ISMAEL GUANIOA ENTRE VENEZUELA Y EDCUADOR S/N SECTOR LA CAÑADA 	1	ASISTENTE	DOCENTE	2004-09-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
381	8782819	JIMÉNEZ SANCHEZ, MARTIN RAMON	Venezolano 	1965-06-20	\N	\N	Masculino 	Soltero 	\N	mrjimenez@ubv.edu.ve 	RESD. JOSE OCTAVIO HENRRIQUEZ, CALLE 21, CASA # 02 	1	INSTRUCTOR	DOCENTE	2007-10-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
382	8787136	MEDINA MARIN, AQUILES JOSE	Venezolano 	1967-06-04	\N	\N	Masculino 	Soltero 	\N	ajmedina@ubv.edu.ve 	CARACAS 	2	ASISTENTE	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
383	8819443	TOVAR ROMERO, BELEN DEL CARMEN	Venezolano 	1968-07-20	\N	\N	Femenino 	Soltero 	\N	btovar@ubv.edu.ve 	AV 10 DE DICIEMBRE CASA 89 CASCO CENTRAL MARACAY 	2	INSTRUCTOR	DOCENTE	2012-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
384	8820362	HERRERA CORRALES, FANNY COROMOTO	Venezolano 	1966-02-04	\N	\N	Femenino 	Soltero 	\N	fcherrera@ubv.edu.ve 	CALLE COMERCIO SECTOR EL CEMENTERIO N 4 VILLA DE CURA 	1	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
385	8845872	ARISMENDI TORREALBA, YANITZA BELL	Venezolano 	1967-03-06	\N	\N	Femenino 	Soltero 	\N	yarismendi@ubv.edu.ve 	URB MALAVE VILLALBA CONJUNTO N2 EDIF 11 APTO 7 APTO 7-2 	0	INSTRUCTOR	CONTRATADO	2015-07-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
386	8870937	CEDEÑO ZAMBRANO, SALOME ALBANIA	Venezolano 	1963-02-15	\N	\N	Femenino 	Soltero 	\N	scedeno@ubv.edu.ve 	CALLE CAÑACO # 109 LA SABANITA CIUDAD BOLIVAR 	1	ASISTENTE	DOCENTE	2004-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
387	8871533	BRAVO JIMENEZ, MILAGROS COROMOTO	Venezolano 	1962-05-17	\N	\N	Femenino 	Casado 	\N	mbravo@ubv.edu.ve 	RESIDENCIAS LOS TURPIALES TORRE B PISO 3 APTO 3-2 CALLE LA GRANJA - LA SABANITA 	1	AGREGADO	DOCENTE	2005-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
388	8872790	FLORES ALMADA, JESÚS RIFER	Venezolano 	1965-04-24	\N	\N	Masculino 	Casado 	\N	jflores@ubv.edu.ve 	CALLE LEZAMA N 22 CASCO HISTORICO 	0	AUXILIAR I	DOCENTE	2004-09-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
389	8874875	TIRADO, PEDRO JESUS 	Venezolano 	1959-05-18	\N	\N	Masculino 	Soltero 	\N	ptirado@ubv.edu.ve 	LA MATA DE TAPACURE 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
487	9981653	MEJIAS, ALEXANDER JOSÉ	Venezolano 	1970-05-01	\N	\N	Masculino 	Soltero 	\N	amejias@ubv.edu.ve 	CALLE AZCUES. 26 NUMERO 02, MATURIN 	2	ASISTENTE	DOCENTE	2006-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
390	8876163	TOMEDES DE CORDOVA, JASMIN DEL CARMEN	Venezolano 	1960-12-21	\N	\N	Femenino 	Casado 	\N	jtomedes@ubv.edu.ve 	CALLE LIBERTAD N 68 CASCO HISTORICO 	2	ASISTENTE	DOCENTE	2007-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
391	8879425	ROSAL ARIAS, DUNIA DEL CARMEN	Venezolano 	1965-10-03	\N	\N	Femenino 	Soltero 	\N	drosal@ubv.edu.ve 	URB. LA CEIBA MANZANA CIUDAD BOLIVAR 	1	AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
392	8884136	FIGUERA LARA, LUIS ALFREDO	Venezolano 	1965-07-21	\N	\N	Masculino 	Soltero 	\N	lfiguera@ubv.edu.ve 	URB. SIMON BOLIVAR CALLE MARIÑO, NRO. 8-1 CIUDAD BOLIVAR. 	0	AGREGADO	DOCENTE	2005-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
393	8884229	GARCIA DE REQUENA, NORIS DE JESUS	Venezolano 	1964-10-08	\N	\N	Femenino 	Casado 	\N	ndgarcia@ubv.edu.ve 	PUEBLO NUEVO DE BONGO, AUTOP. CDAD. BOLIVAR - PTO. ORDAZ 	0	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
394	8885660	MARTINEZ FUENTES, GINETTE ISOLINA	Venezolano 	1967-04-21	\N	\N	Femenino 	Soltero 	\N	gimartinez@ubv.edu.ve 	URB. MARHUANTA MANZANA G N 07 CIUDAD BOLIVAR 	0	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
395	8889751	ROMERO CHAURAN, JEANNETH CECILIA	Venezolano 	1967-01-28	\N	\N	Femenino 	Casado 	\N	jromero@ubv.edu.ve 	URB LOS COQUITOS VEREDA 30 SECTOR 2- Edo Bolívar 	1	ASISTENTE	DOCENTE	2005-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
396	8894563	LUGO BOLIVAR, FELSON ALFONSO	Venezolano 	1966-02-12	\N	\N	Masculino 	Soltero 	\N	flugo@ubv.edu.ve 	ESTADO BOLIVAR 	1	AGREGADO	DOCENTE	2003-12-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
397	8895387	GUTIERREZ SANCHEZ, ARNARDO EUSEBIO	Venezolano 	1966-05-15	\N	\N	Masculino 	Soltero 	\N	aegutierrez@ubv.edu.ve 	URBANIZACION MARHUANTA M CALLE N 8 CASA N 28 	0	COORDINADOR PFG INSTRUCTOR	DOCENTE	2011-01-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
398	8895585	SALLOUM SALAZAR, ANTONIO ELIAS	Venezolano 	1969-01-16	\N	\N	Masculino 	Casado 	\N	asalloum@ubv.edu.ve 	CALLE CARABOBO N 76 CASCO HISTORICO 	1	ASOCIADO	DOCENTE	2005-10-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
399	8896786	SOTO, REINALDO	Venezolano 	1968-11-11	\N	\N	Masculino 	Soltero 	\N	rsoto@ubv.edu.ve 	AV. SAN SALVADOR NUM. 28, LA SABANITA. CIUDAD BOLIVAR - EDO BOLIVAR 	2	INSTRUCTOR	DOCENTE	2005-10-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
400	8898373	PARRA, ROGER JOSÉ	Venezolano 	1965-09-07	\N	\N	Masculino 	Soltero 	\N	roparra@ubv.edu.ve 	URB LAS VILLAS DEL ORINOCO CALLE EL MORICHAL CASA 33 	2	AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
401	8903369	PADRON, JESÚS ANTONIO	Venezolano 	1959-10-10	\N	\N	Masculino 	Soltero 	\N	jepadron@ubv.edu.ve 	SECTOR LOS LIRIOS, URB. LA FLORIDA, CASA S/N 	0	INSTRUCTOR	DOCENTE	2007-05-30	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
402	8903404	DIAZ BONA, FAVIO JESUS	Venezolano 	1960-10-15	\N	\N	Masculino 	Casado 	\N	fjdiaz@ubv.edu.ve 	BARRIO MORICHALITO, SECTOR JIVI, CASA S/N 	3	INSTRUCTOR	CONTRATADO	2015-07-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
403	8914931	GUERRA HIGUERA, JOSE GREGORIO	Venezolano 	1969-11-15	\N	\N	Masculino 	Soltero 	\N	jgguerra@ubv.edu.ve 	BARRIO JOSE A PAEZ II VEREDA 5 CASA N5 CALLE PAULA DE SANTANDER 	0	ASISTENTE	DOCENTE	2010-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
404	8915339	MOSQUERA AULAR, MARILYN	Venezolano 	1960-12-26	\N	\N	Femenino 	Soltero 	\N	mamosquera@ubv.edu.ve 	URB LOS PROCERES MANZANA 32 N 40 III ETAPA 	0	AGREGADO	DOCENTE	2005-04-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
405	8926612	MIRABAL RODRIGUEZ, MIREYA DEL VALLE	Venezolano 	1963-02-15	\N	\N	Femenino 	Soltero 	\N	mdmirabal@ubv.edu.ve 	URB. VENEZUELA, SECTOR ALTAMIRA, CALLE VENEZUELA, CASA #2 	0	ASISTENTE	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
406	8937089	MAESTRE VARGAS, LEONARDO RAFAEL	Venezolano 	1965-05-09	\N	\N	Masculino 	Casado 	\N	lmaestre@ubv.edu.ve 	URB. MARHUANTA CALLE MANZ A CASA N 08 ESTADO BOLIVAR 	1	AGREGADO	DOCENTE	2005-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
407	8939736	HURTADO GARRIDO, ELADIA MARGARITA	Venezolano 	1963-11-22	\N	\N	Femenino 	Casado 	\N	ehurtado@ubv.edu.ve 	MANZ A. PRIMERA N 8 CIUDAD BOLIVAR 	2	AGREGADO	DOCENTE	2004-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
408	8946813	RIVERA URIBE, JOSÉ SALOMON	Venezolano 	1963-03-15	\N	\N	Masculino 	Soltero 	\N	jrivera@ubv.edu.ve 	URB LA FLORIDA SDA CAÑLLE # 525 CARACAS 	2	INSTRUCTOR	DOCENTE	2006-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
409	8959051	BORJAS FIGUEROA, MIRIAN LISBET	Venezolano 	1965-07-21	\N	\N	Femenino 	Soltero 	\N	mlborjas@ubv.edu.ve 	CALLE ANTONIO BARQUERO,CAS # 66-80, UD-145 URB. LA LLOVIZNA, SAN FELIX 	0	INSTRUCTOR	DOCENTE	2006-09-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
410	9062753	PARADA GOMEZ, JULIO MANUEL	Venezolano 	1965-01-27	\N	\N	Masculino 	Soltero 	\N	jmparada@ubv.edu.ve 	SECTOR U.D-4,TERRAZA A BLOQUE,55,PISO 5,APTO.0502,CARICUAO 	2	ASISTENTE	DOCENTE	2005-10-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
411	9098539	RONDON RODRIGUEZ, JILIAN DEL VALLE	Venezolano 	1964-11-27	\N	\N	Femenino 	Soltero 	\N	jdrondon@ubv.edu.ve 	AVENIDA BELLA VISTA URB CAMPO CLARO 3RA TRANSVERSAL CASA 81 	0	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
412	9098626	BRICEÑO LINARES, GIOCONDA	Venezolano 	1964-02-07	\N	\N	Femenino 	Soltero 	\N	glinares@ubv.edu.ve 	AV.PRINCIPAL DE SAN INES CON CALLE D.QUINTA LOS ABUELOS.URB.SANTA INES BARUTA 	1	AGREGADO	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
413	9164012	CASADIEGO DELGADO, YVONNE GRACIELA	Venezolano 	1961-12-31	\N	\N	Femenino 	Soltero 	\N	ycasadiego@ubv.edu.ve 	CCS 	1	INSTRUCTOR	DOCENTE	2011-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
414	9194869	BRICEÑO PEÑA, YASMINA COROMOTO	Venezolano 	1964-12-22	\N	\N	Femenino 	Casado 	\N	ybriceno@ubv.edu.ve 	URB. VISTA HERMOSA CALLE N 8 CON CARRERA N 5 RESIDENCIA CILLA FABIANA CASA N 06 	0	INSTRUCTOR	DOCENTE	2007-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
415	9210507	ORTEGA, NURY MILADY 	Venezolano 	1965-03-06	\N	\N	Femenino 	Soltero 	\N	nortega@ubv.edu.ve 	URB. LOMAS DE TOLQUITO CASA N31 PALMIRA 	1	ASISTENTE	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
416	9219444	GAMBASICA DE DIAZ, GLADYS	Venezolano 	1965-06-18	\N	\N	Femenino 	Casado 	\N	ggambasica@ubv.edu.ve 	BARRIO SANTA TERESA CALLE  2, NUM. 2 133, SAN CRISTOBAL EDO TACHIRA 	0	COORDINADOR PFG ASISTENTE	DOCENTE	2006-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
417	9231328	ZAMBRANO BERNAL, FRANKLIN  MIGUEL	Venezolano 	1967-11-05	\N	\N	Masculino 	Soltero 	\N	fzambrano@ubv.edu.ve 	URB. ALTOS DE PARAMILLO CALLE LOS SACUCESS, MANZANA 16 PARULAI, PALO VERDE 	0	INSTRUCTOR	DOCENTE	2010-08-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
418	9243749	ARIZA, MARIBEL	Venezolano 	1966-10-30	\N	\N	Femenino 	Casado 	\N	mariza@ubv.edu.ve 	AV MARGINALDEL TORBES BARRIO SAN CRISTOBAL VEREDA N 3 3-A2 	3	INSTRUCTOR	DOCENTE	2011-01-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
419	9246819	VIVAS HEVIA, RAMON ALIPIO	Venezolano 	1970-01-24	\N	\N	Masculino 	Soltero 	\N	rvivas@ubv.edu.ve 	CALLE 4 N 8-60 SECTOR GRAMALOTE PALMIRA MUNICIPIO GUASIMO 	0	ASISTENTE	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
420	9250441	CHÁVEZ TORREALBA, IVÁN ELICIRIO	Venezolano 	1964-01-09	\N	\N	Masculino 	Soltero 	\N	iechavez@ubv.edu.ve 	BARRIO LOS POSONES, CALLE 10 CARRERA S/N RESIDENSIA 12-29 	1	INSTRUCTOR	DOCENTE	2007-12-03	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
421	9261430	BENITEZ MONTOYA, VALENTINA VICTORIA	Venezolano 	1963-10-03	\N	\N	Femenino 	Divorciado 	\N	benitezmontoya@gmail.com 	LOMAS DE ALTO BARINAS, CONJUNTO LAGUNITA,#24 	0	INSTRUCTOR	CONTRATADO	2015-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
422	9280038	PRIETO, ALCIDES RAFAEL	Venezolano 	1963-06-19	\N	\N	Masculino 	Soltero 	\N	aprieto@ubv.edu.ve 	CALLE COLON N 16 URBANIZACION LOS COCOS 	2	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
423	9284134	CARPINTERO GARCIA, GRACIELA JOSEFINA	Venezolano 	1965-12-15	\N	\N	Femenino 	Soltero 	\N	gcarpintero@ubv.edu.ve 	CALLE 8 BRISAS DEL ORINOCO 	0	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
424	9288626	BLANCO, JOSÉ GREGORIO	Venezolano 	1965-05-01	\N	\N	Masculino 	Soltero 	\N	jgblanco@ubv.edu.ve 	CAICARA DE MATURIN SECTOR LA TOMATERA AV BERMUDEZ FINAL SN 	1	ASISTENTE	DOCENTE	2007-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
425	9292743	MARCANO FERNANDEZ, JESUS ALEJANDRO	Venezolano 	1967-10-14	\N	\N	Masculino 	Casado 	\N	jamarcano@ubv.edu.ve 	MONAGAS-MATURIN 	2	TITULAR	DOCENTE	1996-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
426	9293799	VILLEGAS DE RODRIGUEZ, REYNA MARGARITA	Venezolano 	1963-07-27	\N	\N	Femenino 	Casado 	\N	rvillegas@ubv.edu.ve 	VEREDA 68 N 3 LOS GODOS 	1	ASISTENTE	DOCENTE	2006-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
427	9294786	CABELLO GUILLENT, MAIRA ROSA	Venezolano 	1968-03-15	\N	\N	Femenino 	Casado 	\N	mcabello@ubv.edu.ve 	URB.LAS VIRGENES CALLE.8 CASA 160, 	0	INSTRUCTOR	DOCENTE	2007-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
428	9298732	WESTPHAL DONOSO, SYLVIA NEVENKA	Venezolano 	1963-04-07	\N	\N	Femenino 	Soltero 	\N	swestphal@ubv.edu.ve 	AV.LIBERTADOR, EDIF. TAMA, TORRE B, APTO 9-D. 	0	INSTRUCTOR	CONTRATADO	2016-06-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
429	9298734	WESTPHAL DONOSO, JUAN RICARDO	Venezolano 	1960-06-20	\N	\N	Masculino 	Soltero 	\N	jwestphal@ubv.edu.ve 	AV. LIBERTADOR. SECTOR FUNDEMOS. RES. TAMA 	0	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
430	9299828	FEBRES DE BRITO, ANA ARACELYS	Venezolano 	1968-08-20	\N	\N	Femenino 	Casado 	\N	afebres@ubv.edu.ve 	URB. DOÑA MENCA DE LEONI, CALLE 5, CASA N 13 	2	ASISTENTE	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
431	9300146	GONZALEZ, DEL VALLE MARIA	Venezolano 	1966-06-16	\N	\N	Femenino 	Soltero 	\N	degonzalez@ubv.edu.ve 	AV 4 DE MAYO EDIFICIO SAN VALENTIN TORREO 2 PISO 2 APTO 23 PORLAMAR 	2	AGREGADO	DOCENTE	2006-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
432	9340255	LOCATELLI COLMENARES, ELMER DARIO	Venezolano 	1972-11-11	\N	\N	Masculino 	Soltero 	\N	elocatelli@ubv.edu.ve 	CALLE NEGRIN CRUCE CON POVNIR EDIFICIO FABRIZIO PISO 6 APTO 32 URB LA FLORIDA 	2	AGREGADO	DOCENTE	2004-04-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
433	9347442	BRICEÑO ORTIZ, ILIANA EMPERATRIZ	Venezolano 	1975-08-08	\N	\N	Femenino 	Soltero 	\N	ibriceno@ubv.edu.ve 	URB. URDANETA CALLE 4 VEREDA 47 N 13 SABANETA 	2	INSTRUCTOR	DOCENTE	2012-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
434	9387902	RONDÓN, JAVIER JOSÉ	Venezolano 	1967-08-10	\N	\N	Masculino 	Soltero 	\N	jjrondon@ubv.edu.ve 	LA CAVIA, CALLE 06, PRINCIPAL 	0	AGREGADO	DOCENTE	2006-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
435	9397326	CALDERÓN, YARITZA JOSEFINA	Venezolano 	1967-12-31	\N	\N	Femenino 	Soltero 	\N	yjcalderon@ubv.edu.ve 	URB. PEDRO MANUEL ARCAYA, II EATAPA MANZANA C, CASA 12 ANTRE AV. D Y C 	0	INSTRUCTOR	DOCENTE	2007-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
436	9412498	PARISCA GRANADOS, MARIBEL JOSEFINA	Venezolano 	1968-12-03	\N	\N	Femenino 	Soltero 	\N	mjparisca@ubv.edu.ve 	CALLE N° 1 RESIDENCIAS FONVICA, PISO 20 APTO 201 URB LOS JARDINES DEL VALLE AV INTERCOMUNAL E LOS JA 	1	ASISTENTE	DOCENTE	2005-03-28	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
437	9416685	GRANADILLO DE BERRIOS, ALICIA ANTONIA	Venezolano 	1969-02-10	\N	\N	Femenino 	Soltero 	\N	agranadillo@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2010-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
438	9420357	GOMEZ GARANTON, LISANDRA	Venezolano 	1967-12-02	\N	\N	Femenino 	Divorciado 	\N	lgomez@ubv.edu.ve 	SECTOR PUNTA , CALLE MANEIRO, EDIF. -9-90 	0	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
439	9455208	OLIVIER HURTADO, AMANDA ROSA	Venezolano 	1967-08-27	\N	\N	Femenino 	Casado 	\N	aolivier@ubv.edu.ve 	SECTOR JONOTE CALLE JOSE HERNANDEZ CASA # 132 	2	AGREGADO	DOCENTE	2004-09-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
440	9471800	CASTRO A, ROSANA	Venezolano 	1968-11-13	\N	\N	Femenino 	Soltero 	\N	rcastro@ubv.edu.ve 	CARACAS 	1	AGREGADO	DOCENTE	2004-10-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
441	9517339	QUERALES GUTIERREZ, RAFAEL ANTONIO	Venezolano 	1967-10-19	\N	\N	Masculino 	Soltero 	\N	rquerales@ubv.edu.ve 	SECTOR LOS LAURELES VIEJOS CALLE TRUJILLO *13 	0	INSTRUCTOR	DOCENTE	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
442	9521054	CHIRINO, JONNY RAFAEL	Venezolano 	1965-04-04	\N	\N	Masculino 	Soltero 	\N	jochirino@ubv.edu.ve 	CALLE 12 FEDEPETROL, CASA 9-1-78 QUINTA ALBA 	0	AGREGADO	DOCENTE	2005-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
443	9521097	GONZALEZ NARANJO, INGRY JOSEFINA	Venezolano 	1967-05-13	\N	\N	Femenino 	Soltero 	\N	ijgonzalez@ubv.edu.ve 	CONJUNTO RESIDENCIAL JUAN CRISOSTOMO FALCON, EDIF. MANAURE, PB, APTO. PB-6 	2	ASOCIADO	DOCENTE	2005-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
444	9544990	RODRÍGUEZ CASTILLO, YULEIMA DEL PILAR	Venezolano 	1963-05-29	\N	\N	Femenino 	Soltero 	\N	ydprodriguez@ubv.edu.ve 	CARACAS 	0	ASISTENTE	DOCENTE	2005-03-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
445	9583767	MEDINA, FRANKLIN FRANCISCO	Venezolano 	1963-12-09	\N	\N	Masculino 	Soltero 	\N	framedina@ubv.edu.ve 	CALLE BOLIVAR N- 68 URB. PUNTA CARDOR 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
446	9584387	CONTRERAS REYES, SUZANNE CATY	Venezolano 	1966-05-06	\N	\N	Femenino 	Soltero 	\N	scontreras@ubv.edu.ve 	2DA AV. URB. ZARABON N 9-39, CAMPO MARAVEN, PUNTO FIJO 	0	INSTRUCTOR	DOCENTE	2007-06-04	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
447	9584519	REYES, AMERICA ELOINA	Venezolano 	1965-02-25	\N	\N	Femenino 	Soltero 	\N	 	FUNDACION MIGUEL ACHE GUBAIRA CALLE PAEZ NR83 	0	INSTRUCTOR	CONTRATADO	2019-05-31	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
448	9585175	MARTINEZ GUTIERREZ, ANNA ESTHER	Venezolano 	1965-12-28	\N	\N	Femenino 	Soltero 	\N	aemartinez@ubv.edu.ve 	CALLE ZAMORA N.46 PTA.CARDOR CSA S/N 	1	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
449	9586481	GONZALEZ DE JURADO, CLAUDIA ROSA	Venezolano 	1967-06-30	\N	\N	Femenino 	Casado 	\N	crgonzalez@ubv.edu.ve 	AV. PRINCIPAL CRUCE CON CIRCULO DON BOSCO CASA #07 GUANADITO SUR 	1	ASOCIADO	DOCENTE	2005-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
450	9653066	DEFFITT HURTADO, SARITA DEL VALLE	Venezolano 	1968-12-13	\N	\N	Femenino 	Casado 	\N	sdeffitt@ubv.edu.ve 	URB.LAS DELICIAS MANZANA T N30 SECTOR SANTA RITA 	1	ASISTENTE	DOCENTE	2010-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
451	9662045	DIAZ LOPEZ, FANY MARIA	Venezolano 	1969-02-26	\N	\N	Femenino 	Soltero 	\N	fdiaz@ubv.edu.ve 	URB. ISAAC OLIVER CALLE 1 CASA N 26. 	3	ASISTENTE	DOCENTE	2011-01-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
452	9662919	ROMAN MONTILLA, JOSÉ GREGORIO	Venezolano 	1969-07-30	\N	\N	Masculino 	Soltero 	\N	jroman@ubv.edu.ve 	CALLE AGUSTIN CODAZZI N 16 BARRIO LA CRUZ EL LIMÓN 	0	ASISTENTE	DOCENTE	2006-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
453	9676470	PARRA ARCIA, EDGAR JOEL	Venezolano 	1971-02-14	\N	\N	Masculino 	Casado 	\N	eparra@ubv.edu.ve 	CALLE RIO CARONI  CRUCE CON AV. LA CASCADA NO. 40. URB. LA FONTADA 	2	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
454	9686086	ROBLES SOTILLO, CARLOS ARTURO	Venezolano 	1972-12-22	\N	\N	Masculino 	Soltero 	\N	carobles@ubv.edu.ve 	URB. CAÑA DE AZUCAR SECTOR 6 UD-9. EDIF 43, APTO. 01-04 	1	ASISTENTE	DOCENTE	2006-03-27	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
455	9700215	TAPIA COVA, ÁNGEL EDUARDO	Venezolano 	1966-01-01	\N	\N	Masculino 	Soltero 	\N	aetapia@ubv.edu.ve 	AV 16 CON CALLE 78 EDF  SILEG APTO 6B 	0	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
456	9700317	ESTEVEZ MEDINA, JUAN FRANCISCO DEL CARMEN 	Venezolano 	1966-01-29	\N	\N	Masculino 	Soltero 	\N	jestevez@ubv.edu.ve 	URB SAN JACINTO SECTOR 2, AV.2 CASA NRO 36 	0	INSTRUCTOR	CONTRATADO	2016-06-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
457	9707090	BENITEZ GONZALEZ, BERENICE	Venezolano 	1966-10-09	\N	\N	Femenino 	Soltero 	\N	bbenitez@ubv.edu.ve 	AV. JUAN CANCIO RODRIGUEZ, N 7-59. LA ASUNCION NUEVA ESPARTA 	0	AGREGADO	DOCENTE	2006-06-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
458	9713312	MAVAREZ BORJAS, JAVIER JOSÉ	Venezolano 	1967-11-08	\N	\N	Masculino 	Soltero 	\N	jjmavarez@ubv.edu.ve 	AV 26 A-252 SECTORV    EL MANZANILLO VIA SAN FRANCISCO 	0	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
459	9721318	MORALES DE CASTILLO, FATIMA	Venezolano 	1969-02-24	\N	\N	Femenino 	Soltero 	\N	famorales@ubv.edu.ve 	B/BALMIRO LEON C/334 N 92-110 	1	ASISTENTE	DOCENTE	2006-10-30	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
460	9722178	MOROS RODRIGUEZ, JOSÉ LUIS	Venezolano 	1968-02-01	\N	\N	Masculino 	Soltero 	\N	jmoros@ubv.edu.ve 	ZARABON, AV. 1B, CASA 11-21 	1	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
461	9727841	VILLAVICENCIO DIAZ, MIGDALIA ELISA	Venezolano 	1966-02-01	\N	\N	Femenino 	Soltero 	\N	mvillavicencio@ubv.edu.ve 	SECTOR ALTOS DE JASINTO.AV.5/C.C/C 45N#5A-121 	2	ASISTENTE	DOCENTE	2005-04-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
462	9735794	TORRENEGRA ORTEGON, MILDRED	Venezolano 	1970-01-04	\N	\N	Femenino 	Soltero 	\N	mtorrenegra@ubv.edu.ve 	BARRIO CUATRICENTENARIO, AV 65B 95C-39 	1	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
463	9740226	URRIBARI VASQUEZ, POLLY ANNA	Venezolano 	1969-05-16	\N	\N	Femenino 	Soltero 	\N	purribari@ubv.edu.ve 	CARACAS 	0	COORDINADOR DE SEDE ASOCIADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
464	9742200	BLANCO FONTALVO, EDITH ENRIQUETA	Venezolano 	1968-02-15	\N	\N	Femenino 	Soltero 	\N	eeblanco@ubv.edu.ve 	CALLE 80 N 92-21.URB. ROTARIA 	1	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
564	10675536	QUIVERA, ZULAY	Venezolano 	1969-01-12	\N	\N	Femenino 	Soltero 	\N	zquivera@ubv.edu.ve 	EL MANZANILLO, CALLE 6A 	0	AGREGADO	DOCENTE	2004-10-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
465	9742201	BLANCO FONTALVO, LUZ BETTY	Venezolano 	1969-12-08	\N	\N	Femenino 	Soltero 	\N	lbblanco@ubv.edu.ve 	CALLE 80. N 92-21. URBA. LA ROTARIA. 	0	COORDINADOR DE SEDE ASISTENTE	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
466	9754440	OBERTO COLINA, ANA ANGELICA	Venezolano 	1968-07-31	\N	\N	Femenino 	Soltero 	\N	aoberto@ubv.edu.ve 	URBANIZACION SAN FELIPE.SECTOR 5 VEREDA 03. CASA 03. CASA N 12 	1	AGREGADO	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
467	9779845	LEON MENDEZ, NURY CLARET	Venezolano 	1970-02-23	\N	\N	Femenino 	Soltero 	\N	ncleon@ubv.edu.ve 	URB. SANTA MARIA SUR, CALLE PICO ESPEJO #70, QUINTA MAMAMIA 	1	ASISTENTE	DOCENTE	2007-05-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
468	9786474	SUÁREZ BRACHO, JAKELINE MARIA	Venezolano 	1971-01-26	\N	\N	Femenino 	Soltero 	\N	jmsuarez@ubv.edu.ve 	BARRIO EL MANZANILLO CALLE 15B # 25E -24 	2	AGREGADO	DOCENTE	2004-05-29	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
469	9790036	OLIVERO AGUILLON, RICHARD DENNYS	Venezolano 	1970-07-05	\N	\N	Masculino 	Soltero 	\N	 	CALLE 89-E N6-36 SECTOR SANTA LUCIA 	1	ASISTENTE	DOCENTE	2004-05-29	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
470	9792462	DIAZ PORTILLO, EDDIE FREDDY	Venezolano 	1970-11-01	\N	\N	Masculino 	Soltero 	\N	efdiazp@ubv.edu.ve 	CALLE TUCACAS CON NIQUITAO, SECTOR PUERTA MARAVEN CASA N S/N 	2	INSTRUCTOR	DOCENTE	2010-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
471	9796297	FERNÁNDEZ FERRER, IGLE J	Venezolano 	1970-11-16	\N	\N	Femenino 	Soltero 	\N	ifernandez@ubv.edu.ve 	SECTOR LA RINCONADA,BARRIO FE Y ALEGRIA AVENIDA 	1	ASISTENTE	DOCENTE	2004-10-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
472	9810791	CALDERA CALDERA, JOHNNY JAVIER	Venezolano 	1969-08-31	\N	\N	Masculino 	Casado 	\N	jcaldera@ubv.edu.ve 	CALLE NUEVA GRANADA N2-B CUJICANA PUNTO FIJO 	1	ASISTENTE	DOCENTE	2005-05-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
473	9854500	RIERA OCANTO, REINA CONZUELO	Venezolano 	1970-08-01	\N	\N	Femenino 	Casado 	\N	rriera@ubv.edu.ve 	URB. LAS TRINITARIAS, CALLE 6, N 288 	2	INSTRUCTOR	CONTRATADO	2016-01-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
474	9859894	CEDEÑO MILLAN , ALEXIS JOSÉ 	Venezolano 	1969-06-08	\N	\N	Masculino 	Soltero 	\N	acedeno@ubv.edu.ve 	CALLE PPAL SANTA CRUZ, CASA N2 TUCUPITA DELTA AMACURO 	2	INSTRUCTOR	CONTRATADO	2012-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
475	9864084	MARTÍNEZ SEQUEA, JOSÉ RAMÓN	Venezolano 	1969-11-26	\N	\N	Masculino 	Soltero 	\N	jrmartinezs@ubv.edu.ve 	PLAZA BOLIVAR SUCRE N8 	0	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
476	9867418	ABREU VALDEZ, EMILYS CAROLINA	Venezolano 	1971-05-04	\N	\N	Femenino 	Divorciado 	\N	eabreu@ubv.edu.ve 	URB. EZEQUIEL ZAMORA 3ERA TRANSVERSAL CASA N 108 TUCUPITA DELTA AMACURO 	1	COORDINADOR ACADEMICO ASISTENTE	DOCENTE	2007-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
477	9871629	TORREALBA, MERQUI CORINA	Venezolano 	1966-08-11	\N	\N	Femenino 	Soltero 	\N	mtorrealba@ubv.edu.ve 	ARAURE ESTADO PORTUQUESA 	2	INSTRUCTOR	DOCENTE	2007-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
478	9873724	RONDON DE BRAVO, MAYRA JOSEFINA	Venezolano 	1968-01-28	\N	\N	Femenino 	Casado 	\N	mjrondon@ubv.edu.ve 	URB SERAFIN CEDEÑO CALLE 5 CASA N31 	1	INSTRUCTOR	CONTRATADO	2015-02-02	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
479	9875046	GARCIA, RAMÓN ALIRIO	Venezolano 	1967-07-28	\N	\N	Masculino 	Soltero 	\N	ragarcia@ubv.edu.ve 	URB. EL TAMARINDO,SECTOR 1. CASA N 7.SAN FERNANDO ESTADO APURE 	3	ASISTENTE	DOCENTE	2006-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
480	9897732	LANDAETA BECERRA, NHAIYARA	Venezolano 	1967-11-04	\N	\N	Femenino 	Casado 	\N	nlandaeta@ubv.edu.ve 	URB. TIPURO II, CALLE 3, VEREDA 10, # 6 LOS CORTIJOS. 	1	ASISTENTE	DOCENTE	2006-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
481	9929782	ROMERO DE GARCIA, YASMIRA  JOSEFINA	Venezolano 	1968-03-18	\N	\N	Femenino 	Casado 	\N	yromero@ubv.edu.ve 	URB. VILLA CARDON CALLR 03 CASA, N 167 	1	ASISTENTE	DOCENTE	2010-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
482	9935699	CEDEÑO SANCHEZ, FRANKLIN ANTONIO	Venezolano 	1969-07-28	\N	\N	Masculino 	Casado 	\N	facedeno@ubv.edu.ve 	SECTOR AGUA SALADA RES VILLA LAUMA II CASA N1 	0	INSTRUCTOR	DOCENTE	2007-10-22	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
483	9936016	BOTTINI GRANADO, MARYORIE	Venezolano 	1972-01-29	\N	\N	Femenino 	Casado 	\N	mabottini@ubv.edu.ve 	RESIDENCIAS LAS MARIAS EDIF LUISA PISO 5 APTO 5-4 FINAL 	2	ASISTENTE	DOCENTE	2006-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
484	9953940	CONTRERAS, LUCILA TAMARA	Venezolano 	1967-06-23	\N	\N	Femenino 	Soltero 	\N	lcontreras@ubv.edu.ve 	URB. SAN ANTONIO PINTO SALIAS. BLOQUE 27. APTO A-1. P.B. 	0	ASOCIADO	DOCENTE	2004-06-28	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
485	9958139	MALDONADO SOTILLO, MARÍA ISABEL	Venezolano 	1970-05-22	\N	\N	Femenino 	Soltero 	\N	 	AV EL EJERCITO RES. PARQUE PARAISO TORRE SUR PISO 15 APTO. 15-C 	2	ASISTENTE	DOCENTE	2005-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
486	9977977	ALCALÁ MUNDARAY, MARÍA DEL CARMEN	Venezolano 	1968-04-15	\N	\N	Femenino 	Casado 	\N	mdcalcala@ubv.edu.ve 	URB JOSE TADEO MONAGAS CALLE M 1 19-A 	2	ASISTENTE	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
488	9990172	RUIZ BRICEÑO, XIOMARA COROMOTO	Venezolano 	1969-04-07	\N	\N	Femenino 	Soltero 	\N	xruiz@ubv.edu.ve 	URB. LINDA BARINAS, CALLEA-4 CASA N 80-B 	1	INSTRUCTOR	DOCENTE	2006-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
489	9993014	CARABALLO MARIN, JOAQUIN ASUNCION	Venezolano 	1968-05-02	\N	\N	Masculino 	Soltero 	\N	caraballo0268@hotmail.com 	URB TANAGUARENA PARROQUIA CARABALLEDA EDIF OROMAR PISO 1 APTO A 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
490	10017044	LARREAL ALMAZO, NEIDA DEL CARMEN	Venezolano 	1975-06-26	\N	\N	Femenino 	Soltero 	\N	ndlarreal@ubv.edu.ve 	BARRIO 4 DE ABRIL C/05 N/C 173 DETRAS DEL SAMBIL 	3	ASISTENTE	DOCENTE	2005-04-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
491	10038775	MESA DE ALCALA, SONIA JACKELINE	Venezolano 	1968-08-03	\N	\N	Femenino 	Casado 	\N	smesa@ubv.edu.ve 	URB LA RAIZA SIMON RODRIGUEZ # 38 	2	COORDINADOR PFG ASISTENTE	DOCENTE	2006-07-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
492	10039050	CONTRERAS GOMEZ, CLAUDIA LILIANA	Venezolano 	1970-09-15	\N	\N	Femenino 	Divorciado 	\N	clcontreras@ubv.edu.ve 	PRADOS DEL SUR CASA 9 DE TRAS DEL GRUPO ESCOLAR ALIANZA LA CONCORDIA 	3	INSTRUCTOR	DOCENTE	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
493	10040844	NAVAS DE ORTIZ, OLGA VILMANIA	Venezolano 	1969-09-21	\N	\N	Femenino 	Soltero 	\N	onavas@ubv.edu.ve 	CALLE VALENCIA # 84. LA SABANITA. CD. BOLIVAR 	2	AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
494	10041285	MAÑEZ GARCIA, JOEL RAFAEL	Venezolano 	1967-12-06	\N	\N	Masculino 	Soltero 	\N	yoelrafaelmnzgrc4@ubv.edu.ve 	AVENIDA ESPAÑA CASA N2 	0	INSTRUCTOR	CONTRATADO	2017-01-09	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
495	10041362	VACARO CAMORA, ARGENIS RAFAEL	Venezolano 	1968-10-02	\N	\N	Masculino 	Casado 	\N	avacaro@ubv.edu.ve 	CALLE ROMULO GALLEGOS  LA DEMOCRACIA NUMERO 6 	2	INSTRUCTOR	DOCENTE	2007-10-08	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
496	10049088	PINTO MARCANO, LUIS RAMON	Venezolano 	1970-01-09	\N	\N	Masculino 	Soltero 	\N	lpinto@ubv.edu.ve 	CALLE 8 CASA 6 URB. JERUSALEN 	0	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
497	10049843	PÉREZ, YOREL JESÚS	Venezolano 	1969-11-02	\N	\N	Masculino 	Casado 	\N	yperez@ubv.edu.ve 	CASCO HISTORICO CALLE CARABOBO CASA #73 	0	INSTRUCTOR	DOCENTE	2004-03-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1288	3190145	CAPOTE VERDE, ANA MARITZA	Venezolano 	1948-07-02	\N	\N	Femenino 	Soltero 	\N	mcapote@ubv.edu.ve 	CARACAS 	0	TITULAR	CONTRATADO	2006-05-02	DOCENTE MEDIO TIEMPO	ACTIVO
694	11782320	ZACARIAS MORENO, ALVIS ALINA	Venezolano 	1974-03-16	\N	\N	Femenino 	Divorciado 	\N	 	SECTOR BRISAS DEL LIBERTADOR CALLE PRINCIPAL 9 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
498	10059412	PERALTA SANCHEZ, VÍCTOR JOSÉ	Venezolano 	1970-06-17	\N	\N	Masculino 	Soltero 	\N	vperalta@ubv.edu.ve 	CARRERA 2 ENTRE CALLES 21 Y 22 SECTOR LA PEÑITA 	2	COORDINADOR PFG AGREGADO	DOCENTE	2006-03-27	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
499	10065835	MARCANO BRIZUELA, EDGAR ANTONIO	Venezolano 	1971-04-09	\N	\N	Masculino 	Soltero 	\N	eamarcano@ubv.edu.ve 	AV. VICTORIA CALLE CATALUÑA EDIF. ANA ROSA PISO 5 APTO 23 URB LAS ACACIAS 	0	AGREGADO	DOCENTE	2004-07-21	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
500	10065921	AVILA GUERRA, YUSMERY DEL CARMEN	Venezolano 	1971-05-08	\N	\N	Femenino 	Soltero 	\N	 	URBANIZACION COTOPERIZ SECTOR LAS AMERICAS CASA 57 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
501	10082917	CEDEÑO HIGUERA, JOHAN M	Venezolano 	1969-11-10	\N	\N	Femenino 	Soltero 	\N	jcedeno@ubv.edu.ve 	SECTOR CORITO, CALLE SANTA ANA # 152 - A1 EDO. ZULIA 	0	AGREGADO	DOCENTE	2004-10-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
502	10095925	GUANCHEZ MILLAN, VERONICA MARLEN	Venezolano 	1968-12-19	\N	\N	Femenino 	Soltero 	\N	vguanchez@ubv.edu.ve 	BARRIO LA COROMOTO, AV. 101, CASA 22-A 	0	ASISTENTE	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
503	10102600	AVENDAÑO ARANGUREN, RAMÓN	Venezolano 	1970-08-20	\N	\N	Masculino 	Soltero 	\N	ravedano@ubv.edu.ve 	URB. LAS ADJUNTAS SECTOR COLONIAL MANZANA 7,CASA B10 	1	ASISTENTE	DOCENTE	2005-05-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
504	10105069	PEÑA  RANGEL, MIRIAM COROMOTO	Venezolano 	1968-10-12	\N	\N	Femenino 	Soltero 	\N	mcpena@ubv.edu.ve 	ZUMBA LA PARROQUIA AV PRINCIPAL 	2	ASISTENTE	DOCENTE	2008-02-28	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
505	10111802	GARCIA, JORGE	Venezolano 	1970-02-27	\N	\N	Masculino 	Soltero 	\N	garciaj@ubv.edu.ve 	URB.PROPATIA,SEGUNDA CALLE C/C TERCERA AVD,CASA SANTA,N#22,CATIA,CARACAS. 	0	ASISTENTE	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
506	10117933	GOMEZ VEGA, NADIA ELENA	Venezolano 	1969-11-15	\N	\N	Femenino 	Casado 	\N	negomezv@ubv.edu.ve 	URB. NUEVA CASARAPA SECTOR LOS PORTALES 1 CALLE A-1 	2	INSTRUCTOR	CONTRATADO	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
507	10119184	LINARES RODRIGUEZ, ANA ROSARIO	Venezolano 	1970-06-13	\N	\N	Femenino 	Soltero 	\N	alinares@ubv.edu.ve 	CALLE, MONTAÑA ALTA, EDF. 11, PISO 5 APTO. 5-5 CARRIZAL, LOS TEQUES 	0	ASISTENTE	DOCENTE	2006-07-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
508	10145404	VARGAS ROSALES, MARY  LUZ	Venezolano 	1967-10-05	\N	\N	Femenino 	Soltero 	\N	mlvargasr@ubv.edu.ve 	CARACAS 	3	ASISTENTE	DOCENTE	2010-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
509	10171422	MOLINA PASTRAN, YOSMAR LARITZA	Venezolano 	1970-10-07	\N	\N	Femenino 	Soltero 	\N	molinayosmar@hotmail.com 	RESIDENCIA EL CONDE PISO 19 APTO 19-C 	0	INSTRUCTOR	CONTRATADO	2018-10-03	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
510	10174782	NUÑEZ RUIZ, CARLA ELIANA	Venezolano 	1972-04-24	\N	\N	Femenino 	Divorciado 	\N	cnunez@ubv.edu.ve 	CALLE 10 N 6-50 URB. MONS. BRICEÑO TARIBA 	1	INSTRUCTOR	DOCENTE	2013-05-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
511	10192927	LAYA LARA, ÁNGEL ESTEBAN	Venezolano 	1971-01-04	\N	\N	Masculino 	Soltero 	\N	aelaya@ubv.edu.ve 	URB. LLOS NARANJOS, ZONA 2, CASA B-15, GUARENAS, EDO. MIRANDA 	1	AGREGADO	DOCENTE	2004-10-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
512	10208662	CHACIN GAMARDO, LIZKEL DEL CARMEN	Venezolano 	1970-06-09	\N	\N	Femenino 	Soltero 	\N	lchacin@ubv.edu.ve 	AV. SUCRE. EDIF. SUCRE. APTO: 14-H 	0	ASISTENTE	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
513	10242549	PEREZ DE VARGAS, NELIDA MAYELA	Venezolano 	1970-06-16	\N	\N	Femenino 	Soltero 	\N	nmperez@ubv.edu.ve 	SECTOR TIERRA NEGRA AV 11 CON CALLE 71 N 70-76 	2	INSTRUCTOR	DOCENTE	2008-03-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
514	10264288	BRICEÑO BRICEÑO, HECTOR RAFAEL	Venezolano 	1974-01-27	\N	\N	Masculino 	Soltero 	\N	hbriceno@ubv.edu.ve 	Urbanización Base Sucre, Calle 1casa #9 ? C 	3	INSTRUCTOR	DOCENTE	2010-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
515	10285799	OLIVEROS GOMEZ, OSWALDO JOSÉ	Venezolano 	1968-11-29	\N	\N	Masculino 	Divorciado 	\N	ojoliveros@ubv.edu.ve 	CARRERA N 5 CASA N 41 SECTOR EL SILENCIO 	4	AGREGADO	DOCENTE	2005-03-28	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
516	10300771	ORDOSGOITTE NOGUERA, EDGAR JESUS	Venezolano 	1967-02-22	\N	\N	Masculino 	Soltero 	\N	eordosgoitte@ubv.edu.ve 	CARRERA 3 NRO 63 LA MURALLA 	2	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
517	10301120	RIVAS RODRIGUEZ, VÍCTOR JULIO	Venezolano 	1970-02-01	\N	\N	Masculino 	Soltero 	\N	vrivas@ubv.edu.ve 	VEREDA 55 CASA N 15 SECTOR LOS GODOS 	2	AGREGADO	DOCENTE	2005-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
518	10335193	TORO MORENO, JOHIRA BELENMIR	Venezolano 	1970-12-29	\N	\N	Femenino 	Soltero 	\N	jbtoro@ubv.edu.ve 	URB. LA FUNDACION, CALLE HUMBOLDT, CASA NO 10-20 	0	AGREGADO	DOCENTE	2004-04-19	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
519	10335609	MORO ALVARADO, ANGEL ERNESTO	Venezolano 	1969-02-07	\N	\N	Masculino 	Soltero 	\N	aemoro@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2010-05-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
520	10337083	PIÑA MATA, HILDA ELENA	Venezolano 	1971-02-28	\N	\N	Femenino 	Soltero 	\N	hpina@ubv.edu.ve 	CALLE LAZALETAM CASA #2. NUEVO PRADO 	2	AGREGADO	DOCENTE	2005-11-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
521	10337759	SILVA ARISTEGUIETA, KAREN CECILIA	Venezolano 	1972-12-05	\N	\N	Femenino 	Soltero 	\N	ksilva@ubv.edu.ve 	URB CAURIMARE CALLE  PRINCIPAL EDIF KAMAR 	0	COORDINADOR DE SEDE ASISTENTE	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
522	10348630	RODRÍGUEZ HERRERA, ANA ISABEL	Venezolano 	1968-09-29	\N	\N	Femenino 	Soltero 	\N	airodriguezh@ubv.edu.ve 	AV. INTERCOMUNAL LOS JARDINES DEL VALLE 	1	INSTRUCTOR	DOCENTE	2006-11-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
523	10369856	PORTILLO JAIMES, MORELVA LILIBETH	Venezolano 	1970-07-07	\N	\N	Femenino 	Soltero 	\N	mportillo@ubv.edu.ve 	AV. 83 DON MANUEL BELLOSO VIA AEROPUERTO ALTOS DE MARACAYBO  CASA# 99V 	0	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
524	10375683	CABRERA GONZALEZ, YULISSA EMPERATRIZ	Venezolano 	1971-03-11	\N	\N	Femenino 	Soltero 	\N	ycabrera@ubv.edu.ve 	AV.PRINCIPAL NORTE SUR ENTRE CALLE LAS MARGARITAS Y JUNCAL CASA N 1 LAS MARITAS 	0	AUXILIAR I	CONTRATADO	2016-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
525	10382196	JIMENEZ RODRIGUEZ, CAROLINA MARIA	Venezolano 	1970-04-29	\N	\N	Femenino 	Soltero 	\N	caroljim821966?gmail.com 	URB SANTA ROSA CALLE 1 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
526	10397004	VALECILLOS MEDINA, ELDANA CORO	Venezolano 	1971-02-07	\N	\N	Femenino 	Soltero 	\N	evalecillos@ubv.edu.ve 	AV. LOS ROBLES EL HATILLO 	3	ASISTENTE	DOCENTE	2005-03-28	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
527	10399086	VIERA SANTIAGO, LILIANA COROMOTO	Venezolano 	1971-05-02	\N	\N	Femenino 	Casado 	\N	lviera@ubv.edu.ve 	UNARE II, BLOQUE 16, PISO 2 APTO 2-11, PTO. ORDAZ EDO. BOLIVAR. 	1	ASISTENTE	DOCENTE	2004-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
528	10405136	LEON IGLESIA, DEINYS MARIA	Venezolano 	1972-04-14	\N	\N	Femenino 	Soltero 	\N	dleon@ubv.edu.ve 	BARRIO LA RINCONADA, SECTOR FE Y ALEGRIA. CALLE 2 CASA 1F-39 MARACAIBO. 	0	ASOCIADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
529	10406412	NAJUL BRACHO, SAMAEL	Venezolano 	1971-04-11	\N	\N	Masculino 	Soltero 	\N	snajul@ubv.edu.ve 	AV. 84 N 67-100 SECTOR PANAMERICANO 	0	AGREGADO	DOCENTE	2004-04-02	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
530	10406683	LEON BUITRAGO, JOSÉ JAVIER	Venezolano 	1971-07-10	\N	\N	Masculino 	Soltero 	\N	jjleon@ubv.edu.ve 	AV.MILAGRO NORTE RSIDENCIA ISLA DORADA EDIFICIO CONSTANZA APARTAMENTO PLANTA BAJA A 	2	ASOCIADO	DOCENTE	2004-05-29	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
531	10415081	DE ANDRADE CARRILLO, ERLEEM	Venezolano 	1972-05-18	\N	\N	Femenino 	Soltero 	\N	edeandrade@ubv.edu.ve 	URB. ALTAMIRA, CALLE 92, N 78-144 SECTOR CLUB HIPICO MARACAIBO. 	2	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
532	10418442	PÉREZ SOSA, LISSETTE DEL CARMEN	Venezolano 	1971-07-10	\N	\N	Femenino 	Soltero 	\N	ldperez@ubv.edu.ve 	BARRIO 5 DE JULIO PARCELAMIENTO SANTA INES CALLE 43K CASA 98-91 	3	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
533	10423892	ARAQUE, PABLO EMIGDIO	Venezolano 	1971-10-24	\N	\N	Masculino 	Soltero 	\N	paraque@ubv.edu.ve 	URB. CUATRICENTENARIO, 2DA ETAPA, CALLE 61 VEREDA 52 CALLE 06 	0	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
534	10430422	PARRA MONTES DE OCA, LENIN IVANOCK	Venezolano 	1972-12-03	\N	\N	Masculino 	Soltero 	\N	lparram@ubv.edu.ve 	AV GUAJIRA, RES  EL CUJI NUCLE 3 , EDIF 6APTO 2B 	0	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
535	10442526	URBINA PEÑA, ENRIQUE JOSÉ	Venezolano 	1974-03-22	\N	\N	Masculino 	Soltero 	\N	eurbina@ubv.edu.ve 	18 DE OCTUBRE, CALLE I, NUMERO 4-65 MARACAIBO - ESTADO ZULIA 	3	ASISTENTE	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
536	10445640	ÁVILA MAVAREZ, GIOVANNY	Venezolano 	1970-09-15	\N	\N	Masculino 	Soltero 	\N	gavila@ubv.edu.ve 	CALLE 70A NUMERO 28A-314 SECTOR SANTA MARIA, 	2	AGREGADO	DOCENTE	2004-10-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
537	10446781	GARCIA MENDEZ, CECILIA ANTONIA	Venezolano 	1968-04-26	\N	\N	Femenino 	Soltero 	\N	cagarcia@ubv.edu.ve 	SECTOR CLUB HIPICO AV. 73A CON CALLE 94 B, NUMERO 94 - 93 	1	ASISTENTE	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
538	10451974	NAVA, CORIBELL JOSEFINA	Venezolano 	1968-10-13	\N	\N	Femenino 	Soltero 	\N	cnava@ubv.edu.ve 	FRANCISCO DE MIRANDA CALLE # 81 EDO. ZULIA 	1	ASISTENTE	DOCENTE	2004-03-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
539	10457879	CARRIZALES NIEVES, JENIFFE  JAMILET	Venezolano 	1970-05-17	\N	\N	Femenino 	Soltero 	\N	jcarrizales@ubv.edu.ve 	CALLE VALENCIA NRO 12 LA CANDELARIA,EL LIMON 	0	ASISTENTE	DOCENTE	2004-10-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
540	10462171	PEREDA SALAZAR, PEDRO ELISEO	Venezolano 	1967-03-07	\N	\N	Masculino 	Soltero 	\N	ppereda@ubv.edu.ve 	URB. EL DIQUE, CUARTA CALLE, CASA N 9-A 	0	INSTRUCTOR	CONTRATADO	2015-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
541	10469559	FIGUEROA HENRIQUEZ, YSABEL CRISTINA	Venezolano 	1969-09-26	\N	\N	Femenino 	Soltero 	\N	yfigueroa@ubv.edu.ve 	URB. BEBEDERO DA. 46 N 8 	1	INSTRUCTOR	DOCENTE	2011-05-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
542	10478390	SANCHEZ GUTIERREZ, ELISANDER	Venezolano 	1970-11-07	\N	\N	Masculino 	Soltero 	\N	elsanchez@ubv.edu.ve 	URBANIZACION SA JACINTO AV, 20 SECTOR # 07 CASA 3 07 	3	AGREGADO	DOCENTE	2005-02-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
543	10479892	HERNÁNDEZ PEREZ, ROMULO ADRIAN	Venezolano 	1969-03-03	\N	\N	Masculino 	Soltero 	\N	rhernandezp@ubv.edu.ve 	AV. JOSEFA CAMEJO QTA, MONICA COROP- FALCON 	3	ASISTENTE	DOCENTE	2007-07-23	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
544	10480469	SANCHEZ MERCADO, MIGUEL ANGEL	Venezolano 	1971-02-25	\N	\N	Masculino 	Soltero 	\N	msanchez@ubv.edu.ve 	URBANIZACION GUARITOS II VEREDA 19 CASA N 19 MATURIN 	1	AGREGADO	DOCENTE	2004-04-24	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
545	10491545	JIMÉNEZ JIMENEZ, LUIS NAPOLEON	Venezolano 	1970-11-07	\N	\N	Masculino 	Soltero 	\N	ljimenez@ubv.edu.ve 	CALLE BELLA VISTA C/C AV ESPAÑA RES. BELLA VISTA CASA N 33 	1	ASISTENTE	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
546	10501564	GONZALEZ GUTIERREZ, MAYANIN JOSEFINA	Venezolano 	1969-11-22	\N	\N	Femenino 	Soltero 	\N	mjgonzalez@ubv.edu.ve 	MONTE PIEDAD BLOQ 5-B APTO 31-C PISO 3, 23 ENERO 	0	ASOCIADO	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
547	10534700	JOSE GREGORIO, CABRERA	Venezolano 	1967-06-12	\N	\N	Masculino 	Soltero 	\N	jgcabrera@ubv.edu.ve 	AUTOPISTA CARACAS LA GUAIRA, URB EL LIMON KILOMETRO 5 CASAN 103 	0	INSTRUCTOR	CONTRATADO	2017-06-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
548	10542796	DÁVILA, MARÍA E	Venezolano 	1972-07-15	\N	\N	Femenino 	Soltero 	\N	mdavila@ubv.edu.ve 	AV. INTERCOMUNAL DE ANTIMANO, CALLE ICOA CASA NUMERO 12 	0	ASISTENTE	DOCENTE	2004-10-19	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
549	10545286	DÍAZ MARTINEZ, TOMAS EDUARDO	Venezolano 	1971-04-11	\N	\N	Masculino 	Soltero 	\N	tediaz@ubv.edu.ve 	PARROQUIA SUCRE, SECTOR ALTA VISTA EDIF. PRIMAVERA, PB. APTO #5 CAERACAS 	0	INSTRUCTOR	DOCENTE	2006-05-22	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
550	10545499	RODRIGUEZ  ROJAS, EFREN ENRIQUE	Venezolano 	1969-06-18	\N	\N	Masculino 	Soltero 	\N	eerodriguez@ubv.edu.ve 	CONJ. RESIDENCIAL VILLA TOSCANA. EDIF. SIENA. PISO 2. APTO 2-1, CALLE PRINCIPAL EL RINCON 	0	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
551	10569115	BUCARITO BERMUDEZ, LUIS EDUARDO	Venezolano 	1970-10-13	\N	\N	Masculino 	Casado 	\N	lbucarito@ubv.edu.ve 	LOS PRADOS I CALLE LOS PINOS CASA N 68 SECTOR 	2	ASISTENTE	DOCENTE	2004-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
552	10569186	VARGAS, FELIX MANUEL	Venezolano 	1971-06-27	\N	\N	Masculino 	Casado 	\N	 	BARRIO A JURO CALLE RAUL LEONI CASA N 17 	2	INSTRUCTOR	CONTRATADO	2019-06-04	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
553	10570295	JIMÉNEZ BARRIOS, INGRID JOSEFINA	Venezolano 	1970-05-04	\N	\N	Femenino 	Soltero 	\N	ijimenez@ubv.edu.ve 	CALLE LAS MERCEDES CASA NUM. 20, PARROQUIA LA SABANITA, CIUDAD BOLIVAR 	0	ASISTENTE	DOCENTE	2007-04-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
554	10571072	MEDINA CEDEÑO, MILDRED JOSEFINA	Venezolano 	1971-10-24	\N	\N	Femenino 	Casado 	\N	mmedina@ubv.edu.ve 	URB. CAURA I MANZANA 6 CASA N3 	3	AGREGADO	DOCENTE	2004-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
555	10571276	CHÁVEZ FONERINO, SUSI TADIA	Venezolano 	1970-12-05	\N	\N	Femenino 	Divorciado 	\N	schavez@ubv.edu.ve 	FUND.MENDOZA CALLE N2 CASA D-1 	1	INSTRUCTOR	DOCENTE	2005-10-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
556	10571472	BUCARITO BERMUDEZ, MARÍA ELENA	Venezolano 	1972-03-23	\N	\N	Femenino 	Soltero 	\N	mbucarito@ubv.edu.ve 	URBANIZACION VISTA HERMOSA CALLE CIRCUNVALACION CASAN5 	0	AGREGADO	DOCENTE	2004-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
557	10581830	TORRES, NORIS	Venezolano 	1971-05-25	\N	\N	Femenino 	Soltero 	\N	ntorres@ubv.edu.ve 	CALLE PRINCIPAL,CARALLACA EDO VARGAS 	1	ASISTENTE	DOCENTE	2004-10-14	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
558	10604907	RIERA PEREZ, HILDRED MACARENA	Venezolano 	1971-09-15	\N	\N	Femenino 	Soltero 	\N	hriera@ubv.edu.ve 	URB. VILLA RITA, CALLE 5. 	2	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
559	10612074	GARCES COLINA, HUMBERTO RANGEL	Venezolano 	1969-09-03	\N	\N	Masculino 	Soltero 	\N	hgarces@ubv.edu.ve 	CALLE AYACUCHO ENTRE PANAMA Y PERU, N 12-99 	2	INSTRUCTOR	DOCENTE	2007-06-04	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
560	10614321	RUIZ, NICOLASA DEL CARMEN 	Venezolano 	1971-08-26	\N	\N	Femenino 	Soltero 	\N	nruiz@ubv.edu.ve 	CALLE ARISMENDI CASA N30-264 PUNTO FIJO 	1	INSTRUCTOR	CONTRATADO	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
561	10662221	CARAMO CARMONA, ROSSANA JESUS	Venezolano 	1973-06-05	\N	\N	Femenino 	Soltero 	\N	rcaramo@ubv.edu.ve 	BARRIO SIMON BOLIVAR CALLE SUCRE CASA N 32 	1	AGREGADO	DOCENTE	2004-09-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
562	10667299	QUEVEDO BRICEÑO, WILLIAM JOSE	Venezolano 	1971-11-12	\N	\N	Masculino 	Soltero 	\N	wquevedo@ubv.edu.ve 	CARACAS 	1	INSTRUCTOR	DOCENTE	2010-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
563	10671965	TOLEDO LIENDO, ANTONIO JOSE	Venezolano 	1972-11-29	\N	\N	Masculino 	Soltero 	\N	ajtoledo@ubv.edu.ve 	URB. ROMULO GALLEGOS. SECTOR 1 AV 1- CALLE #3, CASA #53 	0	INSTRUCTOR	DOCENTE	2009-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
565	10693398	GASCÓN GARCIA, SORANGEL	Venezolano 	1971-10-28	\N	\N	Femenino 	Soltero 	\N	sgascon@ubv.edu.ve 	URB. VILLA DEL ESTE CALLE K CASA N 13 MUNICIPIO ZAMORA 	0	ASOCIADO	DOCENTE	2004-10-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
566	10700865	ABREU ARAQUE, ZORAIDA BEATRIZ	Venezolano 	1970-02-09	\N	\N	Femenino 	Soltero 	\N	zbabreu@ubv.edu.ve 	URB. PEDRO MANUEL ARCAYA, 2DA ETAPA, MANZANAC-11 	1	ASISTENTE	DOCENTE	2005-10-17	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
567	10707045	MORENO CHIRINO, LUIS ALBERTO	Venezolano 	1968-09-03	\N	\N	Masculino 	Soltero 	\N	lamoreno@ubv.edu.ve 	SECTOR SAN BASILIO CALLE CARONI CASA N 71. OCUMRE DEL TUY 	1	AUXILIAR I	CONTRATADO	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
568	10707298	OLLARVES COLINA, PEDRO JOSÉ	Venezolano 	1968-04-15	\N	\N	Masculino 	Casado 	\N	pollarves@ubv.edu.ve 	ZAMBRANO CALLE PRINCIPALSECTOR COROMOTO CASA S/N 	1	INSTRUCTOR	DOCENTE	2007-03-21	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
569	10719468	MORA DUGARTE, MAIRA ELENA	Venezolano 	1971-04-04	\N	\N	Femenino 	Soltero 	\N	memora@ubv.edu.ve 	URB. VALLE REAL, SECTOR TIPIRO II, CASA R-9 	1	AGREGADO	DOCENTE	2006-06-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
570	10730313	RODRÍGUEZ, LOURDES DEL VALLE	Venezolano 	1971-01-29	\N	\N	Femenino 	Soltero 	\N	lvrodriguez@ubv.edu.ve 	URB. LA ESMERALDA, 2DA, ETAPA, MANZANA F10-11. SAN DIEGO. EDO, CARABOBO 	2	ASOCIADO	DOCENTE	2004-06-21	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
571	10747542	MACHADO MOLINA, JOSEFA YAJAIRA	Venezolano 	1972-08-06	\N	\N	Femenino 	Soltero 	\N	jmachado@ubv.edu.ve 	CARACAS 	1	INSTRUCTOR	DOCENTE	2010-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
572	10759106	SALAZAR TORO, PEDRO MANUEL	Venezolano 	1971-11-08	\N	\N	Masculino 	Soltero 	\N	PEDROSALAZAR250@ROCKETMAIL.COM 	BARRIO SAN RAFAEL CALLE ROMULO GALLEGOS NRO 22 	0	INSTRUCTOR	CONTRATADO	2016-01-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
573	10788571	GIMENEZ GUARIGUATA, MARIBEL JOSÉ	Venezolano 	1971-10-03	\N	\N	Femenino 	Soltero 	\N	mgimenez@ubv.edu.ve 	LAS MARGARITAS SECTOR I CASA 4 	2	ASOCIADO	DOCENTE	2005-05-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
574	10794479	MANZO AGOSTINI, LEONARDO ANDRES	Venezolano 	1972-11-25	\N	\N	Masculino 	Soltero 	\N	lmanzo@ubv.edu.ve 	CARACAS 	2	ASISTENTE	DOCENTE	2006-11-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
575	10799544	SEGOVIA HERNANDEZ, ALEJANDRA YELITZA	Venezolano 	1973-06-02	\N	\N	Femenino 	Soltero 	\N	aysegovia@ubv.edu.ve 	LA MONTAÑA AV PAEZ RES LAS AVES PISO 13-A 	0	AGREGADO	DOCENTE	2005-03-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
576	10826633	OROPEZA MIRANDA, HÉCTOR EMILIO	Venezolano 	1974-10-17	\N	\N	Masculino 	Soltero 	\N	horopeza@ubv.edu.ve 	AV.FUERZAS ARMADAS,ESQ.SAN LUIS A PANORAMA SAN JOSE CARACAS 	0	ASISTENTE	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
577	10838756	LANDAETA BECERRA, JENNIFER COROMOTO	Venezolano 	1971-02-02	\N	\N	Femenino 	Casado 	\N	jlandaeta@ubv.edu.ve 	VEREDA 10 N 6 LOS CORTIJOS 	0	INSTRUCTOR	DOCENTE	2011-01-17	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
578	10847782	GUERRERO, MARÍA	Venezolano 	1972-05-10	\N	\N	Femenino 	Soltero 	\N	mguerrero@ubv.edu.ve 	URB. SAN MIGUEL, AV. 60 N# 96G-77 	1	AGREGADO	DOCENTE	2004-10-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
579	10866303	TORRES MARTÍNEZ, ALEXANDER ARMANDO	Venezolano 	1972-03-17	\N	\N	Masculino 	Soltero 	\N	atorres@ubv.edu.ve 	AV INTERCOMUNAL CALLEJON EL LORO CASA N12-1 	0	INSTRUCTOR	DOCENTE	2008-01-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
580	10867711	TAMAYO ALVAREZ, ALFREDO JOSÉ	Venezolano 	1970-11-09	\N	\N	Masculino 	Soltero 	\N	ajtamayo@ubv.edu.ve 	CABOTAJE CALLE MIQUILEN EDIF ANTONIO HANNA 	0	AGREGADO	DOCENTE	2007-04-21	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
581	10870893	ROJAS MARIN, YINETTE SORAYA	Venezolano 	1973-02-28	\N	\N	Femenino 	Soltero 	\N	yrojas@ubv.edu.ve 	AV. NORTE 20, ENTRE ESQUINAS DE FLORES A SAN CARLOS, #7 LA PASTORA. 	0	AUXILIAR III	DOCENTE	2004-04-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
582	10871500	RODRÍGUEZ ALGARIN, SIUL GERMAN	Venezolano 	1971-12-06	\N	\N	Masculino 	Soltero 	\N	sgrodriguez@ubv.edu.ve 	CALE EL CONVENTO,  EDIF. ILVA, APTO. 5, VALLE ABAJO. 	2	ASISTENTE	DOCENTE	2005-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
583	10879877	FARIAS TINEO, NEULYS EVANGELIA	Venezolano 	1969-09-21	\N	\N	Femenino 	Soltero 	\N	nfarias@ubv.edu.ve 	RESIDENCIAL ALTAGRACIA PISO 3 APTO 3-F LA PASTORA 	0	ASISTENTE	DOCENTE	2004-09-27	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
584	10882256	VERDE, DOUGLAS ERNESTO	Venezolano 	1970-11-19	\N	\N	Masculino 	Casado 	\N	dverde@ubv.edu.ve 	AVENIDA REPUBLICA EDIFICIO HERMANOS TORRI PISO N4 APTO N6 	1	ASISTENTE	DOCENTE	2005-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
585	10886786	BLANCO PEREZ, EUGENIO ALBERTO	Venezolano 	1969-01-16	\N	\N	Masculino 	Casado 	\N	eablanco@ubv.edu.ve 	URB.LOMAS DE GUADALUPE, CASA N 117, PILONCITO 	2	INSTRUCTOR	CONTRATADO	2016-06-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
586	10914358	MEDINA DE MENTADO, CRUZ MARINA	Venezolano 	1971-02-04	\N	\N	Femenino 	Casado 	\N	cmedina@ubv.edu.ve 	URB. ALTO DDE CARUNO NRO G-6 SECTOR TITUPO 	4	ASISTENTE	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
587	10923454	HERRERA MEDINA, YUDRIS GREGORIA	Venezolano 	1973-02-07	\N	\N	Femenino 	Soltero 	\N	yherrera@ubv.edu.ve 	CALLE 89 D CON AV. 10 # 10-3 SAECTOR BELLOSO 	1	AGREGADO	DOCENTE	2005-10-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
588	10930883	FERMÍN DE CAMPOS, ADELAIDA DEL VALLE	Venezolano 	1972-01-26	\N	\N	Femenino 	Soltero 	\N	afermin@ubv.edu.ve 	URB. LOMAS DEL VIENTO CONDOMINIO 5 CASA N 304 	1	ASISTENTE	DOCENTE	2006-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
589	10933116	GÓMEZ GONZALEZ, KAIRELY CAROLINA	Venezolano 	1971-11-04	\N	\N	Femenino 	Casado 	\N	kgomez@ubv.edu.ve 	URB GRAN SABANA MANZANA N 13 	1	ASISTENTE	DOCENTE	2005-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
590	10933408	HERNÁNDEZ OLIVARES, JOANNOLIS ANAYILA	Venezolano 	1971-09-09	\N	\N	Femenino 	Casado 	\N	jnhernandez@ubv.edu.ve 	URB. VILLA DE LA LAGUNA CALLE N 5 CASA N 175 SECTOR TIPURO 	1	AGREGADO	DOCENTE	2005-01-17	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
591	10942145	MEDINA ATOPO, GIOMAR ADRIANA	Venezolano 	1971-06-08	\N	\N	Femenino 	Soltero 	\N	gamedinaa@ubv.edu.ve 	URB SIERRA PARIMA MANZANA 75 CASA 17 PTO ORDAZ 	2	AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
592	10944842	VILLARROEL TORRES, NEYTIS MARIA	Venezolano 	1973-11-19	\N	\N	Femenino 	Soltero 	\N	nmvillarroel@ubv.edu.ve 	VEREDA 33 CASA N 7 GUARITOS 	1	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
593	10948899	LANZA, DANIEL JOSE	Venezolano 	1971-05-17	\N	\N	Masculino 	Casado 	\N	dalanza@ubv.edu.ve 	URB. ALTAMIRA CASA SIN NRO SAN ANTONIO DEL GOLFO MUN. MEJIA EDO SUCRE 	3	ASISTENTE	DOCENTE	2010-10-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
594	10949648	BENITEZ BAPTISTA, ROSA MILEDYS	Venezolano 	1971-12-10	\N	\N	Femenino 	Soltero 	\N	rmbenitez@ubv.edu.ve 	AV. SAN MARTIN, URB. LA QUEBRADITAI. CJON, AGROZA - EDIF. BLOQUE 12 PISO 13 APTO. 13-02 EL ARAISO. 	3	COORDINADOR ACADEMICO ASISTENTE	DOCENTE	2005-03-14	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
595	10965575	GUANIPA MOLINA, ALIX JOSÉ	Venezolano 	1970-01-30	\N	\N	Masculino 	Soltero 	\N	aguanipa@ubv.edu.ve 	CALLE LA ESCUELA NUM. 55, CON CALLE ARAGUANEY PROLONGACION BELLA VISTA 	1	ASISTENTE	DOCENTE	2007-07-02	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
596	10971014	BUSTILLOS, LISETH DEL CARMEN	Venezolano 	1971-01-24	\N	\N	Femenino 	Soltero 	\N	lbustillos@ubv.edu.ve 	CALLE PRICIPAL S/N GUANADITO NORTE 	1	INSTRUCTOR	DOCENTE	2005-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
597	10973631	OLIVERO SANCHEZ, CARLOS JULIO	Venezolano 	1974-01-31	\N	\N	Masculino 	Soltero 	\N	colivero@ubv.edu.ve 	AV. PRICIPAL BALLE VISTA, CASA N 2 	0	COORDINADOR DE SEDE ASISTENTE	DOCENTE	2004-09-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
598	10973913	ARENDS RODRIGUEZ, HENRY JOSE	Venezolano 	1971-03-16	\N	\N	Masculino 	Soltero 	\N	harends@ubv.edu.ve 	JORGE HERNANDEZ SECTOR 4 VEREDA 16 CASA N 32 	3	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
599	11010819	MONTILLA MATA, RAFAEL JESUS	Venezolano 	1972-10-11	\N	\N	Masculino 	Soltero 	\N	rmontilla@ubv.edu.ve 	LOS CERRITOS, CALLE GUEVARA Y LIRA 45 	1	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
600	11012596	VERA, CARMEN CAROLINA 	Venezolano 	1973-12-31	\N	\N	Femenino 	Soltero 	\N	cvera@ubv.edu.ve 	EL SILENCIO DE CAMPO ALEGRE CARRERA N 4 CASA N 134 MATURIN ESTADO MONAGAS 	3	INSTRUCTOR	DOCENTE	2010-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
601	11025556	BRICENO YASELLI, KATIA	Venezolano 	1971-01-17	\N	\N	Femenino 	Soltero 	\N	kbriceno@ubv.edu.ve 	URB. LOS SAMANES,CALLE COLEGIO,RES. EL NARANJAL,PISO18,APTO 185,TORRE A,CARACAS 	2	VICERRECTOR	ALTO NIVEL	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
602	11049012	DABOIN ROMERO, LIDUBINA RAMONA	Venezolano 	1973-03-15	\N	\N	Femenino 	Soltero 	\N	lrdaboin@ubv.edu.ve 	URB. DELGADO CHALBAUD CALLE LOS JABILLOS EDIFICIO SOMBRITA 	2	AGREGADO	DOCENTE	2004-10-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
603	11055619	CASTILLO SANTANA, INGRID BEATRIZ	Venezolano 	1971-09-02	\N	\N	Femenino 	Soltero 	\N	icastillo@ubv.edu.ve 	CALLE SUCRE # 37, PARROQUIA CARLOS SOUBLETTE ALCABALA VIEJA, MONTESANO EDO. VARGAS 	0	ASISTENTE	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
604	11062752	VELAZCO RUJANO, SOLEY	Venezolano 	1973-09-26	\N	\N	Femenino 	Soltero 	\N	svelasco@ubv.edu.ve 	URBANIZACION LA ROSALEDA CALLE 12 CASA N D-070 	2	INSTRUCTOR	DOCENTE	2007-05-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
605	11071380	CORAO PEREZ, MARÍA FABIANA	Venezolano 	1972-01-20	\N	\N	Femenino 	Soltero 	\N	mfcorao@ubv.edu.ve 	KM. 25 ALTOS DE IZACARAGUA, CASA N 14 EL JUNQUITO 	1	INSTRUCTOR	DOCENTE	2005-12-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
606	11072042	VILLARROEL MORENO, ANABEL	Venezolano 	1972-12-20	\N	\N	Femenino 	Soltero 	\N	avillarroel@ubv.edu.ve 	SECTOR LEONCIO MARTINEZ CALLE FATIMA 	1	AGREGADO	DOCENTE	2005-03-14	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
607	11088697	GONZALEZ GUERRA, YURIGSA IRAIDY	Venezolano 	1972-10-11	\N	\N	Femenino 	Soltero 	\N	yigonzalez@ubv.edu.ve 	GRANJA VICTORIA, VIA EL CASTRERO, 50 MTS ANTES DEL AREA DE CIENCIAS ECONOMICAS DE LA UNIVERSIDAD 	3	ASISTENTE	DOCENTE	2006-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
608	11141021	PIMENTEL AMAYA, ROSA MIGUELINA	Venezolano 	1972-01-05	\N	\N	Femenino 	Casado 	\N	rpimentel@ubv.edu.ve 	URB. LAS EUGENIAS, 5 ETAPA E 10-15 	3	AGREGADO	DOCENTE	2005-10-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
609	11141842	PIREZ GRATEROL, FRANCISCO RAFAEL	Venezolano 	1973-04-02	\N	\N	Masculino 	Soltero 	\N	fpirez@ubv.edu.ve 	CALLE RAFAEL GONZALEZ, SECTOR SAN JOSE, DIAGONAL AL HIPERLAU. CASA N# 14. FALCON - CORO 	3	AGREGADO	DOCENTE	2004-09-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
610	11145253	RODRÍGUEZ RODRÍGUEZ, ALDEMARO JESÚS	Venezolano 	1972-03-27	\N	\N	Masculino 	Casado 	\N	ajrodriguez@ubv.edu.ve 	AV MIRANDA PLAZA PERIODISTA CASA N 15-118-FRENTE REFRIGERACION INSULAR 	3	INSTRUCTOR	DOCENTE	2008-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
611	11161742	HERNÁNDEZ NARANJO, MAYELA INES	Venezolano 	1973-05-31	\N	\N	Femenino 	Soltero 	\N	mihernandez@ubv.edu.ve 	AV.SAN MARTIN ESQUINA PALO GRANDE A ALCABALA EDIFICIO ALCAPAL 	1	ASISTENTE	DOCENTE	2004-04-19	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
612	11163430	BORGES, HUMBERTO	Venezolano 	1971-11-18	\N	\N	Masculino 	Soltero 	\N	hborges@ubv.edu.ve 	COLINAS DE CARRIZAL,URB. MONTAÑA ALTA EDIF.10, APTO 16-4 	1	ASISTENTE	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
613	11166957	RUIZ SUAREZ, XIOMAY	Venezolano 	1973-07-30	\N	\N	Femenino 	Soltero 	\N	xruizs@ubv.edu.ve 	CALLE COLOMBIA # 37 MIRAFLORES 	1	ASISTENTE	DOCENTE	2005-03-04	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
614	11168245	PRINCIPAL ALVILLAR, LILIA ZORAIDA	Venezolano 	1971-07-11	\N	\N	Femenino 	Soltero 	\N	liliazp@ubv.edu.ve 	CALLE PRINCIPAL CASA NRO 113-8 BARRIO BOLIBARIANA 2000 PARROQUIA LA SABANITA 	0	INSTRUCTOR	CONTRATADO	2017-07-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
615	11170122	TURAREN MAGRO, MARÍA CAROLINA	Venezolano 	1972-04-15	\N	\N	Femenino 	Soltero 	\N	mturaren@ubv.edu.ve 	URB VISTA HERMOSA CARRERA N 5 C/C 4 RES VILLA DORADA CASA N 4 	1	AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
616	11176263	FORTI VALLENILLA, YOJANA DE LAS NIEVES	Venezolano 	1972-10-11	\N	\N	Femenino 	Casado 	\N	yforti@ubv.edu.ve 	CALLE LA GALLERA N35 LAS MOXEAS 	0	AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
617	11176379	BLANCA GUTIÉRREZ, OGLAS MARINA	Venezolano 	1971-06-27	\N	\N	Femenino 	Soltero 	\N	oblanca@ubv.edu.ve 	URB. LOS COQUITOS. CALLE GRANZONAL. CASA 43. 	0	INSTRUCTOR	DOCENTE	2007-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
618	11181321	VILORIA PANTOJA, JOSÉ LUIS	Venezolano 	1973-01-16	\N	\N	Masculino 	Soltero 	\N	jlviloria@ubv.edu.ve 	SAN AGUSTIN DEL SUR AV. LEONARDO RUIZ PINEDA EDIFICIO VUELTA DEL CASQUILLO 	0	ASISTENTE	DOCENTE	2004-09-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
619	11186797	COVAULT RODRIGUEZ, NORA MERCEDES	Venezolano 	1971-04-04	\N	\N	Femenino 	Soltero 	\N	ncovault@ubv.edu.ve 	BARNIAS 	1	INSTRUCTOR	DOCENTE	2011-05-02	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
620	11190952	AGUILERA, ENRIQUE JOSÉ	Venezolano 	1972-04-25	\N	\N	Masculino 	Soltero 	\N	eaguilera@ubv.edu.ve 	SECTOR CUATRICENTINARIO AV. GUAICAIPURO BLOQUE 13 	2	ASISTENTE	DOCENTE	2006-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
621	11196439	GARCIA CISNEROS, CRISMAR DEL CARMEN	Venezolano 	1970-12-27	\N	\N	Femenino 	Soltero 	\N	 	URBANIZACION JOSE TADEO MONAGAS CALLE 22 CASA 64 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
622	11201042	CASTILLO CEDEÑO, LUIS FELIPE	Venezolano 	1973-12-02	\N	\N	Masculino 	Soltero 	\N	lfcedeno@ubv.edu.ve 	KENEDDY AV. PRINCIPAL EDIF.19-1 PLANTA BAJA APTO 00-01 	0	INSTRUCTOR	CONTRATADO	2016-06-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
623	11202887	JIMENEZ RODRIGUEZ, MARIA ALEJANDRA	Venezolano 	1973-09-17	\N	\N	Femenino 	Soltero 	\N	marialejandrajro25@gmail.com 	URB SANTA ROSA CALLE 1 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
624	11210922	JARAMILLO RODRIGUEZ, MAIQUEL COROMOTO	Venezolano 	1975-02-23	\N	\N	Femenino 	Soltero 	\N	mjaramillo@ubv.edu.ve 	SECTOR FE Y ALEGRIA, CALLE 4 LA MANGA, CASA N 15 	3	ASISTENTE	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
625	11211002	BENAVIDES FARIAS, MEISYS SANTA	Venezolano 	1973-04-22	\N	\N	Femenino 	Soltero 	\N	msbenavides@ubv.edu.ve 	URB. EL FARO CONDOMINIO AVES N 24 ZONA INDUSTRIAL VIA SAN JAIME 	1	ASISTENTE	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
626	11227188	FUENMAYOR CONTRERAS, VERONICA SANDRA	Venezolano 	1971-11-05	\N	\N	Femenino 	Soltero 	\N	vfuenmayor@ubv.edu.ve 	COLINAS DE CARRIZAL  SECTOR CERRO GRANDE  RAMAL 2 QTA  VERONICA  CARRIZAL  EDO MIRANDA 	2	AGREGADO	DOCENTE	2005-02-21	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
627	11229230	SUAREZ LEON, FIORELA ALEJANDRA	Venezolano 	1973-01-27	\N	\N	Femenino 	Soltero 	\N	fasuarez@ubv.edu.ve 	CARACAS 	2	INSTRUCTOR	DOCENTE	2010-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
628	11233027	CORREA NUÑEZ, BELKIS DEL SOCORRO	Venezolano 	1971-02-18	\N	\N	Femenino 	Soltero 	\N	 	CALLE EL BOSQUE CASA 610, MANZANA 29, I ETAPA CIUDAD ALIANZA 	0	INSTRUCTOR	CONTRATADO	2019-05-31	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
629	11263612	CONTRERAS, CARLOS	Venezolano 	1969-06-29	\N	\N	Masculino 	Soltero 	\N	ccontreras@ubv.edu.ve 	URB EL PERU VAREDA N 24 SECTOR 3 CASA N 8 	0	AGREGADO	DOCENTE	2005-10-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1734	84557626	GILBERT, CHRISTOPHER ANDREW	Extranjero 	1966-07-28	\N	\N	Masculino 	Soltero 	\N	 	CARACAS 	0	INSTRUCTOR TCV 3	DOCENTE	2006-11-13	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
630	11281420	SALAZAR VALERA, JAIDY JOSEFINA	Venezolano 	1972-06-24	\N	\N	Femenino 	Soltero 	\N	jjsalazar@ubv.edu.ve 	LA CANDELARIA. TRUCABORDO A FERREQUIEN. EDIF. FELIPE LEMNO. APTO 1 	0	AGREGADO	DOCENTE	2005-02-28	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
631	11282939	LUZARDO SANCHEZ, LISSETTE NAYARIT	Venezolano 	1972-01-19	\N	\N	Femenino 	Soltero 	\N	lluzardo@ubv.edu.ve 	URB ANA MARIA CAMPOS MACROPARCELAS #  05 # 94163 SECTOR LA RINCONADA 	1	ASOCIADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
632	11284938	GONZALEZ GONZALEZ, YAJAIRA DEL CARMEN	Venezolano 	1973-09-07	\N	\N	Femenino 	Soltero 	\N	ycgonzalez@ubv.edu.ve 	RED LAS PIRAMIDES TORRE  F  APTO  315 PISO 3 POMONA 	1	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
633	11287112	FERNÁNDEZ IGUARAN, ROBINSON	Venezolano 	1971-07-02	\N	\N	Masculino 	Soltero 	\N	robfernandez@ubv.edu.ve 	AV. 22D CASA # 101-127 SECTOR LA SONRISA SABANETA 	0	INSTRUCTOR	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
634	11287349	FARIAS SUAREZ, GISELA PATRICIA	Venezolano 	1972-07-07	\N	\N	Femenino 	Soltero 	\N	gfarias@ubv.edu.ve 	SECTOR LA LAGO CALLE 76 CON 3 E RESIDENCIAS CABRINI APARTAMENTO 7 	3	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
635	11295060	MORON FUENMAYOR, FREDDY DANIEL	Venezolano 	1972-07-19	\N	\N	Masculino 	Soltero 	\N	fmoron@ubv.edu.ve 	URB. CUATRICENTENARIO, SECTOR 1, VDA. 16 CASA 3 07 	3	AGREGADO	DOCENTE	2004-04-02	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
636	11312206	RAMON Y RIVERA, ISABEL	Venezolano 	1975-06-28	\N	\N	Femenino 	Soltero 	\N	iramon@ubv.edu.ve 	CALLE TICOPORO RESIDENCIAS ACUARIO PISO 9 APTO 91 CARACAS 	1	ASISTENTE	DOCENTE	2004-10-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
637	11319169	GONZALEZ  RIVERO, JUDITH LISETTE	Venezolano 	1973-03-12	\N	\N	Femenino 	Casado 	\N	jlgonzalez@ubv.edu.ve 	URB. GODOFREDO GONZALEZ AX, 2 CALLE  N 15-F 	2	ASISTENTE	DOCENTE	2010-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
638	11337846	CENTENO AYALA, NILSA MERCEDES	Venezolano 	1970-11-02	\N	\N	Femenino 	Soltero 	\N	ncenteno@ubv.edu.ve 	BLOQUES DE LA PARAGUA SECTOR 4 EDIFICIO 4 APTO 4-A CIUDAD BOLIVAR 	1	AGREGADO	DOCENTE	2004-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
639	11340459	LAVERDE MALAVE, ANTONIO RAMON	Venezolano 	1973-07-31	\N	\N	Masculino 	Soltero 	\N	arlaverde@ubv.edu.ve 	CALLE CARABOBO EDIFICIO CARONI PISO N 1 APTTO N 3 	2	INSTRUCTOR	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
640	11348370	MONGUI ACEVEDO, MARÍA YENNY	Venezolano 	1973-06-25	\N	\N	Femenino 	Soltero 	\N	mmongui@ubv.edu.ve 	URB. LOMAS DE FUNVAL. MANZANA 7. VEREDA 11. CASA I-1 	1	ASISTENTE	DOCENTE	2007-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
641	11364098	AROCHA MANZO, GRECIA MARIA	Venezolano 	1972-02-27	\N	\N	Femenino 	Soltero 	\N	garocha@ubv.edu.ve 	URB. POPULAR EL CAÑAVERAL AV. 108-A NUMERO 78-16 	0	INSTRUCTOR	DOCENTE	2006-09-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
642	11366493	CORTEZ BLANCO, MARITDALIA	Venezolano 	1972-06-21	\N	\N	Femenino 	Soltero 	\N	mcortez@ubv.edu.ve 	UEB MACARACUAY CALLE NAIGUATA RES VILLA ESMERALDA APT 34 	2	INSTRUCTOR	DOCENTE	2006-04-17	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
643	11380463	NUNEZ VELASQUEZ, KATIUSKA DE LOS ANGELES	Venezolano 	1973-10-08	\N	\N	Femenino 	Casado 	\N	knunez@ubv.edu.ve 	URB. CUCUTA AV JUNCAL BLOQUE 8 C PISO 1 APTO 4 	2	ASISTENTE	DOCENTE	2005-04-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
644	11392187	PADRON M., JOSÉ RAFAEL	Venezolano 	1973-10-17	\N	\N	Masculino 	Soltero 	\N	jpadron@ubv.edu.ve 	URBANIZACION LA POMONA, CALLE 8, N D-23. MARACAIBO 	2	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
645	11393027	NAVARRO URDANETA, YELITZA CAROLINA	Venezolano 	1972-10-07	\N	\N	Femenino 	Soltero 	\N	ynavarro@ubv.edu.ve 	CARACAS 	0	AGREGADO	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
646	11393522	FERNÁNDEZ MORAN, ANTONIO ENRIQUE	Venezolano 	1973-12-18	\N	\N	Masculino 	Soltero 	\N	aefernandezm@ubv.edu.ve 	LA, PAZ CAMPO BOYACA N 33-B, CALLE N 4 , PARROQUIA JOSE RAMON YEPEZ, MUNICIPIO JESUS ENRIQUE LOSSA 	3	AGREGADO	DOCENTE	2005-04-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
647	11405105	CARMONA PINTO, WILMER OSWALDO	Venezolano 	1972-05-16	\N	\N	Masculino 	Soltero 	\N	wcarmona@ubv.edu.ve 	MONTALBAN II, CALLE 40, RES. SAN GERARDO, APTO 11. CARACAS 	0	INSTRUCTOR	CONTRATADO	2016-06-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
648	11411770	SARMIENTO PEREZ, KATIUSKA VERONICA	Venezolano 	1968-05-20	\N	\N	Femenino 	Casado 	\N	ksarmiento@ubv.edu.ve 	RES. BALCONES DE PARAGUANA, TORRE 4, PB. APTO A 	2	ASISTENTE	DOCENTE	2005-10-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
649	11414141	SUBERO PARUCHA, MARÍA EUGENIA	Venezolano 	1973-12-03	\N	\N	Femenino 	Soltero 	\N	mesubero@ubv.edu.ve 	URB LONGARAY CALLE ALI PRIMERA AV. INTERCOMUNAL DEL VALLE EDIF SANTA ROSA 	2	AGREGADO	DOCENTE	2004-03-31	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
650	11448120	CASTELLIN PALOMO, JOSE  LUIS	Venezolano 	1972-05-23	\N	\N	Masculino 	Soltero 	\N	jcastellin@ubv.edu.ve 	CCS 	0	ASISTENTE	DOCENTE	2011-03-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
651	11449006	SALAZAR, MELIDA DEL CARMEN	Venezolano 	1975-02-10	\N	\N	Femenino 	Soltero 	\N	mcsalazar@ubv.edu.ve 	SECTOR BELLO MONTE CALLE 4 CASA 19 	1	INSTRUCTOR	CONTRATADO	2015-02-02	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
652	11451211	ANGULO MONTILLA, CARLOS RAFAEL	Venezolano 	1970-12-09	\N	\N	Masculino 	Casado 	\N	crangulo@ubv.edu.ve 	URB LOS CACTUS CALLE DON VICENTE # 14 	3	ASISTENTE	DOCENTE	2005-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
653	11454568	FERNÁNDEZ HERNANDEZ, LUIS SEGUNDO	Venezolano 	1967-09-02	\N	\N	Masculino 	Soltero 	\N	lfernandez@ubv.edu.ve 	CARRETERA ORIENTAL, CALLE STA. ELENA. SECTOR MONTE CLARO II - CABIMAS 	0	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
654	11465221	NIETO QUINTERO, MARLENI ALEJANDRA	Venezolano 	1969-10-11	\N	\N	Femenino 	Soltero 	\N	mnieto@ubv.edu.ve 	URB LA MARA CALLE TAMANACO N 25 MERIDA 	1	INSTRUCTOR	DOCENTE	2012-07-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
655	11468968	MELO QUINTERO, WILMER DEL CARMEN	Venezolano 	1973-11-12	\N	\N	Masculino 	Soltero 	\N	wmelo@ubv.edu.ve 	CALLE19 ENTRE AVENIDA1 Y 2 CASA N 1-21 	0	INSTRUCTOR	CONTRATADO	2017-01-09	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
656	11473620	PETIT VALLES, OSCAR ALEXANDER	Venezolano 	1971-02-16	\N	\N	Masculino 	Casado 	\N	opetit@ubv.edu.ve 	SECTOR LA ADJUNTAS URB. SANTA ANA, MANZANA H-02 	3	ASISTENTE	DOCENTE	2004-09-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
657	11476546	PADILLA CEDEÑO, YUDITH JOSEFINA	Venezolano 	1972-12-07	\N	\N	Femenino 	Soltero 	\N	yjpadilla@ubv.edu.ve 	URBANIZACION EL JOBITO EDF 01-02 PISO 03 APT 03-04 	2	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
658	11478133	COLINA MORENO, YUNIMAR JOSEFINA	Venezolano 	1973-02-28	\N	\N	Femenino 	Casado 	\N	ycolina@ubv.edu.ve 	RES. ESMERALDA TORRE P APTO PB-2 PASPO GASPASO CON REPUBLICA 	2	AUXILIAR I	DOCENTE	2004-09-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
659	11539003	MARIN MARCANO, CRUZLENY JOSÉ	Venezolano 	1974-03-27	\N	\N	Femenino 	Casado 	\N	cmarin@ubv.edu.ve 	CALLE ESTRELLA POLAR BOCA DEL RIOP NUEVA ESPARTA 	3	AGREGADO	DOCENTE	2005-12-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
660	11550226	RAGA  NASPE, BLADIMIR	Venezolano 	1968-03-07	\N	\N	Masculino 	Casado 	\N	braga@ubv.edu.ve 	SECTOR EL SITIO, RES. LOS HELECLAS TORRE A, PISO 5 APTO 501 	2	INSTRUCTOR	DOCENTE	2005-10-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
661	11550429	LEON PARRA, ALEXIS FERNANDO	Venezolano 	1972-05-30	\N	\N	Masculino 	Soltero 	\N	aleon@ubv.edu.ve 	URB LA ROSA COJUNTO COLINAS DE GUATIRE AV 3 MANZANA D QUINTA N 129 MIRANDA 	0	COORDINADOR NACIONAL AGREGADO	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
662	11552991	REYES, LIRIO	Venezolano 	1973-07-08	\N	\N	Femenino 	Soltero 	\N	lvreyes@ubv.edu.ve 	URB ANTONIO JOSE DE SUCRE 3 CALLE  BLOQ 3 PISO 3 APTO 5-5 CARACAS 	3	ASISTENTE	DOCENTE	2004-11-30	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
663	11554785	CONTRERAS ALVARADO, OMAR DANIGEL	Venezolano 	1974-05-05	\N	\N	Masculino 	Casado 	\N	odcontreras@ubv.edu.ve 	CALLE PRINCIPAL ARAGUITA 1, VEREDA 14 CASA # 7 	1	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
664	11557846	ALVAREZ TORRES, ILSE MARGARITA	Venezolano 	1972-12-15	\N	\N	Femenino 	Casado 	\N	imalvarez@ubv.edu.ve 	FINAL 4TA CALLE LA ACEQUIA. URB. LA RIVERA.ETAPA A. CASA N 9 	2	INSTRUCTOR	CONTRATADO	2013-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
665	11559131	DE ARMAS GONZALEZ, CIARELYS NAYLLIBEC	Venezolano 	1975-02-22	\N	\N	Femenino 	Soltero 	\N	cdearmas@ubv.edu.ve 	ESQ. LA PASTORA A ESQ. PTE. MONAGAS LA PASTORA 	4	COORDINADOR ACADEMICO ASISTENTE	DOCENTE	2006-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
666	11561759	SILVA CHAPELLIN, EMILIO JOSÉ	Venezolano 	1973-07-09	\N	\N	Masculino 	Soltero 	\N	esilva@ubv.edu.ve 	BARRIO JOSE FELIX RIVAS, ZONA #3, CALLE PRINCIPAL,#46, PETARE 	0	AGREGADO	DOCENTE	2004-04-14	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
667	11563795	BENJAMÍN ALEXANDER, CHRISTAL MALVERY	Venezolano 	1975-02-13	\N	\N	Femenino 	Soltero 	\N	cbenjamin@ubv.edu.ve 	BARRIO BRISAS DEL ORINOCO, CALLE LOS PROCERES DETRAS DEL SERVI CAUCHO MY FRIEND CASA S/N 	0	AGREGADO	DOCENTE	2004-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
668	11605163	GARCIA VALERO, MARIANE NORELIS	Venezolano 	1971-12-14	\N	\N	Femenino 	Soltero 	\N	mngarcia@ubv.edu.ve 	URB. SAN JACINTO. SECTOR 8. VEREDA 2 - CASA # 07 	2	ASISTENTE	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
669	11609902	MARTÍNEZ HERRERA, FERNANDO JOSÉ	Venezolano 	1974-01-12	\N	\N	Masculino 	Soltero 	\N	fjmartinez@ubv.edu.ve 	AV.57C SECTOR LA PASTORA # 95 F EDO ZULIA 	0	AGREGADO	DOCENTE	2005-04-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
670	11611966	GARCIA URDANETA, ADRIANA LUISA	Venezolano 	1974-10-12	\N	\N	Femenino 	Soltero 	\N	algarcia@ubv.edu.ve 	AV. PRINCIPAL POMONA, URB. EL PINAREDIF. PINO MONO1 APTO. 3-F 	2	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
671	11613777	PERDOMO DE AVILA, NEIDA JOSEFINA	Venezolano 	1972-05-28	\N	\N	Femenino 	Casado 	\N	nperdomo@ubv.edu.ve 	URB.ROSALEDA RES.LOS HONGOS CALLE 81 APTO1.T.9 	2	ASISTENTE	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
672	11616678	RODRIGUEZ BERMUDEZ, MONICA	Venezolano 	1976-04-16	\N	\N	Femenino 	Casado 	\N	mrodriguezb@ubv.edu.ve 	AV. ORINOCO SECTOR SAN RAFAEL CASA SIN NUMERO 	2	INSTRUCTOR	DOCENTE	2010-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
673	11647921	TIRADO MARCANO, YULITZA GREGORIA	Venezolano 	1973-05-11	\N	\N	Femenino 	Soltero 	\N	ytirado@ubv.edu.ve 	CAMPO MARA. AV. PRINCIPAL DIAGONAL DEL FUERTE  MARA, CASA #19 B 	2	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
674	11656796	RODRÍGUEZ ORDAZ, GILBERTO RAFAEL	Venezolano 	1973-02-07	\N	\N	Masculino 	Casado 	\N	grrodriguezo@ubv.edu.ve 	CALLE COROMOTO AV. ESPARTA PARROQUIA LA SABANITA CASA #01 OTA. GILCARDANMARY 	4	ASISTENTE	DOCENTE	2004-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
675	11668143	MARTÍNEZ AZOCAR, JENNIFER SIKIU	Venezolano 	1974-07-12	\N	\N	Femenino 	Soltero 	\N	jsmartinez@ubv.edu.ve 	BLOQUE 5-B, APTO. 27 PISO 2. MONTE PIEDAD 	2	AGREGADO	DOCENTE	2006-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
676	11683087	TOVAR ALVARADO, GRACIELA COROMOTO	Venezolano 	1970-09-16	\N	\N	Femenino 	Soltero 	\N	gtovar@ubv.edu.ve 	AV. UNIVERSIDAD EDIF. EL DIAMANTE APTO. 08 PISO 8 LOS CHAGUARAMOS 	0	INSTRUCTOR	DOCENTE	2007-05-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
677	11686464	CORDERO RAMOS, SUGEY MILAGROS	Venezolano 	1975-03-22	\N	\N	Femenino 	Soltero 	\N	scordero@ubv.edu.ve 	AV.10 DE DICIEMBRE CASA NRO. 85,CENTRO.MARACAY 	2	ASISTENTE	DOCENTE	2014-03-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
678	11691325	SALAZAR HERNANDEZ, MIRBER MARIA	Venezolano 	1973-05-20	\N	\N	Femenino 	Soltero 	\N	mmsalazar@ubv.edu.ve 	PARQUE ALTO, URB. LAS LOMAS, EDIF. A3, PISO 2 APTO. A3-31 GUATIRE 	0	ASOCIADO	DOCENTE	2004-04-28	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
679	11697504	APONTE LOPEZ, JANET KAROLINA 	Venezolano 	1974-07-29	\N	\N	Femenino 	Soltero 	\N	jkaponte@ubv.edu.ve 	URB. ROBLES CALLE 10 CARRERA 2 Y 3  CARORA 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
680	11703187	ANGEL, TIODARDO	Venezolano 	1972-09-10	\N	\N	Masculino 	Soltero 	\N	tangel@ubv.edu.ve 	AV. FUERZAR ARMADAS, RESIDENCIAS DORADO. TORE B, PISO 2 - APTO. 22B 	2	ASOCIADO	DOCENTE	2004-04-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
681	11705026	CASANOVA GONZALEZ, EGLEE ELAINES	Venezolano 	1973-01-30	\N	\N	Femenino 	Soltero 	\N	ecasanova@ubv.edu.ve 	EDIF. BARLOVENTO, APTO. 17, PISO 4, CALLE LAS AULAS, VALLE ABAJO, LOS CHAGUARAMOS 	0	ASISTENTE	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
682	11715261	POLEO NOVOA, MASSIEL CELESTE	Venezolano 	1973-11-13	\N	\N	Femenino 	Soltero 	\N	mpoleo@ubv.edu.ve 	URB.DON SAMUEL 1 ETAPA SECTOR F, CASA 25 	2	ASOCIADO	DOCENTE	2006-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
683	11727725	ALVES BOLIVAR, JULISSA VENTURA	Venezolano 	1974-04-10	\N	\N	Femenino 	Soltero 	\N	jalves@ubv.edu.ve 	CARRERA 5 ENTRE CALLE 1 Y 2, CASA N 32-59 	3	AGREGADO	DOCENTE	2004-09-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
684	11728078	TOLEDO ROMERO, JOSÉ SIFREDO	Venezolano 	1973-02-10	\N	\N	Masculino 	Casado 	\N	jtoledo@ubv.edu.ve 	RESIDENCIA BOLIVAR, PASEO SIMON BOLIVAR 	2	AGREGADO	DOCENTE	2004-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
685	11729924	POYO, SIMÓN ANTONIO	Venezolano 	1973-07-24	\N	\N	Masculino 	Soltero 	\N	spoyo@ubv.edu.ve 	CALLE JOSÉ GREGORIO HERNÁNDEZ CASA N 8 CIUDAD BOLÍVAR 	5	AUXILIAR I	DOCENTE	2012-03-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
686	11731257	GUTIÉRREZ DIAZ, NAYMA DEL CARMEN	Venezolano 	1975-08-16	\N	\N	Femenino 	Soltero 	\N	ngutierrez@ubv.edu.ve 	URB. VISTA HERMOSA I VEREDA N 2 CASA N 10 	1	ASISTENTE	DOCENTE	2004-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
687	11732188	POLISINI  DE QUIJADA, ROXANNA TERESA	Venezolano 	1974-10-15	\N	\N	Femenino 	Casado 	\N	rpolisini@ubv.edu.ve 	AV. URDANETA. CASA N# 34 BRISAS DEL ORINOCO CIUDAD BOLIVAR 	1	INSTRUCTOR	DOCENTE	2004-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
688	11739901	ALVARADO VARGAS, MANUEL ALEXANDER	Venezolano 	1976-12-01	\N	\N	Masculino 	Casado 	\N	malvarado@ubv.edu.ve 	URB. PABLO VI RESID. SOFIA, PISO 02, APTO: 04. EL LLANITO. 	2	INSTRUCTOR	DOCENTE	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
689	11748800	ARIAS DE HERNANDEZ, YELITZA DEL CARMEN	Venezolano 	1974-12-14	\N	\N	Femenino 	Casado 	\N	ydarias@ubv.edu.ve 	BOCA DE ARAURO LAS DELICIAS CALLE ZARAZA CASA # 06 	2	ASISTENTE	DOCENTE	2005-10-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
690	11753353	SOLANO, ALDRIN NATALIO	Venezolano 	1969-10-01	\N	\N	Masculino 	Soltero 	\N	asolano@ubv.edu.ve 	MONAGAS VIA NACIONAL HACIA CARIPITO COSTO ARRIBA, ENTRADA LA CAPILLA 	2	COORDINADOR PFG ASISTENTE	DOCENTE	2004-03-02	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
691	11766552	SANCHEZ PRADO, MAYRA YOSELY	Venezolano 	1974-02-26	\N	\N	Femenino 	Casado 	\N	mysanchez@ubv.edu.ve 	CALLE RIVAS N 62 URB LA CANDELARIA PTA CARDON 	2	AGREGADO	DOCENTE	2004-09-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
692	11768560	LUGO VARGAS, MOISES ANTONIO	Venezolano 	1975-01-09	\N	\N	Masculino 	Soltero 	\N	mlugo@ubv.edu.ve 	CALLE SUCRE # 25 BELLA VISTA PUNTO FIJO EDO FALCON 	0	ASISTENTE	DOCENTE	2006-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
693	11772770	SANCHEZ DE PADILLA, CARMEN DEL VALLE	Venezolano 	1973-04-15	\N	\N	Femenino 	Casado 	\N	csanchez@ubv.edu.ve 	CALLE MARIÑO N 17 PUNTA CARDON 	1	ASISTENTE	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
695	11800224	MEDINA AREVALO, YVIS MARGARITA	Venezolano 	1971-07-21	\N	\N	Femenino 	Soltero 	\N	ymmedina@ubv.edu.ve 	MATARUCA ARRIBA, MUNICIPIO COLINA, CALLE STA. MARIA, CASA N81 	1	ASISTENTE	DOCENTE	2005-10-17	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
696	11828126	GAMARDO BALDAN, MILENA DEL CARMEN	Venezolano 	1972-11-02	\N	\N	Femenino 	Soltero 	\N	mgamardo@ubv.edu.ve 	BOCA DE SABANA CALE RIO CARIBE CASA N 50 CUMANA 	1	ASISTENTE	DOCENTE	2005-04-14	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
697	11833840	MORENO NORIEGA, LEONARDO ALEXANDER	Venezolano 	1972-11-06	\N	\N	Masculino 	Casado 	\N	lamorenon@ubv.edu.ve 	URB. 7 SOLES, CALLE PRINCIPAL, CASA N 66 	2	INSTRUCTOR	CONTRATADO	2015-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
698	11850277	RIERA MENDEZ, LEANDRO ANTONIO	Venezolano 	1973-02-18	\N	\N	Masculino 	Soltero 	\N	lriera@ubv.edu.ve 	URB. BARAURE 1, CALLE 6, VDA. 18, CASA N 01 	2	INSTRUCTOR	DOCENTE	2007-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
699	11857904	FUENMAYOR CHACIN, MAIGRET CHIQUINQUIRÁ	Venezolano 	1973-07-12	\N	\N	Femenino 	Soltero 	\N	mcfuenmayorc@ubv.edu.ve 	AV. 9B 60 NUMERO CASA 60-45 SECTOR PUBLE NUEVO MARACAIBO 	1	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
700	11865186	TORRES A, MARÍA C	Venezolano 	1973-01-04	\N	\N	Femenino 	Soltero 	\N	mctorres@ubv.edu.ve 	LOS ROBLES AVENIDA 61 A MARACAIBO ESTADO ZULIA 	3	ASISTENTE	DOCENTE	2004-10-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
701	11865539	GONZALEZ PEREZ, VÍCTOR HUGO	Venezolano 	1973-02-24	\N	\N	Masculino 	Soltero 	\N	vhgonzalez@ubv.edu.ve 	AV. 17  95C-19 SECTOR STA.ROSALIA 	1	AGREGADO	DOCENTE	2004-09-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
702	11867660	URDANETA VILLASMIL, ROSALBA LEONOR	Venezolano 	1972-11-03	\N	\N	Femenino 	Soltero 	\N	rurdaneta@ubv.edu.ve 	URB. LA FLORESTA, AV. 85 #79, A-272 	1	ASISTENTE	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
703	11870357	PAZ PALACIOS, NINOSKA MARGARITA	Venezolano 	1974-05-19	\N	\N	Femenino 	Soltero 	\N	npaz@ubv.edu.ve 	BARRIO SUR AMERICA, CALLE 154, CASA #57-53 	0	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
704	11873912	CARDOZO CUNARRO, SANDRA	Venezolano 	1968-08-22	\N	\N	Femenino 	Soltero 	\N	scardozo@ubv.edu.ve 	URB.SAN JACINTO. SECTOR 12. TRANSVERSAL 12. VERESA 04. CASA 03 	1	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
705	11888204	CHIRINOS DUQUE, EDWIN ARMANDO	Venezolano 	1975-08-02	\N	\N	Masculino 	Soltero 	\N	echirinos@ubv.edu.ve 	SAN JUANA, URB CAMPO DE ORO, BLOQ NUMERO 6 PRIMER PISO APARTAMENTO 01--04 	2	ASISTENTE	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
706	11908169	NEGRON MARTINEZ, MARVILA HELEN	Venezolano 	1975-02-23	\N	\N	Femenino 	Casado 	\N	mhnegron@ubv.edu.ve 	CARRETERA PRINCIPAL ALTOS SUCRE NRO 154 	1	INSTRUCTOR	DOCENTE	2010-10-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
707	11952098	BRICEÑO TRIAY, INTI MIGUEL	Venezolano 	1973-06-13	\N	\N	Masculino 	Casado 	\N	imbriceno@ubv.edu.ve 	UB. ARIAS CALLE 2 CASA N 35 	1	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
708	11960100	QUINTERO MENDOZA, SANDRA LISBETH	Venezolano 	1975-12-15	\N	\N	Femenino 	Soltero 	\N	squintero@ubv.edu.ve 	CARACAS 	1	INSTRUCTOR	DOCENTE	2010-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
709	11963625	MATUTE RUIZ, ROSA ELENA	Venezolano 	1973-01-24	\N	\N	Femenino 	Soltero 	\N	rematute@ubv.edu.ve 	SECTOR LINDA BARINAS CALLE 8  11-1 	1	COORDINADOR PFG INSTRUCTOR	DOCENTE	2006-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
710	11968427	RAMIREZ, ABRAHAM JOSE	Venezolano 	1973-10-09	\N	\N	Masculino 	Casado 	\N	abramirez@ubv.edu.ve 	URB EL TIGRE BLOQUE 7 PISO 2 APARTAMENTO 5 	1	INSTRUCTOR	DOCENTE	2011-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
711	11983823	TORREALBA MEDRANO, LUZ DEL ROCIO	Venezolano 	1972-11-19	\N	\N	Femenino 	Soltero 	\N	medranoluzr@yahoo.es 	AV.BARALT RESIDENCIAS DON JOSE PISO 6 APTO 6-B 	0	INSTRUCTOR	CONTRATADO	2017-10-23	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
712	11988396	SALAS COHEN, BETSY ROSSANA	Venezolano 	1974-02-20	\N	\N	Femenino 	Soltero 	\N	bsalas@ubv.edu.ve 	FINAL CALLE LAS DELICIAS C S/N VIAJERA DEL RIO 	2	ASISTENTE	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
713	12011566	SANOJA CHÁVEZ, AMILCAR ALEXANDER	Venezolano 	1973-03-06	\N	\N	Masculino 	Soltero 	\N	aasanoja@ubv.edu.ve 	SECTOR LA GRANJA AV ESTACIONAMIENTO 2, MANZANA C CASA 86 GUANARE 	2	INSTRUCTOR	DOCENTE	2007-12-03	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
714	12011854	PEREZ GRATEROL, RENE ALEXANDER	Venezolano 	1975-08-22	\N	\N	Masculino 	Soltero 	\N	raperez@ubv.edu.ve 	CCS 	0	INSTRUCTOR	DOCENTE	2011-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
715	12014755	TROCONIS BETANCOURT, ESKEILA SALVADORA	Venezolano 	1974-02-22	\N	\N	Femenino 	Casado 	\N	etroconis@ubv.edu.ve 	URB. SAN CARLOS, CALLE B, CASA B-30 	1	INSTRUCTOR	DOCENTE	2007-09-17	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
716	12015576	ROSSI RIVAS, AUDREY YUDELZI	Venezolano 	1975-08-10	\N	\N	Femenino 	Casado 	\N	arossi@ubv.edu.ve 	URB.LOS COQUITOS VEREDA  5,SECTOR  2 CASA N11 	2	ASISTENTE	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
717	12038829	SANTANA  DE SOTO, AIDA	Venezolano 	1973-04-20	\N	\N	Femenino 	Soltero 	\N	asantana@ubv.edu.ve 	CCS 	2	INSTRUCTOR	DOCENTE	2011-04-04	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
718	12038840	ESPINOZA MANZANILLA, OSWALDO JOSÉ	Venezolano 	1973-05-23	\N	\N	Masculino 	Soltero 	\N	oespinoza@ubv.edu.ve 	PARQUE AMUAY CERRO BAJO CASA S/N FALCON 	1	ASISTENTE	DOCENTE	2004-09-22	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
719	12045990	TORRES GONZALEZ, ANABELA LUISA	Venezolano 	1974-07-26	\N	\N	Femenino 	Soltero 	\N	altorres@ubv.edu.ve 	AV. EL ATLANTIDA CALLE 4 QUINTA FIORIMER CATIA LA MAR EDO VARGAS 	1	ASISTENTE	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
720	12049097	SANDOVAL RUJANO, DAVID ANTONIO	Venezolano 	1975-06-20	\N	\N	Masculino 	Soltero 	\N	 	CARACAS 	0	AGREGADO	DOCENTE	2006-05-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
721	12055656	NECEPURENKO ANDREEVNA, SERGUEI	Venezolano 	1972-06-18	\N	\N	Masculino 	Divorciado 	\N	snecepurenko@ubv.edu.ve 	CRUZ DE LA PALOMA, CALLE EL VALLE, CASA 53-11 	3	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
722	12080061	RODRÍGUEZ MUÑOZ, RAÚL AMILCAR	Venezolano 	1974-01-11	\N	\N	Masculino 	Soltero 	\N	rarodriguezm@ubv.edu.ve 	URB POMELO AV PRINCIPAL VEREDA 2 CASA 03 	3	INSTRUCTOR	DOCENTE	2007-10-29	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
723	12097121	MARCANO RIOS, YAMILA	Venezolano 	1973-04-15	\N	\N	Femenino 	Soltero 	\N	yammarcano@ubv.edu.ve 	CARACAS 	0	AUXILIAR II	DOCENTE	2010-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
724	12099956	BERMUDEZ, ADRIANA COROMOTO	Venezolano 	1972-10-26	\N	\N	Femenino 	Soltero 	\N	acbermudez@ubv.edu.ve 	URB LAGO AZUL CALLE 109-ACASA NO 22-77 	1	AGREGADO	DOCENTE	2005-04-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
725	12100349	RINCON INCIARTE, LENYS JOSEFINA	Venezolano 	1975-01-06	\N	\N	Femenino 	Soltero 	\N	lrincon@ubv.edu.ve/abogado_wayuu333@hotmail.com 	URB. ALTOS DEL SOL AMADO. 3ERA ETAPA, N 300 CALLE EZEQUIEL ZAMORA, EDO ZULIA 	1	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
726	12104222	SALINAS DELGADO, YOSMAR ALBERTO	Venezolano 	1975-06-20	\N	\N	Masculino 	Soltero 	\N	ysalinas@ubv.edu.ve 	CALLE 115 AV. 23 SECTOR POMONA CONJUNTO RESIDENCIAL EL PINAR EDIF. PINO SILVESTREIII PISO 2, APTO 2- 	2	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
727	12114518	BASTIDAS RODRIGUEZ, SORENA	Venezolano 	1974-11-26	\N	\N	Femenino 	Soltero 	\N	sbastidas@ubv.edu.ve 	EL PARAISO SANABRIA EDF ORINOCO 	0	ASISTENTE	DOCENTE	2006-11-03	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
728	12136187	DEL MAR SOLARTE, EDGAR DE JESÚS	Venezolano 	1971-12-21	\N	\N	Masculino 	Soltero 	\N	edelmar@ubv.edu.ve 	AV 14B PARROQUIACHIQUIMQUIRA FRENTE CIRCULO MILITAR 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
729	12141374	ABREU PARRA, ZUGEY OSMARY	Venezolano 	1974-06-12	\N	\N	Femenino 	Soltero 	\N	zabreu@ubv.edu.ve 	CALLE ROMULO GALLEGOS NRO 100 URBANIZACION LA CNDELARIA 	2	ASISTENTE	DOCENTE	2007-04-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
730	12143730	HERRERA MARTINEZ, CARLOS ALBERTO	Venezolano 	1975-09-22	\N	\N	Masculino 	Soltero 	\N	cherrera@ubv.edu.ve 	CAÑA DE AZUCAR SECTOR 01 AVENIDA    02  CASA # 26 MARACAY 	1	AGREGADO	DOCENTE	2005-02-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
731	12150311	HENRÍQUEZ CONEJEROS, JOSÉ FERNANDO	Venezolano 	1957-09-28	\N	\N	Masculino 	Divorciado 	\N	jhenriquez@ubv.edu.ve 	LOS OLIVOS BILBAO N 6 CASA # 4 	0	ASISTENTE	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
732	12153973	SANABRIA, CARMEN CECILIA	Venezolano 	1975-01-10	\N	\N	Femenino 	Soltero 	\N	csanabria@ubv.edu.ve 	CALLE 12 CASA NRO 3. SECTOR I ALTO DE LOS GODOS 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
733	12168988	REQUENA DUARTE, MARY NAZARETT	Venezolano 	1976-04-14	\N	\N	Femenino 	Soltero 	\N	mrequena@ubv.edu.ve 	URB. RAFAEL URDANETA, SECTOR 3, VEREDA 39, CASA 07 	1	INSTRUCTOR	CONTRATADO	2016-01-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
734	12186520	FLORES SALAZAR, MAIRIM CLARET	Venezolano 	1975-01-17	\N	\N	Femenino 	Casado 	\N	mflores@ubv.edu.ve 	URBANIZACION SIMON BOLIVAR CALLE SIMON BOLIVAR CASA S/N 	2	AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
735	12186782	LEZAMA MALAVE, LIDIA JOSEFINA	Venezolano 	1976-03-19	\N	\N	Femenino 	Soltero 	\N	llezama@ubv.edu.ve 	BARRIO ANGOSTURA CALLEJON MARACAY CASA N 2 CIUDAD BOLIVAR 	0	COORDINADOR PFG ASISTENTE	DOCENTE	2004-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
736	12186843	GUZMÁN MARTINEZ, LUZMILA COROMOTO	Venezolano 	1973-11-22	\N	\N	Femenino 	Soltero 	\N	lcguzman@ubv.edu.ve 	CALLE SANTA ELENA, CRUCE CON CALLE ARAGUA NRO 17 	0	INSTRUCTOR	DOCENTE	2011-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
737	12188831	MORALES  APONTE, ANGEL LEONEL	Venezolano 	1974-06-14	\N	\N	Masculino 	Casado 	\N	almoralesa@ubv.edu.ve 	SECTOR LOS COQUITOS  CALLE 3 CALLEJON 18 CASA 7 	0	ASISTENTE	DOCENTE	2007-11-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
738	12190678	LORETO FLORES, CLARET JOSEFINA	Venezolano 	1972-12-05	\N	\N	Femenino 	Soltero 	\N	cloreto@ubv.edu.ve 	URB LA MACARENA N36 CALLE N2 	2	AGREGADO	DOCENTE	2005-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
739	12191019	BERRA ANGULO, LUZMERY	Venezolano 	1974-12-03	\N	\N	Femenino 	Soltero 	\N	lberra@ubv.edu.ve 	URB VILLA SANTA ANA CASA N 16 CALLE N 1 SECTOR CAÑAFISTOLA I 	1	AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
740	12191985	ALMEA PINO, JUAN PABLO	Venezolano 	1975-06-30	\N	\N	Masculino 	Casado 	\N	jalmea@ubv.edu.ve 	PARROQUIA LA SABANITA AV ESPAÑA CALLE EL COROZO  CASA N32 	2	AGREGADO	DOCENTE	2004-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
741	12193353	GONZALEZ VASQUEZ, YASIRA DE JESUS	Venezolano 	1976-12-03	\N	\N	Femenino 	Soltero 	\N	yjgonzalez@ubv.edu.ve 	CARACAS 	2	INSTRUCTOR	DOCENTE	2010-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
742	12194292	PÉREZ LEAL, HENRRY JOSÉ	Venezolano 	1975-08-30	\N	\N	Masculino 	Soltero 	\N	hperez@ubv.edu.ve 	SECTOR NEGRO PRIMERO CALLE PORTUGAL CD BOLIVAR 	1	ASISTENTE	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
743	12194936	RONDÓN GARCIA, MICK JAGGER	Venezolano 	1975-10-06	\N	\N	Masculino 	Casado 	\N	mrondon@ubv.edu.ve 	URB. VILLA AGUA DULCE 2 CASA N 13 CALLE PRINCIPAL AGUA SALADA 	2	ASOCIADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
744	12194962	GARCIA PEREZ, JESSICA JOSEFINA	Venezolano 	1976-05-08	\N	\N	Femenino 	Soltero 	\N	jjgarciap@ubv.edu.ve 	AV BOLIVAR EDIFICIO VICENTA GIOVANNA PISO 1 APTO2-A 	0	INSTRUCTOR	DOCENTE	2010-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
745	12197609	CHACÓN NATERA, JOHNNY JOSÉ	Venezolano 	1975-07-27	\N	\N	Masculino 	Casado 	\N	jchacon@ubv.edu.ve 	ALLE PRINCIPAL DE LAS FLORES DE AGUA SALADA CONJUNTO RESIDENCIAL BELLO CAMPO N 15 	2	ASISTENTE	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
746	12212965	MARCANO HERNANDEZ, GABRIELA RAMONA	Venezolano 	1973-09-01	\N	\N	Femenino 	Soltero 	\N	grmarcano@ubv.edu.ve 	B/ CARMELO URDANETA AVE. 101A NO 73A-68 	1	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
747	12216009	PALMAR, SOL CELINA	Venezolano 	1974-04-27	\N	\N	Femenino 	Soltero 	\N	spalmar@ubv.edu.ve 	MARCELINO I CALLE HACIA LA ESTACION EDE PDVSA GRANJA MIRAFLORES 	2	ASISTENTE	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
748	12220705	ARELLANO PEÑA, ELIANNE CAROLINA	Venezolano 	1976-06-04	\N	\N	Femenino 	Soltero 	\N	earellano@ubv.edu.ve 	CARACAS 	2	INSTRUCTOR	DOCENTE	2011-01-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
749	12223632	ÁVILA SALAZAR, NORMA ELIZABETH	Venezolano 	1973-03-26	\N	\N	Femenino 	Soltero 	\N	navila@ubv.edu.ve 	LA CRUZ DEL PASTEL CALLE SAN BENITO 	1	AGREGADO	DOCENTE	2007-05-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
750	12230785	AYALA MUÑOZ, JENNIFER DEL CARMEN	Venezolano 	1975-05-19	\N	\N	Femenino 	Soltero 	\N	jayala@ubv.edu.ve 	URB. COLINAS DE SANTA MONICA AV. INTERVECINAL, RAMAL 4, QUINTA EL TUBAZO 	2	INSTRUCTOR	DOCENTE	2008-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
751	12238861	PERALTA SANCHEZ, JOSE GREGORIO	Venezolano 	1975-09-15	\N	\N	Masculino 	Soltero 	\N	jperalta@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2011-01-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
752	12239943	JIMÉNEZ GALLARDO, MAGDA CRISTINA	Venezolano 	1974-12-01	\N	\N	Femenino 	Soltero 	\N	mjimenez@ubv.edu.ve 	BARRIO LOS CORTIJOS 	3	INSTRUCTOR	DOCENTE	2007-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
753	12257473	MORONTA RIERA, JORGE LUIS	Venezolano 	1975-01-05	\N	\N	Masculino 	Soltero 	\N	jmoronta@ubv.edu.ve 	URB. VILLA NUEVA, CALLE 1A, CASA N 9-13 	2	ASISTENTE	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
754	12257684	URUETA MENDOZA, CIRA DEL CARMEN	Venezolano 	1974-08-06	\N	\N	Femenino 	Casado 	\N	cdurueta@ubv.edu.ve 	URB. JARDINES DE SAN JAIME LOS HELECHOS CALLE 5-B HM4-18 	2	INSTRUCTOR	DOCENTE	2010-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
755	12257772	MORILLO FERRER, DAVID JESÚS	Venezolano 	1974-10-17	\N	\N	Masculino 	Soltero 	\N	dmorillo@ubv.edu.ve 	URB SANTA RITA CASA N 64 MUNICIPIO SANTA RITA EDO ZULIA 	2	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
756	12259737	COLINA CAMACHO, ERNESTO ISMAEL	Venezolano 	1975-03-17	\N	\N	Masculino 	Soltero 	\N	ecolina@ubv.edu.ve 	CALLE PRINCIPAL COLOMBIA CASA N 26 SAN FERNANDO DE APURE 	2	INSTRUCTOR	CONTRATADO	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
757	12265628	GALINDEZ TORRES, GOOJENGS RAFAEL	Venezolano 	1976-04-10	\N	\N	Masculino 	Soltero 	\N	ggalindez@ubv.edu.ve 	CARACAS 	2	INSTRUCTOR	DOCENTE	2011-01-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
758	12267259	ARCIA, JUAN	Venezolano 	1975-12-19	\N	\N	Masculino 	Casado 	\N	jarcia@ubv.edu.ve 	URB LA PARAGUA EDIFICIO 57A APARTAMENTO 32A 	3	AGREGADO	DOCENTE	2004-09-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
759	12267624	CABELLO LOPEZ, ALBERTO JOSE	Venezolano 	1974-07-08	\N	\N	Masculino 	Casado 	\N	acabello@ubv.edu.ve 	URB. COLINAS DEL NORTE CALLE N 5 CASA 66 SECTOR TIPURO MATURIN 	1	INSTRUCTOR	DOCENTE	2010-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
760	12275012	CARPINTERO SERRADA, SOLIVER	Venezolano 	1976-11-29	\N	\N	Femenino 	Casado 	\N	scarpintero@ubv.edu.ve 	URB. LA LLANADA SECTOR II AV 04 CASA N 35 	2	COORDINADOR ACADEMICO INSTRUCTOR	DOCENTE	2007-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
761	12276046	SALAS, JUAN CARLOS	Venezolano 	1975-02-25	\N	\N	Masculino 	Soltero 	\N	jusalas@ubv.edu.ve 	CALLE 2  VILLA SANTA ANA CASA N01 	1	ASISTENTE	DOCENTE	2007-10-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
794	12505320	DIAZ SALGADO, EMILTERAN JOSE	Venezolano 	1976-04-19	\N	\N	Masculino 	Soltero 	\N	ediaz@ubv.edu.ve 	CCS 	0	INSTRUCTOR	CONTRATADO	2011-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
762	12292153	RUDAS ECHEZURIA, TANIA YATCELY	Venezolano 	1974-07-16	\N	\N	Femenino 	Soltero 	\N	trudas@ubv.edu.ve 	URB. EL PALMAR, CALLE SAN DIEGO, CASA N 42 	0	INSTRUCTOR	DOCENTE	2007-09-24	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
763	12292478	MALDONADO BERMUDEZ, ANA BEATRIZ	Venezolano 	1976-01-28	\N	\N	Femenino 	Soltero 	\N	amaldonado@ubv.edu.ve 	EDIF NAVARRO PISO. 8 APTO81 EL SILENCIO CARACAS 	2	ASISTENTE	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
764	12305750	PORTILLO GARCIA, MARÍA ALEJANDRA	Venezolano 	1975-10-31	\N	\N	Femenino 	Soltero 	\N	maportillo@ubv.edu.ve 	URB  LA FLORIDA SETOR LA LIMPIA AV 88 N 79 I- 36 	1	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
765	12305753	PORTILLO GARCIA, MARÍA ISABEL	Venezolano 	1975-10-31	\N	\N	Femenino 	Soltero 	\N	miportillo@ubv.edu.ve 	URB LA  FLORIDA SECTOR LA LIMPIA AV 88 N 79 I- 36 	0	ASOCIADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
766	12307098	PEROZO CASTRO, ROCENDY MARLIN	Venezolano 	1975-07-13	\N	\N	Femenino 	Soltero 	\N	rperozo@ubv.edu.ve 	CCS 	1	INSTRUCTOR	DOCENTE	2011-02-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
767	12310813	BELLO SILVA, HÉCTOR	Venezolano 	1976-09-22	\N	\N	Masculino 	Casado 	\N	hbello@ubv.edu.ve 	SECTOR 10, UD-14, BLOQUE 3, APTO.03-02 	0	ASISTENTE	DOCENTE	2004-04-19	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
768	12335541	FISCHER HERNANDEZ, ANA MARIA	Venezolano 	1975-03-19	\N	\N	Femenino 	Casado 	\N	afischer@ubv.edu.ve 	CALLE PICHINCHA N 47  SANTA ROSA 	1	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
769	12343682	RAMÍREZ GUTIERREZ, EDICCIO JOSÉ	Venezolano 	1973-11-07	\N	\N	Masculino 	Soltero 	\N	ejramirez@ubv.edu.ve 	AV. LOS MEDANOS PROLONGACION NORTE QUINTA ELBA #31 	2	ASISTENTE	DOCENTE	2004-09-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
770	12348397	PIÑA IZARRA, JOSÉ ABEL 	Venezolano 	1976-05-13	\N	\N	Masculino 	Casado 	\N	jpina@ubv.edu.ve 	URBANIZACIÓN CARABOBO-VEREDA 31 - CASA 04 	2	INSTRUCTOR	CONTRATADO	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
771	12356452	ARAQUE, IDANIA ROSA	Venezolano 	1977-01-24	\N	\N	Femenino 	Soltero 	\N	iaraque@ubv.edu.ve 	URB. LAS ADJUNTAS, SECTOR CONJUNTO COLONIAL VEREDA 07, CASA M13, C4 	0	ASOCIADO	DOCENTE	2004-09-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
772	12356812	CONTRERAS BECERRA, TEOFILO MIGUEL	Venezolano 	1976-11-07	\N	\N	Masculino 	Soltero 	\N	tcontreras@ubv.edu.ve 	PARQUE CHAMA CALLE 2A CASA 73 	2	INSTRUCTOR	DOCENTE	2007-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
773	12360871	LEZAMA PINO, BORIS BOGOMIL	Venezolano 	1974-06-10	\N	\N	Masculino 	Casado 	\N	bblezama@ubv.edu.ve 	URB. ANTONIO JOSE DE SUCRE CALLE N 7 18-E 	1	COORDINADOR DE SEDE ASISTENTE	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
774	12364305	ALVAREZ GARCIA, MARIELA YUDITH	Venezolano 	1975-10-04	\N	\N	Femenino 	Soltero 	\N	myalvarez@ubv.edu.ve 	LAS VEGAS SECTOR CENTRO II, CALLE RAFAEL ROSALES PARCELA NRO 52 	2	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
775	12374743	HEREDIA MARQUINA, MILDRED DEL VALLE	Venezolano 	1973-04-19	\N	\N	Femenino 	Soltero 	\N	mheredia@ubv.edu.ve 	1RA. AV. DE PROPATRIA, RES. ORSARIA PISO 4 APTO 4-A 	0	AGREGADO	DOCENTE	2005-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
776	12379064	GÓMEZ ACOSTA, GERSON JOSÉ	Venezolano 	1972-04-05	\N	\N	Masculino 	Soltero 	\N	gjgomez@ubv.edu.ve 	AV.VISTORIA 	0	ASISTENTE	DOCENTE	2005-11-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
777	12382115	DUQUE LUNA, JUANA ELIZABETH	Venezolano 	1973-05-03	\N	\N	Femenino 	Divorciado 	\N	jduque@ubv.edu.ve 	URB. NUEVO PRADO DE MARIA, AV. LOS SAMANES CON LAS ACACIAS, RESIDENCIAS MANZANA L-B1, PISO 2, APTO 3, CARACAS. 	1	INSTRUCTOR	CONTRATADO	2015-07-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
778	12383086	MATOS CONTRERAS, JOSÉ ANTONIO	Venezolano 	1974-01-17	\N	\N	Masculino 	Soltero 	\N	jmatos@ubv.edu.ve 	AV.FUERZAS ARMADAS ESQUINA SAN RAMON CRUCEZITA EDIF. GLANORAL 	0	AGREGADO	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
779	12389289	RANGEL UZCATEGUI, JOSE ANTONIO	Venezolano 	1974-06-10	\N	\N	Masculino 	Soltero 	\N	jarangel@ubv.edu.ve 	URB.CASA DE TEJAS, I TRASVERSAL CASA 15A-1, OCUMARE DEL TUT 	3	INSTRUCTOR	DOCENTE	2011-05-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
780	12391946	MARQUEZ UGUETO, LILIA ANA	Venezolano 	1973-09-03	\N	\N	Femenino 	Soltero 	\N	lamarquez@ubv.edu.ve 	VIA PRINCIPAL LOS MORALITOS, CASA LAS NEGRAS N 7 CASERIO EL AMARILLO 	1	ASISTENTE	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
781	12400094	CASTILLO GAMEZ, ANA MARIA	Venezolano 	1976-11-21	\N	\N	Femenino 	Soltero 	\N	amcastillo@ubv.edu.ve 	EL VALLE, LONGARAY, RES MARISCAL DE AYACUCHO, PISO 20, APTO 11, TORRE B. CARACAS 	1	ASISTENTE	DOCENTE	2005-03-28	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
782	12406036	ALTUVES, ENDER E	Venezolano 	1974-08-25	\N	\N	Masculino 	Soltero 	\N	ealtuves@ubv.edu.ve 	CALLE 45A ROJA , 5-39, SECTOR ALTO DE JALISCO 	1	ASISTENTE	DOCENTE	2004-10-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
783	12410017	TORRES MEJIAS, GERLYS BRIZOLEY	Venezolano 	1975-03-20	\N	\N	Femenino 	Soltero 	\N	gtorres@ubv.edu.ve 	SECTOR CECILIO ACOSTA CALLE LA LUCITEÑA NRO 308 PETARE 	0	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
784	12432584	LOPEZ GIMENEZ, EGLYMAR YAMILETH	Venezolano 	1975-11-08	\N	\N	Femenino 	Soltero 	\N	eylopez@ubv.edu.ve 	PATARATA AV NEGRO PRIMERO RESIDENCIAS LARENSE TORRE B APTO 4 	1	INSTRUCTOR	DOCENTE	2008-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
785	12440227	VILLAMIZAR TREJO, ARELIS CHIQUINQUIRÁ	Venezolano 	1975-05-12	\N	\N	Femenino 	Soltero 	\N	acvillamizart@ubv.edu.ve 	SABANETA, CALLE SAN BERNARDINO. CASA 196-23 	2	ASISTENTE	DOCENTE	2005-04-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
786	12442231	CASTRO JAIMES, CELINA	Venezolano 	1973-11-02	\N	\N	Femenino 	Soltero 	\N	ccastro@ubv.edu.ve 	URB. SAN JACINTO, SECTOR 15, AV. 07,CASA # 10MPARROQUIA JUANAN DE AVILA 	0	ASISTENTE	DOCENTE	2006-05-08	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
787	12444090	BASTIDAS LOZANO, ROOSALINE ROOSMERI	Venezolano 	1974-11-15	\N	\N	Femenino 	Soltero 	\N	rbastidas@ubv.edu.ve 	SECTOR EL LIBERTADOR AV. 9479G- 64 QUINTA VILLA-MARINA 	1	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
788	12460388	GÓMEZ MATA, HENDRIK EDUARDO	Venezolano 	1975-03-17	\N	\N	Masculino 	Soltero 	\N	hegomez@ubv.edu.ve 	URB. ARMANDO REVERON BLOQUE 15,PISO 2 ,APTO 0206 RAUL LEONI EST. VARGAS 	3	INSTRUCTOR	DOCENTE	2006-01-09	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
789	12466644	ARANAGA PEREZ, MANUEL ANGEL	Venezolano 	1976-05-13	\N	\N	Masculino 	Soltero 	\N	maaranaga@ubv.edu.ve 	BARRIO GUANIPAMATOS, TRES CUADRAS DE LA RUAT 6 AL LADO DE LA IGLESIA EVANGELICA SOL DE JUSTICIA 	3	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
790	12467201	MAVAREZ FRANCO, LORENA MARGARITA	Venezolano 	1975-12-29	\N	\N	Femenino 	Soltero 	\N	lmavarez@ubv.edu.ve 	RES. PINOSTROBUS I, URB. EL PINAR PLANTA BAJA, PBE,POMONA, SECTOR CAMPO ELIAS CALLE ZAMORA CABIMAS 	1	ASISTENTE	DOCENTE	2004-10-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
791	12475409	SOLORZANO, CARMEN SUGIS	Venezolano 	1975-10-16	\N	\N	Femenino 	Soltero 	\N	csolorzano@ubv.edu.ve 	BARRIO A JURO  CALLE HUMBOLT EDF SANTA MONICA APTO # 1 	1	AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
792	12480236	RIOS RODRIGUEZ, DORIANA CAROLINA	Venezolano 	1976-03-09	\N	\N	Femenino 	Soltero 	\N	drios@ubv.edu.ve 	URB LAS ROSAS VEREDA 10 CASA N 17 EL CONSEJO 	1	INSTRUCTOR	CONTRATADO	2013-05-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
793	12497288	DÍAZ DIAZ, TATIANA MARIA	Venezolano 	1975-07-15	\N	\N	Femenino 	Soltero 	\N	tadiaz@ubv.edu.ve 	URB. SANTA IRENE, PROLONGACION ZAMORA, CASA ANA MARIA S/N 	2	AGREGADO	DOCENTE	2005-10-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
795	12509781	PERDOMO RODRÍGUEZ, PAVEL ALFONZO	Venezolano 	1975-12-09	\N	\N	Masculino 	Soltero 	\N	pperdomo@ubv.edu.ve 	BARRIO LA PEÑITA CALLE 24 ENTRE CARRERA 2 Y 3 CASA N2-15 GUANARE 	2	ASISTENTE	DOCENTE	2007-12-03	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
796	12513302	CASTILLO MARIN, YARA MARIA	Venezolano 	1976-01-20	\N	\N	Femenino 	Soltero 	\N	ymcastillo@ubv.edu.ve 	SECTRO STA. LUCIOA AV. 2E CALLE 87# 87-94 	2	ASISTENTE	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
797	12513579	TARAZONA GONZALEZ, ALEJANDRA MARIA	Venezolano 	1974-11-29	\N	\N	Femenino 	Soltero 	\N	atarazona@ubv.edu.ve 	URB. SAN FELIPE SECTOR 1, CALLE 11, N-04 MUNICIPIO SAN FRANCISCO 	1	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
798	12514040	VILCHES FERNANDEZ, OMER ENRIQUE	Venezolano 	1973-10-13	\N	\N	Masculino 	Soltero 	\N	ovilches@ubv.edu.ve 	URB. FRANCISCO DE MIRANDA, AV. 68, CASA N 81-45 	1	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
799	12535409	CORONEL PIÑA, JORGE LUIS	Venezolano 	1976-05-12	\N	\N	Masculino 	Soltero 	\N	jcoronel@ubv.edu.ve 	CARACAS 	2	INSTRUCTOR	DOCENTE	2010-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
800	12552512	HENRÍQUEZ, RAFAEL GERARDO	Venezolano 	1975-11-11	\N	\N	Masculino 	Soltero 	\N	rhenriquez@ubv.edu.ve 	EL PARAISO WASHINGTON EDIF 3B-86 CONJUNTO RES EL PARAISO 	0	ASISTENTE	DOCENTE	2007-04-21	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
801	12563494	VELIS LAREZ, MARÍA VICTORIA	Venezolano 	1975-01-10	\N	\N	Femenino 	Divorciado 	\N	mvelis@ubv.edu.ve 	ALMA MATER CALLE F CASA 03 	1	COORDINADOR DE SEDE ASISTENTE	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
802	12590096	BRACHO, GIOVANY JOSÉ	Venezolano 	1974-08-06	\N	\N	Masculino 	Soltero 	\N	gbracho@ubv.edu.ve 	BARRIO EL LIBERTADOR, AV. 93C, CASA 79H-60 	0	AGREGADO	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
803	12598661	RAMOS MARTINEZ, RONALD Y	Venezolano 	1976-11-01	\N	\N	Masculino 	Soltero 	\N	ryramos@ubv.edu.ve 	EDIFICIO AUGUSTO MALAVE VILLALBA 	1	AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
804	12602043	JARAMILLO, YILSA DEL VALLE	Venezolano 	1974-11-05	\N	\N	Femenino 	Casado 	\N	yjaramillo@ubv.edu.ve 	LOS COQUITOS AV BOLIVAR VEREDA 49 CASA N 8 	2	INSTRUCTOR	DOCENTE	2007-11-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
805	12602580	MILLAN BETANCOURT, JONAT ALEXANDER	Venezolano 	1976-09-06	\N	\N	Masculino 	Soltero 	\N	jmillan@ubv.edu.ve 	CALLEJON SAN SIMON PARROQUIA LA SABANITA # 129 	4	INSTRUCTOR	DOCENTE	2005-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
806	12602975	RODRÍGUEZ DE FREITAS, JUDITH	Venezolano 	1977-09-03	\N	\N	Femenino 	Soltero 	\N	jurodriguez@ubv.edu.ve 	AV PRINCIPAL EL BOSQUE RES ELM BOSQUE DEL COUNTRY CLUB CHACAO 	1	ASISTENTE	DOCENTE	2006-11-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
807	12617309	LUCENA RAMIREZ, LUIS ALFREDO	Venezolano 	1976-08-17	\N	\N	Masculino 	Soltero 	\N	lalucena@ubv.edu.ve 	UBV 	0	INSTRUCTOR	CONTRATADO	2017-10-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1160	16631384	VALBUENA HERNANDEZ, MIGUEL ANGEL	Venezolano 	1983-10-29	\N	\N	Masculino 	Soltero 	\N	mvalbuena@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2010-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1127	15880415	SUÁREZ CARREÑO, ESTEFANIA YOLIMAR	Venezolano 	1983-05-30	\N	\N	Femenino 	Soltero 	\N	esuarez@ubv.edu.ve 	URB. SANTA ANA 	2	ASISTENTE	DOCENTE	2006-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
808	12630014	MOLINA CONTRERAS, CESAR REINALDO	Venezolano 	1975-06-26	\N	\N	Masculino 	Soltero 	\N	cmolina@ubv.edu.ve 	SECTOR BARRIO TACHIRA CALLE 189 DE ABRIL CALLEJON NUMERO 03 	0	ASISTENTE	DOCENTE	2006-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
809	12642184	MUJICA SALAZAR, EDGAR VALENTIN	Venezolano 	1975-10-06	\N	\N	Masculino 	Soltero 	\N	emujica@ubv.edu.ve 	URB. LOMAS DEL AVILA, CALLE N 12, EDF. MIRADOR, PISO 1, APTO. 13 PALO VERDE EDO. MIRANDA 	3	AUXILIAR I	DOCENTE	2005-03-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
810	12643165	LOZADA RODRIGUEZ, MARÍA DEL VALLE	Venezolano 	1974-09-08	\N	\N	Femenino 	Casado 	\N	mvlozada@ubv.edu.ve 	UNARE II SECTOR 1 VEREDA 23 CASA #14 	3	ASISTENTE	DOCENTE	2004-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
811	12645390	CUMANA PEREZ, YANEIRA DEL VALLE	Venezolano 	1977-01-09	\N	\N	Femenino 	Casado 	\N	ycumana@ubv.edu.ve 	CAMBIO DE RUMBO MANZ N 8 CASA N 18 	1	INSTRUCTOR	DOCENTE	2010-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
812	12645436	SOLANO MATUTE, NATALY CRISANDY	Venezolano 	1976-07-10	\N	\N	Femenino 	Casado 	\N	nsolano@ubv.edu.ve 	URB EL PERU BLOQUE N1 PISO N3 APTO 03-02 	2	ASOCIADO	DOCENTE	2004-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
813	12645564	BARRETO TOLEDO, ELLUZ YAMILETH	Venezolano 	1976-05-27	\N	\N	Femenino 	Casado 	\N	ebarreto@ubv.edu.ve 	URB. DOÑA BARBARA EDIFICIO N 7 APTO 03-09 SAN FELIX 	2	AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
814	12648644	COLMENAREZ LINARES, MILVER JOSÉ	Venezolano 	1975-11-09	\N	\N	Masculino 	Soltero 	\N	mcolmenarez@ubv.edu.ve 	LAS BATEAS CALLE PRINCIPAL CASA MAURY 	2	AGREGADO	DOCENTE	2007-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
815	12658243	CARDOZA, DEIVIS ENRIQUE	Venezolano 	1976-01-22	\N	\N	Masculino 	Soltero 	\N	dcardoza@ubv.edu.ve 	EL CEMENTERIO, CALLE BOGOTA  VEREDA 19 DE ABRIL RESIDENCIA CASA N 02 	2	ASISTENTE	DOCENTE	2004-04-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
816	12661766	HERNÁNDEZ RIVERO, JENNY DE LOS ANGELES	Venezolano 	1976-10-26	\N	\N	Femenino 	Casado 	\N	jahernandez@ubv.edu.ve 	AV. LIBERTADOR SECTOR LA PARAGUA RESIDENCIAS LOMAS DE ANGOSTURA EDIFICIO ROSIO II 	2	AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
817	12664778	VARGAS, ALESTER	Venezolano 	1976-09-02	\N	\N	Femenino 	Soltero 	\N	alvargas@ubv.edu.ve 	LA SOLEDAD DE SAN ANTONIO DEL GOLFO MUNICIPIO MEJIA 	1	ASISTENTE	DOCENTE	2004-09-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
818	12665897	MOLINET SUAREZ, BORLANY JOSEFINA	Venezolano 	1976-11-18	\N	\N	Femenino 	Soltero 	\N	bmolinet@ubv.edu.ve 	CARACAS 	1	INSTRUCTOR	DOCENTE	2010-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
819	12665971	TARACHE RENGEL, SIDDARTHA	Venezolano 	1975-10-13	\N	\N	Femenino 	Divorciado 	\N	starache@ubv.edu.ve 	BARRIO CRUZ SALMERON ACOSTA CASA N 4 	1	INSTRUCTOR	DOCENTE	2010-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
820	12676679	RODRÍGUEZ RAMIREZ, EVAHEMIR	Venezolano 	1976-05-30	\N	\N	Femenino 	Soltero 	\N	erodriguez@ubv.edu.ve 	AV JUNCAL EDIG JGH PISO N 1 APTO 1 	2	ASISTENTE	DOCENTE	2005-02-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
821	12678237	GARCIA BUCARITO, RUBÉN DARIO	Venezolano 	1976-03-03	\N	\N	Masculino 	Soltero 	\N	rdgarciab@ubv.edu.ve 	 COMUNIDAD INDIGENA EL GUAMO MUNICIPIO AGUASAY CALLE LA ESCUELA N1 	0	ASISTENTE	DOCENTE	2006-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
822	12683612	ORTIZ CASTILLO, DAYANA MARIELA	Venezolano 	1976-10-10	\N	\N	Femenino 	Soltero 	\N	dortiz@ubv.edu.ve 	URB. EL MARQUES, AV. SANZRESIDENCIAS BREMEN, PISO 6 APTO. 64 	0	AGREGADO	DOCENTE	2005-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
823	12688437	GOITTE FERNANDEZ, MELSI RAMIG	Venezolano 	1976-02-08	\N	\N	Femenino 	Soltero 	\N	mgoitte@ubv.edu.ve 	EL CEMENTERIO,  AV. LA PROVIDENCIA CASA 03-22 	2	ASISTENTE	DOCENTE	2005-04-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
824	12689970	SILVA ARTIGAS, SOIRAM	Venezolano 	1975-07-31	\N	\N	Masculino 	Soltero 	\N	ssilva@ubv.edu.ve 	EL VALLE, AV. INTERCOMUNAL, ENTRADA SUR. LOS SAMANES TORRE E. APTO. 7-D 	2	AGREGADO	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
825	12693013	GARCIA DIAZ, GERARDO ENRIQUE	Venezolano 	1976-04-21	\N	\N	Masculino 	Soltero 	\N	ggarcia@ubv.edu.ve 	URB. LA FLORESTA,AV. 88NUMERO 791-138QUINTA GINETTE 	1	AGREGADO	DOCENTE	2005-04-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1394	15686429	LOPEZ MUÑOZ, GUILFER	Venezolano 	1984-02-06	\N	\N	Masculino 	Soltero 	\N	gulopez@ubv.edu.ve 	CARACAS 	0	AUXILIAR I	DOCENTE	2011-05-16	DOCENTE MEDIO TIEMPO	ACTIVO
826	12701099	LOPEZ SILVA, NEYDA YUBISAY	Venezolano 	1975-06-22	\N	\N	Femenino 	Soltero 	\N	nlopez@ubv.edu.ve 	URB. LOS CREPUSCLOS SECTOR II VEREDA 07, CASA N 06 	1	INSTRUCTOR	DOCENTE	2011-01-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
827	12705229	GUTIÉRREZ JIMENEZ, VANESSA CAROLINA	Venezolano 	1976-11-10	\N	\N	Femenino 	Soltero 	\N	vgutierrez@ubv.edu.ve 	CALLE N15 ENTRE CARRETERAS 21Y22,N21-91. 	1	ASISTENTE	DOCENTE	2006-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
828	12706337	LINAREZ ROJAS, ISABEL CRISTINA	Venezolano 	1977-02-10	\N	\N	Femenino 	Casado 	\N	ilinarez@ubv.edu.ve 	AV. RAMON ANTONIO MEDINA, URB. VILLA FUTURO CASA N 12 	2	AGREGADO	DOCENTE	2004-09-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
829	12710113	ALVARADO HERNANDEZ, ELIANNY MABEL	Venezolano 	1975-11-25	\N	\N	Femenino 	Soltero 	\N	ealvarado@ubv.edu.ve 	URB. LA MORA TORE 409 APTO B-41 	2	ASISTENTE	DOCENTE	2006-06-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
830	12719829	CASTILLO RODRIGUEZ, EDDITH NEILETH	Venezolano 	1976-05-09	\N	\N	Femenino 	Soltero 	\N	encastillo@ubv.edu.ve 	CALLE RICAUTE(CEUBO)AV.24 DE JULIO CASA N0-20 	2	ASISTENTE	DOCENTE	2007-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
831	12761551	ÁVILA PONTON, JOSEFINA JUDITH	Venezolano 	1974-02-23	\N	\N	Femenino 	Soltero 	\N	jjavila@ubv.edu.ve 	AV, ESTE 2, PTE. ANAUCO A PTE. REPUBLICA, PARQUE RES. LOS CAOBOS. TORRE A, PISO 7, APTO. 76 LA CANDE 	3	INSTRUCTOR	DOCENTE	2006-09-19	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
832	12777262	DÁVILA DAVILA, AMARELLY DEL CARMEN	Venezolano 	1975-02-17	\N	\N	Femenino 	Casado 	\N	adcdavila@ubv.edu.ve 	SECTOR BOLARE, AV. BUCHIVACOA CASA N 52 	2	INSTRUCTOR	DOCENTE	2007-06-04	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
833	12778880	FLORES RODRIGUEZ, MARIA MERCEDES	Venezolano 	1977-11-04	\N	\N	Femenino 	Soltero 	\N	mmflores@ubv.edu.ve 	CARACAS 	2	INSTRUCTOR	DOCENTE	2010-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
834	12790901	DE ARMAS RAMIREZ, RITA DESIREE	Venezolano 	1974-10-14	\N	\N	Femenino 	Soltero 	\N	rdearmas@ubv.edu.ve 	URB. EL OASIS CALLE 3 CASA N 64 VIA JADACAQUIRA 	2	INSTRUCTOR	CONTRATADO	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
835	12794990	HERNANDEZ MATA, GUILLERMO RAFAEL	Venezolano 	1974-12-12	\N	\N	Masculino 	Casado 	\N	 	CALLE PRINCIPAL PARAISO CASA N 14 CALLE N1 MATURIN MONAGAS 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
836	12799147	LEON, CLORY	Venezolano 	1977-05-12	\N	\N	Femenino 	Soltero 	\N	clleon@ubv.edu.ve 	CARACAS 	2	INSTRUCTOR	DOCENTE	2011-03-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
837	12810982	PARIATA GARCIA, TAHYCA DE LAS MERCEDES	Venezolano 	1977-09-25	\N	\N	Femenino 	Soltero 	\N	tpariata@ubv.edu.ve 	CALLE COLOMBIA 113-1 SAN MATEO ESTADO ARAGUA 	1	ASISTENTE	DOCENTE	2005-02-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
838	12820198	DIAZ DE HERNANDEZ, CARMEN VICTORIA	Venezolano 	1975-11-27	\N	\N	Femenino 	Soltero 	\N	carmenvictoria2020@gmail.com 	URB LA VERANIEGA CALLE JOSE MARIA VARGAS XCASA 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
839	12840850	TORRES GIL, NICOLASA RAMONA	Venezolano 	1974-11-04	\N	\N	Femenino 	Soltero 	\N	torresnicolasa@gmail.com 	URB LA ESTRELLA EDIF PLUTON APTO AI-2 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
840	12853850	PISOS CAÑIZALEZ, RICHARD JOSÉ	Venezolano 	1976-02-19	\N	\N	Masculino 	Soltero 	\N	rjpisos@ubv.edu.ve 	SECTOR NEGRO PRIMERO, CALLE CARABOBO N 1 	0	ASISTENTE	DOCENTE	2007-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
841	12868662	ROSALES MARQUEZ, ADALANA MAYESTY	Venezolano 	1976-10-09	\N	\N	Femenino 	Soltero 	\N	arosales@ubv.edu.ve 	CALLE 89 N 7A-28 SECTOR VERITAS 	2	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
842	12874993	ESPINOZA RIVAS, AYLEU RAYMI	Venezolano 	1976-10-14	\N	\N	Femenino 	Casado 	\N	aespinoza@ubv.edu.ve 	UNARE II, SECTOR II, BLOQUE N13 APTO 04-1 	2	AGREGADO	DOCENTE	2004-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
843	12890438	DUQUE DE MONTILVA, ODALINA	Venezolano 	1977-07-10	\N	\N	Femenino 	Soltero 	\N	oduque@ubv.edu.ve 	EL SURURAL, VEREDA EL ROSAL CASA N# 2-12 LA GRITA, EDO, TACHIRA 	3	AGREGADO	DOCENTE	2006-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
844	12893052	BRICENO ESPINOZA, MARIANNA DEL VALLE	Venezolano 	1977-01-02	\N	\N	Femenino 	Casado 	\N	mbriceno@ubv.edu.ve 	UNARE II SECTOR II CALLE LA PARAGUA BLOQUE 29 APTO 00-10 	2	ASISTENTE	DOCENTE	2005-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
845	12911824	PIÑANGO, MEYLING ADRIANA	Venezolano 	1977-05-11	\N	\N	Femenino 	Soltero 	\N	mapinango@ubv.edu.ve 	AV. BARALT 1RA CALLE EL RETIRO CASA NRO 31 SAN JOSE AVILA 	1	AGREGADO	DOCENTE	2004-09-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
846	12912198	ORDAZ MARTINEZ, YENIANA LUDBEE	Venezolano 	1976-09-06	\N	\N	Femenino 	Soltero 	\N	yordaz@ubv.edu.ve 	AV VICTORIA CON AV NICARAGUA QTA MAMACHI APTO 214 	1	INSTRUCTOR	CONTRATADO	2015-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
847	12912243	DÍAZ PADRON, ALEX DAVID SAID	Venezolano 	1978-05-18	\N	\N	Masculino 	Soltero 	\N	addiaz@ubv.edu.ve 	RCALLE CARDONES A AURORA EDIF. PANICO APTO C, PARROQUIA ALTAGRACIA 	1	COORDINADOR DE SEDE ASOCIADO	DOCENTE	2004-09-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
848	12916453	NAVAS CAMINO, YAMILA	Venezolano 	1975-11-07	\N	\N	Femenino 	Soltero 	\N	ynavasc@ubv.edu.ve 	SECTOR EL PARAISO- AV. J. A. PAEZ RESIDENCIA LOS FLORES EDIF. EL CLAVEL APTO N 81 	4	AGREGADO	DOCENTE	2004-10-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
849	12918405	ARGUINZONES LUGO, FRANCIS  DEL CARMEN	Venezolano 	1977-10-23	\N	\N	Femenino 	Soltero 	\N	farguinzonez@ubv.edu.ve 	EL TRIGO RES,LAGUNETICA-EDF.ARAGUANEY PISO 18- APTO 18A OS TEQUES 	2	AGREGADO	DOCENTE	2004-04-19	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
850	12972360	CHAPETA LIZCANO, MARÍA EUGENIA	Venezolano 	1977-07-22	\N	\N	Femenino 	Soltero 	\N	mchapeta@ubv.edu.ve 	CARRETERA 4, CASAS N# 2- 82. LA CONCORDIA 	2	ASISTENTE	DOCENTE	2006-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
851	12976584	GARCIA ACOSTA, HELMIS LUIS	Venezolano 	1976-09-01	\N	\N	Masculino 	Casado 	\N	hlgarciaa@ubv.edu.ve 	SECTOR KENEDY CALLE RAFAEL URDANETA 	0	INSTRUCTOR	DOCENTE	2011-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
852	13002304	ATIENZO COLINA, MARÍA ALEXANDRA	Venezolano 	1977-09-25	\N	\N	Femenino 	Soltero 	\N	matienzo@ubv.edu.ve 	SECTOR MONTE CLARO, BARRIO 18 DE OCTUBRE CALLE 1 AV 7. 7-06 	1	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
853	13002923	RIVAS MONTERO, LUZ MARINA	Venezolano 	1974-11-26	\N	\N	Femenino 	Soltero 	\N	lrivas@ubv.edu.ve 	URB. NUEVA DEMOCRACIA VILLA CELESTIAL, CASA # 68-104 SECTOR CIUDAD DE LA FARIA  MARACAIBO 	1	ASOCIADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
854	13003001	HERNÁNDEZ VERA, ROSANGELA	Venezolano 	1976-01-04	\N	\N	Femenino 	Soltero 	\N	rohernandez@ubv.edu.ve 	AV. UNION EL MANZANILLO # 25B-55 	3	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
855	13005833	ALVARADO SAAVEDRA, JOHAIRA ANDREINA	Venezolano 	1977-11-02	\N	\N	Femenino 	Soltero 	\N	jalvarado@ubv.edu.ve 	BARRIO ELM MANZANILLO CALLE 15 # 26B-101 	3	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
856	13006064	MUJICA BAVARESCO, ARIZAY	Venezolano 	1977-01-09	\N	\N	Femenino 	Soltero 	\N	amujica@ubv.edu.ve 	URB. LA ROTARIA AV. 103#008 MARACAIBO 	2	ASISTENTE	DOCENTE	2004-04-02	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
857	13016667	PATRUYO NIÑO, DANIEL ALEJANDRO	Venezolano 	1977-03-01	\N	\N	Masculino 	Soltero 	\N	 	FALCON 	0	INSTRUCTOR	DOCENTE	2004-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
858	13016669	MEDINA ASTUDILLO, SAMIRA JOSEFINA	Venezolano 	1977-08-16	\N	\N	Femenino 	Casado 	\N	sjmedina@ubv.edu.ve 	CALLE SUPERIOR NUMERO 14 LA SABANITA CIUDAD BOLIVAR 	2	ASOCIADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
859	13027461	RODRÍGUEZ SANCHEZ, ELVICELIS CLARETT	Venezolano 	1977-07-16	\N	\N	Femenino 	Soltero 	\N	ecrodriguez@ubv.edu.ve 	URB. LA VELITA II, CALLE 18 VEREDA N 62, CASA N 03 	3	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
860	13028111	ORTIZ HERNANDEZ, ÁNGEL EMIRO	Venezolano 	1977-11-06	\N	\N	Masculino 	Soltero 	\N	aeortiz@ubv.edu.ve 	AV. MANAURE CON CALLE GONZALEZ, CALLE MONZON 	2	INSTRUCTOR	DOCENTE	2007-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
861	13028464	REYES CHIRINO, JESUHIL V	Venezolano 	1977-01-21	\N	\N	Femenino 	Soltero 	\N	jvreyes@ubv.edu.ve 	AV. 16 GUAJIRA CONJUNTO RESIDENCIAL EL CUJI NUCLEO 3 EDIF. 3 APTO. 1-C PLANTA BAJA 	1	ASISTENTE	DOCENTE	2004-10-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
862	13059517	GARCIA SARMIENTO, JOSÉ GREGORIO	Venezolano 	1977-05-01	\N	\N	Masculino 	Soltero 	\N	jggarcia@ubv.edu.ve 	BARRIO AJURO CALLE LAS DELICIAS EDO. BOLIVAR CIUDAD BOLIVAR 	1	AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
863	13060057	INFANTE RAMIREZ, MARÍA MERCEDES	Venezolano 	1976-09-24	\N	\N	Femenino 	Soltero 	\N	mminfante@ubv.edu.ve 	BARRIO BRISAS DEL SUR, SECTOR II,CALLE LAS FLORES # 17CIUDAD BOLIVAR, BOLIVAR 	2	ASISTENTE	DOCENTE	2003-12-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
864	13064551	QUINTERO CEDEÑO, JOHANIA MARGARITA	Venezolano 	1977-02-01	\N	\N	Femenino 	Soltero 	\N	jquintero@ubv.edu.ve 	PARCELAMIENTO SANTA ANA CASA # 791-32 SECTOR LA FLORESTA 	0	ASISTENTE	DOCENTE	2005-04-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
865	13089657	GÓMEZ GERDET, ANDRÉS JOSÉ	Venezolano 	1976-08-15	\N	\N	Masculino 	Casado 	\N	ajgomez@ubv.edu.ve 	URB. UNARE II SECTOR BLOQUE N 13 APTO 04-01 	2	AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
866	13093226	CAZORLA MARCANO, RONALD JOSE	Venezolano 	1976-10-18	\N	\N	Masculino 	Soltero 	\N	rcazorla@ubv.edu.ve 	CARRERA 2-A Nr. 31, URB, NEGRO PRIMERO 	1	ASISTENTE	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
867	13096191	FERNANDEZ ESPINOZA, PAULA YACNERIS	Venezolano 	1977-06-16	\N	\N	Femenino 	Soltero 	\N	pfernandez@ubv.edu.ve 	CALLE PRIMERO DE MAYO, N 26 LOS EUCALIPTOS 	0	INSTRUCTOR	CONTRATADO	2017-09-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
868	13107021	BLANCHARD LUGO, ISBELIA CRISTINA	Venezolano 	1978-01-12	\N	\N	Femenino 	Casado 	\N	iblanchard@ubv.edu.ve 	AV. FRANCISCO MIRANDA CRUSE CON CALLE  1 SECTOR 1 CASA 21 	2	INSTRUCTOR	CONTRATADO	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
869	13109423	ALVAREZ, ALEJANDRINA LUPE DE JESUS	Venezolano 	1977-11-01	\N	\N	Femenino 	Soltero 	\N	alvarezalejandrina1@gmail.com 	FUERTE TIUNA 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
870	13127781	MENDOZA HERNANDEZ, CARLOS JOSÉ	Venezolano 	1977-02-28	\N	\N	Masculino 	Soltero 	\N	cmendoza@ubv.edu.ve 	URB CIUDAD LOZADA SECTOR 2 CALLE 9 CASA N 12 	1	INSTRUCTOR	DOCENTE	2007-05-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
871	13135157	ALVARADO CONTRERAS, ROSA ELEMAR	Venezolano 	1977-07-17	\N	\N	Femenino 	Soltero 	\N	realvarado@ubv.edu.ve 	URB VILLA ROSA SECTOR N 1 BLOQUE PLANTA BAJA APTO 00-06 	1	INSTRUCTOR	CONTRATADO	2010-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
872	13136257	ACOSTA MARTINEZ, NATHALIE DEL VALLE	Venezolano 	1978-03-11	\N	\N	Femenino 	Soltero 	\N	ndacosta@ubv.edu.ve 	CALLE MARIÑO RESIDENCIAL VILLA MAR CASA S/N 	3	INSTRUCTOR	DOCENTE	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
873	13162864	ESCAÑO MATAMOROS, DAYANA MILAGROS	Venezolano 	1977-09-18	\N	\N	Femenino 	Soltero 	\N	descano@ubv.edu.ve 	CALLE COROMOTO CON 3 CALLE SANTA BARBARA EL MANICOMIO CASA S/N PISO 1 LA PASTORA 	1	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1099	15371539	AGÜERO, MERCEDES	Venezolano 	1975-02-28	\N	\N	Femenino 	Soltero 	\N	maguero@ubv.edu.ve 	URB.ALTA VISTA SUR, VEREDA 22, CASA N6 	2	INSTRUCTOR	DOCENTE	2005-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
874	13164117	HERNANDEZ GARCIA, LEONOR ANGELES	Venezolano 	1978-02-08	\N	\N	Femenino 	Soltero 	\N	lahernandez@ubv.edu.ve 	CALLE CEMENTERIO EDIFICIO MONTECENERE 	1	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
875	13165080	MARCANO RODRIGUEZ, GLORIMAR DEL VALLE	Venezolano 	1977-04-23	\N	\N	Femenino 	Casado 	\N	gmarcano@ubv.edu.ve 	URB. BATALLA DE CARABOBO. MANZ. B. N 15. LOS GUAYOS 	0	AGREGADO	DOCENTE	2006-11-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
876	13202333	ROSILLO MORA, NILKA GUADALUPE	Venezolano 	1978-02-20	\N	\N	Femenino 	Soltero 	\N	nrosillo@ubv.edu.ve 	CALLE GONZALEZ ENTRE CALLES NORTE Y VUENVANCARAS 	0	AGREGADO	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
877	13206445	BENITEZ ALVAREZ, JOSÉ ELEAZAR	Venezolano 	1975-07-28	\N	\N	Masculino 	Soltero 	\N	jebenitez@ubv.edu.ve 	URB. LA ESTANCIA, EDIF. N , APTO 3-4, CABUDARE 	1	ASOCIADO	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
878	13231999	OCHOA CASTRO, ZENAIDA TRINIDAD	Venezolano 	1974-08-25	\N	\N	Femenino 	Soltero 	\N	zochoa@ubv.edu.ve 	URB. QUENDA, RES. KENDALL, TORRE A, PÌSO 12, APTO.22 	2	INSTRUCTOR	DOCENTE	2012-01-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
879	13243149	BOHORQUEZ CISNEROS, HÉCTOR HERNAN	Venezolano 	1978-01-26	\N	\N	Masculino 	Soltero 	\N	hectorbohorquez@ubv.edu.ve 	URBANIZACION LA TRINIDAD CALLE 58 CASA#15 	1	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
880	13243950	RAFFE LOPEZ, FELIPE  RAMON	Venezolano 	1978-04-11	\N	\N	Masculino 	Casado 	\N	fraffe@ubv.edu.ve 	CALLE SARMIENTO CASA N 27 ENTRE AV. BOLIVAR Y CALLEJON PENINSULAR. SECTOR INSDUSTRIAL 	2	INSTRUCTOR	DOCENTE	2011-02-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
881	13250570	BLANCO CARVAJAL, AGLAYDIS RAMON	Venezolano 	1976-12-29	\N	\N	Masculino 	Soltero 	\N	ablanco@ubv.edu.ve 	CALLE PRINCIPAL PINTO SALINAS CASA N 14-A MATURIN 	0	INSTRUCTOR	CONTRATADO	2018-01-22	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
882	13251943	HERNÁNDEZ GUILLEN, DAYENI YAQUELINE	Venezolano 	1978-03-27	\N	\N	Femenino 	Soltero 	\N	dhernandez@ubv.edu.ve 	URB. VILLAS DEL ORINOCO CASA N 33 SECOR ZANJOTE LOS PROCERES 	2	AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
883	13267962	GÓMEZ DIAZ, JOSÉ FELIX	Venezolano 	1976-07-23	\N	\N	Masculino 	Soltero 	\N	jfgomez@ubv.edu.ve 	RAMON RUIZ POLANCO, ANTIGUO AEROPUERTO, CALLE CENTRAL, NUM. B A 1 	0	ASISTENTE	DOCENTE	2005-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
884	13301142	LÓPEZ CHIRINOS, MIRIHELIS	Venezolano 	1976-07-05	\N	\N	Femenino 	Soltero 	\N	milopez@ubv.edu.ve 	URBANIZACION VILLA RITA CALLE LAS FLORES CASA #64 	2	AGREGADO	DOCENTE	2004-04-28	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
885	13310630	TEJEDOR RUIZ, LEIDA EUGENIA	Venezolano 	1976-03-04	\N	\N	Femenino 	Soltero 	\N	ltejedor@ubv.edu.ve 	BALOA CALLE CARUTAL EDF. MONTERREY APTO 4B 	1	INSTRUCTOR	DOCENTE	2007-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
886	13310825	ALVAREZ LEAL, DARLING SHUGUEY	Venezolano 	1978-09-18	\N	\N	Femenino 	Soltero 	\N	dsalvarez@ubv.edu.ve 	CONJ. PEDRO MARIA FREITES EDF. PLATILLON PISO 1 APTO F16 FUERTE TIUNA CARACAS 	2	AGREGADO	DOCENTE	2005-03-14	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
887	13326537	CORASPE GONZÁLEZ, ZULEIDA JOSEFINA	Venezolano 	1976-07-08	\N	\N	Femenino 	Soltero 	\N	zcoraspe@ubv.edu.ve 	URB. NEGRO PRIMERO, CALLE BETHEL, CASA #4 	1	ASISTENTE	DOCENTE	2007-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
888	13334725	MUJICA MATA, JASMIN KARINA	Venezolano 	1978-08-16	\N	\N	Femenino 	Soltero 	\N	jmujica@ubv.edu.ve 	CALLE INDEPENDENCIA C/C PASEO GASPARI N 7 	1	INSTRUCTOR	DOCENTE	2007-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
889	13335492	CAMACHO TOVAR, MARLIN MIGDALIA	Venezolano 	1978-03-13	\N	\N	Femenino 	Soltero 	\N	mmcamacho@ubv.edu.ve 	RESIDENCIAS LAS CHURUATAS TORRE 0 PISO N4 APTO 43 ALTA VISTA SUR 	0	AGREGADO	DOCENTE	2004-09-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
890	13336333	ORIVE PONTE, ISABEL CRISTINA	Venezolano 	1976-06-13	\N	\N	Femenino 	Soltero 	\N	iorive@ubv.edu.ve 	RESIDENCIAS CHURUATA, EDIFICIO 9 PISO 4 APTO 43 	2	AGREGADO	DOCENTE	2004-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
891	13349784	ZAMBRANO DELGADO, ATHAMAIKA DEL VALLE	Venezolano 	1977-11-11	\N	\N	Femenino 	Soltero 	\N	azambrano@ubv.edu.ve 	CARACAS 	0	COORDINADOR PFG ASISTENTE	DOCENTE	2004-09-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
892	13356821	FLORES FEBRES, LIUSVIMAR DEL VALLE	Venezolano 	1978-05-05	\N	\N	Femenino 	Soltero 	\N	lvflores@ubv.edu.ve 	CALLE PAEZ N58 -LA CANDELARIA 	5	ASISTENTE	DOCENTE	2004-09-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
893	13371365	CASTELLANO URDANETA, EDGAR JUNIOR	Venezolano 	1977-07-14	\N	\N	Masculino 	Soltero 	\N	ecastellano@ubv.edu.ve 	CARACAS 	2	ASISTENTE	DOCENTE	2011-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
894	13376861	MORENO SUAREZ, ROSSINIS ESTHER	Venezolano 	1978-12-21	\N	\N	Femenino 	Soltero 	\N	rmoreno@ubv.edu.ve 	SECTOR LIMPIA, URB. SUCRE, CALLE 26 CASA 26-69 	0	COORDINADOR DE SEDE ASOCIADO	DOCENTE	2005-04-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
895	13406248	GONZALEZ GUTIERREZ, ELVIS JOSE	Venezolano 	1977-09-26	\N	\N	Masculino 	Soltero 	\N	ejgonzalez@ubv.edu.ve 	MONTE PIEDAD, BLOQUE 5, APTO. 31 LETRA C 	0	INSTRUCTOR	CONTRATADO	2016-01-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
896	13415995	OLLARVE MARCHAN, JUANA JOSEFINA	Venezolano 	1977-06-09	\N	\N	Femenino 	Casado 	\N	jjollarve@ubv.edu.ve 	CALLE CLARET CAS S/N 	2	ASISTENTE	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
897	13417123	LAGUNA GOMEZ, ALIFRANK JAVIER	Venezolano 	1978-01-27	\N	\N	Masculino 	Soltero 	\N	alaguna@ubv.edu.ve 	URB. PEDRO MANUEL ARAYA II CALLE 1 ENTRE CALLE 5 Y 4 M-K N 71 	1	VICERRECTOR	ALTO NIVEL	2004-09-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
898	13431896	ACOSTA MOSQUERA, CRISTOBAL SEGUNDO	Venezolano 	1975-02-23	\N	\N	Masculino 	Casado 	\N	csacosta@ubv.edu.ve 	URB. CRUZ VERDE SECTOR 4, CALLE 9, CASA NUM . 10 	1	AGREGADO	DOCENTE	2005-02-28	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
899	13461322	SEMPRUN ORTIGOZA, LILIBETH CAROLINA	Venezolano 	1979-04-01	\N	\N	Femenino 	Soltero 	\N	lsemprun@ubv.edu.ve 	LAGO AZUL RIO MISOA 	1	AGREGADO	DOCENTE	2005-04-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
900	13467039	OTERO SILVA, GLADYS MIREYA	Venezolano 	1963-07-28	\N	\N	Femenino 	Casado 	\N	gotero@ubv.edu.ve 	URB,EL SINARAL, CALLE PPAL, N# 67 	1	ASISTENTE	DOCENTE	2006-10-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
901	13467811	HERNANDEZ  RAMIREZ, JENIFER JACKELINE	Venezolano 	1978-10-11	\N	\N	Femenino 	Casado 	\N	jjhernandez@ubv.edu.ve 	URBANIZACION SUCRE VEREDA 11 N 4 AVENIDA PRINCIPAL 	2	INSTRUCTOR	DOCENTE	2010-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
902	13474907	GARCIA PEREZ, SIULYPIN	Venezolano 	1977-08-01	\N	\N	Femenino 	Soltero 	\N	sigarcia@ubv.edu.ve 	SECTOR5 EL MEMBRILLO, DIAGONAL A LA PLANTA, URBANIZACION LA VICTORIA 	0	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
903	13487388	CANELA MENDOZA, FRANCIS VERONICA	Venezolano 	1977-12-15	\N	\N	Femenino 	Soltero 	\N	fcanela@ubv.edu.ve 	PARROGUIA SAN JUAN SAN MARTIN 	2	ASISTENTE	DOCENTE	2004-11-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
904	13488122	ACOSTA YANEZ, GIOANNA INMACULADA	Venezolano 	1977-12-08	\N	\N	Femenino 	Soltero 	\N	gacosta@ubv.edu.ve 	CALLE PRINCIPAL, SECTOR ANTONIO CAS # 77-A 	1	INSTRUCTOR	DOCENTE	2007-07-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
905	13495029	PAREDES B, CARLOS A	Venezolano 	1976-11-09	\N	\N	Masculino 	Soltero 	\N	cparedes@ubv.edu.ve 	SAN JOSE CALLE 35 CASA# 92-126 	0	ASISTENTE	DOCENTE	2004-10-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1296	4084991	CARRILLO GIL, FRANCISCO	Venezolano 	1951-05-15	\N	\N	Masculino 	Soltero 	\N	frcarrillo@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2011-01-10	DOCENTE MEDIO TIEMPO	ACTIVO
906	13500420	LARRAZABAL COLINA, MARÍA CRISTINA	Venezolano 	1979-03-14	\N	\N	Femenino 	Soltero 	\N	mlarrazabal@ubv.edu.ve 	AV, LA LIMPIEZA, URBANIZACION LA FLORESTA, CALLE 79-J 	1	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
907	13507156	CARPIO PEREZ, JESÚS RAMON	Venezolano 	1977-08-15	\N	\N	Masculino 	Casado 	\N	jrcarpio@ubv.edu.ve 	URB. VISTA HERMOSA II MANZANA 27 N 1 QUINTA NINOSKA 	3	ASISTENTE	DOCENTE	2005-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
908	13507419	GONZALEZ, CARLOS	Venezolano 	1978-02-21	\N	\N	Masculino 	Soltero 	\N	cisrrael@ubv.edu.ve 	URBANIZACION MEDINA ANGARITA, VEREDA NUM. 6CASA NUM 89, PARROQUIA CATEDRAL, MUNICIPIO HERES CIUDAD B 	2	COORDINADOR PFG AGREGADO	DOCENTE	2004-09-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
909	13511560	VALBUENA ACOSTA, KENIA K	Venezolano 	1977-12-15	\N	\N	Femenino 	Soltero 	\N	kvalbuena@ubv.edu.ve 	URB.SAN FRANCISCO AV 25 BLOQ.01 EDIF 047.03-02 EDOMARACAYBO 	1	ASISTENTE	DOCENTE	2004-10-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
910	13525456	SOTO, CAROL EYENNI	Venezolano 	1978-05-18	\N	\N	Femenino 	Soltero 	\N	csoto@ubv.edu.ve 	CALLE MONACO QUINTA RUEDA MEDRANO CALIFORNIA NORTE 	0	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
911	13529833	BRACHO LEON, CARLOS ALBERTO	Venezolano 	1978-12-09	\N	\N	Masculino 	Soltero 	\N	cabracho@ubv.edu.ve 	SECTOR EL NARANJAL, CALLE 47 CON AV. 15J. CJTO. RESIDENCIAL LA CALIFORNIA. EDIF. 3, APTO. 2-D, PISO 	1	INSTRUCTOR	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
912	13533098	GARCIA, FABIOLA	Venezolano 	1978-12-21	\N	\N	Femenino 	Casado 	\N	fabgarcia@ubv.edu.ve 	URB EL PERU, SECTOR 1 VEREDA N 22 CASA N 16 	1	ASISTENTE	DOCENTE	2005-10-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
913	13535324	BLANCO SEQUERA, ANTONIO JOSE	Venezolano 	1978-09-19	\N	\N	Masculino 	Soltero 	\N	ablancosequera@gmail.com 	SECTOR BARRIO UNION DE PETARE 	0	INSTRUCTOR	CONTRATADO	2018-01-22	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
914	13540254	HERNÁNDEZ, FERNANDO ELEAZAR	Venezolano 	1978-04-14	\N	\N	Masculino 	Casado 	\N	fehernandez@ubv.edu.ve 	LAS DINAMITAS, CARRERA 5 ENTRE CALLES 1 Y 2. CASA 32 - 59 	2	AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
915	13544252	RODRÍGUEZ LOPEZ, YUSMARYS JOSEFINA	Venezolano 	1977-04-14	\N	\N	Femenino 	Casado 	\N	yrodriguez@ubv.edu.ve 	SECTOR NAZARENO I, CALLE #3, CASA #25 	3	ASISTENTE	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
916	13544425	MENDOZA VARGAS, NODIL RAFAEL	Venezolano 	1976-11-01	\N	\N	Masculino 	Soltero 	\N	 	CALLE SANTO DOMINGO CASA N 28 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
917	13544916	CARDIE FAJARDO, EDWARD JOSÉ	Venezolano 	1978-01-28	\N	\N	Masculino 	Casado 	\N	ejcardie@ubv.edu.ve 	CALLE PRINCIPAL VALLE VERDE S/N 	2	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
918	13546196	GONZALEZ RUIZ, ELIZABETH MORELYS	Venezolano 	1977-07-02	\N	\N	Femenino 	Soltero 	\N	elmgonzalez@ubv.edu.ve 	MAIPURE I CALLE EL PORVENIR AL FINAL CASA S/N 	1	AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
919	13547293	RAMIREZ PALENCIA, ELEIDA DEL CARMEN	Venezolano 	1978-08-08	\N	\N	Femenino 	Soltero 	\N	ecramirez@ubv.edu.ve 	SAN ISIDRO CALLE 10 ENTRE AV 18 	1	ASISTENTE	DOCENTE	2008-02-28	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
920	13571569	SANCHEZ ARTEAGA, SUMY	Venezolano 	1978-10-27	\N	\N	Femenino 	Soltero 	\N	susanchez@ubv.edu.ve 	URB.CARIBE CALLE CIRCUNVALACION  RES. LA VILLA 	1	INSTRUCTOR	DOCENTE	2008-02-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
921	13573101	FERMAN CABEZA, YUDELQUIS MARGARITA	Venezolano 	1977-06-13	\N	\N	Femenino 	Soltero 	\N	yferman@ubv.edu.ve 	AVENIDA LIBERTADOR BLOQUES DE LA PARAGUA EDIFICIO 6-3-A 2DO PISO 	2	AGREGADO	DOCENTE	2004-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
922	13573790	GARCIA DIAZ, GABRIEL ALEXANDER	Venezolano 	1977-08-31	\N	\N	Masculino 	Soltero 	\N	gagarcia@ubv.edu.ve 	URB. LA FLORESTA AV. 88# 791-138 MARACAIBO 	0	AGREGADO	DOCENTE	2006-11-20	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
923	13577400	ACOSTA ZAMBRANO, SABHIB GABRIEL	Venezolano 	1977-09-25	\N	\N	Masculino 	Casado 	\N	sacosta@ubv.edu.ve 	BELLA VISTA, CONJ. RES. LAS CUMARAGUAS 	3	AGREGADO	DOCENTE	2004-09-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
924	13595107	MORA ROMERO, YORLIS JOSEFINA	Venezolano 	1979-07-31	\N	\N	Femenino 	Soltero 	\N	yjmora@ubv.edu.ve 	BARRIO VENEZUELA, CALLE PRINCIPAL N 36 	1	ASISTENTE	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
925	13628289	RODRÍGUEZ TELLEZ, BELLALIZ ESPERANZA	Venezolano 	1979-01-19	\N	\N	Femenino 	Soltero 	\N	berodriguez@ubv.edu.ve 	PUERTA MARAVEN CALLE H.M PEÑA CASA N8 	2	AGREGADO	DOCENTE	2004-09-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
926	13637189	SLIBE JIMENEZ, LARISSA	Venezolano 	1977-12-29	\N	\N	Femenino 	Soltero 	\N	lslibe@ubv.edu.ve 	KARIMAO COUNTRY ALTOS DE PARQUE CAIZA 	2	AGREGADO	DOCENTE	2005-03-28	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
927	13637732	RETIFF VAHLIS, GUILLERMO ANDRES	Venezolano 	1979-11-07	\N	\N	Masculino 	Casado 	\N	gretiff@ubv.edu.ve 	URB LA PARAGUA POTOGUE 4-8-B 	1	ASISTENTE	DOCENTE	2004-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
928	13647007	LEDEZMA MARCANO, YAMILETH DEL CARMEN	Venezolano 	1978-06-21	\N	\N	Femenino 	Soltero 	\N	yledezma@ubv.edu.ve 	AV LA PARAGUA BLOQUE LA PARAGUA EDIFICIO 1-1-B PISO 3 APTO 42 	2	AGREGADO	DOCENTE	2005-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
929	13649533	TORO SANCHEZ, YAJAIRA JOSEFINA	Venezolano 	1977-04-30	\N	\N	Femenino 	Soltero 	\N	ytoro@ubv.edu.ve 	URB. LOS CACIQUES, CALLE NORTE 06 CASA 11 	1	ASISTENTE	DOCENTE	2005-10-17	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
930	13654546	TRIAS RUIZ, NIURKYS NINOZCA	Venezolano 	1977-03-13	\N	\N	Femenino 	Soltero 	\N	ntrias@ubv.edu.ve 	VEREDA 26 NUMERO 5 LOS GODOS SECTOR III 	2	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
931	13656971	ABREU CHOPITE, CRISTIAN EDUVIJES	Venezolano 	1978-12-01	\N	\N	Femenino 	Soltero 	\N	cabreu@ubv.edu.ve 	CARRERA 6 CASA NRO 204 ANTIGUA CALLE BERMUDEZ AV LAS PALMERAS 	0	INSTRUCTOR	CONTRATADO	2014-02-17	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
932	13656999	SANCHEZ  ZERPA, SILIO   CESAR	Venezolano 	1979-05-11	\N	\N	Masculino 	Soltero 	\N	 	CALLE 14 DE LOS JARDINES DEL VALLE EDIF. B 52 APTO 10-05 	2	ASISTENTE	DOCENTE	2017-10-23	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
933	13657106	PEREZ HERNANDEZ, HERNAN ELIEZER	Venezolano 	1978-02-23	\N	\N	Masculino 	Casado 	\N	hepereze@ubv.edu.ve 	URB VILLA SAN GABRIEL MANZANA N 8 CASA N 132 	1	COORDINADOR ACADEMICO ASISTENTE	DOCENTE	2005-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
934	13657158	LEON VALLES, JOELIS ESTHER	Venezolano 	1979-02-18	\N	\N	Femenino 	Casado 	\N	jeleonv@ubv.edu.ve 	URB VILLA SANTA ANA CASA N 73 CAÑA FISTOLA 1 PARROQUIA CATEDRAL 	2	INSTRUCTOR	DOCENTE	2012-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
935	13658654	VALLADARES NARVAEZ, YESSI YAQUELINE	Venezolano 	1977-12-18	\N	\N	Femenino 	Soltero 	\N	yvalladares@ubv.edu.ve 	COMUNIDAD EL MILAGRO CALLE N 196 	2	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
936	13658948	RUEDA, DELIA ELVIRA JOSEFINA	Venezolano 	1978-10-25	\N	\N	Femenino 	Soltero 	\N	drueda@ubv.edu.ve 	URB EL ZAMURO PASEO HERES #1 QTA DOÑA DELIA 	1	AGREGADO	DOCENTE	2005-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
937	13662492	PADILLA GUANIPA, YOLIMAR DEL ROSARIO	Venezolano 	1978-09-22	\N	\N	Femenino 	Casado 	\N	ypadilla@ubv.edu.ve 	AV. ANDRES BELLO CASA N# 95 DIAGONAL AL ESTADIO LOS MAYORES. 	1	AGREGADO	DOCENTE	2004-09-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
938	13662779	OVIEDO GONZALEZ, NELGLYS LOURDES	Venezolano 	1977-10-05	\N	\N	Femenino 	Soltero 	\N	noviedo@ubv.edu.ve 	AV. E 2 K30 PEDIZO MANVEL ARCAYA II ETAPA 	0	ASISTENTE	DOCENTE	2005-05-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
939	13662877	GÓMEZ LOPEZ, ROY JOSÉ	Venezolano 	1979-04-05	\N	\N	Masculino 	Casado 	\N	rjgomez@ubv.edu.ve 	URB. PEDRO MANUEL ARYA II ETAPA MANZANA M # 44 	2	AGREGADO	DOCENTE	2004-09-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
940	13662944	QUIÑONES GAUNA, ALEXIS EDUARDO	Venezolano 	1979-02-28	\N	\N	Masculino 	Casado 	\N	aquinones@ubv.edu.ve 	AV. JACINTO JANS, ENTRE CALLE 1 DE MAYO Y AV. RAMON DIAZ POLANCO, SECTOR ROJAS 	2	INSTRUCTOR	DOCENTE	2007-06-04	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
941	13664772	TROMP FLORES, MAYRA ALEJANDRA	Venezolano 	1976-02-11	\N	\N	Femenino 	Soltero 	\N	mtromp@ubv.edu.ve 	URB COLINAS DE PEQUIVEN CALLE 20 CASA N 07 anex 	1	ASISTENTE	DOCENTE	2006-11-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
942	13684655	MARIANY JANSEN, JENNIFER JOSEFINA	Venezolano 	1977-06-28	\N	\N	Femenino 	Casado 	\N	jmariany@ubv.edu.ve 	URB. BARRIO SUCRE, CALLE PRINCIPAL, CASA N 4 FRENTE AL PARQUE MUNICIPAL 	2	COORDINADOR DE SEDE ASISTENTE	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
943	13699061	RONDÓN ARAUJO, ROSALYNN	Venezolano 	1978-03-08	\N	\N	Femenino 	Soltero 	\N	rrondona@ubv.edu.ve 	URB. LAS MERCEDES SECTOR 1 BLOQUR 3 APTO 0104 PISO 1 	3	COORDINADOR NACIONAL ASISTENTE	DOCENTE	2007-01-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
944	13704263	AMOROCHO ALBURJAS, PEDRO LUIS	Venezolano 	1979-07-16	\N	\N	Masculino 	Casado 	\N	pamorocho@ubv.edu.ve 	URB. SAN JACINTO SECTOR 14, CALLE 6 CASA 03, VEREDA 7 	2	ASISTENTE	DOCENTE	2005-04-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
945	13713503	CHÁVEZ LAURI, JOSÉ ENRIQUE	Venezolano 	1958-08-11	\N	\N	Masculino 	Soltero 	\N	jechavez@ubv.edu.ve 	CALLE CECILIO ACOSTA (ENTRE LA AV FCO DE MIRANDA Y CALLE SUCRE) EDIF PAMEGAL PISO 7 APTO 22 CHACAO 	1	ASISTENTE	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
946	13723881	VELASCO, REINALDO JESÚS	Venezolano 	1979-12-01	\N	\N	Masculino 	Casado 	\N	rvelasco@ubv.edu.ve 	URB MONSEÑOR ITURRIZA CALLE #1 CASA #20 	2	AGREGADO	DOCENTE	2006-11-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
947	13730114	GARCIA GARCIA, LUIS MARCIAL	Venezolano 	1979-06-21	\N	\N	Masculino 	Soltero 	\N	lgarcia@ubv.edu.ve 	UNARE II SECTOR#2 BLOQUE 18 APARTAMENTO 0005 PUERTO ORDAZ ESTADO BOLIVAR 	1	AGREGADO	DOCENTE	2004-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
948	13737559	VELOZ, MARÍA CELIA	Venezolano 	1960-07-25	\N	\N	Femenino 	Soltero 	\N	mveloz@ubv.edu.ve 	CAMPO ELIAS A CAMILO TORRES EDIF MAICA PISO 6 APTO 61 SAN AGUSTIN DEL  NORTE 	0	INSTRUCTOR	DOCENTE	2005-09-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
949	13754871	CORDOVA SALAS, ERVER ALEXANDER	Venezolano 	1976-05-20	\N	\N	Masculino 	Soltero 	\N	eacordova@ubv.edu.ve 	BARRIO LOS TAMARINDOS CALLE NEGRO PRIMERO CASA  N 77 	0	ASISTENTE	DOCENTE	2011-01-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
950	13769711	PARIATA GARCIA, MAYRA MARIA	Venezolano 	1978-11-27	\N	\N	Femenino 	Soltero 	\N	mpariata@ubv.edu.ve 	CALLE COLOMBIA # 113-1 SAN MATEO EDO ARAGUA 	1	ASISTENTE	DOCENTE	2004-09-20	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
951	13778737	NUÑEZ ZAPATA, MIRELBA DEL CARMEN	Venezolano 	1979-11-15	\N	\N	Femenino 	Soltero 	\N	mdnunez@ubv.edu.ve 	SECTOR LA FLORESTA CALLE CARVAJAL  MUNICIPIO BOLIVAR  CARIPITO 	2	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
952	13778793	LOZADA VELASQUEZ, LUIS FELIPE	Venezolano 	1978-05-09	\N	\N	Masculino 	Soltero 	\N	llozada@ubv.edu.ve 	VEREDA 21 CASA NUMERO 5 LOS GUARITOS III 	1	INSTRUCTOR	DOCENTE	2008-10-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
953	13804328	BAUTISTA RUIZ, LILIANA ESTHER	Venezolano 	1965-06-12	\N	\N	Femenino 	Soltero 	\N	lbautista@ubv.edu.ve 	CONJUNTO  RESIDENCIAL EL VIADUCTO EDIF. GIRASO, PISO 4V APTO4-1, AV. LAS AMERICAS 	1	ASISTENTE	DOCENTE	2006-04-17	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
954	13811307	DELGADO GONZALEZ, FLORALYS DANIELA	Venezolano 	1979-07-11	\N	\N	Femenino 	Soltero 	\N	fddelgado@ubv.edu.ve 	CALLE SAN LUIS CAS N 11 LOS COLORADOS VILLA DE CURA 	2	INSTRUCTOR	DOCENTE	2005-10-17	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
955	13813171	DEL PINO FEBRES, LAURA CAROLINA	Venezolano 	1978-05-29	\N	\N	Femenino 	Soltero 	\N	lpino@ubv.edu.ve 	URB. MENCA DE LEONI  CALLE ONICE CASA NRO 10 SECTOR PUNTA DE MATA EDO. MONAGAS 	3	AGREGADO	DOCENTE	2007-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
956	13813397	ROMERO CABELLO, ANA DIAMALVER	Venezolano 	1977-05-31	\N	\N	Femenino 	Casado 	\N	aromero@ubv.edu.ve 	QUINTA CALLEJON LA MURALLITA #59 	1	AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
957	13815330	MAZA FERMIN, JESÚS GABRIEL	Venezolano 	1978-05-12	\N	\N	Masculino 	Casado 	\N	jgmaza@ubv.edu.ve 	CALLEJON EL JOBO CASA S/NRO, ARAGUA DE MATURIN 	1	INSTRUCTOR	DOCENTE	2006-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
958	13829440	URDANETA ANEZ, NINOSKA MARIA	Venezolano 	1979-03-06	\N	\N	Femenino 	Soltero 	\N	nmurdaneta@ubv.edu.ve 	URB. LA FLORESTA AV. 88,CASA#79A-84. MARACAIBO EDO ZULIA 	2	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
959	13835533	SALAZAR PEREZ, ANA CAROLINA	Venezolano 	1979-07-25	\N	\N	Femenino 	Soltero 	\N	asalazar@ubv.edu.ve 	URB. CANTARRANA VILLA SAN JOSE CASA CORO DEL VALLE 	2	ASISTENTE	DOCENTE	2004-09-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
960	13837594	SANCHEZ FIGUEROA, YUDITH JOSEFINA	Venezolano 	1978-09-04	\N	\N	Femenino 	Casado 	\N	yjsanchez@ubv.edu.ve 	URB. 1 DE MAYO CALLE 5 CASA #83 	4	AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
961	13870974	GUERRERO AZOCAR, ILKA EDITMAR	Venezolano 	1979-06-06	\N	\N	Femenino 	Soltero 	\N	AEDITMAR@GMAIL.COM 	URB. SOROCAIMA CON CALLE MARIA CASTRO CASA NO 2-2 MUNICIPIO SANTIAGO MARIÑO-MARACAY 	0	INSTRUCTOR	CONTRATADO	2018-10-29	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
962	13884776	ELIAZ GUEVARA, TANIA LIBERTAD	Venezolano 	1977-07-31	\N	\N	Femenino 	Soltero 	\N	teliaz@ubv.edu.ve 	AV.LAS CIENCIAS CALLE RAZETTI RES.ELITE 	1	ASISTENTE	DOCENTE	2007-11-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
963	13911387	LUJANO PINO, LAURA JOSEFINA	Venezolano 	1977-12-08	\N	\N	Femenino 	Soltero 	\N	llujano@ubv.edu.ve 	SECTOR LAS PIEDRITAS VI CALLE LOS ANDES N119 	2	COORDINADOR DE SEDE AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
964	13911608	PARRA PEÑA, ROSALIN MARIA	Venezolano 	1977-06-02	\N	\N	Femenino 	Soltero 	\N	rmparra@ubv.edu.ve 	URB. LOS OLIVOS, CALLE PALERMO M68 #78 PTO. ORDAZ EDO. BOLIVAR. 	1	AGREGADO	DOCENTE	2004-09-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
965	13911919	SANCHEZ FONT, SOFIA DESSIREE	Venezolano 	1977-09-18	\N	\N	Femenino 	Soltero 	\N	sdsanchez@ubv.edu.ve 	URB. VENTURI EDIFICIO N9 PISO N2 	1	ASISTENTE	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1629	9286275	MISEL, ARQUIMEDES	Venezolano 	1960-09-26	\N	\N	Masculino 	Divorciado 	\N	 	CALLE N 10 CASA N 23 	0	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
966	13919238	MORILLO, FELIX	Venezolano 	1978-08-21	\N	\N	Masculino 	Soltero 	\N	fmorillo@ubv.edu.ve 	CALLE IGUALDA NUM. 13, CASCO HISTORICO, CIUDAD BOLIVAR, EDO BOLIVAR 	3	INSTRUCTOR	DOCENTE	2004-09-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
967	13919433	ALVINO MARTINEZ, MARÍA GRACIELA	Venezolano 	1979-04-11	\N	\N	Femenino 	Soltero 	\N	mgalvino@ubv.edu.ve 	URB EL PARAISO 2 CALLE LOS SAMANES RESD.SANTA MARTA CARACAS 	3	ASISTENTE	DOCENTE	2004-09-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
968	13920779	BARRIOS COVA, HEIDI COROMOTO	Venezolano 	1979-10-07	\N	\N	Femenino 	Soltero 	\N	hbarrios@ubv.edu.ve 	URB. BICENTENARIO CALLE 7, CASA N 15 	2	ASISTENTE	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
969	13932815	FARIAS SUAREZ, ADRIANA	Venezolano 	1980-01-06	\N	\N	Femenino 	Soltero 	\N	afarias@ubv.edu.ve 	URB MONTE BELLO 	1	ASISTENTE	DOCENTE	2004-10-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
970	13933441	RODRIGUEZ GARCIA, MILENNYS CRISTINA	Venezolano 	1978-06-17	\N	\N	Femenino 	Soltero 	\N	MILADYRODIGGAR@GMAIL.COM 	LA RINCONADA VIA SANTA ANA SECTOR SAN SALVADOR 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
971	13943290	GONZALEZ HERNANDEZ, JOSE GREGORIO	Venezolano 	1978-12-31	\N	\N	Masculino 	Soltero 	\N	jggonzalez@ubv.edu.ve 	URBANIZACION BUENOS AIRES AV  4 CALLE 5 CASA 4-29 	1	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
972	13945947	PALACIO PAEZ, YURAIMA MARIA	Venezolano 	1979-06-18	\N	\N	Femenino 	Soltero 	\N	ypalacio@ubv.edu.ve 	CARACAS 	2	INSTRUCTOR	DOCENTE	2011-06-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
973	13952311	RENGEL NARANJO, CARLOS ANTONIO	Venezolano 	1979-08-28	\N	\N	Masculino 	Soltero 	\N	crengel@gmail.com 	CATIA AV PPAL EL CUARTEL, VEREDA N8 CASA MARY 	0	COORDINADOR NACIONAL INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
974	13963920	GUTIÉRREZ GONZALEZ, ELIO ANTONIO	Venezolano 	1979-10-27	\N	\N	Masculino 	Casado 	\N	egutierrez@ubv.edu.ve 	CENTRO CALLEJON BARRETO CALLE 23 B 	2	AGREGADO	DOCENTE	2006-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
975	13971327	GOYO OJEDA, NELLYS YESSENIA	Venezolano 	1979-12-02	\N	\N	Femenino 	Soltero 	\N	ngoyo@ubv.edu.ve 	URBANIZACION CANTACLARO SECTOR I CASA N09 	2	INSTRUCTOR	DOCENTE	2007-01-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
976	13981327	SIFONTES MARCANO, DETSY CAROLINA	Venezolano 	1978-12-02	\N	\N	Femenino 	Casado 	\N	dsifontes@ubv.edu.ve 	RESIDENCIA BOLIVAR, PASEO SIMON BOLIVAR, CIUDAD BOLIVAR 	2	AGREGADO	DOCENTE	2004-09-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
977	13985622	AGUILAR MATERAN, YORVIN JOSÉ	Venezolano 	1979-12-18	\N	\N	Masculino 	Casado 	\N	yaguilar@ubv.edu.ve 	URB. VISTA HERMOSA CALLE N 1 CASA N 15 CAMPO ALEGRE 	3	COORDINADOR ACADEMICO AGREGADO	DOCENTE	2006-09-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
978	13986021	LOZADA CEDENO, ALEXIS PASTOR	Venezolano 	1979-11-28	\N	\N	Masculino 	Soltero 	\N	alozada@ubv.edu.ve 	CALLE PRINCIPAL  SAN JOSE  EDF CARDDON I, PISO  4 APTO 44 	0	ASOCIADO	DOCENTE	2004-09-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
979	13987422	RODRIGUEZ DE REIMI, NILSE MIRLEY	Venezolano 	1977-11-11	\N	\N	Femenino 	Casado 	\N	 	URBANIZAION PPOPULAR ATLAS QNTA AVENIDA 90-41 	0	INSTRUCTOR	CONTRATADO	2019-05-31	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
980	14003683	ALVAREZ MORILLO, MARLYN TERESA	Venezolano 	1980-03-23	\N	\N	Femenino 	Soltero 	\N	mmorillo@ubv.edu.ve 	URB CONCOROLIA CALLE PROGRESO HACIA EL TAPONDE LA CUMANA CASA #2 	4	INSTRUCTOR	DOCENTE	2008-11-03	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
981	14009297	GARCIA RUIZ, DAVID JOSÉ	Venezolano 	1978-01-05	\N	\N	Masculino 	Casado 	\N	djgarciar@ubv.edu.ve 	AV.19 DE ABRIL EDF- ANTONUCCI   PISO 3 APTO 9 CIUDAD BOLIVAR 	1	ASISTENTE	DOCENTE	2004-09-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
982	14009556	ESPINOZA GARCIA, ROSALINA ENCARNACION	Venezolano 	1980-08-05	\N	\N	Femenino 	Soltero 	\N	respinoza@ubv.edu.ve 	URB. LAS MARIAS CALLE N 9 MATURIN EDO MONAGAS 	0	ASISTENTE	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
983	14009981	VARGAS RODRIGUEZ, ALENNY DEL CARMEN	Venezolano 	1979-04-04	\N	\N	Femenino 	Casado 	\N	advargas@ubv.edu.ve 	LA SOLEDAD DE SAN ANTONIO DEL GOLFO CALLE CAIGURE CASA S/N 	2	ASISTENTE	DOCENTE	2004-09-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
984	14012294	DIAZ RODRIGUEZ, SANDY JOSE 	Venezolano 	1979-10-18	\N	\N	Masculino 	Soltero 	\N	sdiaz@ubv.edu.ve 	EL SILENCIO CARRERA 5 CASA N 9 SECTOR JUANA RAMIREZ 	3	ASISTENTE	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
985	14012300	GARCIA PARIA, LUIS EDUARDO	Venezolano 	1978-12-13	\N	\N	Masculino 	Soltero 	\N	lgarciap@ubv.edu.ve 	CALLE 13,VEREDA 64, CASA N 03,LOS GODOS SECTOR LAS COMUNALES 	0	INSTRUCTOR	CONTRATADO	2017-01-09	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
986	14013705	GONZALEZ BRITO, DAYANA ROSELYN	Venezolano 	1978-10-16	\N	\N	Femenino 	Divorciado 	\N	drgonzalez@ubv.edu.ve 	RESIDENCIAS EL CASTILLO, TORRE D, PISO 8, APTO 82, AV. LOS PROCERES 	2	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
988	14044186	FLORES HERRERA, MARTHA ELENA	Venezolano 	1980-02-01	\N	\N	Femenino 	Casado 	\N	meflores@ubv.edu.ve 	BARRIO VISTA ALEGRE, CALLEJON VISTA ALEGRE, CASA # 3, CIUDAD BOLIVAR 	3	INSTRUCTOR	DOCENTE	2005-10-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
989	14046903	CARIPE URBINA, MARY LORENA	Venezolano 	1978-09-28	\N	\N	Femenino 	Soltero 	\N	mcaripe@ubv.edu.ve 	EL SILENCIO, CARRETERA 4 C/C 5, CASA #41 	3	ASISTENTE	DOCENTE	2007-10-20	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
990	14065228	GRILLET COA, AURIMAR DE JESÚS	Venezolano 	1980-02-05	\N	\N	Femenino 	Casado 	\N	ajgrillet@ubv.edu.ve 	CARRERA AMAGUA CASA N22 MANZANA N22A URB MANOAS SAN FELIX 	1	AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
991	14074870	PORTACIO RODRIGUEZ, CARLOS ALBERTO	Venezolano 	1979-01-11	\N	\N	Masculino 	Soltero 	\N	cportacio@ubv.edu.ve 	CALLE AREVALO GONZALEZ 	1	COORDINADOR DE SEDE ASOCIADO	DOCENTE	2004-09-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
992	14076694	ABREU LUGO, FREDDY ENRIQUE	Venezolano 	1978-01-20	\N	\N	Masculino 	Soltero 	\N	feabreu@ubv.edu.ve 	SECTOR FUNDEMOS CAICARA CASA # 66 	0	ASISTENTE	DOCENTE	2006-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
993	14090434	TORRES FREITES, YURAIMA AIME	Venezolano 	1977-09-15	\N	\N	Femenino 	Soltero 	\N	yatorresf@ubv.edu.ve 	CAMPO JUNIN, 2DA. CALLE, CASA N 38-B 	1	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
994	14133263	RODRÍGUEZ ORDAZ, DANIEL JOSÉ	Venezolano 	1979-02-24	\N	\N	Masculino 	Casado 	\N	djrodriguezo@ubv.edu.ve 	CALLE CENTURION N 38-3 	2	ASISTENTE	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
995	14136752	ESCALONA CONTRERAS, JACKELINE	Venezolano 	1979-11-08	\N	\N	Femenino 	Soltero 	\N	jescalona@ubv.edu.ve 	BUENA VISTA CALLE 95A CASA N37 	1	ASOCIADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
996	14140713	PARRA MENDOZA, ALBERTO DOMINGO	Venezolano 	1978-01-05	\N	\N	Masculino 	Soltero 	\N	adparra@ubv.edu.ve 	URB. SAN MIGUEL AV. 66 CASA 96A-29 	1	ASISTENTE	DOCENTE	2003-10-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
997	14144120	ROGERT FARFAN, ANA PRAGEDES	Venezolano 	1979-08-05	\N	\N	Femenino 	Casado 	\N	aprogert@ubv.edu.ve 	URB. SIMON BOLIVAR CALLE ZULIA CASA N 02 	3	INSTRUCTOR	DOCENTE	2007-11-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1359	11746149	ROMERO IRAUSQUIN, IRMA JEANETT	Venezolano 	1973-05-06	\N	\N	Femenino 	Soltero 	\N	ijromero@ubv.edu.ve 	SANTA ANA CALLE 03, CASA 42, 	0	INSTRUCTOR	DOCENTE	2012-02-15	DOCENTE MEDIO TIEMPO	ACTIVO
998	14154228	BARCENAS TERAN, ROSSY ALEXANDRA	Venezolano 	1978-08-10	\N	\N	Femenino 	Soltero 	\N	rbarcenas@ubv.edu.ve 	SECTOR CANDELERO, VIA COLONIA MENDOZA 75 	3	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
999	14154492	TERAN MORENO, MARIA  KARLA	Venezolano 	1978-04-09	\N	\N	Femenino 	Soltero 	\N	mteran@ubv.edu.ve 	CARACAS 	2	INSTRUCTOR	DOCENTE	2010-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1000	14158625	GIMENEZ MENDEZ, PABLO EMILIO	Venezolano 	1977-08-19	\N	\N	Masculino 	Soltero 	\N	pegimenez@ubv.edu.ve 	COHCE URBANIZACION DENSIFICACION AV INTERCOMUNAL DE COCHE EDIF TOVAR Y TOVAR, APTO 14-02 	0	COORDINADOR DE SEDE INSTRUCTOR	DOCENTE	2008-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1001	14161348	CHIRINOS PADILLA, DOUGALIZ DAINETTE	Venezolano 	1979-07-02	\N	\N	Femenino 	Casado 	\N	ddchirinos@ubv.edu.ve 	PEDRO MANUEL ARCAYA CALLE 8 CASA 4 	1	INSTRUCTOR	CONTRATADO	2014-09-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1002	14162592	LAMAS, ELENA DAYANARA	Venezolano 	1977-09-12	\N	\N	Femenino 	Soltero 	\N	 	COMUNIDAD 1ERO DE ABRIL CALLE LA VOLUNTAD DE DIOS CASA N 83 SECTOR LA GLORIETA 	0	INSTRUCTOR	CONTRATADO	2019-05-30	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1003	14165785	SANTANDER, JUAN	Venezolano 	1978-08-13	\N	\N	Masculino 	Soltero 	\N	jcsantander@ubv.edu.ve 	BARRIO JOSE FELIX RIVAS, SECTOR #, CXALLE BRISAS DEL TORBES,CALLE BRISAS DELO TORBES, CALLEJONLOS AG 	1	AGREGADO	DOCENTE	2005-10-17	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1004	14168202	CHIRINOS FERRER, ALCIRA DE LA CRUZ	Venezolano 	1979-04-13	\N	\N	Femenino 	Casado 	\N	achirinos@ubv.edu.ve 	URB. LAS MARGARITAS CALLE N 9 CASA N 15 	0	ASISTENTE	DOCENTE	2010-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1005	14173279	LAREZ QUIJADA, ELIUD EFRAIN	Venezolano 	1980-03-06	\N	\N	Masculino 	Soltero 	\N	eelarez@ubv.edu.ve 	SILENCIO DE CAMPO ALEGRE, CALLE 11, CASA N 74 	0	ASISTENTE	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1006	14194270	FERRER DUPUY, CHRISTIANNE MARCELLE	Venezolano 	1978-05-22	\N	\N	Femenino 	Soltero 	\N	cferrer@ubv.edu.ve 	UD7, RES. YAGUARY, PISO 14, APTO 14-04,RUIZ PINEDA CARICUAO 	2	INSTRUCTOR	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1007	14201043	ARRIOJAS ALVAREZ, LISSET JOSEFINA	Venezolano 	1979-12-15	\N	\N	Femenino 	Soltero 	\N	larriojas@ubv.edu.ve 	COCHERA A PESCADOR,TORRE 997. PB-03,SAN JUAN 	0	AGREGADO	DOCENTE	2004-04-19	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1008	14208465	CIFUENTES GIL, NICANOR A	Venezolano 	1978-12-21	\N	\N	Masculino 	Soltero 	\N	ncifuentes@ubv.edu.ve 	URB. SAN JACINTO SECTOR 9 VEREDA 7 CASA 02 	0	COORDINADOR PFG AGREGADO	DOCENTE	2004-10-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1009	14218948	LAYA HERRERA, ROSA DEL CARMEN	Venezolano 	1977-05-22	\N	\N	Femenino 	Soltero 	\N	rdlaya@ubv.edu.ve 	URB. SAN ANTONIO JOSE DE SUCRE CALLE PRINCIPAL CASA N 5. SAN FERNANDO DE APURE 	1	ASISTENTE	DOCENTE	2005-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1010	14223730	ROJAS RIVERO, KEYLA JANETH	Venezolano 	1979-08-15	\N	\N	Femenino 	Casado 	\N	kjrojas@ubv.edu.ve 	C PALERMO MANZA N 20 LOS OLIVOS 	2	AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1011	14231839	RICHARD PARRA, YELITZA BARBARA	Venezolano 	1980-04-29	\N	\N	Femenino 	Casado 	\N	yrichard@ubv.edu.ve 	CALLE RÍO CARONÍ CRUCE CON CALLE LA CASCADA  CASA N40\t\t\t\t  URBANIZACIÓN LA FONTANA  EN LA PROLONG. DE LA AV. ARAGUA 	2	ASISTENTE	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1012	14236391	MOLINA TORRES, HEIDY MASSIEL	Venezolano 	1978-02-23	\N	\N	Femenino 	Soltero 	\N	hmolina@ubv.edu.ve 	URB. TAMARE. SECTOR CARABOBO. CALLE 3 CON AV. 12. CASA 9 	2	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1013	14239297	SIMOSA PALIMA, GABRIELA MARIA	Venezolano 	1981-02-10	\N	\N	Femenino 	Soltero 	\N	gsimosa@ubv.edu.ve 	CALLE VIDAL AGOSTO MENDEZ CASA 8 	2	COORDINADOR PFG INSTRUCTOR	DOCENTE	2011-01-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1014	14253272	LAREZ MARTINEZ, JOSÉ MANUEL	Venezolano 	1979-09-15	\N	\N	Masculino 	Casado 	\N	jlarez@ubv.edu.ve 	URB NEGRO PRIMERO, CALLE 1-C NUMERO 4, MATURIN MONAGAS 	2	AGREGADO	DOCENTE	2007-05-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1015	14271929	RIERA FLORES, MAYRA ALEJANDRA	Venezolano 	1978-09-01	\N	\N	Femenino 	Soltero 	\N	mayrariera@ubv.edu.ve 	BALCONES DE ARAGUANA I, TORRE 16 APTO. 2-C 	2	ASISTENTE	DOCENTE	2007-06-04	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1016	14277254	FERRER TORREALBA, IVAN JOSE	Venezolano 	1979-12-15	\N	\N	Masculino 	Soltero 	\N	iferrer@ubv.edu.ve 	RESIDENCIA LOURDES, AV CRISTOBAL ROJAS CON CALLE 13-BP3 	1	INSTRUCTOR	DOCENTE	2010-11-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1017	14277769	MACHADO TORRES, YNGRI CAROLINA	Venezolano 	1978-08-27	\N	\N	Femenino 	Soltero 	\N	ymachado@ubv.edu.ve 	AV. PRINCIPAL DE PEDRO CAMEJO, SARRIA NO 10. DPTO 1 	2	ASISTENTE	DOCENTE	2004-09-27	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1018	14280222	POLANCO RODRIGUEZ, NAIROBIS DEL CARMEN	Venezolano 	1980-09-16	\N	\N	Femenino 	Soltero 	\N	nairobispolanco@ubv.edu.ve 	AV.MILAGRO SECTOR TEOTISTA CALLE 10 N23/04 	1	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1019	14287710	VIDAL MORALES, MIGUEL ANGEL	Venezolano 	1979-06-23	\N	\N	Masculino 	Soltero 	\N	mvidal@ubv.edu.ve 	BARRIO ANDRES ELOY BLANCO CALLE LOS FLORES CASA N 3 	0	INSTRUCTOR	DOCENTE	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1020	14305532	CORDERO RODRIGUEZ, XIOMELI	Venezolano 	1978-11-18	\N	\N	Femenino 	Soltero 	\N	xcordero@ubv.edu.ve 	CALLE 67 (CECILIO ACOSTA) N 3C-127. 	1	AGREGADO	DOCENTE	2004-10-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1021	14310982	PADRON AGUILAR, MARÍA GRACIELA	Venezolano 	1980-10-08	\N	\N	Femenino 	Soltero 	\N	mpadron@ubv.edu.ve 	AV CUATRICENTENARIO EDF INAVI PISO 4 APTO 04-05 	3	AGREGADO	DOCENTE	2004-04-02	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1022	14323959	BORROME, EMMA JACQUELINE	Venezolano 	1979-06-28	\N	\N	Femenino 	Soltero 	\N	eborrome@ubv.edu.ve 	AV OHIGGINGS RES LOS LEONES PISO 5 APTO N53 	1	INSTRUCTOR	CONTRATADO	2015-03-02	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1023	14339685	SALINA CONTRERA, LIDHEISYS SARAHY	Venezolano 	1978-01-20	\N	\N	Femenino 	Soltero 	\N	 	CALLE MARIÑO EDIF LA MINA PISO 2 APTO 06. 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1024	14357216	UZCATEGUI VELASQUEZ, LILIANA MARIA	Venezolano 	1979-04-09	\N	\N	Femenino 	Soltero 	\N	lmuzcategui@ubv.edu.ve 	SECTOR CANCHANCHA IAVB 15B - 2 CASA 25C-16 	4	AGREGADO	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1025	14358095	MÁRQUEZ CARIPE, FRANCYS DEL VALLE	Venezolano 	1979-04-16	\N	\N	Femenino 	Casado 	\N	fdmarquez@ubv.edu.ve 	LA SABANITA PROLONGACION CALLE COROMOTO QUINTA GILCARDANMARY 	2	INSTRUCTOR	DOCENTE	2007-10-29	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1026	14359442	HERNÁNDEZ VELASQUEZ, ILIANA DEL VALLE	Venezolano 	1979-04-14	\N	\N	Femenino 	Soltero 	\N	ihernandez@ubv.edu.ve 	URB VISTA HERMOSA II BLOQUE 4 APTO 01-04 CIUDAD BOLIVAR 	0	AGREGADO	DOCENTE	2004-09-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1027	14371212	ARIAS MOLINA, DARCI EVELYN	Venezolano 	1981-04-05	\N	\N	Femenino 	Casado 	\N	darias@ubv.edu.ve 	URB. LAS ADJUNTAS, SECTOR COLONIAL, CASA E-01 	3	ASISTENTE	DOCENTE	2005-10-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1028	14382160	ACOSTA BOGADO, WILLMER RAFAEL	Venezolano 	1979-01-08	\N	\N	Masculino 	Soltero 	\N	wacosta@ubv.edu.ve 	FLOR AMARILLO, URB. POPULAR SANTIAGO BETANCOURT INFANTE, CALLE HORA CERO, CASA #06 	2	ASISTENTE	DOCENTE	2007-09-17	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1029	14395438	SEIJAS VERGARA, LISSETH FABIOLA	Venezolano 	1979-09-21	\N	\N	Femenino 	Soltero 	\N	lseijas@ubv.edu.ve 	SAN CASIMIRO SECTOR TORONQUEY CASA 19 	2	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1030	14400318	DAZA HERRERA, LUIS JHONATHAN	Venezolano 	1980-07-13	\N	\N	Masculino 	Soltero 	\N	ldaza@ubv.edu.ve 	AV. CENTENARIO EDIF. MILAGROS CHIQUIN 	0	INSTRUCTOR	DOCENTE	2013-05-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1031	14401674	VALECILLOS UZCATEGUI, NELSON MIGUEL	Venezolano 	1980-01-27	\N	\N	Masculino 	Soltero 	\N	nmvalecillos@ubv.edu.ve 	AV. LOS PROCERES URB.SAN JOSE N.13  SECT. MONT. BE 	0	INSTRUCTOR	DOCENTE	2007-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1032	14407266	CARRILLO REYES, JOHANA CAROLINA	Venezolano 	1980-10-30	\N	\N	Femenino 	Soltero 	\N	jccarrillo@ubv.edu.ve 	UD-4, RESD. EL YAGUAL, EDIF. ACHAGUAS, PISO 09, APTO 0906 	2	ASISTENTE	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1033	14409026	ROJAS REYES, ORANGEL DANIEL	Venezolano 	1981-01-08	\N	\N	Masculino 	Casado 	\N	orojas@ubv.edu.ve 	SECTOR 1 LOS COQUITOS VEREDA N 28 CASA 04 	2	INSTRUCTOR	DOCENTE	2007-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1034	14409315	GUTIÉRREZ, ELIANDRA	Venezolano 	1979-12-21	\N	\N	Femenino 	Casado 	\N	eligutierrez@ubv.edu.ve 	URB. LA PARAGUA SECTOR 5 11-B APARTAMENTO 12 PB 	1	INSTRUCTOR	DOCENTE	2004-03-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1035	14409825	PÉREZ RUEDA, ANA ZUNEIRIS DE JESUS	Venezolano 	1979-12-01	\N	\N	Femenino 	Soltero 	\N	azperez@ubv.edu.ve 	LA PARAGUA SECTOR #5 EDIFICIO 5-6-B APTO. 11 PLANTA BAJA 	3	ASISTENTE	DOCENTE	2004-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1036	14411318	RIVERO PARADA, JOHANNA DEL CARMEN	Venezolano 	1981-03-07	\N	\N	Femenino 	Soltero 	\N	jrivero@ubv.edu.ve 	BARRIO EL CAMPITO CALLE MILANO CALLEJON EL OLVIDO CASA N 15 PETARE ESTADOS MIRANDA 	1	INSTRUCTOR	DOCENTE	2017-10-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1037	14418365	PÉREZ MORENO, ERIKA SUSANA	Venezolano 	1979-06-25	\N	\N	Femenino 	Soltero 	\N	esperez@ubv.edu.ve 	CARRERA N 22 CON CALLE N 7 CASA 7-5 BARRIO JOSE GREGORIO HERNANDEZ 	2	AGREGADO	DOCENTE	2006-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1038	14422425	MARTINEZ TINEO, LETICIA DEL VALLE	Venezolano 	1979-10-22	\N	\N	Femenino 	Soltero 	\N	ldmartinez@ubv.edu.ve 	UBV 	0	INSTRUCTOR	CONTRATADO	2017-10-23	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1039	14423671	ÁVILA SALAZAR, ADRIAN JOSÉ	Venezolano 	1979-03-07	\N	\N	Masculino 	Soltero 	\N	ajavila@ubv.edu.ve 	LA GRAN VICTORIA, ZONA 88, EDIF. B, APARTAMENTO3-2 	2	AGREGADO	DOCENTE	2006-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1040	14423903	MALAVE MOROCOIMA, YURIT JOHANA DEL JESUS	Venezolano 	1979-10-22	\N	\N	Femenino 	Casado 	\N	ymalave@ubv.edu.ve 	URB. VISTA HERMOSA SECTOR A CASA N15 SECTOR CAMPO ALEGRE 	3	ASISTENTE	DOCENTE	2006-03-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1041	14424802	MARIN ZAPATA, ELIOMAR ANTONIO	Venezolano 	1980-08-12	\N	\N	Masculino 	Soltero 	\N	eamarin@ubv.edu.ve 	CARRERA 18B # 27 URB. EL PARAISO MATURIN 	2	AGREGADO	DOCENTE	2004-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1042	14455425	ALVAREZ CARREÑO, MARISELA	Venezolano 	1978-06-28	\N	\N	Femenino 	Soltero 	\N	malvarezc@ubv.edu.ve 	URBANIZACION CIUDAD LOZADA CALLE 09 CASA 12 SECTOR 2 SANTA TERESA DELTUY 	0	INSTRUCTOR	CONTRATADO	2013-02-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1043	14457593	HERNÁNDEZ VALLES, KAREN GABRIELA	Venezolano 	1978-09-19	\N	\N	Femenino 	Soltero 	\N	kghernandez@ubv.edu.ve 	AV.19 LA PALOMA CONJUNTO. RES LAS PIRAMIDES TORRE C PIDO 4.APTO 407 	0	ASISTENTE	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1044	14496973	URDANETA PEREZ, DELIA CELINA	Venezolano 	1980-05-29	\N	\N	Femenino 	Soltero 	\N	durdaneta@ubv.edu.ve 	MCPO. LA CAÑADA DE URDANTA.CALLE 2. CASA S/N 	4	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1045	14526099	FUENTES ALVARADO, YDELYS JOSEFINA	Venezolano 	1979-11-11	\N	\N	Femenino 	Soltero 	\N	yfuentes@ubv.edu.ve 	AV. SAN MARTIN RES. LAS GUACAMAY. TORRE B, PISO 21, APTO.216 	1	INSTRUCTOR	DOCENTE	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1046	14567311	PACHECO SOJO, FRANCIS JOSEFINA	Venezolano 	1979-06-14	\N	\N	Femenino 	Soltero 	\N	fjpacheco@ubv.edu.ve 	EDIF. ZULIA, TORRE A, PISO 10, APTO. 104-A MAIQUETIA 	2	ASISTENTE	DOCENTE	2005-03-28	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1047	14571644	MAITA CHIRINO, PEDRO RAFAEL	Venezolano 	1981-02-09	\N	\N	Masculino 	Soltero 	\N	pmaita@ubv.edu.ve 	CALLE MEREYAL # 14 , SECTOR CHAMBERI-EL TEJERO , 	0	ASISTENTE	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1048	14573936	SEQUERA PAEZ, JOSJANIA PASTORA	Venezolano 	1980-12-20	\N	\N	Femenino 	Soltero 	\N	jpsequera@ubv.edu.ve 	CALLE 20 ENTRE AVENIDA 6 Y 7 GUANARE- EDO. PORTUGUESA 	1	INSTRUCTOR	DOCENTE	2011-01-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1049	14580645	MOYA RODRIGUEZ, HAIDELVIA MARIA	Venezolano 	1980-08-31	\N	\N	Femenino 	Soltero 	\N	hmoya@ubv.edu.ve 	URB. JARDINES DE SAN JAIME CONDOMINIO GERANIO CALLE 3 NRO 16 	1	AGREGADO	DOCENTE	2006-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1050	14599440	ESCALANTE ROJAS, MARÍA ARSENIA	Venezolano 	1980-08-15	\N	\N	Femenino 	Soltero 	\N	maescalante@ubv.edu.ve 	CALLE 83 N 70-24. SECTOR SANTA MARIA. QUINTA CORINA 	1	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1051	14600458	MANZANILLA PARRA, SAMANDA ANGÉLICA	Venezolano 	1980-07-18	\N	\N	Femenino 	Soltero 	\N	smanzanilla@ubv.edu.ve 	URB SANTA FE EDIF ORINOCO APTO 6.2 	1	COORDINADOR NACIONAL ASISTENTE	DOCENTE	2008-03-31	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1052	14628010	ORTIZ  MARCHAN, ANA MARIA	Venezolano 	1979-11-05	\N	\N	Femenino 	Soltero 	\N	aortiz@ubv.edu.ve 	CALLE 6 N 164, 8RB CAMPO CLARO, PERTA MARAVEN 	2	ASISTENTE	DOCENTE	2005-10-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1219	18381334	CHAMORRO PEÑA, MARIA TRINIDAD	Venezolano 	1986-05-05	\N	\N	Femenino 	Soltero 	\N	mchamorro@ubv.edu.ve 	CARACAS 	1	INSTRUCTOR	DOCENTE	2011-01-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1053	14629026	BERMUDEZ VILORIA, YAMILE DEL VALLE	Venezolano 	1980-12-19	\N	\N	Femenino 	Soltero 	\N	ybermudez@ubv.edu.ve 	BARRIO MI ESPERANZA  CALLE 76# 107-199 	0	ASISTENTE	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1054	14651180	URDANETA MACHADO, JENICE CATALINA	Venezolano 	1980-12-15	\N	\N	Femenino 	Soltero 	\N	jcurdaneta@ubv.edu.ve 	SECTOR LA CARMELA 	0	INSTRUCTOR	CONTRATADO	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1055	14651229	ROMERO LLANOS, ELIZABETH CRISTINA	Venezolano 	1979-08-12	\N	\N	Femenino 	Casado 	\N	ecromerol@ubv.edu.ve 	URB LAS QUINTAS, 7MA CALLE CASA # 96A-36 	3	AGREGADO	DOCENTE	2004-03-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1056	14678254	SEIJAS CARRERA, JOSÉ LEONARDO	Venezolano 	1979-02-10	\N	\N	Masculino 	Soltero 	\N	jseijas@ubv.edu.ve 	URB. SAN JUAN CALLE COCHERA TORRE 997 APTO 1-03 	1	INSTRUCTOR	DOCENTE	2005-03-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1057	14697216	COQUIES DIAZ, UNALDO ELIECER	Venezolano 	1980-08-29	\N	\N	Masculino 	Soltero 	\N	ucoquies@ubv.edu.ve 	SECTOR HATO VERDE, CALLE 95 	2	ASOCIADO	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1058	14736434	ALVARADO CANDELARIO, FLORANYEL CAROLIN	Venezolano 	1979-08-24	\N	\N	Femenino 	Soltero 	\N	falvarado@ubv.edu.ve 	CUATRICENTENARIO  95-6#95-30 CASA 95-30 	0	AGREGADO	DOCENTE	2005-04-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1059	14753754	ACOSTA BOGADO, NELKIS JOSEFINA	Venezolano 	1981-08-14	\N	\N	Femenino 	Soltero 	\N	njacostab@ubv.edu.ve 	FLOR DE AMARILLO.URB.POPULAR SANTIAGO BETANCOURT INFANTE. CALLE HORA CERO CASA N  6 	2	INSTRUCTOR	DOCENTE	2006-11-13	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1060	14761349	MENDOZA HERNADEZ, JHOE LUIS	Venezolano 	1981-10-19	\N	\N	Masculino 	Casado 	\N	JHOEMENDOZAH@GMAIL.COM 	URBANIZACION LA PAEZ, SECTOR 1, VEREDA N40 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1061	14762078	RAMIREZ MORA, YUNEY CAROLINA	Venezolano 	1979-09-13	\N	\N	Femenino 	Soltero 	\N	yramirez@ubv.edu.ve 	URB CARABOBO CALLE PRINCIPAL VEREDA 9 CASA 28 	1	INSTRUCTOR	DOCENTE	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1062	14769011	ESCOBAR LIENDO, ANGEL CUSTODIO	Venezolano 	1980-12-12	\N	\N	Masculino 	Soltero 	\N	angelcel29@hotmail.com 	AV.SOUBLETTE SECTOR EL CANTON CALLE ATRAS DEL EDIF.TAUREL MAIQUETIA 	0	INSTRUCTOR	CONTRATADO	2018-01-22	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1063	14785901	HERNANDEZ DIAZ, NATHALY	Venezolano 	1981-11-29	\N	\N	Femenino 	Soltero 	\N	nhernandez@ubv.edu.ve 	PIRINEAS N 1 LOTE A VEREDA N 22 CASA # 14 	0	INSTRUCTOR	DOCENTE	2011-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1064	14797202	ULRRICHE RAMOS, MARIOTTZI GEOMAR	Venezolano 	1979-12-27	\N	\N	Femenino 	Soltero 	\N	mulrriche@ubv.edu.ve 	URBANIZACION LAS ACEQUIAS EDF  BLOQUE 13 APTO 01-04 	3	ASISTENTE	DOCENTE	2006-07-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1065	14840186	LAZARDE AMAIZ, HENRY  CRISTIAM	Venezolano 	1980-06-18	\N	\N	Masculino 	Soltero 	\N	hlazarde@ubv.edu.ve 	CALLE SAN MIGUEL SANTA ANA DEL NORTE 	1	INSTRUCTOR	DOCENTE	2010-10-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1066	14859737	VALDEZ CEBALLOS, KARYMAR	Venezolano 	1979-09-30	\N	\N	Femenino 	Casado 	\N	kvaldez@ubv.edu.ve 	URB. VILLAS MORICHAL CASA N 41 SECTOR TIPURO 	0	ASISTENTE	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1067	14873263	PERNIA MONCADA, JESSICA	Venezolano 	1980-05-11	\N	\N	Femenino 	Soltero 	\N	jmpernia@ubv.edu.ve 	BARRIO PUEBLO NUEVO PASAJE ALBARREGAS CASA 1-74 	0	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1068	14883297	HURTADO, JULIO	Venezolano 	1981-02-27	\N	\N	Masculino 	Soltero 	\N	jhurtado@ubv.edu.ve 	CALLE NICARAGUA 	1	INSTRUCTOR	DOCENTE	2005-10-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1069	14919920	SEQUERA CAMACHO, LOLIMAR ANDREINA	Venezolano 	1980-11-17	\N	\N	Femenino 	Soltero 	\N	lsequera@ubv.edu.ve 	BARRIO CENTRO AV. 9 ENTRE CALLES 15 Y 16 CASA #16-14 	1	ASISTENTE	DOCENTE	2006-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1070	14959641	CARIDAD ARANA, ERIKA JOHANA	Venezolano 	1982-04-23	\N	\N	Femenino 	Soltero 	\N	ecaridad@ubv.edu.ve 	SECTOR 12 VEREDA 16 BLOQUE 17 PISO 3 APTO 0304 	2	AUXILIAR I	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1071	14968965	FORTIQUE, JOSÉ	Venezolano 	1980-11-15	\N	\N	Masculino 	Soltero 	\N	jfortique@ubv.edu.ve 	VISTA HERMOSA II BLOQUE 4 APTO. 0102 	0	COORDINADOR ACADEMICO AGREGADO	DOCENTE	2004-09-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1072	14982430	ORTIZ RAMIREZ, NINOSKA DARLYSBETH	Venezolano 	1980-05-15	\N	\N	Femenino 	Soltero 	\N	ndortiz@ubv.edu.ve 	UBR LOS RUICES CALLE B EDIF SAN FRANCISCO PISO 2 APTO 21 B 	1	AGREGADO	DOCENTE	2004-04-19	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1073	14993095	RENGEL ALGUACA, JUDITH DEL VALLE	Venezolano 	1982-01-11	\N	\N	Femenino 	Soltero 	\N	ydrengel@ubv.edu.ve 	LA PUENTE SECTOR LA CAÑADA CALLE N 3 CALLA N 11 MATURIN MONAGAS 	1	ASISTENTE	DOCENTE	2006-06-30	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1074	14998104	SANCHEZ ARTEAGA, TRINO JOSE	Venezolano 	1980-06-18	\N	\N	Masculino 	Soltero 	\N	trinos@ubv.edu.ve 	SECTOR PUEBLO NUEVO CALLE N 7 	1	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1075	15001789	GONZALEZ ARREDONDO, RAQUEL ANAYS 	Venezolano 	1980-12-31	\N	\N	Femenino 	Soltero 	\N	ragonzaleza@ubv.edu.ve 	AV UPATA N 12 	1	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1076	15012272	CABRERA COLMENARES, RAFAEL JOSÉ	Venezolano 	1979-12-14	\N	\N	Masculino 	Soltero 	\N	rcabrera@ubv.edu.ve 	CALLE 38 ANTES AV, EL MILAGRO, # 42-176 MARACAIBO 	0	ASISTENTE	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1077	15012533	ORTEGA MORENO, JEAN JOSE	Venezolano 	1982-04-04	\N	\N	Masculino 	Soltero 	\N	jjortega@ubv.edu.ve 	MARACAIBO 	3	INSTRUCTOR	DOCENTE	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1078	15013679	RONDÓN ROCA, MIGUEL ANGEL	Venezolano 	1979-05-27	\N	\N	Masculino 	Soltero 	\N	marondonr@ubv.edu.ve 	CARACAS 	3	ASISTENTE	DOCENTE	2005-04-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1079	15073565	DÁVILA VARELA, ROSEMARY DEL CARMEN	Venezolano 	1978-08-16	\N	\N	Femenino 	Soltero 	\N	rdavila@ubv.edu.ve 	EL TIGRE CALLE 3-13 	0	AGREGADO	DOCENTE	2006-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1080	15080174	CACERES HERNANDEZ, MIGUEL EDUARDO	Venezolano 	1981-08-13	\N	\N	Masculino 	Soltero 	\N	miguelcaceres1@hotmail.com 	CALLE 11, RESD. VERSALLES, PISO 5, APT 1, LOS JARDINES DEL VALLE 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1081	15095368	BERMUDEZ CHIRINOS, FRANKLIN ALEXANDER	Venezolano 	1979-12-04	\N	\N	Masculino 	Soltero 	\N	fabermudezc@ubv.edu.ve 	CALLE ZAMORA URB. SANTA IRENE 	0	ASISTENTE	DOCENTE	2005-10-24	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1082	15116232	ARAY APAEZ, NOHEMI  GABRIELA	Venezolano 	1981-09-20	\N	\N	Femenino 	Casado 	\N	naray@ubv.edu.ve 	URB. AVES DEL PARAISO CALLE N 10 CASA N 	1	INSTRUCTOR	DOCENTE	2011-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1083	15145442	AMARO LEON, VALENTIN ANTONIO	Venezolano 	1982-11-13	\N	\N	Masculino 	Soltero 	\N	vamaro@ubv.edu.ve 	URBANIZACIÓN ROSA INES TERRAZA 2 N47 	2	ASISTENTE	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1084	15186395	JIMÉNEZ, NEYLA	Venezolano 	1980-08-21	\N	\N	Femenino 	Soltero 	\N	njimenez@ubv.edu.ve 	URB. UNARE II EDIFICIO 17 APTO 02-06 	2	INSTRUCTOR	DOCENTE	2005-10-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1085	15204653	CORDERO HUIZI, AMARILIS JOSEFINA	Venezolano 	1977-05-28	\N	\N	Femenino 	Soltero 	\N	amaj15204@gmail.com 	URB ALI PRIMERA CALLE PRINCIPAL CASA 1 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1086	15223533	GUAIMARA RONDON, SAUL ELIAS	Venezolano 	1981-06-24	\N	\N	Masculino 	Soltero 	\N	sguaimara@ubv.edu.ve 	RESIDENCIAS MEDITERRANEO APTO, A-2-11 CHARALLAVE 	0	INSTRUCTOR	CONTRATADO	2016-01-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1087	15223729	LINARES ARIAS, JESUS ALBERTO	Venezolano 	1981-11-13	\N	\N	Masculino 	Soltero 	\N	jesuslin13@gmail.com 	RESIDENCIA EL BOSQUE  TORRE A PISO 1 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1088	15239846	MENDEZ FUENMAYOR, FERNANDA HAYBER	Venezolano 	1982-11-10	\N	\N	Femenino 	Soltero 	\N	fhmendez@ubv.edu.ve 	URB. NUEVA MIRANDA, CALLE 4, VEREDA 9, CASA N 5 	2	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1089	15241332	MENDOZA MENDOZA, SOLANGE CELESTE	Venezolano 	1983-03-26	\N	\N	Femenino 	Soltero 	\N	smendoza@ubv.edu.ve 	URB SAN SEBASTIAN AV 1 N 81 	1	INSTRUCTOR	DOCENTE	2010-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1090	15252364	YEPEZ RONDÓN, REINALDO ANTONIO	Venezolano 	1982-09-08	\N	\N	Masculino 	Soltero 	\N	ryepez@ubv.edu.ve 	URBANIZACION BLOQUES DE LA PARAGUA SECTOR N1 EDIFICIO N1-1B APTO N 42 	2	ASISTENTE	DOCENTE	2007-10-29	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1091	15260201	REYES VILORIA, MAYRA ALEJANDRA	Venezolano 	1981-02-03	\N	\N	Femenino 	Soltero 	\N	mareyes@ubv.edu.ve 	CARACAS 	0	AGREGADO	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1092	15270968	JIMÉNEZ ALVAREZ, JOSÉ DEL CARMEN	Venezolano 	1979-07-15	\N	\N	Masculino 	Soltero 	\N	jjimeneza@ubv.edu.ve 	URBANIZACION SIMON BOLIVAR AV. P CHUPACHUPA CASA NUM. 1 	1	ASISTENTE	DOCENTE	2007-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1093	15276560	SILVA SALAZAR, LILIAN CONCHITA	Venezolano 	1979-02-05	\N	\N	Femenino 	Soltero 	\N	lsilva@ubv.edu.ve 	AV. BALMORE NAGUANAGUA, VALENCIA ESTADO CARABOBO 	1	ASISTENTE	DOCENTE	2004-04-19	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1567	4437276	GIL VARGAS, VICTOR WILFREDO	Venezolano 	1956-08-18	\N	\N	Masculino 	Soltero 	\N	 	CARACAS 	0	INSTRUCTOR	DOCENTE	2009-05-04	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1094	15287405	GÓMEZ ECHEVERRIA, CIARA REBECA	Venezolano 	1982-12-30	\N	\N	Femenino 	Soltero 	\N	crgomez@ubv.edu.ve 	URB. PEDRO MANUEL AROYO II ETAPA CALLE 6 MANZANA I CASA I-2 	2	COORDINADOR DE SEDE AGREGADO	DOCENTE	2005-05-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1095	15288629	ALZOLAR MONTEVERDE, MARIAMELYS 	Venezolano 	1981-12-09	\N	\N	Femenino 	Soltero 	\N	maalzolar@ubv.edu.ve 	URB. BEBEDERO, VEREDA 24 CASA No. 01 SUCRE 	0	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1096	15347295	BELISARIO DAMAS, ZULAY MILAGROS	Venezolano 	1982-08-06	\N	\N	Femenino 	Soltero 	\N	zbelisario@ubv.edu.ve 	URB. SIMON BOLIVAR CALLE GONZALO BARRIOS CASA NO 10-1 	1	ASISTENTE	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1097	15348904	FUENTES PEREZ, MARÍA TAMARA	Venezolano 	1981-02-23	\N	\N	Femenino 	Soltero 	\N	mtfuentes@ubv.edu.ve 	URB. LA PARAGUA LOQUE 4-9-B APTTO N 42 	2	ASISTENTE	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1098	15362150	RODRIGUEZ LUNA, MANUEL FRANCISCO	Venezolano 	1981-03-06	\N	\N	Masculino 	Casado 	\N	mfrodriguez@ubv.edu.ve 	LAS DELICIAS CARRERA 3, ENTRE CALLES 2 Y 3 	1	INSTRUCTOR	DOCENTE	2017-10-23	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1100	15397363	BUSTAMANTE, BLANCA	Venezolano 	2018-06-18	\N	\N	Femenino 	Soltero 	\N	desarrollocarabobo2013@gmail.com 	CALLE 3 CASA MANZANA 3 N URB CIUDADELA TACARIGUA CARABOBO 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1101	15411976	ARRIETA NAVA, DUILIMARTH	Venezolano 	1981-08-07	\N	\N	Femenino 	Soltero 	\N	darrieta@ubv.edu.ve 	URB. SAN JACINTO SECTOR 7 TRANSVERSAL 7 CASA NRO.12 	3	ASISTENTE	DOCENTE	2004-10-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1102	15414194	ACOSTA ALFONZO, NATHALI DEL MAR	Venezolano 	1981-01-04	\N	\N	Femenino 	Casado 	\N	nacosta@ubv.edu.ve 	URB. LA MURALLITA, CALLE SAN ROMAN. CASA 5 	2	INSTRUCTOR	DOCENTE	2007-05-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1103	15429629	AZOCAR SALAZAR, DANIEL BAUTISTA	Venezolano 	1976-12-27	\N	\N	Masculino 	Soltero 	\N	dazocar@ubv.edu.ve 	CARIPITO, ZAMURO AFUERA, CALLE SAN SIMON CASA S/N 	1	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1104	15525058	GÓMEZ ARVELO, YUSVELYS DEL CARMEN	Venezolano 	1979-08-02	\N	\N	Femenino 	Soltero 	\N	ydgomez@ubv.edu.ve 	B/GAITERO CALLE 130 CASA Nº71-103 	0	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1105	15525703	VILLALOBOS FERRER, EURY JOSÉ	Venezolano 	1980-08-04	\N	\N	Masculino 	Soltero 	\N	ejvillalobos@ubv.edu.ve 	SECTOR  PARAISO NORTE AV 123 #77A-291 PARROQUIA VENANCIIO PULGAR 	0	AGREGADO	DOCENTE	2004-10-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1106	15544268	AREVALO POUCHOULO, GABRIELA ANASTASIA	Venezolano 	1982-05-20	\N	\N	Femenino 	Soltero 	\N	garevalo@ubv.edu.ve 	CARACAS 	1	INSTRUCTOR	DOCENTE	2010-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1107	15559387	FUENMAYOR BENAVIDES, MERUSKA DEL CARMEN	Venezolano 	1982-03-02	\N	\N	Femenino 	Soltero 	\N	medfuenmayorb@ubv.edu.ve 	SECTOR CARMELO URDANETAAV.105 CALLE 73 CASA 104-A-79 	0	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1108	15586589	QUINTERO APONTE, ZULEIMA DEL CARMEN	Venezolano 	1981-02-21	\N	\N	Femenino 	Soltero 	\N	zquintero@ubv.edu.ve 	AV.SUCRE, EDIF 99.PISO 2. APTO 4. ALTA VISTA- CATIA 	1	INSTRUCTOR	CONTRATADO	2014-05-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1109	15592627	BARRIOS FLORES, BEATRIZ ADRIANA	Venezolano 	1982-12-23	\N	\N	Femenino 	Soltero 	\N	bbarrios@ubv.edu.ve 	CALLE PRINCIPAL CASA #04 DIAGONAL AL COLEGIO 	2	COORDINADOR NACIONAL AGREGADO	DOCENTE	2004-09-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1110	15593736	REYES HERNANDEZ, EILYNS VANESSA	Venezolano 	1982-01-21	\N	\N	Femenino 	Soltero 	\N	evreyes@ubv.edu.ve 	URB.ANTIGUO AEROPUERTO STR 4 CLL, 13 VDA 18 CASA 1 	0	AGREGADO	DOCENTE	2005-10-24	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1111	15597513	ROJAS HERNANDEZ, MÓNICA DEL VALLE	Venezolano 	1981-05-09	\N	\N	Femenino 	Soltero 	\N	mdrojas@ubv.edu.ve 	URB LOS CARDONES RES MANANTIAL TORRE E PISO 6 APTO 61 	2	INSTRUCTOR	DOCENTE	2008-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1112	15606589	FLORES GEANT, FEDORA AIMARA	Venezolano 	1981-09-24	\N	\N	Femenino 	Casado 	\N	faflores@ubv.edu.ve 	URB LA PONDEROSA RES LAS FLORES EDIF AMAPOLA PISO 5 APTO 52-A 	1	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1113	15617935	AULAR BASANTA, JOLEIDY JAQUELIN	Venezolano 	1981-09-22	\N	\N	Femenino 	Soltero 	\N	jaular@ubv.edu.ve 	BLOQUES DE LA PARAGUA, SECTOR 3. EDIF. 3-9-A APTO. 32 CIUDAD BOLIVAR 	1	ASISTENTE	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1114	15624520	MATOS MOSQUERA, MARÍA GABRIELA	Venezolano 	1983-03-09	\N	\N	Femenino 	Soltero 	\N	mgmatos@ubv.edu.ve 	AV LA LIMPIA CALLE 79 QUINTA CEMABEL N|40 -127 SECTOR PANAMERICANO 	0	ASISTENTE	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1115	15633136	GOMEZ FONSECA, DANETTE COROMOTO	Venezolano 	1983-01-09	\N	\N	Femenino 	Casado 	\N	dgomez@ubv.edu.ve 	CARRERA 3 N 122 ANTIGUA AVENIDA RIVAS SECTOR MERCADO VIEJO 	3	INSTRUCTOR	DOCENTE	2011-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1116	15637629	CORASPE ORTIZ, IXCHEL AMARU	Venezolano 	1981-10-08	\N	\N	Femenino 	Soltero 	\N	icoraspe@ubv.edu.ve 	URB EL PERU SECTOR 5 USA 03 VEREDA 6 	1	ASISTENTE	DOCENTE	2007-01-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1117	15700445	ESCALONA, JOSE RAMON	Venezolano 	1982-08-25	\N	\N	Masculino 	Soltero 	\N	jrescalona@ubv.edu.ve 	CALLE 18, LOS JARDINES DEL VALLE 	1	INSTRUCTOR	CONTRATADO	2016-01-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1118	15726629	PRIETO MOLINA, VANESSA BEATRIZ	Venezolano 	1981-06-18	\N	\N	Femenino 	Soltero 	\N	vprieto@ubv.edu.ve 	CALLE 72 CON AV.10 RESD, CLARET TORRE 2 , PISO 8 	0	AGREGADO	DOCENTE	2005-11-14	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1119	15736056	DIAZ LUCENA, CESAR AUGUSTO	Venezolano 	1982-07-02	\N	\N	Masculino 	Soltero 	\N	cdiaz@ubv.edu.ve 	FINAL CALLE LARA N 138-1 SECTOR AGUAS CALIENTES MARIARA 	1	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1120	15758491	MARTINEZ, YULI NAKARI	Venezolano 	1981-02-10	\N	\N	Femenino 	Soltero 	\N	ymartinez@ubv.edu.ve 	CARACAS 	3	INSTRUCTOR	DOCENTE	2010-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1121	15766734	FUENMAYOR BOLAÑO, MAIDELIN BEATRIZ	Venezolano 	1983-06-26	\N	\N	Femenino 	Soltero 	\N	mbfuenmayor@ubv.edu.ve 	B/ GUANIPA MATOS AV. 101 A 58-48 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1122	15790227	MOYA GOMEZ, JESUS RAFAEL	Venezolano 	1982-09-21	\N	\N	Masculino 	Soltero 	\N	 	CALLE DELTA N 42 SECTOR EL CENTRO DE LA CIUDAD 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1123	15800553	ANAYA SALAS, JEANPIER JOSE	Venezolano 	1983-05-01	\N	\N	Masculino 	Soltero 	\N	 	UBV 	0	COORDINADOR NACIONAL INSTRUCTOR	CONTRATADO	2017-10-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1124	15814172	LÓPEZ MENESES, ARLENYS DEL VALLE	Venezolano 	1982-08-24	\N	\N	Femenino 	Soltero 	\N	avlopez@ubv.edu.ve 	URB. #2 LOS GODOS, CALLE PRINCIPAL, VEREDA 66, RESIDENCIA #66 	2	INSTRUCTOR	DOCENTE	2007-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1125	15818764	OROPEZA ALAHES, FANNY ESTHER	Venezolano 	1983-09-05	\N	\N	Femenino 	Casado 	\N	foropeza@ubv.edu.ve 	SECTOR PEDRO VILLA CASTIN CALLE LAS FLORES CASA NÚMERO 08 	2	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1126	15828338	BASTIDAS MEJIAS, JAKLIN SILENI	Venezolano 	1980-04-03	\N	\N	Femenino 	Soltero 	\N	jsbastidasm@ubv.edu.ve 	CALLE ANDRES ELOY BLANCO CASA N 47 BARRIO BELTRAN LUCENA 	2	AGREGADO	DOCENTE	2006-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1128	15888773	VILLALOBOS DAVILA, JENNYCET CAROLISKA	Venezolano 	1982-01-10	\N	\N	Femenino 	Soltero 	\N	jvillalobos@ubv.edu.ve 	Urb. Rancho Grande, Av. Bolivar cruce con calle 36, Residencias Orinoco, Apartamento 1-1 	3	INSTRUCTOR	DOCENTE	2010-08-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1129	15895479	GONZALEZ CARREÑO, MARIA DE LOS ANGELES	Venezolano 	1983-05-01	\N	\N	Femenino 	Divorciado 	\N	MARYANGELS51@GMAIL.COM 	CALLE LAS MARGARITAS SECTOR CONEJEROS 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1130	15904310	LIMPIO PEÑALVER, YOLANDA DE JESUS 	Venezolano 	1982-09-04	\N	\N	Femenino 	Soltero 	\N	ylimpio@ubv.edu.ve 	SECTOR CAMPO AYACUCHO CALLE 20 CASA 2 	3	INSTRUCTOR	CONTRATADO	2014-02-17	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1131	15904566	MAESTRE, MARIA ELENA	Venezolano 	1980-03-10	\N	\N	Femenino 	Soltero 	\N	memaestre@ubv.edu.ve 	CARRERA 5 N 35 SECTOR EL PARAISO 	2	INSTRUCTOR	CONTRATADO	2015-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1132	15904575	MORALES ALARCON, JOHANNA ALEJANDRINA	Venezolano 	1981-11-04	\N	\N	Femenino 	Soltero 	\N	 	GUARIJOS III CANAL 90 VEREDA 33 CASA N 19 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1133	15964275	LOZADA CEDEÑO, ALEXANDER JAVIER	Venezolano 	1983-11-26	\N	\N	Masculino 	Soltero 	\N	ajlozada@ubv.edu.ve 	FALCON 	2	ASISTENTE	DOCENTE	2011-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1134	15972876	HERNANDEZ RAMIREZ, FRANCYS JOHANNA	Venezolano 	1984-08-26	\N	\N	Femenino 	Soltero 	\N	fjhernandez@ubv.edu.ve 	CALLE PAEZ CASA N25 LA SABANITA CUIDAD BOLIVAR 	0	INSTRUCTOR	DOCENTE	2012-09-29	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1135	15977142	CALDERON SECO, RODOLFO JOSE	Venezolano 	1982-05-26	\N	\N	Masculino 	Soltero 	\N	rcalderon@ubv.edu.ve 	URB. POPULAR LIBERTADOR, CALLE 102, CASA N24 	0	ASISTENTE	DOCENTE	2011-06-09	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1136	15989085	SANCHEZ CARRERO, ANGIE CAROLINA	Venezolano 	1981-08-08	\N	\N	Femenino 	Soltero 	\N	acsanchez@ubv.edu.ve 	CALLE 8  CASA #1-39 	0	ASISTENTE	DOCENTE	2006-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1137	16032053	VARGAS NAVARRO, MARINELLA BETZABETH	Venezolano 	1981-05-27	\N	\N	Femenino 	Soltero 	\N	mvargas@ubv.edu.ve 	SEGUINDA CALLE DE BELLO MONTE CON AV. ORINOCO BELLON MONTE TORRE ALFA PISO 16 	0	AGREGADO	DOCENTE	2005-02-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1138	16037258	RAMIREZ MILANO, EFREN RODERICK	Venezolano 	1982-08-24	\N	\N	Masculino 	Soltero 	\N	 	URBANIZACION VILLA SAN ANTONIO SECTOR C CALLE PRINCIPAL CASA 018 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1139	16053016	TRIVIÑO YANNUZZI, RIGGER ALEJANDRO	Venezolano 	1983-10-20	\N	\N	Masculino 	Soltero 	\N	rtrivino@ubv.edu.ve 	URB.VALLES DE CHARA, AV.SAN NICOLAS DE BARI, VALLE FRESCO. TORRE E, APARTAMENTO 41E 	0	INSTRUCTOR	CONTRATADO	2017-01-09	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1140	16104852	NAVARRO COLINA, OSMERY JOSEFINA	Venezolano 	1984-09-28	\N	\N	Femenino 	Soltero 	\N	onavarro@ubv.edu.ve 	CALLE DEMOCRACIA SECTOR LA ROSA I. CASA N# 3-A BELLA VISTA. PUNTO FIJO 	1	ASISTENTE	DOCENTE	2007-06-04	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1141	16118027	TAPIAS SANCHEZ, EGDIMAR ALEXANDRA	Venezolano 	1982-09-08	\N	\N	Femenino 	Soltero 	\N	eatapias@ubv.edu.ve 	LA POMANA RESIDENCIA LAS PIRAMEDAS TORRE C PISO 6 MARACAIBO-ZULIA 	1	INSTRUCTOR	DOCENTE	2003-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1142	16122591	NIETO MENDEZ, EDGAR MIGUEL	Venezolano 	1982-05-19	\N	\N	Masculino 	Casado 	\N	enieto@ubv.edu.ve 	CALLE CONCORDIA N 152 CASCO HISTORICO 	0	ASISTENTE	DOCENTE	2005-10-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1143	16151812	MAYOR MORENO, YURIS MARGARITA	Venezolano 	1981-10-08	\N	\N	Femenino 	Soltero 	\N	ymmayorm@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2010-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1144	16175436	SULEIMAN ELIAS , ELIA	Venezolano 	1983-10-05	\N	\N	Masculino 	Soltero 	\N	esuleiman@ubv.edu.ve 	AV ORINOCO EDF BASHOUR PISO 2 APTO 2A 	2	INSTRUCTOR	CONTRATADO	2014-02-17	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1145	16187763	PRIETO MORAN, MARIA CAROLINA	Venezolano 	1982-07-07	\N	\N	Femenino 	Soltero 	\N	mcprietom@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2009-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1146	16191503	URBINA CASTILLO, DAGNY DANIEL DE LA TRINIDAD	Venezolano 	1982-12-11	\N	\N	Masculino 	Soltero 	\N	durbina@ubv.edu.ve 	BARRIO SAN JOSE AVENIDA SAN JOAQUIN CASA 13-79 	2	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1147	16191652	GUERRA RODRIGUEZ, JUAN CARLOS	Venezolano 	1984-05-25	\N	\N	Masculino 	Soltero 	\N	jcguerra@ubv.edu.ve 	URB. RAUL LEONI SECTOR 01 VEREDA 7 CASA N 15 	2	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1148	16199391	LENZO, JUAN CARLOS	Venezolano 	1983-03-20	\N	\N	Masculino 	Soltero 	\N	jlenzo@ubv.edu.ve 	BARRIO PUEBLO NUEVO PASALE 1 	0	INSTRUCTOR	CONTRATADO	2017-01-09	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1149	16199571	GARCIA LOBO, JUNIOR ORLANDO	Venezolano 	1982-07-26	\N	\N	Masculino 	Soltero 	\N	jogarcial@ubv.edu.ve 	AVENIDA ANDRES BELLO, SECTOR PIE DEL LLANO, CASA 54-172, PARROQUIA DOMINGO PEÑA MUNICIPIO LIBERTADOR MERIDA 	0	INSTRUCTOR	DOCENTE	2012-07-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1150	16210032	LAVADO RUIZ, CARLOS IVAN	Venezolano 	1983-10-29	\N	\N	Masculino 	Soltero 	\N	cilavado@ubv.edu.ve 	AV.LA LIMPIA CALLE 79, EDIF 52-26 MJR APTO.2-A AL LADO DE BANESCO 	0	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1151	16222214	ROJAS FIGUERA, RENNY RAFAEL	Venezolano 	1984-06-08	\N	\N	Masculino 	Soltero 	\N	rrrojas@ubv.edu.ve 	RUIZ PINEDA CASA N 11 BARRIO LIBERTADOR 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1152	16400259	ALVAREZ RIVERO, PEDRO ANTONIO	Venezolano 	1981-04-16	\N	\N	Masculino 	Soltero 	\N	palvarez@ubv.edu.ve 	URB CARAQUITA AV PRINCIPAL GUIGUIE VALENCIA CRUCE CON CALLE SUCRE 	1	INSTRUCTOR	DOCENTE	2011-01-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1153	16424063	MARTINEZ DURAN, GLENDYS ELISSA	Venezolano 	1982-12-21	\N	\N	Femenino 	Soltero 	\N	gmartinez@ubv.edu.ve 	URBANIZACION SAMANES I CALLE EL BAUL C/C MANRIQUE CASA N 15 	2	INSTRUCTOR	DOCENTE	2011-01-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1154	16474242	CARRILLO MORILLO, BELKIS YAMILETH	Venezolano 	1984-02-16	\N	\N	Femenino 	Soltero 	\N	bcarrillo@ubv.edu.ve 	EL RECREO CALLE PRINCIPAL SECTOR 7 DE SEPTIEMBRE 	2	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1155	16513698	AL HAJALI UZCATEGUI, YSHAMDRA JAQUELINE	Venezolano 	1983-05-13	\N	\N	Femenino 	Soltero 	\N	yalhajali@ubv.edu.ve 	CARRETERA 6 CRUCE CON CALLE 32 N# 32-10, ESQ, CALLEJON LOS SUPERLANO, BARINITAS, EDO, BARINAS. 	1	AGREGADO	DOCENTE	2006-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1156	16518331	ORTEGA PEREIRA, YOXIMAR DEL VALLE	Venezolano 	1983-03-16	\N	\N	Femenino 	Casado 	\N	yortega@ubv.edu.ve 	LA CRUZ CALLE PRINCIPAL CASA N 45 	1	INSTRUCTOR	DOCENTE	2010-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1157	16545193	MARVAL, MARIENNY DEL VALLE 	Venezolano 	1985-04-21	\N	\N	Femenino 	Soltero 	\N	mmarval@ubv.edu.ve 	URB. EL HATICO, AV JUAN BAUTISTA ARISMENDI,CALLE PRINCIPAL. 	0	INSTRUCTOR	CONTRATADO	2015-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1158	16609576	CASANOVA BORGAS, ERIKA	Venezolano 	1981-11-12	\N	\N	Femenino 	Soltero 	\N	ercasanova@ubv.edu.ve 	AV. 28 LA LIMPIA SECTOR GRANO DE ORO CASA # 57-95 MARACAIBO-ZULIA 	2	ASOCIADO	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1159	16627466	VALDERREY  BONILLO, SONIA KARINA 	Venezolano 	1985-04-04	\N	\N	Femenino 	Soltero 	\N	svalderrey@ubv.edu.ve 	URB LA GRAN VICTORIA ZONA 15 BLOQUE A APTO 2-2 	2	INSTRUCTOR	CONTRATADO	2014-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1161	16685443	FEREIRA PLASENCIA, DAYANA ALEXANDRA	Venezolano 	1984-04-10	\N	\N	Femenino 	Soltero 	\N	dfereira@ubv.edu.ve 	Urbanización Parque Aragua Edf Sauce, Piso 3, Apto 03-01 	1	INSTRUCTOR	DOCENTE	2010-11-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1162	16693331	ORTEGA VOLCAN, MERY GABRIELA	Venezolano 	1984-06-05	\N	\N	Femenino 	Casado 	\N	mgortega@ubv.edu.ve 	PARROQUIA SANTA TERESA BARRIO MACA CALLE AREVALO GONZÁLEZ PETARE 	3	INSTRUCTOR	DOCENTE	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1163	16704145	ANGULO SIMOZA, YELITZA COROMOTO	Venezolano 	1984-06-21	\N	\N	Femenino 	Soltero 	\N	ycangulo@ubv.edu.ve 	AV PRINCIPAL DE JOSÉ FELIX RIBAS ZONA 6 CASA N 27-A PETARE 	3	ASISTENTE	DOCENTE	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1164	16730479	ALVILLAR SANCHEZ, PATRICIA YERALDINA	Venezolano 	1982-10-13	\N	\N	Femenino 	Soltero 	\N	pyalvillar@ubv.edu.ve 	SECTOR RAFAEL URDANETA AV. 80 NRO. 65A-32 	0	ASISTENTE	DOCENTE	2006-11-20	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1165	16730950	BAPTISTA VALERO, ENDER JOSE	Venezolano 	1984-02-25	\N	\N	Masculino 	Soltero 	\N	ejbaptista@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2010-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1166	16742194	RONDON QUINTERO, ARLENY PAOLA	Venezolano 	1985-03-08	\N	\N	Femenino 	Soltero 	\N	aprondon@ubv.edu.ve 	SECTOR RIO PERDIDO CARRETERA PANAMERICANA ANTES DEL PUENTE RIO PERDIDO 	1	INSTRUCTOR	DOCENTE	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1167	16745739	ESCALANTE SIMOES, MILAGROS DAIYELYTH	Venezolano 	1984-04-24	\N	\N	Femenino 	Soltero 	\N	mescalante@ubv.edu.ve 	LA CURIACHA VEREDA LA PIEDRA RIO PPAL BOROTA PARROQUIA CONSTITUCION 	1	INSTRUCTOR	DOCENTE	2010-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1168	16756323	PALENCIA YRAUSQUIN, ELIANA JOSEFINA	Venezolano 	1984-06-28	\N	\N	Femenino 	Soltero 	\N	ejpalencia@ubv.edu.ve 	URB. BICENTENARIA MANZANA 6, CALLE 1-1 CON1-11 	4	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1169	16757861	LUNA RAMIREZ, SANDRA ANISDEL	Venezolano 	1983-09-05	\N	\N	Femenino 	Soltero 	\N	sluna@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2009-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1170	16819323	HERNANDEZ ARRIECHE, LENIN EDUARDO	Venezolano 	1983-11-25	\N	\N	Masculino 	Soltero 	\N	lhernandez@ubv.edu.ve 	URB LA ROSA SECTOR LA EXPLANADA EDIF D APTO D-41 	0	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1171	16825526	SANCHEZ LOW, CLAUDIA MARCELA	Venezolano 	1972-05-31	\N	\N	Femenino 	Soltero 	\N	claudialow1@hotmail.com 	URBANIZACION CONUCO VIEJO,CALLE DOÑA PETRA,CASA SOBERANA MUNICIPIO GARCIA 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1172	16830790	ROQUE ACOSTA, ANGELIMAR DEL VALLE	Venezolano 	1985-02-07	\N	\N	Femenino 	Soltero 	\N	aroque@ubv.edu.ve 	URBANIZACION CRUZ VERDE 5/8, CALLE N 11, CASA N 31, 	4	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1173	16910264	BRIZUELA MARTINEZ, CARMELO JOSE	Venezolano 	1982-08-14	\N	\N	Masculino 	Soltero 	\N	cbrizuela@ubv.edu.ve 	CARACAS 	1	INSTRUCTOR	DOCENTE	2011-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1174	16938999	ARMAS TOVAR, KRISTELL DAYANA	Venezolano 	1984-09-08	\N	\N	Femenino 	Soltero 	\N	karmas@ubv.edu.ve 	SECTOR TIPURO URBANIZACION LA PRADERA CASA B-4. 	2	ASISTENTE	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1175	16939884	VARGAS GARCIA, ISMELVYS JOSEFINA	Venezolano 	1984-03-25	\N	\N	Femenino 	Soltero 	\N	ijvargas@ubv.edu.ve 	CALLE LAS ACACIAS CASA N 27, SECTOR LAS FLORES DE LA PUENTE 	1	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1176	16974399	TARIFA MONTOYA, JAIOL EDGARDO	Venezolano 	1985-10-24	\N	\N	Masculino 	Soltero 	\N	jtarifa@ubv.edu.ve 	CALLE 12 AV.1 Y 2 CASA 34-04 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1177	17045383	SANCHEZ CONTRERAS, NAIBET SUSANA	Venezolano 	1983-05-24	\N	\N	Femenino 	Soltero 	\N	nssanchez@ubv.edu.ve 	AV BOLIVAR CC SIEGART CASA 55.B SECOR MENESES 	2	AUXILIAR I	DOCENTE	2005-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1178	17091235	SALAZAR SOUQUETT, RAFAEL JOSE	Venezolano 	1984-01-24	\N	\N	Masculino 	Casado 	\N	rsalazarq@ubv.edu.ve 	RESIDENCIAS EL TAMA TORRE B PISO 9 APTO 9-D 	0	INSTRUCTOR	DOCENTE	2010-12-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1179	17137182	GONZALEZ ROJAS, DIANA MARIELA	Venezolano 	1984-10-19	\N	\N	Femenino 	Soltero 	\N	dmgonzalez@ubv.edu.ve 	SECTOR LAS COLINAS DEL TRIANGULO, CALLE 7 DE OCTUBRE  CASA N 02 	2	INSTRUCTOR	CONTRATADO	2015-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1180	17163757	MEZONES MEDINA, LUIS ENRIQUE RAFAEL	Venezolano 	1985-10-02	\N	\N	Masculino 	Soltero 	\N	lmezones@ubv.edu.ve 	APARTAMENTO 23 PARTE ALTA DE LAS MAYAS,PARROQUIA COCHE DE CARACAS 	0	ASISTENTE	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1181	17227054	LOPEZ ECHEGARAI, MARIEUDIL DOIRALITH	Venezolano 	1984-06-12	\N	\N	Femenino 	Soltero 	\N	mlopez@ubv.edu.ve 	CALLE N8 ENTRE CARRERAS 6 Y 7 ANDRES ELOY BLANCO 	0	ASISTENTE	DOCENTE	2011-01-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1182	17235259	BLANCO MOYA, THOMAS ALEXANDER	Venezolano 	1985-09-25	\N	\N	Masculino 	Soltero 	\N	tblanco@ubv.edu.ve 	GUIRIGUIRI, CALLE LA RINCONADA, LA PLAZA, ANTOLIN DEL CAMPO 	1	INSTRUCTOR	CONTRATADO	2015-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1183	17293642	SOTURNO PALOMARES, YEIMAR JOSEFINA 	Venezolano 	1984-03-19	\N	\N	Femenino 	Casado 	\N	ysoturno@ubv.edu.ve 	CARRETERA VÍA LA CONCEPCIÓN URB CAMINO DE LA LAGUNITA CONJUNTO 18 	1	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1184	17318725	GARCIA MORENO, JOSE GABRIEL DEL CARMEN	Venezolano 	1986-04-14	\N	\N	Masculino 	Soltero 	\N	jggarciam@ubv.edu.ve 	URB. LAS CAYENAS CALLE N 8 CASA N 58 	1	INSTRUCTOR	DOCENTE	2010-12-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1185	17377125	GARRIDO RAMOS, MARCOS AURELIO	Venezolano 	1984-12-10	\N	\N	Masculino 	Soltero 	\N	mgarrido@ubv.edu.ve 	SECTOR CAMPO LA MESA CALLE DE ACCESO QUINTA MAYORA CASA 18-05 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1186	17442108	MORALES DE PIÑA, JAINEY KARINA	Venezolano 	1987-01-25	\N	\N	Femenino 	Casado 	\N	jainey17@gmail.com 	CALLE PRINCIPAL CASA NRO 132 SECTOR LOS ALPES SECTOR 01 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1187	17447136	RODRIGUEZ FERNANDEZ, YOSMAR DEL VALLE	Venezolano 	1986-03-06	\N	\N	Femenino 	Soltero 	\N	yvrodriguez@ubv.edu.ve 	SUCRE-CUMANA, APTO 01 EDIF. 403 	0	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1188	17461724	GOMEZ GONZALEZ, MARIA LUISA	Venezolano 	1987-02-08	\N	\N	Femenino 	Soltero 	\N	mlgomez@ubv.edu.ve 	B/MATO GRANDE AV. 114C-2 CASA N 83-82 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1189	17491815	BERBESI MALDONADO, JESUS	Venezolano 	1987-03-27	\N	\N	Masculino 	Soltero 	\N	jberbesi@ubv.edu.ve 	SECTOR EL SALADO PARTE ALTA CASA S7N JUNTO AL CEMENTERIO 	0	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1190	17499585	PEREZ ROMERO, MARIANA ALEJANDRA	Venezolano 	1986-02-26	\N	\N	Femenino 	Soltero 	\N	maperez@ubv.edu.ve 	URB. SAN RAFAEL CALLE JOSEFA, MANZANO 01 CASA 15 PTO. FIJO 	0	ASISTENTE	DOCENTE	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1191	17503349	MORENO CARRILLO, CATALINA	Venezolano 	1983-11-25	\N	\N	Femenino 	Soltero 	\N	cmoreno@ubv.edu.ve 	CALLE ALTAMIRA N C 1-2 LA ERMITA DETRAS DE LA CLINICA EL SAMAN 	2	INSTRUCTOR	DOCENTE	2010-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1192	17531910	SALAZAR GONZALEZ, EGNY BRICEYDA	Venezolano 	1986-11-04	\N	\N	Femenino 	Soltero 	\N	ebsalazar@ubv.edu.ve 	URB. PALO VERDE. CALLE AYACUCHO. CASA 6 	1	AUXILIAR III	DOCENTE	2007-06-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1193	17546425	CALDERON VASQUEZ, KAREN PATRICIA	Venezolano 	1986-09-30	\N	\N	Femenino 	Soltero 	\N	kcalderon@ubv.edu.ve 	CALLE EL VALLE 5005 LA cruz maturin edo,monagas 	1	INSTRUCTOR	CONTRATADO	2017-01-09	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1194	17560133	FABRA QUICENO, RAFAEL ANTONIO	Venezolano 	1985-02-20	\N	\N	Masculino 	Soltero 	\N	rfabra@ubv.edu.ve 	PARROQ. LAS MINAS BARUTA STA. CRUZ CASA N 51-6 BARUTA 	1	INSTRUCTOR	CONTRATADO	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1195	17629090	GOMEZ CASTILLO, FRANCISCO JAVIER	Venezolano 	1987-01-29	\N	\N	Masculino 	Soltero 	\N	fjgomez@ubv.edu.ve 	CALLE LA VEREDA ANTE COLON Y PROVIDENCIAS SECTOR CUAZAITO 	1	INSTRUCTOR	DOCENTE	2011-01-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1196	17695770	LATAN CEDEÑO, MONICA ROBDANIS	Venezolano 	1987-03-14	\N	\N	Femenino 	Soltero 	\N	mlatan@ubv.edu.ve 	CALLE MARIÑO CAS N 39 	1	INSTRUCTOR	DOCENTE	2011-05-02	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1197	17695808	RAMOS RAMOS, JUANA ANDREINA	Venezolano 	1986-09-29	\N	\N	Femenino 	Soltero 	\N	jaramosr@ubv.edu.ve 	MEREYAL LOS ROSALES CARRERA 2 CASA 08 	0	INSTRUCTOR	CONTRATADO	2014-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1198	17769878	ZAMBRANO ZAMBRANO, MARIA ISABEL	Venezolano 	1985-06-28	\N	\N	Femenino 	Soltero 	\N	mizambrano@ubv.edu.ve 	URB. I Y II CAÑO EL TIGRE KM6 CALLE 4 CASA N 28 	2	INSTRUCTOR	CONTRATADO	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1199	17784229	CORDERO LUGO, HECTOR DANIEL	Venezolano 	1985-04-26	\N	\N	Masculino 	Soltero 	\N	hcordero@ubv.edu.ve 	URB. LOS CLAVELES C/BOBARE SECTOR PUERTA MARAVE CSA S/N 	1	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1200	17793258	CARRILLO DAVILA, KARINA DEL CARMEN	Venezolano 	1986-11-02	\N	\N	Femenino 	Soltero 	\N	kcarrillo@ubv.edu.ve 	ANTIGUA VIA A MERIDA SECTOR BOLERO ALTO 	2	INSTRUCTOR	DOCENTE	2012-07-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1201	17841360	NAVAS RUIZ, RAISBELT DEL CARMEN	Venezolano 	1988-08-18	\N	\N	Femenino 	Soltero 	\N	RAISBELNAVAS@HOTMAIL.COM 	CALLE ARISMENDI, CASA N 30-264, PUNTO FIJO 	0	INSTRUCTOR	CONTRATADO	2016-01-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1202	17861761	BARAJAS SILVA, JENNY LILIANA	Venezolano 	1986-11-05	\N	\N	Femenino 	Soltero 	\N	jbarajas@ubv.edu.ve 	CALLE LA REVOLUCION, CASA Nro 4 TERRAZAS DE LA FORTUNA RUBIO 	3	INSTRUCTOR	DOCENTE	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1203	17865335	PERDOMO VALERO, DAIYIRETH CELESTINA 	Venezolano 	1986-09-11	\N	\N	Femenino 	Soltero 	\N	dperdomov@ubv.edu.ve 	CALLE PAEZ N 12 B SECTOR CADILLAL, SAN VICENTE 	1	INSTRUCTOR	CONTRATADO	2015-02-02	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1204	17963269	ZAMBRANO SANTIAGO, ALBA CECILIA	Venezolano 	1985-10-27	\N	\N	Femenino 	Casado 	\N	 	CONJ RES COLINAS DE SAN FRANCISCO DE YARE MANZANA  8 EDIF 27 PISO 1 APTO 06 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1205	17980490	MEJIAS MENDEZ, KEILYTH JOHELY	Venezolano 	1986-08-21	\N	\N	Femenino 	Soltero 	\N	kmejias@ubv.edu.ve 	CALLE MIRANDA, SECTOR CALANCHE CASA S/N TACATA 	0	INSTRUCTOR	CONTRATADO	2016-06-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1206	18013265	RATIA SUAREZ, GREICY CAROLINA	Venezolano 	1987-12-23	\N	\N	Femenino 	Soltero 	\N	gratia@ubv.edu.ve 	CALLE UNION CASA NRO 8 SECTOR BRISAS DEL SUR II. CIUDAD BOLIVAR 	0	INSTRUCTOR	CONTRATADO	2017-07-03	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1207	18081038	DUERTO ACOSTA, MARJORIE MARLENE	Venezolano 	1984-10-16	\N	\N	Femenino 	Soltero 	\N	mduerto@ubv.edu.ve 	JUSEPIN SECTOR LAS MARGARITAS CALLE LAS ROSAS S/N 	1	ASISTENTE	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1208	18088642	GONZALEZ GALINDO, REINALDO ELIESER	Venezolano 	1985-09-28	\N	\N	Masculino 	Soltero 	\N	regonzalez@ubv.edu.ve 	CARACAS 	1	ASISTENTE	DOCENTE	2010-03-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1209	18094315	TORRES JASPE, YANNY YANEISY	Venezolano 	1985-09-16	\N	\N	Femenino 	Soltero 	\N	yytorre@ubv.edu.ve 	CARRETERA NACIONAL GUATIRE CAUCAGUA CUPO SECTOR GURUPERA 	0	INSTRUCTOR	CONTRATADO	2017-01-09	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1210	18098349	QUINTERO LOPEZ, IRENIS ROSALIA	Venezolano 	1986-10-01	\N	\N	Femenino 	Soltero 	\N	iquintero@ubv.edu.ve 	YOCO, CALLE PRINCIPAL CASA N 52 AL LADO DE VARIEDADES OLGA 	0	INSTRUCTOR	DOCENTE	2011-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1211	18124371	SULBARAN MENDEZ, DEISY COROMOTO	Venezolano 	1986-07-16	\N	\N	Femenino 	Soltero 	\N	scsulbaran@ubv.edu.ve 	AVENIDA LOS PROCERES PROLONGACION EL LLANITO 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1212	18172072	ROMERO BRITO, RICARDO JAVIER	Venezolano 	1985-09-10	\N	\N	Masculino 	Soltero 	\N	rjromerob@ubv.edu.ve 	CALLE EL TUBO, SECTOR JUANICO, CASA S/N 	0	INSTRUCTOR	CONTRATADO	2015-02-02	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1213	18172980	HERNANDEZ MENDOZA, JOSE JESUS TADEO	Venezolano 	1987-09-07	\N	\N	Masculino 	Soltero 	\N	jjhernandezm@ubv.edu.ve 	CALLE CIRCUNVALACIÓN, CASA AIMARA, SECTOR SANTA FE 	0	INSTRUCTOR	DOCENTE	2012-01-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1214	18200141	HERNANDEZ MOLINA, WILMER ENRIQUE	Venezolano 	1987-04-23	\N	\N	Masculino 	Soltero 	\N	whernandez@ubv.edu.ve 	URBANIZACION LA MONTAÑITA CALLE 94 JEONAV 103 CASA 103-04 	1	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1215	18223429	ROJAS CHAVEZ, MARIANA ALEJANDRA	Venezolano 	1986-05-20	\N	\N	Femenino 	Soltero 	\N	marojasc@ubv.edu.ve 	CALLE PRINCIPAL DE PIPE,PARCELA 82, CASA MARIANA, PIPE ABAJO, RUIZ PINEDA CARICUAO 	0	INSTRUCTOR	CONTRATADO	2018-01-22	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1216	18274479	AGUILAR SALAZAR, RAMON JOSE 	Venezolano 	1988-05-03	\N	\N	Masculino 	Soltero 	\N	raguilar@ubv.edu.ve 	SILENCIO DE CAMPO A, CALLE N 11, CASA N 4 	0	INSTRUCTOR	CONTRATADO	2015-07-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1217	18328981	PEREZ PANTOJA, YESICA YULITZA	Venezolano 	1988-04-23	\N	\N	Femenino 	Soltero 	\N	YESICAMS.PEREZ74@GMAIL.COM 	URB LAS MARAVILLAS CASA 18 SAN FERNANDO DE APURE 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1218	18361627	GUERRA OLLARVES, KARLA CECILIA	Venezolano 	1985-11-09	\N	\N	Femenino 	Soltero 	\N	 	URB.RICARDO URRIERA SECTOR 2 CALLE 2 CASA67 	0	INSTRUCTOR	CONTRATADO	2019-05-31	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1220	18478059	VILLEGAS DIAZ, DENALI MERIRIEL	Venezolano 	1986-06-05	\N	\N	Femenino 	Soltero 	\N	dmvillegas@ubv.edu.ve 	LA DINAMITA CALLE LAS FLORES CASA N21 	0	INSTRUCTOR	CONTRATADO	2014-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1221	18586099	BARRETO MARCANO, JOSE JESUS	Venezolano 	1986-12-22	\N	\N	Masculino 	Soltero 	\N	jbarreto@ubv.edu.ve 	URB LA LIBERTAD, CALLE G CASA 29 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1222	18586869	GONZALEZ LUCES, YANICETH MARIA 	Venezolano 	1988-05-25	\N	\N	Femenino 	Soltero 	\N	ymgonzalez@ubv.edu.ve 	CALLE BOYACÁ N42 	0	INSTRUCTOR	CONTRATADO	2014-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1223	18637505	VEJAR DÍAZ, INGRID PAOLA	Venezolano 	1988-09-22	\N	\N	Femenino 	Casado 	\N	ivejar@ubv.edu.ve 	URBANIZACIÓN PARQUE CHAMA CALLE 2C CASA N 03 	2	INSTRUCTOR	DOCENTE	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1224	18789847	GIL NORIEGA, ARIANNY ANDREINA	Venezolano 	1988-08-22	\N	\N	Femenino 	Soltero 	\N	agil@ubv.edu.ve 	EL SILENCIO DE CAMPO ALEGRE CALLE 11 	2	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1225	18827167	HERRERA OLIVERO, DUBRASKA MARYLEINS	Venezolano 	1987-11-14	\N	\N	Femenino 	Soltero 	\N	dmherrera@ubv.edu.ve 	BARRIO EL MIRACLON CALLE ALTA VISTA CASA N44 	1	AUXILIAR I	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1226	18880502	ROSALES RAMIREZ, LUCYBELL LISSETT	Venezolano 	1987-01-01	\N	\N	Femenino 	Soltero 	\N	lrosales@ubv.edu.ve 	LOS JARDINES DEL VALLE, APTO 21-3, PISO 21, TORRE 1, RES. BRICOMONT 	0	INSTRUCTOR	CONTRATADO	2016-01-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1227	18938068	BARBOZA SIRI, LUCIA	Venezolano 	1972-12-27	\N	\N	Femenino 	Soltero 	\N	lbarboza@ubv.edu.ve 	CALLE EL CONVENTO,EDF. ILVA, APTO. 5, URB. VALLE ABAJO, CARACAS 	2	ASISTENTE	DOCENTE	2004-05-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1228	19092397	FAURE RAMIREZ, YOSMERY ALEJANDRA	Venezolano 	1990-02-08	\N	\N	Femenino 	Soltero 	\N	yfaure@ubv.edu.ve 	SECTOR TIPURO, VIA VILABORAL, BELLO CAMPO CALLE B, CASA N B-59 	1	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1229	19177199	OLASCUAGA PACHECO, JONATHAN	Venezolano 	1985-04-20	\N	\N	Masculino 	Soltero 	\N	jolascuaga@ubv.edu.ve 	CARACAS 	1	ASISTENTE	DOCENTE	2010-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1230	19415031	OSORIO JIMENEZ, ROSA AURISTELA	Venezolano 	1990-01-18	\N	\N	Femenino 	Soltero 	\N	rosorio@ubv.edu.ve 	CALLE 2 N 14 LOS ROSALES SECTOR EL MEREYAL 	1	INSTRUCTOR	CONTRATADO	2015-02-02	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1231	19730219	DIAZ BLANCA, KEYLA NOHEMI	Venezolano 	1989-12-26	\N	\N	Femenino 	Casado 	\N	kndiaz@ubv.edu.ve 	AV PERIMETRAL, SECTOR VILLA DEL SUR CALLE PIAR N 12 	0	INSTRUCTOR	CONTRATADO	2014-09-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1232	19746907	 LAREZ PEREZ, LUISANA DAYRID	Venezolano 	1989-04-30	\N	\N	Femenino 	Casado 	\N	llarezperez@ubv.edu.ve 	CALLE LA PLANTA CASA N35SECTOR LAS PARCELAS 	1	INSTRUCTOR	CONTRATADO	2014-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1233	19782519	CANACHE REINALES, ELIANNY JOSE	Venezolano 	1988-10-28	\N	\N	Femenino 	Soltero 	\N	ecanache@ubv.edu.ve 	LA CRUZ CALLE EL CALVARIO CASA 5444 FRENTE AL MODULO POLICIAL 	2	INSTRUCTOR	CONTRATADO	2015-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1234	19796976	VALLEJO BARRIOS, DANYELA MARELLY	Venezolano 	1988-12-26	\N	\N	Femenino 	Soltero 	\N	dmvallejo@ubv.edu.ve 	AV PRINCIPAL DEL SECTOR 3 CASA H-22 URBANIZACION GARCIA CARBALLO,UP3CARICUAO 	0	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1235	19825265	BECERRA VARELA, KARLA MARIA	Venezolano 	1989-04-12	\N	\N	Femenino 	Soltero 	\N	kbecerra@ubv.edu.ve 	URBANIZACIÓN LA FLORESTA, SECTOR NUEVO PARAISO, CALLE 02 	0	ASISTENTE	DOCENTE	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1236	19830324	REVERON IBARRA, KAREN RUSMERY	Venezolano 	1989-03-16	\N	\N	Femenino 	Soltero 	\N	kreveron@ubv.edu.ve 	VALLECITO 1 OCUMARE DEL TUY 	0	INSTRUCTOR	DOCENTE	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1237	19851774	COLMENAREZ SIMANCA, KEYLA YERALDIN	Venezolano 	1989-09-08	\N	\N	Femenino 	Soltero 	\N	 	CALLE REAL SANTA ANA SECTOR EL 70 CALLEALTAMIRA CASA 22 	0	INSTRUCTOR	CONTRATADO	2018-02-05	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1238	19960660	CEDEÑO RAMOS, DAFNA YAEL	Venezolano 	1990-11-27	\N	\N	Femenino 	Soltero 	\N	dycedeno@ubv.edu.ve 	URB.CIUDAD BETANIA EDIF.LOS SAUCES 4 APTO PB-C 	0	AUXILIAR I	CONTRATADO	2017-10-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1239	20092981	DELGADO, MARIA VIRGINIA 	Venezolano 	1989-07-24	\N	\N	Femenino 	Soltero 	\N	 	CALLE LA CEIBA SECTOR LAS MARIAS CASA N 76 SAN FERNANDO DE APURE 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1240	20125060	CORDOVA FERNANDEZ, ORLYMAR JOSE	Venezolano 	1988-11-10	\N	\N	Masculino 	Soltero 	\N	ocordova@ubv.edu.ve 	AV ORINOCO, EDIFICIO, LOS ROBLES PISO8-1 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1241	20549002	HAKANDU KANOO, AMIELA PERSAUD	Venezolano 	1974-09-21	\N	\N	Femenino 	Soltero 	\N	ahakandu@ubv.edu.ve 	LOS PROCERES CALLE FRANCISCO DE MIRANDA CASA 14-03 	2	ASISTENTE	DOCENTE	2007-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1242	20551772	ROVELLA GUANIPA, KAREN  KATHERINA	Venezolano 	1993-05-28	\N	\N	Femenino 	Soltero 	\N	karen_28_98 hotmail.com 	URB.LAS ADJUNTAS SECTOR NICOLAS DE BARI MANZANA C 10 	0	INSTRUCTOR	CONTRATADO	2019-02-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1243	20645490	SANCHEZ LOPEZ, FATIMA BETSABE	Venezolano 	1989-03-06	\N	\N	Femenino 	Soltero 	\N	fbsanchez@ubv.edu.ve 	AV BOLIVAR, SECTOR CENTRO ESTE. PUNTA DE MATA, CASA N 74 	0	INSTRUCTOR	CONTRATADO	2015-12-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1244	20746008	GONZALEZ ESPINOZA, WILLIAMS ANTONIO	Venezolano 	1991-10-27	\N	\N	Masculino 	Soltero 	\N	 	Los Teques. El Cabotaje. Calle Ramòn Vicente Tovar. Escalera N3. Casa N 23 	0	AUXILIAR I	CONTRATADO	2018-04-23	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1245	21007797	GAMEZ ROMAN, REYMER ESTHEFANO	Venezolano 	1991-09-06	\N	\N	Masculino 	Soltero 	\N	 	BARRIO DAVID MORALES BELLO LA SABANITA CALLETUMEREMO CASA 7-B 	0	INSTRUCTOR	CONTRATADO	2017-06-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1246	21052172	PEÑALVER BRITO, ROSSANA DEL VALLE	Venezolano 	1994-09-21	\N	\N	Femenino 	Soltero 	\N	rpenalver@ubv.edu.ve 	PRADOS DEL SUR CALLE 10 CASA 18 	0	INSTRUCTOR	CONTRATADO	2017-01-09	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1247	21326861	BUITRAGO DIAZ, FEDERICO	Venezolano 	1970-05-28	\N	\N	Masculino 	Soltero 	\N	febuitrago@ubv.edu.ve 	PRIMERA ENTRADA DE LAS HERNANDEZ CASA N15 TUBORES 	2	INSTRUCTOR	DOCENTE	2012-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1248	21601921	SOLANO, WILSON JOSÉ	Venezolano 	1964-01-21	\N	\N	Masculino 	Soltero 	\N	wsolano@ubv.edu.ve 	URB. LOS SAMANES, AV. 49F. CALLE 202A, MUNICIPIO SAN FRANCISCO 	2	AGREGADO	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1249	22056318	CHARRIS BUJATO, OSIRIS ROSA	Venezolano 	1957-11-28	\N	\N	Femenino 	Soltero 	\N	ocharris@ubv.edu.ve 	SECTOR LA LIMPIA| 	0	AGREGADO	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1250	22082423	OYAGA MARMOL, EDUARDO	Venezolano 	1956-01-05	\N	\N	Masculino 	Soltero 	\N	eoyaga@ubv.edu.ve 	CALLE 148 CON AV. 67A-26 EL GAITERO MARACAIBO 	0	ASISTENTE	DOCENTE	2004-05-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1251	22478483	MENDIVIL GIL, HENRRY EDWARD	Venezolano 	1972-07-23	\N	\N	Masculino 	Soltero 	\N	hmendivil@ubv.edu.ve 	VENEZUELA FALCON 	2	AGREGADO	DOCENTE	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1252	22723596	BERRA, CARLOS EDUARDO	Venezolano 	1994-04-13	\N	\N	Masculino 	Soltero 	\N	cberra@ubv.edu.ve 	LAS PARCELAS CALLE MIRANDA CASA 13 MUNICIPIO LIBERTADOR 	0	INSTRUCTOR	CONTRATADO	2017-11-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1568	4505803	ASTUDILLO, FANNY	Venezolano 	1954-12-12	\N	\N	Femenino 	Soltero 	\N	fastudillo@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2005-10-15	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1253	22804280	VEGA GUTIERREZ, JUAN CARLOS	Venezolano 	1967-10-14	\N	\N	Masculino 	Casado 	\N	jcvega@ubv.edu.ve 	SECTOR MEDINA ANGARITA, VEREDA 1, CASA NUM. 29 	2	ASISTENTE	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1254	22816080	BULNES SOTO, RAIMUNDO EZEQUIEL	Venezolano 	1967-12-16	\N	\N	Masculino 	Soltero 	\N	rbulnes@ubv.edu.ve 	URB. LA PARAGUA EDIF. 1-22B APTO 42 	1	AUXILIAR I	DOCENTE	2004-09-15	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1255	22984210	ALVARADO GARCIA, EMEL ARCESIO	Venezolano 	1969-01-12	\N	\N	Masculino 	Soltero 	\N	ealvaradog@ubv.edu.ve 	CCS 	1	INSTRUCTOR	DOCENTE	2011-01-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1256	22998137	ACOSTA VICENT, ALI RAFAEL	Venezolano 	1994-12-26	\N	\N	Masculino 	Soltero 	\N	 	URBANIZACION LOS ROBLES M-11-A EL DATIL 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1257	23149301	SIERRA CORTEZ, NELSON MARLON	Venezolano 	1970-05-30	\N	\N	Masculino 	Casado 	\N	nsierra@ubv.edu.ve 	BARRIO LIBERTADOR PASAJE ORINOCO CASA N 3-18 	2	INSTRUCTOR	DOCENTE	2009-04-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1258	23899634	DE LA OSSA NAVARRO, ARECENIO ALFREDO	Venezolano 	1955-12-14	\N	\N	Masculino 	Soltero 	\N	adelaossa@ubv.edu.ve 	CALLE N 9 CASA N 29 CINCO DE JULIO MATURIN 	0	INSTRUCTOR	DOCENTE	2011-02-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1259	23899843	GONZALEZ FUENMAYOR, DARWIN DAVID	Venezolano 	1993-05-20	\N	\N	Masculino 	Soltero 	\N	ddgonzalez@ubv.edu.ve 	SECTOR TIPURO URB TERRAZAS DEL NORTE CONJUNTO RESIDENCIAL CIGARRAL CASA 102 	0	INSTRUCTOR	CONTRATADO	2017-01-09	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1260	24757715	GRANILLO MAZARIEGOS, GABRIEL	Venezolano 	1972-03-24	\N	\N	Masculino 	Soltero 	\N	 	URB HUGO CHAVEZ CATIA LA AMR APTO 08 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1261	24891376	MURUA, JORGELINA	Venezolano 	1975-06-12	\N	\N	Femenino 	Soltero 	\N	jmurua@ubv.edu.ve 	URB SANTA FE CARRERA 6 CALLE 16 	0	AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1262	24944465	PEREZ ARAGUACHE, YENIFER COROMOTO	Venezolano 	1994-08-18	\N	\N	Femenino 	Soltero 	\N	perezyenifer1808@gmail.com 	SECTOR EZEQUIEL ZAMORA CALLE NUEVA VENEZUELA 114 PUNTO FIJO 	0	INSTRUCTOR	CONTRATADO	2019-02-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1263	24958738	MONSALVE GARCIA, EMMA	Venezolano 	1973-02-01	\N	\N	Femenino 	Soltero 	\N	emonsalve@ubv.edu.ve 	AV. 85, NO. 79H-07 URB. LA FLORESTA MARACAIBO-EDO. ZULIA 	1	AGREGADO	DOCENTE	2004-10-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1264	25659908	CENTENO ALVAREZ, ELAINE	Venezolano 	1966-12-10	\N	\N	Femenino 	Soltero 	\N	ecenteno@ubv.edu.ve 	AV. EL MIRAGRO. RESIDENCIAS SAN MARTIN. PISO 16 APTO 3-4 MODULO 4 	0	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1265	26621620	LUONGO PEREZ, CARLA ODETTE	Venezolano 	1975-12-31	\N	\N	Femenino 	Soltero 	\N	coluongo@ubv.edu.ve 	CALLE HUMBOLT RES. MARHUANTA PISO 10 APTO 104 TORRE B 	1	AGREGADO	DOCENTE	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1266	26989428	RIVERA GONZALEZ, HÉCTOR LUIS	Venezolano 	1964-10-13	\N	\N	Masculino 	Soltero 	\N	hrivera@ubv.edu.ve 	CARACAS 	0	ASISTENTE	DOCENTE	2007-01-09	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1267	28642583	RIVERA CASTELLANOS, LUZ DARY	Venezolano 	1976-11-04	\N	\N	Femenino 	Soltero 	\N	ldrivera@ubv.edu.ve 	CAMPO C CALLE EL BOSUQE DIAGONAL ESQUINA LA ESCUELA TARABAY 	1	INSTRUCTOR	DOCENTE	2010-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1268	30888513	PEÑALOSA ACUÑA, OSVALDO ENRIQUE	Venezolano 	1969-06-25	\N	\N	Masculino 	Soltero 	\N	openaloza@ubv.edu.ve 	CARACAS 	3	INSTRUCTOR	DOCENTE	2010-06-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1269	81118037	DE SA DE FREITAS, DAISI    MARIA	Extranjero 	1950-08-18	\N	\N	Femenino 	Soltero 	\N	dfreitas@ubv.edu.ve 	LA CANDELARIA ALCABALA PUENTE ANAUCO EDIF. PUENTE ANAUCO PISO 8. APTO 84 	0	AUXILIAR II	DOCENTE	2005-10-10	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1270	82007118	ONETO REBORI, FANNY MARIELA	Extranjero 	1974-03-06	\N	\N	Femenino 	Soltero 	\N	fmoneto@ubv.edu.ve 	AV. LIMPIA DETRAS DE VIKO0 OLIVAR RESIDENCIAL LA RINCONANADA . APTO. 5-C 	3	ASISTENTE	DOCENTE	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1271	82180767	NEUS , MARKUS JOZEF	Extranjero 	1962-11-11	\N	\N	Masculino 	Soltero 	\N	mjneus@ubv.edu.ve 	AV. SUR 9 ENTRE ESQUINA PERICO A SAN LOZARO, LA CANDELARIA 	0	ASISTENTE	DOCENTE	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1272	82233808	RAMÍREZ MORALES, CARLOS DARIO	Extranjero 	1961-04-02	\N	\N	Masculino 	Soltero 	\N	cdramirez@ubv.edu.ve 	CONJUNTO RESIDENCIAL SAN ANTONIO AV. PERIMETRAL TORRE C APTO 13-3 	1	COORDINADOR NACIONAL INSTRUCTOR	DOCENTE	2008-02-18	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1273	84398928	SALGADO AYALA, ANA ROSBINDA	Extranjero 	1986-11-23	\N	\N	Femenino 	Soltero 	\N	asalgado@ubv.edu.ve 	URBANIZACION ROSA INES 	1	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1274	6495441	PINTO IRIARTE, MAIGUALIDA DEL VALLE	Venezolano 	1967-04-03	\N	\N	Femenino 	Casado 	\N	mvpinto@ubv.edu.ve 	PARROQUIA VARGAS SECTOR SAN PEDRO 	0	ASOCIADO	ALTO NIVEL	2014-05-20	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1275	8709239	MARIBEL CHIQUINQUIRA, PRIETO HERNANDEZ	Venezolano 	1968-11-14	\N	\N	Femenino 	Casado 	\N	mcprietoh@ubv.edu.ve 	CARACAS 	0	ASOCIADO	ALTO NIVEL	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1276	10480795	RIVAS ROA, FRANCIS DEL CARMEN	Venezolano 	1970-04-02	\N	\N	Femenino 	Soltero 	\N	frivas@ubv.edu.ve 	LA REDOMA LA INDIA DEL PARAISO EN LA VEGA CALLE SANJOSE EDF HILDA PISO 6 APT 56 	1	ASISTENTE	ALTO NIVEL	2006-01-09	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1277	11925875	MATA LEON, ROSICAR DEL VALLE	Venezolano 	1975-11-18	\N	\N	Femenino 	Soltero 	\N	rmata@ubv.edu.ve 	CARACAS 	2	ASOCIADO	ALTO NIVEL	2012-01-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1278	14322588	FUMERO JIMENEZ, ALBIN HENDERSON	Venezolano 	1981-03-11	\N	\N	Masculino 	Soltero 	\N	afumero@ubv.edu.ve 	UEB. PAROSCA, SECTOR 2, CALLE 2, CASA N 64 	0	INSTRUCTOR	ALTO NIVEL	2007-10-16	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1279	647923	GOMEZ LOZADA, JUAN MANUEL 	Venezolano 	1950-07-07	\N	\N	Masculino 	Soltero 	\N	juangomezbater@hotmail.com 	URB LOS MAGALLANES DE CATIA AV EL CRISTO NRO 36 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE MEDIO TIEMPO	ACTIVO
1280	776464	NAVARRETE ORTA, LUIS ALBERTO	Venezolano 	1932-06-22	\N	\N	Masculino 	Casado 	\N	 	AV LA ACACIAS EDIF MARISOL APTO6 LA FLORIDA 	0	INSTRUCTOR	CONTRATADO	2018-06-01	DOCENTE MEDIO TIEMPO	ACTIVO
1281	2062971	MORO, ANGEL  GUILLERMO	Venezolano 	1942-05-22	\N	\N	Masculino 	Soltero 	\N	 	CARACAS 	0	TITULAR	CONTRATADO	2010-07-13	DOCENTE MEDIO TIEMPO	ACTIVO
1282	2234714	GARCIA, PEDRO ANTONIO	Venezolano 	1945-04-26	\N	\N	Masculino 	Soltero 	\N	pgarcia@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2011-03-21	DOCENTE MEDIO TIEMPO	ACTIVO
1283	2235276	CASTELLANO DE SJOSTRAND, MARIA EGILDA	Venezolano 	1937-01-04	\N	\N	Femenino 	Casado 	\N	castellanom63@gmail.com 	COLINAS DE BELLO MONTE AV CARONI EDIF DEBORAH APTO 31 PISO 3 	0	TITULAR	CONTRATADO	2019-02-11	DOCENTE MEDIO TIEMPO	ACTIVO
1284	2746338	ESCALONA HERNANDEZ, LUIS EVARISTO	Venezolano 	1945-10-26	\N	\N	Masculino 	Soltero 	\N	leescalona@ubv.edu.ve 	AV FLOR DE MAYO CC LAGUNITA CASA 19 	0	INSTRUCTOR	DOCENTE	2007-10-16	DOCENTE MEDIO TIEMPO	ACTIVO
1285	2808971	CORREDOR ARIAS, CESAR ORLANDO	Venezolano 	1948-10-20	\N	\N	Masculino 	Casado 	\N	ccorredor@ubv.edu.ve 	VIA ALTOS DE PARAMILLIO CALLE TOICO GABIELIS N 14 PALO GORDO 	0	INSTRUCTOR	DOCENTE	2008-03-01	DOCENTE MEDIO TIEMPO	ACTIVO
1286	3174769	IZARRA CALDERA, WILLIAM ERNESTO	Venezolano 	1947-05-07	\N	\N	Masculino 	Casado 	\N	 	URB TOVAR CASA S/N CALLE CLIMA 	0	AGREGADO 	CONTRATADO	2017-09-01	DOCENTE MEDIO TIEMPO	ACTIVO
1287	3185627	NUÑEZ NUÑEZ, MIGUEL ANGEL	Venezolano 	1950-02-07	\N	\N	Masculino 	Divorciado 	\N	IPIAT2000@YAHOO.ES 	SECTOR MILLA AV 3 	0	INSTRUCTOR	CONTRATADO	2019-03-15	DOCENTE MEDIO TIEMPO	ACTIVO
1289	3236454	GAMBOA DE DOMINGUEZ, MINERVA DE LOURDES	Venezolano 	1944-08-12	\N	\N	Femenino 	Casado 	\N	 	CALLE DOS RESI MARIA TERESA APTO 62 LA URBIBA MUNICIPIO SUCRE 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE MEDIO TIEMPO	ACTIVO
1290	3246406	MATA ESSAYAG, JOSE ALBERTO 	Venezolano 	1947-09-12	\N	\N	Masculino 	Casado 	\N	josematae@gmail.com 	CALLE TAMANACO N 14 	0	ASISTENTE	CONTRATADO	2018-06-18	DOCENTE MEDIO TIEMPO	ACTIVO
1291	3550443	FERNANDEZ MARTINEZ, NUMA JOSE	Venezolano 	1949-05-13	\N	\N	Masculino 	Casado 	\N	 	AVE ESTE TORRE LOS CAOBOS TORRE A PISO 3 APTO 32-A 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE MEDIO TIEMPO	ACTIVO
1292	3777707	GONZALEZ FERNANDEZ, MARLENY DEL CARMEN	Venezolano 	1951-05-12	\N	\N	Femenino 	Soltero 	\N	mdgonzalezf@ubv.edu.ve 	AV. PAEZ CON CALLE MONTEELENA EDF. BELVEDERE PISO #4 APTO 4-D 	0	INSTRUCTOR	DOCENTE	2007-05-14	DOCENTE MEDIO TIEMPO	ACTIVO
1293	3788588	DUQUE ESCALANTE, PEDRO CELESTINO	Venezolano 	1951-05-19	\N	\N	Masculino 	Soltero 	\N	pduque@ubv.edu.ve 	AV. PPAL LOS CHAGUARAMOS 	0	INSTRUCTOR	CONTRATADO	2012-07-16	DOCENTE MEDIO TIEMPO	ACTIVO
1294	3945615	ESPINOZA DE CAMINO , NORMA JOSEFINA	Venezolano 	1954-04-01	\N	\N	Femenino 	Casado 	\N	 	CALLE CARABOBO N 117 CARUPANO ESTADO SUCRE 	0	INSTRUCTOR	CONTRATADO	2012-07-16	DOCENTE MEDIO TIEMPO	ACTIVO
1295	3954987	GUAREMA COA, ALEXIS DE JESÚS	Venezolano 	1951-11-19	\N	\N	Masculino 	Casado 	\N	 	URB. LA COLINA VIA EL RINCON. CASA 14-07A 	0	INSTRUCTOR	DOCENTE	2007-10-17	DOCENTE MEDIO TIEMPO	ACTIVO
1297	4118016	GÓMEZ IRIARTE, RAISSA MIGUELINA	Venezolano 	1953-05-08	\N	\N	Femenino 	Soltero 	\N	rgomez@ubv.edu.ve 	PACHANO A SAN JUAN DE DIOS ENTRE PUNTO FIJO Y MARIÑO, CASA # 47, SECTOR CASCO COLONIAL 	0	INSTRUCTOR	DOCENTE	2007-10-16	DOCENTE MEDIO TIEMPO	ACTIVO
1298	4181719	DIAZ FLORES, MERLYS JOSEFA	Venezolano 	1967-09-09	\N	\N	Femenino 	Soltero 	\N	 	URB PARQUE CENTRAL  EDIF EL TEJAR PISO 19 APTO 19-C 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE MEDIO TIEMPO	ACTIVO
1299	4277982	PULIDO, CRUZ HERMINIA	Venezolano 	1956-06-06	\N	\N	Femenino 	Soltero 	\N	 	AVENIDA BOLIVAR CONJUNTO RESIDENCIAL LA MARINA PHC-6 CALLE AVANCAY 	0	INSTRUCTOR	CONTRATADO	2018-09-17	DOCENTE MEDIO TIEMPO	ACTIVO
1300	4335756	VILLANUEVA, LUIS CESAR	Venezolano 	1952-04-30	\N	\N	Masculino 	Casado 	\N	lvillanueva@ubv.edu.ve 	LOS GUARITOS 3 CALLE N1 CANAL 90 CASA N 18 	0	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE MEDIO TIEMPO	ACTIVO
1301	4585731	FUENMAYOR FLORES, WILFREDO DE JESUS	Venezolano 	1956-11-22	\N	\N	Masculino 	Soltero 	\N	wilfredofuenmayor@yahoo.com 	LA CANDELARIA EDIF AVILANES APTO 154 B 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE MEDIO TIEMPO	ACTIVO
1302	4693999	VELÁSQUEZ ROJAS, ALBERTO JESUS	Venezolano 	1947-11-15	\N	\N	Masculino 	Casado 	\N	ajvelasquez@ubv.edu.ve 	URB EL PERU  EDF #  3 	0	INSTRUCTOR	DOCENTE	2005-10-15	DOCENTE MEDIO TIEMPO	ACTIVO
1303	4716546	ALCALA, JUAN JOSE	Venezolano 	1956-05-03	\N	\N	Masculino 	Casado 	\N	 	URB. GUANIRE SECTOR E VEREDA CAS N 17 	1	INSTRUCTOR	DOCENTE	2011-05-02	DOCENTE MEDIO TIEMPO	ACTIVO
1304	4719405	ALCALA CERMEÑO, GUINES  DEL VALLE	Venezolano 	1958-11-07	\N	\N	Femenino 	Soltero 	\N	gdalcala@ubv.edu.ve 	URB. VILLA APPSO MANZANA L N 6 SECTOR CURAGUA PTO ORDAZ 	0	INSTRUCTOR	DOCENTE	2009-06-01	DOCENTE MEDIO TIEMPO	ACTIVO
1305	4790957	CIPRIANI SANKAR, MARISA LUISA	Venezolano 	1957-09-14	\N	\N	Femenino 	Soltero 	\N	 	MONTECLARO LAGUNA II ETAPA EDIF A 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE MEDIO TIEMPO	ACTIVO
1306	4887819	RODRÍGUEZ RAMOS, RAFAEL FIORELO	Venezolano 	1956-10-24	\N	\N	Masculino 	Soltero 	\N	rfrodriguez@ubv.edu.ve 	PARQUE LA INDIRA, LA PAZ 	0	INSTRUCTOR	DOCENTE	2004-04-13	DOCENTE MEDIO TIEMPO	ACTIVO
1307	4977968	MARTÍNEZ  RAMIREZ, JOSÉ LUIS	Venezolano 	1957-12-06	\N	\N	Masculino 	Soltero 	\N	jlmartinezr@ubv.edu.ve 	CONJUNTO RESIDENCIAL SAN SHARBEL TORRE B APTO 32-B PUERTO ORDAZ 	3	INSTRUCTOR	DOCENTE	2005-10-15	DOCENTE MEDIO TIEMPO	ACTIVO
1308	5006195	RAIMONDI, RICARDO ARCANGEL 	Venezolano 	1957-04-03	\N	\N	Masculino 	Soltero 	\N	rraimondi@ubv.edu.ve 	AV FUERZAS ARMADAS ESQ SAN JOSE A SAN RAFAEL EDIF CAPAYA PISO 11 APTO 11-2 	0	INSTRUCTOR	CONTRATADO	2014-09-18	DOCENTE MEDIO TIEMPO	ACTIVO
1309	5171412	VILLALOBOS DE QUINTERO, YUNARIA AMALOA	Venezolano 	1959-11-15	\N	\N	Femenino 	Casado 	\N	yavillalobos@ubv.edu.ve 	LA TAHONA RES CIMA QUEEN APTO 33 A BARUTA 	0	INSTRUCTOR	CONTRATADO	2014-11-01	DOCENTE MEDIO TIEMPO	ACTIVO
1310	5311279	CAMPOMAS TRUJILLO, NURY NERLYS	Venezolano 	1962-09-23	\N	\N	Femenino 	Soltero 	\N	ncampomas@ubv.edu.ve 	ALTAGRACIA ESTE 5 CANONIGOS A SAN RAMON P:B APTO 15 	0	INSTRUCTOR	DOCENTE	2007-10-16	DOCENTE MEDIO TIEMPO	ACTIVO
1311	5500196	SIMANCAS PEREZ, EXDY RAMONA	Venezolano 	1959-10-13	\N	\N	Femenino 	Soltero 	\N	esimancas@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2009-10-15	DOCENTE MEDIO TIEMPO	ACTIVO
1312	5579489	BOMPART RODRIGUEZ, ELINA ROSA	Venezolano 	1960-06-25	\N	\N	Femenino 	Divorciado 	\N	ebompart@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2011-05-02	DOCENTE MEDIO TIEMPO	ACTIVO
1313	5875470	MUJICA  GRANADO, OSTILY RAMONA	Venezolano 	1964-01-12	\N	\N	Femenino 	Casado 	\N	ormujica@ubv.edu.ve 	SECTOR LOS ALACRANES LILLA GUMILLA MANZANA N 3 CALLE FUNDELI CASA N 14 SAN FELIX 	0	INSTRUCTOR	DOCENTE	2009-06-01	DOCENTE MEDIO TIEMPO	ACTIVO
1314	5894935	PEREZ NATERA, FERNANDO YSIDRO	Venezolano 	1961-05-30	\N	\N	Masculino 	Soltero 	\N	ATENCIONSOCIALISTA@GMAIL.COM 	ZONA A TERRAZA 9, CASA N3 UD-2 CARICUAO 	0	INSTRUCTOR	CONTRATADO	2018-02-07	DOCENTE MEDIO TIEMPO	ACTIVO
1315	5964482	GALUE FLORES, CARLOS  ALBERTO	Venezolano 	1960-12-15	\N	\N	Masculino 	Casado 	\N	carlosgalue@gmail.com 	urb. sebucan resd jardines de sebucan, torre b piso 1 apto 1-g 	0	INSTRUCTOR	CONTRATADO	2018-10-15	DOCENTE MEDIO TIEMPO	ACTIVO
1316	6049769	GUTIERREZ RIVERA, MARIA FAVIOLA	Venezolano 	1961-09-15	\N	\N	Femenino 	Soltero 	\N	mfgutierrez@ubv.edu.ve 	AV.WASHINGTON, COJUNTO RESIDENCIAL EL PARAISO, T3A, APTO 33 LAS FUENTES 	0	ASISTENTE	CONTRATADO	2016-01-13	DOCENTE MEDIO TIEMPO	ACTIVO
1317	6127985	ALONSO OROZCO, ALBA MARINA	Venezolano 	1963-05-28	\N	\N	Femenino 	Soltero 	\N	aalonso@ubv.edu.ve 	URB. CARIBE CARABALLEDA C/GUICAIPURO QTA GEMINIS 	0	INSTRUCTOR	CONTRATADO	2012-07-16	DOCENTE MEDIO TIEMPO	ACTIVO
1318	6135730	GARZON ARDILA, HECTOR LUIS	Venezolano 	1946-06-19	\N	\N	Masculino 	Casado 	\N	hgarzon@ubv.edu.ve 	AVENIDA PRINCIPAL DE SANTA SOFIA N 25 QUINTA MI CANEY 	0	INSTRUCTOR	CONTRATADO	2013-10-14	DOCENTE MEDIO TIEMPO	ACTIVO
1319	6395044	LEON RENGIFO, LESLIE ROSA	Venezolano 	1961-11-03	\N	\N	Femenino 	Soltero 	\N	lrleon@ubv.edu.ve 	AV.LEONARDO RUIZ PINEDA.RESD.HORMOS DE CAL TORRE C,PISO 17 APTO,4 SAN AGUSTN DEL SUR 	0	INSTRUCTOR	DOCENTE	2005-02-16	DOCENTE MEDIO TIEMPO	ACTIVO
1320	6846174	GONZALEZ ROMERO, AISKEL MAGDALENA	Venezolano 	1965-05-13	\N	\N	Femenino 	Soltero 	\N	amgonzalezr@ubv.edu.ve 	CONJUNTO RESINDENCIAL MATA DE LA MIEL, BLOQUE 22, PISO 10, APTO 10-04, CARICUAO 	0	INSTRUCTOR	CONTRATADO	2016-01-11	DOCENTE MEDIO TIEMPO	ACTIVO
1321	6925672	CRUZ GUERRA, DINORAH HELENA	Venezolano 	1964-09-21	\N	\N	Femenino 	Soltero 	\N	dcruz@ubv.edu.ve 	AV LAUDELINO MEJIAS URB BARRETO Y UZCATEGUI QUINTA DELIA TRUJILLO ESTADO TRUJILLO 	0	INSTRUCTOR	DOCENTE	2004-10-01	DOCENTE MEDIO TIEMPO	ACTIVO
1322	6928613	MONTERREY ABAD, JULIO CESAR	Venezolano 	1966-09-09	\N	\N	Masculino 	Soltero 	\N	jmonterrey@ubv.edu.ve 	SAN JOSE 	0	INSTRUCTOR	DOCENTE	2012-02-15	DOCENTE MEDIO TIEMPO	ACTIVO
1323	7563795	CAMACARO DE NAVAS, LUCY CARIN	Venezolano 	1964-08-09	\N	\N	Femenino 	Soltero 	\N	lcamacaro@ubv.edu.ve 	BANCO OBRERO VEREDAD 5 CASA# 5-28 	0	AUXILIAR I	DOCENTE	2007-10-01	DOCENTE MEDIO TIEMPO	ACTIVO
1707	16108318	TAJ EL DINE EL CHAHINE, LAILA	Venezolano 	1981-10-01	\N	\N	Femenino 	Soltero 	\N	 	CARACAS 	0	INSTRUCTOR	DOCENTE	2011-09-05	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1324	7967503	ANTEPAZ, OSCAR JOSE	Venezolano 	1967-06-06	\N	\N	Masculino 	Soltero 	\N	ojantepaz@gmail.com 	CALLE RAUL LEONI,SECTOR LAS MARITAS 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE MEDIO TIEMPO	ACTIVO
1325	7997993	QUIÑONES GARCIA, JHON MANUEL 	Venezolano 	1970-10-09	\N	\N	Masculino 	Casado 	\N	jquinones@ubv.edu.ve 	URB CIUDAD CASARAPA PARCELA 19 PARTE BAJA EDIF 3 APTO PB-C GUARENA 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE MEDIO TIEMPO	ACTIVO
1326	8180457	ZAPATA MARCO, ALBERTO ENRIQUE	Venezolano 	1962-10-16	\N	\N	Masculino 	Soltero 	\N	albertozapata@ubv.edu.ve 	SABANA GRANDE SECTOR II N22 CARRERA 2 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE MEDIO TIEMPO	ACTIVO
1327	8229463	MACUARAN HERNANDEZ, FRANCISCO RAMÓN	Venezolano 	1965-07-07	\N	\N	Masculino 	Casado 	\N	fmacuaran@ubv.edu.ve 	URBANIZACION BOYACA I VEREDA 20 N 19 	1	INSTRUCTOR	DOCENTE	2007-09-25	DOCENTE MEDIO TIEMPO	ACTIVO
1328	8369356	COA PEREIRA, JESUS RAMON	Venezolano 	1961-06-17	\N	\N	Masculino 	Soltero 	\N	JESUSCOAP@HOTMAIL.COM 	VIA LA PICA SECTOR CAMPO ALEGRE N 53 	0	INSTRUCTOR	CONTRATADO	2016-01-11	DOCENTE MEDIO TIEMPO	ACTIVO
1329	8505249	VILLALOBOS DE SALAZAR, ZULLY DEL VALLE	Venezolano 	1967-01-12	\N	\N	Femenino 	Casado 	\N	 	SECTOR LA PONDEROSA CALLE SAMUEL AVENDAÑO,MUNICIPIO TUBORES 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE MEDIO TIEMPO	ACTIVO
1330	8620082	OJEDA BATISTA, GILBERTO ANTONIO	Venezolano 	1963-11-27	\N	\N	Masculino 	Soltero 	\N	 	AV JOSE GREGORIO HERNANDEZ RESIDENCIAS LA ARBOLEDA N 91  VIRGEN DEL VALLE 	0	INSTRUCTOR	DOCENTE	2011-10-17	DOCENTE MEDIO TIEMPO	ACTIVO
1331	8746004	CONTI DE SANDOVAL, PERLA YOMARA	Venezolano 	1961-06-22	\N	\N	Femenino 	Casado 	\N	pconti@ubv.edu.ve 	23 DE ENERO 	0	INSTRUCTOR	DOCENTE	2012-02-15	DOCENTE MEDIO TIEMPO	ACTIVO
1332	8881865	QUINTO MADRID, SILFRIDA ANTONIA	Venezolano 	1965-07-06	\N	\N	Femenino 	Soltero 	\N	saquinto@ubv.edu.ve 	CALLE I SANTA ELENA N 06 BARRIO LAS PIEDRITAS 	0	INSTRUCTOR	DOCENTE	2005-10-15	DOCENTE MEDIO TIEMPO	ACTIVO
1333	8915397	DUERTO ACEVEDO, YRAIDA CECILIA	Venezolano 	1961-01-05	\N	\N	Femenino 	Soltero 	\N	yduerto@ubv.edu.ve 	AV. ALEJANDRO VARGAS. RES. VILLAS DE ORAZIO CASA N 1 	1	INSTRUCTOR	DOCENTE	2005-04-15	DOCENTE MEDIO TIEMPO	ACTIVO
1334	8936536	YEPEZ ASCANIO, ASDRUBAL JESÚS	Venezolano 	1961-07-27	\N	\N	Masculino 	Soltero 	\N	yepezasdrubal@ubv.edu.ve 	RES. ALTA VISTA II TORRES A PISO 3 APTO 1 	2	INSTRUCTOR	DOCENTE	2006-04-28	DOCENTE MEDIO TIEMPO	ACTIVO
1335	9145485	CUEVAS AVELLANEDA, ELGA ZULAY	Venezolano 	1964-02-25	\N	\N	Femenino 	Soltero 	\N	ecuevas@ubv.edu.ve 	PTE NUEVO A PUERTO ESCONDIDO RES TORRE DEL OESTE PISO 10 APTO 101-B EL SILENCIO 	1	INSTRUCTOR	DOCENTE	2006-09-15	DOCENTE MEDIO TIEMPO	ACTIVO
1336	9147526	ZAPATA CRUZ, MELBA MARITZA	Venezolano 	1966-06-21	\N	\N	Femenino 	Soltero 	\N	mzapata@ubv.edu.ve 	URB. EL MARQUES, SECTOR EL CONVENTO I AV. SANZ. RESIDENCIA, ARIES. PISO 17 APTO 17-C 	1	INSTRUCTOR	DOCENTE	2007-10-16	DOCENTE MEDIO TIEMPO	ACTIVO
1337	9260345	DUGARTE PLAZA, JOSÉ ONESIMO	Venezolano 	1962-02-14	\N	\N	Masculino 	Soltero 	\N	 	URB. LAS AMERICAS CALLE 3 CON CALLE 9 CASA NRO 34 	0	INSTRUCTOR	DOCENTE	2006-09-01	DOCENTE MEDIO TIEMPO	ACTIVO
1338	9308274	ALIENDRES VASQUEZ, PEDRO LUIS	Venezolano 	1964-08-03	\N	\N	Masculino 	Soltero 	\N	paliendres@ubv.edu.ve 	URB VILLA ROSA SECTOR A CALLE 25 N 2737 	0	INSTRUCTOR	CONTRATADO	2016-06-15	DOCENTE MEDIO TIEMPO	ACTIVO
1339	9420965	ROJAS MARTINEZ, BETTYS DEL VALLE	Venezolano 	1966-10-16	\N	\N	Femenino 	Soltero 	\N	 	CALLE MARCANO SECTOR EL PIACHE MUNICIPIO GARCIA 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE MEDIO TIEMPO	ACTIVO
1340	9697172	PINTO GUEVARA, ELIZABETH OBDULIA	Venezolano 	1973-07-31	\N	\N	Femenino 	Soltero 	\N	epinto@ubv.edu.ve 	CARACAS 	1	INSTRUCTOR	DOCENTE	2010-05-01	DOCENTE MEDIO TIEMPO	ACTIVO
1341	9698726	CROES GONZALEZ, HEMMI THEODORO	Venezolano 	1973-09-10	\N	\N	Masculino 	Soltero 	\N	hcroes@ubv.edu.ve 	EDIF CATUCHE APTO 12-N PARQUE CENTRAL 	0	INSTRUCTOR	CONTRATADO	2014-09-15	DOCENTE MEDIO TIEMPO	ACTIVO
1342	9840638	MENDEZ CAMACHO, SIGLE MARBELLA	Venezolano 	1969-11-11	\N	\N	Femenino 	Soltero 	\N	SIGLEMENDEZ@HOTMAIL.COM 	SECTOR EL CANAL, BARRIO CAMPO LINDO, CASA 25-17 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE MEDIO TIEMPO	ACTIVO
1343	9857589	NAVAS GIL, LEOBALDO HUMBERTO	Venezolano 	1970-01-04	\N	\N	Masculino 	Soltero 	\N	lnavas@ubv.edu.ve 	EL ONOTO  TANQUE 	0	INSTRUCTOR	CONTRATADO	2015-03-02	DOCENTE MEDIO TIEMPO	ACTIVO
1344	9964504	DIAZ GOMEZ, VLADIMIR ANTONIO	Venezolano 	1970-02-12	\N	\N	Masculino 	Soltero 	\N	vadiazg@ubv.edu.ve 	CARACAS 	3	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE MEDIO TIEMPO	ACTIVO
1345	10335214	TOVAR MUJICA, MARIANLY GERALDINE	Venezolano 	1972-02-06	\N	\N	Femenino 	Soltero 	\N	 	QTA LIGIA MACARACUAY AV ARICHUNA 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE MEDIO TIEMPO	ACTIVO
1346	10401720	HERNANDEZ VILLEGAS, JOSE GREGORIO	Venezolano 	1970-08-12	\N	\N	Masculino 	Soltero 	\N	JADEMARIANS@YAHOO.ES 	CALLE 14, EDIF. N 51, APTO. 003, PB. LOS JARDINES DEL VALLE 	0	INSTRUCTOR	CONTRATADO	2016-01-28	DOCENTE MEDIO TIEMPO	ACTIVO
1347	10673181	GONCALVES PONTE, NELSON ROBERTO	Venezolano 	1974-07-16	\N	\N	Masculino 	Soltero 	\N	goncalvesnelson2014@gmail.com 	CALLE TACHIRA QUINTA EL ENCANTO SECTOR LA OTRA BANDA LA ASUNCION NUEVA ESPARTA CODIGO POSTAL 6311 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE MEDIO TIEMPO	ACTIVO
1348	10794564	VILLARROEL NIÑO, ALEXANDER DEL VALLE	Venezolano 	1972-11-20	\N	\N	Masculino 	Soltero 	\N	alexander.villarroel@gmail.com 	URB DIEGO LOZADA, CALLE REAL DE LOS MECEDORES, EDIF 2 PISO 7 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE MEDIO TIEMPO	ACTIVO
1349	10815859	SANCHEZ BRICEÑO, VICTOR MANUEL	Venezolano 	1971-09-20	\N	\N	Masculino 	Soltero 	\N	ROCTIV5000-VS@GMAIL.COM 	URB. BICENTENARIO DEL LIBERTADOR KM 5 DE LA CARRETERA PANAMERICANA TERRAZA 1 EDIF.02-1 FUNDAPOL 	0	AUXILIAR I	CONTRATADO	2018-06-18	DOCENTE MEDIO TIEMPO	ACTIVO
1350	10865687	ACOSTA LARA, GABRIELA  VICTORIA	Venezolano 	1972-03-28	\N	\N	Femenino 	Soltero 	\N	gabyshakty28@gmail.com 	URB LAS ACACIAS APTO 5 PISO 2 	0	AUXILIAR I	CONTRATADO	2019-02-11	DOCENTE MEDIO TIEMPO	ACTIVO
1351	10871817	RODRÍGUEZ ALAYON, JAIME ANTONIO	Venezolano 	1972-04-27	\N	\N	Masculino 	Soltero 	\N	jarodrigueza@ubv.edu.ve 	URB ARAGUANEY KM 15 EDIF LOS JABILLOS 	0	INSTRUCTOR	DOCENTE	2007-05-08	DOCENTE MEDIO TIEMPO	ACTIVO
1352	11042469	PEREZ NOGUERA, CARLOS ANDRES	Venezolano 	1973-03-26	\N	\N	Masculino 	Soltero 	\N	CAP@MUNDOCAP.COM 	AV.SAN JUAN BOSCO 2DA TRANSV. EDIF. TEREPAIMA. PISO 5, APTO 52, LA CASTELLANA 	0	INSTRUCTOR	CONTRATADO	2016-01-11	DOCENTE MEDIO TIEMPO	ACTIVO
1353	11120141	RAMOS ESCALONA , RONALD RAFAEL 	Venezolano 	1974-03-29	\N	\N	Masculino 	Soltero 	\N	rrramos@ubv.edu.ve 	AV. OESTE 16 ESQ EL CARMEN A PUENTE ARAUCA CONJUNTO RESIDENCIAL DON ELIAS TORRE B PISO 14 APTO F 	0	INSTRUCTOR	CONTRATADO	2012-09-16	DOCENTE MEDIO TIEMPO	ACTIVO
1354	11170147	PRIETO, YUDELY	Venezolano 	1971-04-28	\N	\N	Femenino 	Soltero 	\N	yuprieto@ubv.edu.ve 	CALLE COLON #23 SECTOR LAS PIEDRITAS LA SABANITA 	0	INSTRUCTOR	DOCENTE	2005-10-15	DOCENTE MEDIO TIEMPO	ACTIVO
1355	11170530	MENDOZA CARMONA, MARIA MAGDALENA	Venezolano 	1970-09-23	\N	\N	Femenino 	Soltero 	\N	 	CALLE GUIACAIPURO SECTOR INDEPENDENCIA CASA N 3 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE MEDIO TIEMPO	ACTIVO
1356	11191100	GALINDEZ SULBARAN, FELIX AURELIO	Venezolano 	1971-01-14	\N	\N	Masculino 	Soltero 	\N	fagalindez@ubv.edu.ve 	SECTOR EL ESTADIO AV 4 ENTRE CALLE 4-5  CASA  4-59 	2	INSTRUCTOR	DOCENTE	2006-09-01	DOCENTE MEDIO TIEMPO	ACTIVO
1357	11355164	TORREALBA TORRES, TAINA MIRLEN	Venezolano 	1973-01-11	\N	\N	Femenino 	Soltero 	\N	ttorrealba@ubv.edu.ve 	BARRIO SANTISIMA TRINIDAD CALLE SAGRADA FAMILIA CASA 58 	0	INSTRUCTOR	DOCENTE	2010-11-01	DOCENTE MEDIO TIEMPO	ACTIVO
1358	11664148	URBINA HERNANDEZ, LOURDES DEL VALLE	Venezolano 	1973-09-09	\N	\N	Femenino 	Soltero 	\N	ldurbina@ubv.edu.ve 	CARAPITA, DIAGONAL ALA IGLESIA SAN JOAQUIN Y SANTA ANA 12 	2	INSTRUCTOR	DOCENTE	2005-04-04	DOCENTE MEDIO TIEMPO	ACTIVO
1360	11772793	CAYAMA, JAIRO MANUEL	Venezolano 	1973-12-13	\N	\N	Masculino 	Soltero 	\N	jcayama@ubv.edu.ve 	SANTA ANA DE PARAGUANA 	0	INSTRUCTOR	DOCENTE	2011-03-01	DOCENTE MEDIO TIEMPO	ACTIVO
1361	11774116	MACHADO, DAMELIS CIPRIANA	Venezolano 	1970-04-19	\N	\N	Femenino 	Casado 	\N	dmachado@ubv.edu.ve 	GUARAPICHE II CALLE GUACAMAYO, CASA II D2, MATURIN MONAGAS 	0	INSTRUCTOR	DOCENTE	2007-05-15	DOCENTE MEDIO TIEMPO	ACTIVO
1362	11831034	MARCANO VELASQUEZ, CARMEN TRINIDAD	Venezolano 	1974-05-04	\N	\N	Femenino 	Soltero 	\N	cmarcano@ubv.edu.ve 	CASA N 55, CALLE FALCON. 	0	INSTRUCTOR	CONTRATADO	2015-01-01	DOCENTE MEDIO TIEMPO	ACTIVO
1363	11916689	URBINA PACHECO, MARÍA EUGENIA	Venezolano 	1974-12-28	\N	\N	Femenino 	Soltero 	\N	meurbina@ubv.edu.ve 	URB. NUEVA CASARAPA. SECTOR CONJUNTO RESIDENCIAL LA RIBERA. EDIF:7B. APTO:33 	0	AUXILIAR I	DOCENTE	2007-09-17	DOCENTE MEDIO TIEMPO	ACTIVO
1364	11934135	ORTA, JUAN LUIS	Venezolano 	1973-03-29	\N	\N	Masculino 	Soltero 	\N	juorta@ubv.edu.ve 	CALLE ARISMENDI RESIDENCIAS HILDA. PSIO 4-A. EL PARAISO 	2	INSTRUCTOR	DOCENTE	2005-10-10	DOCENTE MEDIO TIEMPO	ACTIVO
1365	11978648	RIVAS GIL, MAURICIO JESUS	Venezolano 	1973-05-25	\N	\N	Masculino 	Soltero 	\N	mjrivas@ubv.edu.ve 	SANTA ROSA DSE LIMA CALLA A CASA ZULLY. LAS MINAS DE BARUTA 	0	INSTRUCTOR	CONTRATADO	2017-06-26	DOCENTE MEDIO TIEMPO	ACTIVO
1366	12069735	ORDAZ CASTRO, FELIX DE JESUS	Venezolano 	1973-12-03	\N	\N	Masculino 	Soltero 	\N	ordaz.feliz@gmail.com 	EDIF EL CASQUILLO, PISO 3 APTO 7, SAN AGUSTIN DEL SUR 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE MEDIO TIEMPO	ACTIVO
1367	12188508	YARI RAGA, DOINES DEL CARMEN	Venezolano 	1974-06-23	\N	\N	Femenino 	Casado 	\N	ddyari@ubv.edu.ve 	MANZANA E N 6 LAS CASITAS 	2	INSTRUCTOR	DOCENTE	2005-03-01	DOCENTE MEDIO TIEMPO	ACTIVO
1368	12293445	MONSALVE SARMIENTO, JUAN CARLOS	Venezolano 	1975-06-24	\N	\N	Masculino 	Casado 	\N	JUANCMONSALVES@GMAIL.COM 	RESD. EL NARANJAL TORRE A, APTO 185 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE MEDIO TIEMPO	ACTIVO
1369	12324221	RUIZ APONTE, YARITZA JOSEFINA	Venezolano 	1976-08-29	\N	\N	Femenino 	Soltero 	\N	 	SECTOR VALLE VERDE AL FINAL CASA 36 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE MEDIO TIEMPO	ACTIVO
1370	12325307	HURTADO ESPINOZA, MARÍA TERESA	Venezolano 	1974-12-26	\N	\N	Femenino 	Soltero 	\N	mthurtado@ubv.edu.ve 	CALLE SUCRE, CASA N 29-33 	1	INSTRUCTOR	DOCENTE	2007-10-16	DOCENTE MEDIO TIEMPO	ACTIVO
1371	12377373	CHACON GUERRERO, DOUGLAS ANTONIO	Venezolano 	1975-06-13	\N	\N	Masculino 	Soltero 	\N	dachacon@ubv.edu.ve 	CALLE VENEZUELA N 1009 SECTOR CAÑAVERAL LOS MAGALLANES 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE MEDIO TIEMPO	ACTIVO
1372	12401462	LAGUNA BAUTISTA, JIMMY ALEXANDER	Venezolano 	1977-08-19	\N	\N	Masculino 	Soltero 	\N	lagunajimmy@gmail.com 	LOS FRAILESDE CATIA, CALLE EL TRANVIA 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE MEDIO TIEMPO	ACTIVO
1373	12569667	PADRON CHACON, CARLOS ALBERTO	Venezolano 	1977-02-25	\N	\N	Femenino 	Soltero 	\N	cpadron@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2011-03-16	DOCENTE MEDIO TIEMPO	ACTIVO
1374	12814503	TARAZONA HERNANDEZ, CONSUELO	Venezolano 	1965-09-27	\N	\N	Femenino 	Divorciado 	\N	cotarazona@ubv.edu.ve 	AVENIDA ESPAÑA U-80 PUEBLO NUEVO SAN CRISTOBAL 	1	INSTRUCTOR	CONTRATADO	2012-09-16	DOCENTE MEDIO TIEMPO	ACTIVO
1375	12854279	PEÑALOZA BRAVO, RAUL ENRIQUE	Venezolano 	1975-12-28	\N	\N	Masculino 	Soltero 	\N	rpenaloza@ubv.edu.ve 	AV LECUNA RES PARQUE  CENTRAL CARACAS 	0	INSTRUCTOR	CONTRATADO	2015-02-02	DOCENTE MEDIO TIEMPO	ACTIVO
1376	12902304	CEBALLO LOPEZ, RAMON ARCENIO	Venezolano 	1975-03-09	\N	\N	Masculino 	Soltero 	\N	 	URB MERECURE CALLE 7 CASA 17 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE MEDIO TIEMPO	ACTIVO
1377	13046506	HENRÍQUEZ RODRIGUEZ, CILA MARIA	Venezolano 	1973-05-02	\N	\N	Femenino 	Soltero 	\N	chenriquez@ubv.edu.ve 	ISLA DE LA CULEBRA, CALLE LA MONTAÑITA, 	0	INSTRUCTOR	DOCENTE	2007-09-17	DOCENTE MEDIO TIEMPO	ACTIVO
1378	13145684	TORRES NIÑO, BEATRIZ KARIN YANETH	Venezolano 	1977-01-26	\N	\N	Femenino 	Soltero 	\N	arqkarimtorres@gmail.com 	AV.YANEZ CASA 43, SAN BERNADINO 	0	INSTRUCTOR	CONTRATADO	2017-01-09	DOCENTE MEDIO TIEMPO	ACTIVO
1379	13609085	GARCIA CASTRO, YAMYR	Venezolano 	1977-03-14	\N	\N	Femenino 	Soltero 	\N	 	AV BOGOTA CON 7ma TRANSVERSAL MANZANA B 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE MEDIO TIEMPO	ACTIVO
1380	13657301	ARZOLAY DE BRIZUELA, MARILENE DEL CARMEN	Venezolano 	1978-11-03	\N	\N	Femenino 	Casado 	\N	 	CALLE GUZMAN BLANCO CASA 44 SECTOR CASCO HISTORICO 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE MEDIO TIEMPO	ACTIVO
1381	13721450	PATIÑO BRAVO, MARIELA LOURDES	Venezolano 	1977-09-19	\N	\N	Femenino 	Soltero 	\N	 	CALLE BERMUDEZ  SAN CARLOS N 48 	0	INSTRUCTOR	CONTRATADO	2019-06-03	DOCENTE MEDIO TIEMPO	ACTIVO
1382	13815825	AZOCAR RINCONES, RUBILMARY	Venezolano 	1979-08-11	\N	\N	Femenino 	Casado 	\N	razocar@ubv.edu.ve 	CALLE VALMORE RODRIGUEZ URB ADRES ELOI BLANCO CASA N 30 PUNTA DE MATA 	0	INSTRUCTOR	DOCENTE	2007-10-20	DOCENTE MEDIO TIEMPO	ACTIVO
1383	14410432	MARTÍNEZ DÍAZ, NELSON	Venezolano 	1980-02-28	\N	\N	Masculino 	Soltero 	\N	nmartinezd@ubv.edu.ve 	URB LOS COQUITOS SECTOR II VEREDA 22 N8 	1	AUXILIAR I	DOCENTE	2007-11-01	DOCENTE MEDIO TIEMPO	ACTIVO
1384	14437959	MONTOYA CORTEZ, VANESSA GRAMMY	Venezolano 	1981-03-03	\N	\N	Femenino 	Soltero 	\N	 	URB.GUASIMAL MANZANA 6 TORRE 4, PB APTO 07 	0	INSTRUCTOR	CONTRATADO	2019-05-31	DOCENTE MEDIO TIEMPO	ACTIVO
1385	14922006	URDANETA VEGA, AMANDA 	Venezolano 	1980-10-25	\N	\N	Femenino 	Soltero 	\N	aurdanetav@ubv.edu.ve 	AV PPAL DE LAS PALMAS EDIF MERCEDES PISO 1 APTO 4 URB LAS PALMAS 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE MEDIO TIEMPO	ACTIVO
1386	14981090	SOLANO LINAREZ, MILEIDY ROSA	Venezolano 	1980-03-29	\N	\N	Femenino 	Soltero 	\N	msolano@ubv.edu.ve 	SECTOR LA CARANTA, CALLE LAS LAJAS CASA NRO 3 PAMPATAR 	0	INSTRUCTOR	CONTRATADO	2016-06-15	DOCENTE MEDIO TIEMPO	ACTIVO
1387	15040419	RIVAS HIDALGO, VICTOR HARRISON	Venezolano 	1982-04-06	\N	\N	Masculino 	Soltero 	\N	vhrivas@ubv.edu.ve 	VIA PPAL EL JUNQUITO KM. 16 CASA N 12 CARACAS 	0	INSTRUCTOR	CONTRATADO	2012-02-16	DOCENTE MEDIO TIEMPO	ACTIVO
1388	15389750	VIEZ ESCALONA, MERLIN LIGMARI	Venezolano 	1982-12-11	\N	\N	Femenino 	Soltero 	\N	merlinviez@gmail.com 	CALLE NORTE 2 CON OESTE- EDIF BOULEVARD PISO 2 APTO 22 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE MEDIO TIEMPO	ACTIVO
1389	15415393	YANEZ DIAZ, LISMARYS JOSEFINA	Venezolano 	1980-12-12	\N	\N	Femenino 	Soltero 	\N	YLISMARYS@GMAIL.COM 	CARAPITA CALLE EL PROGRESO CASA 227 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE MEDIO TIEMPO	ACTIVO
1390	15496564	COELLO RAMIREZ, ELEAZAR ERNESTO SALVADO	Venezolano 	1980-04-15	\N	\N	Femenino 	Soltero 	\N	ELEAZAR.COELLO.ARQ@GMAIL.COM 	AV. JOSE ANGEL LAMAS, SAN MARTIN PARROQUIA SAN JUAN CARACAS-DF 	0	INSTRUCTOR	CONTRATADO	2017-01-09	DOCENTE MEDIO TIEMPO	ACTIVO
1391	15592421	SMITH GONZALEZ, CAROLINA DEL VALLE	Venezolano 	1982-06-25	\N	\N	Femenino 	Soltero 	\N	cvsmith@ubv.edu.ve 	URB. EL PLACER MANZANA 1 CASA #17 	0	ASISTENTE	DOCENTE	2004-09-06	DOCENTE MEDIO TIEMPO	ACTIVO
1392	15638298	LISCANO SOLIS, MARIANA DEL CARMEN	Venezolano 	1983-06-16	\N	\N	Femenino 	Soltero 	\N	 	EL PERU VIEJO C/C LAS FLORES CASA N 07 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE MEDIO TIEMPO	ACTIVO
1393	15676642	AMUNDARAY, JEAN CARLOS	Venezolano 	1981-12-09	\N	\N	Masculino 	Soltero 	\N	 	URBANIZACION BICENTENARIO CALLE 1 AVENIDA 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE MEDIO TIEMPO	ACTIVO
1395	15701106	GONZALEZ CONTRERAS, FRANCISCO ESAU	Venezolano 	1981-10-26	\N	\N	Masculino 	Soltero 	\N	fegonzalez@ubv.edu.ve 	URB.TERRAZAS DE CUA, VIA TACATA. MANZANA O-N CASA O-3 	0	INSTRUCTOR	CONTRATADO	2016-06-15	DOCENTE MEDIO TIEMPO	ACTIVO
1396	15775973	QUINTERO ARELLANO, LEIDY CAROLINA	Venezolano 	1983-08-20	\N	\N	Femenino 	Soltero 	\N	ladyqblue@gmail.com 	CALLE 6 CASA ESTE N 118 URBANIZACION COTOPERIZ II SAN JUAN BAUTISTA NUEVA ESPARTA ZONA POSTAL 6301 	1	INSTRUCTOR	CONTRATADO	2018-01-22	DOCENTE MEDIO TIEMPO	ACTIVO
1397	15864109	MONTILLA MEJIA, YENMI YAKELIN	Venezolano 	1978-10-04	\N	\N	Femenino 	Soltero 	\N	yymontillam@ubv.edu.ve 	URB. LAS DARAS. AV. 41 CON CALLE C. CASA 24 	0	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE MEDIO TIEMPO	ACTIVO
1398	15947903	BOADAS HENRIQUEZ, ANA MARIA	Venezolano 	1980-03-18	\N	\N	Femenino 	Soltero 	\N	anamariaboadas@gmail.com 	RESIDENCIA EL ENCANTADO HUMBOLT PISO 4 	0	INSTRUCTOR	CONTRATADO	2018-09-24	DOCENTE MEDIO TIEMPO	ACTIVO
1399	15990125	ALZUALDE DE PEÑA, ESTER NOEMI	Venezolano 	1984-02-11	\N	\N	Femenino 	Casado 	\N	 	AV PRINCIPAL EL CEMENTERIO AV LAS PALMAS 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE MEDIO TIEMPO	ACTIVO
1400	16196037	ANTICHAN RIVAS, MIREILLE DE LOURDES GABRIELA	Venezolano 	1983-04-12	\N	\N	Femenino 	Soltero 	\N	mdantichan@ubv.edu.ve 	CALLE 11, ENTRE AV, OLLARVIDES Y GENARAL RIERA 	2	INSTRUCTOR	DOCENTE	2007-06-04	DOCENTE MEDIO TIEMPO	ACTIVO
1401	16370390	ROMERO MORENO, FRANCISCO JAVIER 	Venezolano 	1983-04-12	\N	\N	Masculino 	Soltero 	\N	fjromero@ubv.edu.ve 	CALLE CHOLULA RES CHOLULA PISO 1 APTO 2-A 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE MEDIO TIEMPO	ACTIVO
1402	16411857	GIL SALAZAR, JESUS ANTONIO	Venezolano 	1985-05-02	\N	\N	Masculino 	Soltero 	\N	jesus.antonio.gil.16@gmail.com 	EL PARAISO CALLE REAL LAS QUINTAS AV. LOS LAURELES 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE MEDIO TIEMPO	ACTIVO
1403	16526925	MURO OCHOA, GERWIN OCHUN	Venezolano 	1900-01-01	\N	\N	Masculino 	Soltero 	\N	gmuro@ubv.edu.ve 	CARACAS 	0	AUXILIAR I	DOCENTE	2011-03-16	DOCENTE MEDIO TIEMPO	ACTIVO
1404	16753291	MORALES DE HERRERA, MARIA PASCUALINA	Venezolano 	1983-11-06	\N	\N	Femenino 	Casado 	\N	 	URB VILLA PASTORA, CENTRO SECTOR CALLE 38 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE MEDIO TIEMPO	ACTIVO
1405	17116315	URBINA DIAZ, JESUA ONER	Venezolano 	1985-12-31	\N	\N	Femenino 	Casado 	\N	urbina.jesua@gmail.com 	AV LOS CARMENES EL CEMENTERIO, QTA EDA 44-16 	0	INSTRUCTOR	CONTRATADO	2019-02-14	DOCENTE MEDIO TIEMPO	ACTIVO
1406	17220424	SUAREZ PEREZ, SUSANA LUCELIA	Venezolano 	1985-10-04	\N	\N	Femenino 	Soltero 	\N	susanasuarez0410@gmail.com 	CONJUNTO RESIDENCIAL PARQUE CENTRAL-EDIF TACAGUA PISO 18 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE MEDIO TIEMPO	ACTIVO
1407	17944726	BRICEÑO CARDONA, YULINAN  COROMOTO	Venezolano 	1986-11-20	\N	\N	Femenino 	Soltero 	\N	LIYUNAN_86@HOTMAIL.COM 	SECTOR EL SAMAN, COMUNIDAD LA TAPA DE PIEDRA 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE MEDIO TIEMPO	ACTIVO
1408	18304578	MARTIN GOMEZ, JASMIN DE JESUS	Venezolano 	1988-03-31	\N	\N	Femenino 	Soltero 	\N	 	AV VICTORIA URB LAS ACACIAS EDIF EL TROVADOR PISO 5 APTO 24 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE MEDIO TIEMPO	ACTIVO
1409	18630854	GARCIA COLINA, YOSELIN DE LA GUADALUPE	Venezolano 	1987-10-31	\N	\N	Femenino 	Soltero 	\N	YOSELYN3110@GMAIL.COM 	URB JUANA LA AVANZADORA, MICROPARCELA 3 CASA J-23 18 ZONA INDUSTRIAL 	0	INSTRUCTOR	CONTRATADO	2017-11-01	DOCENTE MEDIO TIEMPO	ACTIVO
1410	18814102	GUTIERREZ MENDOZA, ORESTES MIGUEL	Venezolano 	1986-06-09	\N	\N	Masculino 	Soltero 	\N	 	AV BARALT DOS PILITAS CUARTEL DE SAN CARLOS EDF PISO 1 APTO 103 PARROQUIA ALTA GRACIA 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE MEDIO TIEMPO	ACTIVO
1411	19432987	JIMENEZ NIEBLA, LUIS ALFREDO	Venezolano 	1990-05-04	\N	\N	Masculino 	Soltero 	\N	xxjimenezluis@gmail.com 	AV JOSE GREGORIO HERNANDEZ AV NORTE 16 ESQUINA COLA DE PATO DTTO CAPITAL 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE MEDIO TIEMPO	ACTIVO
1412	19649782	CLAVO BARRIOS, YANDERG ALBERTO	Venezolano 	1990-05-01	\N	\N	Masculino 	Soltero 	\N	arqyanubv01@gmail.com 	URB BOSQUE VERDE,  CASA NRO 75 SECTOR VEGA ARRIBA 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE MEDIO TIEMPO	ACTIVO
1413	19766004	CALLES URDANETA, ANDRES DAVID	Venezolano 	1989-07-23	\N	\N	Masculino 	Soltero 	\N	acalles@fmed.luz.edu.ve 	URB.OPS, TORRE 5, APTO 3-1 SAN ANTONIO 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE MEDIO TIEMPO	ACTIVO
1414	20111946	MATUTE RODRIGUEZ, JUAN SALVADOR	Venezolano 	1992-08-31	\N	\N	Masculino 	Soltero 	\N	 	CALLE LA MARINA CRUCE CON CALLE INDEPENDENCIA SECTOR LOS COCOS 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE MEDIO TIEMPO	ACTIVO
1415	20232109	BRAVO RONDON, CELMARY GERALDINE	Venezolano 	1991-05-31	\N	\N	Femenino 	Soltero 	\N	celmarbravo91@gmail.com 	URB.SERAFIN CEDEÑO CALLE 5, CASA NRO 31 	0	INSTRUCTOR	CONTRATADO	2017-07-10	DOCENTE MEDIO TIEMPO	ACTIVO
1416	20919620	CABRERA MEJIAS, MARIANNYS JOSEFINA	Venezolano 	1991-12-18	\N	\N	Femenino 	Soltero 	\N	 	URB.LOS OLIVOS VILLA TOLEDO VIA LA TOSCANIA 	0	INSTRUCTOR	CONTRATADO	2018-01-22	DOCENTE MEDIO TIEMPO	ACTIVO
1417	22906851	MARTINEZ DUARTE, ANYESCA KATIUSCA	Venezolano 	1993-10-01	\N	\N	Femenino 	Soltero 	\N	anyesca-m@gmail.com 	URB SAN MARTIN CALLE SEVILLA NRO 7 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE MEDIO TIEMPO	ACTIVO
1418	24887658	BONILLA  MORALES, JOSE JAVIER	Venezolano 	1971-04-01	\N	\N	Masculino 	Soltero 	\N	jjbonilla@ubv.edu.ve 	CARACAS 	4	INSTRUCTOR	DOCENTE	2011-03-16	DOCENTE MEDIO TIEMPO	ACTIVO
1419	82279660	ALCIME, LULLY	Extranjero 	1978-11-23	\N	\N	Masculino 	Soltero 	\N	lalcime@ubv.edu.ve 	CCS 	0	AUXILIAR I	DOCENTE	2006-11-13	DOCENTE MEDIO TIEMPO	ACTIVO
1420	82296978	CHEN, HUI YEN	Extranjero 	1975-10-19	\N	\N	Femenino 	Casado 	\N	hychen@ubv.edu.ve 	CALLE LA GUARITA QUINTA MILAGROS URB CHUAO CARACAS 	2	INSTRUCTOR	DOCENTE	2007-04-30	DOCENTE MEDIO TIEMPO	ACTIVO
1421	84480765	GONZALEZ MUÑOZ, YUNIESKY	Extranjero 	1983-08-10	\N	\N	Masculino 	Casado 	\N	yunieskyqm@yahoo.es 	URB 10 DE MARZO AV CARLOS SOULETTE APTO 705 PISO 7 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE MEDIO TIEMPO	ACTIVO
1422	643645	GUERRA GALENO, FREDDY ORLANDO	Venezolano 	1949-04-21	\N	\N	Masculino 	Soltero 	\N	fguerra@ubv.edu.ve 	ESQUINAS DOS PILITAS A PORTILLO RESIDENCIAS FATIMA 	2	INSTRUCTOR	DOCENTE	2006-10-16	DOCENTE TIEMPO COMPLETO	ACTIVO
1423	945519	TORO JIMÉNEZ, FERMIN	Venezolano 	1933-10-26	\N	\N	Masculino 	Soltero 	\N	ftoro@ubv.edu.ve 	CARACAS 	0	TITULAR	DOCENTE	2010-01-01	DOCENTE TIEMPO COMPLETO	ACTIVO
1424	1747281	SALAZAR ILARRAZA, LIMBER ANTONIO	Venezolano 	1940-03-24	\N	\N	Masculino 	Soltero 	\N	lasalazar@ubv.edu.ve 	URB. BOSQUE DE LA LAGUNA, CONJ. JABILLO, J-12 	1	INSTRUCTOR	CONTRATADO	2012-11-01	DOCENTE TIEMPO COMPLETO	ACTIVO
1425	2553973	HERNANDEZ LOPEZ, DANIEL ANTONIO	Venezolano 	1954-05-05	\N	\N	Masculino 	Soltero 	\N	dahernandezl@ubv.edu.ve 	TRES ISLAS 	1	ASISTENTE	DOCENTE	2015-07-06	DOCENTE TIEMPO COMPLETO	ACTIVO
1426	2609451	TORRES DE RODRIGUEZ, FREDDZIA AMELY	Venezolano 	1950-12-20	\N	\N	Femenino 	Casado 	\N	ftorres@ubv.edu.ve 	AV.6 QUINTA TOSCANA URB ALTO PRADO BARUTA 	0	AGREGADO	CONTRATADO	2017-01-09	DOCENTE TIEMPO COMPLETO	ACTIVO
1427	2767322	MISTICONI DI FELICE, FRANCESCO	Venezolano 	1957-12-31	\N	\N	Masculino 	Soltero 	\N	fmisticoni@ubv.edu.ve 	AV ROMULO GALLEGOS RESIDENCIAS DEL ESTE TORRE B PISO 7 APARTAMENTO 19 SEBUCAN 	1	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE TIEMPO COMPLETO	ACTIVO
1428	2879838	VILLALOBOS, RAFAEL ANTONIO	Venezolano 	1940-01-03	\N	\N	Masculino 	Soltero 	\N	rvillalobos@ubv.edu.ve 	AV. 11, N 6-74 SINAMANCA MUNICIPO PAEZ 	0	AUXILIAR III	DOCENTE	2007-05-01	DOCENTE TIEMPO COMPLETO	ACTIVO
1429	2943644	SAPENE  LANDER, FRANCISCO	Venezolano 	1945-12-02	\N	\N	Masculino 	Soltero 	\N	fsapene@ubv.edu.ve 	CCS 	0	INSTRUCTOR	DOCENTE	2011-01-01	DOCENTE TIEMPO COMPLETO	ACTIVO
1430	2950016	ZAMBRANO PEREZ, ROQUE ILDEFONZO	Venezolano 	1942-10-20	\N	\N	Masculino 	Soltero 	\N	rizambranop@ubv.edu.ve 	CARACAS 	0	AGREGADO	DOCENTE	2008-04-21	DOCENTE TIEMPO COMPLETO	ACTIVO
1431	3242881	BLANCA MARGARITA, FLORES PEREZ	Venezolano 	1947-03-02	\N	\N	Femenino 	Viudo 	\N	bflores@ubv.edu.ve 	AV. PPAL LA FUENTES. RES TROPICAL. P 2. N 8 	0	INSTRUCTOR	CONTRATADO	2014-09-17	DOCENTE TIEMPO COMPLETO	ACTIVO
1432	3550336	YANEZ CASNEIRO, JOSÉ LUIS	Venezolano 	1948-03-28	\N	\N	Masculino 	Casado 	\N	jyanez@ubv.edu.ve 	CALLE CANTAURA, CASA #48 	0	INSTRUCTOR	DOCENTE	2007-06-01	DOCENTE TIEMPO COMPLETO	ACTIVO
1433	3725072	BARRETO GARCIA, MANUEL ENRIQUE	Venezolano 	1951-10-24	\N	\N	Masculino 	Soltero 	\N	mbarreto@ubv.edu.ve 	CALLE BERMUDEZ N22-15 LOS ROSALES 	0	INSTRUCTOR	DOCENTE	2004-05-15	DOCENTE TIEMPO COMPLETO	ACTIVO
1434	3887587	ALVAREZ FIGUEROA, MARIA AMELIA	Venezolano 	1953-07-13	\N	\N	Femenino 	Soltero 	\N	malvarezf@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2010-12-01	DOCENTE TIEMPO COMPLETO	ACTIVO
1435	3936912	JOHN, YELITZA	Venezolano 	1956-07-24	\N	\N	Femenino 	Soltero 	\N	yjohn@ubv.edu.ve 	LAS MINAS DE BARUTA CONJUNTO RESIDENSIAL LAS DANIELAS. 	0	INSTRUCTOR	DOCENTE	2004-09-09	DOCENTE TIEMPO COMPLETO	ACTIVO
1436	3977279	RENGIFO AVENDAÑO, RAMON ANSELMO	Venezolano 	1952-04-21	\N	\N	Masculino 	Soltero 	\N	rrengifo@ubv.edu.ve 	CONJ. RES. EL PARAISO SEGUNDA ETAPA TORRE A APTO 21 	0	INSTRUCTOR	CONTRATADO	2015-03-02	DOCENTE TIEMPO COMPLETO	ACTIVO
1437	3978415	HERNANDEZ TRUJILLO, ABDON  RICARDO	Venezolano 	1952-09-12	\N	\N	Masculino 	Soltero 	\N	 	CCS 	0	ASOCIADO	DOCENTE	2011-01-01	DOCENTE TIEMPO COMPLETO	ACTIVO
1438	4167468	OVALLES FALCON, OMAR JOSE	Venezolano 	1954-10-18	\N	\N	Masculino 	Soltero 	\N	ojovalles@ubv.edu.ve 	CARACAS 	0	TITULAR	DOCENTE	2011-01-01	DOCENTE TIEMPO COMPLETO	ACTIVO
1439	4179318	MORILLO ROJAS, MANUEL DE JESÚS 	Venezolano 	1954-12-25	\N	\N	Masculino 	Casado 	\N	mdmorillo@ubv.edu.ve 	CALLE MIRANDA #19 SECTOR BOLÍVAR 	0	INSTRUCTOR	CONTRATADO	2012-09-16	DOCENTE TIEMPO COMPLETO	ACTIVO
1440	4248113	BRAVO GUARAMATO, OSCAR BRAULIO	Venezolano 	1956-03-26	\N	\N	Masculino 	Casado 	\N	obbravo@ubv.edu.ve 	CONJUNTO RESIDENCIAL RAMO VERDE EDIF MARITZA PISO 6 APTO D-64 LOS TEQUES 	2	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE TIEMPO COMPLETO	ACTIVO
1441	4268396	ANGULO RENDON, TAMARA ELIZABETH	Venezolano 	1953-03-15	\N	\N	Femenino 	Divorciado 	\N	TEAR53VE@YAHOO.ES 	CHARAIMA SAN RAFAEL RESIDENCIAS TIAMAR PISO 1 APTO 1-H PORLAMAR 	0	ASISTENTE	CONTRATADO	2018-06-18	DOCENTE TIEMPO COMPLETO	ACTIVO
1442	4524101	MONTES DE OCA, KEILA ROSA	Venezolano 	1955-03-29	\N	\N	Femenino 	Soltero 	\N	kmontes@ubv.edu.ve 	AV. CORO RES. EL CUJI, TORRE 3,PRIMER PISO,APTO. 3-1D,PTO. FIJO,FALCON 	0	INSTRUCTOR	DOCENTE	2006-11-15	DOCENTE TIEMPO COMPLETO	ACTIVO
1443	4595529	BONALDE MORENO, JESÚS AMADO	Venezolano 	1955-01-09	\N	\N	Masculino 	Casado 	\N	jbonalde@ubv.edu.ve 	CALLE 3, QTA. AMABEL URB. VISTA HERMOSA, CIUDAD BOLIVAR 	0	INSTRUCTOR	DOCENTE	2005-04-15	DOCENTE TIEMPO COMPLETO	ACTIVO
1444	4601882	MORALES, RAFAEL	Venezolano 	1954-10-21	\N	\N	Masculino 	Casado 	\N	ramorales@ubv.edu.ve 	URB CASCO HISTORICO  CALLE EL PILAR  CASA # 32 	0	AUXILIAR I	DOCENTE	2004-09-01	DOCENTE TIEMPO COMPLETO	ACTIVO
1445	4652811	RODRIGUEZ DIAZ, FEBRES RAMON	Venezolano 	1957-08-20	\N	\N	Masculino 	Casado 	\N	 	NUEVA ESPARTA 	0	AUXILIAR III	CONTRATADO	2018-06-18	DOCENTE TIEMPO COMPLETO	ACTIVO
1446	4984487	LÓPEZ DE RUIZ, SONIA MARGARITA	Venezolano 	1957-08-16	\N	\N	Femenino 	Soltero 	\N	slopez@ubv.edu.ve 	RES. LOS TURPIALES TORRE B APTO. PB-3 SABANITA 	0	INSTRUCTOR	DOCENTE	2005-04-01	DOCENTE TIEMPO COMPLETO	ACTIVO
1447	5312392	LOGREIRA LINARES, FERNANDO ENRIQUE	Venezolano 	1946-02-27	\N	\N	Masculino 	Soltero 	\N	flogreira@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2011-01-10	DOCENTE TIEMPO COMPLETO	ACTIVO
1448	5554802	PACHECO BROWN, TEOFILO BELTRAN	Venezolano 	1960-05-06	\N	\N	Masculino 	Soltero 	\N	tbpacheco@ubv.edu.ve 	CALLE CARONI QTA VIRGINIA URB VISTA HERMOSA 	0	AUXILIAR I	DOCENTE	2007-11-01	DOCENTE TIEMPO COMPLETO	ACTIVO
1449	5885983	CROQUER BOLIVAR, NORMA COROMOTO	Venezolano 	1957-03-11	\N	\N	Femenino 	Soltero 	\N	 	URB LIDICE CALLE LOS TEQUES CASA 65-48 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE TIEMPO COMPLETO	ACTIVO
1450	5887291	PEÑA PORRAS, HIRAM OSWALDO	Venezolano 	1960-06-30	\N	\N	Masculino 	Soltero 	\N	MOVHOPP@GMAIL.COM 	URB.SAN LUIS AV.PRINCIPAL EDIF.ATLANTA PISO 6 APRT.6-A 	0	AUXILIAR I	CONTRATADO	2017-09-25	DOCENTE TIEMPO COMPLETO	ACTIVO
1451	5890103	CASTELLANOS BRANDES, FREDDY JOSE	Venezolano 	1960-08-31	\N	\N	Masculino 	Soltero 	\N	fcastellanos@ubv.edu.ve 	ESQUINA DE PELOTA EDIF. FETRASALUD. APTO 6-1. CARAVAS 	1	INSTRUCTOR	DOCENTE	2010-09-16	DOCENTE TIEMPO COMPLETO	ACTIVO
1452	5964681	SANTANA MATA, JORGE LUIS 	Venezolano 	1960-09-28	\N	\N	Masculino 	Soltero 	\N	jsantana@ubv.edu.ve 	UD-4 SECTOR MUCURITASBLOQUE 13 APTO 0007 PB CARICUAO 	0	INSTRUCTOR	CONTRATADO	2012-09-16	DOCENTE TIEMPO COMPLETO	ACTIVO
1453	5978413	TORRES HUNEL, SIMON	Venezolano 	1961-10-17	\N	\N	Masculino 	Casado 	\N	storres@ubv.edu.ve 	URB LA MACARENA, MANZANA NCASA N 4 	2	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE TIEMPO COMPLETO	ACTIVO
1454	6004291	ROJAS BRICEÑO, ARTURO ANTONIO	Venezolano 	1960-09-29	\N	\N	Masculino 	Soltero 	\N	 	URB. JUAN PABLO II, RESD. PARQUE III, ALA I PISO 14 APTO. 1E05, MONTALBAN 	0	INSTRUCTOR	DOCENTE	2007-10-08	DOCENTE TIEMPO COMPLETO	ACTIVO
1455	6090815	HERNANDEZ OSTA, ARACELIS COROMOTO	Venezolano 	1962-07-07	\N	\N	Femenino 	Soltero 	\N	achernandez@ubv.edu.ve 	CARACAS 	0	ASISTENTE	DOCENTE	2010-12-01	DOCENTE TIEMPO COMPLETO	ACTIVO
1456	6115928	HERNÁNDEZ VALLEN DE MONCANUT, ANA MARIA	Venezolano 	1963-04-24	\N	\N	Femenino 	Soltero 	\N	amhernandezv@ubv.edu.ve 	URB.VALLE ABAJO EDF. ULTRAMAR,PISO 4, APTO. 8 AVD.ORINOCO SECTOR LOS SIMBOLOS 	0	INSTRUCTOR	DOCENTE	2004-02-25	DOCENTE TIEMPO COMPLETO	ACTIVO
1457	6181090	BOURNAT ANDRADE, INGRID LORENA	Venezolano 	1966-02-10	\N	\N	Femenino 	Soltero 	\N	gerundiofront@gmail.com 	URB.SANTA MONICA, EDIF.CENTAURO TORRE B,PISO 4 APTO-4-B 	0	INSTRUCTOR	CONTRATADO	2018-01-22	DOCENTE TIEMPO COMPLETO	ACTIVO
1458	6200863	SANCHEZ, OSCAR ORLANDO	Venezolano 	1965-03-07	\N	\N	Masculino 	Soltero 	\N	 	SANTA ROSALIA TERCERA CALLE DEL TRIANGULO CASA21-3 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE TIEMPO COMPLETO	ACTIVO
1459	6404489	GONZALEZ ESCALONA, WILSON JOSÉ	Venezolano 	1965-12-21	\N	\N	Masculino 	Soltero 	\N	wjgonzalez@ubv.edu.ve 	CALLE 24 DE JULIO 2DA. TRANSV. EDF. MONS.LUIS MARIA 	3	INSTRUCTOR	DOCENTE	2008-01-01	DOCENTE TIEMPO COMPLETO	ACTIVO
1460	6433972	CARREÑO, CARLOS ALFREDO	Venezolano 	1961-11-24	\N	\N	Masculino 	Soltero 	\N	carlitoscar2021@gmail.com 	LOMAS DEL AVILA-PETARE APTO 8-A PISO 8 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE TIEMPO COMPLETO	ACTIVO
1461	6549001	BLAK BORGES, TRINIDAD DE JESUS	Venezolano 	1962-04-16	\N	\N	Femenino 	Soltero 	\N	tblak@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2011-09-15	DOCENTE TIEMPO COMPLETO	ACTIVO
1462	6562897	TRAVIESO LUGO, FERNANDO JOSE	Venezolano 	1962-11-11	\N	\N	Masculino 	Casado 	\N	ftravieso@ubv.edu.ve 	URB. LOS NARANJITOS, QUINTA LA COLINA CALLE NORTE 6, EL AMARILLO 	1	INSTRUCTOR	DOCENTE	2014-10-01	DOCENTE TIEMPO COMPLETO	ACTIVO
1463	6800068	RODRIGUEZ, CARMEN YUDITH	Venezolano 	1963-10-24	\N	\N	Femenino 	Soltero 	\N	cyrodriguez@ubv.edu.ve 	EL DESAGUE EL JABILLO PARTE BAJA DE MAMO CASA SAMANTHA CATIA LA MAR 	1	INSTRUCTOR	DOCENTE	2007-11-15	DOCENTE TIEMPO COMPLETO	ACTIVO
1464	6972226	BAEZ LANDER, GINA JOSEFINA	Venezolano 	1966-08-11	\N	\N	Femenino 	Soltero 	\N	gbaez@ubv.edu.ve 	CALLE NAIGUATA EDIF.CODICA P3 APTO 32-A EL MARQUEZ 	1	INSTRUCTOR	CONTRATADO	2013-02-13	DOCENTE TIEMPO COMPLETO	ACTIVO
1465	7138349	LOYO HERNANDEZ, JUAN CARLOS	Venezolano 	1972-02-16	\N	\N	Masculino 	Casado 	\N	jcloyo@ubv.edu.ve 	URB.LA FLORIDA CALLE LAS ACACIAS EDIF.VISTA AVILA 	0	INSTRUCTOR	CONTRATADO	2014-03-16	DOCENTE TIEMPO COMPLETO	ACTIVO
1466	7294469	OLIVO, JOSÉ FRANKLIN	Venezolano 	1964-10-06	\N	\N	Masculino 	Soltero 	\N	jolivo@ubv.edu.ve 	URB.UD-3RES.LIBERTADOR EDF.N 20 PB APTO 04 CARICUAO 	0	INSTRUCTOR	DOCENTE	2008-01-01	DOCENTE TIEMPO COMPLETO	ACTIVO
1467	7463784	PIÑERO GRANADILLO, MARÍA EUGENIA	Venezolano 	1962-04-11	\N	\N	Femenino 	Soltero 	\N	mepinero@ubv.edu.ve 	URB SIMON BOLIVAR BLOQUE 9 PISO 3 APT 03 	0	INSTRUCTOR	DOCENTE	2004-02-25	DOCENTE TIEMPO COMPLETO	ACTIVO
1468	7561124	LOPEZ CARRASQUEL, ZORAIMA JOSEFINA	Venezolano 	1964-08-20	\N	\N	Femenino 	Soltero 	\N	zlopez@ubv.edu.ve 	ALCABALA A PELIGRO TORRE ALCABALA B APTO 13-B LA CANDELARIA 	0	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE TIEMPO COMPLETO	ACTIVO
1469	8499264	PÉREZ MARTELO, MAIGUALIDA JOSEFINA	Venezolano 	1966-12-16	\N	\N	Femenino 	Soltero 	\N	mjperez@ubv.edu.ve 	LOS PROCERES CALLEJON BOLIVAR 	1	AUXILIAR I	DOCENTE	2007-10-22	DOCENTE TIEMPO COMPLETO	ACTIVO
1470	8644681	REYES ACUÑA, RONNY RAFAEL	Venezolano 	1967-09-15	\N	\N	Masculino 	Soltero 	\N	ronnyreyes2013@gmail.com 	RESIDENCIAS CENTRO PARQUE CARACAS, PISO 19 APTO 196-B 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE TIEMPO COMPLETO	ACTIVO
1471	8773889	GALLARDO ROJAS, LISBETH JOSEFINA	Venezolano 	1971-05-16	\N	\N	Femenino 	Soltero 	\N	 	KM 5 DE LA PANAMERICANA FRENTE AL CN LUIS BRION CASA N 7B, LA VEGA 	0	AUXILIAR I	CONTRATADO	2018-06-18	DOCENTE TIEMPO COMPLETO	ACTIVO
1472	8873597	MARCO PARRA, LISNA YONYRAY	Venezolano 	1965-11-02	\N	\N	Femenino 	Casado 	\N	lmarco@ubv.edu.ve 	CALLE CARABOBO N 40, CASCO HISTORICO, CIUDAD BOLIVAR 	2	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE TIEMPO COMPLETO	ACTIVO
1473	8880143	GARCIA VERA, OLIANIT SINNAYS	Venezolano 	1965-09-19	\N	\N	Femenino 	Soltero 	\N	ogarcia@ubv.edu.ve 	AV HUMBOLT RES MARHUANTA TORRE A P2 APTO 26 	0	AUXILIAR I	DOCENTE	2007-11-01	DOCENTE TIEMPO COMPLETO	ACTIVO
1474	8883694	MAESTRE HERNÁNDEZ, GENY JOSEFINA	Venezolano 	1965-12-11	\N	\N	Femenino 	Casado 	\N	gjmaestre@ubv.edu.ve 	LA SABANITA CALLE COLON n 18 	0	INSTRUCTOR	DOCENTE	2007-10-29	DOCENTE TIEMPO COMPLETO	ACTIVO
1475	8925792	GASCÓN, DOUGLAS RAFAEL	Venezolano 	1962-12-28	\N	\N	Masculino 	Casado 	\N	dgascon@ubv.edu.ve 	VILLA ROSA, CALLE 06, CASA N 05 	3	INSTRUCTOR	DOCENTE	2007-10-07	DOCENTE TIEMPO COMPLETO	ACTIVO
1476	8956895	RODRIGUEZ, JAVIER JOSE	Venezolano 	1965-07-12	\N	\N	Masculino 	Soltero 	\N	javx2012@gmail.com 	AVENIDA PRINCIPAL CIUDAD TIUNA-EDIF.B04 APT 8A 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE TIEMPO COMPLETO	ACTIVO
1477	9094974	PEÑA, ADRIANA JASMIN	Venezolano 	1963-11-01	\N	\N	Femenino 	Soltero 	\N	ajpena@ubv.edu.ve 	LAS ACACIAS CALLE MARIA TERESA TORO CALLEJON CHILE EDIFICIO CALLANA APTO3 	0	AUXILIAR II	DOCENTE	2006-02-15	DOCENTE TIEMPO COMPLETO	ACTIVO
1478	9901878	GALEA SERRANO, SAUL EDECIO	Venezolano 	1968-02-20	\N	\N	Masculino 	Casado 	\N	sgalea@ubv.edu.ve 	URB. DOÑA BARBARA CALLE 1 EDIF. BLOQUE 5 	2	INSTRUCTOR	DOCENTE	2004-09-29	DOCENTE TIEMPO COMPLETO	ACTIVO
1479	9904707	GUTIÉRREZ ARO, IRAIDA JOSEFINA	Venezolano 	1968-05-22	\N	\N	Femenino 	Soltero 	\N	ijgutierrez@ubv.edu.ve 	CALLE JOSE MENDEZ N 18 QTA EL ESPLENDOR SECTOR EL DIAMANTE 	0	INSTRUCTOR	DOCENTE	2007-01-01	DOCENTE TIEMPO COMPLETO	ACTIVO
1480	10049472	CARABALLO RAMOS, BETTY JOSEFINA 	Venezolano 	1967-10-28	\N	\N	Femenino 	Soltero 	\N	bcaraballo@ubv.edu.ve 	SECTOR I AGUA SALADAS CALLE CHAGUARAMAS N03 	0	INSTRUCTOR	CONTRATADO	2014-11-01	DOCENTE TIEMPO COMPLETO	ACTIVO
1481	10108326	NAVAS RAMIREZ, THANIA COROMOTO	Venezolano 	1971-08-19	\N	\N	Femenino 	Soltero 	\N	tnavas@ubv.edu.ve 	URB TERRAZAS DEL ESTE EDS 21 APTO 1-4 GUARENAS EDO MIRANDA 	1	AGREGADO	DOCENTE	2017-09-18	DOCENTE TIEMPO COMPLETO	ACTIVO
1482	10477310	MONTERO LEAL, KELVIS GREGORIO	Venezolano 	1969-09-02	\N	\N	Masculino 	Soltero 	\N	kmontero@ubv.edu.ve 	SABANITA UPATA NUEVA PIEDRITAS IV CASA ELOISA 	1	AUXILIAR I	DOCENTE	2007-10-29	DOCENTE TIEMPO COMPLETO	ACTIVO
1483	10572856	ALBORNOZ CAMPOS, CARMEN LUISA	Venezolano 	1971-02-28	\N	\N	Femenino 	Soltero 	\N	calbornoz@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2007-04-23	DOCENTE TIEMPO COMPLETO	ACTIVO
1484	10606442	TIVIDOR, ALIRIO	Venezolano 	1970-01-22	\N	\N	Masculino 	Soltero 	\N	atividor@ubv.edu.ve 	PUERTO AYACUCHO 	0	INSTRUCTOR	DOCENTE	2015-03-02	DOCENTE TIEMPO COMPLETO	ACTIVO
1485	10943838	DELGADO, LIGIA ANTONIA	Venezolano 	1970-04-08	\N	\N	Femenino 	Soltero 	\N	ligidelgado@hotmail.com 	CALLE GARCILAZO EDIF BALKAV, APTO 10 COLINAS DE BELLO MONTE 	0	INSTRUCTOR	DOCENTE	2013-02-15	DOCENTE TIEMPO COMPLETO	ACTIVO
1486	11085356	OSORIO, DOMINGO SEBASTIAN	Venezolano 	1973-07-31	\N	\N	Masculino 	Soltero 	\N	DOMINGOOSORIO1973@GMAIL.COM 	URB. LA BELISA BLOQUE 05 2DO. PISO APTO. B7 PUERTO CABELLO EDO. CARABOBO 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE TIEMPO COMPLETO	ACTIVO
1487	11485275	PACHECO, YHON ELOY 	Venezolano 	1974-12-01	\N	\N	Masculino 	Soltero 	\N	ypacheco@ubv.edu.ve 	AV PRINCIPAL RUIZ PINEDA ZONA 2 CASA APACHE GUARENA 	0	AUXILIAR I	CONTRATADO	2013-09-16	DOCENTE TIEMPO COMPLETO	ACTIVO
1488	11550311	PIÑA IRIARTE, KARYSMYR COROMOTO	Venezolano 	1973-04-05	\N	\N	Femenino 	Soltero 	\N	kpina@ubv.edu.ve 	URB CIUDAD CASARAPA PARSELA 11 	2	INSTRUCTOR	DOCENTE	2006-11-13	DOCENTE TIEMPO COMPLETO	ACTIVO
1489	11641907	HERNÁNDEZ, YISELL	Venezolano 	1974-05-19	\N	\N	Femenino 	Soltero 	\N	ynhernandez@ubv.edu.ve 	AV. SAN MARTIN URB. PALO GRANDE CONJUNTO RES. PARQUE SAN JUAN EDIF. C PISO 7 APTO 73C 	1	AGREGADO	DOCENTE	2004-10-26	DOCENTE TIEMPO COMPLETO	ACTIVO
1490	11758158	RODRIGUEZ VILLAZANA, YURAIMA MARILYN	Venezolano 	1974-12-08	\N	\N	Femenino 	Soltero 	\N	ODARILYS-07@HOTMAIL.COM 	URB MERECURE AV PRINCIPAL CALLE 1 CASA 2 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE TIEMPO COMPLETO	ACTIVO
1491	11854962	AVILA GUERRA, HILDERGARDIS MILENA	Venezolano 	1974-04-14	\N	\N	Femenino 	Soltero 	\N	havila@ubv.edu.ve 	URB LAS MERCEDES CALLE 2 CASA 16 	0	INSTRUCTOR	CONTRATADO	2016-06-15	DOCENTE TIEMPO COMPLETO	ACTIVO
1492	11993445	CANELON ALBERTO, RAUL ALBERTO	Venezolano 	1972-08-21	\N	\N	Masculino 	Soltero 	\N	 	EDIFICIO BRISAS DE MIRANDA BLOQUE 2 APTO 2-02 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE TIEMPO COMPLETO	ACTIVO
1493	11994461	SANCHEZ MARTINEZ, DANIEL ALBERTO	Venezolano 	1974-01-12	\N	\N	Masculino 	Soltero 	\N	dsanchez@ubv.edu.ve 	URB URDANETA VEREDA 41, # 14 EL CUARTEL CATIA , CARACAS 	0	AUXILIAR I	DOCENTE	2007-01-01	DOCENTE TIEMPO COMPLETO	ACTIVO
1494	12304354	CASTELLANO JIMENEZ, THAIYENNY VIRGINIA	Venezolano 	1974-08-04	\N	\N	Femenino 	Soltero 	\N	tcastellano@ubv.edu.ve 	CANDELARIA SUR CALLE ESTE O CONJUNTO RESIDENCIAL CENTRO PARQUE CARACAS TORRE B PISO9N96-B 	1	ASISTENTE	DOCENTE	2006-11-06	DOCENTE TIEMPO COMPLETO	ACTIVO
1495	12383978	SANCHEZ, HECTOR JULIO	Venezolano 	1976-04-02	\N	\N	Masculino 	Divorciado 	\N	hesanchez@ubv.edu.ve 	AV. SANZ EDIF.ONDARRETA NORTE PHA EL MARQUES 	1	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE TIEMPO COMPLETO	ACTIVO
1496	12389271	ASCANIO, JESÚS EDUARDO	Venezolano 	1974-03-14	\N	\N	Masculino 	Soltero 	\N	jascanio@ubv.edu.ve 	BLOQUE 5-B LETRA D PISO 6 APTO N62 SECTOR MONTE PIEDAD 	1	INSTRUCTOR	DOCENTE	2007-02-01	DOCENTE TIEMPO COMPLETO	ACTIVO
1497	12402221	HERNÁNDEZ MARTINEZ, MARÍA CENEM	Venezolano 	1977-04-06	\N	\N	Femenino 	Soltero 	\N	mhernandez@ubv.edu.ve 	AV. INTERCOMUNAL CON CALLE EL PALMAR, RES. LOS SAMANES, TORRE D, PISO 2, APTO. 2-C 	0	INSTRUCTOR	CONTRATADO	2018-01-22	DOCENTE TIEMPO COMPLETO	ACTIVO
1498	12470255	GONZALEZ PARADA, FRANCISCO HUMBERTO 	Venezolano 	1976-05-23	\N	\N	Masculino 	Soltero 	\N	fhgonzalez@ubv.edu.ve 	LAS ADJUNTA URB LA SOSA CALLE JULIO BAEZ 	0	INSTRUCTOR	DOCENTE	2014-07-14	DOCENTE TIEMPO COMPLETO	ACTIVO
1499	12762466	REYES DE LORZA, TANIA LIBERTAD	Venezolano 	1976-06-12	\N	\N	Femenino 	Soltero 	\N	tlreyes@ubv.edu.ve 	URB. ANTONIO JOSE SUCRE BLOQUE 3 ENTRADA B 3ER PISO. APARTAMIENTO # 5 CUTIRA-CATIA 	0	AUXILIAR I	DOCENTE	2007-01-09	DOCENTE TIEMPO COMPLETO	ACTIVO
1500	12912872	MENDOZA DE HERRERA, NINOSKA ARACELIS	Venezolano 	1977-02-07	\N	\N	Femenino 	Soltero 	\N	nmendoza@ubv.edu.ve 	AV SAN MARTIN RESIDENCIA EL PARQUE 1- B 	1	INSTRUCTOR	DOCENTE	2006-05-11	DOCENTE TIEMPO COMPLETO	ACTIVO
1501	13128248	MARTINEZ MARTINEZ, ANA ALEJANDRA	Venezolano 	1977-02-13	\N	\N	Femenino 	Soltero 	\N	efrainmanu45@gmail.com 	EL NARANJAL-SECTOR PALMARITO 2 CASA 3 	0	AUXILIAR I	CONTRATADO	2019-02-11	DOCENTE TIEMPO COMPLETO	ACTIVO
1502	13218346	PARRA CASTILLO, FRANCYS YOLEYDA	Venezolano 	1976-07-02	\N	\N	Femenino 	Soltero 	\N	fparra@ubv.edu.ve 	VALLE DEL TUY 	2	AUXILIAR I	CONTRATADO	2012-09-16	DOCENTE TIEMPO COMPLETO	ACTIVO
1503	13640711	ZERPA GUEVARA, JULIA COROMOTO 	Venezolano 	1978-05-20	\N	\N	Femenino 	Soltero 	\N	JULIACOROMOTO.2005@GMAIL.COM 	SAN FERNANDO DE APURE PLAZA N 10 SECTOR 19 DE ABRIL 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE TIEMPO COMPLETO	ACTIVO
1504	14073223	IZAGUIRRE UGUETO, BETZABE ALEJANDRA	Venezolano 	1980-04-13	\N	\N	Femenino 	Soltero 	\N	bizaguirre@ubv.edu.ve 	CALLE CAMPO ELIAS AV. FUERZAS ARMADAS 	1	COORDINADOR NACIONAL ASISTENTE	DOCENTE	2006-09-15	DOCENTE TIEMPO COMPLETO	ACTIVO
1505	14075842	BRACHO RAMONEZ, VICTOR JOSE	Venezolano 	1975-04-23	\N	\N	Masculino 	Casado 	\N	vjbracho@ubv.edu.ve 	AUDARE FALCON 	0	INSTRUCTOR	CONTRATADO	2012-09-16	DOCENTE TIEMPO COMPLETO	ACTIVO
1506	14362552	MOLINA, ANGEL JOEL	Venezolano 	1978-09-07	\N	\N	Masculino 	Soltero 	\N	ajmolina@ubv.edu.ve 	CARACAS 	1	INSTRUCTOR	DOCENTE	2011-05-16	DOCENTE TIEMPO COMPLETO	ACTIVO
1507	14527734	LORETO BRACHO, JUAN CARLOS	Venezolano 	1980-01-11	\N	\N	Masculino 	Soltero 	\N	jloreto@ubv.edu.ve 	AV. INTERCOMUNAL DEL VALLE, CALLE 14, RESIDENCIA GUAIQUERI, PISO 1 APTO.0103 LOS JARDINES DEL VALLE, 	0	AGREGADO	DOCENTE	2003-08-04	DOCENTE TIEMPO COMPLETO	ACTIVO
1508	14644899	GAVIDIA ALARCÓN, YAMILET DEL CARMEN	Venezolano 	1979-08-14	\N	\N	Femenino 	Casado 	\N	ydgavidia@ubv.edu.ve 	CALLE 7 DE SEPTIEMBRE, CASA N23 LA VEGA CARACAS 	3	AUXILIAR I	DOCENTE	2012-10-01	DOCENTE TIEMPO COMPLETO	ACTIVO
1509	14704982	MARTINEZ GONZALEZ, RICARDO RAFAEL	Venezolano 	1980-03-13	\N	\N	Masculino 	Soltero 	\N	arqricardomartinezg1@ubv.edu.ve 	URB 5 JULIO CALLE F NRO 18 PUNTA DE MATA EDO MONAGAS 	0	INSTRUCTOR	CONTRATADO	2017-06-26	DOCENTE TIEMPO COMPLETO	ACTIVO
1510	14936953	CONTRERAS MARQUEZ, YELY FERMINA 	Venezolano 	1981-04-27	\N	\N	Femenino 	Soltero 	\N	yfcontreras@ubv.edu.ve 	VIA LA TIGRERA SECTOR EL RETIRO PALO VERDE SANARE 	0	INSTRUCTOR	CONTRATADO	2015-02-02	DOCENTE TIEMPO COMPLETO	ACTIVO
1511	15118523	MEIRELLES  MONDRAGON, MARIA VIRGINIA	Venezolano 	1982-06-20	\N	\N	Femenino 	Soltero 	\N	mmeirelles@ubv.edu.ve 	CARACAS 	1	INSTRUCTOR	DOCENTE	2010-08-16	DOCENTE TIEMPO COMPLETO	ACTIVO
1512	15146459	ROJAS ORELLANA, HENDRIS JOVENIS	Venezolano 	1982-01-04	\N	\N	Femenino 	Soltero 	\N	 	URB ROMULO GALLEGOS PARROQUIA EL RECREO 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE TIEMPO COMPLETO	ACTIVO
1513	15161795	CHACON CUEVAS, VERLLY CARIN	Venezolano 	1982-02-04	\N	\N	Femenino 	Soltero 	\N	 	SECTOR LOS ALTOS DE PIPE CASA LOS TORRES S/N CARICUAO RUIZ PINEDA 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE TIEMPO COMPLETO	ACTIVO
1514	15177770	CAÑIZALEZ GAMBOA, JESUS RAMON	Venezolano 	1979-09-01	\N	\N	Masculino 	Soltero 	\N	jcanizalez@ubv.edu.ve 	CALLE 1 EL PASEO MATEO SEGUNDO VIERA 	2	INSTRUCTOR	CONTRATADO	2015-02-02	DOCENTE TIEMPO COMPLETO	ACTIVO
1515	15266532	CORRO GARCIA, JHONAR JOSE	Venezolano 	1982-03-08	\N	\N	Masculino 	Soltero 	\N	jcorro@ubv.edu.ve 	CALLE REAL DE NAVARRETE CASA N6 PARROQUIA MAIQUETIA 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE TIEMPO COMPLETO	ACTIVO
1516	15314273	MARCANO ALAMO, WILLNER RAFAEL	Venezolano 	1981-08-03	\N	\N	Femenino 	Soltero 	\N	willalamo@hotmail.com 	CARACAS 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE TIEMPO COMPLETO	ACTIVO
1517	15335725	BASTARDO VARGAS, EDGAR ANTONIO	Venezolano 	1979-02-26	\N	\N	Masculino 	Soltero 	\N	eabastardo@ubv.edu.ve 	ALTO PARACONI 	1	AUXILIAR I	DOCENTE	2006-11-13	DOCENTE TIEMPO COMPLETO	ACTIVO
1518	15404003	CABRERA MILANO, YENDRYS LENNI	Venezolano 	1979-01-13	\N	\N	Masculino 	Soltero 	\N	centinela3264@gmail.com 	URBANIZACION PIEDRAS AZULES CASA 19 HOYO DE LA PUERTA 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE TIEMPO COMPLETO	ACTIVO
1519	15531053	PEÑA LUCHONY, KARINES VIRGINIA 	Venezolano 	1980-08-06	\N	\N	Femenino 	Soltero 	\N	klpena@ubv.edu.ve 	EDIF ACOSTAFERRO IPISO MEZANINA ESQUINA DE AGUACATE Y SAN FRANCISQUITO 	1	INSTRUCTOR	CONTRATADO	2014-11-16	DOCENTE TIEMPO COMPLETO	ACTIVO
1520	15616335	CENTENO CURIEL, GABRIELA ISABELA	Venezolano 	1981-04-14	\N	\N	Femenino 	Soltero 	\N	gcenteno@ubv.edu.ve 	AV EL BOSQUE CON AV SAN RAFAEL QTA EL CASTOLLI. LA FLORIDA 	1	INSTRUCTOR	DOCENTE	2006-11-13	DOCENTE TIEMPO COMPLETO	ACTIVO
1521	15662819	RIVAS BRICEÑO, JHONATHAN MANUEL	Venezolano 	1982-11-20	\N	\N	Masculino 	Soltero 	\N	jmrivas@ubv.edu.ve 	URBANIZACION SAN ANTONIO PARTE BAJA CALLE N 1, CASA N 1 	1	AUXILIAR I	DOCENTE	2008-05-15	DOCENTE TIEMPO COMPLETO	ACTIVO
1522	15682680	GALLARDO DE TIRADOS, CARMEN MARIA	Venezolano 	1982-08-12	\N	\N	Femenino 	Casado 	\N	 	AVENIDA PERIMETRAL SUR FRENTE A LOS CENTAUROS 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE TIEMPO COMPLETO	ACTIVO
1523	15794188	MIRO SOSA, NERVIC YAMILET	Venezolano 	1983-03-19	\N	\N	Femenino 	Casado 	\N	nmiro@ubv.edu.ve 	URB CIUDAD CASARAPA, PARCELA 21 EDIF. 3 PISO 3 APTO 3-C 	1	INSTRUCTOR	DOCENTE	2012-09-16	DOCENTE TIEMPO COMPLETO	ACTIVO
1524	15843868	GAVIDIA ARAUJO, YSABEL	Venezolano 	1984-02-25	\N	\N	Femenino 	Soltero 	\N	ygavidia@ubv.edu.ve 	BUCARE A PUENTE JUMIN PARROQUIA SAN JUAN 	0	ASISTENTE	DOCENTE	2007-03-01	DOCENTE TIEMPO COMPLETO	ACTIVO
1525	15857476	CASTRO MEDINA, RAFAEL ARCANGEL	Venezolano 	1982-08-24	\N	\N	Masculino 	Soltero 	\N	rcastrom@ubv.edu.ve 	AVENIDA ROMULO GALLEGOS ZONA INDUSTRIAL 	1	INSTRUCTOR	CONTRATADO	2017-09-18	DOCENTE TIEMPO COMPLETO	ACTIVO
1526	15863639	MISLER CAMACARO, NESTOR ALFREDO	Venezolano 	1983-11-01	\N	\N	Masculino 	Soltero 	\N	nestormisler@gmail.com 	SECTOR EL BAMBU CASA 38 AV FERNANDO PEÑALVER 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE TIEMPO COMPLETO	ACTIVO
1527	15908764	SANTANDER, GERARDO	Venezolano 	1982-12-13	\N	\N	Masculino 	Soltero 	\N	gesantander@ubv.edu.ve 	EL CEMENTERIO, LOS JABILLOS 	2	INSTRUCTOR	DOCENTE	2005-12-12	DOCENTE TIEMPO COMPLETO	ACTIVO
1528	16224260	TORREALBA SILLIE, WILMER JOSE	Venezolano 	1982-12-28	\N	\N	Masculino 	Soltero 	\N	wtorrealba@ubv.edu.ve 	BLOQUE 6 CALLE AYACUCHO PISO 3, APTO. 38, LETRA E 	0	INSTRUCTOR	DOCENTE	2012-11-16	DOCENTE TIEMPO COMPLETO	ACTIVO
1529	16558829	NIEVES, JONATHAN JOSE	Venezolano 	1983-05-16	\N	\N	Masculino 	Soltero 	\N	jonieves@ubv.edu.ve 	MEZA 	2	AUXILIAR I	DOCENTE	2011-03-16	DOCENTE TIEMPO COMPLETO	ACTIVO
1530	16766217	FIGUEREDO, RAQUEL JESUS	Venezolano 	1981-11-26	\N	\N	Femenino 	Soltero 	\N	rjfigueredo@ubv.edu.ve 	CATIA AV SUCRE CALLE LA COLINA ALTA VISTA 	1	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE TIEMPO COMPLETO	ACTIVO
1531	16954927	MERLO SOTILLO, JUAN CARLOS	Venezolano 	1983-11-25	\N	\N	Masculino 	Soltero 	\N	jmerlo@ubv.edu.ve 	URB.CIUADAD TIUNA, SECTOR NORESTE, EL VALLE CARACAS 	0	COORDINADOR NACIONAL INSTRUCTOR	CONTRATADO	2017-01-09	DOCENTE TIEMPO COMPLETO	ACTIVO
1532	17009612	MAITA MAITA, HÉCTOR JOSÉ	Venezolano 	1984-08-13	\N	\N	Masculino 	Soltero 	\N	hmaita@ubv.edu.ve 	AV. SANTA TERESA DE JESUS COMUNIDAD INDIGENA KASHAMA EDO. ANZOATEGUI 	0	AUXILIAR I	DOCENTE	2007-01-15	DOCENTE TIEMPO COMPLETO	ACTIVO
1533	17285889	DEPAOLI SOLIS, DANIEL OMAR	Venezolano 	1986-07-01	\N	\N	Masculino 	Soltero 	\N	ddepaoli@ubv.edu.ve 	URB. NUEVA CASARAPA, CONJUNTO RESIDENCIAL LOS ALEROS, CASA E-586, QUINTA BALBINA 	0	AUXILIAR I	CONTRATADO	2015-09-17	DOCENTE TIEMPO COMPLETO	ACTIVO
1534	17301957	ARAUJO CABEZA, BLANDINA COROMOTO	Venezolano 	1984-01-28	\N	\N	Femenino 	Soltero 	\N	baraujo@ubv.edu.ve 	CARACAS 	2	INSTRUCTOR	DOCENTE	2010-12-01	DOCENTE TIEMPO COMPLETO	ACTIVO
1535	17841968	COLINA LINDO, BETSY EVANGELINA	Venezolano 	1985-02-27	\N	\N	Femenino 	Soltero 	\N	bcolina@ubv.edu.ve 	URB MONTALBAN CALLE 50 RES CAROLINA PISO 5 APTO 5B 	1	AUXILIAR I	CONTRATADO	2015-02-02	DOCENTE TIEMPO COMPLETO	ACTIVO
1536	18011675	MOLINA FERMIN, GERARD ALEXANDER	Venezolano 	1988-07-10	\N	\N	Masculino 	Soltero 	\N	crach8.drareg@gmail.com 	LA VEGUITA 	0	AUXILIAR I	CONTRATADO	2019-02-11	DOCENTE TIEMPO COMPLETO	ACTIVO
1537	18042436	GARRIDO VELASCO, LEONARD JESUS	Venezolano 	1987-04-07	\N	\N	Femenino 	Soltero 	\N	lgarrido@ubv.edu.ve 	CALLE CANAIMA,CALLEJON ORINOCO,LA VEGA CASA 20-36 	0	AUXILIAR I	CONTRATADO	2018-01-22	DOCENTE TIEMPO COMPLETO	ACTIVO
1538	18323958	GONZALEZ RAMIREZ, STEFANY ANGELICA	Venezolano 	1988-12-24	\N	\N	Femenino 	Soltero 	\N	alanisssin@gmail.com 	CALLEJON ESCUELA CALLE RIO BUENO CASA NRO 21 	0	AUXILIAR I	CONTRATADO	2019-02-11	DOCENTE TIEMPO COMPLETO	ACTIVO
1539	19287542	RODRIGUEZ CARBAJAL, LUIS EDUARDO	Venezolano 	1989-12-18	\N	\N	Masculino 	Soltero 	\N	lerodriguez@ubv.edu.ve 	AV GUZMAN BLANCO COTA 905 BARRIO BUENOS AIRES CASA S/N 	1	AUXILIAR I	CONTRATADO	2013-09-16	DOCENTE TIEMPO COMPLETO	ACTIVO
1540	19370861	RAMOS MIRABAL, CARLOS ALBERTO	Venezolano 	1988-03-16	\N	\N	Masculino 	Soltero 	\N	caramos@ubv.edu.ve 	CALLE MIRANDA, EDIF. B-06, PISO 8, APTO 8H, DESARROLLO HABITACIONAL CIUDAD TIUNA 	0	INSTRUCTOR	CONTRATADO	2016-01-11	DOCENTE TIEMPO COMPLETO	ACTIVO
1541	19398387	SILVA PINTO, KARINA DEL VALLE 	Venezolano 	1987-10-29	\N	\N	Femenino 	Soltero 	\N	kvsilva@ubv.edu.ve 	 K18 ECTOR CHAGUARAMAS CASA N12 	1	AUXILIAR I	CONTRATADO	2014-09-18	DOCENTE TIEMPO COMPLETO	ACTIVO
1542	19557824	OLIVARES GARCES, ALFONSO	Venezolano 	1951-08-14	\N	\N	Masculino 	Soltero 	\N	alolivares@ubv.edu.ve 	LA URBINA. 1ERA PRINCIPAL. EDIF. NELISSA. APTO 6-C 	0	INSTRUCTOR	DOCENTE	2005-02-14	DOCENTE TIEMPO COMPLETO	ACTIVO
1543	19581758	CORDOVA CASIQUE, ROBERTH ANTONIO	Venezolano 	1990-12-31	\N	\N	Masculino 	Soltero 	\N	 	ALTA VISTA FINAL DE LA CALLE ITALIA CASA NRO 35 CATIA CARACAS 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE TIEMPO COMPLETO	ACTIVO
1544	20155558	CORRO MUÑOZ, DEHIKER ENRIQUE	Venezolano 	1992-09-01	\N	\N	Masculino 	Soltero 	\N	dehiker80@gmail.com 	BARRIO ANDRES ELOY BLANCO SECTOR EL PLAN CALLEJON LAS MONJAS 	0	AUXILIAR I	CONTRATADO	2018-06-18	DOCENTE TIEMPO COMPLETO	ACTIVO
1545	21312616	GONZALEZ QUIRPA, MARIANA CAROLINA	Venezolano 	1994-05-10	\N	\N	Femenino 	Soltero 	\N	 	URB,LA TRINIDAD CALLE 4 CASA 135 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE TIEMPO COMPLETO	ACTIVO
1546	23651726	GUTIERREZ CORREA, EFREN ANTONIO	Venezolano 	1993-09-17	\N	\N	Masculino 	Soltero 	\N	GUTIERREZCOERREA17@OUTLOOK.COM 	RESID. DORAL PLAZA AV ESTE 2 APTO 17 PISO 1 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE TIEMPO COMPLETO	ACTIVO
1547	24995549	GONZALEZ RODRIGUEZ, BETZI ELIBETH	Venezolano 	1995-10-10	\N	\N	Femenino 	Soltero 	\N	betzy101095@gmail.com 	LA VEGUITA AV LIBERTADOR CASA 13-18 	0	AUXILIAR I	CONTRATADO	2019-02-11	DOCENTE TIEMPO COMPLETO	ACTIVO
1548	26131700	GIL AVENDAÑO, CLEIDY VERONICA	Venezolano 	1998-05-30	\N	\N	Femenino 	Soltero 	\N	cleidygil42@gmail.com 	SECTOR EL GUARATARO CALLE MANSON-LA PEDERA CASA 12 	0	AUXILIAR I	CONTRATADO	2019-02-11	DOCENTE TIEMPO COMPLETO	ACTIVO
1549	32276172	SANTANA ESTRADA, CARMEN ROSA	Venezolano 	1965-04-19	\N	\N	Femenino 	Divorciado 	\N	CARMENCHUDECUBA@HOTMAIL.COM 	CALLE RESIDENCIA CARACOLA II CASA 44 URBANIZACION ZAITA 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE TIEMPO COMPLETO	ACTIVO
1550	84557480	PASCUAL MARQUINA, CIRA MARIA	Extranjero 	1976-05-25	\N	\N	Femenino 	Soltero 	\N	cpascual@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2006-11-13	DOCENTE TIEMPO COMPLETO	ACTIVO
1551	84558705	LEET PATRICK, CRAIG	Extranjero 	1976-10-03	\N	\N	Masculino 	Soltero 	\N	cleet@ubv.edu.ve 	CARACAS 	1	ASISTENTE	DOCENTE	2007-09-17	DOCENTE TIEMPO COMPLETO	ACTIVO
1552	84562654	FRANCIA, JUAN LUIS	Extranjero 	1968-12-12	\N	\N	Masculino 	Soltero 	\N	jfrancia@ubv.edu.ve 	ESQ AVILEN A MIRADOR CALLE NORTE 13 	1	INSTRUCTOR	DOCENTE	2013-12-12	DOCENTE TIEMPO COMPLETO	ACTIVO
1553	616935	CHACÓN PEREZ, ADALBERTO	Venezolano 	1938-09-17	\N	\N	Masculino 	Casado 	\N	adchacon@ubv.edu.ve 	URB. BELLA VISTA, AV. PPAL. DE B. V. CALLE LOS FLORES, CONJ. RESIDENCIAL DOÑA INES N # M3PB4 	0	INSTRUCTOR	DOCENTE	2005-11-21	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1554	994580	MATUTE L, CARMEN	Venezolano 	1936-10-28	\N	\N	Femenino 	Soltero 	\N	cmatute@ubv.edu.ve 	URB. LA TRINIDAD, CALLE 57, BLOQUE 15 N-41, 3ER PISO, APTO 44- MARACAIBO, ZULIA 	0	AGREGADO	DOCENTE	2003-11-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1555	1713758	NAVARRO, ÁNGEL DOMINGO	Venezolano 	1939-02-05	\N	\N	Masculino 	Soltero 	\N	adnavarro@ubv.edu.ve 	URB GUAICAIPURO AV TACHIRA QUINTA BELKYS 	0	INSTRUCTOR	DOCENTE	2007-10-22	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1556	1890716	MONSALVE CACERES, JOSE	Venezolano 	1938-12-27	\N	\N	Masculino 	Soltero 	\N	jmonsalve@ubv.edu.ve 	AV CASIQUIARE QUINTA NORA, COLINAS DE BELLO MONTE CARACAS 	0	INSTRUCTOR	DOCENTE	2009-09-24	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1557	2435703	HURTADO RAYUGSEN, OMAR EDMUNDO	Venezolano 	1945-04-13	\N	\N	Masculino 	Casado 	\N	 	LOS CHAGUARAMOS 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1558	2643701	ROQUE RIVERO, FELIX MIGUEL	Venezolano 	1954-01-05	\N	\N	Masculino 	Soltero 	\N	 	C 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1559	3171966	MENDEZ, JULIAN TOMAS	Venezolano 	1948-12-12	\N	\N	Masculino 	Soltero 	\N	 	MONAGAS 	0	INSTRUCTOR	DOCENTE	2011-05-02	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1560	3222324	HERNÁNDEZ VALERA, MARTIN	Venezolano 	1946-07-29	\N	\N	Masculino 	Soltero 	\N	mahernandez@ubv.edu.ve 	QTA DON HUMBERTO CALLE CAUCAGA URB LAS MERCEDES SECTOR SAN ROMAN 	0	INSTRUCTOR	DOCENTE	2005-02-16	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1696	14479935	ALVAREZ, RAMÓN	Venezolano 	1979-10-29	\N	\N	Masculino 	Soltero 	\N	ralvarez@ubv.edu.ve 	SECTOR CARURIBANA, CALLE BOLIVAR, CASA N 13 	0	INSTRUCTOR	DOCENTE	2005-11-11	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1561	3401948	SANTELIZ GRANADILLO, ANDRES BESALIO	Venezolano 	1947-06-07	\N	\N	Masculino 	Casado 	\N	asanteliz@ubv.edu.ve 	CALLE A Y B DE LA URBANIZACION LOS PINOS EDIFICIO BEATRIZ APTO 11 	0	AGREGADO	CONTRATADO	2014-04-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1562	3683396	GÓMEZ OROPEZA, FERNANDO RAMON	Venezolano 	1957-01-19	\N	\N	Masculino 	Casado 	\N	frgomez@ubv.edu.ve 	CALLE JOSE LEONARDO CHIRINOS N5 SECTOR ALI PRIMERA 	0	INSTRUCTOR	DOCENTE	2007-05-25	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1563	3863383	GALINDEZ SUAREZ, HÉCTOR REINALDO	Venezolano 	1949-03-23	\N	\N	Masculino 	Soltero 	\N	hrgalindez@ubv.edu.ve 	CALLE LAS CIENCIAS EDIF ATXOURI  LOS CHAGUARAMOS   CARACAS 	0	INSTRUCTOR	DOCENTE	2004-10-19	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1564	3872546	ORTEGA RONDÓN, JESÚS ARMANDO	Venezolano 	1955-08-27	\N	\N	Masculino 	Soltero 	\N	 	URB EL SAMAN AGUA SALADA CASA K-2 SECTOR LAS FLORES 	0	INSTRUCTOR	DOCENTE	2007-11-22	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1565	3899611	LÓPEZ JUAN, DOMINGO	Venezolano 	1954-08-04	\N	\N	Masculino 	Soltero 	\N	domlopez@ubv.edu.ve 	LA PARAGUA LIBERTADOR RES 1-13-A APTO N21 	0	INSTRUCTOR	DOCENTE	2007-10-29	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1566	4090004	MARCANO MARCANO, MIRIAM COROMOTO	Venezolano 	1956-09-20	\N	\N	Femenino 	Soltero 	\N	MIRIAMMARCANO@CANTV.NET 	URB. PARQUE NACIONAL 1TRANV. DE SEBUCAN 3RA. CALLE CIEGA QTA NARMA. 	0	INSTRUCTOR	CONTRATADO	2017-01-09	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1569	4539895	BOZAN PARRA, ACONCITO	Venezolano 	1954-01-20	\N	\N	Masculino 	Soltero 	\N	acbozan@ubv.edu.ve 	AV CRUZ VERDE 	0	INSTRUCTOR	DOCENTE	2009-06-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1570	4555454	DIAZ CASTILLO, ANGEL CIPRIANO	Venezolano 	1953-08-02	\N	\N	Masculino 	Soltero 	\N	diacahi@hotmail.com 	LA VEGA CALLE SAN JOSE EDIF ILDA PISO 6 APTO 56 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1571	4677683	RAUSEO, JOSÉ ANTONIO	Venezolano 	1954-02-02	\N	\N	Masculino 	Soltero 	\N	 	LA CANDELARIA AV UNIVERSIDAD RES TEQUENDANA PISO 8 APTO 82 	0	INSTRUCTOR	DOCENTE	2007-10-16	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1572	4679026	CAMERO ENGUAIMA , CARMEN CLARA 	Venezolano 	1958-08-12	\N	\N	Femenino 	Soltero 	\N	frances@ubv.edu.ve 	CONJUNTO RESIDENCIAL ARICHUNA 2DA ETAPA EDIF 1 APTO 136 	0	INSTRUCTOR	CONTRATADO	2015-02-02	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1573	4680927	ORTEGA, DAVID GEOVANNY	Venezolano 	1957-02-16	\N	\N	Masculino 	Soltero 	\N	daortega@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2011-01-16	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1574	4690275	VASQUEZ MARVAL, ARISTIDES JOSE	Venezolano 	1954-12-07	\N	\N	Masculino 	Soltero 	\N	 	CONJUNTO RESIDENCIAL LA FONTANA, CASA # 11, QUINTA ROMARIS,CIUDAD BOLIVAR, EDO.BOLIVAR 	0	INSTRUCTOR	DOCENTE	2007-04-16	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1575	4718749	ORTA RODRIGUEZ, ERNESTO LUIS	Venezolano 	1957-02-06	\N	\N	Masculino 	Casado 	\N	eorta@ubv.edu.ve 	CARRERA 3 N 26 (ANTIGUA AV. RIBAS) SECTOR CATEDRAL. MATURIN 	0	INSTRUCTOR	CONTRATADO	2015-02-02	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1576	4855881	RUIZ DE ROSILLO, DAMARIS ALEX	Venezolano 	1956-07-28	\N	\N	Femenino 	Soltero 	\N	 	SECTOR MEDINA ANGARITA  AV CARLOS MEDINA PIAS  EDF CARMELIO MIRO PLORES 	0	INSTRUCTOR	DOCENTE	2007-01-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1577	4977848	ARCIA, ISAURA	Venezolano 	1954-06-17	\N	\N	Femenino 	Soltero 	\N	 	CALLE SAN RAFAEL IERA TRASVERSAL PUENTE GOMEZ CASA N 03 LA SABANITA CIUDAD BOLIVAS 	0	INSTRUCTOR TCV 5	DOCENTE	2005-10-15	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1578	5031076	CONTRERAS CONTRERAS, JOSÉ	Venezolano 	1957-03-05	\N	\N	Masculino 	Casado 	\N	 	LOMAS DE SAN DIEGO SECTOR EL PRADO. CALLE N2 CASA 868-A. EDO. MIRANDA 	0	TITULAR	CONTRATADO	2012-09-16	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1579	5097534	GAVIDIA LEON, FIDEL OMAR	Venezolano 	1959-02-07	\N	\N	Masculino 	Casado 	\N	FIDELGAVI509@GMAIL.COM 	URB. ROTA FE, EDIF. MIRABELLA II, PISO 7, APTO. 7D 	0	INSTRUCTOR	CONTRATADO	2015-07-06	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1580	5217716	HERRERA ZAMBRANO, FLOR ELISA	Venezolano 	1959-06-19	\N	\N	Femenino 	Soltero 	\N	feherrera@ubv.edu.ve 	LAS ACASIAS AVENIDA VICTORIA EDIFICIO LAS PALMAS 	0	INSTRUCTOR	DOCENTE	2004-09-20	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1581	5288174	MEDINA SANCHEZ, WILLIANS AQUILES	Venezolano 	1958-04-23	\N	\N	Masculino 	Soltero 	\N	wmedina@ubv.edu.ve 	SECTOR PROGRESO. CALLEJON KENNEDY. CASA 113A-51 	0	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1582	5400937	LARA GARCIA, EDITHA COROMOTO	Venezolano 	1956-09-16	\N	\N	Femenino 	Soltero 	\N	EDILIAR16@HOTMAIL.COM 	RESIDENCIAS EL RODEO 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1583	5521726	MONTES, MARÍA NELA	Venezolano 	1962-12-17	\N	\N	Femenino 	Divorciado 	\N	mnmontes@ubv.edu.ve 	CALLE 29, ENTRE AZCUE Y BICENTENARIO # 4 	0	INSTRUCTOR	DOCENTE	2005-03-28	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1584	5522503	BASTARDO, GUSTAVO	Venezolano 	1959-05-29	\N	\N	Masculino 	Soltero 	\N	gubastardo@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	CONTRATADO	2012-10-16	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1585	5522664	HERNANDEZ MOSQUERA, ROSSY ADRIANA	Venezolano 	1960-07-28	\N	\N	Femenino 	Casado 	\N	 	LOS CHAGUARAMOS 	0	ASOCIADO	CONTRATADO	2018-01-22	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1586	5579045	HERRERA BLANCO, NELSON SALVADOR	Venezolano 	1958-11-09	\N	\N	Masculino 	Soltero 	\N	nherrera@ubv.edu.ve 	CALLE LOS CEDROS CASA# 28 CATIA CARCAS 	0	INSTRUCTOR	DOCENTE	2007-03-05	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1587	5742307	BONILLA MOLINA, LUIS FRANCISCO	Venezolano 	1962-07-20	\N	\N	Masculino 	Soltero 	\N	 	PARQUE CENTRAL TORREMOEDARO.PISO15.APTO.3. 	0	INSTRUCTOR	DOCENTE	2004-02-25	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1588	5753585	GIL MEDINA, EDUARDO ANTONIO	Venezolano 	1959-08-21	\N	\N	Masculino 	Casado 	\N	GILQUES@GMAIL.COM 	RESIDENCIAS VERSALES, COLINAS DE BELLO MONTE APART 13 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1589	5800066	POCATERRA LEON, ANABEL	Venezolano 	1960-12-04	\N	\N	Femenino 	Soltero 	\N	anpocaterra@ubv.edu.ve 	AV 28 A CALLE 84 N 5-125 AV LA LIMPIA SECTOR PUERTO RICO 	0	INSTRUCTOR	DOCENTE	2004-05-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1590	5975520	GONZALEZ, IVÁN	Venezolano 	1962-08-14	\N	\N	Masculino 	Soltero 	\N	ivgonzalez@ubv.edu.ve 	URB PALO VERDE QUINTA GLOELI 	0	INSTRUCTOR TCV 5	DOCENTE	2004-05-14	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1591	6115933	TRAVIESO HIDALGO, VLADIMIR	Venezolano 	1963-01-24	\N	\N	Masculino 	Soltero 	\N	vltravieso@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2006-04-03	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1592	6120687	DIAZ AGUDELO, JULAYMA ELIZABETH	Venezolano 	1963-03-02	\N	\N	Femenino 	Soltero 	\N	diazjulayma@gmail.com 	AV.FUERZAS ARMADAS ESQ SAN JOSE A SANTA ROSA RESIDENCIAS CASACOIMA PISO 14, APTO 14-4 SAN JOSE 	0	INSTRUCTOR	CONTRATADO	2018-03-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1593	6123099	PINTOS SILVA, MARIA DOLORES	Venezolano 	1951-12-22	\N	\N	Femenino 	Divorciado 	\N	mpintos2011@gmail.com 	RESIDENCIA LAS DANIELAS, EDIF 1 PISO 9 APTO 9-2 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1594	6153950	DUARTE RONDÓN, JESÚS ENRIQUE	Venezolano 	1962-10-19	\N	\N	Masculino 	Soltero 	\N	jduarte@ubv.edu.ve 	EL PARAISO. AV. PAEZ. CALLEJON SANTA ELENA. EDIFICIO: CIMA. APTO: 8C 	0	INSTRUCTOR	DOCENTE	2007-10-22	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1595	6200429	NUNEZ, JOSÉ RAFAEL	Venezolano 	1965-03-03	\N	\N	Masculino 	Soltero 	\N	 	AV EL CORTIJO QUINTA MARIANA SAN PEDRO 	0	INSTRUCTOR	DOCENTE	2004-03-22	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1596	6216272	MARQUEZ RAMIREZ, ESMERALDA DE LOURDES	Venezolano 	1965-11-16	\N	\N	Femenino 	Soltero 	\N	ruizmaire@gmail.com 	LOMAS DE PROPATRIA CALLE LOS CACIQUES BLOQUE 3 PISO 5 APTO 59 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1597	6229095	RAMOS CLEMENTE, JULIO CESAR	Venezolano 	1965-05-18	\N	\N	Masculino 	Casado 	\N	JCESARALEJANDRO@GMAIL.COM 	AV. PRINCIPAL ARAGUITA 2 SECTOR2 CASA 44 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1598	6294367	DÁVILA PELAEZ, JOSÉ ANTONIO	Venezolano 	1968-08-10	\N	\N	Masculino 	Soltero 	\N	jadavila@ubv.edu.ve 	ANTIMANO -CARAPITA CASA N#12-5 	0	INSTRUCTOR	DOCENTE	2007-04-26	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1599	6361490	REYES PAEZ, VÍCTOR MANUEL	Venezolano 	1961-05-18	\N	\N	Masculino 	Soltero 	\N	vmreyes@ubv.edu.ve 	CALLE DEL  MEDIO # 25-05 PARROQUIA ANTIMANO  CASCO CENTRAL 	0	INSTRUCTOR	DOCENTE	2004-02-25	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1600	6414283	CHACIN DIAZ, MERCEDES ELENA	Venezolano 	1964-08-31	\N	\N	Femenino 	Soltero 	\N	 	RES. SAN BERNARDINO. APTO . 42-B. AVENIDA FRANCISCO JAVIER USTARIZ. SAN BERNARDINO 	0	INSTRUCTOR	DOCENTE	2004-04-13	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1601	6433048	LOPEZ, ORANGE DEL VALLE	Venezolano 	1960-09-13	\N	\N	Masculino 	Soltero 	\N	 	ACHIPANO II CALLE LA ESPERANZA CON CARACUEY CASA DAIFRE 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1602	6480909	GARRIDO GONZALEZ, ESPERANZA LUISA	Venezolano 	1962-03-04	\N	\N	Femenino 	Soltero 	\N	egarrido@ubv.edu.ve 	URB ATLANTIDA CALLE 7 QTA ANGELITA 	0	INSTRUCTOR	DOCENTE	2005-03-28	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1603	6503842	DÍAZ MARIN, MARÍA ALEJANDRA	Venezolano 	1966-09-13	\N	\N	Femenino 	Soltero 	\N	 	SANTA INES, AV. PRINCIPAL CALLES CY EL PARQUE CASA ( L A ENEIDA) 	0	INSTRUCTOR	DOCENTE	2006-10-18	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1604	6524598	MARTINEZ, SERGIO ENRIQUE	Venezolano 	1962-03-23	\N	\N	Masculino 	Casado 	\N	sermartinez@ubv.edu.ve 	PROLONGACION AV. EL COLEGIO, SECTOR PALGUARIME CASA MARISELA 	0	INSTRUCTOR	CONTRATADO	2016-06-15	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1605	6553092	ROJAS CAMPOS, CARLOS ENRIQUE	Venezolano 	1964-01-12	\N	\N	Masculino 	Soltero 	\N	 	URBANIZACION LONGARAI CALLE ALI PRIMERA EDF. SANTA ROSA 	0	INSTRUCTOR	DOCENTE	2006-11-23	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1606	6556512	GARCIA ALVAREZ, BELKYS ALEIDA	Venezolano 	1960-06-15	\N	\N	Femenino 	Soltero 	\N	bgarcia@ubv.edu.ve 	AV. INT EL VALLE CONJUNTO RES. PARQUE EL VALLE, EDIFICIO GEBO I, PISO 13 APTO. 13- 1 	0	INSTRUCTOR	DOCENTE	2004-01-19	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1607	6650243	RODRIGUEZ GARCIA, PEDRO PABLO	Venezolano 	1961-12-18	\N	\N	Masculino 	Soltero 	\N	 	EDIF.URDANETA PISO 14 APTO 14-3 VALENCIA 	0	INSTRUCTOR	CONTRATADO	2019-05-31	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1608	6849619	MARRERO, YANETTE	Venezolano 	1963-08-28	\N	\N	Femenino 	Soltero 	\N	ymarrero@ubv.edu.ve 	AV. LOS CLAVELES, PASAJE VALERA  N12 APTO. 2 - PUENTE HIERRO 	0	INSTRUCTOR	DOCENTE	2004-10-13	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1609	6876757	CONTRERAS  NATERA, MIGUEL ANGEL	Venezolano 	1966-03-29	\N	\N	Masculino 	Soltero 	\N	macontreras@ubv.edu.ve 	CARACAS 	0	AGREGADO	DOCENTE	2009-10-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1610	6899767	WIEDEMAN RODRIGUEZ, FEDORA TATIANA	Venezolano 	1966-05-06	\N	\N	Femenino 	Soltero 	\N	kerana69@gmail.com 	CALLE LIBERTADOR BLOQUE N 2 APTO A-117 PISO 11 	0	INSTRUCTOR	CONTRATADO	2018-01-22	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1611	6904579	SUNIAGA VALENCIA, RUTH CAROLINA	Venezolano 	1964-11-10	\N	\N	Femenino 	Soltero 	\N	 	URB LAS AMERICAS, EDF URUGUAY PISO 6 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1612	6913918	BRICENO GONZALEZ, WADIM	Venezolano 	1966-05-05	\N	\N	Masculino 	Soltero 	\N	wabriceno@ubv.edu.ve 	ALTA FLORIDA, CALLE EL RETIRO, RES. MARYFLOR, PISO 1 APTO. 1 	0	INSTRUCTOR	DOCENTE	2004-04-13	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1613	7020400	LOPEZ EIZAU, CARLOS RAMON	Venezolano 	1959-09-15	\N	\N	Masculino 	Soltero 	\N	 	URB SANTA CRUZ SECTOR NRO 1 VEREDA 07 CASA 09 	0	AGREGADO	CONTRATADO	2019-05-31	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1614	7273179	ECHENAGUCIA GALLARDO, LAHETITIA YVANORA	Venezolano 	1968-04-17	\N	\N	Femenino 	Soltero 	\N	lyechenagucia@ubv.edu.ve 	SECTOR 3,  VEREDA 38,  No. 11 	0	INSTRUCTOR	DOCENTE	2007-09-17	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1615	7474617	LOPEZ ALCALA, VLADIMIR GREGORIO	Venezolano 	1960-04-04	\N	\N	Masculino 	Casado 	\N	vlopez@ubv.edu.ve 	AV PPAL SECTOR CUTICANA CASA NRO 15 MUNICIPIO CARIRUBANA PTO FIJO FALCON 	2	INSTRUCTOR	DOCENTE	2012-02-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1616	7708044	BAO BARRIENTOS, MERVIN AMILCAR	Venezolano 	1961-03-26	\N	\N	Masculino 	Soltero 	\N	 	URBANIZACION LA PAZ I ETAPA CALLE 96 PARROQUIA CECILIO ACOSTA DEL MUNICIPIO MARACAIBO 	0	INSTRUCTOR	DOCENTE	2004-02-26	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1617	7823259	PARRA CORZO, CARLOS ALBERTO	Venezolano 	1964-09-05	\N	\N	Masculino 	Soltero 	\N	caparra@ubv.edu.ve 	SECTOR LA LIMPIA AVENIDA 79 CON CALLE 79 E 	0	INSTRUCTOR	DOCENTE	2004-02-26	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1618	7913425	HEREDIA, ROBERT	Venezolano 	1967-10-08	\N	\N	Masculino 	Soltero 	\N	roheredia@ubv.edu.ve 	SECTOR SAN JONOTE AV. JOSE GREGORIO HERNANDEZ, NUM. 132CIUDAD BOLIVAR, EDO BOLIVAR 	0	INSTRUCTOR	DOCENTE	2005-10-15	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1619	7924395	ZAMBRANO ESPINOZA, FRANCIS ELENA	Venezolano 	1966-09-20	\N	\N	Femenino 	Soltero 	\N	fezambranoe@ubv.edu.ve 	CALLEJON LOYOLA EL PARAISO 	0	INSTRUCTOR	DOCENTE	2004-04-13	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1620	8006531	MATOS CALDERON, ANTONIO RAMON	Venezolano 	1960-02-18	\N	\N	Masculino 	Soltero 	\N	armatos@ubv.edu.ve 	APTO 9C TORRE A 20, SECTOR NORESTE, CIUDAD TIUNA, EL VALLE CARACAS 	0	INSTRUCTOR	CONTRATADO	2017-01-09	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1621	8359575	ORTIZ GRAZZIANI, MECELENIA COROMOTO	Venezolano 	1961-10-27	\N	\N	Femenino 	Soltero 	\N	mcograzziani@gmail.com 	AV.LUOS BRAILE RESIDENCIAS PARQUE GRANADA PH A , LAS ACACIAS 	0	INSTRUCTOR	CONTRATADO	2018-01-22	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1622	8378932	RONDON, RAMONA ZULAY	Venezolano 	1963-06-04	\N	\N	Femenino 	Soltero 	\N	rzrondon@ubv.edu.ve 	URB. VILLA ALTAMIRA, CASA 9, MANZANA 11 	0	INSTRUCTOR	CONTRATADO	2015-02-02	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1623	8390193	MILLAN, DORIS MARÍA	Venezolano 	1960-01-28	\N	\N	Femenino 	Soltero 	\N	dorismillan2009@gmail.com 	CALLE SAN MIGUEL CON PRIMERA TRANSVERSAL LAS ACACIAS 	0	AUXILIAR I	CONTRATADO	2018-02-27	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1624	8390328	GONZALEZ GIL, RAMÓN JOSÉ	Venezolano 	1961-10-17	\N	\N	Masculino 	Soltero 	\N	rjgonzalez@ubv.edu.ve 	PERU I CALLE S CASA 11 	0	INSTRUCTOR	DOCENTE	2005-04-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1625	8442108	FUENTES VERA, YUSDELIS	Venezolano 	1963-04-12	\N	\N	Femenino 	Soltero 	\N	yufuentes@ubv.edu.ve 	URB LOS PAFAROS AV UNIVERSIDAD EDIF 5 APART 3-B 	0	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1626	8533560	ALLONG BATSON, JOSÉ IRVING	Venezolano 	1960-08-29	\N	\N	Masculino 	Soltero 	\N	jallong@ubv.edu.ve 	AV, ALEJANDRO VARGAS RESD, VILLA DE ORAZIO, CASA 1, SECTOR LAS FLORES 	0	INSTRUCTOR	DOCENTE	2007-01-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1627	8879266	BOLÍVAR CARVAJAL, YADIRA EMELINA	Venezolano 	1964-02-13	\N	\N	Femenino 	Soltero 	\N	 	LA SHELL ANZOATEQUI 	0	INSTRUCTOR	DOCENTE	2005-04-15	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1628	8978493	SABATINO HERRERA, YLLUMINATA	Venezolano 	1967-03-04	\N	\N	Femenino 	Divorciado 	\N	 	SECTOR CENTRO ESTE. CALLE PROGRESO. CASA 9606 	1	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1630	9289980	PÉREZ, DIGLIS JOSEFINA	Venezolano 	1966-08-30	\N	\N	Femenino 	Soltero 	\N	diperez@ubv.edu.ve 	CALLE MAGDALENA N46, SANTA BÁRBARA MONAGAS 	0	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1631	9294707	BRITO PADILLA, ALPIDIO JOSÉ	Venezolano 	1966-08-08	\N	\N	Masculino 	Casado 	\N	ajbrito@ubv.edu.ve 	CALLE N 1 EL PARAISO 	0	INSTRUCTOR	DOCENTE	2007-10-15	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1632	9315089	VALERA, MARÍA DEL CARMEN	Venezolano 	1963-07-16	\N	\N	Femenino 	Soltero 	\N	 	URB. VILLA BRASIL, BLOQUE 9, PISO 1 APTO. B-3 	0	INSTRUCTOR	DOCENTE	2007-06-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1633	9415592	DEL CASTILLO PULIDO, CRUZ MEYBER	Venezolano 	1968-09-27	\N	\N	Masculino 	Soltero 	\N	cdelcastillo@ubv.edu.ve 	URB. RUIZ PINEDA.AV.PPPAL BLOQUE 12. EDF 1. PISO 5 APTO 05-06.SECTORU D-7 	0	ASISTENTE	DOCENTE	2004-02-25	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1634	9425587	FRANKLIN JOSE, RODRIGUEZ	Venezolano 	1967-07-01	\N	\N	Masculino 	Soltero 	\N	 	VILLA ROSA VEREDA 54 CASA S/N 	0	INSTRUCTOR	CONTRATADO	2018-01-22	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1635	9480051	CAMARGO FIGUEROA, YUL ALEXANDER	Venezolano 	1969-04-07	\N	\N	Masculino 	Soltero 	\N	yulcamargo@gmail.com 	AV.EL EJERCITO FUERTE TIUNA RESIDENCIA DE OFICIALES ABRE E LIMA PISO 1 HAB 1-1 EL VALLE 	0	ASISTENTE	CONTRATADO	2016-06-15	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1636	9503106	NUÑEZ, CASTOR	Venezolano 	1964-06-10	\N	\N	Masculino 	Soltero 	\N	casnunez@ubv.edu.ve 	URB. ANDARA CALLE 2 RESD. ANDORA CASA N 24 	0	INSTRUCTOR	DOCENTE	2005-10-11	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1705	15373971	DECIMAVILLA TUA, LUIS EDUARDO	Venezolano 	1975-09-08	\N	\N	Masculino 	Soltero 	\N	decimavilla64@gmail.com 	PELAEZ A ALCABALA, PUENTE SUCRE CASA 92 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1638	9592971	TORREALBA TORRES, YNGRID MARISOL	Venezolano 	1966-01-26	\N	\N	Femenino 	Soltero 	\N	ymtorrealba@ubv.edu.ve 	AV FRANCISCO SOLANO  LOPEZ CENTRO RESIDENCIAL SOLANO  TORRE A PISO 3 APTO A-0304 SABANA GRANDE 	0	INSTRUCTOR	DOCENTE	2006-05-15	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1639	9709329	GARCIA PADILLA, DENNYS JOSEFINA	Venezolano 	1968-04-06	\N	\N	Femenino 	Soltero 	\N	djgarcia@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2005-03-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1640	9808042	LÓPEZ DE LEON, ALEXANDER RENIER	Venezolano 	1968-06-11	\N	\N	Masculino 	Soltero 	\N	arlopezd@ubv.edu.ve 	CALLE MORELOS, QUINTA IRENE URBANIZACION SANTA IRENES, PUNTO FIJO, ESTADO FALCON 	0	INSTRUCTOR	DOCENTE	2005-05-16	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1641	9841161	LEON CARO, ZORAYDA COROMOTO	Venezolano 	1969-10-25	\N	\N	Femenino 	Soltero 	\N	zoraly11@gmail.com 	URB EL PARAISO EDIF 1-B PISO 7 APTO 74 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1642	9864087	MARTÍNEZ GUZMAN, YULITZA JOSEFINA	Venezolano 	1968-12-02	\N	\N	Femenino 	Soltero 	\N	 	URB. SANTA CLARA, CALLE #3, CASA S/N 	0	INSTRUCTOR TCV 2	DOCENTE	2007-10-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1643	9924436	GÓMEZ ALVAREZ, EDUARDO JOSÉ	Venezolano 	1967-08-25	\N	\N	Masculino 	Soltero 	\N	ejgomez@ubv.edu.ve 	URB INDEPENDENCIA IV ETAPA CALLE #7  CASA #11  CORO EDO FALCON 	0	INSTRUCTOR	DOCENTE	2006-10-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1644	10114586	ZERPA RIVAS, ZARA BELEN 	Venezolano 	1970-06-15	\N	\N	Femenino 	Soltero 	\N	 	BARRIO UNION DE ARTIGAS CALLE AMERICA CASA 168 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1645	10169301	CALDERÓN PADILLA, CARLOS CESAR	Venezolano 	1975-05-03	\N	\N	Masculino 	Soltero 	\N	cccalderonp@ubv.edu.ve 	CARRERA 12  15-31 SAN CRISTOBAL 	1	INSTRUCTOR	DOCENTE	2007-05-02	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1646	10194041	LAYA LARA, ANDRES ELOY 	Venezolano 	1974-06-08	\N	\N	Masculino 	Soltero 	\N	aelayal@ubv.edu.ve 	RESIDENCIA LA ENSENADA EDIF MOCHIMA APTO 3-3 	0	INSTRUCTOR TCV 5	CONTRATADO	2014-07-14	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1647	10203997	FIGUEROA SALAZAR, EDGARDO ANTONIO	Venezolano 	1970-10-23	\N	\N	Masculino 	Soltero 	\N	edgardofigueroa2009@gmail.com 	SECTOR RODULFO,CARRETERA PRINCIPAL VIA ALTAGRACIA QUINTA PELUSA SANTA ANA 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1648	10300611	MARTINEZ MAITA, ROSA MALENNY	Venezolano 	1969-12-03	\N	\N	Femenino 	Soltero 	\N	rmmartinez@ubv.edu.ve 	PRADOS DEL ESTE II NRO 152 CALLE PRINCIPAL 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1649	10381876	PONTE LEON, MADELEINE DEL CARMEN	Venezolano 	1972-01-19	\N	\N	Femenino 	Soltero 	\N	mdponte@ubv.edu.ve 	RESD, EL PARAISO, 	0	INSTRUCTOR	DOCENTE	2004-09-06	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1650	10471683	CORRO, SONIA DEL VALLE	Venezolano 	1968-12-19	\N	\N	Femenino 	Soltero 	\N	 	CARRETERA PETARE-GUARENAS CALLE RAFAEL 	0	AUXILIAR I	CONTRATADO	2019-02-14	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1651	10484890	DURAN ESCHARBAY, HILDEMAR VERONICA	Venezolano 	1972-04-30	\N	\N	Femenino 	Soltero 	\N	hduran@ubv.edu.ve 	23 DE ENERO 	0	INSTRUCTOR	DOCENTE	2004-09-16	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1652	10617436	ZUÑIGA CASTILLO, ODALYS	Venezolano 	1971-07-31	\N	\N	Femenino 	Soltero 	\N	ozuniga@ubv.edu.ve 	URB TERRAZAS DEL ESTE PARCELA 63-H EDIF, 16 APTO 1-1 GUARENAS EDO MIRANDA 	0	INSTRUCTOR	DOCENTE	2004-02-25	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1653	10709205	VARGAS ROQUE, MAIRENY BERENICE	Venezolano 	1970-01-26	\N	\N	Femenino 	Soltero 	\N	mbvargas@ubv.edu.ve 	FALCON 	0	INSTRUCTOR	DOCENTE	2011-09-16	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1654	10815666	MAVAREZ LAGUNA, MARBELYS JOSEFINA	Venezolano 	1973-12-03	\N	\N	Femenino 	Soltero 	\N	 	ESQ. DE TRUCO EDIF. CATELGRANDE 	0	INSTRUCTOR	DOCENTE	2004-02-25	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1655	10815782	ZIA CIPRIANI, ANNA MARIA	Venezolano 	1955-04-26	\N	\N	Femenino 	Soltero 	\N	laprofezia55@gmail.com 	AV.PRESIDENTE MEDINA CON AV.GUAYANA,EDIF. ARSANOVA,PISO 4 APTO 8, LAS ACACIAS 	0	ASOCIADO	CONTRATADO	2018-01-22	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1656	10836130	CHAURAN SEVILLA, LISBETH COROMOTO	Venezolano 	1971-01-24	\N	\N	Femenino 	Soltero 	\N	 	CALLE 5 CASA N 39 SECTOR EL PARAISO 	0	INSTRUCTOR	DOCENTE	2011-03-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1657	10934678	IDROGO TORRES, LENIS DEL VALLE	Venezolano 	1973-04-22	\N	\N	Femenino 	Soltero 	\N	LENISIDROGO@HOTMAIL.COM 	URB.VILLA BETANIA N 4 CASA N 86 AV.FUERZAS ARMADAS 	0	INSTRUCTOR	CONTRATADO	2017-02-07	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1658	10974291	BRACHO GUANIPA, ESGARDO JOSÉ	Venezolano 	1973-02-25	\N	\N	Masculino 	Soltero 	\N	ejbracho@ubv.edu.ve 	CALLE APURE # 13, URB. SANTA IRENE PUNTO FIJO EDO. FALCON 	0	INSTRUCTOR	DOCENTE	2007-01-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1659	11010064	LEZAMA NAVARRO, SUBDELYS COROMOTO	Venezolano 	1971-07-12	\N	\N	Femenino 	Soltero 	\N	slezama@ubv.edu.ve 	CALLE MARIÑO  69, EL RINCIN CARIPITO EDO. MONAGAS 	0	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1660	11030208	CARRIZO  AGUILERA, ANNAYE DEL CARMEN	Venezolano 	1972-04-04	\N	\N	Femenino 	Soltero 	\N	 	CARACAS 	0	INSTRUCTOR	DOCENTE	2010-04-20	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1661	11198023	GARCIA REYES, ARACELIS DEL VALLE 	Venezolano 	1970-02-23	\N	\N	Femenino 	Soltero 	\N	 	LA CANDELARIA 	0	INSTRUCTOR	CONTRATADO	2012-09-16	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1662	11199617	ACEVEDO, NORHEMMA	Venezolano 	1972-04-11	\N	\N	Femenino 	Soltero 	\N	 	CARACAS 	0	INSTRUCTOR	DOCENTE	2004-04-19	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1697	14532903	CHIRINO ANDRADES, CHAYREM ADAIA	Venezolano 	1981-03-09	\N	\N	Femenino 	Soltero 	\N	cchirino@ubv.edu.ve 	AV SAN MARTIN RES LAMAS TORRE A PISO 13 APTO 132 	0	INSTRUCTOR	DOCENTE	2006-11-13	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1663	11212485	CEDEÑO BRITO, LEUDYS DEL CARMEN	Venezolano 	1974-07-28	\N	\N	Femenino 	Soltero 	\N	lccedeno@ubv.edu.ve 	URB. DELFIN MENDOZA CARRERA 4 N 27 TUCUPITA EDO TACHIRA DELTA AMACURO 	2	INSTRUCTOR	DOCENTE	2011-01-16	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1664	11287348	FARIAS SUAREZ, JENNY	Venezolano 	1973-12-19	\N	\N	Femenino 	Soltero 	\N	jfarias@ubv.edu.ve 	URB. MONTE BELLO CALLE L12 CASA # 13 MARACAIBO-ZULIA 	2	TITULAR	DOCENTE	2004-09-06	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1665	11398256	GUEDEZ ROJAS, CARLOS MANUEL	Venezolano 	1969-02-15	\N	\N	Masculino 	Casado 	\N	 	AV. FUERZAS ARMADAS EDIF. SAN AGUSTÍN APTO. 82. SANTA ROSALIA 	0	INSTRUCTOR	CONTRATADO	2012-09-16	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1666	11413564	ALVAREZ MAZA, YAHVE MARCELO	Venezolano 	1973-01-30	\N	\N	Masculino 	Soltero 	\N	ymalvarez@ubv.edu.ve 	LAS COLINAS DEL VALLE CALLE 12 RES SANTO ANGEL 	0	ASOCIADO	DOCENTE	2004-02-25	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1667	11672208	PIMENTEL, RAQUEL COROMOTO	Venezolano 	1974-09-01	\N	\N	Femenino 	Soltero 	\N	rapimentel@ubv.edu.ve 	SECTOR II LOS GODOS VEREDA 11 CASA NRO 12 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1668	11683668	VARGAS PEROZO, ERIKA COROMOTO	Venezolano 	1974-09-29	\N	\N	Femenino 	Soltero 	\N	ecvargas@ubv.edu.ve 	CALLE DON ANDRES N 09 LOS TEQUES VILA DE CURA 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1669	11727125	AZOCAR, JOSE ESTEBAN	Venezolano 	1969-10-21	\N	\N	Masculino 	Soltero 	\N	YEPUY500@GMAIL.COM 	BARRIO AJURO CALLE 9 DE MAYO CASA NO. 05 CIUDAD BOLIVAR 	0	INSTRUCTOR TCV 3	CONTRATADO	2017-01-09	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1670	11737866	RODRÍGUEZ, JUAN CARLOS	Venezolano 	1975-10-02	\N	\N	Masculino 	Soltero 	\N	 	AV MANAURE RES SELEXTAS APTO 502 EL MARQUES CCS 	0	INSTRUCTOR	DOCENTE	2005-03-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1671	11767874	GOITIA SANCHEZ, SACHENKA BERIOSKA	Venezolano 	1973-11-26	\N	\N	Femenino 	Soltero 	\N	sbgoitia@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2009-04-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1672	11769823	NAVARRO LOYO, ADRIAN JOSÉ	Venezolano 	1974-03-04	\N	\N	Masculino 	Soltero 	\N	ajnavarro@ubv.edu.ve 	AVENIDA 15 ENTRE CALLE 7 Y 8 CASA # 7-239 COMUNIDAD CARDON, PUNTOFIJO ESTADO FALCON 	0	INSTRUCTOR	DOCENTE	2005-05-16	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1673	11947026	BARRETO SANCHEZ, EMILY HAYDEE	Venezolano 	1974-10-30	\N	\N	Femenino 	Soltero 	\N	ehbarreto@ubv.edu.ve 	URBANIZACION ESPAÑA CALLE ESPAÑA CASA 3 2 	0	INSTRUCTOR	DOCENTE	2005-03-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1674	12210992	ARREDONDO RAMIREZ, YAGNORA	Venezolano 	1976-02-12	\N	\N	Femenino 	Soltero 	\N	yaarredondo@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2010-11-16	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1675	12297042	VELASCO COLMENARES, NAZARETH DE JESÚS	Venezolano 	1975-09-17	\N	\N	Femenino 	Soltero 	\N	nvelasco@ubv.edu.ve 	URBANIZACION DOÑA MENCA DE LEONI AV. MARTIN VERA 	0	INSTRUCTOR	DOCENTE	2006-10-31	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1676	12298659	HERNANDEZ GRATEROL, ROBERTO RAFAEL	Venezolano 	1976-09-29	\N	\N	Masculino 	Soltero 	\N	chinovaleramora@gmail.com 	LAS TERRAZAS AV PRINCIPAL VICENTE EMILIO EDIF 35 PISO 3 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1677	12454885	FUENTES NIEVES, JESÚS ALFREDO	Venezolano 	1976-11-23	\N	\N	Masculino 	Soltero 	\N	jfuentes@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	DOCENTE	2004-04-19	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1678	12490929	MONTILVA SANCHEZ, NARLY ROSIRIS	Venezolano 	1976-07-18	\N	\N	Femenino 	Soltero 	\N	nrmontilva@ubv.edu.ve 	LA  CALIFORNIA NORTE, CALLE PARIS 	0	INSTRUCTOR	DOCENTE	2004-02-25	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1679	12505567	NARVAEZ APONTE, ANNLIX KARIJOVA	Venezolano 	1975-12-02	\N	\N	Femenino 	Soltero 	\N	 	SECTOR RODULFO, QUINTA PELUSA, SANTA ANA VIA PRINCIPAL A ALTAGRACIA 	0	INSTRUCTOR	CONTRATADO	2018-01-22	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1680	12506811	EGLYS CLARET, SALAZAR CAMPOS	Venezolano 	1976-09-01	\N	\N	Femenino 	Soltero 	\N	ecsalazar@ubv.du.ve 	AV.JUAN BAUTISTA ARISMENDI LAS HERNANDEZ 	0	INSTRUCTOR	CONTRATADO	2016-06-15	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1681	12569879	MARIN HERNANDEZ, GUSTAVO ALFONSO	Venezolano 	1974-10-27	\N	\N	Masculino 	Soltero 	\N	 	URB. RUIZ PINEDA, ESCALERA 01, BLOQUE 03, APTO 03-02 	0	INSTRUCTOR	DOCENTE	2011-03-16	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1682	12671411	SANCHEZ, MALEISKA	Venezolano 	1977-02-14	\N	\N	Femenino 	Soltero 	\N	maleiska@ubv.edu.ve 	LA CALIFORNIA FRANCISCO DE MIRANDA CALLEJON EL LAGO EDF. EL LAGO E-1 	0	INSTRUCTOR	DOCENTE	2004-11-15	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1683	12716545	ESCOBAR BERROTERAN, DELVIS DANIEL	Venezolano 	1977-04-16	\N	\N	Masculino 	Soltero 	\N	descobar@ubv.edu.ve 	PUEBLO NUEVO CALLE LA CUADRA CASA S/N FRENTE LA CANCHA 	0	INSTRUCTOR	CONTRATADO	2012-09-16	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1684	12891058	ROMERO LOPEZ, FRANCIS DE DIOS	Venezolano 	1976-08-06	\N	\N	Femenino 	Soltero 	\N	francisdedios@gmail.com 	VILLA CENTRAL EDIF.21 APTO D-1 PUERTO ORDAZ 	0	INSTRUCTOR	CONTRATADO	2018-01-23	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1685	13114251	ESCARRA GIL, CAROLINA UYUNNI	Venezolano 	1977-08-29	\N	\N	Femenino 	Soltero 	\N	 	AV PRINCIPAL DE CAURIMARE EDIF ARICHUNA APTO A-1 	0	INSTRUCTOR	DOCENTE	2013-09-16	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1686	13303812	SERRANO LOPEZ, EGLE CORADI	Venezolano 	1979-07-01	\N	\N	Femenino 	Soltero 	\N	eserrano@ubv.edu.ve 	CALLE 6M 13 ALTOS DE PARAMILLO TACHIRA, VENEZUELA 	2	AGREGADO	DOCENTE	2006-09-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1687	13311829	REYES CHING, YUNMERY JOSEFINA	Venezolano 	1977-12-24	\N	\N	Femenino 	Soltero 	\N	 	QDA DE CUA AV PRINCIPAL ARAGUANEY EDIF ARGENTINA 	0	INSTRUCTOR	DOCENTE	2007-04-21	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1688	13424381	REYES DE VENALES, CARMEN SOFIA	Venezolano 	1976-09-18	\N	\N	Femenino 	Casado 	\N	 	APOSTADERO URBANIZACION LAS PERLAS PAMPATAR 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1689	13622859	AGÜERO MARTÍNEZ, CARLOS ALBERTO	Venezolano 	1950-09-30	\N	\N	Masculino 	Viudo 	\N	caguero@ubv.edu.ve 	VEREDA 22 # 06 ALTA VISTA SUR 	0	INSTRUCTOR	DOCENTE	2005-11-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1690	13642880	CEDEÑO PACHECO, CRUZ EDUARDO	Venezolano 	1978-08-02	\N	\N	Masculino 	Soltero 	\N	cecedeno@ubv.edu.ve 	CCS 	0	INSTRUCTOR	DOCENTE	2011-04-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1691	13673250	RODRIGUEZ GOMEZ, ROELIA DEL VALLE	Venezolano 	1979-02-03	\N	\N	Femenino 	Soltero 	\N	roerodriguez@ubv.edu.ve 	CALLE COLON C/C TRUJILLO CASA 57 SECTOR LA SABANITA ESTADO BOLIVAR CIUDAD BOLIVAR 	0	INSTRUCTOR TCV 3	CONTRATADO	2017-01-09	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1692	14168849	PÉREZ PAREDES, KELLY DUBRASKA	Venezolano 	1977-06-29	\N	\N	Femenino 	Soltero 	\N	kperez@ubv.edu.ve 	URB. PEDRO MANUEL ARCAYA, AV. C, MANZANA M, CASA # M-46 	0	INSTRUCTOR	DOCENTE	2007-10-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1693	14196505	FREITES RODRIGUEZ, INGERZON DAVID	Venezolano 	1978-04-15	\N	\N	Masculino 	Soltero 	\N	 	SECTOR 2 BLOQUE 5 PISO 4 APTO. 401 	0	INSTRUCTOR TCV 3	CONTRATADO	2012-09-16	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1694	14307117	LIENDO LEZAMA, ROSELYS DEL VALLE	Venezolano 	1979-10-03	\N	\N	Femenino 	Soltero 	\N	rliendo@ubv.edu.ve 	DESARROLLO URBANISTICO JAIRO ALFONZO LLANOS MORALES CASA N 27 LOS PROCERES 	0	INSTRUCTOR TCV 5	DOCENTE	2005-04-28	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1695	14433145	URBINA QUINTERO , WILFREDO	Venezolano 	1978-03-24	\N	\N	Masculino 	Soltero 	\N	wurbina@ubv.edu.ve 	BARINAS 	0	INSTRUCTOR	CONTRATADO	2013-09-16	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1698	14630646	PAZ ALBORNOZ, MARY OLGA	Venezolano 	1980-09-15	\N	\N	Femenino 	Soltero 	\N	mopaz@ubv.edu.ve 	SAN FRANCISCO, AV. 4 N 27-91 	0	INSTRUCTOR	DOCENTE	2005-03-10	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1699	14869874	RODRIGUEZ URRUTIA, ELIAIRA ANDREINA	Venezolano 	1982-05-13	\N	\N	Femenino 	Soltero 	\N	earodriguez@ubv.edu.ve 	LA ROSA, EDIF. V, APARTAMENTO V-11, PB 	0	INSTRUCTOR TCV 3	CONTRATADO	2013-06-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1700	14907078	BLANCO LÓPEZ, FREILICE ARLENYS	Venezolano 	1982-04-12	\N	\N	Femenino 	Casado 	\N	 	SECTOR 23 DE ENERO, CALLE REAL SIERRA MAESTRA, BARRIO SANTA ROSA, CASA N 45 	0	INSTRUCTOR	CONTRATADO	2012-09-16	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1701	15118564	LEON NAVAS, AGUSTIN ENRIQUE	Venezolano 	1980-02-24	\N	\N	Masculino 	Soltero 	\N	agustineln@gmail.com 	RESIDENCIAS SUZZANE PISO 03 APTO 31 CALLE LA ANUNCIACION SECTOR PICACHO SAN ANTONIO DE LOS ALTOS 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1702	15153854	ESPINOZA MACHADO, ADNALOY AMIRUAY	Venezolano 	1981-02-20	\N	\N	Femenino 	Soltero 	\N	ladn0881@gmail.com 	RUIZ PINEDA-CARICUAO UD 7 BLOQUE 1 PISO 14 APTO-14-02 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1703	15244599	VILLALBA SUNIAGA, LUIS ALEXANDER	Venezolano 	1980-03-11	\N	\N	Masculino 	Soltero 	\N	gabrielaysofia5@gmail.com 	CALLE LOS JABILLOS URBANIZACION ATAHUALPA APT 03 PISO 10 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1704	15246121	OCANTO CARDOZO, JOSE DEL CARMEN	Venezolano 	1982-12-25	\N	\N	Masculino 	Soltero 	\N	jocanto@ubv.edu.ve 	SECTOR CASCO HISTORICO CIUDAD BOLIVAR 	0	INSTRUCTOR	CONTRATADO	2017-01-09	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1706	15587748	CENTENO POTTELLA, ALONSO ALEJANDRO	Venezolano 	1982-06-27	\N	\N	Masculino 	Soltero 	\N	alonsocenteno@gmail.com 	SANTA ROSALIA 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1708	16431590	HERNANDEZ  BIELIUKAS, VICTOR EDUARDO	Venezolano 	1984-05-05	\N	\N	Masculino 	Soltero 	\N	vehernandez@ubv.edu.ve 	AV. PANTEON ESQ. DE SAN NARCISO, EDIF. ANDREA, PISO 18 APTO. 18-D, SAN JOSE CARACAS 	0	INSTRUCTOR	CONTRATADO	2017-01-09	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1709	16496178	MUÑOZ OLIVERO, GUADALUPE DEL CARMEN 	Venezolano 	1984-12-01	\N	\N	Femenino 	Soltero 	\N	guadalupemunoz1984@gmail.com 	URB LAS ACACIAS, AV HONDURAS QUINTA PIPINA PB 	0	INSTRUCTOR	CONTRATADO	2015-09-17	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1710	16545939	DIAZ, ROSMIRA NAZARET	Venezolano 	1983-03-17	\N	\N	Femenino 	Soltero 	\N	rdiaz@ubv.edu.ve 	CALLE PRINCIPAL AGUA DE VACA CASA N 23 	0	INSTRUCTOR	CONTRATADO	2016-06-15	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1711	16557036	FAJARDO JIMENEZ, LILLY CHARLOT	Venezolano 	1984-06-21	\N	\N	Femenino 	Soltero 	\N	 	CARACAS 	0	AUXILIAR I	DOCENTE	2009-01-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1712	16834522	GOVEA GONZALEZ, WILBER SEGUNDO	Venezolano 	1984-02-19	\N	\N	Masculino 	Soltero 	\N	wsegundo@ubv.edu.ve 	URB LOS ROSALES, AV LUISA CACERES DE ARISMENDI 	0	INSTRUCTOR	CONTRATADO	2013-02-18	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1713	17110518	MARCANO JIMENEZ, DEIVIC JOSE	Venezolano 	1983-07-03	\N	\N	Masculino 	Soltero 	\N	MARCANODEIVIC@GMAIL.COM 	CALLE SOUBLETTE CASA N 52-23 URBANIZACION LA LAGUNITA EL ESPINAL 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1714	17136002	WELMANS GONZALEZ, ALVARO LUIS	Venezolano 	1984-01-09	\N	\N	Masculino 	Casado 	\N	alwelmans@ubv.edu.ve 	URB. JORGE HERNANDEZ, AV. HECTOR M PEÑA SECTOR #2 C/08 BANCO OBRERO 	0	INSTRUCTOR	DOCENTE	2011-03-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1715	17287527	MERTZ RODRIGUEZ, JEAN PAUL	Venezolano 	1986-09-20	\N	\N	Masculino 	Soltero 	\N	 	CALLE MARACAY QUINTA UTOPIA, SAN JOSE DE LOS ALTOS 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1716	17562064	PALOMINO MAYORGA, LUZ DARY 	Venezolano 	1985-10-16	\N	\N	Femenino 	Soltero 	\N	lpalomino@ubv.edu.ve 	URB.LOS SAMANES CONJUNTO RES. EL NARANJAL 	2	INSTRUCTOR TCV 4	CONTRATADO	2012-11-01	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1717	17741041	SUAREZ BRITO, ANGEL JOSE ANTONIO	Venezolano 	1984-07-25	\N	\N	Masculino 	Soltero 	\N	angelnegro_767@hotmail.com 	CALLE VUELTA DE LA OLLA CASA NR 20 SECTOR MIRADOR 	0	AUXILIAR I	CONTRATADO	2017-10-30	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1718	17755717	TORRES ZAPATA, CRISTOFER ENRIQUE	Venezolano 	1987-03-13	\N	\N	Masculino 	Soltero 	\N	1987CETZ@GMAIL.COM 	SECTOR VUELTA EL FRAILE, CALLEJON 19 DE ABRIL, CASA N002 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1719	17772297	YVIRMA VASQUEZ, YUSBALY MERCEDES	Venezolano 	1987-01-29	\N	\N	Femenino 	Soltero 	\N	yusbalyvirma@gmail.com 	SECTOR CUPO BARLOVENTO MARIZAPA CALLE LAS CHAMISAS CASA NRO 8 	0	INSTRUCTOR	CONTRATADO	2019-02-11	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1720	17828536	MONTILLA LEON , ORLANDO JOSE 	Venezolano 	1986-10-29	\N	\N	Masculino 	Soltero 	\N	omontilla@ubv.edu.ve 	AV VICTORIA PROLONGACION CALLE EL PROGRESO CASA N 10-24 	0	INSTRUCTOR	CONTRATADO	2014-02-16	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1721	18446412	SAHMKOW ORELLANA, IEHOVANA MAITREYA	Venezolano 	1985-10-02	\N	\N	Femenino 	Soltero 	\N	isahmkow@ubv.edu.ve 	KM 19 CARRETERA VIA AL JUNQUITO URB EL JUNKO CALLE GLORIA QUINTA MI MAITREYA 129 	0	INSTRUCTOR	CONTRATADO	2015-03-02	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1722	19314495	MENDEZ GUTIERREZ, JESUS EDUARDO	Venezolano 	1988-09-11	\N	\N	Masculino 	Soltero 	\N	jemendez@ubv.edu.ve 	AV.SAN NICOLAS DE BARI, URB VALLES DE CHARA, CONJUNTO VALLE PLATA, TORRE A APTO 32-A 	0	INSTRUCTOR	CONTRATADO	2016-06-15	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1723	19710708	ARTEAGA GARCIA, WINNER ALBERTO 	Venezolano 	1990-04-06	\N	\N	Femenino 	Soltero 	\N	 	AV INTERVECINAL ENTRE SANTA MONICA Y CUMBRE DE CURUMO MANDELA TORRE C PISO 7 APTO C702 	0	INSTRUCTOR TCV 3	CONTRATADO	2019-02-11	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1724	20195076	MEDINA RUIZ, LENNYS ALEXANDRIA	Venezolano 	1990-05-17	\N	\N	Femenino 	Soltero 	\N	LENNYS.MEDINA17@GMAIL.COM 	URB CIUDAD TIUNA TORRE B  09- FUERTE TIUNA 	0	INSTRUCTOR TCV 3	CONTRATADO	2019-02-11	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1725	21250743	FARIAS HENRIQUEZ, MANUEL ATAHUALPA	Venezolano 	1993-04-20	\N	\N	Masculino 	Soltero 	\N	 	URB LA MACARENA MANZANA 24 CASA NRO 60 	0	INSTRUCTOR	CONTRATADO	2017-07-11	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1726	21640527	ORTA MILANO, KEVIN RAMON	Venezolano 	1991-10-22	\N	\N	Masculino 	Soltero 	\N	KEVORTA@GMAIL.COM 	COROCITO CALLE PICHINCHA N 29 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1727	22014778	JIMENEZ DOMINGUEZ, RANSES	Venezolano 	1984-10-06	\N	\N	Masculino 	Soltero 	\N	rajimenez@ubv.edu.ve 	BARRIO SAN ANDRES, CALLE EL TAMARINDO, PARADA 4, CASA 10 	0	AUXILIAR I	CONTRATADO	2015-02-02	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1762	4054485	FLORES MATAMOROS, LUIS RAUL	Venezolano 	1955-05-27	\N	\N	Masculino 	Soltero 	\N	lflores@ubv.edu.ve 	CALLE LA FRANCESA CASA NRO 43 EL VIGIA 	3	ASISTENTE	JUBILADOS	2005-03-03	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1728	22653315	RAMIREZ FERNANDEZ, AIDA PILAR	Venezolano 	1963-09-22	\N	\N	Femenino 	Soltero 	\N	PILLIRAMIREZ_22@HOTMAIL.COM 	URB VILLAS DE SAN ANTONIO CASA 14-A TERRAZA 1 NUEVA ESPARTA 	0	INSTRUCTOR	CONTRATADO	2017-06-26	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1729	22817424	SOTO VICUÑA, LISEHINIT DEL CARMEN	Venezolano 	1990-03-16	\N	\N	Femenino 	Soltero 	\N	lisehinit@ubv.edu.ve 	CALLE LOS OLIVOS CASA 40 BARRIO LLANO ALTO. 	0	INSTRUCTOR	CONTRATADO	2017-07-11	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1730	22910365	PEREIRA, ZEGRI MARGARITA	Venezolano 	1958-08-14	\N	\N	Femenino 	Soltero 	\N	zpereira@ubv.edu.ve 	URB LAS FLORES 	0	INSTRUCTOR	DOCENTE	2004-04-12	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1731	30761009	FONTAINE, LENES	Venezolano 	1969-05-07	\N	\N	Masculino 	Soltero 	\N	lfontaine@ubv.edu.ve 	BELLO MONTE 	0	INSTRUCTOR	DOCENTE	2007-09-17	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1732	31455046	WONG MAESTRE, ERNESTO	Venezolano 	1948-11-07	\N	\N	Masculino 	Casado 	\N	 	S 	0	INSTRUCTOR	CONTRATADO	2018-06-18	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1733	84482780	CARISME, CADET	Extranjero 	1990-05-20	\N	\N	Masculino 	Soltero 	\N	ccarisme@ubv.edu.ve 	SECTOR LOS CUATRO VIENTOS, CASA N 155, LOS FRAILES 	0	INSTRUCTOR	CONTRATADO	2016-01-11	DOCENTE TIEMPO CONVENCIONAL	ACTIVO
1735	1565892	BRAVO DE CARABALLO, OGLA ISABEL	Venezolano 	1952-05-14	\N	\N	Femenino 	Soltero 	\N	obravo@ubv.edu.ve 	BULEVAR EL CAFETAL EDIF. PELDRAGREPISO 5 APTO 51 	0	AGREGADO	JUBILADOS	2005-02-21	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1736	2088111	PEÑA, GONZALO ALFONZO	Venezolano 	1941-11-25	\N	\N	Masculino 	Soltero 	\N	gopena@ubv.edu.ve 	EL CAFETAL. MARA A. APTO 41. FRENTE AL COLEGIO SAN LUIS 	0	INSTRUCTOR	JUBILADOS	2004-02-25	DOCENTE TIEMPO CONVENCIONAL	PASIVO
1737	2509725	ALVAREZ GARAICOECHEA, ZULAY	Venezolano 	1944-01-15	\N	\N	Femenino 	Soltero 	\N	zualvarez@ubv.edu.ve 	LA URBIAN CALLE N12. EDIF. ZEUS APART 1-B. 	0	INSTRUCTOR	JUBILADOS	2005-03-28	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1738	2618622	TORO ARAUJO, PABLO ORANGEL	Venezolano 	1943-03-27	\N	\N	Masculino 	Soltero 	\N	 	SECTOR EL RINCON.LOS TEQUES ESTADO MIRANDA 	2	AUXILIAR I	JUBILADOS	2006-10-15	DOCENTE TIEMPO COMPLETO	PASIVO
1739	2824494	BRICEÑO PEÑA, AMILCAR DE JESÚS	Venezolano 	1950-01-29	\N	\N	Masculino 	Casado 	\N	ajbricenop@ubv.edu.ve 	LOMAS VERDE, CARERA 1 CON CALLE 3, VIA EL CERCAO 	1	ASISTENTE	JUBILADOS	2005-04-01	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1740	2940712	PÉREZ GARCIA, LUIS ALFREDO	Venezolano 	1944-05-01	\N	\N	Masculino 	Soltero 	\N	laperezg@ubv.edu.ve 	URB. BASE ARAGUA, AV. PPAL, EDIF. CUYAGUA, PISO 6 APTO N 64/A 	0	AUXILIAR II	JUBILADOS	2007-10-01	DOCENTE TIEMPO COMPLETO	PASIVO
1741	2944595	BALZAN MORRELL, HUGO	Venezolano 	1945-06-11	\N	\N	Masculino 	Soltero 	\N	hubalzan@ubv.edu.ve 	RIO CHICO CALLE LOS APAMATES PARCELA # 4-11  CASA CAROLINA 	0	INSTRUCTOR	JUBILADOS	2006-09-01	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1742	3027221	GUZMAN AYALA, OSCAR DAGOBERTO	Venezolano 	1947-11-14	\N	\N	Masculino 	Viudo 	\N	odguzman@ubv.edu.ve 	EDIFICIO MARIANGELICA APARTAMENTO 4-C ENTRADA DE LAS COCUIZA 	0	INSTRUCTOR	JUBILADOS	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1743	3029842	LÓPEZ, GILBERTO JOSÉ	Venezolano 	1947-07-12	\N	\N	Masculino 	Casado 	\N	gilopez@ubv.edu.ve 	URB. LA TERESERA CALLE A CASA N 16 VIA PLANTACION MATURIN 	1	ASISTENTE	JUBILADOS	2006-06-01	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1744	3061868	JAIMES, MARISOL	Venezolano 	1957-05-01	\N	\N	Femenino 	Soltero 	\N	mjaimes@ubv.edu.ve 	URB. SANTA ROSA CALLE MATEO MANUERE 	1	ASISTENTE	JUBILADOS	2006-11-01	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1745	3255078	GONZALEZ PERDOMO, MAGALY	Venezolano 	1946-03-05	\N	\N	Femenino 	Soltero 	\N	mgonzalezp@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	JUBILADOS	2007-05-07	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1746	3396807	PÉREZ KEPP, ROBERTO JOSÉ	Venezolano 	1949-09-25	\N	\N	Masculino 	Soltero 	\N	rperezk@ubv.edu.ve 	CARACAS 	0	ASISTENTE	JUBILADOS	2004-01-07	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1747	3396809	PÉREZ KEPP, OLIVIA SOFIA	Venezolano 	1951-09-18	\N	\N	Femenino 	Soltero 	\N	operez@ubv.edu.ve 	EDIF. PARQUE CACHAMAY. APTO, 5-A.CALLE COROMOTO, URB. CALICANTO 	0	ASISTENTE	JUBILADOS	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1748	3409401	LÓPEZ REVETE, ROGELIO	Venezolano 	1948-06-27	\N	\N	Masculino 	Soltero 	\N	rlopez@ubv.edu.ve 	URBANIZACION LA CAMPIÑA CALLE RUBEN DARIO CASA N 72 EJIDO 	3	INSTRUCTOR	JUBILADOS	2007-01-01	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1749	3548071	GARCIA ABREU, SERGIO ANTONIO	Venezolano 	1950-07-28	\N	\N	Masculino 	Soltero 	\N	sagarcia@ubv.edu.ve 	RES.MONTE REAL SECTOR RIO NEGRO,MANZANA11 CASAN20,URARE PUERTO ORDAS 	2	TITULAR	JUBILADOS	2003-10-16	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1750	3550368	PÉREZ, TERESA DE JESÚS	Venezolano 	1950-08-27	\N	\N	Femenino 	Soltero 	\N	tperez@ubv.edu.ve 	LOMAS DE URQUIA CALLE BERTHA MUJICA CASA LAS TRES HERNANAS CARRIZAL ESTADO MIRANDA 	0	INSTRUCTOR	JUBILADOS	2008-01-01	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1751	3637837	CHIRINOS BRACHO, WILLIAM ALFONZO	Venezolano 	1951-06-24	\N	\N	Masculino 	Soltero 	\N	 	URB EL NORAYAL AV 15K 50A-30 	1	ASISTENTE	JUBILADOS	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1752	3649577	RIOS MARQUEZ, MAGALI DEL CONSUELO	Venezolano 	1948-01-24	\N	\N	Femenino 	Soltero 	\N	mrios@ubv.edu.ve 	AGRUPACION EL PILARCITO CASA N 15 AV 12B 	0	ASISTENTE	JUBILADOS	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1753	3751698	COLOMINE RINCONES, LUISANA	Venezolano 	1952-09-16	\N	\N	Femenino 	Soltero 	\N	lcolomine@ubv.edu.ve 	CALLE VOLTAIRE, EDIF. IGUAZU, PISO 1. APTO 1-B COLINAS DE BELLO MONTE 	0	ASISTENTE	JUBILADOS	2007-06-11	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1754	3764430	BERRIOS VILLARREAL, JOSÉ ELIBERTO	Venezolano 	1952-01-01	\N	\N	Masculino 	Soltero 	\N	jberrios@ubv.edu.ve 	URB. SABOES IV, N 15, PUENTE MONAJON 	2	TITULAR	JUBILADOS	2004-04-30	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1755	3820901	ÁVILA MORA, TULA MARIA	Venezolano 	1948-08-14	\N	\N	Femenino 	Soltero 	\N	tmavila@ubv.edu.ve 	EDIFICIO LA TRINIDAD APTO 3-A N 10-30  CALLE REAL DE ALTA VISTA CATIA - PARROQUIA SUCRE 	0	ASISTENTE	JUBILADOS	2005-03-03	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1756	3830750	GARCIA, CRISTOBAL GABRIEL	Venezolano 	1951-05-20	\N	\N	Masculino 	Soltero 	\N	cgarcia@ubv.edu.ve 	AV 3E N 68-171, CASA QUINTA PALMICHAL COLONIA BELLA VISTA DETRAS DEL TEATRO BELLAS ARTES 	0	ASISTENTE	JUBILADOS	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1757	3887530	PASTRAN LOZADA, GONZALO	Venezolano 	1952-07-08	\N	\N	Masculino 	Casado 	\N	gpastran@ubv.edu.ve 	Urb San Carlos. Manzan D-2.   Num 19. 	1	ASISTENTE	JUBILADOS	2005-10-05	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1758	3974652	PIÑANGO PIÑANGO, FERNANDO DE LA CRUZ	Venezolano 	1952-11-24	\N	\N	Masculino 	Soltero 	\N	 	SAN JOSE CALLE SANTA ROSA A ISABEL 	0	INSTRUCTOR	JUBILADOS	2005-04-01	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1759	3975853	ROSAS TORRES, MARÍA DE LA COROMOTO	Venezolano 	1951-05-08	\N	\N	Femenino 	Soltero 	\N	mrosas@ubv.edu.ve 	AVENIDA INTERCOMUNAL GUARENAS- GUATIRE MUNICIPIO PLAZA ESTADO MIRANDA 	0	ASISTENTE	JUBILADOS	2005-10-27	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1760	4030121	CARNEIRO CARABALLO, JESÚS BRAULIO	Venezolano 	1952-11-11	\N	\N	Masculino 	Divorciado 	\N	jbcarneiro@ubv.edu.ve 	URB. RIO CAURA, MANZANA 13, CASA #13 PUERTO ORDAZ 	0	AUXILIAR I	JUBILADOS	2004-09-01	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1761	4036134	MATTEY LIRA, JUAN ANDRES	Venezolano 	1952-06-28	\N	\N	Masculino 	Soltero 	\N	jamattey@ubv.edu.ve 	SAN ENRIQUE PRINCIPAL 	2	INSTRUCTOR	JUBILADOS	2007-01-15	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1763	4076883	MORALES ROSSI, ÁNGEL DELVALLE	Venezolano 	1952-06-07	\N	\N	Masculino 	Soltero 	\N	amorales@ubv.edu.ve 	CALLE CARABOBO N 72 CASCO HISTORICO 	0	AUXILIAR I	JUBILADOS	2004-03-15	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1764	4117223	POUCHOULO, TAMARA	Venezolano 	1955-10-28	\N	\N	Femenino 	Soltero 	\N	tpouchoulo@ubv.edu.ve 	URB. SOUBLETTE, FRENTE ENTRADA MARAPA, CASA N 23-1 CATIA LA MAR 	0	AGREGADO	JUBILADOS	2004-03-01	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1765	4117842	FELICE CARQUEZ, MEYLYN ELENA	Venezolano 	1951-08-14	\N	\N	Femenino 	Soltero 	\N	mfelice@ubv.edu.ve 	SECTOR EL ARADO, EDIF. #3, APTO. 3-33 	0	INSTRUCTOR	JUBILADOS	2007-05-15	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1766	4161579	SALAS DE GARCIA, ROSILDA JOSEFINA	Venezolano 	1954-10-13	\N	\N	Femenino 	Soltero 	\N	rsalas@ubv.edu.ve 	AV. 3E # 68 -171- SECTOR BELLAS ALTES, MARACAIBO -ESTADO ZULIA 	1	AGREGADO	JUBILADOS	2004-02-26	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1767	4163667	MENDEZ DIAZ, FREDDY JOSÉ	Venezolano 	1952-06-23	\N	\N	Masculino 	Soltero 	\N	fjmendezd@ubv.edu.ve 	URB. LAS ROSAS, SECTOR LA PENINSULA, EDIF. 1, APTO. I-24 PISO 1, GUATIRE 	0	ASISTENTE	JUBILADOS	2006-07-27	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1768	4355198	LEON, NAPOLEON JOSÉ	Venezolano 	1953-05-24	\N	\N	Masculino 	Soltero 	\N	njleon@ubv.edu.ve 	LA HACIENDA CARICUA, SECTOR UD4 MUCURITA BLOQUE 13, EDIFICIO PALMARITO PISO 3, APTO 03 	0	AUXILIAR II	JUBILADOS	2006-04-01	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1769	4356096	LARA CARRERO, CARMEN CECILIA	Venezolano 	1954-04-13	\N	\N	Femenino 	Soltero 	\N	clara@ubv.edu.ve 	CALLE CHAMA, CENTRO POLO TORRE B, PISO 10 APTO 104 COLINAS DE BELLO MONTE CARACS 	0	INSTRUCTOR	JUBILADOS	2004-02-25	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1770	4746288	MAYOR VIVAS, MELVIN JESÚS	Venezolano 	1954-09-24	\N	\N	Masculino 	Soltero 	\N	mmayor@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	JUBILADOS	2006-06-15	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1771	4760540	ORTIZ GERMAN, BEATRIZ COROMOTO	Venezolano 	1956-12-27	\N	\N	Femenino 	Soltero 	\N	bcortiz@ubv.edu.ve 	COJUNTO RES. EL VARILLAL, EDF. LA CEIBA, #4 APTO. 3-A DE LA CIUDAD DE MARACAIBO 	0	ASISTENTE	JUBILADOS	2005-02-01	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1772	4806683	GUEVARA SOTO, LUIS ALBERTO	Venezolano 	1956-09-07	\N	\N	Masculino 	Soltero 	\N	laguevara@ubv.edu.ve 	AV ANAUCO RES LA WLINA PISO 4 APTO 4-13 COLINAS DE BELLO MONTE 	0	INSTRUCTOR	JUBILADOS	2004-03-22	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1773	4841644	ALVAREZ SAYAGO, ISNIRIDA COROMOTO	Venezolano 	1955-08-01	\N	\N	Femenino 	Soltero 	\N	ialvarez@ubv.edu.ve 	CALLE LA FRANCESA WEDIF. VILLAQ FRANCESA PISO 13-APTRO 13-C LOS TEQUES 	1	ASISTENTE	JUBILADOS	2004-04-19	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1774	4884377	HENRÍQUEZ FARFAN, TERESA	Venezolano 	1958-02-24	\N	\N	Femenino 	Soltero 	\N	thenriquez@ubv.edu.ve 	ESQ. SAN PEDRO CAN JUANM APTO 212 SAN JUAN 	0	ASISTENTE	JUBILADOS	2006-10-16	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1775	4975614	MADRID CORTEZ, MANUEL RAFAEL	Venezolano 	1960-07-28	\N	\N	Masculino 	Casado 	\N	mmadrid@ubv.edu.ve 	CARACAS 	1	INSTRUCTOR	JUBILADOS	2011-09-16	DOCENTE TIEMPO COMPLETO	PASIVO
1776	4981641	MORALES ROSSI, ANDRÉS ELOY	Venezolano 	1957-05-27	\N	\N	Masculino 	Casado 	\N	aemorales@ubv.edu.ve 	MARUAHNTA SUR CALLE LA COLINA N 32 CIUDAD BOLIVAR 	1	AUXILIAR I	PENSIONADOS	2004-03-15	DOCENTE TIEMPO COMPLETO	PASIVO
1777	5002505	CACERES TEJERA, ANA CECILIA	Venezolano 	1957-05-17	\N	\N	Femenino 	Soltero 	\N	acaceres@ubv.edu.ve 	AV. EL EJERCITO C/C MACHADO EDIF. KYOTO, PISO 8, APTO 81, URBANIZACION EL PARAISO 	0	INSTRUCTOR	PENSIONADOS	2007-10-16	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1778	5132900	CARDENAS RUIZ, ILDA LORENA	Venezolano 	1955-04-19	\N	\N	Femenino 	Soltero 	\N	icardenas@ubv.edu.ve 	AV. JOSE ANTONIO PAEZ, CALLE LOYOLA UNIDAD RESIDENCIAL EL PARAISO. EDIF...3, PISO 12 APTO.B 22, URBA 	0	AUXILIAR II	JUBILADOS	2007-10-16	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1779	5162022	GONZALEZ RAMA, ROSANA MARGARITA	Venezolano 	1956-11-28	\N	\N	Femenino 	Casado 	\N	rgonzalez@ubv.edu.ve 	COLINAS DE BELLO MONTE CALLE CAURIMARE CALLEJON RANAL 	1	ASISTENTE	JUBILADOS	2006-10-02	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1780	5221437	GARCIA JIMENEZ, HÉCTOR LUIS	Venezolano 	1958-05-01	\N	\N	Masculino 	Soltero 	\N	hlgarcia@ubv.edu.ve 	URB LECUMBERRY MANZANA T # 816 QTA AZAHAR CUA EDO MIANDA 	0	ASISTENTE	JUBILADOS	2005-03-14	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1781	5396337	VARGAS POLIQUET, ARGENIS RAFAEL	Venezolano 	1957-05-25	\N	\N	Masculino 	Casado 	\N	arvargas@ubv.edu.ve 	BLOQUE 2 UNARE II CESTRO 1 APTO 04-08 	1	AGREGADO	JUBILADOS	2005-10-16	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1782	5534053	PALUMBO GIOIA, ADA	Venezolano 	1961-04-25	\N	\N	Femenino 	Soltero 	\N	apalumbo@ubv.edu.ve 	URB. LOMAS DE MATURIN, CALLE GUANIPA QTA. ACUARIO, EL CAFETAL 	0	INSTRUCTOR	JUBILADOS	2005-10-10	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1783	5548023	RUIZ LAZARDY, CECILIA MERCEDES	Venezolano 	1961-08-21	\N	\N	Femenino 	Soltero 	\N	cruiz@ubv.edu.ve 	CALLE UNIÓN 5-A  SECTOR LA ROMANA MARACAY EDO ARAGUA 	0	INSTRUCTOR	JUBILADOS	2008-02-15	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1784	5609770	AZCOAGA, MARÍA	Venezolano 	1953-04-16	\N	\N	Femenino 	Soltero 	\N	mazcoaga@ubv.edu.ve 	RESIDENCIAS EL ALTORAL 	1	ASOCIADO	JUBILADOS	2005-01-01	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1785	6039896	ALVAREZ  DE ORTEGA, LIGIA MERCEDES	Venezolano 	1960-09-24	\N	\N	Femenino 	Soltero 	\N	lmalvarez@ubv.edu.ve 	CARACAS 	0	ASISTENTE	JUBILADOS	2006-11-13	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1786	6090677	TROBO VIERA, JESÚS CALIXTO	Venezolano 	1935-02-28	\N	\N	Masculino 	Soltero 	\N	jtrobo@ubv.edu.ve 	CALLE CAURIMARE RES NELLO B PH 2 CANAS BELLO MTE 	0	ASISTENTE	JUBILADOS	2005-04-25	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1787	6095818	PEÑA MALDONADO, ALICE SOCORRO	Venezolano 	1961-09-28	\N	\N	Femenino 	Soltero 	\N	aspena@ubv.edu.ve 	A.V PANTEON ESQUINA PALO NEGRO RES OR PISO 6APT 61 SSAN JOSE CARACAS 	0	ASOCIADO	JUBILADOS	2004-09-24	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1788	6104120	GONCALVES SARDINHA, JOSÉ ANTONIO	Venezolano 	1949-07-24	\N	\N	Masculino 	Soltero 	\N	jgoncalves@ubv.edu.ve 	CONJUNTO RESIDENCIAL SAN ANTONIO DE LOS ALTOS, EDIF. EL DRAGO, APTO.4A-61. LA ROSALEDA SUR. 	2	AGREGADO	JUBILADOS	2004-01-01	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1789	6125397	DE FARIAS, NAIDI	Venezolano 	1963-09-09	\N	\N	Femenino 	Soltero 	\N	ndefarias@ubv.edu.ve 	KM.8 EL JUNQUITO 	0	INSTRUCTOR	PENSIONADOS	2005-03-14	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1790	6200790	PÉREZ DE ACOSTA, NURY DEL CARMEN	Venezolano 	1956-03-04	\N	\N	Femenino 	Casado 	\N	ndperezd@ubv.edu.ve 	1ERA CALLE DE LAS LUCES, N 8, EL CEMENTERIO 	0	AUXILIAR I	JUBILADOS	2007-09-17	DOCENTE TIEMPO COMPLETO	PASIVO
1791	6433331	DA SILVA ESPINOLA, MARGARITA	Venezolano 	1963-03-04	\N	\N	Femenino 	Soltero 	\N	mdasilva@ubv.edu.ve 	CTRO. RES. LA CALIFORNIA NORTE. EDIF. 5 APTO 165.AV. FCO, DE MIRANDA 	0	ASISTENTE	JUBILADOS	2006-01-09	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1792	7174437	SOLORZANO CONTRERAS, RAQUEL YOLANDA	Venezolano 	1963-03-07	\N	\N	Femenino 	Soltero 	\N	rsolorzano@ubv.edu.ve 	URB SIMON RODRIGUEZ EDIF 2 ESCALERA D PISO 1 APTO 16 CARACAS 	1	ASISTENTE	JUBILADOS	2004-08-29	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1793	7245085	CORTES ARCINIEGAS, ADRIANA	Venezolano 	1967-03-06	\N	\N	Femenino 	Soltero 	\N	adcortes@ubv.edu.ve 	AV. FUERZAS AEREAS CONJ. RES GUAICAMACUTO TORRE M APTO-MPB-4 	2	ASISTENTE	JUBILADOS	2006-01-15	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1794	7489414	GOMEZ, FRANCISCO ANTONIO	Venezolano 	1959-04-16	\N	\N	Masculino 	Soltero 	\N	fagomez@ubv.edu.ve 	CARACAS 	0	INSTRUCTOR	JUBILADOS	2008-10-01	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1795	7974150	MORENO LICONES, FANNY BERCELIA	Venezolano 	1966-05-19	\N	\N	Femenino 	Soltero 	\N	fbmoreno@ubv.edu.ve 	AVENIDA PRINCIPAL DE UNARE II, BLOQUE 8, APARTAMENTO 00 - 01, PUERTO ORDAZ, ESTADO BOLIVAR 	0	INSTRUCTOR	PENSIONADOS	2006-05-02	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1796	8370595	VILLARROEL DE MISEL, MAGALYS JOSEFINA	Venezolano 	1961-04-06	\N	\N	Femenino 	Casado 	\N	mvillarroel@ubv.edu.ve 	BARIIO NEGRO PRIMERO, CALLE #2, CASA #19 	0	INSTRUCTOR	JUBILADOS	2007-10-01	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1797	8759202	ROJAS GUILLEN, AURA ELENA	Venezolano 	1967-11-07	\N	\N	Femenino 	Soltero 	\N	arojas@ubv.edu.ve 	ESQUINA DE TABLITA A SORDO RESIDENCIAS EL JARDIN DEL CENTRO PISO 2 APTO 2-B 	0	AGREGADO	JUBILADOS	2004-09-13	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1798	9090118	COLINA NOGUERA, FELICIA NICOLASA	Venezolano 	1961-05-11	\N	\N	Femenino 	Soltero 	\N	fcolina@ubv.edu.ve 	URB EL PAUJI SECTOR 2 CALLE 11 N58 	0	INSTRUCTOR	JUBILADOS	2012-09-16	DOCENTE TIEMPO COMPLETO	PASIVO
1799	9619415	BARRIOS URBINA, KATERINA	Venezolano 	1969-11-28	\N	\N	Femenino 	Soltero 	\N	kbarrios@ubv.edu.ve 	RES. 19 DE ABRIL TORRE A APTO 603 A-EL VALLE CARACAS 	0	ASOCIADO	JUBILADOS	2004-09-06	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1800	9879357	FIGUEROA PORTELA, SANDRA SORAYA	Venezolano 	1967-07-13	\N	\N	Femenino 	Soltero 	\N	sfigueroa@ubv.edu.ve 	CARACAS 	1	INSTRUCTOR	JUBILADOS	2010-09-16	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1801	12602903	ESPARRAGOZA RIVERO, MILDRED SCARLET	Venezolano 	1976-04-08	\N	\N	Femenino 	Soltero 	\N	mesparragoza@ubv.edu.ve 	RIVERAS DEL ORINOCO MANZANO 16 	1	INSTRUCTOR	PENSIONADOS	2007-01-01	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1802	12625691	TABBAKH DE CASTILLO, MAHA	Venezolano 	1954-07-01	\N	\N	Femenino 	Soltero 	\N	mtabbakh@ubv.edu.ve 	COLINAS DE BELLO MONTE CALLE NEWTON EDF.SAN PEDRO 	0	INSTRUCTOR	JUBILADOS	2008-02-18	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1803	12731629	RODRÍGUEZ MARTINEZ, DILIANA NOHEMI	Venezolano 	1977-09-12	\N	\N	Femenino 	Soltero 	\N	drodriguez@ubv.edu.ve 	AV ANTONIO BERTONELLI CISNEROS ,SECTOR CAMATUAGA N 7 	0	AGREGADO	PENSIONADOS	2003-08-04	DOCENTE DEDICACION EXCLUSIVA	PASIVO
1804	81637182	PIRES DA COSTA, AUREA	Extranjero 	1947-04-03	\N	\N	Femenino 	Soltero 	\N	apires@ubv.edu.ve 	CALLE TUMEREMO N 16 URB SIMON BOLIVAR LAS MOREAS 	0	AUXILIAR I	JUBILADOS	2007-11-01	DOCENTE TIEMPO COMPLETO	PASIVO
1805	11920478	INGRID, RODRIGUEZ	Venezolano	1983-02-08	Venezuela	Miranda	Femenino	Soltero	4167021575	irodriguez@ubv.edu.ve	CUALQUIER COSA AVENIDA CENTRAL	0	AUXILIAR I	CONTRATADO	2016-11-16	DOCENTE TIEMPO COMPLETO	ACTIVO
1806	21130409	EMPERATRIZ CAROLINA, FRANKIS ESCALONA	Venezolano	1992-12-08	Venezuela	Lara	Femenino	Soltero	4143654896	empe@gmail.com	PALO VERDE AV CENTRAL EDIF EL REMANSO	1	INSTRUCTOR	DOCENTE	2017-11-06	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1809	14809099	MARIA AUXILIADORA, ESCALONA RAMOS	Venezolano	1974-09-14	Venezuela	Lara	Femenino	Soltero	4264156744	maria750@gmail.com	PALO VERDE	2	ASISTENTE	DOCENTE	2006-07-23	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1810	27083961	NINO MARIANO, PALACIOS RINCONES	Venezolano	1994-03-16	Venezuela	Miranda	Masculino	Soltero	4261112559	ninomariano.5@gmail.com	LA BOMBILLA	0	ASISTENTE	DOCENTE	2006-04-21	DOCENTE TIEMPO COMPLETO	ACTIVO
1811	24757961	YOSMERY ARELIS, SANTANA DELGADO	Venezolano	1994-06-20	Venezuela	Miranda	Femenino	Soltero	4261153497	yosmerylml@gmail.com	EL GUARATARO 	0	AGREGADO	DOCENTE	2006-08-24	DOCENTE TIEMPO COMPLETO	ACTIVO
1808	11670992	CARLOS AUGUSTO, FRANKIS MUÑOZ	Venezolano	1974-09-05	Venezuela	Miranda	Femenino	Soltero	4163064462	carlos.74.frankis@gmail.com	PALO VERDE 	2	AUXILIAR I	DOCENTE	2006-06-07	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1813	19958858	ANGEL JAVIER, TORREALBA	Venezolano	0180-04-06	Venezuela	Miranda	Masculino	Soltero	4265378964	anjel_javier_18@hotmail.com	santa teresa	1	INSTRUCTOR	DOCENTE	2010-03-09	DOCENTE TIEMPO COMPLETO	ACTIVO
1814	12345678	Admin, Admin	Venezolano	1998-02-16	Venezuela	Miranda	Masculino	Soltero	4143356987	admin@gmail.com	ADMIN ADMIN	0	AGREGADO	DOCENTE	1999-03-06	DOCENTE TIEMPO COMPLETO	ACTIVO
4	1564690	RUFO PAYEMA, PRUDENCIO	Venezolano 	1949-04-28	Colombia	\N	Masculino 	Soltero 	\N	prufo@ubv.edu.ve 	SECTRO ARAMARE PUERTO AYACUCHO ESTADO AMAZONAS 	0	INSTRUCTOR	DOCENTE	2007-03-01	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
1815	25847860	RUEDA TUTILLO, MARTIN SEGUNDO	Venezolano	1995-08-30	Venezuela	Miranda	Masculino	Soltero	4142569634	mruedaiunp@gmail.com	PROPATRIA LOS MAGALLANES	0	AGREGADO	CONTRATADO	2019-03-11	DOCENTE DEDICACION EXCLUSIVA	ACTIVO
\.


--
-- Name: nomina_docente_sigad_id_nomina_docente_sigad_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.nomina_docente_sigad_id_nomina_docente_sigad_seq', 1815, true);


--
-- Data for Name: noticia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.noticia (id_noticia, contenido, nom_arch, titulo, fecha) FROM stdin;
\.


--
-- Name: noticia_id_noticia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.noticia_id_noticia_seq', 1, false);


--
-- Data for Name: nucleo_academico; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.nucleo_academico (id_nucleo_academico, descripcion, id_centro_estudio) FROM stdin;
1	nucleo academico 1	1
\.


--
-- Name: nucleo_academico_id_nucleo_academico_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.nucleo_academico_id_nucleo_academico_seq', 1, true);


--
-- Data for Name: oferta_academica; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.oferta_academica (id_oferta_academica, estatus, id_periodo_academico, id_aldea_malla_curricula, descripcion) FROM stdin;
\.


--
-- Data for Name: oferta_academica_detalle; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.oferta_academica_detalle (id_oferta_academica_detalle, id_oferta_academica, id_malla_curricular_detalle, id_seccion) FROM stdin;
\.


--
-- Name: oferta_academica_detalle_id_oferta_academica_detalle_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.oferta_academica_detalle_id_oferta_academica_detalle_seq', 1, false);


--
-- Name: oferta_academica_id_oferta_academica_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.oferta_academica_id_oferta_academica_seq', 1, false);


--
-- Data for Name: pais; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pais (id_pais, descripcion, codigo, estatus) FROM stdin;
54	Cuba	CUB	true
55	Dinamarca	DNK	true
56	Dominica	DMA	true
57	Ecuador	ECU	true
58	Egipto	EGY	true
59	El Salvador	SLV	true
60	Emiratos Árabes Unidos	ARE	true
61	Eritrea	ERI	true
62	Eslovaquia	SVK	true
63	Eslovenia	SVN	true
64	España	ESP	true
65	Estados Unidos	USA	true
66	Estonia	EST	true
67	Etiopía	ETH	true
68	Filipinas	PHL	true
69	Finlandia	FIN	true
70	Fiyi	FJI	true
71	Francia	FRA	true
72	Gabón	GAB	true
73	Gambia	GAB	true
74	Georgia	GEO	true
75	Ghana	GHA	true
76	Gibraltar	GIB	true
77	Granada	GRD	true
78	Grecia	GRC	true
79	Groenlandia	GRL	true
80	Guadalupe	GPL	true
81	Guam	GUM	true
82	Guatemala	GTM	true
83	Guyana Francesa	GUF	true
84	Guernsey	GGY	true
85	Guinea	GIN	true
86	Guinea Ecuatorial	GNQ	true
87	Guinea-Bissau	GNB	true
88	Guyana	GUY	true
89	Haití	HTI	true
90	Honduras	HND	true
91	Hong kong	HKG	true
92	Hungía	HUN	true
93	India	IND	true
94	Indonesia	IDN	true
95	Irán	IRN	true
96	Irak	IRQ	true
97	Irlanda	IRL	true
98	Isla Bouvet	BVT	true
99	Isla de Man	IMN	true
100	Isla de Navidad	CXR	true
101	Isla Norfolk	NFK	true
102	Islandia	ISL	true
103	Islas Bermudas	BMU	true
104	Islas Caimán	CYM	true
105	Islas Cocos (Keeling)	CCK	true
106	Islas  Cook	COK	true
107	Islas  Cook	COK	true
108	Islas  Cook	COK	true
109	Islas de Alan	ALA	true
110	Islas  Feroe	FRO	true
111	Islas  Georgias del Sur y Sandwich del Sur	SGS	true
112	Islas Heard y McDonald	HMD	true
113	Islas Maldivas	MDV	true
114	Islas Malvinas	FLK	true
115	Islas Marianas del Norte	MNP	true
116	Islas Marshall	MHL	true
117	Islas Pitcairn	PCN	true
118	Islas Salomón	SLB	true
119	Islas Turcas y Caicos	TCA	true
120	Islas Ultramarinas Menores de Estados Unidos	UMI	true
121	Islas Vírgenes Brítanicas	VG	true
122	Islas Vírgenes de los Estados Unidos	VIR	true
123	Israel	ISR	true
124	Italia	ITA	true
125	Jamaica	JAM	true
126	Japón	JPN	true
127	Jersey	JEY	true
128	Jordania	JOR	true
129	Kazajistán	KAZ	true
130	Kenia	KEN	true
131	Kirgizstán	KGZ	true
132	Kiribati	KIR	true
133	Kuwait	KWT	true
134	Líbano	LBN	true
135	Laos	LAO	true
136	Lesoto	LSO	true
137	Letonia	LVA	true
138	Liberia	LBR	true
139	Libia	LBY	true
140	Liechtenstein	LIE	true
141	Lituania	LTU	true
142	Luxemburgo	LUX	true
143	México	MEX	true
144	Mónaco	MCO	true
145	Macedônia	MKD	true
146	Madagascar	MDG	true
147	Malasia	MYS	true
148	Malawi	MWI	true
149	Mali	MLI	true
150	Malta}	MLT	true
151	Marruecos	MAR	true
152	Martinica	MTQ	true
153	Mauricio	MUS	true
154	Mauritania	MRT	true
155	Mayotte	MYT	true
156	Micronesia	FSM	true
157	Moldavia	MDA	true
158	Mongolia	MNG	true
159	Montenegro	MNE	true
160	Montserrat	MSR	true
161	Mozambique	MOZ	true
162	Namibia	NAM	true
163	Nauru	Nauru	true
164	Nepal	NPL	true
165	Nicaragua	NIC	true
166	Niger	NER	true
167	Nigeria	NGA	true
168	Niue	NIU	true
169	Noruega	NOR	true
170	Nueva Caledonia	NCL	true
171	Nueva Zelanda	NZL	true
172	Omán	OMN	true
173	Paises Bajos	NLD	true
174	Pakistán	PAK	true
175	Palau	PLW	true
176	Palestina	PSE	true
177	Panamá	PAN	true
178	Papúa Nueva Guinea	PNG	true
179	Paraguay	PRY	true
180	Perú	PER	true
181	Polinesia Francesa	PYF	true
182	Polonia	POL	true
183	Portugal	PRT	true
184	Puerto Rico	MYS	true
185	Qatar	QAT	true
186	Reino Unido	GBR	true
187	República Centroafricana	CAF	true
188	República Checa	CZE	true
189	República Dominicana	DOM	true
190	Reunión	REU	true
191	Ruanda	RWA	true
192	Rumanía	ROU	true
193	Rusia	RUS	true
194	Sahara Occidental	ESH	true
195	Samoa	WSM	true
196	Samoa Americana	ASM	true
197	San Bartolomé	BLM	true
198	San Cristóbal y Nieves	KNA	true
199	San Marino	SMR	true
200	San Martín (Francia)	MAF	true
201	San Pedro y Miquelón	SPM	true
202	San Vicente Y las Granadinas	VCT	true
203	Santa Elena	SHN	true
204	Santa Lucía	LCA	true
2	Afganistán	AFG	true
3	Albania	ALB	true
4	Alemania	ALM	true
5	Algeria	DZA	true
6	Andorra	ADN	true
7	Angola	ADG	true
8	Anguila	ADG	true
9	Antártida	ANT	true
10	Antigua y Barbuda	ATG	true
11	Antillas Neerlandesas	ANT	true
12	Arabia Saudita	SAU	true
13	Argentina	ARG	true
14	Armenia	ARM	true
15	Aruba	ABW	true
16	Austria	AUS	true
17	Azerbayán	AZE	true
18	Bélgica	BEL	true
19	Bahamas	BHS	true
20	Bahrein	BHR	true
21	Bangladesh	BGD	true
22	Barbados	BRB	true
23	Belice	BLZ	true
24	Benín	BEN	true
25	Bhután	BTN	true
26	Bielorrusia	BLR	true
27	Birmania	MMR	true
28	Bolivia	BOL	true
29	Bosnia y Herzegovina	BIH	true
30	Botsuana	BWA	true
31	Brasil	BRA	true
32	Brunéi	BRN	true
33	Bulgaria	BGR	true
34	Burkina Faso	BFA	true
35	Burundi	BDI	true
36	Cavo Verde	CPV	true
37	Camboya	KHM	true
38	Camerún	CMR	true
39	Canadá	CAN	true
40	Chad	TCD	true
41	Chile	CHL	true
42	China	CHN	true
43	Chipre	CYP	true
44	Ciudad del Vaticano	VAT	true
45	Colombia	COL	true
46	Comoras	COM	true
47	Arabia Saudita	SAU	true
48	Congo	COG	true
49	Corea del Norte	PRK	true
50	Corea del Sur	KOR	true
51	Costa de Marfil	CIV	true
52	Costa Rica	CRI	true
53	Croacia	HRV	true
205	Santo Tomé y Príncipe	STP	true
206	Senegal	SEN	true
207	Serbia	SBR	true
208	Seychelles	SYC	true
209	Sierra Leona	SLE	true
210	Singapur	SGP	true
211	Siria	SYR	true
212	Somalia	SOM	true
213	Sri lanka	LKA	true
214	Sudáfrica	ZAF	true
215	Sudán	SDN	true
216	Suecia	SWE	true
218	Surínam	SUR	true
219	Svalbard y  Jan Mayen	SJM	true
220	Swazilandia	SWZ	true
221	Swazilandia	SWZ	true
222	Tadjikistán	TKJ	true
223	Tailandia	THA	true
224	Taiwán	TWN	true
225	Tanzania	TZA	true
226	Territorio Británico del Océano Índico	IOT	true
227	Territorios Australes y Antárticas Franceses	ATF	true
228	Timor Oriental	TLS	true
229	Togo	TGO	true
230	Tokelau	TKL	true
231	Tonga	TON	true
232	Trinidad y Tobago	TTO	true
233	Tunez	TUN	true
234	Turkmenistán	TKM	true
235	Turquía	TUR	true
236	Tuvalu	TUV	true
237	Ucrania	UKR	true
238	Uganda	UGA	true
239	Uruguay	URY	true
240	Uzbekistán	UZB	true
241	Vanuatu	VUT	true
242	Venezuela	VEN	true
243	Vietnam	VNM	true
244	Wallis y Futuna	WLF	true
245	Yemen	YEM	true
246	Yibuti	DJI	true
247	Zambia	ZMB	true
248	Zimbabue	ZWE	true
217	Suiza	CHE	true
\.


--
-- Name: pais_id_pais_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pais_id_pais_seq', 248, true);


--
-- Data for Name: parroquia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.parroquia (id_parroquia, descripcion, codigo, id_municipio, id_eje_municipal) FROM stdin;
1	Altagracia	010101	1	7
2	Antimano	010102	1	7
3	Candelaria	010103	1	7
4	Caricuao	010104	1	1
5	Catedral	010105	1	7
6	Coche	010106	1	7
7	El Junquito	010107	1	1
8	El Paraíso	010108	1	7
9	El Recreo	010109	1	7
10	El Valle	010110	1	7
11	La Pastora	010111	1	7
12	La Vega	010112	1	7
13	Macarao	010113	1	1
14	san Agustín	010114	1	7
15	San Bernardino	010115	1	7
16	San José	010116	1	7
17	San Juan	010117	1	7
18	San Pedro	010118	1	2
19	Santa Rosalía	010119	1	3
20	Santa Teresa	010120	1	7
21	Sucre	010121	1	7
22	23 de Enero	010122	1	7
23	Parroquia Huachamacare	020101	2	7
24	Parroquia Marawaka	020102	2	7
25	Parroquia Mavaca	020103	2	7
26	Parroquia Sierra Parima	020104	2	7
27	Parroquia Ucat	020201	3	7
28	Parroquia Yapacana	020202	3	7
29	Parroquia Caname	020203	3	7
30	Parroquia Fernando Girón Tovar	020301	4	28
31	Parroquia Luis Alberto Gómez	020302	4	28
32	Parroquia Parhueña	020303	4	28
33	Parroquia Platanillal	020304	4	28
34	Parroquia Samariapo	020401	5	28
35	Parroquia Sipapo	020402	5	28
36	Parroquia Munduapo	020403	5	28
37	Parroquia Guayapo	020404	5	28
38	Parroquia Victorino	020501	6	30
39	Parroquia Comunidad	020502	6	30
40	Parroquia Alto Ventuari	020601	7	30
41	Parroquia Medio Ventuari	020602	7	30
42	Parroquia Bajo Ventuari	020603	7	30
43	Parroquia Solano	020701	8	30
44	Parroquia Casiquiare	020702	8	30
45	Parroquia Cocuy	020703	8	30
46	Parroquia Capital Anaco 1	030101	9	38
47	Parroquia San Joaquín	030102	9	38
48	Parroquia Buena Vista	030103	9	38
49	Parroquia Capital Aragua	030201	10	38
50	Parroquia Cachipo	030202	10	38
51	Parroquia Capital Fernando de Peñalver	030301	11	38
52	Parroquia San Miguel	030302	11	38
53	Parroquia Sucre	030303	11	38
54	Parroquia Capital Francisco del Carmen Carvajal	030401	12	38
55	Parroquia Santa Bárbara	030402	12	38
56	Parroquia Capital Guanta	030601	14	38
57	Parroquia Chorrerón	030602	14	38
58	Parroquia Capital Puerto La Cruz	030801	16	38
59	Parroquia Pozuelos	030802	16	38
60	Parroquia Capital Juan Manuel Cajigal	03090	17	38
61	Parroquia San Pablo	030902	17	38
62	Parroquia Capital Libertad	031101	19	38
63	Parroquia El Carito	031102	19	38
64	Parroquia Santa Inés	031103	19	38
65	Parroquia Capital Manuel Ezequiel Bruzual	03120	20	38
66	Parroquia Guanape	031202	20	38
67	Parroquia Sabana de Uchire	031203	20	38
68	Parroquia Capital Pedro María Fréites	031301	21	38
69	Parroquia Libertador	03130	21	38
70	Parroquia Santa Rosa	031303	21	38
71	Parroquia Urica	031304	21	38
72	Parroquia Capital Píritu	031401	22	38
73	Parroquia San Francisco	031402	22	38
74	Parroquia Capital San Juan de Capistrano	031601	24	38
75	Parroquia Boca de Chávez	031602	24	38
76	Parroquia Capital Santa Ana	031701	25	38
77	Parroquia Pueblo Nuevo	031702	25	38
78	Parroquia El Carmen	031801	26	38
79	Parroquia San Cristóbal	031802	26	38
80	Parroquia Bergantín	031803	26	38
81	Parroquia Caigua	031804	26	38
82	Parroquia El Pilar	031805	26	38
83	Parroquia Naricual	031806	26	38
84	Parroquia Capital Sir Arthur Mc Gregor	032001	28	38
85	Parroquia Tomás Alfaro Calatrava	032002	28	38
86	Parroquia Capital Diego Bautista Urbaneja	032101	29	38
87	Parroquia El Morro	032102	29	38
88	Parroquia Capital Francisco de Miranda	030501	13	32
89	Parroquia Atapirire	030502	13	32
90	Parroquia Boca del Pao	030503	13	32
91	Parroquia El Pao	030504	13	32
92	Parroquia Múcura	030505	13	32
93	Parroquia Capital Independencia	030701	15	32
94	Parroquia Mamo	030702	15	32
95	Parroquia Capital José Gregorio Monagas	031001	18	38
96	Parroquia Piar	031002	18	38
97	Parroquia San Diego de Cabrutica	031003	18	38
98	Parroquia Santa Clara	031004	18	38
99	Parroquia Uverito	031005	18	38
100	Parroquia Zuata	03100	18	38
101	Parroquia Urbana Guasdualito	040401	33	23
102	Parroquia Aramendi	040402	33	23
103	Parroquia El Amparo	040403	33	23
104	Parroquia San Camilo	040404	33	23
105	Parroquia Urdaneta	040405	33	23
106	Parroquia Urbana Achaguas	040101	30	29
107	Parroquia Apurito	040102	30	29
108	Parroquia El Yagual	040103	30	29
109	Parroquia Guachara	040104	30	29
110	Parroquia Mucuritas	040105	30	29
111	Parroquia Queseras del Medio	040106	30	29
112	Parroquia Urbana Bruzual	040301	32	29
113	Parroquia Mantecal	040302	32	29
114	Parroquia Quintero	040303	32	29
115	Parroquia Rincón Hondo	040304	32	29
116	Parroquia San Vicente	040305	32	29
117	Parroquia Urbana Elorza	040601	35	29
118	Parroquia La Trinidad	040602	35	29
119	Parroquia Urbana Biruaca	040201	31	31
120	Parroquia Urbana San Juan de Payara	040501	34	31
121	Parroquia Codazzi	040502	34	31
122	Parroquia Cunaviche	040503	34	31
123	Parroquia Urbana San Fernando	040701	36	31
124	Parroquia El Recreo	040702	36	31
125	Parroquia Peñalver	040703	36	31
126	Parroquia San Rafael de Atamaica	040704	36	31
127	Parroquia Camatagua	050201	37	5
128	Parroquia No Urbana Carmen de Cura	050202	37	5
129	Parroquia San Casimiro	050901	44	5
130	Parroquia No Urbana Güiripa	050902	44	5
131	Parroquia No Urbana Ollas de Caramacate	050903	44	5
132	Parroquia No Urbana Valle Morín	050904	44	5
133	Parroquia Santos Michelena	051201	47	5
134	Parroquia No Urbana Tiara	051202	47	5
135	Parroquia No Urbana Choroní	050302	38	9
136	Parroquia Urbana Las Delicias	050303	38	9
137	Parroquia Urbana Madre María de San José	050304	38	9
138	Parroquia Urbana Joaquín Crespo 	050305	38	9
139	Parroquia Urbana Pedro José Ovalles	050306	38	9
140	Parroquia Urbana José Casanova Godoy	050307	38	9
141	Parroquia Urbana Andrés Eloy Blanco	050308	38	9
142	Parroquia Urbana Los Tacariguas	050309	38	9
143	Parroquia Urbana Juan Vicente Bolívar y Ponte	050501	40	9
144	Parroquia Urbana Castor Nieves Ríos	050502	40	9
145	Parroquia No Urbana Las Guacamayas	050503	40	9
146	Parroquia No Urbana Pao de Zárate	050504	40	9
147	Parroquia No Urbana Zuata	050505	40	9
148	Parroquia Libertador	050701	42	9
149	Parroquia No Urbana San Martín de Porres	050702	42	9
150	Parroquia Mario Briceño Iragorry	050801	43	9
151	Parroquia Urbana Caña de Azúcar	050802	43	9
152	Parroquia Santiago Mariño	051101	46	9
153	Parroquia No Urbana Arévalo Aponte	051102	46	9
154	Parroquia No Urbana Chuao	051103	46	9
155	Parroquia No Urbana Samán de Güere	051104	46	9
156	Parroquia No Urbana Alfredo Pacheco Miranda	051105	46	9
157	Parroquia Sucre	051301	48	9
158	Parroquia No Urbana Bella Vista	051302	48	9
159	Parroquia Zamora	051601	51	9
160	Parroquia No Urbana Magdaleno	051602	51	9
161	Parroquia No Urbana San Francisco de Asís	051603	51	9
162	Parroquia No Urbana Valles de Tucutunemo	051604	51	9
163	Parroquia No Urbana Augusto Mijares	051605	51	9
164	Parroquia Francisco Linares Alcántara	051701	52	9
165	Parroquia No Urbana Francisco de Miranda	051702	52	9
166	Parroquia No Urbana Monseñor Feliciano González	051703	52	9
167	Parroquia Urdaneta	051501	50	10
168	Parroquia No Urbana Las Peñitas	051502	50	10
169	Parroquia No Urbana San Francisco de Cara	051503	50	10
170	Parroquia No Urbana Taguay	051504	50	10
171	Parroquia Santa Bárbara	060701	60	23
172	Parroquia José Ignacio del Pumar	060702	60	23
173	Parroquia Pedro Briceño Méndez	060703	60	23
174	Parroquia Ramón Ignacio Méndez	060704	60	23
175	Parroquia El Cantón	061201	65	23
176	Parroquia Santa Cruz de Guacas	061202	65	23
177	Parroquia Puerto Vivas	061203	65	23
178	Parroquia Sabaneta	060101	54	26
179	Parroquia Rodríguez Domínguez	060102	54	26
180	Parroquia Ticoporo	060201	55	26
181	Parroquia Andrés Bello	060202	55	26
182	Parroquia Nicolás Pulido	060203	55	26
183	Parroquia Arismendi	060301	56	26
184	Parroquia Guadarrama	060302	56	26
185	Parroquia La Unión	060303	56	26
186	Parroquia San Antonio	060304	56	26
187	Parroquia Barinas	060401	57	26
188	Parroquia Alfredo Arvelo Larriva	060402	57	26
189	Parroquia San Silvestre	060403	57	26
190	Parroquia Santa Inés	060404	57	26
191	Parroquia Santa Lucía	060405	57	26
192	Parroquia Torunos	060406	57	26
193	Parroquia El Carmen	060407	57	26
194	Parroquia Don Rómulo Betancourt	060408	57	26
195	Parroquia Corazón de Jesús	060409	57	26
196	Parroquia Ramón Ignacio Méndez	060410	57	26
197	Parroquia Alto Barinas	060411	57	26
198	Parroquia Manuel Palacio Fajardo	060412	57	26
199	Parroquia Juan Antonio Rodríguez Domínguez	060413	57	26
200	Parroquia Dominga Ortiz de Páez	060414	57	26
201	Parroquia Barrancas	060601	59	26
202	Parroquia El Socorro	060602	59	26
203	Parroquia Masparrito	060603	59	26
204	Parroquia Obispos	060801	61	26
205	Parroquia El Real	060802	61	26
206	Parroquia La Luz	060803	61	26
207	Parroquia Los Guasimitos	060804	61	26
208	Parroquia Ciudad Bolivia	060901	62	26
209	Parroquia Ignacio Briceño	060902	62	26
210	Parroquia José Félix Ribas	060903	62	26
211	Parroquia Páez	060904	62	26
212	Parroquia Libertad	061001	63	26
213	Parroquia Dolores	061002	63	26
214	Parroquia Palacios Fajardo	061003	63	26
215	Parroquia Santa Rosa	061004	63	26
216	Parroquia Simón Rodríguez	061005	63	26
217	Parroquia Ciudad de Nutrias	061101	64	26
218	Parroquia El Regalo	061102	64	26
219	Parroquia Puerto de Nutrias	061103	64	26
220	Parroquia Santa Catalina	061104	64	26
221	Parroquia Simón Bolívar	061105	64	26
222	Parroquia Cachamay	070101	66	34
223	Parroquia Chirica	070102	66	34
224	Parroquia Dalla Costa	070103	66	34
225	Parroquia Once de Abril	070104	66	34
226	Parroquia Simón Bolívar	070105	66	34
227	Parroquia Unare	070106	66	34
228	Parroquia Universidad	070107	66	34
229	Parroquia Vista al Sol	070108	66	34
230	Parroquia Pozo Verde	070109	66	34
231	Parroquia Yocoima	070110	66	34
232	Parroquia Cinco de Julio	070111	66	34
233	Sección Capital Cedeño	070201	67	28
234	Parroquia Altagracia	070202	67	28
235	Parroquia Ascensión Farreras	070203	67	28
236	Parroquia Guaniamo	070204	67	28
237	Parroquia La Urbana	070205	67	28
238	Parroquia Pijiguaos	070206	67	28
239	Sección Capital Gran Sabana	070401	67	33
240	Parroquia Ikabarú	070402	67	33
241	Parroquia Agua Salada	070501	70	35
242	Parroquia Catedral	070502	70	35
243	Parroquia José Antonio Páez	070503	70	35
244	Parroquia La Sabanita	070504	70	35
245	Parroquia Marhuanta	070505	70	35
246	Parroquia Vista Hermosa	070506	70	35
247	Parroquia Orinoco	070507	70	35
248	Parroquia Panapana	070508	70	35
249	Parroquia Zea	070509	70	35
250	Sección Capital Angostura	070701	72	35
251	Parroquia Barceloneta	070702	72	35
252	Parroquia San Francisco	070703	72	35
253	Parroquia Santa Bárbara	070704	72	35
254	Sección Capital Sucre	071001	75	35
255	Parroquia Aripao	071002	75	35
256	Parroquia Guarataro	071003	75	35
257	Parroquia Las Majadas	071004	75	35
258	Parroquia Moitaco	071005	75	35
259	Sección Capital Piar	070601	71	36
260	Parroquia Andrés Eloy Blanco	070602	71	36
261	Parroquia Pedro Cova	070603	71	36
262	Parroquia Urbana Bejuma	080101	77	8
263	Parroquia No Urbana Canoabo	080102	77	8
264	Parroquia No Urbana Simón Bolívar	080103	77	8
265	Parroquia Urbana Güigüe	080201	78	8
266	Parroquia No Urbana Belén	080202	78	8
267	Parroquia No Urbana Tacarigua	080203	78	8
268	Parroquia Urbana Aguas Calientes	080301	79	8
269	Parroquia Urbana Mariara	080302	79	8
270	Parroquia Urbana Ciudad Alianza	080401	80	8
271	Parroquia Urbana Guacara	080402	80	8
272	Parroquia No Urbana Yagua	080403	80	8
273	Parroquia Urbana Morón	080501	81	8
274	Parroquia No Urbana Urama	080502	81	8
275	Parroquia Urbana Tocuyito	080601	82	8
276	Parroquia Urbana Independencia	080602	82	8
277	Parroquia Urbana Los Guayos	080701	83	8
278	Parroquia Urbana Miranda	080801	84	8
279	Parroquia Urbana Montalbán	080901	85	8
280	Parroquia Urbana Naguanagua	081001	86	8
281	Parroquia Urbana San Diego	081201	87	8
282	Parroquia Urbana San Joaquín	081301	88	8
283	Parroquia Urbana Candelaria	081401	89	8
284	Parroquia Urbana Catedral	081402	89	8
285	Parroquia Urbana El Socorro	081403	89	8
286	Parroquia Urbana Miguel Peña	081404	89	8
287	Parroquia Urbana Rafael Urdaneta	081405	89	8
288	Parroquia Urbana San Blas	081406	89	8
289	Parroquia Urbana San José	081407	89	8
290	Parroquia Urbana Santa Rosa	081408	89	8
291	Parroquia No Urbana Negro Primero	081409	89	8
292	Parroquia Cojedes	090101	90	25
293	Parroquia Juan de Mata Suárez	090102	90	25
294	Parroquia Tinaquillo	090201	91	25
295	Parroquia El Baúl	090301	92	25
296	Parroquia Sucre	090302	92	25
297	Parroquia Macapo	090401	93	25
298	Parroquia La Aguadita	090402	93	25
299	Parroquia El Pao	090501	94	25
300	Parroquia Libertad de Cojedes	090601	95	25
301	Parroquia El Amparo	090602	95	25
302	Parroquia Rómulo Gallegos	090701	96	25
303	Parroquia San Carlos de Austria	090801	97	25
304	Parroquia Juan Ángel Bravo	090802	97	25
305	Parroquia Manuel Manrique	090803	97	25
306	Parroquia General en Jefe José Laurencio Silva	090901	98	25
307	Parroquia Curiapo	100101	99	39
308	Parroquia Almirante Luis Brión	100102	99	39
309	Parroquia Francisco Aniceto Lugo	100103	99	39
310	Parroquia Manuel Renaud	100104	99	39
311	Parroquia Padre Barral	100105	99	39
312	Parroquia Santos de Abelgas	100106	99	39
313	Parroquia Pedernales	100301	101	39
314	Parroquia Luis Beltrán Prieto Figueroa	100302	101	39
315	Parroquia San José	100401	102	39
316	Parroquia José Vidal Marcano	100402	102	39
317	Parroquia Juan Millán	100403	102	39
318	Parroquia Leonardo Ruíz Pineda	100404	102	39
319	Parroquia Mariscal Antonio José de Sucre	100405	102	39
320	Parroquia Monseñor Argimiro García	100406	102	39
321	Parroquia San Rafael	100407	102	39
322	Parroquia Virgen del Valle	100408	102	39
323	Parroquia Imataca	100201	100	34
324	Parroquia Cinco de Julio	100202	100	34
325	Parroquia Juan Bautista Arismendi	100203	100	34
326	Parroquia Manuel Piar	100204	100	34
327	Parroquia Rómulo Gallegos	100205	100	34
328	Parroquia San Juan de los Cayos	110101	103	13
329	Parroquia Capadare	110102	103	13
330	Parroquia La Pastora	110103	103	13
331	Parroquia Libertador	110104	103	13
332	Parroquia San Luis	110201	104	13
333	Parroquia Aracua	110202	104	13
334	Parroquia La Peña	110203	104	13
335	Parroquia Churuguara	111001	112	13
336	Parroquia Agua Larga	111002	112	13
337	Parroquia El Paují	111003	112	13
338	Parroquia Independencia	111004	112	13
339	Parroquia Mapararí	111005	112	13
340	Parroquia Jacura	111101	113	13
341	Parroquia Agua Linda	111102	113	13
342	Parroquia Araurima	111103	113	13
343	Parroquia Chichiriviche	111501	117	13
344	Parroquia Boca de Tocuyo	111502	117	13
345	Parroquia Tocuyo de la Costa	111503	117	13
346	Parroquia Cabure	111701	119	13
347	Parroquia Colina	111702	119	13
348	Parroquia Curimagua	111703	119	13
349	Parroquia Tucacas	112001	122	13
350	Parroquia Boca de Aroa	112002	122	13
351	Parroquia Sucre	112101	123	13
352	Parroquia Pecaya	112102	123	13
353	Parroquia Santa Cruz de Bucaral	112301	125	13
354	Parroquia El Charal	112302	125	13
355	Parroquia Las Vegas del Tuy	112303	125	13
356	Parroquia Mene de Mauroa	111301	115	15
357	Parroquia Casigua	111302	115	15
358	Parroquia San Félix	111303	115	15
359	Parroquia Capatárida	110301	105	13
360	Parroquia Bariro	110302	105	13
361	Parroquia Borojó	110303	105	13
362	Parroquia Guajiro	110304	105	13
363	Parroquia Seque	110305	105	13
364	Parroquia Zazárida	110306	105	13
365	Parroquia Carirubana	110501	107	12
366	Parroquia Norte	110502	107	12
367	Parroquia Punta Cardón	110503	107	12
368	Parroquia Santa Ana	110504	107	12
369	Parroquia La Vela de Coro	110601	108	12
370	Parroquia Acurigua	110602	108	12
371	Parroquia Guaibacoa	110603	108	12
372	Parroquia Las Calderas	110604	108	12
373	Parroquia Macoruca	110605	108	12
374	Parroquia Pedregal	110801	110	12
375	Parroquia Agua Clara	110802	110	12
376	Parroquia Avaria	110803	110	12
377	Parroquia Piedra Grande	110804	110	12
378	Parroquia Purureche	110805	110	12
379	Parroquia Pueblo Nuevo	110901	111	12
380	Parroquia Adícora	110902	111	12
381	Parroquia Baraived	110903	111	12
382	Parroquia Buena Vista	110904	111	12
383	Parroquia Jadacaquiva	110905	111	12
384	Parroquia Moruy	110906	111	12
385	Parroquia Adaure	110907	111	12
386	Parroquia El Hato	110908	111	12
387	Parroquia El Vínculo	110909	111	12
388	Parroquia Los Taques	111201	114	12
389	Parroquia Judibana	111202	114	12
390	Parroquia San Antonio	111401	116	12
391	Parroquia San Gabriel	111402	116	12
392	Parroquia Santa Ana	111403	116	12
393	Parroquia Guzmán Guillermo	111404	116	12
394	Parroquia Mitare	111405	116	12
395	Parroquia Río Seco	111406	116	12
396	Parroquia Sabaneta	111407	116	12
397	Parroquia Urumaco	112401	126	12
398	Parroquia Bruzual	112402	126	12
399	Parroquia Puerto Cumarebo	112501	127	12
400	Parroquia La Ciénaga	112502	127	12
401	Parroquia La Soledad	112503	127	12
402	Parroquia Pueblo Cumarebo	112504	127	12
403	Parroquia Zazárida	112505	127	12
404	Parroquia Chaguaramas	120201	126	10
405	Parroquia El Socorro	120301	130	10
406	Parroquia Capital Valle de La Pascua	120501	132	10
407	Parroquia Espino	120502	132	10
408	Parroquia Capital Las Mercedes	120601	133	10
409	Parroquia Capital El Sombrero	120701	134	10
410	Parroquia Sosa	120702	134	10
411	Parroquia Capital Calabozo	120801	135	10
412	Parroquia El Calvario	120802	135	10
413	Parroquia El Rastro	120803	135	10
414	Parroquia Guardatinajas	120804	135	10
415	Parroquia Capital Altagracia de Orituco 	120901	136	10
416	Parroquia Lezama	120902	136	10
417	Parroquia Libertad de Orituco	120903	136	10
418	Parroquia Paso Real de Macaira	120904	136	10
419	Parroquia San Francisco de Macaira	120905	136	10
420	Parroquia San Rafael de Orituco	120906	136	10
421	Parroquia Soublette	120907	136	10
422	Parroquia Capital Ortiz	121001	137	10
423	Parroquia San Francisco de Tiznado	121002	137	10
424	Parroquia San José de Tiznado	121003	137	10
425	Parroquia San Lorenzo de Tiznado	121004	137	10
426	Parroquia Capital Tucupido	121101	138	10
427	Parroquia San Rafael de Laya	121102	138	10
428	Parroquia Capital San Juan de Los Morros	121201	139	10
429	Parroquia Cantagallo	121202	139	10
430	Parroquia Parapara	121203	139	10
431	Parroquia San José de Guaribe	121301	140	10
432	Parroquia Capital Santa María de Ipire	121401	141	10
433	Parroquia Altamira	121402	141	10
434	Parroquia Capital Zaraza	121501	142	10
435	Parroquia San José de Unare	121502	142	10
436	Parroquia Capital Camaguán	120101	128	31
655	Parroquia Boquerón	160803	203	37
437	Parroquia Puerto Miranda	120102	128	31
438	Parroquia Uverito	120103	128	31
439	Parroquia Capital San Gerónimo de Guayabal	120401	131	31
440	Parroquia Cazorla	120402	131	31
441	Parroquia Cabruta	120602	133	31
442	Parroquia Santa Rita de Manapire	120603	133	31
443	Parroquia Pío Tamayo	130101	143	11
444	Parroquia Quebrada Honda de Guache	130102	143	11
445	Parroquia Yacambú	130103	143	11
446	Parroquia Fréitez	130201	144	11
447	Parroquia José María Blanco	130202	144	11
448	Parroquia Catedral	130301	145	11
449	Parroquia Concepción	130302	145	11
450	Parroquia El Cují	130303	145	11
451	Parroquia Juan de Villegas	130304	145	11
452	Parroquia Santa Rosa	130305	145	11
453	Parroquia Tamaca	130306	145	11
454	Parroquia Unión	130307	145	11
455	Parroquia Aguedo Felipe Alvarado	130308	145	11
456	Parroquia Buena Vista	130309	145	11
457	Parroquia Juárez	130310	145	11
458	Parroquia Juan Bautista Rodríguez	130401	146	11
459	Parroquia Cuara	130402	146	11
460	Parroquia Diego de Lozada	130403	146	11
461	Parroquia Paraíso de San José	130404	146	11
462	Parroquia San Miguel	130405	146	11
463	Parroquia Tintorero	130406	146	11
464	Parroquia José Bernardo Dorante	130407	146	11
465	Parroquia Coronel Mariano Peraza	130408	146	11
466	Parroquia Bolívar	130501	147	11
467	Parroquia Anzoátegui	130502	147	11
468	Parroquia Guarico	130503	147	11
469	Parroquia Hilario Luna y Luna	130504	147	11
470	Parroquia Humocaro Alto	130505	147	11
471	Parroquia Humocaro Bajo	130506	147	11
472	Parroquia La Candelaria	130507	147	11
473	Parroquia Morán	130508	147	11
474	Parroquia Cabudare	130601	148	11
475	Parroquia José Gregorio Bastidas	130602	148	11
476	Parroquia Agua Viva	130603	148	11
477	Parroquia Sarare	130701	149	11
478	Parroquia Buría	130702	149	11
479	Parroquia Gustavo Vegas León	130703	149	11
480	Parroquia Trinidad Samuel	130801	150	11
481	Parroquia Antonio Díaz	130802	150	11
482	Parroquia Camacaro	130803	150	11
483	Parroquia Castañeda	130804	150	11
484	Parroquia Cecilio Zubillaga	130805	150	11
485	Parroquia Chiquinquirá	130806	150	11
486	Parroquia El Blanco	130807	150	11
487	Parroquia Espinoza de los Monteros	130808	150	11
488	Parroquia Lara	130809	150	11
489	Parroquia Las Mercedes	130810	150	11
490	Parroquia Manuel Morillo	130811	150	11
491	Parroquia Montaña Verde	130812	150	11
492	Parroquia Montes de Oca	130813	150	11
493	Parroquia Torres	130814	150	11
494	Parroquia Heriberto Arroyo	130815	150	11
495	Parroquia Reyes Vargas	130816	150	11
496	Parroquia Altagracia	130817	150	11
497	Parroquia Siquisique	130901	151	13
498	Parroquia Moroturo	130902	151	13
499	Parroquia San Miguel	130903	151	13
500	Parroquia Xaguas	130904	151	13
501	Parroquia Presidente Betancourt	140101	152	18
502	Parroquia Presidente Páez	140102	152	18
503	Parroquia Presidente Rómulo Gallegos	140103	152	18
504	Parroquia Gabriel Picón González	140104	152	18
505	Parroquia Héctor Amable Mora	140105	152	18
506	Parroquia José Nucete Sardi	140106	152	18
507	Parroquia Pulido Méndez	140107	152	18
508	Parroquia Capital Antonio Pinto Salinas	140301	154	18
509	Parroquia Mesa Bolívar	140302	154	18
510	Parroquia Mesa de Las Palmas	140303	154	18
511	Parroquia Capital Aricagua	140401	155	18
512	Parroquia San Antonio	140402	155	18
513	Parroquia Capital Arzobispo Chacón	140501	156	18
514	Parroquia Capurí	140502	156	18
515	Parroquia Chacantá	140503	156	18
516	Parroquia El Molino	140504	156	18
517	Parroquia Guaimaral	140505	156	18
518	Parroquia Mucutuy	140506	156	18
519	Parroquia Mucuchachí	140507	156	18
520	Parroquia Capital Guaraque	140901	160	18
521	Parroquia Mesa de Quintero	140902	160	18
522	Parroquia Río Negro	140903	160	18
523	Parroquia Capital Rivas Dávila	141801	169	18
524	Parroquia Gerónimo Maldonado	141802	169	18
525	Parroquia Capital Sucre	142001	171	18
526	Parroquia Chiguará	142002	171	18
527	Parroquia Estánques	142003	171	18
528	Parroquia La Trampa	142004	171	18
529	Parroquia Pueblo Nuevo del Sur	142005	171	18
530	Parroquia San Juan	142006	171	18
531	Parroquia El Amparo	142101	172	18
532	Parroquia El Llano	142102	172	18
533	Parroquia San Francisco	142103	172	18
534	Parroquia Tovar	142104	172	18
535	Parroquia Capital Zea	142301	174	18
536	Parroquia Caño El Tigre	142302	174	18
537	Parroquia Fernández Peña	140601	157	22
538	Parroquia Matriz	140602	157	22
539	Parroquia Montalbán	140603	157	22
540	Parroquia Acequias	140604	157	22
541	Parroquia Jají	140605	157	22
542	Parroquia La Mesa	140606	157	22
543	Parroquia San José del Sur	140607	157	22
544	Parroquia Capital Caracciolo Parra Olmedo	140701	158	22
545	Parroquia Florencio Ramírez	140702	158	22
546	Parroquia Capital Cardenal Quintero	140801	159	22
547	Parroquia Las Piedras	140802	159	22
548	Parroquia Capital Guaraque	140901	161	22
549	Parroquia Mesa de Quintero	140902	161	22
550	Parroquia Río Negro	140903	161	22
551	Parroquia Capital Justo Briceño	141101	162	22
552	Parroquia San Cristóbal de Torondoy	141102	162	22
553	Parroquia Antonio Spinetti Dini	141201	163	22
554	Parroquia Arias	141202	163	22
555	Parroquia Caracciolo Parra Pérez	141203	163	22
556	Parroquia Domingo Peña	141204	163	22
557	Parroquia El Llano	141205	163	22
558	Parroquia Gonzalo Picón Febres	141206	163	22
559	Parroquia Jacinto Plaza	141207	163	22
560	Parroquia Juan Rodríguez Suárez	141208	163	22
561	Parroquia Lasso de la Vega	141209	163	22
562	Parroquia Mariano Picón Salas	141210	163	22
563	Parroquia Milla	141211	163	22
564	Parroquia Osuna Rodríguez	141212	163	22
565	Parroquia Sagrario	141213	163	22
566	Parroquia El Morro	141214	163	22
567	Parroquia Los Nevados	141215	163	22
568	Parroquia Capital Miranda	141301	164	22
569	Parroquia Andrés Eloy Blanco	141302	164	22
570	Parroquia La Venta	141303	164	22
571	Parroquia Piñango	141304	164	22
572	Parroquia Capital Obispo Ramos de Lora	141401	165	22
573	Parroquia Eloy Paredes	141402	165	22
574	Parroquia San Rafael de Alcázar	141403	165	22
575	Parroquia Capital Rangel	141701	168	22
576	Parroquia Cacute	141702	168	22
577	Parroquia La Toma	141703	168	22
578	Parroquia Mucurubá	141704	168	22
579	Parroquia San Rafael	141705	168	22
580	Parroquia Capital Tulio Febres Cordero	142201	173	22
581	Parroquia Independencia	142202	173	22
582	Parroquia María de la Concepción Palacios Blanco	142203	173	22
583	Parroquia Santa Apolonia	142204	173	22
584	Parroquia Caucagua	150101	175	22
585	Parroquia Aragüita	150102	175	22
586	Parroquia Arévalo González	150103	175	22
587	Parroquia Capaya	150104	175	22
588	Parroquia El Café	150105	175	22
589	Parroquia Marizapa	150106	175	22
590	Parroquia Panaquire	150107	175	22
591	Parroquia Ribas	150108	175	22
592	Parroquia San José de Barlovento	150201	176	6
593	Parroquia Cumbo	150202	176	6
594	Parroquia Higuerote	150401	178	6
595	Parroquia Curiepe	150402	178	6
596	Parroquia Tacarigua	150403	178	6
597	Parroquia Mamporal	150501	179	6
598	Parroquia Río Chico	151401	188	6
599	Parroquia El Guapo	151402	188	6
600	Parroquia Tacarigua de La Laguna	151403	188	6
601	Parroquia Paparo	151404	188	6
602	Parroquia San Fernando del Guapo	151405	188	6
603	Parroquia Cúpira	151601	190	6
604	Parroquia Machurucuto	151602	190	6
605	Parroquia Guarenas	151701	191	6
606	Parroquia Guatire	152101	195	6
607	Parroquia Bolívar	152102	195	6
608	Parroquia Charallave	150801	182	5
609	Parroquia Las Brisas	150802	182	5
610	Parroquia Los Teques	151001	184	5
611	Parroquia Altagracia de La Montaña	151002	184	5
612	Parroquia Cecilio Acosta	151003	184	5
613	Parroquia El Jarillo	151004	184	5
614	Parroquia Paracotos	151005	184	5
615	Parroquia San Pedro	151006	184	5
616	Parroquia Tácata	151007	184	5
617	Parroquia Santa Teresa del Tuy	151101	184	5
618	Parroquia El Cartanal	151102	184	5
619	Parroquia Ocumare del Tuy	151201	186	5
620	Parroquia La Democracia	151202	186	5
621	Parroquia Santa Bárbara	151203	186	5
622	Parroquia Santa Lucía	151501	189	5
623	Parroquia San Francisco de Yare	151801	103	5
624	Parroquia San Antonio de Yare	151802	103	5
625	Parroquia Cúa	152001	194	5
626	Parroquia Nueva Cúa	152002	194	5
627	Parroquia Carrizal	150601	180	3
628	Parroquia San Antonio de Los Altos	151301	187	3
629	Parroquia Nuestra Señora del Rosario de Baruta	150301	177	2
630	Parroquia El Cafetal	150302	177	2
631	Parroquia Las Minas de Baruta	150303	177	2
632	Parroquia Chacao	150701	181	2
633	Parroquia El Hatillo	150901	103	2
634	Parroquia Petare	151901	193	2
635	Parroquia Caucagüita	151902	193	2
636	Parroquia Fila de Mariche	151903	193	2
637	Parroquia La Dolorita	151904	193	2
638	Parroquia Leoncio Martínez	151905	193	2
639	Parroquia Capital Acosta	160101	196	2
640	Parroquia San Francisco	160102	196	2
641	Parroquia Capital Caripe	160401	199	37
642	Parroquia El Guácharo	160402	199	37
643	Parroquia La Guanota	160403	199	37
644	Parroquia Sabana de Piedra	160404	199	37
645	Parroquia San Agustín	160405	199	37
646	Parroquia Teresén	160406	199	37
647	Parroquia Capital Cedeño	160501	200	37
648	Parroquia Areo	160502	200	37
649	Parroquia San Félix	160503	200	37
650	Parroquia Viento Fresco	160504	200	37
651	Parroquia Capital Ezequiel Zamora	160601	201	37
652	Parroquia El Tejero	160602	201	37
653	Parroquia Capital Maturín	160801	203	37
654	Parroquia Alto de los Godos	160802	203	37
656	Parroquia Las Cocuizas	160804	203	37
657	Parroquia San Simón	160805	203	37
658	Parroquia Santa Cruz	160806	203	37
659	Parroquia El Corozo	160807	203	37
660	Parroquia El Furrial	160808	203	37
661	Parroquia Jusepín	160809	203	37
662	Parroquia La Pica	160810	203	37
663	Parroquia San Vicente	160811	203	37
664	Parroquia Capital Piar	160901	204	37
665	Parroquia Aparicio	160902	204	37
666	Parroquia Chaguaramal	160903	204	37
667	Parroquia El Pinto	160904	204	37
668	Parroquia Guanaguana	160905	204	37
669	Parroquia La Toscana	160906	204	37
670	Parroquia Taguaya	160907	204	37
671	Parroquia Capital Punceres	161001	206	37
672	Parroquia Cachipo	161002	206	37
673	Parroquia Capital Libertador	160701	103	39
674	Parroquia Chaguaramas	160702	103	39
675	Parroquia Las Alhuacas	160703	103	39
676	Parroquia Tabasca	160704	103	39
677	Parroquia Capital Sotillo	161201	207	34
678	Parroquia Los Barrancos de Fajardo	161202	207	34
679	Parroquia Capital Díaz	170301	211	40
680	Parroquia Zabala	170302	211	40
681	Parroquia Capital García	170401	212	40
682	Parroquia Francisco Fajardo	170402	212	40
683	Parroquia Capital Gómez	170501	213	40
684	Parroquia Bolívar	170502	213	40
685	Parroquia Guevara	170503	213	40
686	Parroquia Matasiete	170504	213	40
687	Parroquia Sucre	170505	213	40
688	Parroquia Capital Maneiro	170601	214	40
689	Parroquia Aguirre	170602	214	40
690	Parroquia Capital Marcano	170701	215	40
691	Parroquia Adrián	170702	215	40
692	Parroquia Capital Península de Macanao	170901	217	40
693	Parroquia San Francisco	170902	217	40
694	Parroquia Capital Tubores	171001	218	40
695	Parroquia Los Barales	171002	218	40
696	Parroquia Capital Villalba	171101	219	40
697	Parroquia Vicente Fuentes	171102	219	40
698	Parroquia Capital Araure	180201	221	40
699	Parroquia Río Acarigua	180202	221	40
700	Parroquia Capital Esteller	180301	222	24
701	Parroquia Uveral	180302	222	24
702	Parroquia Capital Ospino	180701	226	24
703	Parroquia Aparición	180702	226	24
704	Parroquia La Estación	180703	226	24
705	Parroquia Capital Páez	180801	227	24
706	Parroquia Payara	180802	227	24
707	Parroquia Pimpinela	180803	227	24
708	Parroquia Ramón Peraza	180804	227	24
709	Parroquia Capital San Rafael de Onoto	181101	230	24
710	Parroquia Santa Fe	181102	230	24
711	Parroquia Thermo Morles	181103	230	24
712	Parroquia Capital Santa Rosalía	181201	231	24
713	Parroquia Florida	181202	231	24
714	Parroquia Capital Turén	181401	233	24
715	Parroquia Canelones	181402	233	24
716	Parroquia Santa Cruz	181403	233	24
717	Parroquia San Isidro Labrador	181404	233	24
718	Parroquia Capital San Genaro de Boconoito	181001	229	26
719	Parroquia Antolín Tovar	181002	229	26
720	Parroquia Capital Guanare	180401	223	27
721	Parroquia Córdoba	180402	223	27
722	Parroquia San José de la Montaña	180403	223	27
723	Parroquia San Juan de Guanaguanare	180404	223	27
724	Parroquia Virgen de la Coromoto	180405	223	27
725	Parroquia Capital Guanarito	180501	224	27
726	Parroquia Trinidad de la Capilla	180502	224	27
727	Parroquia Divina Pastora	180503	224	27
728	Parroquia Capital Mons. José Vicente de Unda	180601	225	27
729	Parroquia Peña Blanca	180602	225	27
730	Parroquia Capital Papelón	180901	228	27
731	Parroquia Caño Delgadito	180902	228	27
732	Parroquia Capital Sucre	181301	232	27
733	Parroquia Concepción	181302	232	27
734	Parroquia San Rafael de Palo Alzado	181303	232	27
735	Parroquia Uvencio Antonio Velásquez	181304	232	27
736	Parroquia San José de Saguaz	181305	232	27
737	Parroquia Villa Rosa	181306	232	27
738	Parroquia Mariño	190101	234	27
739	Parroquia Rómulo Gallegos	190102	234	27
740	Parroquia San José de Aerocuar	190201	235	41
741	Parroquia Tavera Acosta	190202	235	41
742	Parroquia Río Caribe	190301	236	41
743	Parroquia Antonio José de Sucre	190302	236	41
744	Parroquia El Morro de Puerto Santo	190303	236	41
745	Parroquia Puerto Santo	190304	236	41
746	Parroquia San Juan de Las Galdonas	190305	236	41
747	Parroquia El Pilar	190401	237	41
748	Parroquia El Rincón	190402	237	41
749	Parroquia General Francisco Antonio Vásquez	190403	237	41
750	Parroquia Guaraúnos	190404	237	41
751	Parroquia Tunapuicito	190405	237	41
752	Parroquia Unión	190406	237	41
753	Parroquia Bolívar	190501	238	41
754	Parroquia Macarapana	190502	238	41
755	Parroquia Santa Catalina	190503	238	41
756	Parroquia Santa Rosa	190504	238	41
757	Parroquia Santa Teresa	190505	238	41
758	Parroquia Yaguaraparo	190701	240	41
759	Parroquia El Paujil	190702	240	41
760	Parroquia Libertad	190703	240	41
761	Parroquia Araya	190801	241	41
762	Parroquia Chacopata	190802	241	41
763	Parroquia Manicuare	190803	241	41
764	Parroquia Tunapuy	190901	242	41
765	Parroquia Campo Elías	190902	242	41
766	Parroquia Irapa	191001	243	41
767	Parroquia Campo Claro	191002	243	41
768	Parroquia Marabal	191003	243	41
769	Parroquia San Antonio de Irapa	191004	243	41
770	Parroquia Soro	191005	243	41
771	Parroquia Cumanacoa	191201	245	41
772	Parroquia Arenas	191202	245	41
773	Parroquia Aricagua	191203	245	41
774	Parroquia Cocollar	191204	245	41
775	Parroquia San Fernando	191205	245	41
776	Parroquia San Lorenzo	191206	245	41
777	Parroquia Cariaco	191301	246	41
778	Parroquia Catuaro	191302	246	41
779	Parroquia Rendón	191303	246	41
780	Parroquia Santa Cruz	191304	246	41
781	Parroquia Santa María	191305	246	41
782	Parroquia Altagracia	191401	247	41
783	Parroquia Ayacucho	191402	247	41
784	Parroquia Santa Inés	191403	247	41
785	Parroquia Valentín Valiente	191404	247	41
786	Parroquia San Juan	191405	247	41
787	Parroquia Raúl Leoni	191406	247	41
788	Parroquia Gran Mariscal	191407	247	41
789	Parroquia Güiria	191501	248	41
790	Parroquia Bideau	191502	248	41
791	Parroquia Cristóbal Colón	191503	248	41
792	Parroquia Punta de Piedras	191504	248	41
793	Parroquia Ayacucho	200301	251	21
794	Parroquia Rivas Berti	200302	251	21
795	Parroquia San Pedro del Río	200303	251	21
796	Parroquia Cárdenas	200501	253	21
797	Parroquia Amenodoro Rangel Lamús	200502	253	21
798	Parroquia La Florida	200503	253	21
799	Parroquia Lobatera	201701	265	21
800	Parroquia Constitución	201702	265	21
801	Parroquia La Concordia	202301	271	21
802	Parroquia Pedro María Morantes	202302	271	21
803	Parroquia San Juan Bautista	202303	271	21
804	Parroquia San Sebastián	202304	271	21
805	Parroquia Dr. Francisco Romero Lobo	202305	271	21
806	Parroquia Fernández Feo	200701	255	23
807	Parroquia Alberto Adriani	200702	255	23
808	Parroquia Santo Domingo	200703	255	23
809	Parroquia Libertador	201601	264	23
810	Parroquia Don Emeterio Ochoa	201602	264	23
811	Parroquia Doradas	201603	264	23
812	Parroquia San Joaquín de Navay	201604	264	23
813	Parroquia Uribante	202801	276	23
814	Parroquia Cárdenas	202802	276	23
815	Parroquia Juan Pablo Peñaloza	202803	276	23
816	Parroquia Potosí	202804	276	23
817	Parroquia Libertad	201501	259	20
818	Parroquia Cipriano Castro	201502	259	20
819	Parroquia Manuel Felipe Rugeles	201503	259	20
820	Parroquia Junín	201401	262	20
821	Parroquia La Petrólea	201402	262	20
822	Parroquia Quinimarí	201403	262	20
823	Parroquia Bramón	201404	262	20
824	Parroquia Libertad	201501	263	20
825	Parroquia Cipriano Castro	201502	263	20
826	Parroquia Manuel Felipe Rugeles	201503	263	20
827	Parroquia Pedro María Ureña	202001	268	20
828	Parroquia Nueva Arcadia	202002	268	20
829	Parroquia Jáuregui	201201	260	20
830	Parroquia Emilio Constantino Guerrero	201202	260	20
831	Parroquia Monseñor Miguel Antonio Salas	201203	260	20
832	Parroquia Sucre	202600	274	19
833	Parroquia Eleazar López Contreras	202602	274	19
834	Parroquia San Pablo	202603	274	19
835	Parroquia Panamericano	201901	267	19
836	Parroquia La Palmita	201902	267	19
837	Parroquia Samuel Darío Maldonado	202201	270	18
838	Parroquia Boconó	202202	270	18
839	Parroquia Hernández	202203	270	18
840	Parroquia Santa Isabel	210101	278	14
841	Parroquia Araguaney	210102	278	14
842	Parroquia El Jagüito	210103	278	14
843	Parroquia La Esperanza	210104	278	14
844	Parroquia Boconó	210201	279	14
845	Parroquia El Carmen	210202	279	14
846	Parroquia Mosquey	210203	279	14
847	Parroquia Ayacucho	210204	279	14
848	Parroquia Burbusay	210205	279	14
849	Parroquia General Rivas	210206	279	14
850	Parroquia Guaramacal	210207	279	14
851	Parroquia Vega de Guaramacal	210208	279	14
852	Parroquia Monseñor Jáuregui	210209	279	14
853	Parroquia Rafael Rangel	210210	279	14
854	Parroquia San Miguel	210211	279	14
855	Parroquia San José	210212	279	14
856	Parroquia Sabana Grande	210301	280	14
857	Parroquia Cheregüé	210302	280	14
858	Parroquia Granados	210303	280	14
859	Parroquia Chejendé	210401	281	14
860	Parroquia Arnoldo Gabaldón	210402	281	14
861	Parroquia Bolivia	210403	281	14
862	Parroquia Carrillo	210404	281	14
863	Parroquia Cegarra	210405	281	14
864	Parroquia Manuel Salvador Ulloa	210406	281	14
865	Parroquia San José	210407	281	14
866	Parroquia Escuque	210601	283	14
867	Parroquia La Unión	210602	283	14
868	Parroquia Sabana Libre	210603	283	14
869	Parroquia Santa Rita	210604	283	14
870	Parroquia Santa Apolonia	210901	286	14
871	Parroquia El Progreso	210902	286	14
872	Parroquia La Ceiba	210903	286	14
873	Parroquia Tres de Febrero	210904	286	14
874	Parroquia El Dividive	211001	287	14
875	Parroquia Agua Santa	211002	287	14
876	Parroquia Agua Caliente	211003	287	14
877	Parroquia El Cenizo	211004	287	14
878	Parroquia Valerita	211005	287	14
879	Parroquia Monte Carmelo	211101	288	14
880	Parroquia Buena Vista	211102	288	14
881	Parroquia Santa María del Horcón	211103	288	14
882	Parroquia Motatán	211201	289	14
883	Parroquia El Baño	211202	289	14
884	Parroquia Jalisco	211203	289	14
885	Parroquia Pampán	211301	290	14
886	Parroquia Flor de Patria	211302	290	14
887	Parroquia La Paz	211303	290	14
888	Parroquia Santa Ana	211304	290	14
889	Parroquia Pampanito	211401	291	14
890	Parroquia La Concepción	211402	291	14
891	Parroquia Pampanito II	211403	291	14
892	Parroquia Betijoque	211501	292	14
893	Parroquia La Pueblita	211502	292	14
894	Parroquia Los Cedros	211503	292	14
895	Parroquia José Gregorio Hernández	211504	292	14
896	Parroquia Carvajal	211601	293	14
897	Parroquia Antonio Nicolás Briceño	211602	293	14
898	Parroquia Campo Alegre	211603	293	14
899	Parroquia José Leonardo Suárez	211604	293	14
900	Parroquia Sabana de Mendoza	211701	294	14
901	Parroquia El Paraíso	211702	294	14
902	Parroquia Junín	211703	294	14
903	Parroquia Valmore Rodríguez	211704	294	14
904	Parroquia Andrés Linares211801	211801	295	14
905	Parroquia Chiquinquirá	211802	295	14
906	Parroquia Cristóbal Mendoza	211803	295	14
907	Parroquia Cruz Carrillo	211804	295	14
908	Parroquia Matriz	211805	295	14
909	Parroquia Monseñor Carrillo	211806	295	14
910	Parroquia Tres Esquinas	211807	295	14
911	Parroquia La Quebrada	211901	296	14
912	Parroquia Cabimbú	211902	296	14
913	Parroquia Jajó	211903	296	14
914	Parroquia La Mesa	211904	296	14
915	Parroquia Santiago	211905	296	14
916	Parroquia Tuñame	211906	296	14
917	Parroquia Juan Ignacio Montilla	212001	297	14
918	Parroquia La Beatriz	212002	297	14
919	Parroquia Mercedes Díaz	212003	297	14
920	Parroquia San Luis	212004	297	14
921	Parroquia La Puerta	212005	297	14
922	Parroquia Mendoza	212006	297	14
923	Parroquia Carache	210501	282	11
924	Parroquia Cuicas	210502	282	11
925	Parroquia La Concepción	210503	282	11
926	Parroquia Panamericana	210504	282	11
927	Parroquia Santa Cruz	210505	282	11
928	Parroquia El Socorro	210701	284	11
929	Parroquia Antonio José de Sucre	210702	284	11
930	Parroquia Los Caprichos	210703	284	11
931	Parroquia Campo Elías	210801	285	27
932	Parroquia Arnoldo Gabaldón	210802	285	27
933	Parroquia Capital Bruzual	220301	300	11
934	Parroquia Campo Elías	220302	300	11
935	Parroquia Capital Nirgua	220901	305	11
936	Parroquia Salom	220902	305	11
937	Parroquia Temerla	220903	305	11
938	Parroquia Capital Peña	221000	306	11
939			306	11
940	Parroquia Capital San Felipe	221101	307	11
941	Parroquia Albarico	221102	307	11
942	Parroquia San Javier	221103	307	11
943	Parroquia Capital Veroes	221401	310	11
944	Parroquia El Guayabo	221402	310	11
945	Parroquia Isla de Toas	230101	311	16
946	Parroquia Monagas	230102	311	16
947	Parroquia La Concepción	230701	317	16
948	Parroquia José Ramón Yepes	230702	317	16
949	Parroquia Mariano Parra León	230703	317	16
950	Parroquia San José	230704	317	16
951	Parroquia Concepción	230901	319	16
952	Parroquia Andrés Bello	230902	319	16
953	Parroquia Chiquinquirá	230903	319	16
954	Parroquia El Carmelo	230904	319	16
955	Parroquia Potreritos	230905	319	16
956	Parroquia Libertad	231101	321	16
957	Parroquia Bartolomé de las Casas	231102	321	16
958	Parroquia Río Negro	231103	321	16
959	Parroquia San José de Perijá	231104	321	16
960	Parroquia San Rafael	231201	322	16
961	Parroquia La Sierrita	231202	322	16
962	Parroquia Las Parcelas	231203	322	16
963	Parroquia Luis de Vicente	231204	322	16
964	Parroquia Monseñor Marcos Sergio Godoy	231205	322	16
965	Parroquia Ricaurte	231206	322	16
966	Parroquia Tamare	231207	322	16
967	Parroquia Antonio Borjas Romero	231301	323	16
968	Parroquia Bolívar	231302	323	16
969	Parroquia Cacique Mara	231303	323	16
970	Parroquia Caracciolo Parra Pérez	231304	323	16
971	Parroquia Cecilio Acosta	231305	323	16
972	Parroquia Cristo de Aranza	231306	323	16
973	Parroquia Coquivacoa	231307	323	16
974	Parroquia Chiquinquirá	231308	323	16
975	Parroquia Francisco Eugenio Bustamante	231309	323	16
976	Parroquia Idelfonso Vásquez	231310	323	16
977	Parroquia Juana de Ávila	231311	323	16
978	Parroquia Luis Hurtado Higuera	231312	323	16
979	Parroquia Manuel Dagnino	231313	323	16
980	Parroquia Olegario Villalobos	231314	323	16
981	Parroquia Raúl Leoni	231315	323	16
982	Parroquia Santa Lucía	231316	323	16
983	Parroquia Venancio Pulgar	231317	323	16
984	Parroquia San Isidro	231318	323	16
985	Parroquia El Rosario	231601	326	16
986	Parroquia Donaldo García	231602	326	16
987	Parroquia Sixto Zambrano	231603	326	16
988	Parroquia San Francisco	231701	327	16
989	Parroquia El Bajo	231702	327	16
990	Parroquia Domitila Flores	231703	327	16
991	Parroquia Francisco Ochoa	231704	327	16
992	Parroquia Los Cortijos	231705	327	16
993	Parroquia Marcial Hernández	231706	327	16
994	Parroquia José Domingo Rus	231707	327	16
995	Parroquia Encontrados	230401	314	17
996	Parroquia Udón Pérez	230402	314	17
997	Parroquia San Carlos del Zulia	230501	315	17
998	Parroquia Moralito	230502	315	17
999	Parroquia Santa Bárbara	230503	315	17
1000	Parroquia Santa Cruz del Zulia	230504	315	17
1001	Parroquia Urribarri	230505	315	17
1002	Parroquia Jesús María Semprún	230801	318	17
1003	Parroquia Barí	230802	318	17
1004	Parroquia San Timoteo	230201	312	17
1005	Parroquia General Urdaneta	230202	312	17
1006	Parroquia Libertador	230203	312	17
1007	Parroquia Manuel Guanipa Matos	230204	312	17
1008	Parroquia Marcelino Briceño	230205	312	17
1009	Parroquia Pueblo Nuevo	230206	312	17
1010	Parroquia Ambrosio	230301	313	15
1011	Parroquia Carmen Herrera	230302	313	15
1012	Parroquia Germán Ríos Linares	230303	313	15
1013	Parroquia La Rosa	230304	313	15
1014	Parroquia Jorge Hernández	230305	313	15
1015	Parroquia Rómulo Betancourt	230306	313	15
1016	Parroquia San Benito	230307	313	15
1017	Parroquia Arístides Calvani	230308	313	15
1018	Parroquia Punta Gorda	230309	313	15
1019	Parroquia Alonso de Ojeda	231001	320	15
1020	Parroquia Libertad	231002	320	15
1021	Parroquia Eleazar López Contreras	231004	320	15
1022	Parroquia Venezuela	231005	320	15
1023	Parroquia El Danto	231006	320	15
1024	Parroquia Altagracia	231401	324	15
1025	Parroquia Ana María Campos	231402	324	15
1026	Parroquia Faría	231403	324	15
1027	Parroquia San Antonio	231404	324	15
1028	Parroquia San José	231405	324	15
1029	Parroquia José Antonio Chaves	231406	324	15
1030	Parroquia Santa Rita	231801	328	15
1031	Parroquia El Mene	231802	328	15
1032	Parroquia José Cenovio Urribarri	231803	328	15
1033	Parroquia Pedro Lucas Urribarri	231804	328	15
1034	Parroquia Manuel Manrique	231901	329	15
1035	Parroquia Rafael María Baralt	231902	329	15
1036	Parroquia Rafael Urdaneta	231903	329	15
1037	Parroquia La Victoria	232101	331	15
1038	Parroquia Rafael Urdaneta	232102	331	15
1039	Parroquia Raúl Cuenca	232103	331	15
1040	Parroquia Bobures	232001	330	14
1041	Parroquia El Batey	232002	330	14
1042	Parroquia Gibraltar	232003	330	14
1043	Parroquia Heras	232004	330	14
1044	Parroquia Monseñor Arturo Celestino Álvarez	232005	330	14
1045	Parroquia Rómulo Gallegos	232006	330	14
1046	Parroquia Caraballeda	240101	332	14
1047	Parroquia Carayaca	240102	332	14
1048	Parroquia Caruao	240103	332	14
1049	Parroquia Catia La Mar	240104	332	14
1050	Parroquia El Junko	240105	332	14
1051	Parroquia La Guaira	240106	332	14
1052	Parroquia Macuto	240107	332	14
1053	Parroquia Maiquetía	240108	332	14
1054	Parroquia Naiguatá	240109	332	14
1055	Parroquia Urimare	240110	332	14
1056	Parroquia Carlos Soublette	240111	332	14
\.


--
-- Name: parroquia_id_parroquia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.parroquia_id_parroquia_seq', 1056, true);


--
-- Data for Name: perfil; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.perfil (id_perfil, descripcion) FROM stdin;
1	Trabajador Académico
2	Coord. Regional del CE
3	Director del CE
4	Admin. DGTA
5	Analista DGTA
6	Vicerrectorado
7	Admin. DGSA
8	Analista DGSA
9	Admin. DGTIT
10	Analista DGTIT
\.


--
-- Name: perfil_id_perfil_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.perfil_id_perfil_seq', 10, true);


--
-- Data for Name: periodo_academico; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.periodo_academico (id_periodo_academico, descripcion, estatus, calendario_academico) FROM stdin;
\.


--
-- Name: periodo_academico_id_periodo_academico_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.periodo_academico_id_periodo_academico_seq', 1, false);


--
-- Data for Name: persona; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.persona (id_persona, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, fecha_nacimiento, id_estado_civil, id_genero, id_pais) FROM stdin;
208	EMPERATRIZ	CAROLINA	FRANKIS	ESCALONA	1992-12-08	9	4	242
209	Jesus		Escalona		2020-01-26	11	3	242
211	YOSMERY	ARELIS	SANTANA	DELGADO	1994-06-20	9	4	242
212	MARIA	AUXILIADORA	ESCALONA	RAMOS	1974-09-14	9	4	242
213	ANGEL	JAVIER	TORREALBA	\N	0180-04-06	9	3	242
214	Admin	\N	Admin	\N	1998-02-16	9	3	242
215	Vanessa		Rojas		2000-12-27	9	4	242
217	pepito		reberon		2019-12-29	9	3	242
210	NINO	MARIANO	PALACIOS	RINCONES	1969-12-31	9	3	242
233	PADRON	RIVAS	MIGUEL	ANGEL	1937-04-14	9	3	242
235	RUFO	PAYEMA	PRUDENCIO	\N	1949-04-28	9	3	45
236	OROZCO	DE RAMOS	CARMEN	AMADA	1953-12-07	9	4	242
237	HACKETT	BRICE O	EVELYN	ELIZABETH	1943-10-21	9	4	242
244	ESCALONA	PEREZ	MARIANO	DE LA	1943-10-16	9	3	242
245	HERNANDEZ	FLORES	MAXIMO	ANTONIO	1941-03-22	9	3	242
246	VICTORIA	YUSMIL	SANTAELLA	\N	1954-11-28	9	4	242
248	PEDRO	ARMENDUL	M	RQUEZ ESCALONA	1953-06-07	9	3	242
249	ORLANDO	DE JES	BENITEZ	RONDON	1945-02-05	11	3	242
271	NERIDA	JOSEFINA	MENDOZA	MONTENEGRO	1954-07-27	9	4	242
207	anthony	de jesus	frankis	escalona	1998-09-21	9	3	242
232	REQUENA		MIGUEL	ANTONIO	1940-05-09	9	3	242
273	TOMASA	DEL CARMEN	REYES	OLIMPIO	1953-07-16	9	4	242
274	GUSTAVO	JOSE	GONZALEZ	NIETO	1965-06-16	9	3	242
272	ROSA	ARELIS	VIRGUEZ	DE OLIVO	1955-06-15	10	4	242
250	MARTIN	SEGUNDO	RUEDA	TUTILLO	1995-08-30	9	3	242
206	CARLOS	AUGUSTO	FRANKIS	MU OZ	1974-05-09	9	4	242
251	JUAN	OSWALDO	SAGARAY	MERCHAN	1950-07-16	10	3	242
258	WIENFRIED	APOLINAR	BRION	ESPINA	1944-07-23	11	3	242
252	OSWALDO	ANTONIO	RAMÍREZ	HERRERA	1945-08-17	9	3	242
\.


--
-- Data for Name: persona_correo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.persona_correo (id_persona_correo, correo, id_persona, id_correo_tipo) FROM stdin;
152	empe@gmail.com	208	5
153	anthony@gmail.com	209	5
155	yosmerylml@gmail.com	211	5
156	maria750@gmail.com	212	5
157	anjel_javier_18@hotmail.com	213	5
158	admin@gmail.com	214	5
159	lopea@gmail.com	215	5
161	usuarioprueba1@gmail.com	217	5
154	ninomariano.5@gmail.com	210	5
163	mapadron@ubv.edu.ve 	233	5
164	prufo@ubv.edu.ve 	235	5
165	corozco@ubv.edu.ve 	236	5
166	ehackett@ubv.edu.ve 	237	5
167	mescalona@ubv.edu.ve 	244	5
168	mahernandezf@ubv.edu.ve 	245	5
169	vsantaella@ubv.edu.ve 	246	5
170	 	248	5
171	obenitez@ubv.edu.ve 	249	5
191	afrankis@ubv.edu.ve	258	6
192	anthony26334@gmail.com	258	5
176	cfrankis@ubv.edu.ve	206	6
179	afrankis@ubv.edu.ve	252	6
180	anthony26334@gmail.com	252	5
174	cfrankis@ubv.edu.veeeee	206	6
205	njmendoza@ubv.edu.ve 	271	5
151	anthony26334@gmail.com	207	5
162	requenami@gmail.com	232	5
207	afrankis@ubv.edu.ve	273	6
208	anthony26334@gmail.com	273	5
209	gjgonzalez@ubv.edu.ve	274	5
206	rvirguez@ubv.edu.ve	272	5
175	cfrankis@ubv.edu.ve	206	6
177	afrankis@ubv.edu.ve	251	6
178	anthony26334@gmail.com	251	5
173	mrueda@ubv.edu.ve	250	6
172	mruedaiunp@gmail.com	250	5
210	cfrankis@ubv.edu.ve	206	6
150	carlos.74.frankis@gmail.com	206	5
\.


--
-- Name: persona_correo_id_persona_correo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.persona_correo_id_persona_correo_seq', 210, true);


--
-- Data for Name: persona_discapacidad; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.persona_discapacidad (id_persona_discapacidad, id_persona, id_discapacidad, codigo_conapdis, observacion) FROM stdin;
\.


--
-- Name: persona_discapacidad_id_persona_discapacidad_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.persona_discapacidad_id_persona_discapacidad_seq', 1, false);


--
-- Data for Name: persona_etnia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.persona_etnia (id_persona_etnia, id_persona, id_etnia) FROM stdin;
\.


--
-- Name: persona_etnia_id_persona_etnia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.persona_etnia_id_persona_etnia_seq', 2, true);


--
-- Data for Name: persona_fecha_ingreso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.persona_fecha_ingreso (id_persona_fecha_ingreso, fecha, id_persona) FROM stdin;
145	2017-11-06	208
146	2006-04-21	210
147	2006-08-24	211
148	2006-07-23	212
149	2010-03-09	213
150	1999-03-06	214
152	2015-09-17	233
153	2007-03-01	235
154	2010-05-01	236
155	2006-03-01	237
156	2005-11-25	244
157	2013-10-08	245
158	2007-10-16	246
159	2006-09-01	248
160	2007-05-21	249
162	2007-01-01	251
164	2014-02-17	258
163	2005-01-10	252
177	2011-09-16	271
144	2019-03-11	207
151	2018-02-06	232
179	2007-01-01	273
180	2017-01-09	274
178	2012-09-16	272
161	2019-03-11	250
143	2006-06-07	206
\.


--
-- Name: persona_fecha_ingreso_id_persona_fecha_ingreso_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.persona_fecha_ingreso_id_persona_fecha_ingreso_seq', 180, true);


--
-- Data for Name: persona_hijo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.persona_hijo (id_persona_hijo, descripcion, id_persona) FROM stdin;
148	1	208
149	0	210
150	0	211
151	2	212
152	1	213
153	0	214
155	0	233
156	0	235
157	0	236
158	0	237
159	1	244
160	0	245
161	0	246
162	0	248
163	0	249
165	0	251
172	0	258
166	0	252
185	0	271
147	0	207
154	0	232
187	0	273
188	0	274
186	0	272
164	0	250
146	2	206
\.


--
-- Name: persona_hijo_id_persona_hijo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.persona_hijo_id_persona_hijo_seq', 188, true);


--
-- Name: persona_id_persona_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.persona_id_persona_seq', 274, true);


--
-- Data for Name: persona_nacionalidad; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.persona_nacionalidad (id_persona_nacionalidad, identificacion, id_nacionalidad, id_documento_identidad_tipo, id_persona) FROM stdin;
164	21130409	1	7	208
165	31103961	1	7	209
167	24757961	1	7	211
168	14809099	1	7	212
169	19958858	1	7	213
170	12345678	1	7	214
171	28645987	1	7	215
173	98765432	1	8	217
166	27083961	1	7	210
181	1526247	1	7	233
183	1564690	2	7	235
184	1566387	1	7	236
185	1928728	1	7	237
186	2073443	1	7	244
187	2359988	1	7	245
188	2476385	1	7	246
189	3449814	1	7	248
190	2906327	1	7	249
192	3046277	1	7	251
199	3158060	1	7	258
193	3088118	1	7	252
212	4361219	1	7	271
163	26334542	1	7	207
180	1488895	1	7	232
214	4023903	1	7	273
215	4087466	1	7	274
213	4419759	1	7	272
191	25847860	1	7	250
162	11670992	1	7	206
\.


--
-- Name: persona_nacionalidad_id_persona_nacionalidad_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.persona_nacionalidad_id_persona_nacionalidad_seq', 215, true);


--
-- Data for Name: persona_telefono; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.persona_telefono (id_persona_telefono, telefono, id_telefono_codigo_area, id_persona, id_telefono_tipo) FROM stdin;
155	3654896	1	208	7
156	2425931	1	209	7
158	1153497	2	211	7
159	4156744	2	212	7
160	5378964	2	213	7
161	3356987	1	214	7
162	2425931	1	215	7
163	2425931	1	217	7
157	1112559	2	210	7
165	2425931	1	251	7
167	2425931	1	258	7
166	2425931	1	252	7
154	2425931	1	207	7
168	2425931	1	273	7
169	4156744	2	274	8
170	4156744	2	272	8
164	2569634	1	250	7
153	3064462	6	206	7
\.


--
-- Name: persona_telefono_id_persona_telefono_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.persona_telefono_id_persona_telefono_seq', 170, true);


--
-- Data for Name: personal_administrativo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.personal_administrativo (id_personal_administrativo, id_persona, id_unidad_administrativa) FROM stdin;
\.


--
-- Name: personal_administrativo_id_personal_administrativo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.personal_administrativo_id_personal_administrativo_seq', 1, false);


--
-- Data for Name: ponencia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ponencia (id_ponencia, nombre_ponencia, nombre_evento, ano_ponencia, ciudad, titulo_memoria, volumen, identificador, pag_inicial, formato, url_ponencia, id_docente, estatus_verificacion) FROM stdin;
\.


--
-- Name: ponencia_id_ponencia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ponencia_id_ponencia_seq', 4, true);


--
-- Data for Name: pregunta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pregunta (id_pregunta, descripcion) FROM stdin;
1	¿Nombre de mi Primera Mascota?
2	¿Nombre de mi Marca de Carro Favorita?
3	¿Nombre de mi Mama?
4	¿Nombre de mi Herman@?
5	¿Nombre de mi papa?
\.


--
-- Name: pregunta_id_pregunta_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pregunta_id_pregunta_seq', 5, true);


--
-- Data for Name: programa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.programa (id_programa, descripcion, codigo, id_centro_estudio, id_programa_tipo, id_nivel_academico, id_linea_investigacion, estatus) FROM stdin;
1	Informática para la Gestión Social		1	1	23	1	1
2	Gestion Ambiental		2	1	23	1	0
3	Líderazgo Productivo		9	1	24	1	1
4	Proyecto Territorializado		9	1	24	1	1
\.


--
-- Name: programa_id_programa_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.programa_id_programa_seq', 4, true);


--
-- Data for Name: programa_nivel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.programa_nivel (id_programa_nivel, descripcion, estatus, codigo) FROM stdin;
2	Postgrado	t	56
1	Pregrado	\N	57
\.


--
-- Name: programa_nivel_id_programa_nivel_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.programa_nivel_id_programa_nivel_seq', 2, true);


--
-- Data for Name: programa_tipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.programa_tipo (id_programa_tipo, descripcion, codigo, id_programa_nivel) FROM stdin;
1	PFG		1
\.


--
-- Name: programa_tipo_id_programa_tipo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.programa_tipo_id_programa_tipo_seq', 1, true);


--
-- Data for Name: propuesta_investigacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.propuesta_investigacion (id_propuesta_investigacion, descripcion, estatus_propuesta, observacion, id_comitepfa_integrante, objetivo, id_linea_investigacion, id_tipo_archivo, estatus_registro) FROM stdin;
\.


--
-- Name: propuesta_investigacion_id_propuesta_investigacion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.propuesta_investigacion_id_propuesta_investigacion_seq', 1, false);


--
-- Data for Name: propuesta_jurado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.propuesta_jurado (id_propuesta_jurado, id_docente, estatus, id_solicitud_ascenso) FROM stdin;
\.


--
-- Name: propuesta_jurado_id_propuesta_jurado_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.propuesta_jurado_id_propuesta_jurado_seq', 158, true);


--
-- Data for Name: proyecto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.proyecto (id_proyecto, titulo, objetivo_general, descripcion, metodologia, estatus, direccion, id_parroquia, id_estudiante, id_tutor_estudios, id_metodologia) FROM stdin;
\.


--
-- Name: proyecto_id_proyecto_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.proyecto_id_proyecto_seq', 1, false);


--
-- Data for Name: reconocimiento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.reconocimiento (id_reconocimiento, id_tipo_reconocimiento, descripcion, entidad_promotora, ano_reconocimiento, caracter, id_documento, id_docente, estatus_verificacion) FROM stdin;
\.


--
-- Name: reconocimiento_id_reconocimiento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.reconocimiento_id_reconocimiento_seq', 4, true);


--
-- Data for Name: reingreso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.reingreso (id_reingreso, numero_solicitud, motivo, estatus, id_estudiante) FROM stdin;
\.


--
-- Name: reingreso_id_reingreso_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.reingreso_id_reingreso_seq', 1, false);


--
-- Data for Name: requisito_grado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.requisito_grado (id_requisito_grado, descripcion_titulo, id_malla_curricular, id_trayecto, id_tramo, id_grado_academico) FROM stdin;
\.


--
-- Name: requisito_grado_id_requisito_grado_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.requisito_grado_id_requisito_grado_seq', 1, false);


--
-- Data for Name: requisito_inscripcion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.requisito_inscripcion (id_requisito_inscripcion, descripcion, estatus, id_programa) FROM stdin;
\.


--
-- Name: requisito_inscripcion_id_requisito_inscripcion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.requisito_inscripcion_id_requisito_inscripcion_seq', 1, false);


--
-- Data for Name: rol; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.rol (id_rol, descripcion) FROM stdin;
3	Agregar
4	Consultar
5	Eliminar
6	Editar
\.


--
-- Name: rol_id_rol_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.rol_id_rol_seq', 6, true);


--
-- Data for Name: seccion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.seccion (id_seccion, descripcion, id_trayecto, id_tramo, id_oferta_academica, cupo) FROM stdin;
\.


--
-- Name: seccion_id_seccion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.seccion_id_seccion_seq', 1, false);


--
-- Data for Name: sno_familia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sno_familia (id_sno_familia, nacionalidad, pnombre, snombre, papellido, sapellido, parentesco, fecha_nac, sexfam, estado_civil, codest, ciudad_residencia, direccion, telefono, correo, cedper, tipo_trabajador, cedfam, estatus, observacion) FROM stdin;
1	Venezolano	Anthony	De Jesus	Frankis	Escalona	Hijo	1998-09-21	F	Soltero	15	Caracas	Palo Verde	(0414) 242-5931	anthony26334@gmail.com	11670992	Administrativo	26334542	1	
\.


--
-- Name: sno_familia_id_sno_familia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sno_familia_id_sno_familia_seq', 4, true);


--
-- Data for Name: solicitud_acta_pida; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.solicitud_acta_pida (id_solicitud_acta_pida, id_documento) FROM stdin;
\.


--
-- Name: solicitud_acta_pida_id_solicitud_acta_pida_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.solicitud_acta_pida_id_solicitud_acta_pida_seq', 95, true);


--
-- Data for Name: solicitud_acta_proyecto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.solicitud_acta_proyecto (id_solicitud_acta_proyecto, id_documento) FROM stdin;
\.


--
-- Name: solicitud_acta_proyecto_id_solicitud_acta_proyecto_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.solicitud_acta_proyecto_id_solicitud_acta_proyecto_seq', 95, true);


--
-- Data for Name: solicitud_ascenso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.solicitud_ascenso (id_solicitud_ascenso, id_escalafon, id_estatus_ascenso, id_docente, id_trabajo_ascenso, fecha_solicitud, id_concurso, id_solicitud_acta_pida, id_solicitud_acta_proyecto, ciudad, fecha_consulta) FROM stdin;
\.


--
-- Name: solicitud_ascenso_id_solicitud_ascenso_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.solicitud_ascenso_id_solicitud_ascenso_seq', 96, true);


--
-- Data for Name: solicitud_escalafon_siguiente; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.solicitud_escalafon_siguiente (id_solicitud_escalafon_siguiente, id_solicitud_ascenso, id_escalafon) FROM stdin;
\.


--
-- Name: solicitud_escalafon_siguiente_id_solicitud_escalafon_siguie_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.solicitud_escalafon_siguiente_id_solicitud_escalafon_siguie_seq', 20, true);


--
-- Data for Name: solicitud_resolucion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.solicitud_resolucion (id_solicitud_resolucion, id_solicitud_ascenso, id_documento) FROM stdin;
\.


--
-- Name: solicitud_resolucion_id_solicitud_resolucion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.solicitud_resolucion_id_solicitud_resolucion_seq', 7, true);


--
-- Data for Name: telefono_codigo_area; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.telefono_codigo_area (id_telefono_codigo_area, descripcion) FROM stdin;
1	414
2	426
3	424
4	412
5	212
6	416
\.


--
-- Name: telefono_codigo_area_id_telefono_codigo_area_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.telefono_codigo_area_id_telefono_codigo_area_seq', 6, true);


--
-- Data for Name: telefono_tipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.telefono_tipo (id_telefono_tipo, descripcion) FROM stdin;
7	Personal
8	Habitacion
9	Trabajo
\.


--
-- Name: telefono_tipo_id_telefono_tipo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.telefono_tipo_id_telefono_tipo_seq', 9, true);


--
-- Data for Name: tipo_activacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_activacion (id_tipo_activacion, descripcion) FROM stdin;
1	Ascensos
2	Concursos
\.


--
-- Name: tipo_activacion_id_tipo_activacion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_activacion_id_tipo_activacion_seq', 2, true);


--
-- Data for Name: tipo_archivo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_archivo (id_tipo_archivo, descripcion) FROM stdin;
\.


--
-- Name: tipo_archivo_id_tipo_archivo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_archivo_id_tipo_archivo_seq', 1, false);


--
-- Data for Name: tipo_discapacidad; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_discapacidad (id_tipo_discapacidad, descripcion) FROM stdin;
5	Sensorial
6	Motriz
7	Cognitivo-Intelectual
8	Psicosocial
\.


--
-- Name: tipo_discapacidad_id_tipo_discapacidad_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_discapacidad_id_tipo_discapacidad_seq', 8, true);


--
-- Data for Name: tipo_documento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_documento (id_tipo_documento, descripcion) FROM stdin;
1	TITULO_ESTUDIO_CONDUCENTE
2	TRABAJO_ASCENSO
3	ACTA_PIDA
4	ACTA_PROYECTO
5	RESOLUCION_ASCENSO
6	RESOLUCION_CONCURSO
7	Acta Reconocimiento
8	Certificado Diseño UC
10	MEMO SOLICITUD 1
11	MEMO SOLICITUD 2
12	PUNTO DE CUENTA SOLICITUD
13	MEMO RESOLUCIÓN 1
14	MEMO RESOLUCIÓN 2
15	ACTA_DEFENSA
16	PUNTO DE CUENTA RESOLUCION
17	TITULO_DOCTOR
\.


--
-- Name: tipo_documento_id_tipo_documento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_documento_id_tipo_documento_seq', 17, true);


--
-- Data for Name: tipo_estudio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_estudio (id_tipo_estudio, descripcion) FROM stdin;
1	Conducente
2	No Conducente
3	Idioma
\.


--
-- Name: tipo_estudio_id_tipo_estudio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_estudio_id_tipo_estudio_seq', 3, true);


--
-- Data for Name: tipo_experiencia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_experiencia (id_tipo_experiencia, descripcion) FROM stdin;
1	Trabajo Administrativo
2	Docencia Previa
3	Comisión Gubernamental
\.


--
-- Name: tipo_experiencia_id_tipo_experiencia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_experiencia_id_tipo_experiencia_seq', 3, true);


--
-- Data for Name: tipo_info; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_info (id_tipo_info, descripcion) FROM stdin;
1	Comisión
2	Excedencia
\.


--
-- Name: tipo_info_id_tipo_info_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_info_id_tipo_info_seq', 2, true);


--
-- Data for Name: tipo_jurado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_jurado (id_tipo_jurado, descripcion) FROM stdin;
1	Principal
2	Suplente
3	Coordinador(a)
\.


--
-- Name: tipo_jurado_id_tipo_jurado_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_jurado_id_tipo_jurado_seq', 3, true);


--
-- Data for Name: tipo_metodologia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_metodologia (id_tipo_metodologia, descripcion) FROM stdin;
\.


--
-- Name: tipo_metodologia_id_tipo_metodologia_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_metodologia_id_tipo_metodologia_seq', 1, false);


--
-- Data for Name: tipo_reconocimiento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_reconocimiento (id_tipo_reconocimiento, descripcion) FROM stdin;
1	Reconocimiento
2	Título Honorifíco
3	Premio
\.


--
-- Name: tipo_reconocimiento_id_tipo_reconocimiento_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_reconocimiento_id_tipo_reconocimiento_seq', 3, true);


--
-- Data for Name: trabajo_administrativo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.trabajo_administrativo (id_trabajo_administrativo, periodo, id_experiencia_docente) FROM stdin;
\.


--
-- Name: trabajo_administrativo_id_trabajo_administrativo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trabajo_administrativo_id_trabajo_administrativo_seq', 27, true);


--
-- Data for Name: trabajo_ascenso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.trabajo_ascenso (id_trabajo_ascenso, titulo, id_documento) FROM stdin;
\.


--
-- Name: trabajo_ascenso_id_trabajo_ascenso_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trabajo_ascenso_id_trabajo_ascenso_seq', 95, true);


--
-- Data for Name: trabajo_grado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.trabajo_grado (id_trabajo_grado, titulo, resumen, id_estudio_conducente) FROM stdin;
41	La pandemia Coronada	corona coronacoronacoronacoronacoronacorona corona coronacoronacoronacoronacoronacorona corona coronacoronacoronacoronacoronacorona corona coronacoronacoronacoronacoronacorona corona coronacoronacoronacoronacoronacorona corona coronacoronacoronacoronacoronacorona corona coronacoronacoronacoronacoronacorona corona coronacoronacoronacoronacoronacorona corona coronacoronacoronacoronacoronacorona corona coronacoronacoronacoronacoronacorona corona 	93
\.


--
-- Name: trabajo_grado_id_trabajo_grado_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trabajo_grado_id_trabajo_grado_seq', 41, true);


--
-- Data for Name: tramo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tramo (id_tramo, descripcion, codigo) FROM stdin;
1	TRAMO 1	T1
\.


--
-- Name: tramo_id_tramo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tramo_id_tramo_seq', 1, true);


--
-- Data for Name: trayecto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.trayecto (id_trayecto, descripcion, codigo) FROM stdin;
\.


--
-- Name: trayecto_id_trayecto_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trayecto_id_trayecto_seq', 1, false);


--
-- Data for Name: turno; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.turno (id_turno, descripcion) FROM stdin;
\.


--
-- Name: turno_id_turno_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.turno_id_turno_seq', 1, false);


--
-- Data for Name: tutor_estudios; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tutor_estudios (id_tutor_estudios, "año_grado", id_institucion_educativa, id_grado_academico, tutor_tipo) FROM stdin;
\.


--
-- Name: tutor_estudios_id_tutor_estudios_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tutor_estudios_id_tutor_estudios_seq', 1, false);


--
-- Data for Name: tutoria; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tutoria (id_tutoria, ano_tutoria, periodo_lectivo, estatus_verificacion, id_docente, id_area_conocimiento_ubv, id_linea_investigacion, id_nucleo_academico, id_centro_estudio, id_programa, id_eje_regional, nro_estudiantes, descripcion) FROM stdin;
\.


--
-- Name: tutoria_id_tutoria_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tutoria_id_tutoria_seq', 4, true);


--
-- Data for Name: unidad_administrativa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.unidad_administrativa (id_unidad_administrativa, descripcion, id_aldea) FROM stdin;
\.


--
-- Name: unidad_administrativa_id_unidad_administrativa_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.unidad_administrativa_id_unidad_administrativa_seq', 1, false);


--
-- Name: unidad_administrativa_telefon_id_unidad_administrativa_tele_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.unidad_administrativa_telefon_id_unidad_administrativa_tele_seq', 1, false);


--
-- Data for Name: unidad_administrativa_telefono; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.unidad_administrativa_telefono (id_unidad_administrativa_telefono, descripcion, id_unidad_administrativa, id_telefono_codigo_area) FROM stdin;
\.


--
-- Data for Name: unidad_curricular; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.unidad_curricular (id_unidad_curricular, descripcion, estatus) FROM stdin;
\.


--
-- Data for Name: unidad_curricular_detalle; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.unidad_curricular_detalle (id_unidad_curricular_detalle, num_horas_aula, num_horas_lab, unidades_credito, id_unidad_curricular, id_unidad_curricular_tipo, id_eje_formacion, id_frecuencia, codigo) FROM stdin;
\.


--
-- Name: unidad_curricular_detalle_id_unidad_curricular_detalle_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.unidad_curricular_detalle_id_unidad_curricular_detalle_seq', 1, false);


--
-- Name: unidad_curricular_id_unidad_curricular_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.unidad_curricular_id_unidad_curricular_seq', 1, false);


--
-- Data for Name: unidad_curricular_tipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.unidad_curricular_tipo (id_unidad_curricular_tipo, descripcion) FROM stdin;
5	Obligatoria
6	Electiva
\.


--
-- Name: unidad_curricular_tipo_id_unidad_curricular_tipo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.unidad_curricular_tipo_id_unidad_curricular_tipo_seq', 6, true);


--
-- Name: unidad_de_pago_id_unidad_de_pago_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.unidad_de_pago_id_unidad_de_pago_seq', 1, false);


--
-- Data for Name: unidad_pago; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.unidad_pago (id_unidad_pago, monto, estatus, fecha_registro, descripcion, hora_registro) FROM stdin;
\.


--
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuario (id_usuario, id_persona, usuario, password, fecha, estatus, id_perfil) FROM stdin;
81	209	ubv31103961	VXpIcjFacUVocGR3bHNTbjMweFRMZz09	2020-02-08	1	7
80	208	ubv21130409	VXpIcjFacUVocGR3bHNTbjMweFRMZz09	2020-02-02	1	7
83	211	ubv24757961	VXpIcjFacUVocGR3bHNTbjMweFRMZz09	2020-02-09	1	3
84	212	ubv14809099	VXpIcjFacUVocGR3bHNTbjMweFRMZz09	2020-02-09	1	5
85	213	ubv19958858	VXpIcjFacUVocGR3bHNTbjMweFRMZz09	2020-02-11	1	6
86	214	ubv12345678	VXpIcjFacUVocGR3bHNTbjMweFRMZz09	2020-02-12	1	9
87	215	ubv28645987	L0VnZ1hBYjlaOUVITlhjSjBvYkJzdz09	2020-02-13	1	10
88	217	ubv98765432	aEZEVnlGbTFvVFNTNzg4cWlOWk1LQT09	2020-02-13	1	2
82	210	ubv27083961	VXpIcjFacUVocGR3bHNTbjMweFRMZz09	2020-02-09	1	1
89	232	ubv1488895	VXpIcjFacUVocGR3bHNTbjMweFRMZz09	2020-02-17	1	1
90	233	ubv1526247	VXpIcjFacUVocGR3bHNTbjMweFRMZz09	2020-02-17	1	1
91	235	ubv1564690	VXpIcjFacUVocGR3bHNTbjMweFRMZz09	2020-02-17	1	1
92	236	ubv1566387	VXpIcjFacUVocGR3bHNTbjMweFRMZz09	2020-02-17	1	1
93	237	ubv1928728	VXpIcjFacUVocGR3bHNTbjMweFRMZz09	2020-02-17	1	1
94	244	ubv2073443	VXpIcjFacUVocGR3bHNTbjMweFRMZz09	2020-02-17	1	1
95	245	ubv2359988	VXpIcjFacUVocGR3bHNTbjMweFRMZz09	2020-02-17	1	1
96	246	ubv2476385	VXpIcjFacUVocGR3bHNTbjMweFRMZz09	2020-02-17	1	1
97	248	ubv3449814	VXpIcjFacUVocGR3bHNTbjMweFRMZz09	2020-02-18	1	1
98	249	ubv2906327	VXpIcjFacUVocGR3bHNTbjMweFRMZz09	2020-02-18	1	1
79	207	ubv26334542	VXpIcjFacUVocGR3bHNTbjMweFRMZz09	2020-02-02	1	2
105	273	ubv4023903	R0dvazJadWVKZ1dQMjNNa1ZKM2t6QT09	2021-02-16	1	1
106	274	ubv4087466	VXpIcjFacUVocGR3bHNTbjMweFRMZz09	2021-02-16	1	1
78	206	ubv11670992	Q2ZHSGNnSWR5YkVWWTVmNk5nQVBsUT09	2020-02-01	1	1
99	250	ubv25847860	VXpIcjFacUVocGR3bHNTbjMweFRMZz09	2020-02-18	1	1
100	251	ubv3046277	bWpaMlFuVmh0cVFMVnpHTG1KRXd1dz09	2021-02-15	1	1
102	258	ubv3158060	QmZyKzVwSlJPbmNYSnBFLzk5VzdnQT09	2021-02-15	1	1
101	252	ubv3088118	WkswUzErZWRDWG81b3RnOGk4dkRDUT09	2021-02-15	1	1
103	271	ubv4361219	VXpIcjFacUVocGR3bHNTbjMweFRMZz09	2021-02-15	1	1
104	272	ubv4419759	VXpIcjFacUVocGR3bHNTbjMweFRMZz09	2021-02-15	1	1
\.


--
-- Name: usuario_id_usuario_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuario_id_usuario_seq', 106, true);


--
-- Data for Name: usuario_pregunta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuario_pregunta (id_usuario_pregunta, id_pregunta, respuesta, id_usuario) FROM stdin;
145	1	asdfasdf	80
146	3	asdfasdf 	80
147	1	asdas	82
148	3	asdfasf	82
149	2	sdfgsdfg	83
150	4	asfasfa	83
151	1	asdfasf	84
152	3	dsfasdfasf	84
153	1	afsdasf	85
154	3	asdfadsfaf	85
155	1	ADMIN	86
156	4	ADMIN2	86
157	1	seguridad 	89
158	3	respuesta	89
159	1	solecito	90
160	2	clas	90
161	1	asdads	91
162	3	asdsad	91
163	1	asdasd	92
164	2	adsfasf	92
165	1	ASDAD	93
166	2	ASDFADS	93
167	1	asdfasdfadsf	94
168	5	asdfasf	94
169	1	asdasd	95
170	2	asdfadsf 	95
171	1	saasfda	96
172	3	fdsdfa	96
173	1	afsdasfd	97
174	3	asdfadsfasf 	97
175	1	asdfsadf	98
176	4	asdfdasf	98
177	1	FSADFA 	99
178	3	ASDFSAF 	99
179	1	pepe	100
180	4	pepe1	100
181	1	pepe 1	101
182	4	pepe 3	101
183	1	sucre	102
184	3	maria	102
185	1	pepe	103
186	3	mama	103
187	1	caca	104
188	2	cece	104
143	1	asdfasfdasf	79
144	2	asdfasfasfdas	79
189	1	sol 	105
190	4	caro	105
191	1	sol	106
192	4	caro	106
141	2	sfasdfas	78
142	4	asdfasdf	78
\.


--
-- Name: usuario_pregunta_id_usuario_pregunta_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuario_pregunta_id_usuario_pregunta_seq', 192, true);


--
-- Data for Name: usuario_rol; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuario_rol (id_usuario_rol, id_usuario, id_rol) FROM stdin;
4	81	3
5	87	3
6	88	4
12	82	4
14	86	3
15	86	4
16	86	5
17	86	6
91	100	3
92	100	4
93	100	5
94	100	6
97	102	3
98	102	4
100	101	3
101	101	5
117	105	3
118	105	4
119	105	5
126	106	3
127	78	4
128	99	3
129	99	4
130	99	5
131	99	6
\.


--
-- Name: usuario_rol_id_usuario_rol_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuario_rol_id_usuario_rol_seq', 131, true);


--
-- Name: activacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.activacion
    ADD CONSTRAINT activacion_pkey PRIMARY KEY (id_activacion);


--
-- Name: aldea_malla_curricula_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aldea_malla_curricula
    ADD CONSTRAINT aldea_malla_curricula_pkey PRIMARY KEY (id_aldea_malla_curricula);


--
-- Name: aldea_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aldea
    ADD CONSTRAINT aldea_pkey PRIMARY KEY (id_aldea);


--
-- Name: aldea_programa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aldea_programa
    ADD CONSTRAINT aldea_programa_pkey PRIMARY KEY (id_aldea_programa);


--
-- Name: aldea_telefono_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aldea_telefono
    ADD CONSTRAINT aldea_telefono_pkey PRIMARY KEY (id_aldea_telefono);


--
-- Name: aldea_tipo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aldea_tipo
    ADD CONSTRAINT aldea_tipo_pkey PRIMARY KEY (id_aldea_tipo);


--
-- Name: aldea_ubv_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aldea_ubv
    ADD CONSTRAINT aldea_ubv_pkey PRIMARY KEY (id_aldea_ubv);


--
-- Name: ambiente_detalle_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ambiente_detalle
    ADD CONSTRAINT ambiente_detalle_pkey PRIMARY KEY (id_ambiente_detalle);


--
-- Name: ambiente_espacio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ambiente_espacio
    ADD CONSTRAINT ambiente_espacio_pkey PRIMARY KEY (id_ambiente_espacio);


--
-- Name: ambiente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ambiente
    ADD CONSTRAINT ambiente_pkey PRIMARY KEY (id_ambiente);


--
-- Name: antropometrico_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.antropometrico
    ADD CONSTRAINT antropometrico_pkey PRIMARY KEY (id_antropometrico);


--
-- Name: arancel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arancel
    ADD CONSTRAINT arancel_pkey PRIMARY KEY (id_arancel);


--
-- Name: area_conocimiento_opsu_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.area_conocimiento_opsu
    ADD CONSTRAINT area_conocimiento_opsu_pkey PRIMARY KEY (id_area_conocimiento_opsu);


--
-- Name: area_conocimiento_ubv_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.area_conocimiento_ubv
    ADD CONSTRAINT area_conocimiento_ubv_pkey PRIMARY KEY (id_area_conocimiento_ubv);


--
-- Name: ascenso_titular_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ascenso_titular
    ADD CONSTRAINT ascenso_titular_pkey PRIMARY KEY (id_ascenso_titular);


--
-- Name: aspirante_estatus_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aspirante_estatus
    ADD CONSTRAINT aspirante_estatus_pkey PRIMARY KEY (id_aspirante_estatus);


--
-- Name: aspirante_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aspirante
    ADD CONSTRAINT aspirante_pkey PRIMARY KEY (id_aspirante);


--
-- Name: aspirante_tipo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aspirante_tipo
    ADD CONSTRAINT aspirante_tipo_pkey PRIMARY KEY (id_aspirante_tipo);


--
-- Name: banco_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.banco
    ADD CONSTRAINT banco_pkey PRIMARY KEY (id_banco);


--
-- Name: cambiar_oferta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cambiar_oferta
    ADD CONSTRAINT cambiar_oferta_pkey PRIMARY KEY (id_cambiar_oferta);


--
-- Name: carga_academica_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.carga_academica
    ADD CONSTRAINT carga_academica_pkey PRIMARY KEY (id_carga_academica);


--
-- Name: cedula; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nomina_docente_sigad
    ADD CONSTRAINT cedula UNIQUE (cedula);


--
-- Name: censo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.censo
    ADD CONSTRAINT censo_pkey PRIMARY KEY (id_censo);


--
-- Name: centro_estudio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.centro_estudio
    ADD CONSTRAINT centro_estudio_pkey PRIMARY KEY (id_centro_estudio);


--
-- Name: certificado_titulo_estudio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.certificado_titulo_estudio
    ADD CONSTRAINT certificado_titulo_estudio_pkey PRIMARY KEY (id_certificado_titulo_estudio);


--
-- Name: clasificacion_docente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.clasificacion_docente
    ADD CONSTRAINT clasificacion_docente_pkey PRIMARY KEY (id_clasificacion_docente);


--
-- Name: comision_excedencia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comision_excedencia
    ADD CONSTRAINT comision_excedencia_pkey PRIMARY KEY (id_comision_excedencia);


--
-- Name: comision_gubernamental_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comision_gubernamental
    ADD CONSTRAINT comision_gubernamental_pkey PRIMARY KEY (id_comision_gubernamental);


--
-- Name: comite_tipo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comite_tipo
    ADD CONSTRAINT comite_tipo_pkey PRIMARY KEY (id_comite_tipo);


--
-- Name: comitepfa_integrante_historico_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comitepfa_integrante_historico
    ADD CONSTRAINT comitepfa_integrante_historico_pkey PRIMARY KEY (id_comitepfa_integrante_historico);


--
-- Name: comitepfa_integrante_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comitepfa_integrante
    ADD CONSTRAINT comitepfa_integrante_pkey PRIMARY KEY (id_comitepfa_integrante);


--
-- Name: concurso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.concurso
    ADD CONSTRAINT concurso_pkey PRIMARY KEY (id_concurso);


--
-- Name: constancia_administrativo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.constancia_administrativo
    ADD CONSTRAINT constancia_administrativo_pkey PRIMARY KEY (id_constancia_administrativo);


--
-- Name: correo_tipo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.correo_tipo
    ADD CONSTRAINT correo_tipo_pkey PRIMARY KEY (id_correo_tipo);


--
-- Name: cuenta_bancaria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cuenta_bancaria
    ADD CONSTRAINT cuenta_bancaria_pkey PRIMARY KEY (id_cuenta_bancaria);


--
-- Name: cuenta_bancaria_tipo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cuenta_bancaria_tipo
    ADD CONSTRAINT cuenta_bancaria_tipo_pkey PRIMARY KEY (id_cuenta_bancaria_tipo);


--
-- Name: deposito_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.deposito
    ADD CONSTRAINT deposito_pkey PRIMARY KEY (id_deposito);


--
-- Name: dia_semana_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dia_semana
    ADD CONSTRAINT dia_semana_pkey PRIMARY KEY (id_dia_semana);


--
-- Name: discapacidad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discapacidad
    ADD CONSTRAINT discapacidad_pkey PRIMARY KEY (id_discapacidad);


--
-- Name: diseno_uc_docente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.diseno_uc_docente
    ADD CONSTRAINT diseno_uc_docente_pkey PRIMARY KEY (id_diseno_uc);


--
-- Name: docencia_previa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docencia_previa
    ADD CONSTRAINT docencia_previa_pkey PRIMARY KEY (id_docencia_previa);


--
-- Name: docencia_previa_ubv_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docencia_previa_ubv
    ADD CONSTRAINT docencia_previa_ubv_pkey PRIMARY KEY (id_docencia_previa_ubv);


--
-- Name: docente_aldea_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docente_aldea
    ADD CONSTRAINT docente_aldea_pkey PRIMARY KEY (id_docente_aldea);


--
-- Name: docente_dedicacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docente_dedicacion
    ADD CONSTRAINT docente_dedicacion_pkey PRIMARY KEY (id_docente_dedicacion);


--
-- Name: docente_estatus_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docente_estatus
    ADD CONSTRAINT docente_estatus_pkey PRIMARY KEY (id_docente_estatus);


--
-- Name: docente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docente
    ADD CONSTRAINT docente_pkey PRIMARY KEY (id_docente);


--
-- Name: docente_programa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docente_programa
    ADD CONSTRAINT docente_programa_pkey PRIMARY KEY (id_docente_programa);


--
-- Name: documento_identidad_tipo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documento_identidad_tipo
    ADD CONSTRAINT documento_identidad_tipo_pkey PRIMARY KEY (id_documento_identidad_tipo);


--
-- Name: documento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documento
    ADD CONSTRAINT documento_pkey PRIMARY KEY (id_documento);


--
-- Name: documento_solicitud_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documento_solicitud
    ADD CONSTRAINT documento_solicitud_pkey PRIMARY KEY (id_documento_solicitud);


--
-- Name: domicilio_detalle_tipo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.domicilio_detalle_tipo
    ADD CONSTRAINT domicilio_detalle_tipo_pkey PRIMARY KEY (id_domicilio_detalle_tipo);


--
-- Name: domicilio_persona_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.domicilio_persona
    ADD CONSTRAINT domicilio_persona_pkey PRIMARY KEY (id_domicilio_persona);


--
-- Name: eje_formacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.eje_formacion
    ADD CONSTRAINT eje_formacion_pkey PRIMARY KEY (id_eje_formacion);


--
-- Name: eje_municipal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.eje_municipal
    ADD CONSTRAINT eje_municipal_pkey PRIMARY KEY (id_eje_municipal);


--
-- Name: eje_regional_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.eje_regional
    ADD CONSTRAINT eje_regional_pkey PRIMARY KEY (id_eje_regional);


--
-- Name: escalafon_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.escalafon
    ADD CONSTRAINT escalafon_pkey PRIMARY KEY (id_escalafon);


--
-- Name: espacio_tipo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.espacio_tipo
    ADD CONSTRAINT espacio_tipo_pkey PRIMARY KEY (id_espacio_tipo);


--
-- Name: estado_civil_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estado_civil
    ADD CONSTRAINT estado_civil_pkey PRIMARY KEY (id_estado_civil);


--
-- Name: estado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estado
    ADD CONSTRAINT estado_pkey PRIMARY KEY (id_estado);


--
-- Name: estatus_ascenso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estatus_ascenso
    ADD CONSTRAINT estatus_ascenso_pkey PRIMARY KEY (id_estatus_ascenso);


--
-- Name: estudiante_detalle_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudiante_detalle
    ADD CONSTRAINT estudiante_detalle_pkey PRIMARY KEY (id_estudiante_detalle);


--
-- Name: estudiante_documento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudiante_documento
    ADD CONSTRAINT estudiante_documento_pkey PRIMARY KEY (id_estudiante_documento);


--
-- Name: estudiante_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudiante
    ADD CONSTRAINT estudiante_pkey PRIMARY KEY (id_estudiante);


--
-- Name: estudio_conducente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudio_conducente
    ADD CONSTRAINT estudio_conducente_pkey PRIMARY KEY (id_estudio_conducente);


--
-- Name: estudio_idioma_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudio_idioma
    ADD CONSTRAINT estudio_idioma_pkey PRIMARY KEY (id_estudio_idioma);


--
-- Name: estudio_nivel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudio_nivel
    ADD CONSTRAINT estudio_nivel_pkey PRIMARY KEY (id_estudio_nivel);


--
-- Name: estudio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudio
    ADD CONSTRAINT estudio_pkey PRIMARY KEY (id_estudio);


--
-- Name: etnia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.etnia
    ADD CONSTRAINT etnia_pkey PRIMARY KEY (id_etnia);


--
-- Name: expediente_documento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.expediente_documento
    ADD CONSTRAINT expediente_documento_pkey PRIMARY KEY (id_expediente_documento);


--
-- Name: fase_ii_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fase_ii
    ADD CONSTRAINT fase_ii_pkey PRIMARY KEY (id_fase_ii);


--
-- Name: genero_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.genero
    ADD CONSTRAINT genero_pkey PRIMARY KEY (id_genero);


--
-- Name: grado_academico_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grado_academico
    ADD CONSTRAINT grado_academico_pkey PRIMARY KEY (id_grado_academico);


--
-- Name: historia_ascenso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historia_ascenso
    ADD CONSTRAINT historia_ascenso_pkey PRIMARY KEY (id_historia_ascenso);


--
-- Name: historia_jurado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historia_jurado
    ADD CONSTRAINT historia_jurado_pkey PRIMARY KEY (id_historia_jurado);


--
-- Name: horario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.horario
    ADD CONSTRAINT horario_pkey PRIMARY KEY (id_horario);


--
-- Name: id_arte_software_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arte_software
    ADD CONSTRAINT id_arte_software_pkey PRIMARY KEY (id_arte_software);


--
-- Name: id_articulo_revista_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.articulo_revista
    ADD CONSTRAINT id_articulo_revista_pkey PRIMARY KEY (id_articulo_revista);


--
-- Name: id_cargo_experiencia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cargo_experiencia
    ADD CONSTRAINT id_cargo_experiencia_pkey PRIMARY KEY (id_cargo_experiencia);


--
-- Name: id_experiencia_docente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.experiencia_docente
    ADD CONSTRAINT id_experiencia_docente_pkey PRIMARY KEY (id_experiencia_docente);


--
-- Name: id_idioma; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.idioma
    ADD CONSTRAINT id_idioma PRIMARY KEY (id_idioma);


--
-- Name: id_libro_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.libro
    ADD CONSTRAINT id_libro_pkey PRIMARY KEY (id_libro);


--
-- Name: id_ponencia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ponencia
    ADD CONSTRAINT id_ponencia_pkey PRIMARY KEY (id_ponencia);


--
-- Name: id_sno_familia; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sno_familia
    ADD CONSTRAINT id_sno_familia PRIMARY KEY (id_sno_familia);


--
-- Name: id_tipo_experiencia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_experiencia
    ADD CONSTRAINT id_tipo_experiencia_pkey PRIMARY KEY (id_tipo_experiencia);


--
-- Name: ins_uni_curricular_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ins_uni_curricular
    ADD CONSTRAINT ins_uni_curricular_pkey PRIMARY KEY (id_ins_uni_curricular);


--
-- Name: inscripcion_detalle_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inscripcion_detalle
    ADD CONSTRAINT inscripcion_detalle_pkey PRIMARY KEY (id_inscripcion_detalle);


--
-- Name: inscripcion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inscripcion
    ADD CONSTRAINT inscripcion_pkey PRIMARY KEY (id_inscripcion);


--
-- Name: institucion_educativa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.institucion_educativa
    ADD CONSTRAINT institucion_educativa_pkey PRIMARY KEY (id_institucion_educativa);


--
-- Name: institucion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.institucion
    ADD CONSTRAINT institucion_pkey PRIMARY KEY (id_institucion);


--
-- Name: institucion_tipo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.institucion_tipo
    ADD CONSTRAINT institucion_tipo_pkey PRIMARY KEY (id_institucion_tipo);


--
-- Name: jurado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jurado
    ADD CONSTRAINT jurado_pkey PRIMARY KEY (id_jurado);


--
-- Name: linea_investigacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.linea_investigacion
    ADD CONSTRAINT linea_investigacion_pkey PRIMARY KEY (id_linea_investigacion);


--
-- Name: malla_curricular_detalle_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.malla_curricular_detalle
    ADD CONSTRAINT malla_curricular_detalle_pkey PRIMARY KEY (id_malla_curricular_detalle);


--
-- Name: malla_curricular_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.malla_curricular
    ADD CONSTRAINT malla_curricular_pkey PRIMARY KEY (id_malla_curricular);


--
-- Name: memo_solicitud_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.memo_solicitud
    ADD CONSTRAINT memo_solicitud_pkey PRIMARY KEY (id_memo_solicitud);


--
-- Name: metodologia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.metodologia
    ADD CONSTRAINT metodologia_pkey PRIMARY KEY (id_metodologia);


--
-- Name: modalidad_estudio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.modalidad_estudio
    ADD CONSTRAINT modalidad_estudio_pkey PRIMARY KEY (id_modalidad_estudio);


--
-- Name: modalidad_malla_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.modalidad_malla
    ADD CONSTRAINT modalidad_malla_pkey PRIMARY KEY (id_modalidad_malla);


--
-- Name: modulo_activacion_ingreso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.modulo_activacion
    ADD CONSTRAINT modulo_activacion_ingreso_pkey PRIMARY KEY (id_modulo_activacion);


--
-- Name: municipio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.municipio
    ADD CONSTRAINT municipio_pkey PRIMARY KEY (id_municipio);


--
-- Name: nacionalidad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nacionalidad
    ADD CONSTRAINT nacionalidad_pkey PRIMARY KEY (id_nacionalidad);


--
-- Name: nivel_academico_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nivel_academico
    ADD CONSTRAINT nivel_academico_pkey PRIMARY KEY (id_nivel_academico);


--
-- Name: nomina_docente_sigad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nomina_docente_sigad
    ADD CONSTRAINT nomina_docente_sigad_pkey PRIMARY KEY (id_nomina_docente_sigad);


--
-- Name: noticia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.noticia
    ADD CONSTRAINT noticia_pkey PRIMARY KEY (id_noticia);


--
-- Name: nucleo_academico_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nucleo_academico
    ADD CONSTRAINT nucleo_academico_pkey PRIMARY KEY (id_nucleo_academico);


--
-- Name: oferta_academica_detalle_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oferta_academica_detalle
    ADD CONSTRAINT oferta_academica_detalle_pkey PRIMARY KEY (id_oferta_academica_detalle);


--
-- Name: oferta_academica_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oferta_academica
    ADD CONSTRAINT oferta_academica_pkey PRIMARY KEY (id_oferta_academica);


--
-- Name: pais_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pais
    ADD CONSTRAINT pais_pkey PRIMARY KEY (id_pais);


--
-- Name: parroquia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parroquia
    ADD CONSTRAINT parroquia_pkey PRIMARY KEY (id_parroquia);


--
-- Name: perfil_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.perfil
    ADD CONSTRAINT perfil_pkey PRIMARY KEY (id_perfil);


--
-- Name: periodo_academico_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.periodo_academico
    ADD CONSTRAINT periodo_academico_pkey PRIMARY KEY (id_periodo_academico);


--
-- Name: persona_correo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_correo
    ADD CONSTRAINT persona_correo_pkey PRIMARY KEY (id_persona_correo);


--
-- Name: persona_discapacidad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_discapacidad
    ADD CONSTRAINT persona_discapacidad_pkey PRIMARY KEY (id_persona_discapacidad);


--
-- Name: persona_etnia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_etnia
    ADD CONSTRAINT persona_etnia_pkey PRIMARY KEY (id_persona_etnia);


--
-- Name: persona_fecha_ingreso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_fecha_ingreso
    ADD CONSTRAINT persona_fecha_ingreso_pkey PRIMARY KEY (id_persona_fecha_ingreso);


--
-- Name: persona_hijo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_hijo
    ADD CONSTRAINT persona_hijo_pkey PRIMARY KEY (id_persona_hijo);


--
-- Name: persona_nacionalidad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_nacionalidad
    ADD CONSTRAINT persona_nacionalidad_pkey PRIMARY KEY (id_persona_nacionalidad);


--
-- Name: persona_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona
    ADD CONSTRAINT persona_pkey PRIMARY KEY (id_persona);


--
-- Name: persona_telefono_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_telefono
    ADD CONSTRAINT persona_telefono_pkey PRIMARY KEY (id_persona_telefono);


--
-- Name: personal_administrativo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.personal_administrativo
    ADD CONSTRAINT personal_administrativo_pkey PRIMARY KEY (id_personal_administrativo);


--
-- Name: pregunta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pregunta
    ADD CONSTRAINT pregunta_pkey PRIMARY KEY (id_pregunta);


--
-- Name: programa_nivel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.programa_nivel
    ADD CONSTRAINT programa_nivel_pkey PRIMARY KEY (id_programa_nivel);


--
-- Name: programa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.programa
    ADD CONSTRAINT programa_pkey PRIMARY KEY (id_programa);


--
-- Name: programa_tipo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.programa_tipo
    ADD CONSTRAINT programa_tipo_pkey PRIMARY KEY (id_programa_tipo);


--
-- Name: propuesta_investigacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.propuesta_investigacion
    ADD CONSTRAINT propuesta_investigacion_pkey PRIMARY KEY (id_propuesta_investigacion);


--
-- Name: propuesta_jurado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.propuesta_jurado
    ADD CONSTRAINT propuesta_jurado_pkey PRIMARY KEY (id_propuesta_jurado);


--
-- Name: proyecto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proyecto
    ADD CONSTRAINT proyecto_pkey PRIMARY KEY (id_proyecto);


--
-- Name: reconocimiento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reconocimiento
    ADD CONSTRAINT reconocimiento_pkey PRIMARY KEY (id_reconocimiento);


--
-- Name: reingreso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reingreso
    ADD CONSTRAINT reingreso_pkey PRIMARY KEY (id_reingreso);


--
-- Name: requisito_grado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.requisito_grado
    ADD CONSTRAINT requisito_grado_pkey PRIMARY KEY (id_requisito_grado);


--
-- Name: requisito_inscripcion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.requisito_inscripcion
    ADD CONSTRAINT requisito_inscripcion_pkey PRIMARY KEY (id_requisito_inscripcion);


--
-- Name: rol_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.rol
    ADD CONSTRAINT rol_pkey PRIMARY KEY (id_rol);


--
-- Name: seccion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seccion
    ADD CONSTRAINT seccion_pkey PRIMARY KEY (id_seccion);


--
-- Name: solicitud_acta_pida_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_acta_pida
    ADD CONSTRAINT solicitud_acta_pida_pkey PRIMARY KEY (id_solicitud_acta_pida);


--
-- Name: solicitud_acta_proyecto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_acta_proyecto
    ADD CONSTRAINT solicitud_acta_proyecto_pkey PRIMARY KEY (id_solicitud_acta_proyecto);


--
-- Name: solicitud_ascenso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_ascenso
    ADD CONSTRAINT solicitud_ascenso_pkey PRIMARY KEY (id_solicitud_ascenso);


--
-- Name: solicitud_escalafon_siguiente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_escalafon_siguiente
    ADD CONSTRAINT solicitud_escalafon_siguiente_pkey PRIMARY KEY (id_solicitud_escalafon_siguiente);


--
-- Name: solicitud_resolucion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_resolucion
    ADD CONSTRAINT solicitud_resolucion_pkey PRIMARY KEY (id_solicitud_resolucion);


--
-- Name: telefono_codigo_area_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.telefono_codigo_area
    ADD CONSTRAINT telefono_codigo_area_pkey PRIMARY KEY (id_telefono_codigo_area);


--
-- Name: telefono_tipo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.telefono_tipo
    ADD CONSTRAINT telefono_tipo_pkey PRIMARY KEY (id_telefono_tipo);


--
-- Name: tipo_activacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_activacion
    ADD CONSTRAINT tipo_activacion_pkey PRIMARY KEY (id_tipo_activacion);


--
-- Name: tipo_archivo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_archivo
    ADD CONSTRAINT tipo_archivo_pkey PRIMARY KEY (id_tipo_archivo);


--
-- Name: tipo_discapacidad_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_discapacidad
    ADD CONSTRAINT tipo_discapacidad_pkey PRIMARY KEY (id_tipo_discapacidad);


--
-- Name: tipo_documento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_documento
    ADD CONSTRAINT tipo_documento_pkey PRIMARY KEY (id_tipo_documento);


--
-- Name: tipo_estudio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_estudio
    ADD CONSTRAINT tipo_estudio_pkey PRIMARY KEY (id_tipo_estudio);


--
-- Name: tipo_info_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_info
    ADD CONSTRAINT tipo_info_pkey PRIMARY KEY (id_tipo_info);


--
-- Name: tipo_jurado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_jurado
    ADD CONSTRAINT tipo_jurado_pkey PRIMARY KEY (id_tipo_jurado);


--
-- Name: tipo_metodologia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_metodologia
    ADD CONSTRAINT tipo_metodologia_pkey PRIMARY KEY (id_tipo_metodologia);


--
-- Name: tipo_reconocimiento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_reconocimiento
    ADD CONSTRAINT tipo_reconocimiento_pkey PRIMARY KEY (id_tipo_reconocimiento);


--
-- Name: trabajo_administrativo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trabajo_administrativo
    ADD CONSTRAINT trabajo_administrativo_pkey PRIMARY KEY (id_trabajo_administrativo);


--
-- Name: trabajo_ascenso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trabajo_ascenso
    ADD CONSTRAINT trabajo_ascenso_pkey PRIMARY KEY (id_trabajo_ascenso);


--
-- Name: trabajo_grado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trabajo_grado
    ADD CONSTRAINT trabajo_grado_pkey PRIMARY KEY (id_trabajo_grado);


--
-- Name: tramo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tramo
    ADD CONSTRAINT tramo_pkey PRIMARY KEY (id_tramo);


--
-- Name: trayecto_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trayecto
    ADD CONSTRAINT trayecto_pkey PRIMARY KEY (id_trayecto);


--
-- Name: turno_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.turno
    ADD CONSTRAINT turno_pkey PRIMARY KEY (id_turno);


--
-- Name: tutor_estudios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutor_estudios
    ADD CONSTRAINT tutor_estudios_pkey PRIMARY KEY (id_tutor_estudios);


--
-- Name: tutoria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutoria
    ADD CONSTRAINT tutoria_pkey PRIMARY KEY (id_tutoria);


--
-- Name: unidad_administrativa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidad_administrativa
    ADD CONSTRAINT unidad_administrativa_pkey PRIMARY KEY (id_unidad_administrativa);


--
-- Name: unidad_administrativa_telefono_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidad_administrativa_telefono
    ADD CONSTRAINT unidad_administrativa_telefono_pkey PRIMARY KEY (id_unidad_administrativa_telefono);


--
-- Name: unidad_curricular_detalle_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidad_curricular_detalle
    ADD CONSTRAINT unidad_curricular_detalle_pkey PRIMARY KEY (id_unidad_curricular_detalle);


--
-- Name: unidad_curricular_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidad_curricular
    ADD CONSTRAINT unidad_curricular_pkey PRIMARY KEY (id_unidad_curricular);


--
-- Name: unidad_curricular_tipo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidad_curricular_tipo
    ADD CONSTRAINT unidad_curricular_tipo_pkey PRIMARY KEY (id_unidad_curricular_tipo);


--
-- Name: unidad_de_pago_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidad_pago
    ADD CONSTRAINT unidad_de_pago_pkey PRIMARY KEY (id_unidad_pago);


--
-- Name: usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id_usuario);


--
-- Name: usuario_pregunta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario_pregunta
    ADD CONSTRAINT usuario_pregunta_pkey PRIMARY KEY (id_usuario_pregunta);


--
-- Name: usuario_rol_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario_rol
    ADD CONSTRAINT usuario_rol_pkey PRIMARY KEY (id_usuario_rol);


--
-- Name: h_aldeaprograma_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER h_aldeaprograma_update BEFORE UPDATE ON public.aldea_programa FOR EACH ROW EXECUTE PROCEDURE public.h_aldeaprograma();


--
-- Name: h_comitepfa_integrante_update; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER h_comitepfa_integrante_update BEFORE UPDATE ON public.comitepfa_integrante FOR EACH ROW EXECUTE PROCEDURE public.h_comitepfa_integrante();


--
-- Name: aldea_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aldea
    ADD CONSTRAINT aldea_ibfk_1 FOREIGN KEY (id_aldea_tipo) REFERENCES public.aldea_tipo(id_aldea_tipo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: aldea_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aldea
    ADD CONSTRAINT aldea_ibfk_2 FOREIGN KEY (id_ambiente) REFERENCES public.ambiente(id_ambiente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: aldea_malla_curricula_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aldea_malla_curricula
    ADD CONSTRAINT aldea_malla_curricula_ibfk_1 FOREIGN KEY (id_aldea) REFERENCES public.aldea(id_aldea) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: aldea_malla_curricula_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aldea_malla_curricula
    ADD CONSTRAINT aldea_malla_curricula_ibfk_2 FOREIGN KEY (id_malla_curricular) REFERENCES public.malla_curricular(id_malla_curricular) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: aldea_malla_curricula_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aldea_malla_curricula
    ADD CONSTRAINT aldea_malla_curricula_ibfk_3 FOREIGN KEY (id_turno) REFERENCES public.turno(id_turno) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: aldea_programa_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aldea_programa
    ADD CONSTRAINT aldea_programa_ibfk_1 FOREIGN KEY (id_aldea) REFERENCES public.aldea(id_aldea) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: aldea_programa_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aldea_programa
    ADD CONSTRAINT aldea_programa_ibfk_2 FOREIGN KEY (id_programa) REFERENCES public.programa(id_programa) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: aldea_telefono_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aldea_telefono
    ADD CONSTRAINT aldea_telefono_ibfk_1 FOREIGN KEY (id_telefono_codigo_area) REFERENCES public.telefono_codigo_area(id_telefono_codigo_area) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: aldea_telefono_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aldea_telefono
    ADD CONSTRAINT aldea_telefono_ibfk_2 FOREIGN KEY (id_aldea) REFERENCES public.aldea(id_aldea) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: ambiente_detalle_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ambiente_detalle
    ADD CONSTRAINT ambiente_detalle_ibfk_1 FOREIGN KEY (id_ambiente) REFERENCES public.ambiente(id_ambiente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: ambiente_espacio_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ambiente_espacio
    ADD CONSTRAINT ambiente_espacio_ibfk_1 FOREIGN KEY (id_espacio_tipo) REFERENCES public.espacio_tipo(id_espacio_tipo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: ambiente_espacio_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ambiente_espacio
    ADD CONSTRAINT ambiente_espacio_ibfk_2 FOREIGN KEY (id_ambiente_detalle) REFERENCES public.ambiente_detalle(id_ambiente_detalle) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: ambiente_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ambiente
    ADD CONSTRAINT ambiente_ibfk_1 FOREIGN KEY (id_parroquia) REFERENCES public.parroquia(id_parroquia) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: area_conocimiento_ubv_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.area_conocimiento_ubv
    ADD CONSTRAINT area_conocimiento_ubv_ibfk_1 FOREIGN KEY (id_area_conocimiento_opsu) REFERENCES public.area_conocimiento_opsu(id_area_conocimiento_opsu) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: aspirante_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aspirante
    ADD CONSTRAINT aspirante_ibfk_1 FOREIGN KEY (id_persona) REFERENCES public.persona(id_persona) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: aspirante_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aspirante
    ADD CONSTRAINT aspirante_ibfk_2 FOREIGN KEY (id_aspirante_tipo) REFERENCES public.aspirante_tipo(id_aspirante_tipo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: aspirante_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aspirante
    ADD CONSTRAINT aspirante_ibfk_3 FOREIGN KEY (id_grado_academico) REFERENCES public.grado_academico(id_grado_academico) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: aspirante_ibfk_4; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aspirante
    ADD CONSTRAINT aspirante_ibfk_4 FOREIGN KEY (id_aldea) REFERENCES public.aldea(id_aldea) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: aspirante_ibfk_5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aspirante
    ADD CONSTRAINT aspirante_ibfk_5 FOREIGN KEY (id_programa) REFERENCES public.programa(id_programa) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: aspirante_ibfk_6; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aspirante
    ADD CONSTRAINT aspirante_ibfk_6 FOREIGN KEY (id_aspirante_estatus) REFERENCES public.aspirante_estatus(id_aspirante_estatus) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: cambiar_oferta_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cambiar_oferta
    ADD CONSTRAINT cambiar_oferta_ibfk_1 FOREIGN KEY (id_oferta_academica) REFERENCES public.oferta_academica(id_oferta_academica) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: cambiar_oferta_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cambiar_oferta
    ADD CONSTRAINT cambiar_oferta_ibfk_2 FOREIGN KEY (id_persona) REFERENCES public.persona(id_persona) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: carga_academica_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.carga_academica
    ADD CONSTRAINT carga_academica_ibfk_1 FOREIGN KEY (id_oferta_academica_detalle) REFERENCES public.oferta_academica_detalle(id_oferta_academica_detalle) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: carga_academica_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.carga_academica
    ADD CONSTRAINT carga_academica_ibfk_2 FOREIGN KEY (id_ambiente_espacio) REFERENCES public.ambiente_espacio(id_ambiente_espacio) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: carga_academica_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.carga_academica
    ADD CONSTRAINT carga_academica_ibfk_3 FOREIGN KEY (id_docente) REFERENCES public.docente(id_docente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: censo_id_persona_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.censo
    ADD CONSTRAINT censo_id_persona_fkey FOREIGN KEY (id_persona) REFERENCES public.persona(id_persona);


--
-- Name: centro_estudio_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.centro_estudio
    ADD CONSTRAINT centro_estudio_ibfk_1 FOREIGN KEY (id_area_conocimiento_opsu) REFERENCES public.area_conocimiento_opsu(id_area_conocimiento_opsu) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: clasificacion_docente_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docente
    ADD CONSTRAINT clasificacion_docente_fk FOREIGN KEY (id_clasificacion_docente) REFERENCES public.clasificacion_docente(id_clasificacion_docente);


--
-- Name: comitepfa_integrante_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comitepfa_integrante
    ADD CONSTRAINT comitepfa_integrante_ibfk_1 FOREIGN KEY (id_programa) REFERENCES public.programa(id_programa) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: comitepfa_integrante_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comitepfa_integrante
    ADD CONSTRAINT comitepfa_integrante_ibfk_2 FOREIGN KEY (id_docente) REFERENCES public.docente(id_docente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: comitepfa_integrante_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comitepfa_integrante
    ADD CONSTRAINT comitepfa_integrante_ibfk_3 FOREIGN KEY (id_comite_tipo) REFERENCES public.comite_tipo(id_comite_tipo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: cuenta_bancaria_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cuenta_bancaria
    ADD CONSTRAINT cuenta_bancaria_ibfk_1 FOREIGN KEY (id_cuenta_bancaria_tipo) REFERENCES public.cuenta_bancaria_tipo(id_cuenta_bancaria_tipo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: cuenta_bancaria_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cuenta_bancaria
    ADD CONSTRAINT cuenta_bancaria_ibfk_2 FOREIGN KEY (id_banco) REFERENCES public.banco(id_banco) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: deposito_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.deposito
    ADD CONSTRAINT deposito_ibfk_1 FOREIGN KEY (id_arancel) REFERENCES public.arancel(id_arancel) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: deposito_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.deposito
    ADD CONSTRAINT deposito_ibfk_2 FOREIGN KEY (id_cuenta_bancaria) REFERENCES public.cuenta_bancaria(id_cuenta_bancaria) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: deposito_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.deposito
    ADD CONSTRAINT deposito_ibfk_3 FOREIGN KEY (id_persona) REFERENCES public.persona(id_persona) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: docente_aldea_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docente_aldea
    ADD CONSTRAINT docente_aldea_ibfk_1 FOREIGN KEY (id_docente) REFERENCES public.docente(id_docente) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: docente_aldea_ubv; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docente_aldea
    ADD CONSTRAINT docente_aldea_ubv FOREIGN KEY (id_aldea_ubv) REFERENCES public.aldea_ubv(id_aldea_ubv);


--
-- Name: docente_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docente
    ADD CONSTRAINT docente_ibfk_2 FOREIGN KEY (id_centro_estudio) REFERENCES public.centro_estudio(id_centro_estudio) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: docente_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docente
    ADD CONSTRAINT docente_ibfk_3 FOREIGN KEY (id_persona) REFERENCES public.persona(id_persona) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: docente_ibfk_5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docente
    ADD CONSTRAINT docente_ibfk_5 FOREIGN KEY (id_docente_dedicacion) REFERENCES public.docente_dedicacion(id_docente_dedicacion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: docente_ibfk_6; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docente
    ADD CONSTRAINT docente_ibfk_6 FOREIGN KEY (id_docente_estatus) REFERENCES public.docente_estatus(id_docente_estatus) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: docente_programa_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docente_programa
    ADD CONSTRAINT docente_programa_fkey1 FOREIGN KEY (id_docente) REFERENCES public.docente(id_docente);


--
-- Name: domicilio_persona_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.domicilio_persona
    ADD CONSTRAINT domicilio_persona_ibfk_2 FOREIGN KEY (id_parroquia) REFERENCES public.parroquia(id_parroquia) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: domicilio_persona_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.domicilio_persona
    ADD CONSTRAINT domicilio_persona_ibfk_3 FOREIGN KEY (id_persona) REFERENCES public.persona(id_persona) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: domicilio_persona_id_domicilio_detalle_tipo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.domicilio_persona
    ADD CONSTRAINT domicilio_persona_id_domicilio_detalle_tipo_fkey FOREIGN KEY (id_domicilio_detalle_tipo) REFERENCES public.domicilio_detalle_tipo(id_domicilio_detalle_tipo);


--
-- Name: eje_municipal_docente; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docente
    ADD CONSTRAINT eje_municipal_docente FOREIGN KEY (id_eje_municipal) REFERENCES public.eje_municipal(id_eje_municipal);


--
-- Name: eje_municipal_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.eje_municipal
    ADD CONSTRAINT eje_municipal_ibfk_1 FOREIGN KEY (id_eje_regional) REFERENCES public.eje_regional(id_eje_regional) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: escalafon_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docente
    ADD CONSTRAINT escalafon_fk FOREIGN KEY (id_escalafon) REFERENCES public.escalafon(id_escalafon);


--
-- Name: escalafon_solicitud_escalafon_siguiente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_escalafon_siguiente
    ADD CONSTRAINT escalafon_solicitud_escalafon_siguiente_fkey FOREIGN KEY (id_escalafon) REFERENCES public.escalafon(id_escalafon);


--
-- Name: estado_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estado
    ADD CONSTRAINT estado_ibfk_1 FOREIGN KEY (id_pais) REFERENCES public.pais(id_pais) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: estudiante_detalle_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudiante_detalle
    ADD CONSTRAINT estudiante_detalle_ibfk_1 FOREIGN KEY (id_estudiante) REFERENCES public.estudiante(id_estudiante) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: estudiante_detalle_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudiante_detalle
    ADD CONSTRAINT estudiante_detalle_ibfk_2 FOREIGN KEY (id_aldea_malla_curricula) REFERENCES public.aldea_malla_curricula(id_aldea_malla_curricula) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: estudiante_detalle_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudiante_detalle
    ADD CONSTRAINT estudiante_detalle_ibfk_3 FOREIGN KEY (id_turno) REFERENCES public.turno(id_turno) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: estudiante_documento_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudiante_documento
    ADD CONSTRAINT estudiante_documento_ibfk_1 FOREIGN KEY (id_requisito_inscripcion) REFERENCES public.requisito_inscripcion(id_requisito_inscripcion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: estudiante_documento_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudiante_documento
    ADD CONSTRAINT estudiante_documento_ibfk_2 FOREIGN KEY (id_persona) REFERENCES public.persona(id_persona) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: estudiante_documento_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudiante_documento
    ADD CONSTRAINT estudiante_documento_ibfk_3 FOREIGN KEY (id_tipo_archivo) REFERENCES public.tipo_archivo(id_tipo_archivo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: estudiante_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudiante
    ADD CONSTRAINT estudiante_ibfk_1 FOREIGN KEY (id_persona) REFERENCES public.persona(id_persona) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: estudiante_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudiante
    ADD CONSTRAINT estudiante_ibfk_2 FOREIGN KEY (id_aspirante_tipo) REFERENCES public.aspirante_tipo(id_aspirante_tipo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: estudio_nivel_acedemico_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudio_nivel
    ADD CONSTRAINT estudio_nivel_acedemico_fk FOREIGN KEY (id_nivel_academico) REFERENCES public.nivel_academico(id_nivel_academico);


--
-- Name: estudio_nivel_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudio_nivel
    ADD CONSTRAINT estudio_nivel_fk FOREIGN KEY (id_estudio) REFERENCES public.estudio(id_estudio);


--
-- Name: estudio_nivel_programa_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudio_nivel
    ADD CONSTRAINT estudio_nivel_programa_fk FOREIGN KEY (id_programa_nivel) REFERENCES public.programa_nivel(id_programa_nivel);


--
-- Name: expediente_documento_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.expediente_documento
    ADD CONSTRAINT expediente_documento_ibfk_1 FOREIGN KEY (id_estudiante_documento) REFERENCES public.estudiante_documento(id_estudiante_documento) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: expediente_documento_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.expediente_documento
    ADD CONSTRAINT expediente_documento_ibfk_2 FOREIGN KEY (id_estudiante_detalle) REFERENCES public.estudiante_detalle(id_estudiante_detalle) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: fk_activacion_tipo_activacion1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.activacion
    ADD CONSTRAINT fk_activacion_tipo_activacion1 FOREIGN KEY (id_tipo_activacion) REFERENCES public.tipo_activacion(id_tipo_activacion);


--
-- Name: fk_ascenso_titular_estudio_conducente1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ascenso_titular
    ADD CONSTRAINT fk_ascenso_titular_estudio_conducente1 FOREIGN KEY (id_estudio_conducente) REFERENCES public.estudio_conducente(id_estudio_conducente);


--
-- Name: fk_ascenso_titular_solicitud_ascenso1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ascenso_titular
    ADD CONSTRAINT fk_ascenso_titular_solicitud_ascenso1 FOREIGN KEY (id_solicitud_ascenso) REFERENCES public.solicitud_ascenso(id_solicitud_ascenso);


--
-- Name: fk_centro_estudio_tutoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutoria
    ADD CONSTRAINT fk_centro_estudio_tutoria FOREIGN KEY (id_centro_estudio) REFERENCES public.centro_estudio(id_centro_estudio);


--
-- Name: fk_certificado_titulo_documento1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.certificado_titulo_estudio
    ADD CONSTRAINT fk_certificado_titulo_documento1 FOREIGN KEY (id_documento) REFERENCES public.documento(id_documento);


--
-- Name: fk_certificado_titulo_estudio1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.certificado_titulo_estudio
    ADD CONSTRAINT fk_certificado_titulo_estudio1 FOREIGN KEY (id_estudio) REFERENCES public.estudio(id_estudio);


--
-- Name: fk_comision_excedencia_docente1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comision_excedencia
    ADD CONSTRAINT fk_comision_excedencia_docente1 FOREIGN KEY (id_docente) REFERENCES public.docente(id_docente);


--
-- Name: fk_comision_excedencia_tipo_info1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comision_excedencia
    ADD CONSTRAINT fk_comision_excedencia_tipo_info1 FOREIGN KEY (id_tipo_info) REFERENCES public.tipo_info(id_tipo_info);


--
-- Name: fk_comision_gubernamental_experiencia_docente1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comision_gubernamental
    ADD CONSTRAINT fk_comision_gubernamental_experiencia_docente1 FOREIGN KEY (id_experiencia_docente) REFERENCES public.experiencia_docente(id_experiencia_docente);


--
-- Name: fk_concurso_docente1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.concurso
    ADD CONSTRAINT fk_concurso_docente1 FOREIGN KEY (id_docente) REFERENCES public.docente(id_docente);


--
-- Name: fk_concurso_documento1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.concurso
    ADD CONSTRAINT fk_concurso_documento1 FOREIGN KEY (id_documento) REFERENCES public.documento(id_documento);


--
-- Name: fk_concurso_escalafon1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.concurso
    ADD CONSTRAINT fk_concurso_escalafon1 FOREIGN KEY (id_escalafon) REFERENCES public.escalafon(id_escalafon);


--
-- Name: fk_conducente_grado_estudio1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudio_conducente
    ADD CONSTRAINT fk_conducente_grado_estudio1 FOREIGN KEY (id_estudio) REFERENCES public.estudio(id_estudio);


--
-- Name: fk_constancia_administrativo_documento1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.constancia_administrativo
    ADD CONSTRAINT fk_constancia_administrativo_documento1 FOREIGN KEY (id_documento) REFERENCES public.documento(id_documento);


--
-- Name: fk_constancia_administrativo_trabajo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.constancia_administrativo
    ADD CONSTRAINT fk_constancia_administrativo_trabajo FOREIGN KEY (id_trabajo_administrativo) REFERENCES public.trabajo_administrativo(id_trabajo_administrativo);


--
-- Name: fk_diseno_uc_docente_docente1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.diseno_uc_docente
    ADD CONSTRAINT fk_diseno_uc_docente_docente1 FOREIGN KEY (id_docente) REFERENCES public.docente(id_docente);


--
-- Name: fk_docencia_previa_experiencia_docente1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docencia_previa
    ADD CONSTRAINT fk_docencia_previa_experiencia_docente1 FOREIGN KEY (id_experiencia_docente) REFERENCES public.experiencia_docente(id_experiencia_docente);


--
-- Name: fk_docencia_previa_modalidad_estudio1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docencia_previa
    ADD CONSTRAINT fk_docencia_previa_modalidad_estudio1 FOREIGN KEY (id_modalidad_estudio) REFERENCES public.modalidad_estudio(id_modalidad_estudio);


--
-- Name: fk_docencia_previa_nivel_academico1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docencia_previa
    ADD CONSTRAINT fk_docencia_previa_nivel_academico1 FOREIGN KEY (id_nivel_academico) REFERENCES public.nivel_academico(id_nivel_academico);


--
-- Name: fk_docencia_previa_ubv_aldea1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docencia_previa_ubv
    ADD CONSTRAINT fk_docencia_previa_ubv_aldea1 FOREIGN KEY (id_aldea) REFERENCES public.aldea(id_aldea);


--
-- Name: fk_docencia_previa_ubv_area_conocimiento_ubv1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docencia_previa_ubv
    ADD CONSTRAINT fk_docencia_previa_ubv_area_conocimiento_ubv1 FOREIGN KEY (id_area_conocimiento_ubv) REFERENCES public.area_conocimiento_ubv(id_area_conocimiento_ubv);


--
-- Name: fk_docencia_previa_ubv_centro_estudio1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docencia_previa_ubv
    ADD CONSTRAINT fk_docencia_previa_ubv_centro_estudio1 FOREIGN KEY (id_centro_estudio) REFERENCES public.centro_estudio(id_centro_estudio);


--
-- Name: fk_docencia_previa_ubv_docente; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docencia_previa_ubv
    ADD CONSTRAINT fk_docencia_previa_ubv_docente FOREIGN KEY (id_docente) REFERENCES public.docente(id_docente);


--
-- Name: fk_docencia_previa_ubv_eje_municipal1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docencia_previa_ubv
    ADD CONSTRAINT fk_docencia_previa_ubv_eje_municipal1 FOREIGN KEY (id_eje_municipal) REFERENCES public.eje_municipal(id_eje_municipal);


--
-- Name: fk_docencia_previa_ubv_eje_regional1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docencia_previa_ubv
    ADD CONSTRAINT fk_docencia_previa_ubv_eje_regional1 FOREIGN KEY (id_eje_regional) REFERENCES public.eje_regional(id_eje_regional);


--
-- Name: fk_docencia_previa_ubv_programa1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docencia_previa_ubv
    ADD CONSTRAINT fk_docencia_previa_ubv_programa1 FOREIGN KEY (id_programa) REFERENCES public.programa(id_programa);


--
-- Name: fk_docente_diseno_uc_area_conocimiento_ubv1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.diseno_uc_docente
    ADD CONSTRAINT fk_docente_diseno_uc_area_conocimiento_ubv1 FOREIGN KEY (id_area_conocimiento_ubv) REFERENCES public.area_conocimiento_ubv(id_area_conocimiento_ubv);


--
-- Name: fk_docente_diseno_uc_documento1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.diseno_uc_docente
    ADD CONSTRAINT fk_docente_diseno_uc_documento1 FOREIGN KEY (id_documento) REFERENCES public.documento(id_documento);


--
-- Name: fk_docente_diseno_uc_programa1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.diseno_uc_docente
    ADD CONSTRAINT fk_docente_diseno_uc_programa1 FOREIGN KEY (id_programa) REFERENCES public.programa(id_programa);


--
-- Name: fk_docente_diseno_uc_tramo1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.diseno_uc_docente
    ADD CONSTRAINT fk_docente_diseno_uc_tramo1 FOREIGN KEY (id_tramo) REFERENCES public.tramo(id_tramo);


--
-- Name: fk_docente_diseno_uc_unidad_curricular_tipo1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.diseno_uc_docente
    ADD CONSTRAINT fk_docente_diseno_uc_unidad_curricular_tipo1 FOREIGN KEY (id_unidad_curricular_tipo) REFERENCES public.unidad_curricular_tipo(id_unidad_curricular_tipo);


--
-- Name: fk_documento_memo_solicitud; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.memo_solicitud
    ADD CONSTRAINT fk_documento_memo_solicitud FOREIGN KEY (id_documento) REFERENCES public.documento(id_documento);


--
-- Name: fk_documento_solicitud_documento1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documento_solicitud
    ADD CONSTRAINT fk_documento_solicitud_documento1 FOREIGN KEY (id_documento) REFERENCES public.documento(id_documento);


--
-- Name: fk_documento_solicitud_solicitud_ascenso1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documento_solicitud
    ADD CONSTRAINT fk_documento_solicitud_solicitud_ascenso1 FOREIGN KEY (id_solicitud_ascenso) REFERENCES public.solicitud_ascenso(id_solicitud_ascenso);


--
-- Name: fk_eje_regional_tutoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutoria
    ADD CONSTRAINT fk_eje_regional_tutoria FOREIGN KEY (id_eje_regional) REFERENCES public.eje_regional(id_eje_regional);


--
-- Name: fk_estudio_docente1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudio
    ADD CONSTRAINT fk_estudio_docente1 FOREIGN KEY (id_docente) REFERENCES public.docente(id_docente);


--
-- Name: fk_estudio_idioma2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudio_idioma
    ADD CONSTRAINT fk_estudio_idioma2 FOREIGN KEY (id_idioma) REFERENCES public.idioma(id_idioma);


--
-- Name: fk_estudio_institucion1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudio
    ADD CONSTRAINT fk_estudio_institucion1 FOREIGN KEY (id_institucion) REFERENCES public.institucion(id_institucion);


--
-- Name: fk_estudio_modalidad_estudio1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudio
    ADD CONSTRAINT fk_estudio_modalidad_estudio1 FOREIGN KEY (id_modalidad_estudio) REFERENCES public.modalidad_estudio(id_modalidad_estudio);


--
-- Name: fk_estudio_pais1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudio
    ADD CONSTRAINT fk_estudio_pais1 FOREIGN KEY (id_pais) REFERENCES public.pais(id_pais);


--
-- Name: fk_estudio_tipo_estudio1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudio
    ADD CONSTRAINT fk_estudio_tipo_estudio1 FOREIGN KEY (id_tipo_estudio) REFERENCES public.tipo_estudio(id_tipo_estudio);


--
-- Name: fk_experiencia_docente_cargo_experiencia1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.experiencia_docente
    ADD CONSTRAINT fk_experiencia_docente_cargo_experiencia1 FOREIGN KEY (id_cargo_experiencia) REFERENCES public.cargo_experiencia(id_cargo_experiencia);


--
-- Name: fk_experiencia_docente_docente1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.experiencia_docente
    ADD CONSTRAINT fk_experiencia_docente_docente1 FOREIGN KEY (id_docente) REFERENCES public.docente(id_docente);


--
-- Name: fk_experiencia_docente_estado1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.experiencia_docente
    ADD CONSTRAINT fk_experiencia_docente_estado1 FOREIGN KEY (id_estado) REFERENCES public.estado(id_estado);


--
-- Name: fk_experiencia_docente_institucion1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.experiencia_docente
    ADD CONSTRAINT fk_experiencia_docente_institucion1 FOREIGN KEY (id_institucion) REFERENCES public.institucion(id_institucion);


--
-- Name: fk_experiencia_docente_tipo_experiencia1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.experiencia_docente
    ADD CONSTRAINT fk_experiencia_docente_tipo_experiencia1 FOREIGN KEY (id_tipo_experiencia) REFERENCES public.tipo_experiencia(id_tipo_experiencia);


--
-- Name: fk_experiencia_docente_trabajo_administrativo; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trabajo_administrativo
    ADD CONSTRAINT fk_experiencia_docente_trabajo_administrativo FOREIGN KEY (id_experiencia_docente) REFERENCES public.experiencia_docente(id_experiencia_docente);


--
-- Name: fk_fase_ii_solicitud_ascenso1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fase_ii
    ADD CONSTRAINT fk_fase_ii_solicitud_ascenso1 FOREIGN KEY (id_solicitud_ascenso) REFERENCES public.solicitud_ascenso(id_solicitud_ascenso);


--
-- Name: fk_historia_ascenso_docente1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historia_ascenso
    ADD CONSTRAINT fk_historia_ascenso_docente1 FOREIGN KEY (id_docente) REFERENCES public.docente(id_docente);


--
-- Name: fk_historia_ascenso_escalafon1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historia_ascenso
    ADD CONSTRAINT fk_historia_ascenso_escalafon1 FOREIGN KEY (id_escalafon) REFERENCES public.escalafon(id_escalafon);


--
-- Name: fk_historia_jurado_jurado1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historia_jurado
    ADD CONSTRAINT fk_historia_jurado_jurado1 FOREIGN KEY (jurado_id_jurado) REFERENCES public.jurado(id_jurado);


--
-- Name: fk_historia_jurado_propuesta_jurado1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historia_jurado
    ADD CONSTRAINT fk_historia_jurado_propuesta_jurado1 FOREIGN KEY (id_propuesta_jurado) REFERENCES public.propuesta_jurado(id_propuesta_jurado);


--
-- Name: fk_historia_jurado_solicitud_ascenso1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.historia_jurado
    ADD CONSTRAINT fk_historia_jurado_solicitud_ascenso1 FOREIGN KEY (id_solicitud_ascenso) REFERENCES public.solicitud_ascenso(id_solicitud_ascenso);


--
-- Name: fk_idioma_estudio1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estudio_idioma
    ADD CONSTRAINT fk_idioma_estudio1 FOREIGN KEY (id_estudio) REFERENCES public.estudio(id_estudio);


--
-- Name: fk_jurado_docente1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jurado
    ADD CONSTRAINT fk_jurado_docente1 FOREIGN KEY (id_docente) REFERENCES public.docente(id_docente);


--
-- Name: fk_jurado_propuesta_jurado1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jurado
    ADD CONSTRAINT fk_jurado_propuesta_jurado1 FOREIGN KEY (id_propuesta_jurado) REFERENCES public.propuesta_jurado(id_propuesta_jurado);


--
-- Name: fk_jurado_tipo_jurado1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jurado
    ADD CONSTRAINT fk_jurado_tipo_jurado1 FOREIGN KEY (id_tipo_jurado) REFERENCES public.tipo_jurado(id_tipo_jurado);


--
-- Name: fk_persona_fecha_ingreso; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_fecha_ingreso
    ADD CONSTRAINT fk_persona_fecha_ingreso FOREIGN KEY (id_persona) REFERENCES public.persona(id_persona);


--
-- Name: fk_pregunta; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario_pregunta
    ADD CONSTRAINT fk_pregunta FOREIGN KEY (id_pregunta) REFERENCES public.pregunta(id_pregunta);


--
-- Name: fk_programa_tutoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutoria
    ADD CONSTRAINT fk_programa_tutoria FOREIGN KEY (id_programa) REFERENCES public.programa(id_programa);


--
-- Name: fk_propuesta_jurado_docente1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.propuesta_jurado
    ADD CONSTRAINT fk_propuesta_jurado_docente1 FOREIGN KEY (id_docente) REFERENCES public.docente(id_docente);


--
-- Name: fk_propuesta_jurado_solicitud_ascenso; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.propuesta_jurado
    ADD CONSTRAINT fk_propuesta_jurado_solicitud_ascenso FOREIGN KEY (id_solicitud_ascenso) REFERENCES public.solicitud_ascenso(id_solicitud_ascenso);


--
-- Name: fk_reconocimiento_docente1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reconocimiento
    ADD CONSTRAINT fk_reconocimiento_docente1 FOREIGN KEY (id_docente) REFERENCES public.docente(id_docente);


--
-- Name: fk_reconocimiento_documento1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reconocimiento
    ADD CONSTRAINT fk_reconocimiento_documento1 FOREIGN KEY (id_documento) REFERENCES public.documento(id_documento) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: fk_reconocimiento_tipo_reconocimiento1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reconocimiento
    ADD CONSTRAINT fk_reconocimiento_tipo_reconocimiento1 FOREIGN KEY (id_tipo_reconocimiento) REFERENCES public.tipo_reconocimiento(id_tipo_reconocimiento);


--
-- Name: fk_solicitud_acta_pida; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_acta_pida
    ADD CONSTRAINT fk_solicitud_acta_pida FOREIGN KEY (id_documento) REFERENCES public.documento(id_documento);


--
-- Name: fk_solicitud_acta_pida; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_ascenso
    ADD CONSTRAINT fk_solicitud_acta_pida FOREIGN KEY (id_solicitud_acta_pida) REFERENCES public.solicitud_acta_pida(id_solicitud_acta_pida);


--
-- Name: fk_solicitud_acta_proyecto; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_acta_proyecto
    ADD CONSTRAINT fk_solicitud_acta_proyecto FOREIGN KEY (id_documento) REFERENCES public.documento(id_documento);


--
-- Name: fk_solicitud_acta_proyecto; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_ascenso
    ADD CONSTRAINT fk_solicitud_acta_proyecto FOREIGN KEY (id_solicitud_acta_proyecto) REFERENCES public.solicitud_acta_proyecto(id_solicitud_acta_proyecto);


--
-- Name: fk_solicitud_ascenso; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_resolucion
    ADD CONSTRAINT fk_solicitud_ascenso FOREIGN KEY (id_solicitud_ascenso) REFERENCES public.solicitud_ascenso(id_solicitud_ascenso);


--
-- Name: fk_solicitud_ascenso_concurso1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_ascenso
    ADD CONSTRAINT fk_solicitud_ascenso_concurso1 FOREIGN KEY (id_concurso) REFERENCES public.concurso(id_concurso);


--
-- Name: fk_solicitud_ascenso_docente1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_ascenso
    ADD CONSTRAINT fk_solicitud_ascenso_docente1 FOREIGN KEY (id_docente) REFERENCES public.docente(id_docente);


--
-- Name: fk_solicitud_ascenso_escalafon1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_ascenso
    ADD CONSTRAINT fk_solicitud_ascenso_escalafon1 FOREIGN KEY (id_escalafon) REFERENCES public.escalafon(id_escalafon);


--
-- Name: fk_solicitud_ascenso_estatus_ascenso1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_ascenso
    ADD CONSTRAINT fk_solicitud_ascenso_estatus_ascenso1 FOREIGN KEY (id_estatus_ascenso) REFERENCES public.estatus_ascenso(id_estatus_ascenso);


--
-- Name: fk_solicitud_ascenso_trabajo_ascenso1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_ascenso
    ADD CONSTRAINT fk_solicitud_ascenso_trabajo_ascenso1 FOREIGN KEY (id_trabajo_ascenso) REFERENCES public.trabajo_ascenso(id_trabajo_ascenso);


--
-- Name: fk_solicitud_memo_solicitud; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.memo_solicitud
    ADD CONSTRAINT fk_solicitud_memo_solicitud FOREIGN KEY (id_solicitud_ascenso) REFERENCES public.solicitud_ascenso(id_solicitud_ascenso);


--
-- Name: fk_solicitud_resolucion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_resolucion
    ADD CONSTRAINT fk_solicitud_resolucion FOREIGN KEY (id_documento) REFERENCES public.documento(id_documento);


--
-- Name: fk_trabajo_ascenso_documento1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trabajo_ascenso
    ADD CONSTRAINT fk_trabajo_ascenso_documento1 FOREIGN KEY (id_documento) REFERENCES public.documento(id_documento);


--
-- Name: fk_trabajo_grado_estudio_conducente1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trabajo_grado
    ADD CONSTRAINT fk_trabajo_grado_estudio_conducente1 FOREIGN KEY (id_estudio_conducente) REFERENCES public.estudio_conducente(id_estudio_conducente);


--
-- Name: fk_usuario_pregunta; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario_pregunta
    ADD CONSTRAINT fk_usuario_pregunta FOREIGN KEY (id_usuario) REFERENCES public.usuario(id_usuario);


--
-- Name: grado_academico_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grado_academico
    ADD CONSTRAINT grado_academico_ibfk_1 FOREIGN KEY (id_nivel_academico) REFERENCES public.nivel_academico(id_nivel_academico) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: horario_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.horario
    ADD CONSTRAINT horario_ibfk_1 FOREIGN KEY (id_dia_semana) REFERENCES public.dia_semana(id_dia_semana) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: horario_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.horario
    ADD CONSTRAINT horario_ibfk_2 FOREIGN KEY (id_carga_academica) REFERENCES public.carga_academica(id_carga_academica) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: id_area_conocimiento_ubv_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutoria
    ADD CONSTRAINT id_area_conocimiento_ubv_fk FOREIGN KEY (id_area_conocimiento_ubv) REFERENCES public.area_conocimiento_ubv(id_area_conocimiento_ubv);


--
-- Name: id_docente_arte_software_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.arte_software
    ADD CONSTRAINT id_docente_arte_software_fk FOREIGN KEY (id_docente) REFERENCES public.docente(id_docente);


--
-- Name: id_docente_articulo_revista_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.articulo_revista
    ADD CONSTRAINT id_docente_articulo_revista_fk FOREIGN KEY (id_docente) REFERENCES public.docente(id_docente);


--
-- Name: id_docente_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutoria
    ADD CONSTRAINT id_docente_fk FOREIGN KEY (id_docente) REFERENCES public.docente(id_docente);


--
-- Name: id_docente_libro_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.libro
    ADD CONSTRAINT id_docente_libro_fk FOREIGN KEY (id_docente) REFERENCES public.docente(id_docente);


--
-- Name: id_docente_ponencia_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ponencia
    ADD CONSTRAINT id_docente_ponencia_fk FOREIGN KEY (id_docente) REFERENCES public.docente(id_docente);


--
-- Name: id_linea_investigacion_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutoria
    ADD CONSTRAINT id_linea_investigacion_fk FOREIGN KEY (id_linea_investigacion) REFERENCES public.linea_investigacion(id_linea_investigacion);


--
-- Name: id_nucleo_academico_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutoria
    ADD CONSTRAINT id_nucleo_academico_fk FOREIGN KEY (id_nucleo_academico) REFERENCES public.nucleo_academico(id_nucleo_academico);


--
-- Name: id_persona_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documento
    ADD CONSTRAINT id_persona_fk FOREIGN KEY (id_persona) REFERENCES public.persona(id_persona);


--
-- Name: id_tipo_documento_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documento
    ADD CONSTRAINT id_tipo_documento_fk FOREIGN KEY (id_tipo_documento) REFERENCES public.tipo_documento(id_tipo_documento);


--
-- Name: ins_uni_curricular_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ins_uni_curricular
    ADD CONSTRAINT ins_uni_curricular_ibfk_1 FOREIGN KEY (id_inscripcion) REFERENCES public.inscripcion(id_inscripcion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: ins_uni_curricular_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ins_uni_curricular
    ADD CONSTRAINT ins_uni_curricular_ibfk_2 FOREIGN KEY (id_malla_curricular) REFERENCES public.malla_curricular(id_malla_curricular) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: inscripcion_detalle_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inscripcion_detalle
    ADD CONSTRAINT inscripcion_detalle_ibfk_1 FOREIGN KEY (id_inscripcion) REFERENCES public.inscripcion(id_inscripcion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: inscripcion_detalle_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.inscripcion_detalle
    ADD CONSTRAINT inscripcion_detalle_ibfk_2 FOREIGN KEY (id_carga_academica) REFERENCES public.carga_academica(id_carga_academica) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: institucion_tipo_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.institucion
    ADD CONSTRAINT institucion_tipo_fk FOREIGN KEY (id_institucion_tipo) REFERENCES public.institucion_tipo(id_institucion_tipo);


--
-- Name: linea_investigacion_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.linea_investigacion
    ADD CONSTRAINT linea_investigacion_ibfk_1 FOREIGN KEY (id_nucleo_academico) REFERENCES public.nucleo_academico(id_nucleo_academico) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: malla_curricular_detalle_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.malla_curricular_detalle
    ADD CONSTRAINT malla_curricular_detalle_ibfk_1 FOREIGN KEY (id_tramo) REFERENCES public.tramo(id_tramo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: malla_curricular_detalle_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.malla_curricular_detalle
    ADD CONSTRAINT malla_curricular_detalle_ibfk_2 FOREIGN KEY (id_malla_curricular) REFERENCES public.malla_curricular(id_malla_curricular) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: malla_curricular_detalle_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.malla_curricular_detalle
    ADD CONSTRAINT malla_curricular_detalle_ibfk_3 FOREIGN KEY (id_trayecto) REFERENCES public.trayecto(id_trayecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: malla_curricular_detalle_ibfk_4; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.malla_curricular_detalle
    ADD CONSTRAINT malla_curricular_detalle_ibfk_4 FOREIGN KEY (id_unidad_curricular_detalle) REFERENCES public.unidad_curricular_detalle(id_unidad_curricular_detalle) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: malla_curricular_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.malla_curricular
    ADD CONSTRAINT malla_curricular_ibfk_1 FOREIGN KEY (id_programa) REFERENCES public.programa(id_programa) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: malla_curricular_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.malla_curricular
    ADD CONSTRAINT malla_curricular_ibfk_2 FOREIGN KEY (id_modalidad_malla) REFERENCES public.modalidad_malla(id_modalidad_malla) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: metodologia_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.metodologia
    ADD CONSTRAINT metodologia_ibfk_1 FOREIGN KEY (id_tipo_metodologia) REFERENCES public.tipo_metodologia(id_tipo_metodologia) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: modulo_activacion_ingreso_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.modulo_activacion
    ADD CONSTRAINT modulo_activacion_ingreso_ibfk_1 FOREIGN KEY (id_oferta_academica) REFERENCES public.oferta_academica(id_oferta_academica) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: modulo_activacion_ingreso_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.modulo_activacion
    ADD CONSTRAINT modulo_activacion_ingreso_ibfk_3 FOREIGN KEY (id_aldea) REFERENCES public.aldea(id_aldea) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: municipio_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.municipio
    ADD CONSTRAINT municipio_ibfk_1 FOREIGN KEY (id_estado) REFERENCES public.estado(id_estado) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: nacionalidad_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nacionalidad
    ADD CONSTRAINT nacionalidad_ibfk_1 FOREIGN KEY (id_pais) REFERENCES public.pais(id_pais) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: nucleo_academico_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nucleo_academico
    ADD CONSTRAINT nucleo_academico_ibfk_1 FOREIGN KEY (id_nucleo_academico) REFERENCES public.nucleo_academico(id_nucleo_academico) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: oferta_academica_detalle_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oferta_academica_detalle
    ADD CONSTRAINT oferta_academica_detalle_ibfk_1 FOREIGN KEY (id_oferta_academica) REFERENCES public.oferta_academica(id_oferta_academica) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: oferta_academica_detalle_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oferta_academica_detalle
    ADD CONSTRAINT oferta_academica_detalle_ibfk_2 FOREIGN KEY (id_malla_curricular_detalle) REFERENCES public.malla_curricular_detalle(id_malla_curricular_detalle) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: oferta_academica_detalle_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oferta_academica_detalle
    ADD CONSTRAINT oferta_academica_detalle_ibfk_3 FOREIGN KEY (id_seccion) REFERENCES public.seccion(id_seccion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: oferta_academica_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oferta_academica
    ADD CONSTRAINT oferta_academica_ibfk_1 FOREIGN KEY (id_periodo_academico) REFERENCES public.periodo_academico(id_periodo_academico) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: oferta_academica_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.oferta_academica
    ADD CONSTRAINT oferta_academica_ibfk_2 FOREIGN KEY (id_aldea_malla_curricula) REFERENCES public.aldea_malla_curricula(id_aldea_malla_curricula) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: parroquia_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parroquia
    ADD CONSTRAINT parroquia_ibfk_1 FOREIGN KEY (id_municipio) REFERENCES public.municipio(id_municipio) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: parroquia_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parroquia
    ADD CONSTRAINT parroquia_ibfk_2 FOREIGN KEY (id_eje_municipal) REFERENCES public.eje_municipal(id_eje_municipal) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: persona_correo_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_correo
    ADD CONSTRAINT persona_correo_ibfk_1 FOREIGN KEY (id_persona) REFERENCES public.persona(id_persona) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: persona_correo_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_correo
    ADD CONSTRAINT persona_correo_ibfk_2 FOREIGN KEY (id_correo_tipo) REFERENCES public.correo_tipo(id_correo_tipo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: persona_discapacidad_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_discapacidad
    ADD CONSTRAINT persona_discapacidad_ibfk_1 FOREIGN KEY (id_persona) REFERENCES public.persona(id_persona) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: persona_discapacidad_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_discapacidad
    ADD CONSTRAINT persona_discapacidad_ibfk_2 FOREIGN KEY (id_discapacidad) REFERENCES public.discapacidad(id_discapacidad) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: persona_etnia_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_etnia
    ADD CONSTRAINT persona_etnia_ibfk_1 FOREIGN KEY (id_persona) REFERENCES public.persona(id_persona) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: persona_etnia_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_etnia
    ADD CONSTRAINT persona_etnia_ibfk_2 FOREIGN KEY (id_etnia) REFERENCES public.etnia(id_etnia) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: persona_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.antropometrico
    ADD CONSTRAINT persona_fk FOREIGN KEY (id_persona) REFERENCES public.persona(id_persona);


--
-- Name: persona_hijo_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_hijo
    ADD CONSTRAINT persona_hijo_ibfk_1 FOREIGN KEY (id_persona) REFERENCES public.persona(id_persona);


--
-- Name: persona_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona
    ADD CONSTRAINT persona_ibfk_1 FOREIGN KEY (id_estado_civil) REFERENCES public.estado_civil(id_estado_civil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: persona_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona
    ADD CONSTRAINT persona_ibfk_2 FOREIGN KEY (id_genero) REFERENCES public.genero(id_genero) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: persona_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona
    ADD CONSTRAINT persona_ibfk_3 FOREIGN KEY (id_pais) REFERENCES public.pais(id_pais) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: persona_nacionalidad_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_nacionalidad
    ADD CONSTRAINT persona_nacionalidad_ibfk_1 FOREIGN KEY (id_nacionalidad) REFERENCES public.nacionalidad(id_nacionalidad) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: persona_nacionalidad_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_nacionalidad
    ADD CONSTRAINT persona_nacionalidad_ibfk_2 FOREIGN KEY (id_documento_identidad_tipo) REFERENCES public.documento_identidad_tipo(id_documento_identidad_tipo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: persona_nacionalidad_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_nacionalidad
    ADD CONSTRAINT persona_nacionalidad_ibfk_3 FOREIGN KEY (id_persona) REFERENCES public.persona(id_persona) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: persona_telefono_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_telefono
    ADD CONSTRAINT persona_telefono_ibfk_1 FOREIGN KEY (id_telefono_codigo_area) REFERENCES public.telefono_codigo_area(id_telefono_codigo_area) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: persona_telefono_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_telefono
    ADD CONSTRAINT persona_telefono_ibfk_2 FOREIGN KEY (id_persona) REFERENCES public.persona(id_persona) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: persona_telefono_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persona_telefono
    ADD CONSTRAINT persona_telefono_ibfk_3 FOREIGN KEY (id_telefono_tipo) REFERENCES public.telefono_tipo(id_telefono_tipo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: personal_administrativo_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.personal_administrativo
    ADD CONSTRAINT personal_administrativo_ibfk_1 FOREIGN KEY (id_persona) REFERENCES public.persona(id_persona) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: personal_administrativo_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.personal_administrativo
    ADD CONSTRAINT personal_administrativo_ibfk_2 FOREIGN KEY (id_unidad_administrativa) REFERENCES public.unidad_administrativa(id_unidad_administrativa) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: programa_docente_fkey2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.docente_programa
    ADD CONSTRAINT programa_docente_fkey2 FOREIGN KEY (id_programa) REFERENCES public.programa(id_programa);


--
-- Name: programa_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.programa
    ADD CONSTRAINT programa_ibfk_1 FOREIGN KEY (id_programa_tipo) REFERENCES public.programa_tipo(id_programa_tipo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: programa_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.programa
    ADD CONSTRAINT programa_ibfk_2 FOREIGN KEY (id_nivel_academico) REFERENCES public.nivel_academico(id_nivel_academico) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: programa_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.programa
    ADD CONSTRAINT programa_ibfk_3 FOREIGN KEY (id_linea_investigacion) REFERENCES public.linea_investigacion(id_linea_investigacion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: programa_id_centro_estudio_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.programa
    ADD CONSTRAINT programa_id_centro_estudio_fkey FOREIGN KEY (id_centro_estudio) REFERENCES public.centro_estudio(id_centro_estudio);


--
-- Name: programa_tipo_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.programa_tipo
    ADD CONSTRAINT programa_tipo_ibfk_1 FOREIGN KEY (id_programa_nivel) REFERENCES public.programa_nivel(id_programa_nivel) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: propuesta_investigacion_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.propuesta_investigacion
    ADD CONSTRAINT propuesta_investigacion_ibfk_1 FOREIGN KEY (id_comitepfa_integrante) REFERENCES public.comitepfa_integrante(id_comitepfa_integrante) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: propuesta_investigacion_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.propuesta_investigacion
    ADD CONSTRAINT propuesta_investigacion_ibfk_2 FOREIGN KEY (id_linea_investigacion) REFERENCES public.linea_investigacion(id_linea_investigacion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: propuesta_investigacion_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.propuesta_investigacion
    ADD CONSTRAINT propuesta_investigacion_ibfk_3 FOREIGN KEY (id_tipo_archivo) REFERENCES public.tipo_archivo(id_tipo_archivo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: proyecto_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proyecto
    ADD CONSTRAINT proyecto_ibfk_1 FOREIGN KEY (id_parroquia) REFERENCES public.parroquia(id_parroquia) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: proyecto_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proyecto
    ADD CONSTRAINT proyecto_ibfk_2 FOREIGN KEY (id_estudiante) REFERENCES public.estudiante(id_estudiante) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: proyecto_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proyecto
    ADD CONSTRAINT proyecto_ibfk_3 FOREIGN KEY (id_tutor_estudios) REFERENCES public.tutor_estudios(id_tutor_estudios) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: proyecto_ibfk_4; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.proyecto
    ADD CONSTRAINT proyecto_ibfk_4 FOREIGN KEY (id_metodologia) REFERENCES public.metodologia(id_metodologia) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: reingreso_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reingreso
    ADD CONSTRAINT reingreso_ibfk_1 FOREIGN KEY (id_estudiante) REFERENCES public.estudiante(id_estudiante) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: requisito_grado_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.requisito_grado
    ADD CONSTRAINT requisito_grado_ibfk_1 FOREIGN KEY (id_malla_curricular) REFERENCES public.malla_curricular(id_malla_curricular) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: requisito_grado_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.requisito_grado
    ADD CONSTRAINT requisito_grado_ibfk_2 FOREIGN KEY (id_trayecto) REFERENCES public.trayecto(id_trayecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: requisito_grado_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.requisito_grado
    ADD CONSTRAINT requisito_grado_ibfk_3 FOREIGN KEY (id_tramo) REFERENCES public.tramo(id_tramo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: requisito_grado_ibfk_4; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.requisito_grado
    ADD CONSTRAINT requisito_grado_ibfk_4 FOREIGN KEY (id_grado_academico) REFERENCES public.grado_academico(id_grado_academico) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: requisito_inscripcion_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.requisito_inscripcion
    ADD CONSTRAINT requisito_inscripcion_ibfk_1 FOREIGN KEY (id_programa) REFERENCES public.programa(id_programa) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: seccion_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seccion
    ADD CONSTRAINT seccion_ibfk_1 FOREIGN KEY (id_tramo) REFERENCES public.tramo(id_tramo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: seccion_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seccion
    ADD CONSTRAINT seccion_ibfk_2 FOREIGN KEY (id_trayecto) REFERENCES public.trayecto(id_trayecto) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: seccion_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.seccion
    ADD CONSTRAINT seccion_ibfk_3 FOREIGN KEY (id_oferta_academica) REFERENCES public.oferta_academica(id_oferta_academica) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: solicitud_ascenso_escalafon_siguiente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.solicitud_escalafon_siguiente
    ADD CONSTRAINT solicitud_ascenso_escalafon_siguiente_fkey FOREIGN KEY (id_solicitud_ascenso) REFERENCES public.solicitud_ascenso(id_solicitud_ascenso);


--
-- Name: tipo_discapacidad; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discapacidad
    ADD CONSTRAINT tipo_discapacidad FOREIGN KEY (id_tipo_discapacidad) REFERENCES public.tipo_discapacidad(id_tipo_discapacidad);


--
-- Name: tutor_estudios_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutor_estudios
    ADD CONSTRAINT tutor_estudios_ibfk_1 FOREIGN KEY (id_tutor_estudios) REFERENCES public.tutor_estudios(id_tutor_estudios) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tutor_estudios_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tutor_estudios
    ADD CONSTRAINT tutor_estudios_ibfk_2 FOREIGN KEY (id_grado_academico) REFERENCES public.grado_academico(id_grado_academico) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: unidad_administrativa_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidad_administrativa
    ADD CONSTRAINT unidad_administrativa_ibfk_1 FOREIGN KEY (id_aldea) REFERENCES public.aldea(id_aldea) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: unidad_administrativa_telefono_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidad_administrativa_telefono
    ADD CONSTRAINT unidad_administrativa_telefono_ibfk_1 FOREIGN KEY (id_unidad_administrativa) REFERENCES public.unidad_administrativa(id_unidad_administrativa) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: unidad_administrativa_telefono_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidad_administrativa_telefono
    ADD CONSTRAINT unidad_administrativa_telefono_ibfk_2 FOREIGN KEY (id_telefono_codigo_area) REFERENCES public.telefono_codigo_area(id_telefono_codigo_area) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: unidad_curricular_detalle_ibfk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidad_curricular_detalle
    ADD CONSTRAINT unidad_curricular_detalle_ibfk_1 FOREIGN KEY (id_unidad_curricular) REFERENCES public.unidad_curricular(id_unidad_curricular) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: unidad_curricular_detalle_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidad_curricular_detalle
    ADD CONSTRAINT unidad_curricular_detalle_ibfk_2 FOREIGN KEY (id_unidad_curricular_tipo) REFERENCES public.unidad_curricular_tipo(id_unidad_curricular_tipo) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: unidad_curricular_detalle_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidad_curricular_detalle
    ADD CONSTRAINT unidad_curricular_detalle_ibfk_3 FOREIGN KEY (id_eje_formacion) REFERENCES public.eje_formacion(id_eje_formacion) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: usuario_ibfk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_ibfk_2 FOREIGN KEY (id_persona) REFERENCES public.persona(id_persona) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: usuario_ibfk_3; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_ibfk_3 FOREIGN KEY (id_perfil) REFERENCES public.perfil(id_perfil) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: usuario_rol_rol_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario_rol
    ADD CONSTRAINT usuario_rol_rol_fk FOREIGN KEY (id_rol) REFERENCES public.rol(id_rol);


--
-- Name: usuario_rol_usuario_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario_rol
    ADD CONSTRAINT usuario_rol_usuario_fk FOREIGN KEY (id_usuario) REFERENCES public.usuario(id_usuario);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

