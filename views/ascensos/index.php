<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Solicitudes De Ascenso | SIDTA</title>


 
    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">


</head>

<body>
 
    <div id="wrapper">
   <?php require 'views/header.php'; ?>
   

   <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-5">
                    <h2>Solicitudes de Ascenso</h2>
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>solicitar_ascenso">Ascensos</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Solicitudes</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-7">
                    <div class="title-action tooltip-demo">
                        <?php 
                        
                        //var_dump(count($this->ascensos)>=1 || $this->band);
                        if(count($this->ascensos)>=1){ ?>
                        <!-- <button href="#" disabled class="btn btn-primary" data-toggle="tooltip" data-placement="left" title="Ya Has Realizado tu Solicitud. Si existe algún Inconveniente comunicate con el administrador"><i class="fa fa-list"></i> Solicitar</button>-->
                        <?php } else{ ?>
                            <a href="<?php echo constant ('URL');?>solicitar_ascenso/viewAgregar" class="btn btn-primary"><i class="fa fa-list"></i> Solicitar</a>
                            <?php } ?>

                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>SIDTA</h5>
                        <div class="ibox-tools">

                 

                        </div>
                    </div>
                    <div class="ibox-content">
                    <?php echo $this->mensaje;?>

                        <!--  contenido -->
                        <div class="table-responsive">
                        
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Fecha de Solicitud</th>
                        <th>Escalafón</th>
                        <th>Estatus</th>
                        <th>Ciudad</th>

                        <th>Acción</th>
                    </tr>
                    </thead >
                    <tbody id="tbody-diseno">
                    <?php 
                            foreach($this->ascensos as $row){
                                $ascensos= new Estructura();
                                $ascensos=$row;?>
                    <tr id ="fila-<?php echo $ascensos->id_ascensos; ?>" class="gradeX">
                        <td><?php echo date("d-m-Y",strtotime($ascensos->fecha_solicitud)); ?> </td>
                        <td><?php echo $ascensos->escalafon; ?> </td>
                        <td>
                            <?php if($ascensos->estatus_ascenso=="En Proceso"){ echo '<span class="label label-warning">'.$ascensos->estatus_ascenso.'</span>'; } ?>
                            <?php if($ascensos->estatus_ascenso=="Fase I"){ echo '<span class="label label-primary">'.$ascensos->estatus_ascenso.'</span>'; } ?>
                            <?php if($ascensos->estatus_ascenso=="Fase II"){ echo '<span class="label label-info">'.$ascensos->estatus_ascenso.'</span>'; } ?>
                            <?php if($ascensos->estatus_ascenso=="Culminada"){ echo '<span class="label label-success">'.$ascensos->estatus_ascenso.'</span>'; } ?>
                            <?php if($ascensos->estatus_ascenso=="Rechazada"){ echo '<span class="label label-danger">'.$ascensos->estatus_ascenso.'</span>'; } ?>
                        </td>
                        <td><?php echo $ascensos->ciudad; ?></td>

                        <td>
                        <a class="btn btn-outline btn-info" href="<?php echo constant ('URL') . "solicitar_ascenso/viewDetail/" . $ascensos->id_solicitud_ascenso. "," . $_SESSION['id_docente'];?>" role="button"><i class="fa fa-eye"></i> Ver</a>&nbsp;

                            <!-- <a class="btn btn-outline btn-danger" href="<?php echo constant('URL') . 'disenoucdocente/deleteDiseno/' . $diseno_uc_docente->id_diseno_uc;?>" role="button"> Remover</a> &nbsp; -->
                            <!-- <button class="btn btn-outline btn-danger bEliminar" data-id="<?php echo $diseno_uc_docente->id_diseno_uc;?>"><i class="fa fa-trash"></i> Eliminar</button> -->
                        </td>
                    </tr>
                            <?php }?>
                    </tbody>
                    </table>
                        <!--  contenido -->

                            </div>
                    </div>
                </div>
            </div>
            </div>
        </div>



<?php require 'views/footer.php'; ?>

        </div>
       
        
    </div>

   
       <!-- datatables Scripts -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>

   

</body>
</html>
