<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Detalle Solicitud de Ascenso | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
    <!-- jasny input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <!-- sweetalert input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

</head>

<body>
 
   <?php require 'views/header.php'; ?>
   

   <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-7">
                    <h2>Detalle Ascenso </h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>solicitar_ascenso/viewAdmin">Lista de Ascensos</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <a href=""><strong>Detalle Solicitud</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-5">
                    <div class="title-action">
                        <?php switch($_SESSION['id_perfil']){
                                case 1:
                            ?>
                            <a href="<?php echo constant ('URL');?>solicitar_ascenso" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Volver</a>
                            <?php break;
                                default:
                                ?>
                                <a href="<?php echo constant ('URL');?>solicitar_ascenso/viewAdmin" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Volver</a>
                                <?php
                                break;
                        } ?>
                    </div>
                </div>
            </div>

            <?php  

if($this->solicitud->id_estatus_ascenso==5){?>
<div  class="alert alert-warning">
    
    <h1><i  class="fa fa-info"></i> Esta Solicitud De Ascenso ha sido Declinada. </h1>
    <br>
    <p>
        Su Solicitud ha sido Declinada debido a: <br>
        <?php echo ucwords($this->motivo->descripcion);?>
    </p>
</div>

<?php exit(); } ?>
        <div class="wrapper wrapper-content animated fadeInRight ">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h2>
                            SOLICITUD DE ASCENSO 
                            <?php if($this->solicitud->estatus_ascenso=='En Proceso'){ ?>
                                <span id="est" style="font-size: 20px;" class="label label-warning">
                                    <?php echo $this->solicitud->estatus_ascenso; ?>
                                </span>
                            <?php } ?>
                            <?php if($this->solicitud->estatus_ascenso=='Fase I'){ ?>
                                <span id="est" style="font-size: 20px;" class="label label-primary">
                                    <?php echo $this->solicitud->estatus_ascenso; ?>
                                </span>
                            <?php } ?>
                            <?php if($this->solicitud->estatus_ascenso=='Fase II'){ ?>
                                <span id="est" style="font-size: 20px;" class="label label-info">
                                    <?php echo $this->solicitud->estatus_ascenso; ?>
                                </span>
                            <?php } ?>
                            <?php if($this->solicitud->estatus_ascenso=='Culminada'){ ?>
                                <span id="est" style="font-size: 20px;" class="label label-success">
                                    <?php echo $this->solicitud->estatus_ascenso; ?>
                                </span>
                            <?php } ?>
                            <?php if($this->solicitud->estatus_ascenso=='Rechazada'){ ?>
                                <span id="est" style="font-size: 20px;" class="label label-danger">
                                    <?php echo $this->solicitud->estatus_ascenso; ?>
                                </span>
                            <?php } ?>
                        </h2>
                        <div class="ibox-tools">

                            <?php 
                            //$this->solicitud->id_estatus_ascenso=1;
                            //$this->estatus_pto_cuentaR->estatus='En Espera';
                            //$this->estatus_memoR2->estatus='En Espera';

                            

                            //Si el perfil es diferente de trabajador academico mostrar boton de detalle
                            /// nuevaaa solicitud ascensos
                            if($_SESSION['id_perfil']!=1){
                                ?>
                                <a  target="_blank" href="<?php echo constant ('URL') . "verificar/detalle/" . $this->solicitud->id_docente;?>" role="button"><button class="btn btn-info"><i class="fa fa-info-circle"></i> Ver Experiencia Laboral</button></a>&nbsp;
                                <?php 
                            }
                            
                            if($this->estatus_propuesta=="En Espera" || $this->estatus_propuesta=="Verificado"){
                                ?>
                                <a href="<?php echo constant ('URL')."solicitar_ascenso/viewCuadroJ/".$this->solicitud->id_solicitud_ascenso.",".$this->solicitud->id_docente;?>" role="button"><button class="btn btn-success"><i class="fa fa-eye"></i> Ver Jurado</button></a>
                                <?php

                            }
                            

                            
                            
                            switch($this->solicitud->id_estatus_ascenso){
                                case 1://"Proceso para verificar Solicitud"
                                    
                                    //por ahora nada
                                    
                                    
                                break;
                                case 2://"Proceso para Avanzar en la propuesta del jurado";
                                    //si falta alguna verificacion oculto los botones de propuesta de jurado
                                    // el perfil nro 2 es el Coord del CE 
                                    
                                        //si esta variable esta vacia la propuesta aun no existe, por ende muestro el de proponer jurado
                                        if(empty($this->estatus_propuesta)){
                                            if($_SESSION['id_perfil']==2){//perifl 2: perfil de Coordinador del CE
                                            ?>
                                            <a href="<?php echo constant ('URL')."solicitar_ascenso/render2/".$this->solicitud->id_solicitud_ascenso.",".$this->solicitud->id_docente;?>" role="button"><button class="btn btn-primary"><i class="fa fa-gavel"></i> Proponer Jurado</button></a>
                                        <?php }
                                        }
                                        
                                        

                                        
                                    
                                    
                                    if($_SESSION['id_perfil']==3){
                                        //si la propuesta esta verificada y estatus_memo1 esta vacio procedemos a insertar el memo uno
                                        if($this->estatus_propuesta=="Verificado" && empty($this->estatus_memo1->estatus)){
                                        ?>
                                        
                                        <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#myModal6"><i class="fa fa-upload"></i> Adjuntar Memo N°1</button>
                                                    <div class="modal inmodal" id="myModal6" tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                        <div class="modal-content animated bounceInRight panel panel-primary">
                                                                <div class="modal-header panel-heading">
                                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                                                    <h4 class="modal-title"><i class="fa fa-paperclip"></i> Adjuntar Memorandum N°1</h4>
                                                                    <small class="font-bold">Adjunta el Memoradum para continuar.</small>
                                                                </div>
                                                                <div class="modal-body text-center ">
                                                                <form id="form-trabajo" enctype="multipart/form-data" action="<?php echo constant ('URL') . "solicitar_ascenso/adjuntarMemo/".$this->solicitud->id_solicitud_ascenso.",10";?>" method="post">
                                                                    <div style="margin: 16px;" class="form-group jumbotron">
                                                                        <label>Cargar Memorandum <span style="color: red;">*</span></label>
                                                                        <div class="form-group">
                                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                <span class="btn btn-default btn-file"><span class="fileinput-new">Seleccionar Archivo</span>
                                                                                <span class="fileinput-exists">Cambiar</span><input type="file" id="trabajo_ascenso" accept=".pdf" class="required"  name="trabajo_ascenso"/></span>
                                                                                <span class="fileinput-filename"></span>
                                                                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                                                            </div>
                                                                        </div>
                                                                        <label id="trabajo_ascenso-error" class="error" for="trabajo_ascenso"></label>
                                                                    <script>
                                                                        
                                                                    </script>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-paperclip"></i> Adjuntar</button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                        <?php
                                        }
                                    }

                                    if($_SESSION['id_perfil']==4 || $_SESSION['id_perfil']==5){
                                        
                                        
                                        if($this->estatus_memo1->estatus=="Verificado"){

                                            if(empty($this->estatus_memoS2->estatus)){
                                        ?>
                                        
                                                    <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#myModal7"><i class="fa fa-paperclip"></i> Adjuntar</button>
                                                    <div class="modal inmodal" id="myModal7" tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content animated bounceInRight panel panel-primary">
                                                                <div class="modal-header panel-heading">
                                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                                                    <h4 class="modal-title"><i class="fa fa-paperclip"></i> Adjuntar</h4>
                                                                    <small class="font-bold">Adjunta el Memoradum y Punto de Cuenta para continuar.</small>
                                                                </div>
                                                                <div class="modal-body text-center ">
                                                                <form id="form-mp1" enctype="multipart/form-data" action="<?php echo constant ('URL') . "solicitar_ascenso/adjuntarMemoPuntoCuenta/".$this->solicitud->id_solicitud_ascenso.",1";?>" method="post">
                                                                    <div style="margin: 16px;" class="row jumbotron">
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group ">
                                                                                <label>Cargar Memorandum N°2<span style="color: red;">*</span></label>
                                                                                <div class="form-group">
                                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                        <span class="btn btn-default btn-file"><span class="fileinput-new">Seleccionar Archivo</span>
                                                                                        <span class="fileinput-exists">Cambiar</span><input type="file" accept=".pdf" class="required" id="memo_solicitud_2" name="memo_solicitud_2"/></span>
                                                                                        <span class="fileinput-filename"></span>
                                                                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                                                                        <label id="memo_solicitud_2-error" class="error" for="memo_solicitud_2"></label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group ">
                                                                                <label>Cargar Punto de Cuenta N°1<span style="color: red;">*</span></label>
                                                                                <div class="form-group">
                                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                        <span class="btn btn-default btn-file"><span class="fileinput-new">Seleccionar Archivo</span>
                                                                                        <span class="fileinput-exists">Cambiar</span><input type="file" accept=".pdf" class="required" id="pto_cuenta_solicitud1" name="pto_cuenta_solicitud1"/></span>
                                                                                        <span class="fileinput-filename"></span>
                                                                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                                                                        <label id="pto_cuenta_solicitud1-error" class="error" for="pto_cuenta_solicitud1"></label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-paperclip"></i> Adjuntar</button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                    <?php }
                                    }
                                } 
                                    
                                break;
                                case 3://"Proceso para Avanzar en el Resultado de la Defensa";
                                    
                                    
                                    if($_SESSION['id_perfil']==2){
                                        if(empty($this->estatus_actaD->estatus)){
                                        ?>
                                        <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#myModal8"><i class="fa fa-paperclip"></i> Adjuntar</button>
                                                    <div class="modal inmodal" id="myModal8" tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content animated bounceInRight panel panel-primary">
                                                                <div class="modal-header panel-heading">
                                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                                                    <h4 class="modal-title"><i class="fa fa-paperclip"></i> Adjuntar</h4>
                                                                    <small class="font-bold">Adjunta el Acta de Defensa de la Solicitud para Continuar.</small>
                                                                </div>
                                                                <div class="modal-body text-center ">
                                                                <form id="form-actaD" enctype="multipart/form-data" action="<?php echo constant ('URL') . "solicitar_ascenso/adjuntarMemo/".$this->solicitud->id_solicitud_ascenso.",15";?>" method="post">
                                                                    <div style="margin: 16px;" class="row jumbotron">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group ">
                                                                                <label>Cargar Acta de Defensa<span style="color: red;">*</span></label>
                                                                                <div class="form-group">
                                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                        <span class="btn btn-default btn-file"><span class="fileinput-new">Seleccionar Archivo</span>
                                                                                        <span class="fileinput-exists">Cambiar</span><input type="file" accept=".pdf" class="required" id="acta_defensa" name="acta_defensa"/></span>
                                                                                        <span class="fileinput-filename"></span>
                                                                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                                                                        <label id="acta_defensa-error" class="error" for="acta_defensa"></label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-paperclip"></i> Adjuntar</button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                        <?php
                                        }
                                    }
                                    if($_SESSION['id_perfil']==3){//perfil de Director de CE
                                        //si el estatus del acta de defensa es "verificado" y el estatus de memorandum 1 esta vacio muestrame el boton
                                        if($this->estatus_actaD->estatus=="Verificado" && empty($this->estatus_memoR1->estatus)){
                                             ?>
                                            <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#myModal9"><i class="fa fa-paperclip"></i> Adjuntar Memo Resolución</button>
                                                    <div class="modal inmodal" id="myModal9" tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content animated bounceInRight panel panel-primary">
                                                                <div class="modal-header panel-heading">
                                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                                                    <h4 class="modal-title"><i class="fa fa-paperclip"></i> Adjuntar</h4>
                                                                    <small class="font-bold">Adjunta el Memorandum de Acta de Defensa N°1 de la Solicitud para Continuar.</small>
                                                                </div>
                                                                <div class="modal-body text-center ">
                                                                <form id="form-mR1" enctype="multipart/form-data" action="<?php echo constant ('URL') . "solicitar_ascenso/adjuntarMemo/".$this->solicitud->id_solicitud_ascenso.",13";?>" method="post">
                                                                    <div style="margin: 16px;" class="row jumbotron">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group">
                                                                                <label>Cargar Memorandum de Acta de Defensa N°1<span style="color: red;">*</span></label>
                                                                                <div class="form-group">
                                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                        <span class="btn btn-default btn-file"><span class="fileinput-new">Seleccionar Archivo</span>
                                                                                        <span class="fileinput-exists">Cambiar</span><input type="file" accept=".pdf" class="required" id="memo_resolucion1" name="memo_resolucion1"/></span>
                                                                                        <span class="fileinput-filename"></span>
                                                                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                                                                        <label id="memo_resolucion1-error" class="error" for="memo_resolucion1"></label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-paperclip"></i> Adjuntar</button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                    <?php }
                                    }
                                    
                                    if($_SESSION['id_perfil']==4 || $_SESSION['id_perfil']==5){//4: analista dgta, 5: admin dgta 
                                        
                                        //me quede aqui ////////////////////////////
                                        //si el estatus de memorandum acta defensa es "verificado" y el estatus del memo de resolucion 2 esta vacio o el punto de cuenta de resolucion esta vacio muestro el boton a adjuntar
                                        if($this->estatus_memoR1->estatus=="Verificado"){
                                            if(empty($this->estatus_memoR2->estatus) && empty($this->estatus_pto_cuentaR->estatus)){?>
                                                <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#myModal10"><i class="fa fa-paperclip"></i> Adjuntar</button>
                                                    <div class="modal inmodal" id="myModal10" tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content animated bounceInRight panel panel-primary">
                                                                <div class="modal-header panel-heading">
                                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                                                    <h4 class="modal-title"><i class="fa fa-paperclip"></i> Adjuntar</h4>
                                                                    <small class="font-bold">Adjunta el Memorandum de Acta de Defensa N°2 y el Pto. de Cuenta N°2 de la Solicitud para Continuar.</small>
                                                                </div>
                                                                <div class="modal-body text-center ">
                                                                <form id="form-mR2" enctype="multipart/form-data" action="<?php echo constant ('URL') . "solicitar_ascenso/adjuntarMemoPuntoCuenta/".$this->solicitud->id_solicitud_ascenso.",2";?>" method="post">
                                                                    <div style="margin: 16px;" class="row jumbotron">
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group ">
                                                                                <label>Cargar Memorandum de Acta de Defensa N°2<span style="color: red;">*</span></label>
                                                                                <div class="form-group">
                                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                        <span class="btn btn-default btn-file"><span class="fileinput-new">Seleccionar Archivo</span>
                                                                                        <span class="fileinput-exists">Cambiar</span><input type="file" accept=".pdf" class="required" id="memo_resolucion_2" name="memo_solicitud_2"/></span>
                                                                                        <span class="fileinput-filename"></span>
                                                                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                                                                        <label id="memo_solicitud_2-error" class="error" for="memo_solicitud_2"></label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <div class="form-group ">
                                                                                <label>Cargar Punto de Cuenta N°2<span style="color: red;">*</span></label>
                                                                                <div class="form-group">
                                                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                        <span class="btn btn-default btn-file"><span class="fileinput-new">Seleccionar Archivo</span>
                                                                                        <span class="fileinput-exists">Cambiar</span><input type="file" accept=".pdf" class="required" id="pto_cuenta_resolucion1" name="pto_cuenta_solicitud1"/></span>
                                                                                        <span class="fileinput-filename"></span>
                                                                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                                                                        <label id="pto_cuenta_solicitud1-error" class="error" for="pto_cuenta_solicitud1"></label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-paperclip"></i> Adjuntar</button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                    <?php }//status
                                        }//vacio
                                    }
                                break;
                                case 4://culminada
                                
                                break;

                                
                            }
                            /// fin nueva solicitud ascensos
                        ?>
                            

                                                    
                        </div>
                    </div>
                    <!--<div class="ibox-content ">

                          contenido -->
                        
                        <!--  end contenido 

                    </div>-->
                </div>
            </div>
            </div>
            <div id="fase2"></div>
           <?php echo $this->mensaje;


            //si hay una propuesta de jurado registrada y el estatus es en espera mostramos este mensaje al perfil 3: coord. del Centro de Estudios
                if($this->estatus_propuesta=='En Espera'){
                    if($_SESSION['id_perfil']==3){
                        ?>
                        <div class="alert alert-warning">
                            Ya existe una Propuesta de Jurado Registrada. Para Visualizarla, haz <b>Click</b> en el botón 
                            <a class="alert-link" href="<?php echo constant ('URL')."solicitar_ascenso/viewCuadroJ/".$this->solicitud->id_solicitud_ascenso.",".$this->solicitud->id_docente;?>">
                            Ver Jurado
                        </a>.</div>
                    <?php }
                }

                if($this->estatus_propuesta=='Verificado'){
                        ?>
                        <div class="alert alert-warning">
                           La Propuesta de Jurado ya ha sido Verificada. Para Visualizarla, haz <b>Click</b> en el botón 
                            <a class="alert-link" href="<?php echo constant ('URL')."solicitar_ascenso/viewCuadroJ/".$this->solicitud->id_solicitud_ascenso.",".$this->solicitud->id_docente;?>">
                            Ver Jurado</a>.
                        </div>
                    <?php 
                }
                if($this->estatus_actaD->estatus=='Verificado'){
                    ?>
                    <div class="alert alert-warning">
                       El Acta de Defensa ya ha sido Verificada por el Director del CE. Para Visualizarla Haz <a href="#doc_sol" class="alert-link">Click Aquí</a> ó <b>Desliza el Mouse <i class="fa fa-arrow-circle-down"></i></b> 
                    </div>
                <?php 
                }
            ?>
            <div class="row">
            
                <div class="col-lg-12">
                    <div class="contact-box">
                        <div class="row">
                        <div class="col-lg-4">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <i class="fa fa-user-circle rounded-circle m-t-xs img-fluid"></i>
                                    Trabajador Académico
                                </div>
                                <div class="panel-body">
                                <h3>
                                    <strong><?php echo ucwords($this->solicitud->primer_apellido) ." ".
                                                    ucwords($this->solicitud->segundo_apellido).", ".
                                                    ucwords($this->solicitud->primer_nombre)." ".
                                                    ucwords($this->solicitud->primer_apellido);?>
                                    </strong>
                                </h3>
                                <p><strong>Identificación:</strong><br><i class="fa fa-address-card"></i> <?php echo ucwords($this->solicitud->identificacion);?></p>
                                <p><strong>Eje Geopolítico Regional:</strong><br><i class="fa fa-map-marker"></i> <?php echo ucwords($this->solicitud->eje_regional);?></p>

                                <p><strong>Centro de Estudio:</strong><br><i class="fa fa-university"></i> <?php echo ucwords($this->solicitud->centro_estudio);?></p>
                                <p><strong>Área Académica:</strong><br><i class="fa fa-area-chart"></i> <?php echo ucwords($this->solicitud->area_conocimiento_opsu);?></p>
                                
                                <?php if($_SESSION['id_perfil']==1){?>
                                <form target="_blank" action="<?php echo constant('URL');?>views/ascensos/print_pdf.php" method="post">
                                        <input type="hidden" name="primer_nombre" value="<?php echo $this->docente->primer_nombre;?>">
                                        <input type="hidden" name="segundo_nombre" value="<?php echo $this->docente->segundo_nombre;?>">
                                        <input type="hidden" name="primer_apellido" value="<?php echo $this->docente->primer_apellido;?>">
                                        <input type="hidden" name="segundo_apellido" value="<?php echo $this->docente->segundo_apellido;?>">
                                        <input type="hidden" name="nacionalidad" value="<?php echo $this->docente->nacionalidad;?>">
                                        <input type="hidden" name="identificacion" value="<?php echo $this->docente->identificacion;?>">
                                        <input type="hidden" name="escalafon" value="<?php echo $this->docente->escalafon;?>">
                                        <input type="hidden" name="centro_estudio" value="<?php echo $this->docente->centro_estudio;?>">
                                        <input type="hidden" name="fecha_solicitud" value="<?php echo $this->docente->fecha_solicitud;?>">
                                        <input type="hidden" name="ciudad" value="<?php echo $this->docente->ciudad;?>">
                                        <input type="hidden" name="fecha_consulta" value="<?php echo $this->docente->fecha_consulta;?>">
                                        <input type="hidden" name="escalafon_siguiente" value="<?php echo $this->docente->escalafon_siguiente;?>">

                                        <button class="btn btn-success" type="submit" name="pdf">Carta de Solicitud</button>

                                    </form>
                            <?php } ?>
                                </div>
                                
                            </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="panel panel-primary">
                                        <div class="panel-heading">
                                            <i class="fa fa-list"></i> Estatus de La Solicitud
                                        </div>
                                        <div class="panel-body tooltip-demo">
                                        <form id="form" method="post" >
                                            <div class="row">
                                                <div class="col-lg-6">
                                                        <div class="checkbox checkbox-primary">
                                                        <input id="checkbox2" class="required" name="trabajo_ascenso" type="checkbox">
                                                        <!-- style="text-decoration: line-through;"-->
                                                        <label for="checkbox2" >
                                                            Trabajo de Ascenso
                                                        </label>
                                                    </div>
                                                    <label id="trabajo_ascenso-error" class="error" for="trabajo_ascenso" style=""></label>

                                                    <div class="checkbox checkbox-primary">
                                                        <input id="checkbox3" class="required" name="acta_pida" type="checkbox">
                                                        <label for="checkbox3">
                                                        Acta de Presentación del PIDA
                                                        </label>
                                                        
                                                    </div> 
                                                    <label id="acta_pida-error" class="error" for="acta_pida" style=""></label>

                                                    <div class="checkbox checkbox-primary">
                                                        <input id="checkbox4" class="required" name="acta_proyecto" type="checkbox">
                                                        <label for="checkbox4">
                                                        Acta de Presentación del Proyecto
                                                        </label>
                                                        
                                                    </div>
                                                    <label id="acta_proyecto-error" class="error" for="acta_proyecto" style=""></label>
                                                    <?php 

                                                    if($this->solicitud->id_escalafon == 2 
                                                    || $this->solicitud->id_escalafon == 3
                                                    || $this->solicitud->id_escalafon == 4
                                                    || $this->solicitud->id_escalafon == 5
                                                    || $this->solicitud->id_escalafon == 6){?>
                                                    <div class="checkbox checkbox-primary">
                                                        <input id="checkbox6" class="required" name="resolucion" type="checkbox">
                                                        <label for="checkbox6">
                                                        Acta de Resolución del Último Ascenso
                                                        </label>
                                                        
                                                    </div>
                                                    <label id="resolucion-error" class="error" for="resolucion" style=""></label>

                                                    <?php } ?>
                                                    <div class="checkbox checkbox-primary">
                                                        <input id="checkbox5" name="requisitos" type="checkbox">
                                                        <label for="checkbox5">
                                                        Requisitos de Experiencia Laboral
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                <?php if($this->estatus_propuesta=='Verificado'){?>
                                                <div class="checkbox checkbox-primary">
                                                <input id="checkbox7" name="propuesta" type="checkbox">
                                                <label for="checkbox7">
                                                Propuesta de Jurado
                                                </label>
                                                </div>
                                            <?php } 
                                             if($this->estatus_memo1->estatus=='Verificado'){?>
                                                <div class="checkbox checkbox-primary">
                                                <input id="checkbox9" name="" type="checkbox">
                                                <label for="checkbox9">
                                                Memorandum de Solicitud N°1
                                                </label>
                                                </div>
                                            <?php } 
                                             if($this->estatus_memoS2->estatus=='Verificado'){?>
                                                <div class="checkbox checkbox-primary">
                                                <input id="checkbox11" name="" type="checkbox">
                                                <label for="checkbox11">
                                                    Memorandum de Solicitud N°2
                                                </label>
                                                </div>
                                            <?php } ?>
                                            <?php if($this->estatus_pto_cuentaS1->estatus=='Verificado'){?>
                                                <div class="checkbox checkbox-primary">
                                                <input id="checkbox12" name="" type="checkbox">
                                                <label for="checkbox12">
                                                    Pto. de Cuenta de Solicitud N°1
                                                </label>
                                                </div>
                                            <?php } ?>
                                            <?php if($this->estatus_actaD->estatus=='Verificado'){?>
                                                <div class="checkbox checkbox-primary">
                                                <input id="checkbox13" name="" type="checkbox">
                                                <label for="checkbox13">
                                                    Acta de Defensa
                                                </label>
                                                </div>
                                            <?php } ?>
                                            <?php if($this->estatus_memoR1->estatus=='Verificado'){?>
                                                <div class="checkbox checkbox-primary">
                                                <input id="checkbox14" name="" type="checkbox">
                                                <label for="checkbox14">
                                                    Memorandum de Resolución N°1
                                                </label>
                                                </div>
                                            <?php } ?>
                                            <?php if($this->estatus_memoR2->estatus=='Verificado'){?>
                                                <div class="checkbox checkbox-primary">
                                                <input id="checkbox15" name="" type="checkbox">
                                                <label for="checkbox15">
                                                    Memorandum de Resolución N°2
                                                </label>
                                                </div>
                                            <?php } ?>
                                            <?php if($this->estatus_pto_cuentaR->estatus=='Verificado'){?>
                                                <div class="checkbox checkbox-primary">
                                                <input id="checkbox16" name="" type="checkbox">
                                                <label for="checkbox16">
                                                    Punto de Cuenta de Resolución
                                                </label>
                                                </div>
                                            <?php } ?>
                                                </div>
                                            </div>
                                            

                                            
                                            <div style="margin-top: 15px;" class="form-group">
                                            <?php if($this->solicitud->id_estatus_ascenso==2){?>
                                                <div id="fase1" class="alert alert-warning">
                                                    <i class="fa fa-exclamation-triangle" style="font-size: 2em; margin-bottom: 15px;"> Importante</i> 
                                                    <ul>
                                                        
                                                    
                                                    <li>
                                                        La Solicitud Ha sido Trasladada <a class="alert-link" href="#">Exitosamente</a> a la Fase I. 
                                                    </li>
                                                        <?php if($_SESSION['id_perfil']==2 && empty($this->estatus_propuesta)){?>
                                                        <li>
                                                            Para Registrar la Propuesta de Jurado 
                                                            haz Click en botón 
                                                            <a href="<?php echo constant ('URL')."solicitar_ascenso/render2/".$this->solicitud->id_solicitud_ascenso.",".$this->solicitud->id_docente;?>" class="alert-link">
                                                            "Proponer Jurado"
                                                            </a>.
                                                        </li>
                                                        <?php } ?>

                                                <!-- mensaje para la el perfil 1: trabajador academico -->
                                                <?php if($_SESSION['id_perfil']==1 && $this->estatus_propuesta=="En Espera"){?>
                                                    <li>
                                                        La Propuesta de Jurado ha sido Registrada, 
                                                        se encuentra en espera de verificación. 
                                                        Para Visualizarla Haz Click en el botón "
                                                        <a class="alert-link" href="<?php echo constant ('URL')."solicitar_ascenso/viewCuadroJ/".$this->solicitud->id_solicitud_ascenso.",".$this->solicitud->id_docente;?>">
                                                        Ver Jurado
                                                    </a>".
                                                    </li>
                                                    <?php } ?>
                                                    
                                                <?php if($_SESSION['id_perfil']==1 && $this->estatus_propuesta=="Verificado"){?>
                                                    <li>
                                                        La Propuesta de Jurado ha sido Verificada. Para Visualizarla Haz Click en el botón "
                                                        <a class="alert-link" href="<?php echo constant ('URL')."solicitar_ascenso/viewCuadroJ/".$this->solicitud->id_solicitud_ascenso.",".$this->solicitud->id_docente;?>">
                                                        Ver Jurado
                                                        </a>".
                                                    </li>
                                                <?php } ?>

                                                <?php if($_SESSION['id_perfil']==2 && $this->estatus_propuesta=="En Espera"){?>
                                                    <li>
                                                        Ahora Comunícate con el <b>Director del CE</b> para Continuar con el Proceso.
                                                    </li>
                                                        <?php } ?>
                                                    
                                                <?php if($_SESSION['id_perfil']==2 && $this->estatus_propuesta=="Verificado"){?>
                                                    <li>
                                                        <b>Su Propuesta de Jurado ha sido Verificada.</b>
                                                    </li>
                                                <?php } ?>
                                                    

                                                <?php if($_SESSION['id_perfil']==4 || $_SESSION['id_perfil']==5 && $this->estatus_memo1->estatus=="En Espera"){?>
                                                    <li>
                                                        <b>Verifique el Memorandum N°1.</b>
                                                    </li>
                                                <?php } ?>
                                                <?php 
                                                
                                                if($this->estatus_memoS2->estatus=="En Espera"){
                                                    if($_SESSION['id_perfil']==4 || $_SESSION['id_perfil']==5){
                                                    ?>
                                                    <li>
                                                        Has Adjuntado El Memorandum de Solicitud N°2, Comunícate con Vicerrectorado para <b>continuar el proceso.</b>
                                                    </li>
                                                    <?php }else{?>
                                                    <li>
                                                        El<b> Memorandum de Solicitud N°2</b> esta en espera de Verificación.
                                                    </li>
                                                    <?php }
                                                }elseif($this->estatus_memoS2->estatus=="Verificado"){?>
                                                    <li>
                                                        El<b> Memorandum de Solicitud N°2</b> Ha sido Verificado.
                                                    </li>
                                                <?php 
                                                } 

                                                if($this->estatus_pto_cuentaS1->estatus=="En Espera"){
                                                    if($_SESSION['id_perfil']==4 || $_SESSION['id_perfil']==5){
                                                    ?>
                                                    <li>
                                                        Has Adjuntado Punto de Cuenta de la Solicitud, Comunícate con Vicerrectorado para <b>continuar el proceso.</b>
                                                    </li>
                                                    <?php }else{?>
                                                    <li>
                                                        El<b> Punto de Cuenta N°1</b> esta en espera de Verificación.
                                                    </li>
                                                    <?php }
                                                } elseif($this->estatus_pto_cuentaS1->estatus=="Verificado"){?>
                                                    <li>
                                                        El<b> Punto de Cuenta N°1</b> Ha sido Verificado.
                                                    </li>
                                                <?php 
                                                } ?>

                                                </ul>
                                                </div>
                                            <?php } ?>


                                            <?php if($this->solicitud->id_estatus_ascenso==3){//fase 2 de solicitud de ascenso  ?>
                                                <div id="fase1" class="alert alert-warning">
                                                <ul>
                                                    <i class="fa fa-exclamation-triangle" style="font-size: 2em; margin-bottom: 15px;"> Importante</i> 
                                                    <li>
                                                        La Solicitud Ha sido Trasladada <a class="alert-link" href="#">Exitosamente</a> a la Fase II (Resultado de Defensa). 
                                                    </li>
                                                    <?php if(!empty($this->estatus_actaD->estatus)){// si esta vacio
                                                            if($this->estatus_actaD->estatus=="En Espera"){//si esta en espera
                                                                if($_SESSION['id_perfil']==2){?>
                                                    <li>
                                                        Haz Adjuntado el Acta de Defensa, Comunícate con el Director del CE para continuar con el Proceso.
                                                    </li>
                                                    <?php   }
                                                     
                                                        if($_SESSION['id_perfil']==3){?>
                                                        <li>
                                                            Tienes el Acta de Defensa Por Verificar
                                                        </li>
                                                    <?php }
                                                        }elseif($this->estatus_actaD->estatus=="Verificado"){ ?>
                                                            
                                                            <li>
                                                                Acta de Defensa Verificada
                                                            </li>
                                                           <?php  if($_SESSION['id_perfil']==3){?>
                                                                <li>
                                                                    Para Adjuntar el Memorandum de Resolución N°1, haz click en el boton "<b><i class="fa fa-paperclip"></i> Adjuntar Memo Resolución</b>".
                                                                </li>
                                                            <?php } 
                                                            
                                                         }
                                                        } else{ 
                                                            
                                                        if($_SESSION['id_perfil']==2){?>

                                                        <li>
                                                            Presiona el botón "<b>Adjuntar</b>" para Cargar El Acta de Defensa del Docente.
                                                        </li>
                                                    <?php } 
                                                    } ?>

                                                    

                                                    <?php if(!empty($this->estatus_memoR1->estatus)){// si esta vacio
                                                            if($this->estatus_memoR1->estatus=="En Espera"){//si esta en espera
                                                                if($_SESSION['id_perfil']==3){?>
                                                    <li>
                                                        Haz Adjuntado el Memorandum de Resolución N°1, Comunícate con la DGTA para continuar con el Proceso.
                                                    </li>
                                                    <?php   }
                                                     
                                                        if($_SESSION['id_perfil']==4 || $_SESSION['id_perfil']==5){?>
                                                        <li>
                                                            Tienes el Memorandum de Resolución N°1 Por Verificar
                                                        </li>
                                                    <?php }
                                                        }elseif($this->estatus_memoR1->estatus=="Verificado"){ 
                                                            ?>
                                                            <li>
                                                                El Memorandum de Resolución N°1 del Docente ha sido Verificado.
                                                            </li>
                                                        <?php  
                                                        }

                                                    }?>

                                                    <?php if(!empty($this->estatus_memoR2->estatus)){// si esta vacio
                                                            if($this->estatus_memoR2->estatus=="En Espera"){//si esta en espera
                                                                if($_SESSION['id_perfil']==4 || $_SESSION['id_perfil']==5){?>
                                                    <li>
                                                        Haz Adjuntado el Memorandum N°2 y El Punto de Cuenta de la Resolución, Comunícate con Vicerrectorado para continuar con el Proceso.
                                                    </li>
                                                    <?php   }
                                                     
                                                        if($_SESSION['id_perfil']==6){?>
                                                        <li>
                                                            Tienes el Memorandum N°2 y El Punto de Cuenta de la Resolución Por Verificar.
                                                        </li>
                                                    <?php }
                                                           if($_SESSION['id_perfil']!=4 && $_SESSION['id_perfil']!=5 && $_SESSION['id_perfil']!=6){
                                                               //aqui mensaje de 
                                                               ?>
                                                        <li>
                                                           Puedes Vizualizar El Memorandum N°2 y El Punto de Cuenta de la Resolución, en la sección Documentos de la Solicitud.
                                                        </li>
                                                    <?php
                                                           } 
                                                        }else{ 
                                                            if($_SESSION['id_perfil']==4 || $_SESSION['id_perfil']==5){?>
    
                                                            <li>
                                                                Presiona el botón "<b>Adjuntar</b>" para Cargar El Memorandum N°2 y El Punto de Cuenta de la Resolución del Docente.
                                                            </li>
                                                        <?php } 
                                                        } 

                                                    } ?>
                                                </ul>
                                                </div>
                                            <?php }//fin fase 3 
                                               
                                                if($this->solicitud->id_estatus_ascenso==4){?>
                                                <div id="fase1" class="alert alert-success">
                                                    La Solicitud de Ascenso ha sido Atendida Satisfactoriamente.
                                                </div>
                                                <?php }

                                                if($this->solicitud->id_estatus_ascenso==1){ 
                                                    if($this->titulo_doctor->estatus=="En Espera"){
                                                        if($_SESSION['id_perfil']==2){?>
                                                        <div id="fase1" class="alert alert-warning">
                                                            <i class="fa fa-info"></i> Desliza hacía Abajo para Verificar el Título de Doctor.
                                                        </div>
                                                    <?php }
                                                    }?>
                                                    
                                                    <button type="submit" class="btn btn-block btn-primary" id="btnV" ><i class="fa fa-check"></i> Verificar</button>
                                                    <!-- <a id="" href="<?php echo constant('URL').'solicitar_ascenso/updateF2/'.$this->solicitud->id_solicitud_ascenso.",5";?>" onclick="return confirm('Si Declina una todos sus datos se Perderan, ¿Está Seguro de Declinar Esta Solicitud?');">
                                                        
                                                    </a>-->
                                                    <?php if($_SESSION['id_perfil']==2){?>
                                                    <a href="" class="btn btn-outline btn-block btn-danger" data-toggle="modal" data-target="#btnD"><i class="fa fa-times"></i> Declinar Solicitud</a>
                                                    

                                                <?php }
                                                } ?>

                                            </div>
                                        </form>
                                        <div class="modal inmodal" id="btnD" tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content animated bounceInRight panel panel-warning">
                                                            

                                                                <div class="modal-header panel-heading">
                                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                                                    <h4 class="modal-title"><i class="fa fa-exclamation-triangle"></i> Declinar Solicitud</h4>
                                                                    <small class="font-bold">Completa el Formulario para declinar la Solicitud.</small>
                                                                </div>
                                                                <div class="modal-body text-center">
                                                                <form id="form-D" action="<?php echo constant ('URL') . "solicitar_ascenso/declinar/".$this->solicitud->id_solicitud_ascenso.",".$this->solicitud->id_docente;?>" method="post">
                                                                    <div style="margin: 16px;" class="row jumbotron">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group ">
                                                                                <label>¿Por qué decide Declinar la Solicitud?<span style="color: red;">*</span></label>
                                                                                <div class="form-group">
                                                                                    <textarea name="motivo" id="motivo" maxlength="250" cols="30" rows="5" placeholder="Ingrese el Motivo por el Cuál se Declina la Solicitud." class="form-control required"></textarea>
                                                                                </div>
                                                                                <!-- tipo de declinacion de propuesta de jurado-->
                                                                                <input type="hidden" name="tipo_declinar" value="1">

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                                                    <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i> Declinar</button>
                                                                    </form>
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                        </div>
                                    </div>
                            </div>
                    </div>
                    </div>
                </div>
                <div id="doc_sol" class="col-lg-12 contact-box text-center">
                    <h2>DOCUMENTOS DE LA SOLICITUD</h2>
                </div>
            <div class="col-lg-4">
            
                <div class="contact-box">
                
                    <div class="row">
                    
                    <div class="col-lg-12">
                        <div class="text-center">
                            <i class="fa fa-file-pdf-o" style="font-size: 100px;"></i>
                            <div class="m-t-xs font-bold">ACTA RESOLUCIÓN PIDA</div>
                        </div>
                    </div>
                    <div class="col-lg-12"><br>
                        <div class="form-group text-center">
                            <a target="_blank" id="pida" onclick="verificar(this.value)" data-value="1" href="<?php echo constant('URL') . "solicitar_ascenso/viewDocumento/" . $this->solicitud->id_solicitud_ascenso.','.$this->acta_pida->id_tipo_documento;?>" class="btn btn-primary"><i class="fa fa-eye"></i> Ver Documento</a>&nbsp;
                            <a  target="_blank" href="<?php echo constant('URL').'src/documentos/ascensos/'.$this->acta_pida->acta_pida;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>                                        
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="contact-box">
                    <div class="row">
                    
                    <div class="col-lg-12">
                        
                        <div class="text-center">
                            <i class="fa fa-file-pdf-o" style="font-size: 100px;"></i>
                            <div class="m-t-xs font-bold">
                                TRABAJO DE ASCENSO 
                                <h3><?php echo ucwords($this->trabajo_ascenso->titulo);?></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12"><br>
                        <div class="form-group text-center">
                            <a target="_blank" href="<?php echo constant('URL') . "solicitar_ascenso/viewDocumento/" . $this->solicitud->id_solicitud_ascenso . ',' . $this->trabajo_ascenso->id_tipo_documento;?>" class="btn btn-primary"><i class="fa fa-eye"></i> Ver Documento</a>&nbsp;
                            <a  target="_blank" href="<?php echo constant('URL').'src/documentos/ascensos/'. $this->trabajo_ascenso->trabajo_ascenso;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>                                        
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="contact-box">
                    <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center">
                            <i class="fa fa-file-pdf-o" style="font-size: 100px;"></i>
                            <div class="m-t-xs font-bold">ACTA PROYECTO</div>
                        </div>
                    </div>
                    <div class="col-lg-12"><br>
                        <div class="form-group text-center">
                            <a target="_blank" href="<?php echo constant('URL') . "solicitar_ascenso/viewDocumento/" . $this->acta_proyecto->id_solicitud_ascenso.','.$this->acta_proyecto->id_tipo_documento;?>" class="btn btn-primary"><i class="fa fa-eye"></i> Ver Documento</a>&nbsp;
                            <a  target="_blank" href="<?php echo constant('URL').'src/documentos/ascensos/'.$this->acta_proyecto->acta_proyecto;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>                                        
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            <?php 
            
            if($this->solicitud->id_escalafon == 2 
            || $this->solicitud->id_escalafon == 3
            || $this->solicitud->id_escalafon == 4
            || $this->solicitud->id_escalafon == 5
            || $this->solicitud->id_escalafon == 6){?>
                <div class="col-lg-12">
                    <div class="contact-box">
                        <div class="row">
                        <div class="col-lg-12">
                            
                            <div class="text-center">
                                <i class="fa fa-file-pdf-o" style="font-size: 100px;"></i>
                                <div class="m-t-xs font-bold">ÚLTIMA ACTA DE RESOLUCIÓN DE ASCENSO</div>
                            </div>
                        </div>
                        <div class="col-lg-12"><br>
                            <div class="form-group text-center">
                                <a target="_blank" href="<?php echo constant('URL') . "solicitar_ascenso/viewDocumento/" . $this->resolucion_ascenso->id_solicitud_ascenso.','.$this->resolucion_ascenso->id_tipo_documento;?>" class="btn btn-primary"><i class="fa fa-eye"></i> Ver Documento</a>&nbsp;
                                <a  target="_blank" href="<?php echo constant('URL').'src/documentos/ascensos/'. $this->resolucion_ascenso->resolucion;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>                                        
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            <?php }
            //si el escalafon es igual 3 o 4 
                 /* 
                 MODIFICADO EL 20-03-2020: Debido a que los estudios se encuentran en la parte de verificación 
                 donde se podrán visualizar.


                 if($this->solicitud->id_escalafon == 3
                || $this->solicitud->id_escalafon == 4){ ?>
                    <div class="col-lg-8">
                        <div class="contact-box">
                            <div class="row">
                            <div class="col-lg-12">
                                
                                <div class="text-center">
                                    <i class="fa fa-mortar-board" style="font-size: 100px;"></i>
                                    <div class="m-t-xs font-bold">TÍTULO DE DOCTOR</div>
                                    <?php 
                                    
                                        if($this->titulo_doctor->estatus=='En Espera'){ 
                                            if($_SESSION['id_perfil']==2){?>
                                            <!-- le pasamos el id_tipo documento para editar todos los memos y puntos de cuenta a traves de esta tabla-->
                                            <form id="form-mDr" action="<?php echo constant('URL') . "solicitar_ascenso/updateMemo/".$this->solicitud->id_solicitud_ascenso.",".$this->titulo_doctor->id_tipo_documento;?>" method="post">
                                                <label id="titulo_doctor-error" class="error" for="titulo_doctor" style=""></label>
                                                <div class="alert alert-warning checkbox checkbox-primary">
                                                    <input id="checkbox23" name="titulo_doctor" class="required" type="checkbox">
                                                    <label for="checkbox23">
                                                        Título de Doctor
                                                    </label>
                                                    <div class="form-group">
                                                        <button class="btn btn-xs btn-success" type="submit"><i class="fa fa-check"></i> Verificar</button>
                                                    </div>
                                                </div>
                                            </form>
                                        <?php }
                                            }elseif($this->titulo_doctor->estatus=='Verificado'){?>
                                            <br>
                                                <p class="alert alert-success">Verificado por el Coord. del CE.</p>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-lg-12"><br>
                                <div class="form-group text-center">
                                    <a target="_blank" href="<?php echo constant('URL') . "solicitar_ascenso/viewDocumento/" . $this->solicitud->id_solicitud_ascenso.','.$this->titulo_doctor->id_tipo_documento;?>" class="btn btn-primary"><i class="fa fa-eye"></i> Ver Documento</a>&nbsp;
                                    <a  target="_blank" href="<?php echo constant('URL').'src/documentos/ascensos/'. $this->titulo_doctor->descripcion;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>                                        
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                <?php

                }*/
            
               if(!empty($this->estatus_memo1->estatus)){
            ?>

            <div class="col-lg-4">
                <div class="contact-box">
                    <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center">
                            <span class="label label-info float-right">¡Nuevo!</span><br> 
                            <i class="fa fa-file-pdf-o" style="font-size: 100px;"></i>
                            <div class="m-t-xs font-bold">Memorandum de Solicitud N°1</div>
                            
                           
                            <?php 
                            if($this->estatus_memo1->estatus=='En Espera'){
                                if($_SESSION['id_perfil']==4 || $_SESSION['id_perfil']==5){?>
                            <!-- le pasamos el id_tipo documento para editar todos los memos y puntos de cuenta a traves de esta tabla-->
                            <form id="form-mS1" action="<?php echo constant('URL') . "solicitar_ascenso/updateMemo/".$this->solicitud->id_solicitud_ascenso.",".$this->estatus_memo1->id_tipo_documento;?>" method="post">
                                <label id="memo_solicitud1-error" class="error" for="memo_solicitud1" style=""></label>
                                <div class="alert alert-warning tooltip-demo checkbox checkbox-primary">
                                    <input id="checkbox8" name="memo_solicitud1" class="required" type="checkbox">
                                    <label for="checkbox8">
                                        Memorandum de Solicitud N°1
                                    </label>
                                    <div class="form-group">
                                        <button class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="right" title="Antes de Verificar Visualiza El Documento" type="submit"><i class="fa fa-check"></i> Verificar</button>
                                    </div>
                                </div>
                            </form>
                            <?php }else{ ?><br>
                                <div class="alert alert-warning checkbox checkbox-primary">En Espera de Verificación de la DGTA</div>
                            <?php }
                            }elseif($this->estatus_memo1->estatus=='Verificado'){
                                ?>
                                <br>
                                    <p class="alert alert-success">Verificado por la DGTA.</p>
                                <?php
                            }
                         ?>
                        </div>
                    </div>
                    <div class="col-lg-12"><br>
                        <div class="form-group text-center">
                            <a target="_blank" href="<?php echo constant('URL') . "solicitar_ascenso/viewDocumento/" . $this->solicitud->id_solicitud_ascenso.','.$this->estatus_memo1->id_tipo_documento;?>" class="btn btn-primary"><i class="fa fa-eye"></i> Ver Documento</a>&nbsp;
                            <a  target="_blank" href="<?php echo constant('URL').'src/documentos/ascensos/'. $this->estatus_memo1->descripcion;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>&nbsp;                                        
                            <br><br>
                            <?php if($_SESSION['id_perfil']==3 && $this->estatus_memo1->estatus=='En Espera'){?>
                                
                            <button class="btn btn-block btn-xs btn-warning" 
                            data-toggle="modal" 
                            data-id_tipo_documento="<?php echo $this->estatus_memo1->id_tipo_documento;?>" 
                            data-tipo_documento="<?php echo $this->estatus_memo1->tipo_documento;?>" 
                            data-descripcion="<?php echo $this->estatus_memo1->descripcion;?>" 
                            data-target="#myModal11"><i class="fa fa-edit"></i> Cambiar Memo</button>
                            <?php } ?>
                                                    <div class="modal inmodal" id="myModal11" tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                        <div class="modal-content animated bounceInRight panel panel-primary">
                                                                <div class="modal-header panel-heading">
                                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                                                    <h4 class="modal-title"><i class="fa fa-paperclip"></i> Cambiar</h4>
                                                                    <small class="font-bold">Adjunta para continuar.</small>
                                                                </div>
                                                                <div class="modal-body text-center ">
                                                                <form id="form-trabajo2" enctype="multipart/form-data" action="<?php echo constant ('URL') . "solicitar_ascenso/cambiarArchivo/".$this->solicitud->id_solicitud_ascenso.",".$this->solicitud->id_docente;?>" method="post">
                                                                    <div style="margin: 16px;" class="form-group jumbotron">
                                                                        <label>Cambiar Documento <span style="color: red;">*</span></label>
                                                                        <input type="hidden" id="id_tipo_documento" name="id_tipo_documento">
                                                                        <input type="hidden" id="tipo_documento" name="tipo_documento">
                                                                        <input type="hidden" id="descripcion" name="descripcion">

                                                                        <div class="form-group">
                                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                                <span class="btn btn-default btn-file"><span class="fileinput-new">Seleccionar Archivo</span>
                                                                                <span class="fileinput-exists">Cambiar</span><input type="file" accept=".pdf" class="required" id="upt_file" name="upt_file"/></span>
                                                                                <span class="fileinput-filename"></span>
                                                                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                                                            </div>
                                                                        </div>
                                                                        <label id="upt_file-error" class="error" for="upt_file"></label>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-paperclip"></i> Adjuntar</button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                           
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            <?php } 
            if(!empty($this->estatus_memoS2->estatus)){
            ?>

            <div class="col-lg-4">
                <div class="contact-box">
                    <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center">
                        <span class="label label-info float-right">¡Nuevo!</span><br> 
                            <i class="fa fa-file-pdf-o" style="font-size: 100px;"></i>
                            
                            <div class="m-t-xs font-bold">Memorandum N°2</div>
                            <!-- le pasamos el id_tipo documento para editar todos los memos y puntos de cuenta a traves de esta tabla-->
                            
                            <?php 
                            if($this->estatus_memoS2->estatus=="En Espera"){
                                if($_SESSION['id_perfil']==6){?>
                                <form id="form-memoS2" action="<?php echo constant('URL') . "solicitar_ascenso/updateMemo/".$this->solicitud->id_solicitud_ascenso.",".$this->estatus_memoS2->id_tipo_documento;?>" method="post">
                                    <label id="memo_solicitud2-error" class="error" for="memo_solicitud2" style=""></label>
                                    <div class="alert alert-warning tooltip-demo checkbox checkbox-primary">
                                        <input id="checkbox22" name="memo_solicitud2" class="required"  type="checkbox">
                                        <label for="checkbox22">
                                            Memorandum de Solicitud N°2
                                        </label>
                                        <div class="form-group">
                                            <button class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="right" title="Antes de Verificar Visualiza El Documento" type="submit"><i class="fa fa-check"></i> Verificar</button>
                                        </div>
                                    </div>
                                </form>
                            <?php }else{ ?><br>
                                <div class="alert alert-warning checkbox checkbox-primary">En Espera de Verificación de Vicerrectorado.</div>
                            <?php }
                            }elseif($this->estatus_memoS2->estatus=="Verificado"){?>
                                <br>
                                <p class="alert alert-success">Verificado por Vicerrectorado.</p>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-lg-12"><br>
                        <div class="form-group text-center">
                        
                            <a target="_blank" href="<?php echo constant('URL') . "solicitar_ascenso/viewDocumento/" . $this->solicitud->id_solicitud_ascenso.','.$this->estatus_memoS2->id_tipo_documento;?>" class="btn btn-primary"><i class="fa fa-eye"></i> Ver Documento</a>&nbsp;
                            <a  target="_blank" href="<?php echo constant('URL').'src/documentos/ascensos/'.$this->estatus_memoS2->descripcion;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>                                        
                        <br><br>
                        <?php if(($_SESSION['id_perfil']==4 || $_SESSION['id_perfil']==5) && $this->estatus_memoS2->estatus=='En Espera'){?>
                        <button class="btn btn-block btn-xs btn-warning" 
                            data-toggle="modal" 
                            data-id_tipo_documento="<?php echo $this->estatus_memoS2->id_tipo_documento;?>" 
                            data-tipo_documento="<?php echo $this->estatus_memoS2->tipo_documento;?>" 
                            data-descripcion="<?php echo $this->estatus_memoS2->descripcion;?>" 
                            data-target="#myModal11"><i class="fa fa-edit"></i> Cambiar Memo</button>
                        <?php } ?>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            <?php } 
            if(!empty($this->estatus_pto_cuentaS1->estatus)){?>
            <div class="col-lg-4">
                <div class="contact-box">
                    <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center">
                            <span class="label label-info float-right">¡Nuevo!</span><br> 
                            <i class="fa fa-file-pdf-o" style="font-size: 100px;"></i>
                            
                            <div class="m-t-xs font-bold">Punto de Cuenta N°2</div>
                            <!-- le pasamos el id_tipo documento para editar todos los memos y puntos de cuenta a traves de esta tabla-->
                            <?php //si es estatus del pto de cuenta solicitud 1 es en espera muestro segun el perfil
                            if($this->estatus_pto_cuentaS1->estatus=="En Espera"){
                                if($_SESSION['id_perfil']==6){?>
                            <form id="form-pto1" action="<?php echo constant('URL') . "solicitar_ascenso/updateMemo/".$this->solicitud->id_solicitud_ascenso.",".$this->estatus_pto_cuentaS1->id_tipo_documento;?>" method="post">
                                <label id="pto_cuenta_solicitud1-error" class="error" for="pto_cuenta_solicitud1" style=""></label>
                                <div class="alert alert-warning tooltip-demo checkbox checkbox-primary">
                                    <input id="checkbox21" name="pto_cuenta_solicitud1" class="required" type="checkbox">
                                    <label for="checkbox21">
                                    Punto de Cuenta N°2
                                    </label>
                                    <div class="form-group">
                                        <button class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="right" title="Antes de Verificar Visualiza El Documento" type="submit"><i class="fa fa-check"></i> Verificar</button>
                                    </div>
                                </div>
                            </form>
                            <?php }else{ ?><br>
                                <div class="alert alert-warning checkbox checkbox-primary">En Espera de Verificación de Vicerrectorado</div>
                            <?php }
                            }elseif($this->estatus_pto_cuentaS1->estatus=="Verificado"){?>
                                <br>
                                <p class="alert alert-success">Verificado por Vicerrectorado</p>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-lg-12"><br>
                        <div class="form-group text-center">
                            <a target="_blank" href="<?php echo constant('URL') . "solicitar_ascenso/viewDocumento/" . $this->solicitud->id_solicitud_ascenso.','.$this->estatus_pto_cuentaS1->id_tipo_documento;?>" class="btn btn-primary"><i class="fa fa-eye"></i> Ver Documento</a>&nbsp;
                            <a  target="_blank" href="<?php echo constant('URL').'src/documentos/ascensos/'.$this->estatus_pto_cuentaS1->descripcion;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>                                        
                            <br><br>
                            <?php if(($_SESSION['id_perfil']==4 || $_SESSION['id_perfil']==5) && $this->estatus_pto_cuentaS1->estatus=='En Espera'){?>
                            <button class="btn btn-block btn-xs btn-warning" 
                                data-toggle="modal" 
                                data-id_tipo_documento="<?php echo $this->estatus_pto_cuentaS1->id_tipo_documento;?>" 
                                data-tipo_documento="<?php echo $this->estatus_pto_cuentaS1->tipo_documento;?>" 
                                data-descripcion="<?php echo $this->estatus_pto_cuentaS1->descripcion;?>" 
                                data-target="#myModal11"><i class="fa fa-edit"></i> Cambiar Punto de Cuenta</button>
                            <?php } ?>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            
            
        <?php if($this->solicitud->id_estatus_ascenso==3 || $this->solicitud->id_estatus_ascenso==4){

                    if(!empty($this->estatus_actaD->estatus)){?>
            <!-- ACTA DE DEFENSA -->
            <div class="col-lg-4">
                <div class="contact-box">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-center">
                                <span class="label label-info float-right">¡Nuevo!</span><br> 
                                <i class="fa fa-file-pdf-o" style="font-size: 100px;"></i>
                                <div class="m-t-xs font-bold">Acta de Defensa</div>
                                <!-- le pasamos el id_tipo documento para editar todos los memos y puntos de cuenta a traves de esta tabla -->
                                
                                <?php if($this->estatus_actaD->estatus=="En Espera"){
                                    if($_SESSION['id_perfil']==3){ ?>
                                <form id="form-acta" action="<?php echo constant('URL') . "solicitar_ascenso/updateMemo/".$this->solicitud->id_solicitud_ascenso.",".$this->estatus_actaD->id_tipo_documento;?>" method="post">
                                    <label id="acta_defensa-error" class="error" for="acta_defensa" style=""></label>
                                    <div class="alert alert-warning tooltip-demo checkbox checkbox-primary">
                                        <input id="checkbox20" name="acta_defensa" class="required" type="checkbox">
                                        <label for="checkbox20">
                                        Acta de Defensa
                                        </label>
                                        <div class="form-group">
                                            <button class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="right" title="Antes de Verificar Visualiza El Documento" type="submit"><i class="fa fa-check"></i> Verificar</button>
                                        </div>
                                    </div>
                                </form>
                                <?php }else{ ?><br>
                                    <div class="alert alert-warning checkbox checkbox-primary">En Espera de Verificación de el Director del CE.</div>
                                <?php }
                                }elseif($this->estatus_actaD->estatus=="Verificado"){?>
                                    <br>
                                    <div class="alert alert-success checkbox checkbox-primary">
                                        Verificado por el Director del CE.
                                    </div>

                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-lg-12"><br>
                            <div class="form-group text-center">
                                <a target="_blank" href="<?php echo constant('URL') . "solicitar_ascenso/viewDocumento/" . $this->solicitud->id_solicitud_ascenso.','.$this->estatus_actaD->id_tipo_documento;?>" class="btn btn-primary"><i class="fa fa-eye"></i> Ver Documento</a>&nbsp;
                                <a  target="_blank" href="<?php echo constant('URL').'src/documentos/ascensos/'.$this->estatus_actaD->descripcion;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>                                        
                                <br><br>
                                <?php if($_SESSION['id_perfil']==2 && $this->estatus_actaD->estatus=='En Espera'){?>
                                <button class="btn btn-block btn-xs btn-warning" 
                                    data-toggle="modal" 
                                    data-id_tipo_documento="<?php echo $this->estatus_actaD->id_tipo_documento;?>" 
                                    data-tipo_documento="<?php echo $this->estatus_actaD->tipo_documento;?>" 
                                    data-descripcion="<?php echo $this->estatus_actaD->descripcion;?>" 
                                    data-target="#myModal11"><i class="fa fa-edit"></i> Cambiar Acta de Defensa</button>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- FIN ACTA DE DEFENSA -->
            <?php } 
            //<!-- MEMO RESOLUCION 1 -->
            if(!empty($this->estatus_memoR1->estatus)){?>
            <div class="col-lg-4">
                <div class="contact-box">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-center">
                                <span class="label label-info float-right">¡Nuevo!</span><br> 

                                <i class="fa fa-file-pdf-o" style="font-size: 100px;"></i>
                                <div class="m-t-xs font-bold">Memorandum de Acta de Defensa N°1</div>
                                <!-- le pasamos el id_tipo documento para editar todos los memos y puntos de cuenta a traves de esta tabla -->
                                
                                <?php if($this->estatus_memoR1->estatus=="En Espera"){
                                    if($_SESSION['id_perfil']==4 || $_SESSION['id_perfil']==5){ ?>
                                <form id="form-R1" action="<?php echo constant('URL') . "solicitar_ascenso/updateMemo/".$this->solicitud->id_solicitud_ascenso.",".$this->estatus_memoR1->id_tipo_documento;?>" method="post">
                                    <label id="memo_R1-error" class="error" for="memo_R1" style=""></label>
                                    <div class="alert alert-warning tooltip-demo checkbox checkbox-primary">
                                        <input id="checkbox19" name="memo_R1" class="required" type="checkbox">
                                        <label for="checkbox19">
                                        Memorandum de Acta de Defensa N°1
                                        </label>
                                        <div class="form-group">
                                            <button class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="right" title="Antes de Verificar Visualiza El Documento" type="submit"><i class="fa fa-check"></i> Verificar</button>
                                        </div>
                                    </div>
                                </form>
                                <?php }else{ ?><br>
                                    <div class="alert alert-warning checkbox checkbox-primary">En Espera de Verificación de la DGTA.</div>
                                <?php }
                                }elseif($this->estatus_memoR1->estatus=="Verificado"){?>
                                    <br>
                                    <div class="alert alert-success checkbox checkbox-primary">
                                        Verificado por la DGTA.
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-lg-12"><br>
                            <div class="form-group text-center">
                                <a target="_blank" href="<?php echo constant('URL') . "solicitar_ascenso/viewDocumento/" . $this->solicitud->id_solicitud_ascenso.','.$this->estatus_memoR1->id_tipo_documento;?>" class="btn btn-primary"><i class="fa fa-eye"></i> Ver Documento</a>&nbsp;
                                <a  target="_blank" href="<?php echo constant('URL').'src/documentos/ascensos/'.$this->estatus_memoR1->descripcion;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>                                        
                                <br><br>
                                <?php if($_SESSION['id_perfil']==3 && $this->estatus_memoR1->estatus=='En Espera'){?>
                                <button class="btn btn-block btn-xs btn-warning" 
                                    data-toggle="modal" 
                                    data-id_tipo_documento="<?php echo $this->estatus_memoR1->id_tipo_documento;?>" 
                                    data-tipo_documento="<?php echo $this->estatus_memoR1->tipo_documento;?>" 
                                    data-descripcion="<?php echo $this->estatus_memoR1->descripcion;?>" 
                                    data-target="#myModal11"><i class="fa fa-edit"></i> Cambiar Memo</button>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
    <?php }
        if(!empty($this->estatus_memoR2->estatus)){?>
        <!-- FIN MEMO RESOLUCION 1 -->
        <div class="col-lg-4">
                <div class="contact-box">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-center">
                                <span class="label label-info float-right">¡Nuevo!</span><br> 

                                <i class="fa fa-file-pdf-o" style="font-size: 100px;"></i>
                                <div class="m-t-xs font-bold">Memorandum de Acta de Defensa N°2</div>
                                <!-- le pasamos el id_tipo documento para editar todos los memos y puntos de cuenta a traves de esta tabla -->
                                
                                <?php if($this->estatus_memoR2->estatus=="En Espera"){
                                    if($_SESSION['id_perfil']==6){ ?>
                                <form id="form-R2" action="<?php echo constant('URL') . "solicitar_ascenso/updateMemo/".$this->solicitud->id_solicitud_ascenso.",".$this->estatus_memoR2->id_tipo_documento;?>" method="post">
                                    <label id="memo_R2-error" class="error" for="memo_R2" style=""></label>
                                    <div class="alert alert-warning tooltip-demo checkbox checkbox-primary">
                                        <input id="checkbox18" name="memo_R2" class="required" type="checkbox">
                                        <label for="checkbox18">
                                        Memorandum de Acta de Defensa N°2
                                        </label>
                                        <div class="form-group">
                                            <button class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="right" title="Antes de Verificar Visualiza El Documento" type="submit"><i class="fa fa-check"></i> Verificar</button>
                                        </div>
                                    </div>
                                </form>
                                <?php }else{ ?><br>
                                    <div class="alert alert-warning checkbox checkbox-primary">En Espera de Verificación de Vicerrectorado.</div>
                                <?php }
                                }elseif($this->estatus_memoR2->estatus=="Verificado"){?>
                                    <br>
                                    <div class="alert alert-success checkbox checkbox-primary">
                                        Verificado por Vicerrectorado.
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-lg-12"><br>
                            <div class="form-group text-center">
                                <a target="_blank" href="<?php echo constant('URL') . "solicitar_ascenso/viewDocumento/" . $this->solicitud->id_solicitud_ascenso.','.$this->estatus_memoR2->id_tipo_documento;?>" class="btn btn-primary"><i class="fa fa-eye"></i> Ver Documento</a>&nbsp;
                                <a  target="_blank" href="<?php echo constant('URL').'src/documentos/ascensos/'.$this->estatus_memoR2->descripcion;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>                                        
                                <br><br>
                                <?php if(($_SESSION['id_perfil']==4 || $_SESSION['id_perfil']==5) && $this->estatus_memoR2->estatus=='En Espera'){?>
                                <button class="btn btn-block btn-xs btn-warning" 
                                    data-toggle="modal" 
                                    data-id_tipo_documento="<?php echo $this->estatus_memoR2->id_tipo_documento;?>" 
                                    data-tipo_documento="<?php echo $this->estatus_memoR2->tipo_documento;?>" 
                                    data-descripcion="<?php echo $this->estatus_memoR2->descripcion;?>" 
                                    data-target="#myModal11"><i class="fa fa-edit"></i> Cambiar Memo</button>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } 
        
            if(!empty($this->estatus_pto_cuentaR->estatus)){?>
            <!-- punto de cuenta fase 2-->
            <div class="col-lg-12">
                <div class="contact-box">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-center">
                                <span class="label label-info float-right">¡Nuevo!</span><br> 

                                <i class="fa fa-file-pdf-o" style="font-size: 100px;"></i>
                                <div class="m-t-xs font-bold">Punto de Cuenta de Acta de Defensa N°2</div>
                                <!-- le pasamos el id_tipo documento para editar todos los memos y puntos de cuenta a traves de esta tabla -->
                                
                                <?php if($this->estatus_pto_cuentaR->estatus=="En Espera"){
                                    if($_SESSION['id_perfil']==6){ ?>
                                <form id="form-pto_cuentaR2" action="<?php echo constant('URL') . "solicitar_ascenso/updateMemo/".$this->solicitud->id_solicitud_ascenso.",".$this->estatus_pto_cuentaR->id_tipo_documento;?>" method="post">
                                    <label id="pto_cuentaR2-error" class="error" for="pto_cuentaR2" style=""></label>
                                    <div class="alert alert-warning tooltip-demo checkbox checkbox-primary">
                                        <input id="checkbox17" name="pto_cuentaR2" class="required" type="checkbox">
                                        <label for="checkbox17">
                                        Punto de Cuenta de Acta de Defensa N°2
                                        </label>
                                        <div class="form-group">
                                            <button class="btn btn-xs btn-success" data-toggle="tooltip" data-placement="right" title="Antes de Verificar Visualiza El Documento" type="submit"><i class="fa fa-check"></i> Verificar</button>
                                        </div>
                                    </div>
                                </form>
                                <?php }else{ ?><br>
                                    <div class="alert alert-warning checkbox checkbox-primary">En Espera de Verificación de Vicerrectorado.</div>
                                <?php }
                                }elseif($this->estatus_pto_cuentaR->estatus=="Verificado"){?>
                                    <br>
                                    <div class="alert alert-success checkbox checkbox-primary">
                                        Verificado por Vicerrectorado.
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-lg-12"><br>
                            <div class="form-group text-center">
                                <a target="_blank" href="<?php echo constant('URL') . "solicitar_ascenso/viewDocumento/" . $this->solicitud->id_solicitud_ascenso.','.$this->estatus_pto_cuentaR->id_tipo_documento;?>" class="btn btn-primary"><i class="fa fa-eye"></i> Ver Documento</a>&nbsp;
                                <a  target="_blank" href="<?php echo constant('URL').'src/documentos/ascensos/'.$this->estatus_pto_cuentaR->descripcion;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>                                        
                                <br><br>
                                <?php if(($_SESSION['id_perfil']==4 || $_SESSION['id_perfil']==5) && $this->estatus_pto_cuentaR->estatus=='En Espera'){?>
                                <button style="width: 30%;" class="btn btn-xs btn-warning" 
                                    data-toggle="modal" 
                                    data-id_tipo_documento="<?php echo $this->estatus_pto_cuentaR->id_tipo_documento;?>" 
                                    data-tipo_documento="<?php echo $this->estatus_pto_cuentaR->tipo_documento;?>" 
                                    data-descripcion="<?php echo $this->estatus_pto_cuentaR->descripcion;?>" 
                                    data-target="#myModal11"><i class="fa fa-edit"></i> Cambiar Memo</button>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php }// fin punto de cuenta fase 2 
    } //fin fase 2?>
        

        

        </div>
       
        </div>

      

    <?php require 'views/footer.php'; ?>
 <!-- Jquery Validate -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>
    <!-- Sweet alert -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/sweetalert/sweetalert.min.js"></script>

    <script>

                // validar extensiones y peso de archivos
                var extensionesValidas = ".pdf";
                var pesoPermitido = 2000;// 2MB = 2000 kb

                // Validacion de extensiones permitidas
                function validarExtension(datos) {
                    
                    var ruta = datos.value;
                    var extension = ruta.substring(ruta.lastIndexOf('.') + 1).toLowerCase();
                    
                    var extensionValida = extensionesValidas.indexOf(extension);
                    //console.log(pesoTrabajo);
                    if(extensionValida < 0) {
                       // $('.error').text('La extensión válida es ".pdf". Su Archivo tiene de extensión: .'+ extension);
                        //alert('La extensión no es válida Su Archivo tiene de extensión: .'+ extension);
                        swal("Ha ocurrido un Error", 'La extensión válida es ".pdf". Su Archivo tiene de extensión: .'+extension, "error");  
                        document.getElementById("trabajo_ascenso").value = "";
                        document.getElementById("memo_solicitud_2").value = "";
                        document.getElementById("pto_cuenta_solicitud1").value = "";
                        document.getElementById("acta_defensa").value = "";
                        document.getElementById("memo_resolucion_2").value = "";
                        document.getElementById("pto_cuenta_resolucion1").value = "";

                        document.getElementById("memo_resolucion1").value = "";

                        return false;
                    } else {
                        return true;
                    }
                }


                // Validacion de peso del fichero en kbs
                function validarPeso(datos) {
                   
                if (datos.files && datos.files[0]) {
                    //alert(pesoPermitido);
                    var pesoFichero = datos.files[0].size/2000;

                    if(pesoFichero > pesoPermitido) {
                        //$('.error').text('El peso máximo permitido del Archivo es: ' + pesoPermitido + ' KBs Su fichero tiene: '+ pesoFichero +' KBs');
                        //alert('El peso maximo permitido del Archivo es: ' + pesoPermitido + ' KBs Su Archivo tiene: '+ pesoFichero +' KBs');
                        
                        swal("Ha ocurrido un Error","El peso máximo permitido del Archivo es: " + pesoPermitido + " KBs Su Archivo tiene: "+ pesoFichero +" KBs", "error");    
                        document.getElementById("trabajo_ascenso").value = "";
                        document.getElementById("memo_solicitud_2").value = "";
                        document.getElementById("pto_cuenta_solicitud1").value = "";
                        document.getElementById("acta_defensa").value = "";
                        document.getElementById("memo_resolucion_2").value = "";
                        document.getElementById("pto_cuenta_resolucion1").value = "";

                        document.getElementById("memo_resolucion1").value = "";

                        return false;
                    } else {
                        return true;
                    }
                }
            }
        
        $(document).ready(function(){
            $("#form-mp1").validate();
            $("#form-actaD").validate();
            $("#form-mR1").validate();
            $("#form-mR2").validate();
            $("#form-mS1").validate();
            $("#form-mDr").validate();
            $("#form-D").validate();

            $("#form").validate();
            $("#form-memoS2").validate();
            $("#form-pto1").validate();
            $("#form-acta").validate();
            $("#form-R1").validate();
            $("#form-trabajo").validate();
            $("#form-trabajo2").validate();
            
            //modal memorandum 1 fase 1
            $('#myModal6').on('show.bs.modal', function(e) {

                $("#trabajo_ascenso").change(function () {
                    $('.error').text('');
                    if(validarExtension(this)) { 
                        if(validarPeso(this)) { 
                        }
                    }  
                });


            });

            //modal memorandum 1 fase 1
            $('#myModal8').on('show.bs.modal', function(e) {

                $("#acta_defensa").change(function () {
                    $('.error').text('');
                    if(validarExtension(this)) { 
                        if(validarPeso(this)) { 
                        }
                    }  
                });


            });

            $('#myModal9').on('show.bs.modal', function(e) {

                $("#memo_resolucion1").change(function () {
                    $('.error').text('');
                    if(validarExtension(this)) { 
                        if(validarPeso(this)) { 
                        }
                    }  
                });


            });

            //modal memorandum 2 y punto de cuenta fase 1
            $('#myModal7').on('show.bs.modal', function(e) {

                $("#memo_solicitud_2").change(function () {
                    $('.error').text('');
                    if(validarExtension(this)) { 
                        if(validarPeso(this)) { 
                        }
                    }  
                });

                $("#pto_cuenta_solicitud1").change(function () {
                    $('.error').text('');
                    if(validarExtension(this)) { 
                        if(validarPeso(this)) { 
                        }
                    }  
                });
            });

            //modal memorandum 2 y punto de cuenta fase 1
            $('#myModal10').on('show.bs.modal', function(e) {

                $("#memo_resolucion_2").change(function () {
                    $('.error').text('');
                    if(validarExtension(this)) { 
                        if(validarPeso(this)) { 
                        }
                    }  
                });

                $("#pto_cuenta_resolucion1").change(function () {
                    $('.error').text('');
                    if(validarExtension(this)) { 
                        if(validarPeso(this)) { 
                        }
                    }  
                });
            });

            $('#myModal11').on('show.bs.modal', function(e) {
                var id_tipo_documento = $(e.relatedTarget).data('id_tipo_documento');
                $("#id_tipo_documento").val(id_tipo_documento);

                var tipo_documento = $(e.relatedTarget).data('tipo_documento');
                $("#tipo_documento").val(tipo_documento);

                var descripcion = $(e.relatedTarget).data('descripcion');
                $("#descripcion").val(descripcion);

                $("#upt_file").change(function () {
                    $('.error').text('');
                    if(validarExtension(this)) { 
                        if(validarPeso(this)) { 
                        }
                    }  
                });

            });

            <?php if($this->solicitud->id_estatus_ascenso==3){?>
                $("#form-R2").validate();
                $("#form-pto_cuentaR2").validate();
            <?php }
            //para detener el cambio de fase momentaneamente
            //$this->estatus_propuesta='En Espera';
            
            if($this->solicitud->id_estatus_ascenso==2){
                if($this->estatus_propuesta=='Verificado' 
                        && $this->estatus_memo1->estatus=='Verificado'
                        && $this->estatus_memoS2->estatus=='Verificado'
                        && $this->estatus_pto_cuentaS1->estatus=='Verificado'){ ?>
                    $.post("<?php echo constant('URL') . 'solicitar_ascenso/updateF2/'.$this->solicitud->id_solicitud_ascenso.",3";?>", function(htmlexterno){
                        $('#fase2').html(htmlexterno);
                    });
                    $("#est").text('Fase II');
                    $("#est").removeClass('label-primary');
                    $("#est").addClass('label-info');
                    $("#fase1").text('La Solicitud Ha sido trasladada Exitosamente a la Fase II (Resultado de Defensa). ');
                <?php }
            }
            

                //culminar una solicitud de ascenso id_estatus_ascenso=4;
            if($this->solicitud->id_estatus_ascenso == 3 
            && $this->estatus_pto_cuentaR->estatus=='Verificado' 
                && $this->estatus_memoR2->estatus=='Verificado'){ ?>

                    $.post("<?php echo constant('URL') . 'solicitar_ascenso/updateF2/'.$this->solicitud->id_solicitud_ascenso.",4";?>", function(htmlexterno){
                        $('#fase2').html(htmlexterno);
                    });
                    $("#est").text('Culminada');
                    $("#est").removeClass('label-info');
                    $("#est").addClass('label-success');
                    $("#fase1").text('La Solicitud Ha sido Culminada Exitosamente.');

                <?php } ?>
        });
    </script>
<?php
//$fecha_actual=date("Y-m-d");


    //var_dump($fecha_actual >= $_SESSION['fecha_inicio'] 
    //&& $fecha_actual <= $_SESSION['fecha_fin']);
   // var_dump($fecha_actual>$_SESSION['fecha_fin']);
    
    //si estatus es igual a fase 2 o 3 mostramos los lista de documentos que se han verificado 
    if($this->solicitud->id_estatus_ascenso==2 || $this->solicitud->id_estatus_ascenso==3 || $this->solicitud->id_estatus_ascenso==4){

        ?>
        <script>
            $('#checkbox2').attr('checked', true);
            $('#checkbox3').attr('checked', true);
            $('#checkbox4').attr('checked', true);
            $('#checkbox5').attr('checked', true);
            $('#checkbox6').attr('checked', true);
            <?php if($this->estatus_propuesta=='Verificado'){?>
                $('#checkbox7').attr('checked', true);
                $('#checkbox7').attr('disabled', true);
            <?php }?>
            <?php if($this->estatus_memo1->estatus=='Verificado'){?>
                $('#checkbox9').attr('checked', true);
                $('#checkbox9').attr('disabled', true);
            <?php }?>
            <?php if($this->estatus_memoS2->estatus=='Verificado'){?>
                $('#checkbox11').attr('checked', true);
                $('#checkbox11').attr('disabled', true);
            <?php }?>
            <?php if($this->estatus_pto_cuentaS1->estatus=='Verificado'){?>
                $('#checkbox12').attr('checked', true);
                $('#checkbox12').attr('disabled', true);
            <?php }?>
            <?php if($this->estatus_actaD->estatus=='Verificado'){?>
                $('#checkbox13').attr('checked', true);
                $('#checkbox13').attr('disabled', true);
            <?php }?>
            <?php if($this->estatus_memoR1->estatus=='Verificado'){?>
                $('#checkbox14').attr('checked', true);
                $('#checkbox14').attr('disabled', true);
            <?php }?>
            <?php if($this->estatus_memoR2->estatus=='Verificado'){?>
                $('#checkbox15').attr('checked', true);
                $('#checkbox15').attr('disabled', true);
            <?php }?>
            <?php if($this->estatus_pto_cuentaR->estatus=='Verificado'){?>
                $('#checkbox16').attr('checked', true);
                $('#checkbox16').attr('disabled', true);
            <?php }?>
            $('#checkbox2').attr('disabled', true);
            $('#checkbox3').attr('disabled', true);
            $('#checkbox4').attr('disabled', true);
            $('#checkbox5').attr('disabled', true);                
            $('#checkbox6').attr('disabled', true);
           
            
            $('#btnV').hide();
            $('#btnD').hide();

        </script>
        <?php

    }elseif($this->estudios!=true 
    || $this->diseno_uc!=true 
    || $this->tutorias!=true
    || $this->docencias_ubv!=true
    || $this->comision!=true
    || $this->articulo_revista!=true
    || $this->libro!=true
    || $this->arte_software!=true
    || $this->ponencia!=true
    || $this->reconocimiento!=true
    || $this->experiencia!=true){
        ?>
        <script>
            $(document).ready(function(){

                $('#checkbox2').attr('disabled', true);
                $('#checkbox3').attr('disabled', true);
                $('#checkbox4').attr('disabled', true);
                $('#checkbox5').attr('disabled', true);
                $('#checkbox6').attr('disabled', true);
//                data-toggle="tooltip" data-placement="top" title="" data-original-title="Debes Verificar La Experiencia Laboral del Docente"
                <?php   switch($_SESSION['id_perfil']){ 
                            case 1: ?>
                            $('#form').html('<div class="alert alert-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Tú Experiencia Laboral aún no ha sido Verificada"><i class="fa fa-info"></i> Tú Experiencia Laboral no ha sido Verificada <a class="alert-link" href="#">Completamente </a>.</div>');
                        <?php break;
                        case 2: ?>
                        $('#form').html('<div class="alert alert-warning" data-toggle="tooltip" data-placement="top" title="" data-original-title="Debes Verificar La Experiencia Laboral del Docente"><i class="fa fa-info"></i> Para Verificar la <a class="alert-link" target="_blank" href="<?php echo constant ('URL') . "verificar/detalle/" . $this->solicitud->id_docente;?>">Experiencia Laboral</a> del Docente Prosiona el botón <b>Ver Experiencia Laboral</b>.</div>');
                <?php break; } ?>
            
            });
        </script>

        <?php 

    }else{

        ?>
        <script>
            $(document).ready(function(){

                <?php switch($_SESSION['id_perfil']){

                    
                    case 2:?>
                $('#checkbox2').removeAttr('disabled');
                $('#checkbox3').removeAttr('disabled');
                $('#checkbox4').removeAttr('disabled');
                $('#checkbox5').removeAttr('disabled');
                $('#checkbox5').attr('checked', true);
                $('#checkbox6').removeAttr('disabled');
                $('#form').attr('action', '<?php echo constant('URL')."solicitar_ascenso/updateF1/".$this->id_solicitud_ascenso.",".$this->id_docente;?>');
                $('#form').append('<div class="alert alert-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Verifica Estos Documentos para continuar"><i class="fa fa-info"></i> Debes Verificar El Acta del PIDA, El Acta de Proyecto y El Trabajo de Ascenso para <a class="alert-link" href="#">Continuar</a>.</div>');
                <?php
                    break;
                 
                default:?>
                $('#checkbox2').attr('disabled', true);
                $('#checkbox3').attr('disabled', true);
                $('#checkbox4').attr('disabled', true);
                $('#checkbox5').attr('disabled', true);
                $('#checkbox6').attr('disabled', true);
                $('#btnV').hide();
                $('#btnD').hide();
                <?php 
                    break;
                }?>
                
                
            });
        </script>
        <?php
    }
     ?>
    

    <!-- dataTables Scripts -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
   <!-- Input Mask-->
   <script src="<?php echo constant ('URL');?>src/js/plugins/jasny/jasny-bootstrap.min.js"></script>
    
    

</body>
</html>
