<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SIDTA | Intermedia</title>


 
    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">


</head>

<body class="ibox-content">
 
  
   <!--  contenido -->
<div class="row">
<div class="col-lg-2"></div>
    <div class="col-lg-8">
        <h1>Tu Solicitud fue realizada exitosamente</h1>
        <p>Ahora Debes Descargar la planilla y dirigirte a la DGTA a formalizar tu solicitud con el analista</p>
        <form target="_blank" action="<?php echo constant('URL');?>views/ascensos/print_pdf.php" method="post">
        <input type="hidden" name="primer_nombre" value="<?php echo $this->docente->primer_nombre;?>">
        <input type="hidden" name="segundo_nombre" value="<?php echo $this->docente->segundo_nombre;?>">
        <input type="hidden" name="primer_apellido" value="<?php echo $this->docente->primer_apellido;?>">
        <input type="hidden" name="segundo_apellido" value="<?php echo $this->docente->segundo_apellido;?>">
        <input type="hidden" name="nacionalidad" value="<?php echo $this->docente->nacionalidad;?>">
        <input type="hidden" name="identificacion" value="<?php echo $this->docente->identificacion;?>">
        <input type="hidden" name="escalafon" value="<?php echo $this->docente->escalafon;?>">
        <input type="hidden" name="centro_estudio" value="<?php echo $this->docente->centro_estudio;?>">
        <input type="hidden" name="fecha_solicitud" value="<?php echo $this->docente->fecha_solicitud;?>">
        <input type="hidden" name="ciudad" value="<?php echo $this->docente->ciudad;?>">
        <input type="hidden" name="fecha_consulta" value="<?php echo $this->docente->fecha_consulta;?>">
        <input type="hidden" name="escalafon_siguiente" value="<?php echo $this->docente->escalafon_siguiente;?>">

        <input class="btn btn-success" type="submit" name="pdf" value="Generar PDF"/>
        <a href="<?php echo constant('URL');?>solicitar_ascenso" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Volver</a>
        </form>
    </div>
    <div class="col-lg-2"></div>

</div>
    
       

    
    
                    <!--  end contenido -->

                        <?php require 'views/footer.php'; ?>


</body>
</html>

