<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SIDTA | Solicitar Ascenso</title>


    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">
  
    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
<!-- jasny input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <!-- datapicker -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    
    <!--  style select2 -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/select2/select2.min.css" rel="stylesheet">
    <!-- sweetalert input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

</head>

<body>
 
    <div id="wrapper">
   <?php require 'views/header.php'; ?>
   

        <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-5">
                    <h2>Solicitudes de Ascenso</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>solicitar_ascenso">Ascensos</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>solicitar_ascenso/viewAgregar">Solicitudes</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Solicitar Ascenso</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-7">
                    <div class="title-action">
                        <a href="<?php echo constant ('URL');?>solicitar_ascenso" class="btn btn-primary"><i class="fa fa-users"></i> Listar Ascensos</a>

                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
           
          
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>
                                <i class="fa fa-angle-double-right"></i> SIDTA <i class="fa fa-angle-double-left"></i>
                            </h5>
                        </div>
                        <div class="ibox-content">
                            <h2>
                                Solicitar Ascenso
                            </h2>
                            <p>
                                Completa el formulario para formalizar la solicitud de ascenso.
                            </p>
                            <?php echo $this->mensaje;?>
                            <div class="alert alert-info">Todos los campos marcados con un (<span style="color: red;">*</span>) Son Obligatorios</div>

                            <form class="m-t" role="form" id="form" method="post" action="<?php echo constant('URL');?>solicitar_ascenso/registrar" enctype="multipart/form-data">
                                <h1>Trabajo de Ascenso</h1>
                                <fieldset>
                                    
                                    
                                    <h2>Requisitos para Solicitar Ascenso</h2>
                                    
                                        <div class="row">
                                        <div class="col-lg-12"> <div class="form-group">
                                                    <label>Título de Trabajo de Ascenso <span style="color: red;">*</span></label>
                                                    <input type="text" minlength="5" maxlength="80" name="titulo" id="titulo" class="form-control required" placeholder="Ingrese el Título de su Trabajo de Ascenso">
                                                </div></div>
                                            <div class="col-lg-6">
                                               
                                                <div class="form-group" id="data_2">
                                                <label>Fecha de Consulta  <span style="color: red;">*</span></label>
                                                
                                                
                                                <div class="input-group date">
                                                    <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i></span>
                                                    <input type="text" name="fecha_consulta" id="fecha_consulta" class="form-control required" placeholder="Seleccione la Fecha en la que consulto la pagina de la UBV">
                                                </div>
                                                <span><i>mm/dd/aaaa</i></span>
                                                <label id="fecha_consulta-error" for="fecha_consulta"></label>
                                            </div>
                                            <script>
                                            var mem = $('#data_2 .input-group.date').datepicker({
                                                
                                                autoclose: true
                                                
                                            });
                                            </script>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Ciudad <span style="color: red;">*</span></label>
                                                    <input type="text" minlength="5" maxlength="80" name="ciudad" id="ciudad" class="form-control required" placeholder="Ingrese la ciudad de donde hace la solicitud">
                                                </div>
                                            </div>

                                            <?php 
                                                    switch($this->docente->id_escalafon){
                                                        case 1:
                                                        $var="ASISTENTE";
                                                        $id_escalafon_sig=2;
                                                        break;
                                                        case 2:
                                                        $var="AGREGADO";
                                                        $id_escalafon_sig=3;
                                                        break;
                                                        case 3:
                                                        $var="ASOCIADO";
                                                        $id_escalafon_sig=4;
                                                        break;
                                                        case 4:
                                                        $var="TITULAR";
                                                        $id_escalafon_sig=5;
                                                        break;
                                                        case 5:
                                                        $var="EMÉRITO";
                                                        $id_escalafon_sig=6;
                                                        break;
                                                        default:
                                                        $var="INSTRUCTOR";
                                                        $id_escalafon_sig=1;
                                                        break;
                                                    }
                                                    ?>
                                                            
                                                        
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <input type="hidden" name="escalafon_sig_statico" value="<?php echo $id_escalafon_sig; ?>">
                                                    
                                                    <label>El escalafón al que ascendera es <b><?php echo $var;?></b></label>
                                                    <input type="hidden" name="correo" value="<?php echo $this->docente->correo;?>">
                                                    
                                                </div>
                                            </div>
                                            <div class="col-lg-6 " id="wno" style="display: none;">
                                                <div class="form-group">
                                                    <label>Escalafón <span style="color: red;">*</span></label>
                                                    <select name="escalafon_next" id="escalafon_next" style="width: 100%;"  class="form-control select2_demo_1" tabindex="4">
                                                        <option value="">Seleccione el escalafón.</option>
                                                        <?php 
                                                            foreach($this->escalafones as $row){
                                                            $escalafon=new Estructura();
                                                            $escalafon=$row;?> 
                                                        <option value="<?php echo $escalafon->id;?>"><?php echo $escalafon->descripcion;?></option>
                                                        <?php }?>                                         
                                                    </select>
                                                </div>
                                                <script>
                                                    $(".select2_demo_1").select2({
                                                        placeholder: "Seleccione el escalafón.",
                                                        allowClear: true
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>Cargar Trabajo de Ascenso <span style="color: red;">*</span></label>

                                                    <div class="form-group">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <span class="btn btn-default btn-file"><span class="fileinput-new">Seleccionar Archivo</span>
                                                            <span class="fileinput-exists">Cambiar</span><input type="file" id="trabajo_ascenso" accept=".pdf" class="required" name="trabajo_ascenso"/></span>
                                                            <label id="trabajo_ascenso-error" class="error" for="trabajo_ascenso"></label>

                                                            <span class="fileinput-filename"></span>
                                                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <div class="col-lg-3">
                                            <label>Cargar Acta del PIDA <span style="color: red;">*</span></label>

                                            <div class="form-group">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-default btn-file"><span class="fileinput-new">Seleccionar Archivo</span>
                                                    <span class="fileinput-exists">Cambiar</span><input type="file" id="acta_pida" accept=".pdf" class="required" name="acta_pida"/></span>
                                                    <label id="acta_pida-error" class="error" for="acta_pida"></label>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                                </div> 
                                            </div>
                                            
                                        </div>
                                        <div class="col-lg-3">
                                            <label>Cargar Acta de Proyecto <span style="color: red;">*</span></label>

                                            <div class="form-group">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-default btn-file"><span class="fileinput-new">Seleccionar Archivo</span>
                                                    <span class="fileinput-exists">Cambiar</span><input type="file" id="acta_proyecto" class="required" accept=".pdf" name="acta_proyecto"/></span>
                                                    <label id="acta_proyecto-error" class="error" for="acta_proyecto"></label>

                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <?php 
                                        
                                       
                                        if($this->docente->id_escalafon == 2 
                                        || $this->docente->id_escalafon == 3
                                        || $this->docente->id_escalafon == 4
                                        || $this->docente->id_escalafon == 5
                                        || $this->docente->id_escalafon == 6){?>
                                        <div class="col-lg-3">
                                            <label>Cargar Última Resolución de Ascensos <span style="color: red;">*</span></label>

                                            <div class="form-group">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-default btn-file"><span class="fileinput-new">Seleccionar Archivo</span>
                                                    <span class="fileinput-exists">Cambiar</span><input type="file" id="resolucion" class="required" accept=".pdf" name="resolucion"/></span>
                                                    <label id="resolucion-error" class="error" for="resolucion"></label>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <?php } ?>
                                        </div>
                                        <script>
                                        //<!-- script para validar peso y extension de los archivos -->

                $("#trabajo_ascenso").change(function () {
                    $('.error').text('');
                    if(validarExtension(this)) { 
                        if(validarPesoTA(this)) { 
                        }
                    }  
                });
                $("#acta_pida").change(function () {
                    $('.error').text('');
                    if(validarExtension(this)) { 
                        if(validarPeso(this)) { 
                        }
                    }  
                });
                $("#acta_proyecto").change(function () {
                    $('.error').text('');
                    if(validarExtension(this)) { 
                        if(validarPeso(this)) { 
                        }
                    }  
                });
                $("#resolucion").change(function () {
                    $('.error').text('');
                    if(validarExtension(this)) { 
                        if(validarPeso(this)) { 
                        }
                    }  
                });
                            
            //<!-- script para validar peso y extension de los archivos -->


                                        </script>
                                </fieldset>
                                
                            </form>

           
                        </div>

                        
                    </div>

                    
                    </div>
                    

                </div>
                
                </div>

        <?php require 'views/footer.php'; ?>

        
    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>
    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>

    <!-- Chosen Select -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script> 
   <!-- Input Mask-->
   <script src="<?php echo constant ('URL');?>src/js/plugins/jasny/jasny-bootstrap.min.js"></script>

       <!-- Data picker -->
       <script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>
       <!-- Select2 -->
       <script src="<?php echo constant ('URL');?>src/js/plugins/select2/select2.full.min.js"></script>

    <!-- Sweet alert -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/sweetalert/sweetalert.min.js"></script>

    
       <script>

            // validar extensiones y peso de archivos
                var extensionesValidas = ".pdf";
                var pesoPermitido = 2000;// 2MB = 2000 kb
                var pesoPermitidoTA = 8192;// 8MB = 8192 kb

                // Validacion de extensiones permitidas
                function validarExtension(datos) {
                    
                    var ruta = datos.value;
                    var extension = ruta.substring(ruta.lastIndexOf('.') + 1).toLowerCase();
                    
                    var extensionValida = extensionesValidas.indexOf(extension);
                    //console.log(pesoTrabajo);
                    if(extensionValida < 0) {
                       // $('.error').text('La extensión válida es ".pdf". Su Archivo tiene de extensión: .'+ extension);
                        //alert('La extensión no es válida Su Archivo tiene de extensión: .'+ extension);
                        swal("Ha ocurrido un Error", 'La extensión válida es ".pdf". Su Archivo tiene de extensión: .'+extension, "error");  
                        document.getElementById("trabajo_ascenso").value = "";
                        document.getElementById("acta_pida").value = "";
                        document.getElementById("acta_proyecto").value = "";
                        document.getElementById("resolucion").value = "";

                        return false;
                    } else {
                        return true;
                    }
                }


                // Validacion de peso del fichero en kbs
                function validarPeso(datos) {
                   
                if (datos.files && datos.files[0]) {
                    //alert(pesoPermitido);
                    var pesoFichero = datos.files[0].size/2000;

                    if(pesoFichero > pesoPermitido) {
                        //$('.error').text('El peso máximo permitido del Archivo es: ' + pesoPermitido + ' KBs Su fichero tiene: '+ pesoFichero +' KBs');
                        //alert('El peso maximo permitido del Archivo es: ' + pesoPermitido + ' KBs Su Archivo tiene: '+ pesoFichero +' KBs');
                        
                        swal("Ha ocurrido un Error","El peso máximo permitido del Archivo es: " + pesoPermitido + " KBs Su Archivo tiene: "+ pesoFichero +" KBs", "error");    
                        document.getElementById("trabajo_ascenso").value = "";
                        document.getElementById("acta_pida").value = "";
                        document.getElementById("acta_proyecto").value = "";
                        document.getElementById("resolucion").value = "";

                        return false;
                    } else {
                        return true;
                    }
                }
            }


            // Validacion de peso del fichero en kbs
            function validarPesoTA(datos) {
                   
                   if (datos.files && datos.files[0]) {
                       //alert(pesoPermitido);
                       var pesoFichero = datos.files[0].size/8192;
   
                       if(pesoFichero > pesoPermitidoTA) {
                           //$('.error').text('El peso máximo permitido del Archivo es: ' + pesoPermitidoTA + ' KBs Su fichero tiene: '+ pesoFichero +' KBs');
                           //alert('El peso maximo permitido del Archivo es: ' + pesoPermitidoTA + ' KBs Su Archivo tiene: '+ pesoFichero +' KBs');
                           
                           swal("Ha ocurrido un Error","El peso máximo permitido del Archivo es: " + pesoPermitidoTA + " KBs Su Archivo tiene: "+ pesoFichero +" KBs", "error");    
                           document.getElementById("trabajo_ascenso").value = "";
                           document.getElementById("acta_pida").value = "";
                           document.getElementById("acta_proyecto").value = "";
                           document.getElementById("resolucion").value = "";
   
                           return false;
                       } else {
                           return true;
                       }
                   }
               }
           // fin validar extensiones y peso de archivos
        // fin validar extensiones y peso de archivos

  

            function mostrar2(id_radio){

                console.log(id_radio);
                switch(id_radio){
                    case "si":
                    $("#wno").hide();
                    $("#escalafon_next").removeClass('required');

                    break;
                    case "no":
                    $("#wno").show();
                    $("#escalafon_next").addClass('required');
                    break;

                }
            }


        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                   
                  

                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#passwd"
                            }
                        }
                    });
       });
    </script>

   

</body>
</html>
