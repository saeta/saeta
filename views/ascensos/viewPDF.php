<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Solicitud</title>
</head>
<body>
    
    <table style=" margin-left: 16mm">
        <tr >
            <td><img style="width: 80mm;" src="../../src/img/logo-ubv.png" alt="" srcset=""></td>
            <td style="font-weight: bold; text-align: left; font-size: 15px; border-left: 2px solid; ">
                <p style="margin-left: 25; margin-bottom: 0;">
                  República Bolivariana de Venezuela
                  <br>MPPP para la Educación Universitaria, 
                  <br>Ciencia y Tecnología
                  <br>Universidad Bolivariana de Venezuela
                  <br><?php echo ucwords($_POST['centro_estudio']);?>
                </p>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="2">
                <h5 style="margin-bottom: 0;"><b>CARTA DE SOLICITUD DE ASCENSO</b></h5>
                <h5>
                    <b>A LA CATEGORIA ACADÉMICA DE PROFESOR <?php echo strtoupper($_POST['escalafon_siguiente']);?></b>
                </h5>
            </td>
        </tr>
        <tr>
            <td style="width: 100mm; text-align: justify;" colspan="2">
            <p>Quién suscribe, <b><?php echo ucwords($_POST['primer_nombre']) ." ".ucwords($_POST['segundo_nombre']).", ".ucwords($_POST['primer_apellido'])." ".ucwords($_POST['segundo_apellido']);?></b>, de nacionalidad <b><?php echo ucwords($_POST['nacionalidad']);?></b>,
            cédula de
            identidad <b>No. <?php echo $_POST['identificacion'];?></b>, habiendo cumplido una permanencia en la institución en el escalafón
            académico <b><?php echo ucwords($_POST['escalafon']);?></b>, solicito ante el <b><?php echo ucwords($_POST['centro_estudio']);?></b> de la UBV, la apertura
            del proceso de ascenso a la categoría de profesor <?php echo strtoupper($_POST['escalafon_siguiente']);?>, conforme a lo contemplado en la
            Constitución de la República Bolivariana de Venezuela en los artículos 104 y 106, Ley de
            Universidades articulo 26 numeral 21 y el Reglamento General de la Universidad Bolivariana de
            Venezuela artículos 1, 2, 84, 118, 119 y 19 numerales 14 y 31, y según las normas publicadas en la
            página web de la U.B.V. consultada en fecha <b><?php echo $_POST['fecha_consulta'];?></b>. Al mismo tiempo, declaro que
            acepto en su totalidad las condiciones previstas para el Ascenso Académico, y consigno esta
            solicitud bajo mi voluntad y conocimiento de los derechos que me asisten como trabajador(a)
            académico(a) de la Universidad Bolivariana de Venezuela.</p>
            <p>Solicitud que se realiza en la Ciudad de <b><?php echo ucwords($_POST['ciudad']);?></b> el día de <b><?php echo $_POST['fecha_solicitud'];?></b>.</p>
            </td>
        </tr>
       
        <tr>
            <td colspan="2" style="text-align: center;">
            <div style="margin-left: -25mm; width: 50mm"><hr></div>
                <p style="margin-bottom: 0;">
                    <b>
                        <?php echo ucwords($_POST['primer_nombre']) ." ".ucwords($_POST['segundo_nombre']).", ".ucwords($_POST['primer_apellido'])." ".ucwords($_POST['segundo_apellido']);?>
                    </b>
                </p>
                <p>
                    <b>
                    C.I. No. <?php echo $_POST['identificacion'];?>
                    </b>
                </p>
            </td>
        </tr>
        
        <tr>
            <td colspan="2"><br><br>
                <p style="margin-bottom: 0;">Consigna solicitud y documentos:</p>
                <p>Esta carta de solicitud no garantiza, por sí sola, la formalización de la inscripción en el proceso de
            ascenso.</p>
            </td>  
        </tr>
    </table> 
    <table style="margin-top: 60mm;">
          <tr>
            <td>
              <p style="margin-left: 105mm;">Av. Leonardo Da Vinci, Edf. UBV, Piso
              <br>10, Anexo A, Los Chaguaramos 
              <br>Teléfono: 0212-6063159 
              <br>RIF. G-20003773-3
              <br><a target="_blank" href="http://ubv.edu.ve">www.ubv.edu.ve</a> @ubv
              </p>
            </td>
            <td><img style="margin-left: 5mm; margin-top: 8mm; width: 20mm;" src="../../src/img/eu.png" alt="" srcset=""></td>
          </tr>
          
        </table>  
</body>
</html>