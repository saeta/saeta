<!--
*
*  INSPINIA - Arte o Software
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin | Diseño de Unidad Curricurar Docente</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

     <!--  style chosen lo lo puse -->
     <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

 


</head>

<body>
    <?php require 'views/header.php'; ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-7">
        <h2>Detalle Diseño de Unidad Curricurar Docente</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="<?php echo constant('URL');?>home">Inicio</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="<?php echo constant ('URL');?>diseno_uc">Desempeño Docente UBV</a>
                </li>
                <li class="breadcrumb-item">
                    <strong>Listar Diseño de Unidad Curricurar Docente</strong>
                </li>
                <li class="breadcrumb-item active">
                    <a href="<?php echo constant ('URL');?>diseno_uc/viewVer"> <strong>Detalle </strong></a>
                </li>
            </ol>
    </div>
<div class="col-lg-5">
    <div class="title-action">
        <a href="<?php echo constant ('URL');?>diseno_uc" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Volver</a>
    </div>
</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>SIDTA</h5>
                    <div class="ibox-tools">
                    </div>
                </div>

    <div class="ibox-content">
        <!--  contenido -->
        <div class="row">
            <div class="col-lg-12">
                <div class="tabs-container">
                    <div class="tabs-right">
                        <ul class="nav nav-tabs">
                            <li><a class="nav-link active" data-toggle="tab" href="#tab-8"> Datos del Diseño</a></li>
                            <li><a class="nav-link" data-toggle="tab" href="#tab-9">Observación</a></li>
                            <li><a class="nav-link" data-toggle="tab" href="#tab-10">Certificado</a></li>
                    <div class="col-lg-12">
                        <div class="text-center">
                            <div style="margin-top: 30px">
                                <i class="fa fa-newspaper-o" style="font-size: 180px;color: #265491 "></i>
                            </div>
                        </div>
                    </div>
                        </ul>
                <div class="tab-content">
                    <div id="tab-8" class="tab-pane active">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <h3> Diseño Unidad Curricular Docente:</h3>
                                    <p><?php echo $this->disenos->nombre; ?></p>
                                </div>
                                <div class="col-lg-6">
                                    <h3> Año Creacion:</h3>
                                    <p><?php echo $this->disenos->ano_creacion; ?></p>
                                </div>
                            </div>
                                <hr>
                            <div class="row">
                                <div class="col-lg-6">
                                    <h3> Tramo:</h3>
                                    <p><?php echo $this->disenos->descripcion_tramo; ?></p>
                                </div>
                                <div class="col-lg-6">
                                    <h3> Tipo de Unidad Curricular:</h3>
                                    <p><?php echo $this->disenos->descripcion_tipo_uc; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tab-9" class="tab-pane">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <h3> Programa de Formación:</h3>
                                    <p><?php echo $this->disenos->descripcion_programa; ?></p>
                                </div>
                                <div class="col-lg-6">
                                    <h3> Area Conocimiento UBV:</h3>
                                    <p><?php echo $this->disenos->descripcion_area_ubv; ?></p>
                                </div>
                            </div>
                                <hr>
                            <div class="row">
                                <div class="col-lg-6">
                                    <h3> Estatus Verificación:</h3>
                                    <p><?php echo $this->disenos->estatus_verificacion; ?></p>
                                </div>
                                <input id="id_diseno_uc" name="id_diseno_uc"  disabled type="hidden"  value="<?php echo $this->disenos->id_diseno_uc;  ?>">
                                <input type="hidden" name="registrar">
                            </div>
                        </div>
                    </div>

                    <div id="tab-10" class="tab-pane">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <h3> <label>Certificado Diseño <i>(Formato PDF)</i><span style="color: red;">*</span></label></h3>
                                    <div class="form-group">
                                        <a target="_blank" href="<?php echo constant('URL') . "diseno_uc/viewDocumento/" . $this->disenos->id_diseno_uc;?>" class="btn btn-primary"><i class="fa fa-external-link"></i> Constancia</a>                                                         
                                        <a target="_blank" href="<?php echo constant('URL').'src/documentos/diseno/'.$this->disenos->descripcion_documento;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                            </div>
                                </div>
                            </div>
                        <!--  end contenido -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>








<?php require 'views/footer.php'; ?>


<!-- Steps -->
<script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

<!-- Jquery Validate -->
<script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

<!-- menu active -->
<script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
<!-- Chosen -->
<script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>

<!-- 
<script>
$(function(){
$("li").removeClass("active");
$("#Extructura").addClass("active");  
$("#re").addClass("active");
});   
</script>-->



<script>
$(document).ready(function(){
$("#wizard").steps();
$("#form").steps({
bodyTag: "fieldset",
onStepChanging: function (event, currentIndex, newIndex)
{
    // Always allow going backward even if the current step contains invalid fields!
    if (currentIndex > newIndex)
    {
        return true;
    }

    // Forbid suppressing "Warning" step if the user is to young
    if (newIndex === 3 && Number($("#age").val()) < 18)
    {true
        return false;
    }

    var form = $(this);

    // Clean up if user went backward before
    if (currentIndex < newIndex)
    {
        // To remove error styles
        $(".body:eq(" + newIndex + ") label.error", form).remove();
        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
    }

    // Disable validation on fields that are disabled or hidden.
    form.validate().settings.ignore = ":disabled,:hidden";true

    // Start validation; Prevent going forward if false
    return form.valid();
},
onStepChanged: function (event, currentIndex, priorIndex)
{
    // Suppress (skip) "Warning" step if the user is old enough.
    if (currentIndex === 2 && Number($("#age").val()) >= 18)
    {true
        $(this).steps("next");
    }

    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
    if (currentIndex === 2 && priorIndex === 3)
    {
        $(this).steps("previous");
    }
},
onFinishing: function (event, currentIndex)
{
    var form = $(this);

    // Disable validation on fields that are disabled.
    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
    form.validate().settings.ignore = ":disabled";

    // Start validation; Prevent form submission if false
    return form.valid();
},
onFinished: function (event, currentIndex)
{
    var form = $(this);

    // Submit form input
    form.submit();
}
}).validate({
        errorPlacement: function (error, element)
        {
            element.before(error);
        }/*,
        rules: {
            confirm: {
                equalTo: "#password"
            },
            ano_arte:{
                required: true
            },
            entidad_promotora: {
                required: true
            },
            url_otro: {
                url: true
            }
            
        }*/
    });
});
</script>


</body>
</html>
