<!--
*
*  INSPINIA - Diseño de Unidad Curricular Docente
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin | Diseño de Unidad Curricurar Docente</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

     <!--  style chosen lo lo puse -->
     <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

        <!-- jasny input mask-->
        <link href="<?php echo constant ('URL');?>src/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <!--  style select2 -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/select2/select2.min.css" rel="stylesheet">
    <!-- datapicker -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <!-- sweetalert input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

</head>

<body>
    <?php require 'views/header.php'; ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Registrar Diseño de Unidad Curricurar Docente</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="<?php echo constant('URL');?>home">Inicio</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="<?php echo constant ('URL');?>diseno_uc">Desempeño Docente UBV</a>
                </li>
                <li class="breadcrumb-item">
                    <strong>Listar Diseño de Unidad Curricurar Docente</strong>
                </li>
                <li class="breadcrumb-item active">
                    <a href="<?php echo constant ('URL');?>diseno_uc/viewRegistrar"> <strong>Agregar </strong></a>
                </li>
            </ol>
    </div>
<div class="col-lg-2">
    <div class="title-action">
        <a href="<?php echo constant ('URL');?>diseno_uc" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Volver</a>
    </div>
</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>SiDTA</h5>
                </div>
<div class="ibox-content">
<div id="respua"><?php echo $this->mensaje; ?></div>
<div class="alert alert-info">Todos los campos marcados con un (<span style="color: red;">*</span>) Son Obligatorios</div>

    <form id="form" method="post" action="<?php echo constant('URL');?>diseno_uc/registrarDiseno" class="wizard-big" enctype="multipart/form-data">
        <h1>Diseño de Unidad Curricurar Docente</h1>
    <fieldset>
        <h2> Información del Diseño de Unidad Curricurar Docente</h2>
    <div class="row">
        <div class="col-lg-4">
            <div class="form-group">
                <label>Diseño de Unidad Curricurar Docente <span style="color: red;">*</span></label>
                <input id="nombre" name="nombre" type="text" placeholder="Ingrese el nombre del Diseño de Unidad Curricurar Docente" maxlength="70" class="form-control required">
            </div>
            <div class="form-group" id="data_2">
                <label>Año creación<span style="color: red;">*</span></label>
                <div class="input-group date">
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <input id="ano_creacion" name="ano_creacion" type="text"  placeholder="Seleccione el año de creacion" class="form-control required number">
            </div>
                <label id="ano_creacion-error" class="error" for="ano_creacion"></label>
            <script>
                var mem = $('#data_2 .input-group.date').datepicker({
                    format: 'yyyy',
                    viewMode: "years",
                    minViewMode: "years",
                    autoclose: true
                });
            </script>
            </div>
            <div class="form-group">
                <label>Cargar Certificado Diseño <span style="color: red;">*</span></label>
            <div class="form-group">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <span class="btn btn-default btn-file"><span class="fileinput-new">Seleccionar Archivo</span>
                    <span class="fileinput-exists">Cambiar</span><input type="file" id="file_reconocimiento" accept=".pdf"  name="pdf_diseno"/></span>
                    <span class="fileinput-filename"></span>
                    <i class="fa fa-upload" style="font-size: 60px;color: #265491  "></i>
                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                    <label id="file_reconocimiento-error" class="error" for="file_reconocimiento"></label>
                    <script>

// Cuando cambie #fichero
$("#file_reconocimiento").change(function () {
    $('.error').text('');
    if(validarExtension(this)) { 
        if(validarPeso(this)) { 
        }
    } 
});


</script>
                </div>
            </div>
            
        </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label>Unidad Curricular Tipo<span style="color: red;">*</span></label>
                <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige el Tipo de Unidad Curricular..." name="id_unidad_curricular_tipo" id="id_unidad_curricular_tipo">
                    <option value="">Seleccione</option>
                        <?php include_once 'models/datosacademicos/tipouc.php';
                        foreach($this->tipos as $row){
                                    $tipo_uc=new TipoUC();
                                    $tipo_uc=$row;?>
                    <option value="<?php echo $tipo_uc->id_unidad_curricular_tipo;?>"><?php echo $tipo_uc->descripcion;?></option>
                        <?php }?>
                </select>
            </div>
            <div class="form-group">
                <label>Programa Formación <span style="color: red;">*</span></label>
                <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige el Programa..." name="id_programa" id="id_programa">
                    <option value="">Seleccione</option>
                        <?php include_once 'models/datosacademicos/programa_formacion.php';
                        foreach($this->programas as $row){
                                    $programa=new Programa();
                                    $programa=$row;?>
                    <option value="<?php echo $programa->id_programa;?>"><?php echo $programa->descripcion;?></option>
                        <?php }?>
                </select>
            </div>
            <div class="form-group">
                <label>Area del Conocimiento UBV <span style="color: red;">*</span></label>
                <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige el Área..." name="id_area_conocimiento_ubv" id="id_area_conocimiento_ubv">
                    <option value="">Seleccione</option>
                        <?php include_once 'models/datosacademicos/areaubv.php';
                        foreach($this->areasubv as $row){
                                    $area_ubv=new AreaUbv();
                                    $area_ubv=$row;?>
                    <option value="<?php echo $area_ubv->id_area_conocimiento_ubv;?>"><?php echo $area_ubv->descripcion;?></option>
                        <?php }?>
                </select>
            </div>
        </div>
        <div class="col-lg-4">
        <div class="form-group">
                <label>Tramo <span style="color: red;">*</span></label>
                <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige el Tramo..." name="id_tramo" id="id_tramo">
                    <option selected="selected" value="">Seleccione</option>
                        <?php include_once 'models/datosacademicos/tramo.php';
                        foreach($this->tramos as $row){
                                    $tramo=new Tramo();
                                    $tramo=$row;?>
                    <option value="<?php echo $tramo->id_tramo;?>"><?php echo $tramo->descripcion;?></option>
                        <?php }?>
                </select>
            </div>
            

                                         <!--   <div class="form-group">
                                    <label>Estatus Verificación<span style="color: red;">*</span></label>
                                        <select class="form-control required m-b " name="estatus_verificacion" id="estatus-verificacion">
                                            <option value="">Seleccione</option>
                                            <option value="Activo"> Verificado</option>
                                            <option value="Inactivo"> Sin Verificar</option>
                                        </select>
                                </div> -->
            <div class="form-group">
                <input type="hidden" name="registrar">
            </div>
                <div class="text-center">
                    <div style="margin-top: 20px">
                        <i class="fa fa-newspaper-o" style="font-size: 180px;color: #265491 "></i>
                    </div>
                </div>
        </div>
    </div>
    </fieldset>
    </form>
</div>
            </div>
        </div>
    </div>
</div>
    <?php require 'views/footer.php'; ?>

   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    <!-- Chosen -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>

     <!-- Sweet alert -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Input Mask-->
<script src="<?php echo constant ('URL');?>src/js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <!-- Select2 -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/select2/select2.full.min.js"></script>

 <!-- Data picker -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>

<!--
    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");
    $("#re").addClass("active");
});
</script>-->



<script>


var extensionesValidas = ".pdf";

var pesoPermitido = 2000;//2MB = 2000 kb
//console.log(pesoPermitido, extensionesValidas);
// Validacion de extensiones permitidas
function validarExtension(datos) {
    
    var ruta = datos.value;
    var extension = ruta.substring(ruta.lastIndexOf('.') + 1).toLowerCase();
    
    var extensionValida = extensionesValidas.indexOf(extension);
    console.log('valor.',extensionValida < 0);
    //console.log(ruta, extension, extensionValida);
    if(extensionValida < 0) {
            $('.error').text('La extensión no es válida Su Archivo tiene de extensión: .'+ extension);
            //alert('La extensión no es válida Su Archivo tiene de extensión: .'+ extension);
        swal("Ha ocurrido un Error", "La extensión no es válida Su Archivo tiene de extensión: ."+extension, "error");  
        document.getElementById("file_reconocimiento").value = "";
      
        
        return false;
    } else {
        return true;
    }
}


// Validacion de peso del fichero en kbs
function validarPeso(datos) {
if (datos.files && datos.files[0]) {
    var pesoFichero = datos.files[0].size/2000;
    if(pesoFichero > pesoPermitido) {
        $('.error').text('El peso máximo permitido del Archivo es: ' + pesoPermitido + ' KBs Su fichero tiene: '+ pesoFichero +' KBs');
        //alert('El peso maximo permitido del Archivo es: ' + pesoPermitido + ' KBs Su Archivo tiene: '+ pesoFichero +' KBs');
        
        swal("Ha ocurrido un Error","El peso máximo permitido del Archivo es: " + pesoPermitido + " KBs Su Archivo tiene: "+ pesoFichero +" KBs", "error");    
        document.getElementById("file_reconocimiento").value = "";
       
        
        return false;
    } else {
        return true;
    }
}
}




$(document).ready(function(){


    $("#wizard").steps();
    $("#form").steps({
        bodyTag: "fieldset",
        onStepChanging: function (event, currentIndex, newIndex)
        {

            ////////////////////////////////////////


            ////////////////////////////////////////


            // Always allow going backward even if the current step contains invalid fields!
            if (currentIndex > newIndex)
            {
                return true;
            }

            // Forbid suppressing "Warning" step if the user is to young
            if (newIndex === 3 && Number($("#age").val()) < 18)
            {
                return false;
            }

            var form = $(this);

            // Clean up if user went backward before
            if (currentIndex < newIndex)
            {
                // To remove error styles
                $(".body:eq(" + newIndex + ") label.error", form).remove();
                $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
            }

            // Disable validation on fields that are disabled or hidden.
            form.validate().settings.ignore = ":disabled,:hidden";

            // Start validation; Prevent going forward if false
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex)
        {
            // Suppress (skip) "Warning" step if the user is old enough.
            if (currentIndex === 2 && Number($("#age").val()) >= 18)
            {
                $(this).steps("siguiente");
            }

            // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
            if (currentIndex === 2 && priorIndex === 3)
            {
                $(this).steps("anterior");
            }
        },
        onFinishing: function (event, currentIndex)
        {
            var form = $(this);

            // Disable validation on fields that are disabled.
            // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
            form.validate().settings.ignore = ":disabled";

            // Start validation; Prevent form submission if false
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            var form = $(this);

            // Submit form input
            form.submit();
        }
    }).validate({
                errorPlacement: function (error, element)
                {
                    element.before(error);
                },
                rules: {
                        ano_creacion: {
                            esfecha: true
                        }
                }

            });
});

$.validator.addMethod("esfecha", esFechaActual, "El Año de Realización no debe ser mayor al Año Actual");


function esFechaActual(value, element, param) {

    
    var fechaActual = <?php echo date('Y');?>;

    if (value > fechaActual) {

        return false; //error de validación

    }

    else {

        return true; //supera la validación

    }

}

</script>


<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<!-- modal ver -->
<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form3").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    //form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [

                ]

            });

        });

    </script>
</body>
</html>
