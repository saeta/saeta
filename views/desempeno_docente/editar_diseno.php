<!--
*
*  INSPINIA - Arte o Software
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin | Diseño de Unidad Curricurar Docente</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

     <!--  style chosen lo lo puse -->
     <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

        <!-- jasny input mask-->
        <link href="<?php echo constant ('URL');?>src/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <!--  style select2 -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/select2/select2.min.css" rel="stylesheet">
    <!-- datapicker -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <!-- sweetalert input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">



</head>

<body>
    <?php require 'views/header.php'; ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Editar Diseño de Unidad Curricurar Docente</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="<?php echo constant('URL');?>home">Inicio</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="<?php echo constant ('URL');?>diseno_uc">Desempeño Docente UBV</a>
                </li>
                <li class="breadcrumb-item">
                    <strong>Listar Diseño de Unidad Curricurar Docente</strong>
                </li>
                <li class="breadcrumb-item active">
                    <a href="<?php echo constant ('URL');?>diseno_uc/viewEditar"> <strong>Editar </strong></a>
                </li>
            </ol>
    </div>
<div class="col-lg-2">
    <div class="title-action">
        <a href="<?php echo constant ('URL');?>diseno_uc" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Volver</a>
    </div>
</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>SIDTA</h5>
            </div>
<div class="ibox-content">
    <div id="respuesta"><?php echo $this->mensaje; session_start(); ?></div>
    <div class="alert alert-info">Todos los campos marcados con un (<span style="color: red;">*</span>) Son Obligatorios</div>

    <form class="m-t" role="form" id="form" method="post" action="<?php echo constant('URL') ."diseno_uc/editarDiseno/".$_SESSION['id_diseno_uc'];?>" class="wizard-big" enctype="multipart/form-data">
        <h1>Diseño de Unidad Curricurar Docente</h1>
    <fieldset>
        <h2>Información del Diseño de Unidad Curricurar Docente</h2>
        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <input id="id_diseno_uc" name="id_diseno_uc" type="hidden" value="<?php echo $this->disenos->id_diseno_uc; ?>" class="form-control">
                    <label>Diseño de Unidad Curricurar Docente<span style="color: red;">*</span></label>
                    <input id="nombre" name="nombre" type="text" placeholder="Ingrese la descripción del Diseño de Unidad Curricurar Docente" maxlength="70" class="form-control required" value="<?php echo $this->disenos->nombre; ?>" >
                </div>
                <div class="form-group" id="data_2">
                    <label>Año Creacion<span style="color: red;">*</span></label>
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input id="ano_creacion" name="ano_creacion" type="text" placeholder="Ingrese el Año de creacion" maxlength="5"class="form-control" value="<?php echo $this->disenos->ano_creacion; ?>" >
                        </div>
                        <label id="ano_creacion-error" class="error" for="ano_creacion"></label>
                    <script>
                        var mem = $('#data_2 .input-group.date').datepicker({
                            format: 'yyyy',
                            viewMode: "years",
                            minViewMode: "years",
                            autoclose: true
                        });
                    </script>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label>Unidad Curricular Tipo <span style="color: red;">*</span></label>
                        <select class="form-control required m-b " tabindex="2" data-placeholder="Elige el Tipo..."  name="id_unidad_curricular_tipo" id="id_unidad_curricular_tipo">
                            <option value="">Seleccione</option>
                                <?php include_once 'models/datosacademicos/tipouc.php';
                                    foreach($this->tipos as $row){
                                                $tipo_uc=new TipoUC();
                                                $tipo_uc=$row;?>
                            <option value="<?php echo $tipo_uc->id_unidad_curricular_tipo;?>"<?php if($tipo_uc->id_unidad_curricular_tipo == $this->disenos->id_unidad_curricular_tipo){ print "selected=selected"; }?>><?php echo $tipo_uc ->descripcion;?></option>
                                <?php }?>
                        </select>
                </div>
                <div class="form-group">
                    <label>Tramo <span style="color: red;">*</span></label>
                        <select class="form-control required m-b "  tabindex="2" data-placeholder="Elige el Tramo..." name="id_tramo"  id="id_tramo">
                            <option value="">Seleccione</option>
                                <?php include_once 'models/datosacademicos/tramo.php';
                                    foreach($this->tramos as $row){
                                                $tramo=new Tramo();
                                                $tramo=$row;?>
                            <option value="<?php echo $tramo->id_tramo;?>"<?php if($tramo->id_tramo == $this->disenos->id_tramo){ print "selected=selected"; }?>><?php echo  $tramo->descripcion;?></option>
                                <?php }?>
                        </select>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <input type="hidden" name="registrar2">
                </div>
                <div class="text-center">
                    <div style="margin-top: 20px">
                        <i class="fa fa-newspaper-o" style="font-size: 180px;color: #265491 "></i>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
        <h1>Diseño de Unidad Curricurar Docente</h1>
    <fieldset>
        <h2>Información del Diseño de Unidad Curricurar Docente</h2>
        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <label>Programa Formacion <span style="color: red;">*</span></label>
                        <select class="form-control required m-b" tabindex="2" data-placeholder="Elige el Programa..." name="id_programa" id="id_programa">
                                                <option value="">Seleccione</option>
                                                    <?php include_once 'models/datosacademicos/programa_formacion.php';
                                                    foreach($this->programas as $row){
                                                    $programa=new Programa();
                                                    $programa=$row;?>
                                                <option value="<?php echo $programa->id_programa;?>"<?php if($programa->id_programa == $this->disenos->id_programa){ print "selected=selected"; }?>><?php echo $programa->descripcion;?></option>
                                                <?php }?>
                                    </select>
                            </div>
                            <div class="form-group">
                                <label>Area del Conocimiento UBV<span style="color: red;">*</span></label>
                                    <select class="form-control required m-b " name="id_area_conocimiento_ubv" id="id_area_conocimiento_ubv">
                                                <option value="">Seleccione</option>
                                                    <?php include_once 'models/datosacademicos/areaubv.php';
                                                    foreach($this->areasubv as $row){
                                                    $area_ubv=new AreaUbv();
                                                    $area_ubv=$row;?>
                                                <option value="<?php echo $area_ubv->id_area_conocimiento_ubv;?>"<?php if($area_ubv->id_area_conocimiento_ubv == $this->disenos->id_area_conocimiento_ubv){ print "selected=selected"; }?>><?php echo $area_ubv->descripcion;?></option>
                                                <?php }?>
                                    </select>
                            </div>
                        </div>
                        <div class="col-lg-4">
                              <div class="text-center">
                                    <label>Ver Certificado Diseño <i>(Formato PDF)</i><span style="color: red;">*</span></label>
                                    <div class="form-group">
                                        <a target="_blank" href="<?php echo constant('URL') . "diseno_uc/viewDocumento/" . $this->disenos->id_diseno_uc;?>" class="btn btn-primary"><i class="fa fa-external-link"></i> Constancia</a>                                                         
                                        <a target="_blank" href="<?php echo constant('URL').'src/documentos/diseno/'.$this->disenos->descripcion_documento;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>
                                    </div>
                                <script></script>
                            </div>
                        </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <input type="hidden" name="registrar2">
                </div>
                <div class="text-center">
                    <div style="margin-top: 20px">
                        <i class="fa fa-newspaper-o" style="font-size: 180px;color: #265491 "></i>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>
    </form>

                </div>
            </div>
        </div>
    </div>
</div>
<?php require 'views/footer.php'; ?>

<!-- dataTables -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
 <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

 <!-- Steps -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

 <!-- Jquery Validate -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

 <!-- menu active -->
 <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
 <!-- Chosen -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>

  <!-- Sweet alert -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Input Mask-->
<script src="<?php echo constant ('URL');?>src/js/plugins/jasny/jasny-bootstrap.min.js"></script>

 <!-- Select2 -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/select2/select2.full.min.js"></script>

<!-- Data picker -->
<script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>

<!-- 
    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");  
    $("#re").addClass("active");
});   
</script>-->

<script>


$(document).ready(function(){


    $("#wizard").steps();
    $("#form").steps({
        bodyTag: "fieldset",
        onStepChanging: function (event, currentIndex, newIndex)
        {

            ////////////////////////////////////////


            ////////////////////////////////////////


            // Always allow going backward even if the current step contains invalid fields!
            if (currentIndex > newIndex)
            {
                return true;
            }

            // Forbid suppressing "Warning" step if the user is to young
            if (newIndex === 3 && Number($("#age").val()) < 18)
            {
                return false;
            }

            var form = $(this);

            // Clean up if user went backward before
            if (currentIndex < newIndex)
            {
                // To remove error styles
                $(".body:eq(" + newIndex + ") label.error", form).remove();
                $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
            }

            // Disable validation on fields that are disabled or hidden.
            form.validate().settings.ignore = ":disabled,:hidden";

            // Start validation; Prevent going forward if false
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex)
        {
            // Suppress (skip) "Warning" step if the user is old enough.
            if (currentIndex === 2 && Number($("#age").val()) >= 18)
            {
                $(this).steps("siguiente");
            }

            // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
            if (currentIndex === 2 && priorIndex === 3)
            {
                $(this).steps("anterior");
            }
        },
        onFinishing: function (event, currentIndex)
        {
            var form = $(this);

            // Disable validation on fields that are disabled.
            // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
            form.validate().settings.ignore = ":disabled";

            // Start validation; Prevent form submission if false
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            var form = $(this);

            // Submit form input
            form.submit();
        }
    }).validate({
                errorPlacement: function (error, element)
                {
                    element.before(error);
                },
                rules: {
                        ano_creacion: {
                            esfecha: true
                        }
                }

            });
});

$.validator.addMethod("esfecha", esFechaActual, "El Año de Realización no debe ser mayor al Año Actual");


function esFechaActual(value, element, param) {

    
    var fechaActual = <?php echo date('Y');?>;

    if (value > fechaActual) {

        return false; //error de validación

    }

    else {

        return true; //supera la validación

    }

}

</script>


    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>





</body>
</html>
