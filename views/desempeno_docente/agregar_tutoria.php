<!--
*
*  INSPINIA - Diseño de Unidad Curricular Docente
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin | Tutoria o Pasantia</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

     <!--  style chosen lo lo puse -->
     <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

        <!-- jasny input mask-->
        <link href="<?php echo constant ('URL');?>src/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <!--  style select2 -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/select2/select2.min.css" rel="stylesheet">
    <!-- datapicker -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <!-- sweetalert input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

</head>

<body>
    <?php require 'views/header.php'; ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Registrar Tutoria o Pasantia</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="<?php echo constant('URL');?>home">Inicio</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="<?php echo constant ('URL');?>tutoria">Desempeño Docente UBV</a>
                </li>
                <li class="breadcrumb-item">
                    <strong>Listar Tutoria o Pasantia</strong>
                </li>
                <li class="breadcrumb-item active">
                    <a href="<?php echo constant ('URL');?>tutoria/viewRegistrar"> <strong>Agregar </strong></a>
                </li>
            </ol>
    </div>
<div class="col-lg-2">
    <div class="title-action">
        <a href="<?php echo constant ('URL');?>tutoria" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Volver</a>
    </div>
</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>SIDTA</h5>
            </div>
        <div class="ibox-content">
            <div id="respuesta"><?php echo $this->mensaje; ?></div>
            <div class="alert alert-info">Todos los campos marcados con un (<span style="color: red;">*</span>) Son Obligatorios</div>

    <form id="form" method="post" action="<?php echo constant('URL');?>tutoria/registrarTutoria" class="wizard-big" enctype="multipart/form-data">
        <h1>Tutoria o Pasantia</h1>
    <fieldset>
        <h2> Información de la Tutoria o Pasantia</h2>
    <div class="row">
        <div class="col-lg-4">
            <div class="form-group">
                <label>Tutoria o Pasantia <span style="color: red;">*</span></label>
                <input id="descripcion" name="descripcion" type="text" placeholder="Ingrese la descripcion de la tutoria" maxlength="70" class="form-control required">
            </div>
            <div class="form-group" id="data_2">
                <label>Año tutoria<span style="color: red;">*</span></label>
                <div class="input-group date">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    <input id="ano_tutoria" name="ano_tutoria" type="text" placeholder="Seleccione el año de la tutoria" class="form-control required number">
                </div>
                <label id="ano_tutoria-error" class="error" for="ano_tutoria"></label>
            <script>
                var mem = $('#data_2 .input-group.date').datepicker({
                    format: 'yyyy',
                    viewMode: "years",
                    minViewMode: "years",
                    autoclose: true
                });
            </script>
            </div>
            <div class="form-group">
                <label>Periodo Lectivo<span style="color: red;">*</span></label>
                <select class="form-control required m-b "  name="periodo_lectivo" id="periodo_lectivo">
                    <option value="" selected="selected">Selecione el período que desea activar</option>
                        <option value="I" >Período I</option>
                        <option value="II" >Período II</option>
                </select>
            </div>
            <div class="form-group">
                <label>Nro. de Estudiantes <span style="color: red;">*</span></label>
                <input id="nro_estudiantes" name="nro_estudiantes" type="text" placeholder="Ingrese la cantidad de estudiantes" maxlength="70" class="form-control required">
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label>Eje Regional <span style="color: red;">*</span></label>
                    <select  name="id_eje_regional" id="id_eje_regional" class="chosen-select form-control required m-b" >
                        <option selected="selected" value="">Seleccione un Eje regional</option>
                            <?php include_once 'models/estructura.php';
                                foreach($this->regionales as $row){
                                        $eje_regional=new Estructura();
                                        $eje_regional=$row;?>
                        <option value="<?php echo $eje_regional->id_eje_regional;?>"><?php echo $eje_regional->descripcion;?></option>
                            <?php }?>
                    </select>
            </div>
            <div class="form-group">
                <label>Area del Conocimiento UBV <span style="color: red;">*</span></label>
                    <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige el Área..." name="id_area_conocimiento_ubv" id="id_area_conocimiento_ubv">
                        <option value="">Seleccione</option>
                            <?php include_once 'models/datosacademicos/areaubv.php';
                                foreach($this->areasubv as $row){
                                        $area_ubv=new AreaUbv();
                                        $area_ubv=$row;?>
                        <option value="<?php echo $area_ubv->id_area_conocimiento_ubv;?>"><?php echo $area_ubv->descripcion;?></option>
                            <?php }?>
                    </select>
            </div>
            <div class="form-group">
                <label>Linea de Investigacion<span style="color: red;">*</span></label>
                    <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige la Linea..." name="id_linea_investigacion" id="id_linea_investigacion">
                        <option value="">Seleccione</option>
                            <?php include_once 'models/datosacademicos/lineainvestigacion.php';
                                foreach($this->lineas as $row){
                                        $linea_investigacion=new LineaInvestigacion();
                                        $linea_investigacion=$row;?>
                        <option value="<?php echo $linea_investigacion->id_linea_investigacion;?>"><?php echo $linea_investigacion->descripcion;?></option>
                            <?php }?>
                    </select>
            </div>
            <div class="form-group">
                <label>Nucleo Académico <span style="color: red;">*</span></label>
                    <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige el Nucleo..." name="id_nucleo_academico" id="id_nucleo_academico">
                        <option value="">Seleccione</option>
                            <?php include_once 'models/datosacademicos/nucleo_academico.php';
                                foreach($this->nucleos as $row){
                                        $nucleo_academico=new Nucleo();
                                        $nucleo_academico=$row;?>
                        <option value="<?php echo $nucleo_academico->id_nucleo_academico;?>"><?php echo $nucleo_academico->descripcion;?></option>
                            <?php }?>
                    </select>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label>Centro de Estudios <span style="color: red;">*</span></label>
                    <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige el Centro..." name="id_centro_estudio" id="id_centro_estudio">
                        <option selected="selected" value="">Seleccione</option>
                            <?php include_once 'models/datosacademicos/centro_estudio.php';
                                foreach($this->centros as $row){
                                        $centro_estudio=new Centro();
                                        $centro_estudio=$row;?>
                        <option value="<?php echo $centro_estudio->id_centro_estudio;?>"><?php echo $centro_estudio->descripcion;?></option>
                            <?php }?>
                    </select>
            </div>
            <div class="form-group">
                <label>Programa Formación <span style="color: red;">*</span></label>
                    <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige el Programa..." name="id_programa" id="id_programa">
                        <option value="">Seleccione</option>
                            <?php include_once 'models/datosacademicos/programa_formacion.php';
                                foreach($this->programas as $row){
                                        $programa=new Programa();
                                        $programa=$row;?>
                        <option value="<?php echo $programa->id_programa;?>"><?php echo $programa->descripcion;?></option>
                            <?php }?>
                    </select>
            </div>
                                         <!-- <div class="form-group">
                                            <label>Estatus Verificación<span style="color: red;">*</span></label>
                                            <select class="form-control required m-b " name="estatus_verificacion" id="estatus_verificacion">
                                                <option value="">Seleccione</option>
                                                <option value="Verificado">Verificado</option>
                                                <option value="Sin Verificar">Sin Verificar</option>
                                            </select>
                                         </div> -->
            <div class="form-group">
                <input type="hidden" name="registrar">
            </div>
            <div class="text-center">
                <div style="margin-top: 20px">
                        <i class="fa fa-briefcase" style="font-size: 180px;color: #265491 "></i>
                </div>
            </div>
        </div>
    </div>
    </fieldset>
    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    <?php require 'views/footer.php'; ?>

   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    <!-- Chosen -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>

     <!-- Sweet alert -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Input Mask-->
<script src="<?php echo constant ('URL');?>src/js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <!-- Select2 -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/select2/select2.full.min.js"></script>

 <!-- Data picker -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<!--
    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");
    $("#re").addClass("active");
});
</script>-->



<script>


$(document).ready(function(){


    $("#wizard").steps();
    $("#form").steps({
        bodyTag: "fieldset",
        onStepChanging: function (event, currentIndex, newIndex)
        {

            ////////////////////////////////////////


            ////////////////////////////////////////


            // Always allow going backward even if the current step contains invalid fields!
            if (currentIndex > newIndex)
            {
                return true;
            }

            // Forbid suppressing "Warning" step if the user is to young
            if (newIndex === 3 && Number($("#age").val()) < 18)
            {
                return false;
            }

            var form = $(this);

            // Clean up if user went backward before
            if (currentIndex < newIndex)
            {
                // To remove error styles
                $(".body:eq(" + newIndex + ") label.error", form).remove();
                $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
            }

            // Disable validation on fields that are disabled or hidden.
            form.validate().settings.ignore = ":disabled,:hidden";

            // Start validation; Prevent going forward if false
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex)
        {
            // Suppress (skip) "Warning" step if the user is old enough.
            if (currentIndex === 2 && Number($("#age").val()) >= 18)
            {
                $(this).steps("siguiente");
            }

            // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
            if (currentIndex === 2 && priorIndex === 3)
            {
                $(this).steps("anterior");
            }
        },
        onFinishing: function (event, currentIndex)
        {
            var form = $(this);

            // Disable validation on fields that are disabled.
            // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
            form.validate().settings.ignore = ":disabled";

            // Start validation; Prevent form submission if false
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            var form = $(this);

            // Submit form input
            form.submit();
        }
    }).validate({
                errorPlacement: function (error, element)
                {
                    element.before(error);
                },
                rules: {
                        ano_tutoria: {
                            esfecha: true
                        }
                }

            });
});

$.validator.addMethod("esfecha", esFechaActual, "El Año de Realización no debe ser mayor al Año Actual");


function esFechaActual(value, element, param) {

    
    var fechaActual = <?php echo date('Y');?>;

    if (value > fechaActual) {

        return false; //error de validación

    }

    else {

        return true; //supera la validación

    }

}

</script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<!-- modal ver -->
<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form3").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    //form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [

                ]

            });

        });

    </script>
</body>
</html>
