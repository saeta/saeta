<script>
/*var myVar;

function myFunction() {

    myVar = setTimeout(alertFunc, 100000);
    
}

function alertFunc() {
  location.href='<?php echo constant('URL');?>login/logout';
    alert("Su Sesion ha estado inactiva durante 10 minutos, Su sesión se cerrara");

}*/

function myconfirm(){
  var con = confirm("¿Está Seguro que Desea Cerrar Sesión?");
  if(con==true){
        location.href='<?php echo constant('URL');?>login/logout';
  } else {
        location.href='#';
  }
}
</script>
 <!-- Toastr style -->
     <!-- favicon de sidta -->
     <link rel="shortcut icon" type="image/png" href="<?php echo constant ('URL');?>src/img/favicon_sidta.png"/>

    <link href="<?php echo constant ('URL');?>src/css/plugins/toastr/toastr.min.css" rel="stylesheet">
<div id="wrapper" style="background: linear-gradient(140deg, #358dd7, black);">
        <nav   class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <img style="width: 120px;" src="<?php echo constant ('URL');?>src/css/patterns/target.png" alt="" srcset="">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="block m-t-xs font-bold"><?php echo $_SESSION['descripcion'];?></span>
                                <span class="text-muted text-xs block"><?php echo $_SESSION['usuario'];?> <b class="caret"></b></span>
                            </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a class="dropdown-item" href="<?php echo constant('URL') ."perfil/render/".$_SESSION['id_persona']; ?>">Perfil</a></li>
                              
                                <li class="dropdown-divider"></li>
                                
                                <li>
                                    <a class="dropdown-item"onclick="myconfirm()">
                                        <i class="fa fa-sign-out"></i> Cerrar Sesión
                                    </a>
                            </li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            <img style="width: 38px;" src="<?php echo constant ('URL');?>src/img/favicon_sidta.png" alt="">
                        </div>
                    </li>
                    
                    <li class="">
                        <a href="<?php echo constant ('URL');?>home"> <i class="fa fa-home"></i> <span class="nav-label">Inicio</span> <span class="fa arrow"></span></a>
                       <!-- <ul class="nav nav-second-level collapse">
                            <li class=""><a href="<?php echo constant ('URL');?>home">Inicio</a></li>             
                        </ul>-->
                    </li>
                    <?php if($_SESSION['id_perfil']==4 || $_SESSION['id_perfil']==5 || $_SESSION['id_perfil']==9){?>
                    
                    <li>
                        <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Gestión de Usuarios</span> <span class="fa arrow"></span></a>


                         <ul class="nav nav-second-level collapse">
                         <!--<li><a href="<?php echo constant ('URL');?>roles">Roles</a></li>-->
                           <!--<li><a href="<?php echo constant ('URL');?>permisos">Permisos</a></li>-->
                           <li><a href="<?php echo constant ('URL');?>usuarios"> Usuarios</a></li>                        
                         
                        </ul>
                    </li>

                    
                    <li class="">
                    <a href="#"><i class="fa fa-database"></i> <span class="nav-label">Admin. del Sistema</span> <span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
    <li><a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Estructura UBV</span> <span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
                            <li>
                                <a href="#" id="damian">Ubicación Geográfica <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">

                                <li ><a href="<?php echo constant ('URL');?>pais">Editar País</a></li>
                                <li><a href="<?php echo constant ('URL');?>estado">Editar Estado</a></li>
                                <li><a href="<?php echo constant ('URL');?>municipio">Editar Municipio</a></li>
                                <li><a href="<?php echo constant ('URL');?>ejer">Editar Eje Regional</a></li>
                                <li><a href="<?php echo constant ('URL');?>ejem">Editar Eje Municipal</a></li>
                                <li><a href="<?php echo constant ('URL');?>parroquia">Editar Parroquia</a></li>
                                <li><a href="<?php echo constant ('URL');?>ambiente">Editar Ambiente</a></li>
                          
                                <li><a href="<?php echo constant ('URL');?>espacio">Editar Espacio</a></li>

                                <li><a href="<?php echo constant ('URL');?>tipoaldea">Editar Tipo aldea</a></li>


                                <li><a href="<?php echo constant ('URL');?>codarea">Editar Codigo Área Teléfono</a></li>                               
                                <li><a href="<?php echo constant ('URL');?>aldea">Editar Aldea</a></li>
                                <li><a href="<?php echo constant ('URL');?>aldeatelf">Editar Aldea Teléfono </a></li>


                                  </ul>
                            </li>
                            <li>                       
                            <a href="#" id="damian">Registrar estructura <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">

                            <!-- <li><a href="<?php echo constant ('URL');?>ciudad">Registrar Ciudad</a></li> -->
                        
                            <li><a href="<?php echo constant ('URL');?>estructuraf">Registrar estructura fisica</a></li>

                            <li><a href="<?php echo constant ('URL');?>espaciotipo">Registrar tipos de espacio</a></li>
                            </ul>
                    </li>
                                   
                         
                </ul>
                </li>
                <li>
                        <a href="#"><i class="fa fa-book"></i> <span class="nav-label">Datos Académicos</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a href="#" id="damian">Áreas de Conocimiento <span class="fa arrow"></span></a>

                                <ul class="nav nav-third-level">
                                    <li><a href="<?php echo constant ('URL');?>area_opsu">Editar Área de Conocimiento OPSU</a></li>
                                    <li><a href="<?php echo constant ('URL');?>area_ubv">Editar Área de Conocimiento</a></li>

                                </ul>
                            </li>

                            <li>
                                <a href="#" id="damian">Centros de Estudio <span class="fa arrow"></span></a>

                                <ul class="nav nav-third-level">
                                    <li><a href="<?php echo constant ('URL');?>centro_estudio">Editar Centro de Estudio</a></li>
                                    
                                    <li><a href="<?php echo constant ('URL');?>linea_investigacion">Registrar Línea de Investigación</a></li>
                                    <li><a href="<?php echo constant('URL');?>nucleoacademico">Editar Nucleo Académico</a></li>
                                    <li><a href="<?php echo constant('URL');?>centro_estudio/viewAssoc">Asociar Alto Nivel a Centros de Estudio</a></li>
                                    
                                </ul>
                            </li>
                            <li>
                                <a href="#" id="damian">Programas <span class="fa arrow"></span></a>

                                <ul class="nav nav-third-level">
                                    <li><a href="<?php echo constant ('URL');?>nivel_academico">Registrar Nivel Acadèmico</a></li>
                                    <li><a href="<?php echo constant ('URL');?>programa_nivel">Editar Nivel de Programa</a></li>
                                    <li><a href="<?php echo constant ('URL');?>programatipo">Editar Tipo de Programa</a></li>
                                    <li><a href="<?php echo constant ('URL');?>programaformacion">Editar Programa de Formación</a></li>
                                    <li><a href="<?php echo constant ('URL');?>aldea_programa">Asociar Programa ∈ Aldea</a></li>
                                    <li><a href="<?php echo constant('URL');?>gradoacademico">Editar Grado Acadèmico</a></li>
                                    
                                </ul>
                            </li>
                            <li>
                                <a href="#" id="damian">Unidades Curriculares <span class="fa arrow"></span></a>

                                <ul class="nav nav-third-level">
                                    <li><a href="<?php echo constant('URL');?>eje_formacion">Registrar Eje de Formación</a></li>
                                    <li><a href="<?php echo constant('URL');?>tipo_uc">Registrar Tipo Unidad Curricular</a></li>
                                    <!--<li><a href="<?php echo constant('URL');?>frecuencia">Registrar Frecuencia</a></li>-->
                                    <li><a href="<?php echo constant('URL');?>modalidad_malla">Editar Modalidad Adm. De Malla</a></li>
                                    <li><a href="<?php echo constant('URL');?>unidadcurricular">Registrar Unidad Curricular</a></li>
                                    <li><a href="<?php echo constant('URL');?>mallacurricular">Registrar Malla Curricular</a></li>

                                </ul>
                            </li>
                            <li>
                                <a href="#" id="damian">Comités <span class="fa arrow"></span></a>

                                <ul class="nav nav-third-level">
                                    <li><a href="<?php echo constant ('URL');?>comite_tipo">Definición Tipo de Comitè</a></li>
                                    <li><a href="<?php echo constant ('URL');?>comite_academico">Registrar Comité Académico PFA</a></li>
                                    <li><a href="<?php echo constant ('URL');?>comite_academico/comite_academico_registros">Registros</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#" id="damian">Servicios Generales <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li><a href="<?php echo constant('URL');?>mestudio">Editar Modalidad de Estudio</a></li>
                                    <li><a href="<?php echo constant('URL');?>turno">Editar Turno</a></li>
                                    <li><a href="<?php echo constant('URL');?>dedicacion">Registrar Dedicación</a></li>
                                    <li><a href="<?php echo constant ('URL');?>tipo_info">Tipo Información</a></li>
                                    <!-- <li><a href="<?php echo constant('URL');?>convenio">Registrar Convenio</a></li> -->
                                </ul>
                            </li>
                            <li>
                                <a href="#" id="damian">Historicos <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li><a href="<?php echo constant('URL');?>h_aldea_programa">Historia Asociar Programa ∈ Aldea </a></li>

                                </ul>
                            </li>
                        </ul>
                    </li>


                    <li>
                        <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Personas</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="<?php echo constant ('URL');?>nacionalidad">Editar Nacionalidad</a></li>
                            <li><a href="<?php echo constant ('URL');?>telefono">Editar Tipo Teléfono</a></li>
                            <li><a href="<?php echo constant ('URL');?>correo">Editar Tipo Correo</a></li>
                            <li><a href="<?php echo constant ('URL');?>documentotipo">Editar Tipo Documento de Identidad</a></li>
                            <li><a href="<?php echo constant ('URL');?>genero">Editar Género</a></li>
                            <li><a href="<?php echo constant ('URL');?>estadocivil">Editar Estado Civil</a></li>
                            <li><a href="<?php echo constant ('URL');?>tipodiscapacidad">Registrar Tipo de discapacidad</a></li>
                            <li><a href="<?php echo constant ('URL');?>discapacidad">Editar Discapacidad</a></li>
                            <li><a href="<?php echo constant ('URL');?>etnia">Editar Etnia Indígena</a></li>
                        </ul>
                    </li>

    </ul>
</li>

                    <!-- <li>
                        <a href="#"><i class="fa fa-line-chart"></i> <span class="nav-label">Ingresos Propios</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li><a href="<?php echo constant ('URL');?>banco">Editar Banco</a></li>
                            <li><a href="<?php echo constant ('URL');?>cuentabancaria">Editar Tipo de Cuenta</a></li>
                            <li><a href="<?php echo constant ('URL');?>cuentau">Editar Cuenta Bancaria</a></li>
                            <li><a  href="<?php echo constant ('URL');?>arancel" >Editar Aranceles</a></li>
                            <li><a  href="<?php echo constant ('URL');?>pago" >Unidad de Pago</a></li>
                      
                        
                        </ul>
                    </li> -->

                    <li>
                        <a href="#"><i class="fa fa-clock-o"></i> <span class="nav-label">Activación de Modulos </span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                             <!-- <li><a href="<?php echo constant('URL');?>periodo">Periodo Académico</a></li>
                            <li><a href="<?php echo constant('URL');?>activacion"> Activar Módulo</a></li> -->
                            <li><a href="<?php echo constant('URL');?>activacion/viewAscenso"> Activar Ascensos</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo constant('URL'); ?>verificar"><i class="fa fa-eye"></i><span class="nav-label">Verificación de Datos</span> <i class="fa arrow"></i></a>
                    </li>
                    <li>
                        <a href="<?php echo constant('URL'); ?>perfil_docente"><i class="fa fa-repeat"></i><span class="nav-label">Sincronizar Con SIGAD</span> </a>
                    </li>

                    
                    
                    <?php } 
                    if($_SESSION['id_perfil']==9 || $_SESSION['id_perfil']==10  || $_SESSION['id_perfil']==11){//analista dgtit admin dgtit analista
                    ?>

<li>
                        <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Censo Docente </span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                          
                            <li> <a href="<?php echo constant('URL'); ?>censo_docente"><i class="fa fa-check"></i><span class="nav-label">Censar</span> </a></li>
                            <li> <a href="<?php echo constant('URL'); ?>reporte"><i class="fa fa-info"></i><span class="nav-label">Reporte del Censo</span> </a></li>
                        </ul>
                    </li>
                   

                    






                    <?php }//censo
                    if($_SESSION['id_perfil']==1){
                    ?>


                <!--actualizacion de datos generales -->
                    
                    <li>
                        <a href="#"><i class="fa fa-address-card"></i> <span class="nav-label">Datos Generales</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                        <li>
                                <a href="#" id="damian">Desempeño Docente UBV <span class="fa arrow"></span></a>

                                <ul class="nav nav-third-level">
                                    <li><a href="<?php echo constant ('URL');?>diseno_uc">Diseño Unidad Curricular Docente</a></li>
                                    <li><a href="<?php echo constant ('URL');?>tutoria">Tutoria</a></li>
                                    <li><a href="<?php echo constant ('URL');?>docencia_previa">Docencia Previa UBV</a></li>
                                    
                                </ul>
                            </li>
                            <li>
                                <a href="#" id="damian">Información Socio-Académica Docente <span class="fa arrow"></span></a>

                                <ul class="nav nav-third-level">
                                    <li><a href="<?php echo constant ('URL');?>comision_excedencia">Comisión Excedencia</a></li>
                                    
                                    

                                </ul>
                            </li>
                            <li>
                                <a href="#" id="damian">Produccion Intelectual <span class="fa arrow"></span></a>

                                <ul class="nav nav-third-level">
                                    <li><a href="<?php echo constant ('URL');?>articulo_revista">Registrar Artículo de Revista</a></li>
                                    <li><a href="<?php echo constant ('URL');?>registro_libro">Registrar libro</a></li>
                                    <li><a href="<?php echo constant ('URL');?>arte_software">Registrar Arte o Software</a></li>
                                    <li><a href="<?php echo constant ('URL');?>registro_ponencia">Registrar Ponencia</a></li>

                                </ul>
                            </li>



                            <li>
                                <a href="<?php echo constant ('URL');?>registro_reconocimiento" id="damian">Reconocimientos <span class="fa arrow"></span></a>
                            </li>


                            <li>
                                <a href="<?php echo constant ('URL');?>experiencia_laboral" id="damian">Experiencia Laboral <span class="fa arrow"></span></a>
                            </li>

                            
                            <li>
                                <a href="<?php echo constant('URL'); ?>datos_academicos" id="damian">Datos Académicos e Idiomas <span class="fa fa-book"></span></a>
                            </li>
                        </ul>
                    </li>
                        
                        <!-- end actualizacion de datos generales -->
                   
                        <li>
                            <a href="<?php echo constant('URL'); ?>concurso"><i class="fa fa-trophy"></i><span class="nav-label">Concurso</span> <span class="label label-danger float-right">¡Nuevo!</span></a>
                                
                        </li>
                        <?php } ?>
                        
                        <?php 

                            
                            
                            if($_SESSION['id_clasificacion_docente']!=2){


                                    $fecha_actual=date("Y-m-d");
                                
                                    //var_dump(!($fecha_actual < $_SESSION['fecha_inicio'] || $fecha_actual > $_SESSION['fecha_fin']));
                                    //negar la falsedad 
                                    if(!($fecha_actual < $_SESSION['fecha_inicio'] 
                                    || $fecha_actual > $_SESSION['fecha_fin'])){?>
                                    
                                    <li>
                                    <a href="#"><i class="fa fa-hand-o-up"></i><span class="nav-label">Ascensos</span> <span class="label label-info float-right">¡Nuevo!</span></a>
                                    <ul class="nav nav-second-level collapse">
                                    <li>
                                        <a href="<?php echo constant('URL'); ?>solicitar_ascenso" id="damian">Solicitudes de Ascenso </a>
                                        <?php if($_SESSION['id_perfil']==7 
                                                || $_SESSION['id_perfil']==2
                                                || $_SESSION['id_perfil']==3
                                                || $_SESSION['id_perfil']==4
                                                || $_SESSION['id_perfil']==5
                                                || $_SESSION['id_perfil']==6
                                                || $_SESSION['id_perfil']==9){?>
                                            <a href="<?php echo constant('URL'); ?>solicitar_ascenso/viewAdmin" id="damian">Solicitudes de Docentes</a>
                                            <a href="<?php echo constant('URL'); ?>propuesta_jurado" id="damian">Historia de Jurados</a>

                                        <?php } ?>

                                </li>
                                </ul>
                            </li>
                          <?php  }
                        }?>
                       

                    
            </div>
        </nav>








        <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-success " href="#"><i class="fa fa-bars"></i> </a>
            <!-- <form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Buscar algo..." class="form-control" name="top-search" id="top-search">
                </div>
            </form> -->
        </div>
        
            <ul class="nav navbar-top-links navbar-right">
                <li id="perf" style="padding: 20px">
                    <span class="m-r-sm text-muted welcome-message"><button class="btn btn-w-m btn-link"><a href="<?php echo constant('URL') . "perfil/render/" . $_SESSION['id_persona'];?>">Bienvenid@ <?php echo $_SESSION['primer_nombre']. " " .$_SESSION['primer_apellido'];?></a></button></span>
                </li>
                
                <li class="dropdown">
                    
                    <ul class="dropdown-menu dropdown-messages dropdown-menu-right">
                        <li>
                            <div class="dropdown-messages-box">
                                <a class="dropdown-item float-left" href="profile.html">
                                    <img alt="image" class="rounded-circle" src="img/a7.jpg">
                                </a>
                                <div class="media-body">
                                    <small class="float-right">46h ago</small>
                                    <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
                                    <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                                </div>
                            </div>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li>
                            <div class="dropdown-messages-box">
                                <a class="dropdown-item float-left" href="profile.html">
                                    <img alt="" class="rounded-circle" src="img/a4.jpg">
                                </a>
                                <div class="media-body ">
                                    <small class="float-right text-navy">5h ago</small>
                                    <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica Smith</strong>. <br>
                                    <small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>
                                </div>
                            </div>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li>
                            <div class="dropdown-messages-box">
                                <a class="dropdown-item float-left" href="profile.html">
                                    <img alt="image" class="rounded-circle" src="img/profile.jpg">
                                </a>
                                <div class="media-body ">
                                    <small class="float-right">23h ago</small>
                                    <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>
                                    <small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>
                                </div>
                            </div>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="mailbox.html" class="dropdown-item">
                                    <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!--<li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell"></i>  <span class="label label-success">8</span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="mailbox.html" class="dropdown-item">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                                    <span class="float-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li>
                            <a href="profile.html" class="dropdown-item">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="float-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li>
                            <a href="grid_options.html" class="dropdown-item">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="float-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="notifications.html" class="dropdown-item">
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>-->


                <li>

                    <a onclick="myconfirm()">
                            <i class="fa fa-sign-out"></i> Cerrar Sesión
                        </a>



                </li>
                <li>
                    <a class="right-sidebar-toggle">
                        <i class="fa fa-tasks"></i>
                    </a>
                </li>
            </ul>

        </nav>
        </div>
<style>

.ir-arriba {
	display:none;
	padding:10px;
	background:#168fd8;
	font-size:20px;
	color:#fff;
	cursor:pointer;
	position: fixed;
	bottom:20px;
	right:20px;
}
	
</style>
        