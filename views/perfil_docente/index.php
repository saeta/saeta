<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Cargar Trabajadores Académicos | SIDTA</title>


 
    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/dropzone/basic.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/dropzone/dropzone.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <!-- sweetalert input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">


</head>

<body>

   <?php require 'views/header.php'; ?>
   

   <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-5">
                    <h2>Perfil Docente</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Perfil Docente</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Cargar Trabajadores Académicos</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-7">
                    <div class="title-action">
                        <a href="<?php echo constant ('URL');?>views/perfil_docente/crear.php" class="btn btn-primary"><i class="fa fa-download"></i> Descargar Formato</a>

                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>SIDTA</h5>
                        <div class="ibox-tools">

                         

                        </div>
                    </div>
                    <div class="ibox-content">

                        <?php if($_SESSION['Agregar']==true){?>
                        <div class="alerts"><?php echo $this->mensaje; echo $this->mensaje2;?></div>
                        <div class="alert alert-info">Todos los campos marcados con un (<span style="color: red;">*</span>) Son Obligatorios</div>

                        <form id="form" method="POST" action="<?php echo constant ('URL');?>perfil_docente/cargar" enctype="multipart/form-data">
                            <div class="row jumbotron">
                            
                                <div class="col-md-6">
                                    <span>Delimitador<span style="color: red "> *</span></span>
                                    <div class="form-group">
                                    
                                        <select class="form-control required" name="delimiter" id="">
                                        <option value="">Seleccione</option>
                                            <option value=",">,</option>
                                            <option value=";">;</option>
                                            <option value=":">:</option>
                                            <option value="*">*</option>
                                            <option value="|">|</option>
                                            <option value=".">.</option>
                                            <option value="/">/</option>
                                            <option value="\">\</option>
                                            <option value="?">?</option>
                                            <option value="+">+</option>
                                        </select>
                                        <!-- <input type="text" name="delimiter" placeholder="Ingrese el delimitador del archivo .csv"> -->
                                    </div>
                                    <div class="form-group">
                                    <span>Cargar Archivo<span style="color: red "> *</span></span>
                                                <div class="form-group  form-control fileinput fileinput-new" data-provides="fileinput">
                                                    <span class="btn btn-default btn-file"><span class="fileinput-new">Examinar...</span>
                                                    <span class="fileinput-exists">Cambiar</span><input type="file" id="file_csv" accept=".csv" class="required" name="uploadedFile"/></span>
                                                    <span class="fileinput-filename"></span>
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                                    <label id="file_csv-error" class="error" for="file_csv"></label>
                                                </div>
                                    </div>
                                    <button type="submit" class="btn btn-success btn-lg btn-block" name="uploadBtn" value="Upload"><i class="fa fa-upload"></i> Enviar</button>
                                </div>
                                <div class="col-md-2">
                                
                                </div>
                                <div class="col-md-2">
                                <i class="fa fa-upload" style="font-size: 180px;color: #1d60c6 "></i>

                                </div>
 
                            </div>
                           
                             
                            <!--<input type="submit" name="uploadBtn" value="Upload" /> -->
                        </form>
                        <?php }else{?>
                            <div class="alert alert-warning"><i class="fa fa-exclamation-triangle"></i> Para tener Acceso a esta sección es necesario el Rol de Usuario de "<b>Agregar</b>"</div>
                        <?php } ?>

                    </div>
                </div>
            </div>
            </div>
        </div>

<?php require 'views/footer.php'; ?>
 <!-- script para validar peso y extension de los archivos -->
 <script>
                                $("#file_csv").change(function () {
                                    $('.error').text('');
                                    if(validarExtension(this)) { 
                                        if(validarPeso(this)) { 
                                        }
                                    }  
                                });
                            </script>
                            <!-- script para validar peso y extension de los archivos -->




    <!-- Jasny -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/jasny/jasny-bootstrap.min.js"></script>
    <!-- Sweet alert -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/sweetalert/sweetalert.min.js"></script>
    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

   
        <script>

           
                var extensionesValidas = ".csv";
                var pesoPermitido = 2000;//2MB = 2000 kb
                //console.log(pesoPermitido, extensionesValidas);
                // Validacion de extensiones permitidas
                function validarExtension(datos) {
                    
                    var ruta = datos.value;
                    var extension = ruta.substring(ruta.lastIndexOf('.') + 1).toLowerCase();
                    
                    var extensionValida = extensionesValidas.indexOf(extension);
                    
                    if(extensionValida < 0) {
                            $('.error').text('La extensión no es válida Su Archivo tiene de extensión: .'+ extension);
                            //alert('La extensión no es válida Su Archivo tiene de extensión: .'+ extension);
                        swal("Ha ocurrido un Error", "La extensión no es válida Su Archivo tiene de extensión: ."+extension, "error");  
                        document.getElementById("file_csv").value = "";
                        return false;
                    } else {
                        return true;
                    }
                }


                // Validacion de peso del fichero en kbs
                function validarPeso(datos) {
                if (datos.files && datos.files[0]) {
                    var pesoFichero = datos.files[0].size/2000;
                    if(pesoFichero > pesoPermitido) {
                        $('.error').text('El peso máximo permitido del Archivo es: ' + pesoPermitido + ' KBs Su fichero tiene: '+ pesoFichero +' KBs');
                        //alert('El peso maximo permitido del Archivo es: ' + pesoPermitido + ' KBs Su Archivo tiene: '+ pesoFichero +' KBs');
                        
                        swal("Ha ocurrido un Error","El peso máximo permitido del Archivo es: " + pesoPermitido + " KBs Su Archivo tiene: "+ pesoFichero +" KBs", "error");    
                        document.getElementById("file_csv").value = "";
                        
                        return false;
                    } else {
                        return true;
                    }
                }
            }
              

    </script>
   



 
<script type="text/javascript">
    $(function(){
        $('#form').validate();     
    });
</script>

   

</body>
</html>
