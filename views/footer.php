<div class="footer">
<span class="ir-arriba fa fa-angle-up"></span>
                <div class="float-right">
                    Independencia y <strong>Patria Socialista</strong> Viviremos y Venceremos.
                </div>
                <div>
                    <strong>UBV</strong> Universidad Bolivariana de Venezuela 2020
                </div>
            </div>
        </div>
</div>
  <!-- Mainly scripts -->
  <script src="<?php echo constant ('URL');?>src/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/popper.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/bootstrap.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

   
    <!-- Custom and plugin javascript -->
    <script src="<?php echo constant ('URL');?>src/js/inspinia.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/pace/pace.min.js"></script>
  
    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    

         <!-- Toastr -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/toastr/toastr.min.js"></script>

<!-- Idle Timer plugin -->
<script src="<?php echo constant ('URL');?>src/js/plugins/idle-timer/idle-timer.min.js"></script>

<script>

                    $(document).ready(function(){
                        $("#perf").removeClass('active');

                        $('.ir-arriba').click(function(){
                            $('body, html').animate({
                                scrollTop: '0px'
                            }, 300);
                        });
                    
                        $(window).scroll(function(){
                            if( $(this).scrollTop() > 0 ){
                                $('.ir-arriba').slideDown(300);
                            } else {
                                $('.ir-arriba').slideUp(300);
                            }
                        });
                    });
               
    /*$(document).ready(function () {

        // Set idle time
        $( document ).idleTimer( 1200000 );

    });

    $( document ).on( "idle.idleTimer", function(event, elem, obj){
        toastr.options = {
            "positionClass": "toast-top-right",
            "timeOut": 8000
        }

        

        toastr.warning('Su Sesión ha Expirado.','Idle time');
        
        $('.custom-alert').fadeIn();
        $('.custom-alert-active').fadeOut();


        location.href='<?php echo constant('URL');?>login/logout';

    });*/

</script>

