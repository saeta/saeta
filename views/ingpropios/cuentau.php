<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin | Cuenta Bancaria</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

 <!-- menu active -->
<script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>


</head>

<body>
    <?php require 'views/header.php'; ?>
    



    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Cuenta Bancaria</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.html">ingpropios UBV</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Cuenta Bancaria</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Editar Cuenta Bancaria</strong>
                        </li>
                    </ol>
                </div>  
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Cuentas registradas</h5>
                       
                        <div class="ibox-tools">
            
                        <a class="btn btn-primary" href="#myModal1" role="button" data-toggle="modal" 
                        data-id="<?php echo $cuenta->id_cuenta_bancaria;?>" data-nac="<?php echo $cuenta->descripcion;?>" 
                        data-tipo="<?php echo $cuenta->cuenta;?>"
                        data-banco="<?php echo $cuenta->banco;?>"
                        data-estatus="<?php echo $cuenta->estatus;?>"
                        
                         > 
                        
                        
                        
                        Agregar</a> &nbsp;


                        </div> 
                    </div>
                    
                    <div class="ibox-content">
                    <?php echo $this->mensaje; ?>
                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                   <!-- <th>nro</th>-->
                        <th>Banco</th>
                        <th>Tipo de Cuenta</th>
                        <th>Número de Cuenta</th>
                        <th>estatus</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php include_once 'models/ingpropios.php';
                            foreach($this->cuentas as $row){
                                $cuenta= new ingpropios();
                                $cuenta=$row;?>
                    <tr class="gradeX">
                    <td><?php echo $cuenta->banco1; ?></td>

                    <td><?php echo $cuenta->id_cuenta_bancaria; ?></td>
                
                    <td><?php echo $cuenta->descripcion; ?></td>

                    <td class="center" style="">
                    <?php if($cuenta->estatus==0){
                      echo"<center style=margin-top:3%;><span class='label label-danger' style='font-size: 12px;'>Inactivo</span></center>";
                        
                    }else{
                        echo"<center style=margin-top:3%;><span class='label label-primary' style='font-size: 12px;'>&nbsp; Activo &nbsp;</span></center>";
                    } ?>
                    </td>
                
                    
                    <td>  
                    
                    <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal" 
                    data-id2="<?php echo $cuenta->id_cuenta_bancaria;?>" data-id3="<?php echo $cuenta->id_cuenta_bancaria2;?>" data-nac2="<?php echo $cuenta->descripcion;?>" 
                        data-tipo2="<?php echo $cuenta->id_cuenta_bancaria;?>"
                        data-banco2="<?php echo $cuenta->banco1;?>"
                        data-estatus2="<?php echo $cuenta->estatus;?>"
                        
                         >  Editar</a> &nbsp;
                    
                     </td>
    
                    </tr>
                            <?php }?>

                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
        

        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Agregue una Cuenta </h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> Son obligatorios </small>
                                        </div>

                                        <div class="modal-body"  >
                                            
                                        <div class="ibox-content" >
                           

                            <form id="form" action="<?php echo constant('URL');?>cuentau/registrarcuentau"  method="post" class="wizard-big"  >
                                <h1>Cuenta Bancaria</h1>
                                <h3 style="text-align:center;color:red;">
                              
                                </h3>


                               
                                <fieldset style="height:395px;">
                                    <!--<h2 >Cuenta Información</h2>-->
                             <div class="alert alert-warning" style="margin-top:-3%;margin-left:5%;">
                                <h4><a class="alert-link" href="#" >Atencion!</a>   Por razones de seguridad Una vez registrada la 
                                cuenta Bancaria no podra eliminar el registro.</h4>
                            </div>

                                    
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <label>Cuenta *</label>

                                                <input id="cuenta" maxlength="24" style="text-align:center;" name="cuenta" type="text" class="form-control required" placeholder="0000 0000 0000 0000 0000">
                                                <script>
// Campo cuenta
var cuenta = document.getElementById('cuenta');

// Poner cursor en el campo cuenta
cuenta.focus();

cuenta.onkeydown = function(e){
	// Permitir la tecla para borrar
	if (e.key == 'Backspace') return true;

	// Permitir flecha izquierda
	if (e.key == 'ArrowLeft') return true;

	// Permitir flecha derecha
	if (e.key == 'ArrowRight') return true;

	// Bloquear tecla de espacio
	if (e.key == ' ') return false;

	// Bloquear tecla si no es un numero
	if (isNaN(e.key)) return false;
};

cuenta.onkeyup = function(){
	cuenta.value = cuenta.value
					// Borrar todos los espacios
					.replace(/\s/g, '')

					// Agregar un espacio cada dos cuenta
                    .replace(/([0-9]{4})/g, '$1 ')

					// Borrar espacio al final
					.trim();
};
</script>

                                                
                                           
                                            <br>
                                               <!--  <input id="cuenta" name="cuenta" type="text" class="form-control required" disabled >


                                               aqui me tengo que traer el nombre del cuenta 
                                                
                                                <input type="hidden" id="id_cuenta_bancaria" name="id_cuenta_bancaria" value="<?php echo $cuenta->id_cuenta_bancaria;?>" class="form-control" >-->
                                                <div class="form-group">
                                            <label>Tipo de Cuenta<span style="color: red;">*</span></label>
                                            <select class="form-control  required m-b " name="tipo_cuenta" id="tipo_cuenta">
                                       

                                            <option selected="selected" value="" >Seleccione un tipo de Cuenta</option> 
                                            <?php include_once 'models/ingpropios.php';
                                          foreach($this->cuentas2 as $row){
                                          $country=new ingpropios();
                                            $country=$row;?> 
                                            <option value="<?php echo $country->id_cuenta_bancaria;?>"><?php echo $country->descripcion;?></option>
                                            <?php }?>
                                               

                                            </select>   
                                            </select>
                                            </div>






                                                <div class="form-group">
                                            <label>banco<span style="color: red;">*</span></label>
                                            <select class="form-control  required m-b " name="banco" id="banco">
                                       

                                            <option selected="selected" value="">Seleccione una banco</option> 
                                            <?php include_once 'models/ingpropios.php';
                                          foreach($this->cuentas3 as $row){
                                          $country=new ingpropios();
                                            $country=$row;?> 
                                            <option value="<?php echo $country->id_cuenta_bancaria;?>"><?php echo $country->descripcion;?></option>
                                            <?php }?>
                                               

                                            </select>   
                                            </select>
                                            </div>
                                                
                                        
                                            <div class="form-group">
                                            <label>Estatus <span style="color: red;">*</span></label>
                                            <select class="form-control required m-b " name="estatus" id="estatus">
                                                <option   value ="1">Activo</option>
                                                <option selected="selected"  value="0">Inactivo</option>
                                               
                                            </select>   
                                           
                                            </div>









                                            </div>
                                        
                                         
                                        </div>
                                        
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-globe" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </fieldset>

                            

                          
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
         

        
             
<div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Edita una Cuenta</h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> Son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                           

                            <form id="form2" action="<?php echo constant('URL');?>cuentau/Actualizarcuentau"  method="post" class="wizard-big" >
                                <h1>Cuenta Bancaria</h1>
                                <fieldset style="height:380px;">
                                    <h2>Cuenta Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                
                                                <div class="form-group">
                                                <label>Cuenta * <span style="color: red;"></span></label>
                                                <input  id="cuenta2" maxlength="24" name="cuenta2"  style="text-align:center;" type="text" placeholder="0000 0000 0000 0000 0000" class="form-control required">
                                                </div>
                                                <script>
var cuenta2 = document.getElementById('cuenta2');

// Poner cursor en el campo cuenta2
cuenta2.focus();

cuenta2.onkeydown = function(e){
	// Permitir la tecla para borrar
	if (e.key == 'Backspace') return true;

	// Permitir flecha izquierda
	if (e.key == 'ArrowLeft') return true;

	// Permitir flecha derecha
	if (e.key == 'ArrowRight') return true;

	// Bloquear tecla de espacio
	if (e.key == ' ') return false;

	// Bloquear tecla si no es un numero
	if (isNaN(e.key)) return false;
};

cuenta2.onkeyup = function(){
	cuenta2.value = cuenta2.value
					// Borrar todos los espacios
					.replace(/\s/g, '')

					// Agregar un espacio cada dos cuenta2
                    .replace(/([0-9]{4})/g, '$1 ')

					// Borrar espacio al final
					.trim();
};
</script>
                                                <input type="hidden" class="form-control required m-b " name="id_cuenta_bancaria" id="id_cuenta_bancaria">
                                         
                                              


                                            <div class="form-group">
                                                <label>tipo de Cuenta* <span style="color: red;"></span></label>
                                                <input  id="banco2" name="id_banco2" type="text" class="form-control required" readonly="readonly" disabled>
                                                </div>
                                               
                                                <div class="form-group">
                                                <label>banco* <span style="color: red;"></span></label>
                                                <input id="tipo_cuenta2" name="tipo_cuenta2" type="text" readonly="readonly" class="form-control required" disabled>
                                                </div>
                                               
                                                <div class="form-group">
                                                <label>Estatus <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " name="estatus2" id="estatus2">
                                                    <option  value="1">Activo</option>
                                                    <option selected="selected" value="0">Inactivo</option>
                                                   
                                                </select>   
                                                </select>
                                                </div>
                                          
                                        </div>
                                        
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-globe" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </fieldset>
                          
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
         








    <?php require 'views/footer.php'; ?>

   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

<!-- menu active -->
<script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>

    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>
    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
$('#myModal1').on('show.bs.modal', function(e) {
    
  var product = $(e.relatedTarget).data('id');
  $("#id_cuenta_bancaria").val(product);

  var product2 = $(e.relatedTarget).data('nac');
  $("#cuenta").val(product2);

  var product3 = $(e.relatedTarget).data('tipo');
  $("#tipo_cuenta").val(product3);

  var product4 = $(e.relatedTarget).data('banco');
  $("#banco").val(product4);

  var product5 = $(e.relatedTarget).data('estatuss');
  $("#estatuss").val(product5);



});
</script>


<script>
$('#myModal2').on('show.bs.modal', function(e) {

    var product = $(e.relatedTarget).data('id2');
    $("#id_cuenta_bancaria2").val(product);
  
    var product2 = $(e.relatedTarget).data('nac2');
    $("#cuenta2").val(product2);
  
    var product3 = $(e.relatedTarget).data('tipo2');
    $("#tipo_cuenta2").val(product3);
  
    var product4 = $(e.relatedTarget).data('banco2');
    $("#banco2").val(product4);

    var product5 = $(e.relatedTarget).data('id3');
    $("#id_cuenta_bancaria").val(product5);

    var product6 = $(e.relatedTarget).data('estatus2');
  $("#estatus2").val(product6);
});
</script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>




</body>
</html>
