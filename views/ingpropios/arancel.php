<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin | Aranceles</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">



    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/cropper/cropper.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">


</head>

<body>
    <?php require 'views/header.php'; ?>
    



    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Aranceles</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.html">ingpropios UBV</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Aranceles</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Editar Aranceles</strong>
                        </li>
                    </ol>
                </div>  
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Aranceles</h5>
                       
                        <div class="ibox-tools">
            
                        <a class="btn btn-primary" href="#myModal1" role="button" data-toggle="modal"
                        
                        data-id="<?php echo $arancel->id_arancel;?>"
                        data-arancel="<?php echo $arancel->descripcion;?>"
                        data-fechai="<?php echo $arancel->fechai;?>"
                        data-fechaf="<?php echo $arancel->fechaf;?>"
                        data-estatus="<?php echo $arancel->estatus;?>"
                        
                        >Agregar</a> &nbsp;


                        </div> 
                    </div>
                    
                    <div class="ibox-content">
                    <?php echo $this->mensaje; ?>
                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                   <!-- <th>nro</th>-->
                        <th>Codigo Unico</th>
                        <th>Arancel</th>
                        <th>fecha Inicio</th>
                        <th>fecha Fin</th>
                        <th>Cantidad</th>
                        <th>Estatus</th>
                        <th>Dirigido a</th>
                        <th>opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php include_once 'models/ingpropios.php';
                            foreach($this->aranceles as $row){
                                $arancele= new ingpropios();
                                $arancele=$row;
                                
                                $newDatei = date("d/m/Y", strtotime($arancele->fechai));
                                $newDatef = date("d/m/Y", strtotime($arancele->fechaf));
                                $arancele->cantidad;
                              /* $cantidad2= number_format($arancele->cantidad,2);*/
                               $cantidad2= $arancele->cantidad;

                               ?>
                    <tr class="gradeX">
                  
                    <td><?php echo $arancele->codigo; ?></td>
                    <td><?php echo $arancele->arancel ;?></td>
                    <td><?php echo $newDatei?></td>
                    <td><?php echo $newDatef?></td>
                
                    <td><?php echo $cantidad2 ?></td>
                                  
                    <td class="center" style="">
                    <?php if($arancele->estatus==0){
                      echo"<center style=margin-top:3%;><span class='label label-danger' style='font-size: 12px;'>Inactivo</span></center>";
                        
                    }else{
                        echo"<center style=margin-top:3%;><span class='label label-primary' style='font-size: 12px;'>&nbsp; Activo &nbsp;</span></center>";
                    } ?>
                    </td>

                    <td>
                    <?php if($arancele->modo=='e'){
                      echo"<center style=margin-top:3%;><span class='text-warning' style='font-size: 16px;'> Extranjeros(a)</span></center>";
                        
                    }else{
                        echo"<center style=margin-top:3%;><span class='text-danger' style='font-size: 16px;'>&nbsp; Venezolanos(a) &nbsp;</span></center>";
                    } ?>






                    </td>
                    
                    <td>  
                    
                    <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal"
                     
                    data-id2="<?php echo $arancele->id_arancel;?>"
                        data-arancel2="<?php echo $arancele->arancel;?>"
                        data-fechai2="<?php echo $arancele->fechai;?>"
                        data-fechaf2="<?php echo $arancele->fechaf;?>"
                        data-estatus2="<?php echo $arancele->estatus;?>"
                        data-modo2="<?php echo $arancele->modo;?>"
                        data-codigo2="<?php echo $arancele->codigo;?>"
                        data-cantidad2="<?php echo $arancele->cantidad;?>"
                    >Editar</a> &nbsp;
                    
                     </td>
    
                    </tr>
                            <?php }?>

                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
        

        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Agregue un arancel </h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> Son obligatorios </small>
                                        </div>
                                        <div class="modal-body"  >
                                            
                                        <div class="ibox-content" >
                           
                            <form id="form" action="<?php echo constant('URL');?>arancel/registrararancel"  method="post" class="wizard-big"  >
                                <h1>Descripcion</h1>
                                <fieldset style="height:150px;">
                                    <h2>Arancel Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                            <label>Codigo Unico *</label>
                                            <input id="codigo" maxlength="10" style="text-align:center;"  name="codigo" type="text" placeholder="codigo" class="form-control required" placeholder="">
                                               
                                            <label>Arancel *</label>

                                                <input id="arancel" onkeypress="return soloLetras(event)" maxlength="45" style="text-align:center;"  name="arancel" type="text" placeholder="Arancel" class="form-control required" placeholder="">

                                                <div class="form-group" id="data_5">
                                                <label class="font-normal">Fecha de Inicio <span style="color: red;">*</span></label>
                                                <label class="font-normal" style=" position: relative;left: 110px;">Fecha de Fin <span style="color: red;">*</span></label>
                                                <div class="input-daterange input-group" id="datepicker">
                                                <input type="text" class="form-control-sm form-control required" name="fechai"  id="fechai" value="<?php echo date('d/m/Y');?>">
                                                <span class="input-group-addon">a</span>
                                                <input type="text" class="form-control-sm form-control required" name="fechaf"  id="fechaf" value="<?php echo date('d/m/Y');?>" >
                                                    </div>
                                                </div>
                                          
                                            


                                            <br>
                                              



                                            </div>
                                        
                                         
                                        </div>
                                        
                                        

                                    </div>

                                </fieldset>

                                <h1>Arancel</h1>
                                <fieldset style="height:150px;">
                                    <h2>Arancel Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                       
                                          
                                            <label>Cantidad *</label>
                                            <input maxlength="13" style="text-align:center;"   id="cantidad" name="cantidad" placeholder="cantidad" type="text" class="form-control required" placeholder="">
                                        <script>
                                            $("#cantidad").on({
                                "focus": function (event) {
                                 $(event.target).select();
                                                     },
                                 "keyup": function (event) {
                               $(event.target).val(function (index, value ) {
                              return value.replace(/\D/g, "")
                             .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                             .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
                                 });
                          }
                                });
                                        
                                        </script>
                                           <!-- estar pendiente con el nmobre de los estatus para que pueda mostrarme el dato-->
                                            <div class="form-group">
                                                <label>Estatus <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " name="estatus" id="estatus" value="">
                                                    <option  value="1">Activo</option>
                                                    <option selected="selected" value="0">Inactivo</option>
                                                   
                                                </select>   
                                                </select>
                                                </div>

                                              
                                           <!-- estar pendiente con el nmobre de los estatus para que pueda mostrarme el dato-->
                                            <div class="form-group">
                                                <label>Dirigido (a) <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " name="modo" id="modo" value="">
                                                    <option selected="selected" value="v">Venezolanos(a)</option>
                                                    <option  value="e">Extranjeros(a)</option>
                                                   
                                                </select>   
                                                </select>
                                                </div>


                                            <br>
                                              



                                            </div>
                                        
                                         
                                        </div>
                                        
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-ruble" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </fieldset>

                          
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
         
<div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Edite un arancel</h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> Son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                           

                            <form id="form2" action="<?php echo constant('URL');?>arancel/actualizararancel"  method="post" class="wizard-big" >
                                <h1>Descripcion</h1>
                                <fieldset style="height:100px;">
                                    <h2>Arancel Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                

                                       
                                                <input id="id_arancel2" name="id_arancel2" type="hidden" class="form-control required">


                                                <div class="form-group">
                                                <label>Codigo*<span style="color: red;"></span></label>
                                                <input  maxlength="10" style="text-align:center;"    id="codigo2" name="codigo2" type="text" class="form-control required" readonly="readonly" value="">
                                                </div>


                                                <div class="form-group">
                                                <label>Arancel* <span style="color: red;"></span></label>
                                                <input onkeypress="return soloLetras(event)" style="text-align:center;"  maxlength="45" id="arancel2" name="arancel2" type="text" class="form-control required" value="">
                                                </div>

                                                
   
                                            <br>
                                  
                                <div class="form-group" id="data_6">
                                <label class="font-normal">Fecha de Inicio <span style="color: red;">*</span></label>
                                <label class="font-normal" style=" position: relative;left: 110px;">Fecha de Fin <span style="color: red;">*</span></label>
                                <div class="input-daterange input-group" id="datepicker">
                                <input type="text" class="form-control-sm form-control required" name="fechai2" id="fechai2" value="">
                                <span class="input-group-addon">a</span>
                                <input type="text" class="form-control-sm form-control required" name="fechaf2"  id="fechaf2" value="" >
                                    </div>
                                </div>
                                               
                                          
                                        
                                            </div>
                                          
                                        </div>
                                        
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-ruble" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                    </div>

                                </fieldset>
                                <h1>Arancel</h1>
                                
                                <fieldset style="height:150px;">
                                    <h2>Arancel Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                       
                                          
                                            <label>Cantidad *</label>
                                            <input id="cantidad2" maxlength="13" style="text-align:center;" name="cantidad2" placeholder="cantidad" type="text" value="" class="form-control required " placeholder=""> 
                                     <script>
                                            $("#cantidad2").on({
                                "focus": function (event) {
                                 $(event.target).select();
                                                     },
                                 "keyup": function (event) {
                               $(event.target).val(function (index, value ) {
                              return value.replace(/\D/g, "")
                             .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                             .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
                                 });
                          }
                                });
                                        
                                        </script>
                                           <!-- estar pendiente con el nmobre de los estatus para que pueda mostrarme el dato-->
                                            <div class="form-group">
                                                <label>Estatus <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " name="estatus2" id="estatus2" value="hola mundo"> 
                                                <option selected="selected" value="">Seleccione el estatus</option> 
                                                    <option  value="1">Activo</option>
                                                    <option selected="selected" value="0">Inactivo</option>
                                                </select>   
                                                </select>
                                                </div>

                                              
                                           <!-- estar pendiente con el nmobre de los estatus para que pueda mostrarme el dato-->
                                            <div class="form-group">
                                                <label>Dirigido (a) <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " name="modo2" id="modo2" value=" hola mundo">
                                                <option selected="selected" value="">Seleccione a quién va dirigido</option> 
                                                    <option selected="selected" value="v">Venezolanos(a)</option>
                                                    <option  value="e">Extranjeros(a)</option>
                                                   
                                                </select>   
                                                </select>
                                                </div>


                                            <br>
                                              



                                            </div>
                                        
                                         
                                        </div>
                                        
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-ruble" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </fieldset>
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>  

<!--////////////////////////////////-->
         







    <?php require 'views/footer.php'; ?>


   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/val_letras.js"></script>
    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

<!-- menu active -->
<script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
<!-- Data picker -->
<script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>


 <!-- Image cropper -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/cropper/cropper.min.js"></script>

 
 <!-- Tags Input -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
<!-- Solo Letras -->
<script src="<?php echo constant ('URL');?>src/js/val_letras.js"></script>



<!-- Data picker -->
<script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>


 <!-- Image cropper -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/cropper/cropper.min.js"></script>

 
 <!-- Tags Input -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>




    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>
    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
$('#myModal1').on('show.bs.modal', function(e) {


  $('.tagsinput').tagsinput({
             tagClass: 'label label-primary'
         });

         $('#data_5 .input-daterange').datepicker({
             keyboardNavigation: false,
             forceParse: false,
             autoclose: true
         });
  

});
</script>


<script>
$('#myModal2').on('show.bs.modal', function(e) {
    var product = $(e.relatedTarget).data('id2');
    $("#id_arancel2").attr('value',product);
  
    var product1 = $(e.relatedTarget).data('arancel2');
    $("#arancel2").attr('value',product1);
  
    var product2 = $(e.relatedTarget).data('fechai2');
    $("#fechai2").attr('value',product2);
  
  
    var product3 = $(e.relatedTarget).data('fechaf2');
    $("#fechaf2").attr('value',product3);
  
  
    var product4 = $(e.relatedTarget).data('estatus2');
    $("#estatus2").val(product4);

    var product5 = $(e.relatedTarget).data('modo2');
    $("#modo2").val(product5);

    var product6 = $(e.relatedTarget).data('codigo2');
    $("#codigo2").attr('value',product6);

    var product7 = $(e.relatedTarget).data('cantidad2');
    $("#cantidad2").attr('value',product7);


    
    $('.tagsinput').tagsinput({
             tagClass: 'label label-primary'
         });

         $('#data_6 .input-daterange').datepicker({
             keyboardNavigation: false,
             forceParse: false,
             autoclose: true
         });

   

});
</script>



    <!-- Page-Level Scripts -->
      <!-- Page-Level Scripts -->
      <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>





</body>
</html>
