<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin | Unidad de Pago</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

 <!-- menu active -->
<script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
<!-- Solo Letras -->
<script src="<?php echo constant ('URL');?>src/js/val_letras.js"></script>

</head>

<body>
    <?php require 'views/header.php'; ?>
    



    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Unidad de Pago</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.html">ingpropios UBV</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Unidad de Pago</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Editar Unidad de Pago</strong>
                        </li>
                    </ol>
                </div>  
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Unidad de pagos registradas</h5>
                       
                        <div class="ibox-tools">
            
                        <a class="btn btn-primary" href="#myModal1" role="button" data-toggle="modal" 
                        
                        
                        >Agregar</a> &nbsp;
                       

                        </div> 
                    </div>
                    
                    <div class="ibox-content">
                    <?php echo $this->mensaje; ?>
                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                   <!-- <th>Nro</th>-->
                  <th>Unidad de pago</th>
                  <th>monto</th>
                  <th>Fecha de Registro</th>
                  <th>hora de Registro</th>
                  <th>Estatus</th>
                  <th>Opciones</th>
                        
                    </thead>
                    <tbody>
                 
                    <?php include_once 'models/ingpropios.php';
                            foreach($this->pagos as $row){
                                $pago= new ingpropios();
                                $pago=$row;

                                $pago->monto;
                                $monto2= $pago->monto;
                                /*$monto2= number_format($pago->monto,2);*/
                                $fecha = date("d/m/Y", strtotime($pago->fecha_registro));
                                ?>
                    <tr class="gradeX">

                    
            
                    <td><?php echo $pago->descripcion; ?></td>
                    <td><?php echo $monto2;?></td>
                    <td><?php echo $fecha;?></td>
                    <td><?php echo $pago->hora_registro;?></td>
                    <td class="center" style="">
                    <?php if($pago->estatus==0){
                      echo"<center style=margin-top:3%;><span class='label label-danger' style='font-size: 12px;'>Inactivo</span></center>";
                        
                    }else{
                        echo"<center style=margin-top:3%;><span class='label label-primary' style='font-size: 12px;'>&nbsp; Activo &nbsp;</span></center>";
                    } ?>
                    </td>
                    
                    <td>  
                    <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal"  data-id="<?php echo $pago->id_unidad_pago;?>"  data-pago="<?php echo $pago->descripcion;?> " data-monto="<?php echo $pago->monto;?> "  data-estatus2="<?php echo $pago->estatus;?>"> Editar</a> &nbsp;
                  
                     </td>
    
                    </tr>
                        

                    <?php }?>
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>

        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Agregue una unidad de Pago</h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> Son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                           

                            <form id="form" action="<?php echo constant('URL');?>pago/registrarpago"  method="post" class="wizard-big" >
                                <h1>Descripcion</h1>
                                <fieldset style="height:100px;">
                                    <h2> Unidad de pago Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <label>Unidad de pago *</label>

                                                <input id="pago" style="text-align:center;" onkeypress="return soloLetras(event)" placeholder="Unidad de pago" maxlength="45" name="pago" type="text" class="form-control required">
                                            <br>
                                            <label>Monto *</label>
                                            <input maxlength="13" style="text-align:center;" id="monto" name="monto" type="text" placeholder="Monto"  class="form-control required ">
                                            <script>
                                            $("#monto").on({
                                "focus": function (event) {
                                 $(event.target).select();
                                                     },
                                 "keyup": function (event) {
                               $(event.target).val(function (index, value ) {
                              return value.replace(/\D/g, "")
                             .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                             .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
                                 });
                          }
                                });
                                        
                                        </script>
                                        </div>
                                            
                                        
                                            <div class="form-group">
                                                <label>Estatus <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " name="estatus" id="estatus">
                                                    <option  value="1">Activo</option>
                                                    <option selected="selected" value="0">Inactivo</option>
                                                   
                                                </select>   
                                                </select>
                                                </div>





                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                 
                                                    <i class="fa fa-bank" style="font-size:180px;color: #e5e5e5;margin-top:5%; "></i>
                                                </div>
                                            </div>
                                        
                                    </div>

                                </fieldset>
                          
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
         

        
             
<div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Edite una Unidad Tributaria</h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> Son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                           

                            <form id="form2" action="<?php echo constant('URL');?>pago/Actualizarpago"  method="post" class="wizard-big" >
                                <h1>Descripcion</h1>
                                <fieldset style="height:100px;">
                                    <h2> pago Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                            <input id="id_unidad_pago" name="id_unidad_pago" type="hidden" class="form-control required">
                                            
                                            <input id="pago2" style="text-align:center;" onkeypress="return soloLetras(event)" maxlength="45" name="pago2" type="text" class="form-control required">
                                            <br>
                                            <label>monto *</label>
                                            <input id="monto2" style="text-align:center;" maxlength="13" name="monto2" type="text" class="form-control required ">
                                            <script>
                                            $("#monto2").on({
                                "focus": function (event) {
                                 $(event.target).select();
                                                     },
                                 "keyup": function (event) {
                               $(event.target).val(function (index, value ) {
                              return value.replace(/\D/g, "")
                             .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                             .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
                                 });
                          }
                                });
                                        
                                        </script>
                                        </div>
                                        
                                            <div class="form-group">
                                                <label>Estatus <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " name="estatus2" id="estatus2">
                                                    <option  value="1">Activo</option>
                                                    <option selected="selected" value="0">Inactivo</option>
                                                   
                                                </select>   
                                                </select>
                                                </div>
                                            
                                               
                                          
                                        
                                          
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                
                                                    <i class="fa fa-bank" style="font-size:180px;color: #e5e5e5;margin-top:5%; "></i>
                                                </div>
                                            </div>
                                        
                                    </div>

                                </fieldset>
                          
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
         








    <?php require 'views/footer.php'; ?>

   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>
<!-- menu active -->
<script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
<!-- Solo Letras -->
<script src="<?php echo constant ('URL');?>src/js/val_letras.js"></script>

    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>
    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
$('#myModal1').on('show.bs.modal', function(e) {
    
 

});
</script>


<script>
$('#myModal2').on('show.bs.modal', function(e) {

  var product = $(e.relatedTarget).data('id');
  $("#id_unidad_pago").val(product);

  var product2 = $(e.relatedTarget).data('pago');
  $("#pago2").val(product2);

  var product3 = $(e.relatedTarget).data('monto');
  $("#monto2").val(product3);

  var product4 = $(e.relatedTarget).data('estatus2');
    $("#estatus2").val(product4);
});
</script>

    <!-- Page-Level Scripts -->
   <!-- Page-Level Scripts -->
   <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>




</body>
</html>
