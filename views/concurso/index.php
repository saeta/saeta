<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SIDTA | Concursos</title>


 
    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">


</head>

<body>
 
   <?php require 'views/header.php'; ?>
   
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-5">
                    <h2>Concursos</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Concurso</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Listar Concursos</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-7">
                    <div class="title-action tooltip-demo">
                    <?php if(count($this->concursos)>=1){ ?>

                        <?php } else{ ?>
                            <a href="<?php echo constant ('URL');?>concurso/viewAdd" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</a>
                            <?php } ?>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>SIDTA</h5>
                        <div class="ibox-tools">

                 

                        </div>
                    </div>
                    <div class="ibox-content">
                    <?php echo $this->mensaje;?>
                        <!--  contenido -->
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Fecha Concurso</th>
                        <th>Escalafón</th>
                        
                        <th>Acción</th>
                    </tr>
                    </thead >
                    <tbody id="tbody-diseno">
                    <?php 
                            foreach($this->concursos as $row){
                                $concurso= new Estructura();
                                $concurso=$row;?>
                    <tr id ="fila-<?php echo $concurso->id_concurso; ?>" class="gradeX">
                        <td><?php echo $concurso->fecha_concurso; ?> </td>
                        <td><?php echo $concurso->escalafon; ?> </td>
                        
                        <td>
                        <a class="btn btn-outline btn-success" href="<?php echo constant ('URL') . "concurso/viewEdit/" . $concurso->id_concurso;?>" role="button"><i class="fa fa-edit"></i> Editar</a>&nbsp;
                        <a class="btn btn-outline btn-info" href="<?php echo constant ('URL') . "concurso/viewVer/" . $concurso->id_concurso;?>" role="button"><i class="fa fa-edit"></i> Ver</a>&nbsp;
                            <a class="btn btn-outline btn-danger" href="<?php echo constant('URL') . 'concurso/delete/' . $concurso->id_concurso;?>" role="button"><i class="fa fa-trash"></i> Remover</a>
                        </td>
                    </tr>
                            <?php }?>
                    </tbody>
                    </table>
                        <!--  end contenido -->

                    </div>
                </div>
            </div>
            </div>
        </div>



    <?php require 'views/footer.php'; ?>

     

    <!-- dataTables Scripts -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>

   

</body>
</html>
