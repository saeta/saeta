<!--
*
*  INSPINIA - Concursos
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin | Concursos</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

     <!--  style chosen lo lo puse -->
     <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

 
    <!-- jasny input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <!--  style select2 -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/select2/select2.min.css" rel="stylesheet">
    <!-- datapicker -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <!-- sweetalert input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">


</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                <h2>Editar Concursos</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                        <a href="<?php echo constant('URL');?>/concurso">concurso</a>
                        </li>
                        <li class="breadcrumb-item">
                        <a href="<?php echo constant('URL');?>concurso/viewEdit"><strong>Editar Concurso</strong></a>
                        </li>
            
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">                                                                                                                                                                                                                                                                                                                                     

          

                <div class="ibox ">                                                                                                                                                                 
                    <div class="ibox-title">
                        <h5> Detalle del Concurso </h5>
                        <div class="ibox-tools">                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                        <!-- boton agregar-->
                            <a href="<?php echo constant('URL');?>concurso"> <button type="button" class="btn btn-primary" >
                            <i class="fa fa-arrow-left"></i> Volver
                            </button>  </a>                                                                                               


                        </div>
                    </div>
                    
                    <div class="ibox-content">
                    <div id="respuesta"><?php echo $this->mensaje;?></div>
                    <div class="alert alert-info">Todos los campos marcados con un (<span style="color: red;">*</span>) Son Obligatorios</div>

                    <form id="form" method="post" action="<?php echo constant('URL')."concurso/editarConcurso/".$this->id_concurso;?>" class="wizard-big">
                                <h1>Concursos</h1>
                                <fieldset>
                                    <h2> Información del Concurso</h2>
                                    <div class="row">
                                        <div class="col-lg-8">



                                        <div class="form-group">
                                        <label>escalafon <span style="color: red;">*</span></label>
                                        <select name="escalafon" id="escalafon" style="width: 100%;"  class="form-control required select2_demo_3" tabindex="4">
                                            <option value="">Seleccione el escalafon</option>
                                           
                                  
                                            
                                            <?php 


                                                foreach($this->escalafones as $row){
                                                    $escalafon=new Estructura();
                                                    $escalafon=$row;?> 
                                                <option value="<?php echo $escalafon->id;?>"<?php if($escalafon->id==$this->concursos->id_escalafon){ print "selected=selected";}?>> <?php echo $escalafon->descripcion;?></option>

                                                <?php }?>                                      
                                        </select>
                                    </div>
                                   

                                        <div class="col-lg-6">
                                                <div class="form-group" id="data_2">
                                                <label>Fecha de Concurso <span style="color: red;">*</span></label>
                                                <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <input type="text"  style="margin-bottom: 5px;"  name="fecha_concurso" id="fecha_concurso" class="form-control required" placeholder="Seleccione la fecha en que concurso" value="<?php echo $this->concursos->fecha_concurso ?>">
                                                </div>
                                                <span><i>mm/dd/aaaa</i></span>
                                                <label id="ano_r-error" class="error" for="ano_r"></label>
                                            </div>
                                            <script>
                                            var mem = $('#data_2 .input-group.date').datepicker({
                                                autoclose: true
                                                
                                            });
                                            </script>
                                                
                                            </div>

                                  <div class="col-lg-6">
                                    <h2><strong>Resolución de Concurso</strong></h2>
                                        <a target="_blank" href="<?php echo constant('URL') . "concurso/viewDocumento/" . $this->concursos->id_concurso;?>" class="btn btn-primary"><i class="fa fa-external-link"></i> Constancia</a>                                                         
                                        <a target="_blank" href="<?php echo constant('URL').'src/documentos/concursos/'. $this->concursos->documento;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>
                                </div>


                           
                                   <!-- <input id="id_concurso" name="id_concurso" type="hidden"  value="<?php echo $this->concursos->id_concurso; ?>">
                                    <input type="hidden" name="registrar">-->
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">

                                            <div style="margin-top: 20px">
                                                    <i class="fa fa-eject" style="font-size: 100px;color: #e5e5e5 "></i>
                                                    
                                                </div>



                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-linux" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                            </fieldset>
                               
                            </form>

                    </div>
                </div>
            </div>
            </div>
        </div>



   




    <?php require 'views/footer.php'; ?>

   
    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    <!-- Chosen -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>




<!-- Sweet alert -->
<script src="<?php echo constant ('URL');?>src/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Input Mask-->
<script src="<?php echo constant ('URL');?>src/js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <!-- Select2 -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/select2/select2.full.min.js"></script>

 <!-- Data picker -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>

 <!-- Typehead -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>




<!-- 
    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");  
    $("#re").addClass("active");
});   
</script>-->



    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {true
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";true

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {true
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        }/*,
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            },
                            ano_arte:{
                                required: true
                            },
                            entidad_promotora: {
                                required: true
                            },
                            url_otro: {
                                url: true
                            }
                            
                        }*/
                    });
       });
    </script>


</body>
</html>
