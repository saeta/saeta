<!--
*
*  INSPINIA - Arte o Software
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <!-- martin <meta http-equiv="X-UA-Compatible" content="IE=edge">-->

    <title>SIDTA | Ver Concurso</title>

   
    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

     <!--  style chosen lo lo puse -->
     <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

</head>

<body>
<div id="wrapper">
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-9">
                <h2>Ver Concurso</h2>
                <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="<?php echo constant ('URL');?>home">Inicio</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="<?php echo constant ('URL');?>concurso">Listar Concursos</a>
                </li>
                <li class="breadcrumb-item active">
                    <a href="<?php echo constant ('URL');?>concurso/viewVer"><strong>Ver Concurso</strong></a>
                </li>
            </ol>
                </div>
                <div class="col-sm-3">
                <div class="title-action">
            
                  
                  <a href="<?php echo constant ('URL');?>concurso" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Volver</a>
                               


                            </div>     

                </div>
            </div>


            <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                    <h5>Información del Concurso</h5>
                        
                    </div>


                    <div class="ibox-content">
               
                   

                        <!--  contenido -->

                        <div class="row">
                 <div class="col-lg-12">
                 <div class="tabs-container">
                 <div class="tabs-right">
                        <ul class="nav nav-tabs">
                                 <li><a class="nav-link active" data-toggle="tab" href="#tab-8"> Datos del Concurso</a></li>
                                 </ul>
                                 <div class="tab-content">
                                 <div id="tab-8" class="tab-pane active">
                                 <div class="panel-body">
                       <!-- <table class="table table-striped table-bordered table-hover dataTables-example" >-->
                       

               
                      
                       <div class="row">
                                             <div class="col-lg-6">
                                             <h2> <strong>FECHA DE CONCURSO:</strong></h2>
                                             <td><?php echo $this->concursos->fecha_concurso; ?> </td>
                                             </div>

                                             
                                             <hr>
                                             <div class="col-lg-6">
                                             <h2> <strong>ESCALAFÓN:</strong></h2>
                                             <td><?php echo $this->concursos->escalafon; ?> </td>
                                             </div>

                                                <hr>
                                             <div class="col-lg-6">
                                            <h2><strong>Acta de Reconocimiento</strong></h2>
                                                <a target="_blank" href="<?php echo constant('URL') . "concurso/viewDocumento/" . $this->concursos->id_concurso;?>" class="btn btn-primary"><i class="fa fa-external-link"></i> Constancia</a>                                                         
                                                <a target="_blank" href="<?php echo constant('URL').'src/documentos/concursos/'. $this->concursos->documento;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>
                                             </div>  


                                             </div>
                                             
                          
                                            
                                               


                    </div>
                                 </div>

                    
                    </div>
                    </div>
                </div>
            </div>
            </div>
            <!--  end contenido -->
            </div>
        </div>
        </div>
        </div>
        </div>

                   



    <?php require 'views/footer.php'; ?>

   
      <!-- Steps -->
      <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

      <!-- Jquery Validate -->
      <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>
  
      <!-- menu active -->
      <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
      <!-- Chosen -->
      <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>
  
 <!-- 
    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");  
    $("#re").addClass("active");
});   
</script>-->



    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {true
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";true

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {true
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        }/*,
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            },
                            ano_arte:{
                                required: true
                            },
                            entidad_promotora: {
                                required: true
                            },
                            url_otro: {
                                url: true
                            }
                            
                        }*/
                    });
       });
    </script>


</body>
</html>

