<!--
*
*  INSPINIA - Arte o Software
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin | Libro o Capítulo</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

     <!--  style chosen lo lo puse -->
     <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

 

     <!-- jasny input mask-->
     <link href="<?php echo constant ('URL');?>src/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <!--  style select2 -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/select2/select2.min.css" rel="stylesheet">
    <!-- datapicker -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <!-- sweetalert input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">



</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-7">
                <h2>Agregar datos del Libro</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                        <a href="<?php echo constant('URL');?>/registro_libro">Datos Generales</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>/registro_libro">Producción Intelectual</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>/registro_libro">Listar</a>
                        </li>
                     
                        <li class="breadcrumb-item active">
                        <a href=""><strong>Agregar datos del Libro</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-5">
                <div class="title-action">
                <a href="<?php echo constant('URL');?>registro_libro"class="btn btn-primary"> <i class="fa fa-arrow-left"></i> Volver</a>
                            
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">                                                                                                                                                                                                                                                                                                                                     


                <div class="ibox ">                                                                                                                                                                 
                    <div class="ibox-title">
                        <h5>Editar Libros Registrados</h5>
                      
                    </div>
                    
                    <div class="ibox-content">
                    <div id="respuesta"><?php echo $this->mensaje; ?></div>
                    <div class="alert alert-info">Todos los campos marcados con un (<span style="color: red;">*</span>) Son Obligatorios</div>

                    <form id="form" method="post" action="<?php echo constant('URL');?>registro_libro/registrarLibro" class="wizard-big">
                                <h1>Libros o Capítulos</h1>
                                <fieldset>
                                    <h2> Información del Libro o Capítulo</h2>
                                    <div class="row">
                                        <div class="col-lg-4">

                                        <!--<div class="form-group">
                                                <label>Docente <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige el Docente..." name="id_docente" id="id_docente">
                                        
                                                    <option selected="selected" value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/docente.php';
                                                        foreach($this->docente as $row){
                                                        $docente=new Docente();
                                                            $docente=$row;?> 
                                                            <option value="<?php echo $docente->id_docente;?>"><?php echo $docente->identificacion; ?></option>
                                                            <?php }?>
                                                </select> 
                                                
                                            </div>-->

                                            <div class="form-group">
                                            <label>Libro o capítulo <span style="color: red;">*</span></label>
                                                <input id="nombre_libro" name="nombre_libro" type="text" placeholder="Ingrese el nombre del libro o capítulo" maxlength="80" class="form-control required">
                                              
                                            </div>

                                            <div class="form-group">
                                                <label>Editorial<span style="color: red;">*</span></label>
                                                <input id="editorial" name="editorial" type="text" placeholder="Ingrese la Editorial" maxlength="15" class="form-control required">
                                            </div>

                                            <div class="form-group">
                                                <label>ISBN del Libro<span style="color: red;">*</span></label>
                                                <input id="isbn_libro" name="isbn_libro" type="text" placeholder="Ingrese El ISBN del Libro" maxlength="15" class="form-control required">
                                            </div>
                                            
                                            <!--<div class="form-group">
                                                <label>Año de publicación del libro<span style="color: red;">*</span></label>
                                                <input id="ano_publicacion" name="ano_publicacion" type="num" placeholder="Ingrese el año de publicación del libro" maxlength="4" class="form-control required">
                                            </div>-->

                                            <div class="form-group" id="data_2">
                                                <label>Año de publicación del libro <span style="color: red;">*</span></label>
                                                <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <input type="text" name="ano_publicacion" id="ano_publicacion" class="form-control required number" placeholder="Seleccione el Año publicación del libro">
                                                </div>
                                                <label id="ano_publicacion-error" class="error" for="ano_publicacion" style=""></label>
                                            </div>
                                            <script>
                                            var mem = $('#data_2 .input-group.date').datepicker({
                                                format: 'yyyy',
                                                viewMode: "years",
                                                minViewMode: "years",
                                                autoclose: true
                                                
                                            });
                                            </script>

                                            </div>


                                            <div class="col-lg-4">

                                            <div class="form-group">
                                                <label>Ciudad de publicación<span style="color: red;">*</span></label>
                                                <input id="ciudad_publicacion" name="ciudad_publicacion" type="text" placeholder="Ingrese la ciudad de publicación" maxlength="15" class="form-control required">
                                            </div>

                                            <div class="form-group">
                                                <label>Volumen del Libro<span style="color: red;">*</span></label>
                                                <input id="volumenes" name="volumenes" type="num" placeholder="Ingrese el volumen del libro" maxlength="15" class="form-control required">
                                            </div>

                                            <div class="form-group">
                                                <label>Número de páginas del libro<span style="color: red;">*</span></label>
                                                <input id="nro_paginas" name="nro_paginas" type="num" placeholder="Ingrese el número de páginas " maxlength="8" class="form-control required">
                                            </div>

                                            <div class="form-group">
                                                <label>URL (Opcional)</label>
                                                <input id="url_libro" name="url_libro" type="text" placeholder="Ingrese la URL del Libro" maxlength="45" class="form-control url">
                                            </div>
                                   
                                            
                                            <input type="hidden" name="registrar">
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                            </fieldset>
                               
                            </form>

                    </div>
                </div>
            </div>
            </div>
        </div>







    <?php require 'views/footer.php'; ?>

     
   
    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    <!-- Chosen -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>

    <!-- Data picker -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>

<!-- 
    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");  
    $("#re").addClass("active");
});   
</script>-->



    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {true
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";true

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {true
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        
                        rules: {
                            ano_publicacion: {
                                esfecha: true
                            }
        
                        }
                    });
       });

       $.validator.addMethod("esfecha", esFechaActual, "El año de públicación debe ser menor que el actual");

function esFechaActual(value,element,param){

    var fechaActual = <?php echo date('Y');?>;
    console.log(fechaActual+value);
    if (value> fechaActual){
        return false; // error de validación

    }else {
        return true; // supera la validación
    }


}

    </script>


</body>
</html>
