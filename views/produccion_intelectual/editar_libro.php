<!--
*
*  INSPINIA - Arte o Software
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin | Libro o Capítulo</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

     <!--  style chosen lo lo puse -->
     <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">
 <!-- jasny input mask-->
 <link href="<?php echo constant ('URL');?>src/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <!--  style select2 -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/select2/select2.min.css" rel="stylesheet">
    <!-- datapicker -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <!-- sweetalert input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

 
 


</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-7">
                <h2>Editar Libro o Capítulo</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>home">Inicio</a>
                        </li>
                        
                        <li class="breadcrumb-item">
                        <a href="<?php echo constant('URL');?>/registro_libro">Datos Generales</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>/registro_libro">Producción Intelectual</a>
                        </li>
                        <li class="breadcrumb-item">
                        <a href="<?php echo constant('URL');?>/registro_libro">Listar </a>
                        </li>
                        <li class="breadcrumb-item active">
                        <a href=""> <strong>Editar Libro o Capítulo</strong></a>

                        </li>
                    </ol>
                </div>
                <div class="col-lg-5">
                <div class="title-action">  
                <a href="<?php echo constant('URL');?>registro_libro" class="btn btn-primary"> <i class="fa fa-arrow-left"></i> Volver</a>
                </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">                                                                                                                                                                                                                                                                                                                                     

                        <!--<div class="form-group">
                                      <label>Docente <span style="color: red;">*</span></label>
                                 <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige el Docente..." name="id_docente" id="id_docente">
                                        
                                 <option selected="selected" value="">Seleccione</option> 
                                   <?php include_once 'models/datosacademicos/docente.php';
                                           foreach($this->docente as $row){
                                           $docente=new Docente();
                                             $docente=$row;?> 
                                         <option value="<?php echo $docente->id_docente;?>"><?php echo $docente->identificacion . $docente->primer_nombre . $docente->primer_apellido ; ?></option>
                                          <?php }?>
                                       </select> 
                                                
                                            </div>-->

                <div class="ibox ">                                                                                                                                                                 
                    <div class="ibox-title">
                        <h5>Lista de Libros o Capítulos Registrados</h5>
                     
                    </div>
                    
                    <div class="ibox-content">
                    <div id="respuesta"><?php echo $this->mensaje; ?></div>
                    <div class="alert alert-info">Todos los campos marcados con un (<span style="color: red;">*</span>) Son Obligatorios</div>

                    <form id="form" method="post" action="<?php echo constant('URL');?>registro_libro/editarLibro" class="wizard-big">
                                <h1>Libros o Capítulos</h1>
                                <fieldset>
                                    <h2> Información de los Libros o Capítulos</h2>
                                    <div class="row">
                                        <div class="col-lg-4">




                                        <div class="form-group">
                                            <label>Libro o capítulo <span style="color: red;">*</span></label>
                                                <input id="nombre_libro2" name="nombre_libro2" type="text" placeholder="Ingrese el nombre del libro o capítulo" maxlength="80" class="form-control required" value="<?php echo $this->libro->nombre_libro; ?>" >
                                              
                                            </div>

                                            <div class="form-group">
                                            <label>Editorial<span style="color: red;">*</span></label>
                                                <input id="editorial2" name="editorial2" type="text" placeholder="Ingrese la editorial del libro" maxlength="80" class="form-control required" value="<?php echo $this->libro->editorial; ?>" >
                                              
                                            </div>

                                            <div class="form-group">
                                            <label>ISBN del libro<span style="color: red;">*</span></label>
                                                <input id="isbn_libro2" name="isbn_libro2" type="text" placeholder="Ingrese el ISBN del libro" maxlength="80" class="form-control required" value="<?php echo $this->libro->isbn_libro; ?>" >
                                              
                                            </div>

                                            <div class="form-group" id="data_2">
                                            <label>Año de publicación<span style="color: red;">*</span></label>
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input id="ano_publicacion2" name="ano_publicacion2" type="text" placeholder="Ingrese el año de publicación del libro" maxlength="80" class="form-control required number" value="<?php echo $this->libro->ano_publicacion; ?>" >
                                              
                                            </div>
                                            <script>
                                            var mem = $('#data_2 .input-group.date').datepicker({
                                                format: 'yyyy',
                                                viewMode: "years",
                                                minViewMode: "years",
                                                autoclose: true
                                                
                                            });
                                            </script>





                                            </div>
                                        </div>



                                            <div class="col-lg-4">

                                            <div class="form-group">
                                            <label>Ciudad de publicación<span style="color: red;">*</span></label>
                                                <input id="ciudad_publicacion2" name="ciudad_publicacion2" type="text" placeholder="Ingrese la ciudad de publicación del libro" maxlength="80" class="form-control required" value="<?php echo $this->libro->ciudad_publicacion; ?>" >
                                              
                                            </div>


                                            <div class="form-group">
                                                <label>Volumen del Libro<span style="color: red;">*</span></label>
                                                <input id="volumenes2" name="volumenes2" type="num" placeholder="Ingrese el volumen del libro" maxlength="10" class="form-control required" value="<?php echo $this->libro->volumenes; ?>">
                                            </div>

                                            <div class="form-group">
                                                <label>Número de páginas del libro</label>
                                                <input id="nro_paginas2" name="nro_paginas2" type="num" placeholder="Ingrese el número de páginas del libro" maxlength="5" class="form-control required" value="<?php echo $this->libro->nro_paginas; ?>">
                                            </div>

                                            <div class="form-group">
                                                <label>URL (Opcional)</label>
                                                <input id="url_libro2" name="url_libro2" type="text" placeholder="Ingrese la URL" maxlength="45" class="form-control url" value="<?php echo $this->libro->url_libro; ?>">
                                            </div>
                                   
                                            <input id="id_libro" name="id_libro" type="hidden"  value="<?php echo $this->libro->id_libro; ?>">
                                            <input type="hidden" name="registrar">
                                        </div>



                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                            </fieldset>
                               
                            </form>

                    </div>
                </div>
            </div>
            </div>
        </div>



   




    <?php require 'views/footer.php'; ?>

      
   
    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    <!-- Chosen -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>
 <!-- Data picker -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>

<!-- 
    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");  
    $("#re").addClass("active");
});   
</script>-->



    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {true
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";true

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {true
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        }/*,
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            },
                            ano_arte:{
                                required: true
                            },
                            entidad_promotora: {
                                required: true
                            },
                            url_otro: {
                                url: true
                            }
                            
                        }*/
                    });
       });
    </script>


</body>
</html>
