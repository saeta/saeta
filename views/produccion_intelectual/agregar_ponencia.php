<!--
*
*  INSPINIA - Ponencias
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin | Ponencias</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

     <!--  style chosen lo lo puse -->
     <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

 
<!-- jasny input mask-->
<link href="<?php echo constant ('URL');?>src/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <!--  style select2 -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/select2/select2.min.css" rel="stylesheet">
    <!-- datapicker -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <!-- sweetalert input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">





</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-7">
                <h2>Agregar datos de la Ponencia</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>home">Inicio</a>
                        </li>
                        
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>registro_ponencia">Datos Generales</a>
                        </li>
                    <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>registro_ponencia">Producción Intelectual</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>registro_ponencia">Listar</a>
                        </li>
                        <li class="breadcrumb-item active">
                        <a href=""> <strong>Agregar datos de la Ponencia</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-5">
                <div class="title-action">         
                      <a href="<?php echo constant('URL');?>registro_ponencia" class="btn btn-primary">  <i class="fa fa-arrow-left"></i> Volver</a>
                        </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">                                                                                                                                                                                                                                                                                                                                     


                <div class="ibox ">                                                                                                                                                                 
                    <div class="ibox-title">
                        <h5>Editar Ponencias Registradas</h5>
                        
                    </div>
                    
                    <div class="ibox-content">
                    <div id="respuesta"><?php echo $this->mensaje; ?></div>
<div class="alert alert-info">Todos los campos marcados con un (<span style="color: red;">*</span>) Son Obligatorios</div>
                    <form id="form" method="post" action="<?php echo constant('URL');?>registro_ponencia/registrarPonencia" class="wizard-big">
                                <h1>Ponencias</h1>
                                <fieldset>
                                    <h2> Información de la Ponencia</h2>
                                    <div class="row">
                                        <div class="col-lg-4">

                                        <!--<div class="form-group">
                                                <label>Docente <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige el Docente..." name="id_docente" id="id_docente">
                                        
                                                    <option selected="selected" value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/docente.php';
                                                        foreach($this->docente as $row){
                                                        $docente=new Docente();
                                                            $docente=$row;?> 
                                                            <option value="<?php echo $docente->id_docente;?>"><?php echo $docente->identificacion; ?></option>
                                                            <?php }?>
                                                </select> 
                                                
                                            </div>-->

                                            <div class="form-group">
                                            <label>Ponencia <span style="color: red;">*</span></label>
                                                <input id="nombre_ponencia" name="nombre_ponencia" type="text" placeholder="Ingrese el nombre de la ponencia" maxlength="80" class="form-control required">
                                              
                                            </div>

                                            <div class="form-group">
                                            <label>Evento <span style="color: red;">*</span></label>
                                                <input id="nombre_evento" name="nombre_evento" type="text" placeholder="Ingrese el nombre del evento" maxlength="100" class="form-control required">
                                              
                                            </div>

                                          <!--  <div class="form-group">
                                                <label>Año<span style="color: red;">*</span></label>
                                                <input id="ano_ponencia" name="ano_ponencia" type="num" placeholder="Ingrese el año de realización de la ponencia" maxlength="4" class="form-control required">
                                            </div>-->


                                            <div class="form-group" id="data_2">
                                                <label>Año de Realización <span style="color: red;">*</span></label>
                                                <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <input type="text" name="ano_ponencia" id="ano_ponencia" class="form-control required number" placeholder="Seleccione el Año de realización de la ponencia">
                                                </div>
                                                <label id="ano_ponencia-error" class="error" for="ano_ponencia" style=""></label>
                                            </div>
                                            <script>
                                            var mem = $('#data_2 .input-group.date').datepicker({
                                                format: 'yyyy',
                                                viewMode: "years",
                                                minViewMode: "years",
                                                autoclose: true
                                                
                                            });
                                            </script>

                                            <div class="form-group">
                                                <label>Ciudad<span style="color: red;">*</span></label>
                                                <input id="ciudad" name="ciudad" type="text" placeholder="Ingrese la ciudad" maxlength="15" class="form-control required">
                                            </div>

                                            <div class="form-group">
                                                <label>Título de la memoria del evento<span style="color: red;">*</span></label>
                                                <input id="titulo_memoria" name="titulo_memoria" type="text" placeholder="Ingrese el título de la memoria del evento" maxlength="100" class="form-control required">
                                            </div>
                                            
                                        
                                            </div>


                                            <div class="col-lg-4">

                                            <div class="form-group">
                                                <label>Volumen<span style="color: red;">*</span></label>
                                                <input id="volumen" name="volumen" type="text" placeholder="Ingrese el volumen de publicación" maxlength="15" class="form-control required">
                                            </div>

                                            <div class="form-group">
                                                <label>ISBN o ISSN<span style="color: red;">*</span></label>
                                                <input id="identificador" name="identificador" type="num" placeholder="Ingrese el identificador de la ponencia" maxlength="15" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                                <label>Página Inicial<span style="color: red;">*</span></label>
                                                <input id="pag_inicial" name="pag_inicial" type="num" placeholder="Ingrese el número de página inical " maxlength="8" class="form-control required">
                                            </div>

                                            <div class="form-group">
                                                <label>Formato de la Ponencia<span style="color: red;">*</span></label>
                                                <input id="formato" name="formato" type="text" placeholder="Ingrese el formato de la ponencia" maxlength="30" class="form-control required">
                                            </div>

                                            <div class="form-group">
                                                <label>URL (Opcional)</label>
                                                <input id="url_ponencia" name="url_ponencia" type="text" placeholder="https://ejemplo.com" maxlength="45" class="form-control url">
                                            </div>
                                   
                                            
                                            <input type="hidden" name="registrar">
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-line-chart" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                            </fieldset>
                               
                            </form>

                    </div>
                </div>
            </div>
            </div>
        </div>







    <?php require 'views/footer.php'; ?>

     
    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    <!-- Chosen -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>

  <!-- Data picker -->
  <script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>

<!-- 
    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");  
    $("#re").addClass("active");
});   
</script>-->



    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {true
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";true

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {true
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        
                        rules: {
                            ano_ponencia: {
                                esfecha: true
                            }
        
                        }
                    });
       });

       $.validator.addMethod("esfecha", esFechaActual, "El año de realización debe ser menor que el actual");

function esFechaActual(value,element,param){

    var fechaActual = <?php echo date('Y');?>;
    console.log(fechaActual+value);
    if (value> fechaActual){
        return false; // error de validación

    }else {
        return true; // supera la validación
    }


}

    </script>


</body>
</html>
