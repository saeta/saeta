<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin | Arte o Software</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

     <!--  style chosen lo lo puse -->
     <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">
 <!-- jasny input mask-->
 <link href="<?php echo constant ('URL');?>src/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <!--  style select2 -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/select2/select2.min.css" rel="stylesheet">
    <!-- datapicker -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <!-- sweetalert input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

 


</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                <h2>Editar Arte o Software</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                        <a href="<?php echo constant('URL');?>/arte_software">Datos Generales</a>
                        </li>
                        <li class="breadcrumb-item">
                        <a href="<?php echo constant('URL');?>arte_software">Producción Intelectual</a>
                        </li>
                        <li class="breadcrumb-item">
                        <a href="<?php echo constant('URL');?>arte_software">listar</a>
                        </li>
                        <li class="breadcrumb-item active">
                        <a href=""><strong>Editar Arte o Software</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                <div class="title-action">
                <a href="<?php echo constant('URL');?>arte_software" class="btn btn-primary"> <i class="fa fa-arrow-left"></i> Volver</a> 
                </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">                                                                                                                                                                                                                                                                                                                                     

                        <!--<div class="form-group">
                                      <label>Docente <span style="color: red;">*</span></label>
                                 <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige el Docente..." name="id_docente" id="id_docente">
                                        
                                 <option selected="selected" value="">Seleccione</option> 
                                   <?php include_once 'models/datosacademicos/docente.php';
                                           foreach($this->docente as $row){
                                           $docente=new Docente();
                                             $docente=$row;?> 
                                         <option value="<?php echo $docente->id_docente;?>"><?php echo $docente->identificacion . $docente->primer_nombre . $docente->primer_apellido ; ?></option>
                                          <?php }?>
                                       </select> 
                                                
                                            </div>-->

                <div class="ibox ">                                                                                                                                                                 
                    <div class="ibox-title">
                        <h5>Lista de Artes o Software Registrados</h5>
                       
                    </div>
                    
                    <div class="ibox-content">
                    <div id="respuesta"><?php echo $this->mensaje; ?></div>
                    <div class="alert alert-info">Todos los campos marcados con un (<span style="color: red;">*</span>) Son Obligatorios</div>

                    <form id="form" method="post" action="<?php echo constant('URL');?>arte_software/editarArte" class="wizard-big">
                                <h1>Obra Artística o Software</h1>
                                <fieldset>
                                    <h2> Información de la obra artística o Software</h2>
                                    <div class="row">
                                        <div class="col-lg-8">


                                        <div class="form-group">
                                            <label>Arte o Software <span style="color: red;">*</span></label>
                                                <input id="nombre_arte2" name="nombre_arte2" type="text" class="form-control required" value="<?php echo $this->arte->nombre_arte; ?>" placeholder="Ingrese el nombre de la obra artística o Software">
                                              
                                            </div>

                                            <div class="form-group" id="data_2">
                                                <label>año de realizacion<span style="color: red;">*</span></label>
                                                <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <input id="ano_arte2" name="ano_arte2" type="text" placeholder="Ingrese El Año de realización" maxlength="4" class="form-control required number" value="<?php echo $this->arte->ano_arte; ?>">
                                            </div>
                                            <script>
                                            var mem = $('#data_2 .input-group.date').datepicker({
                                                format: 'yyyy',
                                                viewMode: "years",
                                                minViewMode: "years",
                                                autoclose: true
                                                
                                            });
                                            </script>





                                            <div class="form-group">
                                                <label>Entidad Promotora<span style="color: red;">*</span></label>
                                                <input id="entidad_promotora2" name="entidad_promotora2" type="text" placeholder="Ingrese la Entidad Promotora" maxlength="70" class="form-control required" value="<?php echo $this->arte->entidad_promotora; ?>">
                                            </div>

                                            <div class="form-group">
                                                <label>URL (Opcional)</label>
                                                <input id="url_otro2" name="url_otro2" type="text" placeholder="Ingrese la URL" maxlength="50" class="form-control url" value="<?php echo $this->arte->url_otro; ?>">
                                            </div>
                                   
                                            <input id="id_arte_software" name="id_arte_software" type="hidden"  value="<?php echo $this->arte->id_arte_software; ?>">
                                            <input type="hidden" name="registrar">
                                        </div>
                                        
                                    </div>

                                            </fieldset>
                               
                            </form>

                    </div>
                </div>
            </div>
            </div>
        </div>



   




    <?php require 'views/footer.php'; ?>

   
    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    <!-- Chosen -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>
 <!-- Data picker -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>

<!--

    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");  
    $("#re").addClass("active");
});   
</script>-->



    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {true
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";true

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {true
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        }/*,
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            },
                            ano_arte:{
                                required: true
                            },
                            entidad_promotora: {
                                required: true
                            },
                            url_otro: {
                                url: true
                            }
                            
                        }*/
                    });
       });
    </script>


</body>
</html>
