<!--
*
*  INSPINIA - Reconocimientos
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>SIDTA | Reconocimientos</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

     <!--  style chosen lo lo puse -->
     <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">



     <!-- jasny input mask-->
     <link href="<?php echo constant ('URL');?>src/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <!--  style select2 -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/select2/select2.min.css" rel="stylesheet">
    <!-- datapicker -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <!-- sweetalert input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
<!-- jasny input mask-->
<link href="<?php echo constant ('URL');?>src/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
 <!-- sweetalert input mask-->
 <link href="<?php echo constant ('URL');?>src/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">


</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-7">
                <h2>Agregar Reconocimientos</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>home">Inicio</a>
                        </li>
                       
                        <li class="breadcrumb-item">
                        <a href="<?php echo constant('URL');?>registro_reconocimiento">Datos Generales</a>
                        </li>
                        <li class="breadcrumb-item">
                        <a href="<?php echo constant('URL');?>registro_reconocimiento">Reconocimientos</a>
                        </li>
                        <li class="breadcrumb-item">
                        <a href="<?php echo constant('URL');?>registro_reconocimiento">Listar</a>
                        </li>
                        <li class="breadcrumb-item active">
                        <a href=""><strong>Agregar Reconocimientos</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-5">
                <div class="title-action"> 
                <a href="<?php echo constant('URL');?>registro_reconocimiento"  class="btn btn-primary">    <i class="fa fa-arrow-left"></i> Volver</a>
                         
                
                    </div>
                        </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">                                                                                                                                                                                                                                                                                                                                     


                <div class="ibox ">                                                                                                                                                                 
                    <div class="ibox-title">
                        <h5>Lista de Reconocimientos Registrados</h5>
                      
                    </div>
                    
                    <div class="ibox-content">
                    <div id="respuesta"><?php echo $this->mensaje; ?></div>

                    <form id="form" method="post" action="<?php echo constant('URL');?>registro_reconocimiento/registrarReconocimiento" enctype="multipart/form-data" class="wizard-big">
                                <h1>Reconocimientos</h1>
                                <fieldset>
                                    <h2> Información de los Reconocimientos</h2>
                                    <div class="row">
                                        <div class="col-lg-8">

                                    
                                            <div class="form-group">
                                                <label>Tipo de Reconocimiento <span style="color: red;">*</span></label>
                                                <select name="tipo_reconocimiento" id="tipo_reconocimiento" style="width: 100%;"  class="form-control required select2_demo_3" tabindex="4">
                                                    <option value="">Seleccione el tipo de Reconocimiento</option>
                                                   
                                                    <?php 
                                                    
                                                            foreach($this->tipos_reconocimiento as $row){
                                                            $tipo_reconocimiento=new Estructura();
                                                            $tipo_reconocimiento=$row;?> 
                                                    <option value="<?php echo $tipo_reconocimiento->id;?>"><?php echo $tipo_reconocimiento->descripcion;?></option>
                                                    <?php }?>                                         
                                                </select>
                                            </div>
                                            
                                            <div class="form-group">
                                            <label>Reconocimiento <span style="color: red;">*</span></label>
                                                <input id="descripcion" name="descripcion" type="text" class="form-control required" placeholder="Ingrese el nombre del reconocimiento">
                                              
                                            </div>

                                         

                                            <div class="form-group">
                                                <label>Entidad Promotora<span style="color: red;">*</span></label>
                                                <input id="entidad_promotora" name="entidad_promotora" type="text" placeholder="Ingrese la Entidad que Promovió el Evento" maxlength="70" class="form-control required">
                                            </div>

                                            <div class="form-group" id="data_2">
                                                <label>Año de Realización <span style="color: red;">*</span></label>
                                                <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <input type="text" name="ano_reconocimiento" id="ano_reconocimiento" class="form-control required number" placeholder="Seleccione el Año de realización">
                                                </div>
                                                <label id="ano_reconocimiento" class="error" for="ano_reconocimiento" style=""></label>
                                            </div>
                                            <script>
                                            var mem = $('#data_2 .input-group.date').datepicker({
                                                format: 'yyyy',
                                                viewMode: "years",
                                                minViewMode: "years",
                                                autoclose: true
                                                
                                            });
                                            </script>


                                                <div class="form-group">
                                                <label>Carácter<span style="color: red;">*</span></label>
                                                <input id="caracter" name="caracter" type="text" placeholder="Ingrese el carácter del reconocimiento" maxlength="70" class="form-control required">
                                                </div>

                                                <div class="col-lg-3">
                                                <div class="form-group">
                                                    <label>Cargar Acta de Reconocimiento <i>(Formato PDF)</i><span style="color: red;">*</span></label>

                                                    <div class="form-group">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <span class="btn btn-default btn-file"><span class="fileinput-new">Seleccionar Archivo</span>
                                                            <span class="fileinput-exists">Cambiar</span><input type="file" id="file_reconocimiento" accept=".pdf"  name="pdf_certificado"/></span>
                                                            <span class="fileinput-filename"></span>
                                                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                                            <label id="file_reconocimiento-error" class="error" for="file_reconocimiento"></label>
                                                        
                                                        
                                                            <script>

                                                // Cuando cambie #fichero
                                                $("#file_reconocimiento").change(function () {
                                                    $('.error').text('');
                                                    if(validarExtension(this)) { 
                                                        if(validarPeso(this)) { 
                                                        }
                                                    } 
                                                });


                                                </script>
                                                        
                                              
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>



                                   

                                            <input type="hidden" name="registrar">
                                        </div>
        
                                    </div>

                                            </fieldset>
                               
                            </form>

                    </div>
                </div>
            </div>
            </div>
        </div>



   




    <?php require 'views/footer.php'; ?>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    <!-- Chosen -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>
    <!-- Data picker -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<!-- Input Mask-->
<script src="<?php echo constant ('URL');?>src/js/plugins/jasny/jasny-bootstrap.min.js"></script>
 <!-- Sweet alert -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/sweetalert/sweetalert.min.js"></script>
   

<!-- 
    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");  
    $("#re").addClass("active");
});   
</script>-->



    <script>

var extensionesValidas = ".pdf";

var pesoPermitido = 2000;//2MB = 2000 kb
//console.log(pesoPermitido, extensionesValidas);
// Validacion de extensiones permitidas
function validarExtension(datos) {
    
    var ruta = datos.value;
    var extension = ruta.substring(ruta.lastIndexOf('.') + 1).toLowerCase();
    
    var extensionValida = extensionesValidas.indexOf(extension);
    console.log('valor.',extensionValida < 0);
    //console.log(ruta, extension, extensionValida);
    if(extensionValida < 0) {
            $('.error').text('La extensión no es válida Su Archivo tiene de extensión: .'+ extension);
            //alert('La extensión no es válida Su Archivo tiene de extensión: .'+ extension);
        swal("Ha ocurrido un Error", "La extensión no es válida Su Archivo tiene de extensión: ."+extension, "error");  
        document.getElementById("file_reconocimiento").value = "";
      
        
        return false;
    } else {
        return true;
    }
}


// Validacion de peso del fichero en kbs
function validarPeso(datos) {
if (datos.files && datos.files[0]) {
    var pesoFichero = datos.files[0].size/2000;
    if(pesoFichero > pesoPermitido) {
        $('.error').text('El peso máximo permitido del Archivo es: ' + pesoPermitido + ' KBs Su fichero tiene: '+ pesoFichero +' KBs');
        //alert('El peso maximo permitido del Archivo es: ' + pesoPermitido + ' KBs Su Archivo tiene: '+ pesoFichero +' KBs');
        
        swal("Ha ocurrido un Error","El peso máximo permitido del Archivo es: " + pesoPermitido + " KBs Su Archivo tiene: "+ pesoFichero +" KBs", "error");    
        document.getElementById("file_reconocimiento").value = "";
       
        
        return false;
    } else {
        return true;
    }
}
}


        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {true
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";true

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {true
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        
                        rules: {
                            ano_reconocimiento: {
                                esfecha: true
                            }
        
                        }
                    });
       });

$.validator.addMethod("esfecha", esFechaActual, "El año de realización debe ser menor que el actual");

function esFechaActual(value,element,param){

    var fechaActual = <?php echo date('Y');?>;
    console.log(fechaActual+value);
    if (value> fechaActual){
        return false; // error de validación

    }else {
        return true; // supera la validación
    }


}




    </script>


</body>
</html>
