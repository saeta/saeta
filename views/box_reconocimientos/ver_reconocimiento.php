

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin | Reconocimientos</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

     <!--  style chosen lo lo puse -->
     <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

 


</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-7">
                <h2>Ver Reconocimientos</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                        <a href="<?php echo constant('URL');?>registro_reconocimiento">Datos Generales</a>
                        </li>
                        <li class="breadcrumb-item">
                        <a href="<?php echo constant('URL');?>registro_reconocimiento">Producción Intelectual</a>
                        </li>
                        <li class="breadcrumb-item">
                        <a href="<?php echo constant('URL');?>registro_reconocimiento">Listar</a>
                        </li>
                      
                        <li class="breadcrumb-item active">
                        <a href=""> <strong>Ver Reconocimientos</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-5">
                <div class="title-action">
                <a href="<?php echo constant('URL');?>registro_reconocimiento" class="btn btn-primary"> <i class="fa fa-arrow-left"></i> Volver</a>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">                                                                                                                                                                                                                                                                                                                                     

                        <!--<div class="form-group">
                                      <label>Docente <span style="color: red;">*</span></label>
                                 <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige el Docente..." name="id_docente" id="id_docente">
                                        
                                 <option selected="selected" value="">Seleccione</option> 
                                   <?php include_once 'models/datosacademicos/docente.php';
                                           foreach($this->docente as $row){
                                           $docente=new Docente();
                                             $docente=$row;?> 
                                         <option value="<?php echo $docente->id_docente;?>"><?php echo $docente->identificacion . $docente->primer_nombre . $docente->primer_apellido ; ?></option>
                                          <?php }?>
                                       </select> 
                                                
                                            </div>-->

                <div class="ibox ">                                                                                                                                                                 
                    <div class="ibox-title">
                        <h5>Lista de Reconocimientos Registrados</h5>
                       
                    </div>
                    
                    <div class="ibox-content">
                    <div class="row">
                            <div class="col-lg-12">
                            <div class="tabs-container">
                            <div class="tabs-right">
                                        <ul class="nav nav-tabs">
                                            <li><a class="nav-link active" data-toggle="tab" href="#tab-8"> Datos del Reconocimiento</a></li>
                                            </ul>
                                            <div class="tab-content">
                                            <div id="tab-8" class="tab-pane active">
                                            <div class="panel-body">

                  
                                    <div class="row">
                                        <div class="col-lg-6">
                                        <h2> <strong>RECONOCIMIENTO:</strong></h2>
                                        <p><?php echo $this->recono->descripcion; ?></p>
                                           </div>
                                           <div class="col-lg-6">
                                        <h2> <strong>TIPO DE RECONOCIMIENTO:</strong></h2>
                                        <p><?php echo $this->recono->descripcion_tipo; ?></p>
                                    </div>      
                                    </div>
                                           <hr>
                                    <div class="row">
                                        <div class="col-lg-6">
                                        <h2> <strong>ENTIDAD PROMOTORA:</strong></h2>
                                        <p><?php echo $this->recono->entidad_promotora; ?></p>
                                        </div>      
                                    
                                        <div class="col-lg-6">
                                        <h2> <strong>AÑO DE REALIZACIÓN:</strong></h2>
                                        <p><?php echo $this->recono->ano_reconocimiento; ?></p>
                                        </div>
                                           </div>
                                           <hr>
                                           <div class="row">
                                           <div class="col-lg-6">
                                           <h2> <strong>CARACTER:</strong></h2>
                                        <p><?php echo $this->recono->caracter; ?></p>
                                        </div>
                                        <div class="col-lg-6">
                                        <h2> <strong>ESTADO DE LA VERIFICACIÓN:</strong></h2>
                                        <p><?php if($this->recono->estatus_verificacion=='Sin Verificar'){ echo '<span class="label label-danger" style="font-size: 12px;">'.ucwords($this->recono->estatus_verificacion).'</span>'; } else{ echo '<span class="label label-success" style="font-size: 12px;">'.ucwords($this->recono->estatus_verificacion).'</span>';} ?></p>
                                        </div>
                                           </div>
                                           <hr>
                                        <div class="col-lg-6">
                                            <h2><strong>ACTA DE RECONOCIMIENTO</strong></h2>
                                                <a target="_blank" href="<?php echo constant('URL') . "registro_reconocimiento/viewDocumento/" . $this->recono->id_reconocimiento;?>" class="btn btn-primary"><i class="fa fa-external-link"></i> Constancia</a>                                                         
                                                <a target="_blank" href="<?php echo constant('URL').'src/documentos/reconocimientos/'. $this->recono->descripcion_documento;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>
                                        </div>    


                                        </div>
                                      <!--  <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-eject" style="font-size: 100px;color: #e5e5e5 "></i>
                                                    
                                                </div>

                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-linux" style="font-size: 100px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                      </div>  -->
                                    </div>

                                          
                               
                          

                    </div>
                </div>
            </div>
            </div>
        </div>

        </div>
                </div>
            </div>
            </div>
        </div>
                                        

   




    <?php require 'views/footer.php'; ?>

   
    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    <!-- Chosen -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>

<!-- 
    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");  
    $("#re").addClass("active");
});   
</script>-->



    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {true
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";true

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {true
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        }/*,
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            },
                            ano_arte:{
                                required: true
                            },
                            entidad_promotora: {
                                required: true
                            },
                            url_otro: {
                                url: true
                            }
                            
                        }*/
                    });
       });
    </script>


</body>
</html>
