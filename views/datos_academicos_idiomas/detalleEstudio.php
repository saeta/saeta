<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SIDTA | Detalle</title>


 
    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">


</head>

<body>
 
   <?php require 'views/header.php'; ?>
   

   <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-7">
                    <h2>Detalle Estudio </h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>datos_academicos">Datos Académicos e Idiomas</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>datos_academicos">Listar Datos Académicos e Idiomas</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <a href=""><strong>Detalle</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-5">
                    <div class="title-action">
                        <a href="<?php echo constant ('URL');?>datos_academicos" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Volver</a>

                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight ">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>SIDTA</h5>
                        <div class="ibox-tools">

                 

                        </div>
                    </div>
                    <div class="ibox-content ">

                        <!--  contenido -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="tabs-container">
                                    <div class="tabs-right">
                                        <ul class="nav nav-tabs">
                                            <li><a class="nav-link active" data-toggle="tab" href="#tab-8"> Datos del Estudio</a></li>
                                            <li><a class="nav-link" data-toggle="tab" href="#tab-9">Institución</a></li>
                                            <li><a class="nav-link" data-toggle="tab" href="#tab-10">Certificación</a></li>

                                        </ul>
                                        <div class="tab-content">
                                            <div id="tab-8" class="tab-pane active">
                                                <div class="panel-body ">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <h2><strong>Nombre del Estudio</strong></h2>
                                                            <p><?php echo $this->estudio->estudio; ?></p>
                                                            <h2><strong>Modalidad de Estudio</strong></h2>
                                                            <p><?php echo $this->estudio->modalidad_estudio; ?></p>

                                                            <h2><strong>Año de Realización</strong></h2>
                                                            <p><?php echo $this->estudio->ano_estudio; ?></p>
                                                            <h2><strong>Tipo de Estudio</strong></h2>
                                                            <p><?php echo $this->estudio->tipo_estudio; ?></p>
                                                        </div>
                                                        <?php if($this->estudio->id_tipo_estudio==1){?>
                                                            <div class="col-lg-6">
                                                                <h2><strong>Nivel Académico</strong></h2>
                                                                <p><?php echo $this->estudio->nivel_academico; ?></p>
                                                                <h2><strong>Tipo de Estudio de Alto Nivel</strong></h2>
                                                                <p><?php echo $this->estudio->programa_nivel; ?></p>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="tab-9" class="tab-pane">
                                                <div class="panel-body">
                                                    <h2><strong>Nombre de la Institución</strong></h2>

                                                    <p><?php echo $this->estudio->institucion; ?> </p>
                                                    <h2><strong>País de Realización</strong></h2>

                                                    <p><?php echo $this->estudio->pais; ?></p>
                                                    <h2><strong>Tipo de Institución</strong></h2>

                                                    <p><?php echo $this->estudio->institucion_tipo; ?></p>

                                                </div>
                                            </div>
                                            <div id="tab-10" class="tab-pane">
                                                <div class="panel-body">
                                                    
                                                        <?php if($this->estudio->id_tipo_estudio!=3){ 
                                                                
                                                                if($this->estudio->id_tipo_estudio==1){?>
                                                                
                                                                <h2><strong>Título por Obtener</strong></h2>

                                                                
                                                                <?php } else{?>

                                                                    <h2><strong>Certificado por Obtener</strong></h2>

                                                                <?php }?>
                                                                <p>
                                                                
                                                                <?php echo $this->estudio->titulo_obtenido;?>

                                                                </p>
                                                                <h2><strong>Estado del Estudio </strong></h2>

                                                                <p><?php echo ucwords($this->estudio->estatus);?></p>
                                                                <?php if($this->estudio->estatus=='concluido' && $this->estudio->id_tipo_estudio==1){?>

                                                                    <h2><strong>Título del Trabajo Especial de Grado <i>(TEG)</i></strong></h2>
                                                                    <p><?php echo $this->estudio->titulo;?></p>
                                                                    <h2><strong>Resumen del <i>(TEG)</i></strong></h2>
                                                                    <p><?php echo $this->estudio->resumen;?></p>

                                                                <?php }?>

                                                                <?php if($this->estudio->estatus=='concluido'){?>
                                                                    <a target="_blank" href="<?php echo constant('URL') . "datos_academicos/viewDocumento/" . $this->documento;?>" class="btn btn-primary"><i class="fa fa-external-link"></i> Certificado</a>                                                         
                                                                    <a target="_blank" href="<?php echo constant('URL').'src/documentos/datos_academicos_idiomas/';?>"  download="<?php echo $this->descripcion_documento;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>

                                                                <?php }?>

                                                        <?php }else{ ?>
                                                        
                                                            <h2><strong>Nombre del Idioma Estudiado</strong></h2>

                                                        <p>
                                                        
                                                        <?php echo $this->estudio->idioma;?>

                                                        </p>
                                                            <div class="row">
                                                                <div class="col-lg-6"> <h2><strong>Nivel de lectura </strong></h2>

                                                            <p><?php echo ucwords($this->estudio->nivel_lectura);?></p>
                                                            <h2><strong>Nivel de Escritura </strong></h2>

                                                            <p><?php echo ucwords($this->estudio->nivel_escritura);?></p>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <h2><strong>Nivel de Habla </strong></h2>

                                                                    <p><?php echo ucwords($this->estudio->nivel_habla);?></p>

                                                                    <h2><strong>Nivel de Comprensión </strong></h2>

                                                                    <p><?php echo ucwords($this->estudio->nivel_comprende);?></p>
                                                                </div>
                                                            </div>
                                                            <h2><strong>Estado del Estudio </strong></h2>

                                                                    <p><?php echo ucwords($this->estudio->estatus);?></p>
                                                                <?php if($this->estudio->estatus='concluido'){?>
                                                                    <a href="<?php echo constant('URL') . "datos_academicos/viewDocumento/" . $this->documento;?>" class="btn btn-primary"><i class="fa fa-external-link"></i> Certificado</a>                                                         
                                                                    <a href="<?php echo constant('URL').'src/documentos/datos_academicos_idiomas/';?>"  download="<?php echo $this->descripcion_documento;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>

                                                            <?php }?>
                                                    <?php  } ?>
                                                   
                                                    
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!--  end contenido -->

                    </div>
                </div>
            </div>
            </div>
        </div>



    <?php require 'views/footer.php'; ?>

     

    <!-- dataTables Scripts -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>

   

</body>
</html>
