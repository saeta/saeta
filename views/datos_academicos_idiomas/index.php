<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SIDTA | Actualizar Datos Académicos e Idiomas</title>


 
    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

    <!-- FooTable -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/footable/footable.core.css" rel="stylesheet">
    <!--  style select2 -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/select2/select2.min.css" rel="stylesheet">

</head>

<body>
 
   <?php require 'views/header.php'; ?>
   

   <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-6">
                    <h2>Datos Académicos e Idiomas</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Datos Académicos e Idiomas</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Listar Datos Académicos e Idiomas</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-6">
                    <div class="title-action">
                        <a href="<?php echo constant ('URL');?>datos_academicos/viewAdd" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar Estudio</a>

                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>SIDTA</h5>
                        <div class="ibox-tools">

                 

                        </div>
                    </div>
                    <div class="ibox-content">
                    
                        <!--  contenido -->
                        <div class="row" style="margin-bottom: 0.5em;">
                        
                            <div class="col-lg-6">
                               
                                <label for=""><i class="fa fa-filter"></i> Filtrar Por: </label>

                                <div class="form-group">
                        
                                    <select name="" id="filtro" class="select2_demo_1" onChange="mostrar2(this.value);" >
                                        <option value="">Seleccione la categoría que desea listar</option>
                                        <option value="conducente1">Estudios Conducentes </option>
                                        <option value="no-conducente1">Estudios No Conducentes</option>
                                        <option value="idiomas1">Idiomas</option>
                                    </select>
                                </div>

                            </div>
                            
                        </div>


                        <!--  contenido -->
                        <?php echo $this->mensaje; ?>
                        <input type="text" class="form-control form-control-sm m-b-xs" id="filter"
                                   placeholder="&#128269; Buscar por cualquier Campo">

                            <!-- tabla estudios -->
                            <table id="tconducente" class="table table-stripped table-hover" data-page-size="8" data-filter=#filter>
                                <thead>
                                    <tr>
                                        <th>Estudio</th>
                                        <th>Modalidad de Estudio</th>
                                        <th>Nivel Académico</th>
                                        <th>Tipo de Alto Nivel</th>

                                        <th>Estatus</th>
                                        <th>Titulo Obtenido</th>
                                        <th>Estado de Verificacion</th>
                                        <th>Acciones</th>
                                        
                                    </tr>
                        </thead>
                        <tbody>
                        <?php 
                                        foreach($this->estudios_conducentes as $row){
                                            $estudio_c= new Estructura();
                                            $estudio_c=$row;?>
                                    <tr class="gradeX">
                                        <td><?php echo $estudio_c->estudio; ?></td>
                                        <td><?php echo $estudio_c->modalidad_estudio; ?></td>
                                        <td><?php echo $estudio_c->nivel_academico; ?></td>
                                        <td><?php echo $estudio_c->programa_nivel; ?></td>
                                        <td><?php if($estudio_c->estatus=='concluido'){ echo '<span class="label label-primary" style="font-size: 12px;">'.ucwords($estudio_c->estatus).'</span>'; } else{ echo '<span class="label label-warning" style="font-size: 12px;">'.ucwords($estudio_c->estatus).'</span>';} ?></td>

                                        
                                        <td><?php echo $estudio_c->titulo_obtenido; ?></td>

                                        <td id="<?php echo $estudio_c->id_estudio_conducente;?>">
                                        <?php if($estudio_c->estatus_verificacion=='Sin Verificar'){ echo '<span class="label label-danger" style="font-size: 12px;"><i class="fa fa-times"></i> '.ucwords($estudio_c->estatus_verificacion).'</span>'; } else{ echo '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> '.ucwords($estudio_c->estatus_verificacion).'</span>';} ?>                                        
                                        </td>
                                        
                                        <td> 
                                        <a class="btn btn-outline btn-success" href="<?php echo constant ('URL') . "datos_academicos/viewEdit/" . $estudio_c->id_estudio_conducente.','.$estudio_c->id_tipo_estudio;?>" role="button"><i class="fa fa-edit"></i> Editar</a>&nbsp;
                                    <!--como incluyo mas a class por las variables de estatus y la foranea-->
                                        <a class="btn btn-outline btn-info" href="<?php echo constant ('URL') . "datos_academicos/viewDetail/" . $estudio_c->id_estudio_conducente.','.$estudio_c->id_tipo_estudio;?>" role="button"><i class="fa fa-eye"></i> Ver</a>
                                        <?php if($estudio_c->estatus=='concluido'){$estatus=1;} elseif($estudio_c->estatus=='en curso'){ $estatus=0;}?>

                                        <a class="btn btn-outline btn-danger" href="<?php echo constant('URL') . 'datos_academicos/delete/' . $estudio_c->id_estudio_conducente.','.$estudio_c->id_tipo_estudio.','.$estudio_c->id_estudio.','.$estatus;?>" role="button"><i class="fa fa-trash"></i> Remover</a> &nbsp; 
                                        <!-- <button class="btn btn-outline btn-danger bEliminar" data-id="<?php echo $estudio->id_estudio;?>"><i class="fa fa-trash"></i> Eliminar</button>-->
                                    </td>
                                    </tr>
                                <?php 
                                    
                            }?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="8">
                                        <ul class="pagination float-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                            <!-- fin tabla estudios -->

                            <!-- tabla no conducente -->
                            <table id="tnoconducente" class="table table-stripped table-hover" data-page-size="8" data-filter=#filter>
                                <thead>
                                    <tr>
                                        <th>Estudio</th>
                                        <th>Tipo de Estudio</th>
                                        <th>Estatus</th>
                                        <th>Titulo Obtenido</th>
                                        <th>Estado de Verificacion</th>
                                        <th>Acciones</th>
                                    </tr>
                        </thead>
                        <tbody>
                        <?php 
                                // lo comente  $i=0;
                                        foreach($this->estudios as $row){
                                            $estudio= new Estructura();
                                            $estudio=$row;?>
                                <tr id ="fila-<?php echo $estudio->id_estudio; ?>" class="gradeX">
                                    <td><?php echo $estudio->estudio; ?></td>
                                    <td><?php echo $estudio->descripcion_tipo; ?></td>

                                    <td><?php if($estudio->estatus=='concluido'){ echo '<span class="label label-primary" style="font-size: 12px;">'.ucwords($estudio->estatus).'</span>'; } else{ echo '<span class="label label-warning" style="font-size: 12px;">'.ucwords($estudio->estatus).'</span>';} ?></td>

                                    <td><?php echo $estudio->titulo_obtenido; ?></td>

                                    <td><?php if($estudio->estatus_verificacion=='Sin Verificar'){ echo '<span class="label label-danger" style="font-size: 12px;">'.ucwords($estudio->estatus_verificacion).'</span>'; } else{ echo '<span class="label label-success" style="font-size: 12px;">'.ucwords($estudio->estatus_verificacion).'</span>';} ?></td>

                                    <td> 
                                        <a class="btn btn-outline btn-success" href="<?php echo constant ('URL') . "datos_academicos/viewEdit/" . $estudio->id_estudio_conducente.','.$estudio->id_tipo_estudio;?>" role="button"><i class="fa fa-edit"></i> Editar</a>&nbsp;
                                    <!--como incluyo mas a class por las variables de estatus y la foranea-->
                                        <a class="btn btn-outline btn-info" href="<?php echo constant ('URL') . "datos_academicos/viewDetail/" . $estudio->id_estudio_conducente.','.$estudio->id_tipo_estudio;?>" role="button"><i class="fa fa-eye"></i> Ver</a>
                                        <?php if($estudio->estatus=='concluido'){$estatus=1;} elseif($estudio->estatus=='en curso'){ $estatus=0;}?>

                                        <a class="btn btn-outline btn-danger" href="<?php echo constant('URL') . 'datos_academicos/delete/' . $estudio->id_estudio_conducente.','.$estudio->id_tipo_estudio.','.$estudio->id_estudio.','.$estatus;?>" role="button"><i class="fa fa-trash"></i> Remover</a> &nbsp; 
                                        <!-- <button class="btn btn-outline btn-danger bEliminar" data-id="<?php echo $estudio->id_estudio;?>"><i class="fa fa-trash"></i> Eliminar</button>-->
                                    </td>
                                </tr>
                                    <?php }?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <ul class="pagination float-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                            <!-- fin tabla no conducente -->
                            <!-- tabla idioma -->
                            <table id="tidioma" class="table table-stripped table-hover" data-page-size="8" data-filter=#filter>
                                <thead>
                                    <tr>
                                        <th>Estudio</th>
                                        <th>Tipo de Estudio</th>

                                        <th>Estatus</th>

                                        <th>Idioma</th>
                                        <th>Estado de Verificacion</th>

                                        <th>Acciones</th>
                                    </tr>
                        </thead>
                        <tbody>
                        <?php 
                                        foreach($this->estudios_idioma as $row){
                                            $estudio_idioma= new Estructura();
                                            $estudio_idioma=$row;?>
                                <tr id ="fila-<?php echo $estudio_idioma->id_estudio; ?>" class="gradeX">
                                    <td><?php echo $estudio_idioma->estudio; ?></td>
                                    <td><?php echo $estudio_idioma->descripcion_tipo; ?></td>

                                    <td><?php if($estudio_idioma->estatus=='concluido'){ echo '<span class="label label-primary" style="font-size: 12px;">'.ucwords($estudio_idioma->estatus).'</span>'; } else{ echo '<span class="label label-warning" style="font-size: 12px;">'.ucwords($estudio_idioma->estatus).'</span>';} ?></td>

                                    <td><?php echo $estudio_idioma->idioma; ?></td>

                                    <td><?php if($estudio_idioma->estatus_verificacion=='Sin Verificar'){ echo '<span class="label label-danger" style="font-size: 12px;">'.ucwords($estudio_idioma->estatus_verificacion).'</span>'; } else{ echo '<span class="label label-success" style="font-size: 12px;">'.ucwords($estudio_idioma->estatus_verificacion).'</span>';} ?></td>

                                    <td> 
                                        <a class="btn btn-outline btn-success" href="<?php echo constant ('URL') . "datos_academicos/viewEdit/" . $estudio_idioma->id_estudio_idioma.','.$estudio_idioma->id_tipo_estudio;?>" role="button"><i class="fa fa-edit"></i> Editar</a>&nbsp;
                                    <!--como incluyo mas a class por las variables de estatus y la foranea-->
                                    <a class="btn btn-outline btn-info" href="<?php echo constant ('URL') . "datos_academicos/viewDetail/" . $estudio_idioma->id_estudio_idioma.','.$estudio_idioma->id_tipo_estudio;?>" role="button"><i class="fa fa-eye"></i> Ver</a>
                                    <?php if($estudio_idioma->estatus=='concluido'){$estatus=1;} elseif($estudio_idioma->estatus=='en curso'){ $estatus=0;}?>
                                     <a class="btn btn-outline btn-danger" href="<?php echo constant('URL') . 'datos_academicos/delete/' . $estudio_idioma->id_estudio_idioma.','.$estudio_idioma->id_tipo_estudio.','.$estudio_idioma->id_estudio.','.$estatus;?>" role="button"><i class="fa fa-trash"></i> Remover</a> &nbsp; 
                                        <!-- <button class="btn btn-outline btn-danger bEliminar" data-id="<?php echo $estudio_idioma->id_estudio;?>"><i class="fa fa-trash"></i> Eliminar</button>-->
                                    </td>
                                </tr>
                                    <?php }?>   
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <ul class="pagination float-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                            <!-- fin tabla idioma -->

                            <!-- tabla blanco -->

                            <table id="tblanco" class="table table-stripped" data-page-size="8" data-filter=#filter>
                                <thead>
                                <tr>
                                    <th>N/A</th>
                                    <th>N/A</th>
                                    <th>N/A</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="gradeX">
                                    <td colspan="3"><div class="text-center alert alert-info">Selecciona una opción para filtrar</div></td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5">
                                        <ul class="pagination float-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                            <!-- fin tabla blanco -->

                        <!--  end contenido -->

                        <!-- tabla estudios conducentes y no conducentes -->
                        <!-- <div class="table-responsive">
                        <?php echo $this->mensaje; ?>
                            <table class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                            <tr>
                                <th>Estudio</th>
                                <th>Tipo de Estudio</th>

                                <th>Estatus</th>

                                <th id="titulo-idioma">Titulo Obtenido</th>
                                <th>Estado de Verificacion</th>

                                <th>Acciones</th>
                             <th>Código</th> como hago aqui con la foranea q esta en esta tabla llamada id_area_conocimiento_opsu 
                                
                            </tr>
                            </thead >

                            <tbody id="tbody-idioma">
                             
                                <?php 
                                        foreach($this->estudios_idioma as $row){
                                            $estudio_idioma= new Estructura();
                                            $estudio_idioma=$row;?>
                                <tr id ="fila-<?php echo $estudio_idioma->id_estudio; ?>" class="gradeX">
                                    <td><?php echo $estudio_idioma->estudio; ?></td>
                                    <td><?php echo $estudio_idioma->descripcion_tipo; ?></td>

                                    <td><?php if($estudio_idioma->estatus=='concluido'){ echo '<span class="label label-danger" style="font-size: 12px;">'.ucwords($estudio_idioma->estatus).'</span>'; } else{ echo '<span class="label label-success" style="font-size: 12px;">'.ucwords($estudio_idioma->estatus).'</span>';} ?></td>

                                    <td><?php echo $estudio_idioma->idioma; ?></td>

                                    <td><?php if($estudio_idioma->estatus_verificacion=='Sin Verificar'){ echo '<span class="label label-danger" style="font-size: 12px;">'.ucwords($estudio_idioma->estatus_verificacion).'</span>'; } else{ echo '<span class="label label-success" style="font-size: 12px;">'.ucwords($estudio_idioma->estatus_verificacion).'</span>';} ?></td>

                                    <td> 
                                        <a class="btn btn-outline btn-success" href="<?php echo constant ('URL') . "datos_academicos/viewEdit/" . $estudio_idioma->id_estudio_idioma.','.$estudio_idioma->id_tipo_estudio;?>" role="button"><i class="fa fa-edit"></i> Editar</a>&nbsp;
                                    como incluyo mas a class por las variables de estatus y la foranea
                                    <a class="btn btn-outline btn-info" href="<?php echo constant ('URL') . "datos_academicos/viewDetail/" . $estudio_idioma->id_estudio_idioma.','.$estudio_idioma->id_tipo_estudio;?>" role="button"><i class="fa fa-eye"></i> Ver</a>
<?php if($estudio_idioma->estatus=='concluido'){$estatus=1;} elseif($estudio_idioma->estatus=='en curso'){ $estatus=0;}?>
                                     <a class="btn btn-outline btn-danger" href="<?php echo constant('URL') . 'datos_academicos/delete/' . $estudio_idioma->id_estudio_idioma.','.$estudio_idioma->id_tipo_estudio.','.$estudio_idioma->id_estudio.','.$estatus;?>" role="button"><i class="fa fa-trash"></i> Remover</a> &nbsp; 
                                         <button class="btn btn-outline btn-danger bEliminar" data-id="<?php echo $estudio_idioma->id_estudio;?>"><i class="fa fa-trash"></i> Eliminar</button>
                                    </td>
                                </tr>
                                    <?php }?>
                              
                            </tbody>

                            <tbody id="tbody-estudio">
                                <?php 
                                // lo comente  $i=0;
                                        foreach($this->estudios as $row){
                                            $estudio= new Estructura();
                                            $estudio=$row;?>
                                <tr id ="fila-<?php echo $estudio->id_estudio; ?>" class="gradeX">
                                    <td><?php echo $estudio->estudio; ?></td>
                                    <td><?php echo $estudio->descripcion_tipo; ?></td>

                                    <td><?php if($estudio->estatus=='concluido'){ echo '<span class="label label-danger" style="font-size: 12px;">'.ucwords($estudio->estatus).'</span>'; } else{ echo '<span class="label label-success" style="font-size: 12px;">'.ucwords($estudio->estatus).'</span>';} ?></td>

                                    <td><?php echo $estudio->titulo_obtenido; ?></td>

                                    <td><?php if($estudio->estatus_verificacion=='Sin Verificar'){ echo '<span class="label label-danger" style="font-size: 12px;">'.ucwords($estudio->estatus_verificacion).'</span>'; } else{ echo '<span class="label label-success" style="font-size: 12px;">'.ucwords($estudio->estatus_verificacion).'</span>';} ?></td>

                                    <td> 
                                        <a class="btn btn-outline btn-success" href="<?php echo constant ('URL') . "datos_academicos/viewEdit/" . $estudio->id_estudio_conducente.','.$estudio->id_tipo_estudio;?>" role="button"><i class="fa fa-edit"></i> Editar</a>&nbsp;
                                    como incluyo mas a class por las variables de estatus y la foranea
                                        <a class="btn btn-outline btn-info" href="<?php echo constant ('URL') . "datos_academicos/viewDetail/" . $estudio->id_estudio_conducente.','.$estudio->id_tipo_estudio;?>" role="button"><i class="fa fa-eye"></i> Ver</a>
                                        <?php if($estudio->estatus=='concluido'){$estatus=1;} elseif($estudio->estatus=='en curso'){ $estatus=0;}?>

                                        <a class="btn btn-outline btn-danger" href="<?php echo constant('URL') . 'datos_academicos/delete/' . $estudio->id_estudio_conducente.','.$estudio->id_tipo_estudio.','.$estudio->id_estudio.','.$estatus;?>" role="button"><i class="fa fa-trash"></i> Remover</a> &nbsp; 
                                         <button class="btn btn-outline btn-danger bEliminar" data-id="<?php echo $estudio->id_estudio;?>"><i class="fa fa-trash"></i> Eliminar</button>
                                    </td>
                                </tr>
                                    <?php }?>
                            
                            </tbody>
                            <tbody id="blanco">
                            <td colspan="6"><div class="text-center alert alert-info">Selecciona una opción para filtrar</div></td>
                            
                            </tbody>
                            </table>
                        </div>

                          end contenido -->

                    </div>
                </div>
            </div>
            </div>
        </div>



    <?php require 'views/footer.php'; ?>

    <!-- FooTable -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/footable/footable.all.min.js"></script>
       <!-- Select2 -->
       <script src="<?php echo constant ('URL');?>src/js/plugins/select2/select2.full.min.js"></script>


    <!-- dataTables Scripts -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Page-Level Scripts -->
    <script>

$(document).ready(function(){
    var id=$("#estudiosi").val();
    mostrar(id);

    var id1=$("#filtro").val();
    mostrar2(id1);
    console.log(id1);

    $(".select2_demo_1").select2({
                                placeholder: "Seleccione la Categoría que desea Filtrar",
                                allowClear: true
                            });
                   

});
        function mostrar2(id){

            if(id=="conducente1"){
                
                //mostrar
                $("#tconducente").show();
                $("#tconducente").addClass('footable');
                $('.footable').footable();

                //ocultar
                $("#tblanco").hide();
                $("#tnoconducente").hide();
                $("#tnoconducente").removeClass('footable');
                $("#tidioma").hide();
                $("#tidioma").removeClass('footable');

            }

            if(id=="no-conducente1"){
                
                //mostrar
                $("#tnoconducente").show();
                $("#tnoconducente").addClass('footable');
                $('.footable').footable();

                //ocultar
                $("#tblanco").hide();
                $("#tconducente").hide();
                $("#tconducente").removeClass('footable');
                $("#tidioma").hide();
                $("#tidioma").removeClass('footable');


            }

            if(id=="idiomas1"){
                
                //mostrar
                $("#tidioma").show();
                $("#tidioma").addClass('footable');
                $('.footable').footable();

                //ocultar
                $("#tnoconducente").hide();
                $("#tnoconducente").removeClass('footable');
                $("#tblanco").hide();
                $("#tconducente").hide();
                $("#tconducente").removeClass('footable');


            }





            if(id==""){
                //mostrar
                $("#blanco").show();
                $("#tblanco").show();

                //ocultar
                $("#tconducente").hide();
                $("#tnoconducente").hide();
                $("#tidioma").hide();

            }

        }


        function mostrar(id){

            if(id=="conducente"){
                $("#tbody-estudio").show();
                $("#tbody-idioma").hide();
                $("#titulo-idioma").text('Título Obtenido');
                $("#blanco").hide();

            }

            if(id=="idiomas"){
                $("#tbody-estudio").hide();
                $("#tbody-idioma").show();
                $("#titulo-idioma").text('Idioma');
                $("#blanco").hide();


            }

            if(id==""){
                $("#tbody-estudio").hide();
                $("#tbody-idioma").hide();
                $("#titulo-idioma").text('N/A');
                $("#blanco").show();


            }
            
        }

        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>

   

</body>
</html>
