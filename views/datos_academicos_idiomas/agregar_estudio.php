<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SIDTA | Registrar Estudio</title>


    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">
  
    
    <!-- jasny input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <!--  style select2 -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/select2/select2.min.css" rel="stylesheet">
    <!-- datapicker -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <!-- sweetalert input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

</head>

<body>
 
    <div id="wrapper">
   <?php require 'views/header.php'; ?>
   

        <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-9">
                    <h2>Agregar Estudio </h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>home">Inicio</a>
                        </li>
                        
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>datos_academicos">Datos Académicos e Idiomas</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="">Listar Datos Académicos e Idiomas</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <a href="<?php echo constant ('URL');?>datos_academicos/viewAdd"><strong>Agregar</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-3">
                    <div class="title-action">
                        <a href="<?php echo constant ('URL');?>datos_academicos" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Volver</a>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5><i class="fa fa-angle-double-right"></i> SIDTA <i class="fa fa-angle-double-left"></i></h5>
                        </div>
                        <div class="ibox-content">
                            <h2>
                                Datos Académicos e Idiomas
                            </h2>
                            <p>
                                Completa el formulario para registrar un estudio Conducente o No Conducente a Grado, o incluso un Idioma.
                            </p>
                            <?php echo $this->mensaje;?>
                            
                            <div class="alert alert-info">Todos los campos marcados con un (<span style="color: red;">*</span>) Son Obligatorios</div>

                            <form class="m-t" role="form" id="form" action="<?php echo constant('URL') . "datos_academicos/registrarEstudio";?>" method="post" enctype="multipart/form-data">
                            <h1>Tipo Estudio</h1>
                                <fieldset>
                                    <h2>Datos Básicos del Estudio</h2>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                

                                                <select name="tipo_estudio" id="tipo_estudio" style="width: 100%;" onChange="mostrar(this.value);" class="form-control required" tabindex="4">
                                                    <option value="">Seleccione el tipo de estudio</option>
                                                    <?php 
                                                            foreach($this->tipos_estudio as $row){
                                                            $tipo_estudio=new Estructura();
                                                            $tipo_estudio=$row;?> 
                                                        <option value="<?php echo $tipo_estudio->id;?>"><?php echo $tipo_estudio->descripcion;?></option>
                                                        <?php }?>      
                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                    <!-- TIPO ESTUDIO: CONDUCENTE A GRADO -->
                                    <div class="row" id="wconducente"  style="display: none;">
                                            
                                        <div class="col-lg-6" >
                                            <div class="form-group">
                                                <label>Título Obtenido o por Obtener <span style="color: red;">*</span></label>
                                                <input type="text" minlength="5" maxlength="80" name="titulo_obtenido" id="titulo_obtenido" class="form-control" placeholder="Ingrese el Titulo obtenido o por obtener">
                                            </div>
                                            <div class="form-group">
                                                <label>Tipo de Estudio de Alto Nivel<span style="color: red;">*</span></label>
                                                <select name="programa_nivel" id="programa_nivel" style="width: 100%;" class="form-control select2_demo_4">
                                                    <option value="">Seleccione el nivel académico</option>
                                                    <?php 
                                                            foreach($this->programa_niveles as $row){
                                                            $programa_nivel=new Estructura();
                                                            $programa_nivel=$row;?> 
                                                        <option value="<?php echo $programa_nivel->id;?>"><?php echo $programa_nivel->descripcion;?></option>
                                                        <?php }?>      
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Nivel Académico del Estudio <span style="color: red;">*</span></label>
                                                <select name="nivel_academico" id="nivel_academico" style="width: 100%;"  class="form-control select2_demo_4">
                                                    <option value="">Seleccione el nivel académico</option>
                                                    <?php 
                                                            foreach($this->niveles_academicos as $row){
                                                            $nivel=new Estructura();
                                                            $nivel=$row;?> 
                                                        <option value="<?php echo $nivel->id;?>"><?php echo $nivel->descripcion;?></option>
                                                        <?php }?>      
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>¿Su estudio está concluido? <span style="color: red;">*</span></label>
                                                <div class="col-sm-6">
                                                    <div class="radio">
                                                        <input type="radio" name="estatus_c" id="radio1" value="concluido-conducente" onChange="mostrar2(this.value);" checked="">
                                                        <label for="radio1">
                                                            Si                                                            
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <input type="radio" name="estatus_c" id="radio2" onChange="mostrar2(this.value);" value="en_curso-conducente">
                                                        <label for="radio2">
                                                            No
                                                        </label>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <!-- estatus concluido -->
                                        <div class="row col-lg-12" id="wconcluido-conducente">
                                        
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label for="">Título del TEG <i>(Trabajo Especial de Grado)</i><span style="color: red;">*</span></label>
                                                        <input type="text" minlength="5" maxlength="120" name="titulo_trabajo" id="titulo_trabajo" class="form-control" placeholder="Ingrese el Titulo del TEG">
                                                    </div>
                                                
                                                
                                               
                                                </div>
                                                <div class="col-lg-6">
                                                    <label>Cargar Título Obtenido <i>(Formato PDF)</i><span style="color: red;">*</span></label>

                                                    <div class="form-group">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <span class="btn btn-default btn-file"><span class="fileinput-new">Seleccionar Archivo</span>
                                                            <span class="fileinput-exists">Cambiar</span><input type="file" id="file_conducente"   accept=".pdf" name="pdf_titulo"/></span>
                                                            <span class="fileinput-filename"></span>
                                                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                                            <label id="file_conducente-error" class="error" for="file_conducente"></label>
                                                            
                                                            <script>

                                                                // Cuando cambie #fichero
                                                                $("#file_conducente").change(function () {
                                                                    $('.error').text('');
                                                                    if(validarExtension(this)) { 
                                                                        if(validarPeso(this)) { 
                                                                        }
                                                                    } 
                                                                });

                                                            
                                                            </script>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>Resumen del TEG <i>(Trabajo Especial de Grado)</i><span style="color: red;">*</span></label>

                                                    <textarea name="resumen" id="resumen" cols="30" rows="4" class="form-control" placeholder="Ingrese el resumen del Trabajo Especial de Grado"></textarea>
                                                </div>
                                                </div>
                                        
                                        </div>
                                    </div>
                                    <!-- ///////////////////FIN CONDUCENTE //////////////////////////// -->
                                    
                                    
                                    
                                    <!-- tipo estudio: no conducente a grado-->
                                    <div class="row" id="wnoconducente" style="display: none;">
                                        <div class="col-lg-6" >
                                            <div class="form-group">
                                                <label>Título del Certificado Obtenido o por Obtener <span style="color: red;">*</span></label>
                                                <input type="text" minlength="5" maxlength="80" name="titulo_certificado" id="titulo_certificado" class="form-control" placeholder="Ingrese el Titulo del Certificado obtenido o por obtener">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                    <label>¿Su estudio está concluido? <span style="color: red;">*</span></label>
                                                    <div class="col-sm-6">
                                                        <div class="radio-primary">
                                                            <input type="radio" name="estatus_nc" id="radio3" value="concluido-noconducente" onChange="mostrar2(this.value);" checked="">
                                                            <label for="radio3">
                                                                Si
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <input type="radio" name="estatus_nc" id="radio4" onChange="mostrar2(this.value);" value="en_curso-noconducente">
                                                            <label for="radio4">
                                                                No
                                                            </label>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                        </div>
                                            <!-- estatus concluido -->

                                            <div class="col-lg-6" id="wconcluido-noconducente">
                                                <label>Cargar Certificado Obtenido <i>(Formato PDF)</i><span style="color: red;">*</span></label>

                                                <div class="form-group">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <span class="btn btn-default btn-file"><span class="fileinput-new">Seleccionar Archivo</span>
                                                        <span class="fileinput-exists">Cambiar</span><input type="file" id="file_no_conducente" accept=".pdf" name="pdf_certificado"/></span>
                                                        <span class="fileinput-filename"></span>
                                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                                        <label id="file_no_conducente-error" class="error" for="file_no_conducente"></label>

                                                    </div>
                                                </div>

                                                <script>
                                                    $("#file_no_conducente").change(function () {
                                                        $('.error').text('');
                                                        if(validarExtension(this)) { 
                                                            if(validarPeso(this)) { 
                                                            }
                                                        }  
                                                    });
                                                </script>

                                            </div>
                                        
                                    </div>
                                    <!-- ///////////////////FIN NO CONDUCENTE //////////////////////////// -->




                                    <!-- tipo estudio: idioma -->
                                    <div class="row" id="widioma" style="display: none;">

                                        <div class="col-lg-12">
                                                <h2>Datos Básicos del Idioma</h2>
                                                
                                            </div>
                                            
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Idioma <span style="color: red;">*</span></label>
                                                    
                                                    <select name="idioma" id="idioma" style="width: 100%;" class="form-control select2_demo_4">
                                                        <option value="">Seleccione el idioma  que domina</option>
                                                        <?php 
                                                                foreach($this->idiomas as $row){
                                                                $idioma=new Estructura();
                                                                $idioma=$row;?> 
                                                            <option value="<?php echo $idioma->id_idioma;?>"><?php echo $idioma->name;?></option>
                                                            <?php }?>      
                                                    </select>

                                                    <script>
                                                   
                                                        $(".select2_demo_4").select2({
                                                            placeholder: "Seleccione el idioma  que domina",
                                                            allowClear: true
                                                        });
                                                    </script>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Nivel de Escritura <span style="color: red;">*</span></label>
                                                    <select name="nivel_escritura" id="nivel_escritura" class="form-control">
                                                        <option value="">Seleccione el nivel de escritura que posee el idioma</option>
                                                        <option value="basico">Básico</option>
                                                        <option value="intermedio">Intermedio</option>
                                                        <option value="avanzado">Avanzado</option>

                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Nivel de Comprensión <span style="color: red;">*</span></label>
                                                    <select name="nivel_comprende" id="nivel_comprende" class="form-control">
                                                        <option value="">Seleccione el nivel en que comprende que posee el idioma</option>
                                                        <option value="basico">Básico</option>
                                                        <option value="intermedio">Intermedio</option>
                                                        <option value="avanzado">Avanzado</option>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Nivel de Lectura <span style="color: red;">*</span></label>
                                                    <select name="nivel_lectura" id="nivel_lectura" class="form-control">
                                                        <option value="">Seleccione el nivel de lectura que posee el idioma</option>
                                                        <option value="basico">Básico</option>
                                                        <option value="intermedio">Intermedio</option>
                                                        <option value="avanzado">Avanzado</option>

                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Nivel de Habla <span style="color: red;">*</span></label>
                                                    <select name="nivel_habla" id="nivel_habla" class="form-control">
                                                        <option value="">Seleccione el nivel del habla que posee el idioma</option>
                                                        <option value="basico">Básico</option>
                                                        <option value="intermedio">Intermedio</option>
                                                        <option value="avanzado">Avanzado</option>

                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label>¿Posee Certificado de Competencia en el Idioma? <span style="color: red;">*</span></label>
                                                    <div class="col-sm-6">
                                                        <div class="radio-primary">
                                                            <input type="radio" name="competencia" id="radio5" value="certificado-idioma" onChange="mostrar2(this.value);" checked="">
                                                            <label for="radio5">
                                                                Si
                                                            </label>
                                                        </div>
                                                        <div class="radio">
                                                            <input type="radio" name="competencia" id="radio6" onChange="mostrar2(this.value);" value="nocertificado-idioma">
                                                            <label for="radio6">
                                                                No
                                                            </label>
                                                        </div>
                                                    </div>
                                                    
                                                </div>

                                            </div>
                                                <!-- estatus concluido -->

                                                <div class="col-lg-6" id="wcertificado-idioma">
                                                    <label>Cargar Certificado de Competencia <i>(Formato PDF)</i><span style="color: red;">*</span></label>

                                                    <div class="form-group">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <span class="btn btn-default btn-file"><span class="fileinput-new">Seleccionar Archivo</span>
                                                            <span class="fileinput-exists">Cambiar</span><input type="file" id="file_idioma" accept=".pdf"  name="file_certificado"/></span>
                                                            <span class="fileinput-filename"></span>
                                                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                                            <label id="file_idioma-error" class="error" for="file_idioma"></label>

                                                        </div>
                                                    </div>
                                                </div>
                                                <script>
                                                    $("#file_idioma").change(function () {
                                                        $('.error').text('');
                                                        if(validarExtension(this)) { 
                                                            if(validarPeso(this)) { 
                                                            }
                                                        }  
                                                    });
                                                </script>
                                            
                                        </div>
                                        <!-- /////////////////// FIN IDIOMA //////////////////////////// -->

                                        
                                        
                                        
                                        
                                </fieldset>
                                <h1>Datos del Estudio</h1>
                                <fieldset>
                                    <h2>Datos Básicos del Estudio</h2>
                                    
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Nombre del Estudio <span style="color: red;">*</span></label>
                                                    <input type="text" minlength="5" maxlength="80" name="estudio" id="estudio" class="form-control required" placeholder="Ingrese el Nombre del estudio">
                                                </div>
                                                <div class="form-group">
                                                    <label>País de Realización <span style="color: red;">*</span></label>
                                                    <select name="pais" id="pais" style="width: 100%;"  class="form-control required select2_demo_1" tabindex="4">
                                                        <option value="">Seleccione el país donde realizó el estudio</option>
                                                        <?php 
                                                            foreach($this->paises as $row){
                                                            $pais=new Estructura();
                                                            $pais=$row;?> 
                                                        <option value="<?php echo $pais->id_pais;?>"><?php echo $pais->descripcion;?></option>
                                                        <?php }?>                                         
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Modalidad de Estudio <span style="color: red;">*</span></label>
                                                    <select name="modalidad" id="modalidad" style="width: 100%;"  class="form-control required select2_demo_2" tabindex="4">
                                                        <option value="">Seleccione la modalidad del estudio</option>
                                                            <?php 
                                                                foreach($this->modalidades as $row){
                                                                $modalidad=new Estructura();
                                                                $modalidad=$row;?> 
                                                            <option value="<?php echo $modalidad->id;?>"><?php echo $modalidad->descripcion;?></option>
                                                            <?php }?>                                         
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Tipo de Institución <span style="color: red;">*</span></label>
                                                    <select name="institucion_tipo" id="institucion_tipo" style="width: 100%;"  class="form-control required select2_demo_3" tabindex="4">
                                                        <option value="">Seleccione el tipo de institución</option>
                                                        <?php 
                                                            foreach($this->tipos_institucion as $row){
                                                            $tipo_institucion=new Estructura();
                                                            $tipo_institucion=$row;?> 
                                                        <option value="<?php echo $tipo_institucion->id;?>"><?php echo $tipo_institucion->descripcion;?></option>
                                                        <?php }?>                                         
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Institución <span style="color: red;">*</span></label>
                                                    <input type="text" maxlength="80" autocomplete="off" name="institucion" id="institucion" class="form-control typeahead_3 required" placeholder="Ingrese el Nombre de la Institución donde realizó estudio">
                                                </div>
                                                
                                                <div class="form-group" id="data_2">
                                                <label>Año de Realización <span style="color: red;">*</span></label>
                                                <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <input type="text" style="margin-bottom: 5px;" name="ano_r" id="ano_r" class="form-control required number" placeholder="Seleccione el Año en que realizó el estudio">
                                                </div>
                                                <label id="ano_r-error" class="error" for="ano_r"></label>
                                            </div>
                                            <script>
                                            var mem = $('#data_2 .input-group.date').datepicker({
                                                format: 'yyyy',
                                                viewMode: "years",
                                                minViewMode: "years",
                                                autoclose: true
                                                
                                            });
                                            </script>
                                                
                                            </div>
                                        </div>
                                        <script> $(".select2_demo_1").select2({
                                                        placeholder: "Seleccione el pais donde realizó el estudio",
                                                        allowClear: true
                                                    });
                                                    $(".select2_demo_2").select2({
                                                        placeholder: "Seleccione la modalidad del estudio",
                                                        allowClear: true
                                                    });
                                                    $(".select2_demo_3").select2({
                                                        placeholder: "Seleccione el tipo de la Institución",
                                                        allowClear: true
                                                    });
                                                </script>
                                        
                                        <script>
                                                    $(document).ready(function(){

                                                        
                                                        $('.typeahead_3').typeahead({
                                                            source: [

                                                                
                                                                <?php 
                                                                    foreach($this->instituciones as $row){
                                                                        $institucion=new Estructura();
                                                                        $institucion=$row;
                                                                        ?>
                                                                {"name": "<?php echo $institucion->descripcion;?>", "code": "BR", "ccn0": "080"},
                                                                <?php }?>
                                                                
                                                            ]
                                                                                                            
                                                        });


                                                    });
                                                </script>
                                        
                                </fieldset>
                               
                            </form>

           
                        </div>

                        
                    </div>

                    
                    </div>
                    

                </div>
                
                </div>



        <?php require 'views/footer.php'; ?>

        
    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>
    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    

    <!-- Sweet alert -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/sweetalert/sweetalert.min.js"></script>

   <!-- Input Mask-->
   <script src="<?php echo constant ('URL');?>src/js/plugins/jasny/jasny-bootstrap.min.js"></script>

       <!-- Select2 -->
       <script src="<?php echo constant ('URL');?>src/js/plugins/select2/select2.full.min.js"></script>

    <!-- Data picker -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>

    <!-- Typehead -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>

    

                <script type="text/javascript">
                var extensionesValidas = ".pdf";

                var pesoPermitido = 2000;//2MB = 2000 kb
                //console.log(pesoPermitido, extensionesValidas);
                // Validacion de extensiones permitidas
                function validarExtension(datos) {
                    
                    var ruta = datos.value;
                    var extension = ruta.substring(ruta.lastIndexOf('.') + 1).toLowerCase();
                    
                    var extensionValida = extensionesValidas.indexOf(extension);

                    //console.log(ruta, extension, extensionValida);
                    if(extensionValida < 0) {
                            $('.error').text('La extensión no es válida Su Archivo tiene de extensión: .'+ extension);
                            //alert('La extensión no es válida Su Archivo tiene de extensión: .'+ extension);
                        swal("Ha ocurrido un Error", "La extensión no es válida Su Archivo tiene de extensión: ."+extension, "error");  
                        document.getElementById("file_conducente").value = "";
                        document.getElementById("file_no_conducente").value = "";
                        document.getElementById("file_idioma").value = "";
                        
                        return false;
                    } else {
                        return true;
                    }
                }


                // Validacion de peso del fichero en kbs
                function validarPeso(datos) {
                if (datos.files && datos.files[0]) {
                    var pesoFichero = datos.files[0].size/2000;
                    if(pesoFichero > pesoPermitido) {
                        $('.error').text('El peso máximo permitido del Archivo es: ' + pesoPermitido + ' KBs Su fichero tiene: '+ pesoFichero +' KBs');
                        //alert('El peso maximo permitido del Archivo es: ' + pesoPermitido + ' KBs Su Archivo tiene: '+ pesoFichero +' KBs');
                        
                        swal("Ha ocurrido un Error","El peso máximo permitido del Archivo es: " + pesoPermitido + " KBs Su Archivo tiene: "+ pesoFichero +" KBs", "error");    
                        document.getElementById("file_conducente").value = "";
                        document.getElementById("file_no_conducente").value = "";
                        document.getElementById("file_idioma").value = "";
                        
                        return false;
                    } else {
                        return true;
                    }
                }
            }

                    //muestra un form u otro dependiendo del tipo de estudio
                    function mostrar(id) {

                        
                        //3=Idioma
                        if (id == "3") {
                            //mostrar
                            $("#widioma").show();
                            $("#file_idioma").addClass('required');
                            $("#nivel_escritura").addClass('required');
                            $("#nivel_comprende").addClass('required');
                            $("#nivel_habla").addClass('required');
                            $("#nivel_lectura").addClass('required');

                            //ocultar
                            $("#wconducente").hide();
                            $("#wnoconducente").hide();
                            $("#titulo_obtenido").removeClass('required');
                            $("#titulo_certificado").removeClass('required');
                            $("#idioma").addClass('required');
                            $("#file_no_conducente").removeClass('required');
                            $("#titulo_trabajo").removeClass('required');
                            $("#resumen").removeClass('required');
                            $("#file_conducente").removeClass('required');
                            $("#nivel_academico").removeClass('required');
                            $("#programa_nivel").removeClass('required');

                        }
                        //1=Conducente
                        if (id == "1") {
                            

                            //mostrar
                            $("#wconducente").show();
                            $("#titulo_trabajo").addClass('required');
                            $("#resumen").addClass('required');
                            $("#file_conducente").addClass('required');
                            $("#nivel_academico").addClass('required');
                            $("#programa_nivel").addClass('required');

                            //ocultar
                            $("#widioma").hide();
                            $("#wnoconducente").hide();
                            $("#file_idioma").removeClass('required');
                            $("#titulo_obtenido").addClass('required');
                            $("#titulo_certificado").removeClass('required');
                            $("#idioma").removeClass('required');
                            $("#nivel_escritura").removeClass('required');
                            $("#nivel_comprende").removeClass('required');
                            $("#nivel_habla").removeClass('required');
                            $("#nivel_lectura").removeClass('required');
                            $("#file_no_conducente").removeClass('required');
                            
                            

                        }
                    //2=noconducente
                        if (id == "2") {
                           
                            //mostrar
                            $("#wnoconducente").show();
                            $("#file_no_conducente").addClass('required');
                            
                            //ocultar
                            $("#widioma").hide();
                            $("#wconducente").hide();
                            $("#file_idioma").removeClass('required');
                            $("#titulo_obtenido").removeClass('required');
                            $("#titulo_certificado").addClass('required');
                            $("#idioma").removeClass('required');
                            $("#nivel_escritura").removeClass('required');
                            $("#nivel_comprende").removeClass('required');
                            $("#nivel_habla").removeClass('required');
                            $("#nivel_lectura").removeClass('required');
                            $("#titulo_trabajo").removeClass('required');
                            $("#resumen").removeClass('required');
                            $("#file_conducente").removeClass('required');
                            $("#nivel_academico").removeClass('required');
                            $("#programa_nivel").removeClass('required');

                            

                        }

                        if (id == "") {
                            $("#widioma").hide();
                            $("#wconducente").hide();
                            $("#wnoconducente").hide();

                            $("#titulo_obtenido").removeClass('required');
                            $("#titulo_certificado").removeClass('required');
                            $("#idioma").removeClass('required');
                            $("#nivel_escritura").removeClass('required');
                            $("#nivel_comprende").removeClass('required');
                            $("#nivel_habla").removeClass('required');
                            $("#nivel_lectura").removeClass('required');
                            $("#file_idioma").removeClass('required');
                            $("#titulo_trabajo").removeClass('required');
                            $("#resumen").removeClass('required');
                            $("#file_conducente").removeClass('required');
                            $("#nivel_academico").removeClass('required');
                            $("#programa_nivel").removeClass('required');

                        }
                        
                    }

                    //muestra un form u otro dependiendo de la respuesta SI/NO en cada uno de los casos 
                    function mostrar2(id_radio){

                        if(id_radio == "concluido-conducente"){
                            $("#wconcluido-conducente").show();
                            $("#titulo_trabajo").addClass('required');
                            $("#resumen").addClass('required');
                            $("#file_conducente").addClass('required');

                        }

                        if(id_radio == "en_curso-conducente"){
                            $("#wconcluido-conducente").hide();
                            $("#titulo_trabajo").removeClass('required');
                            $("#resumen").removeClass('required');
                            $("#file_conducente").removeClass('required');
                        }

                        if(id_radio == "concluido-noconducente"){
                            $("#wconcluido-noconducente").show();
                            $("#file_no_conducente").addClass('required');
                        }

                        if(id_radio == "en_curso-noconducente"){
                            $("#wconcluido-noconducente").hide();
                            $("#file_no_conducente").removeClass('required');

                        }

                        if(id_radio == "certificado-idioma"){
                            $("#wcertificado-idioma").show();
                            $("#file_idioma").addClass('required');

                        }

                        if(id_radio == "nocertificado-idioma"){
                            $("#wcertificado-idioma").hide();
                            $("#file_idioma").removeClass('required');

                        }


                    }
                </script>
       

   
    <script>



            

        $(document).ready(function(){

            

            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                   
                   
                    ////////////////////////////////////////       
                  

                    ////////////////////////////////////////   


                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            
                                ano_r: {

                                    esfecha: true

                                }
                        }
                       
                    });
       });
       
       $.validator.addMethod("esfecha", esFechaActual, "El Año de Realización no debe ser mayor al Año Actual");


        function esFechaActual(value, element, param) {

            
            var fechaActual = <?php echo date('Y');?>;

            if (value > fechaActual) {

                return false; //error de validación

            }

            else {

                return true; //supera la validación

            }

        }

    </script>

   

</body>
</html>
