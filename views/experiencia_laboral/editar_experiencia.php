<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SIDTA | Editar Experiencia</title>


    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">
  
    
    <!-- jasny input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <!--  style select2 -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/select2/select2.min.css" rel="stylesheet">
    <!-- datapicker -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <!-- sweetalert input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

</head>

<body>
 
    <div id="wrapper">
   <?php require 'views/header.php'; ?>
   

        <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-9">
                    <h2>Agregar Experiencia </h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>home">Inicio</a>
                        </li>
                        
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>experiencia_laboral">Experiencia Laboral</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="">Listar Experiencia Laboral</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <a href="<?php echo constant ('URL');?>experiencia_laboral/viewAdd"><strong>Editar</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-3">
                    <div class="title-action">
                        <a href="<?php echo constant ('URL');?>experiencia_laboral" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Volver</a>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5><i class="fa fa-angle-double-right"></i> SIDTA <i class="fa fa-angle-double-left"></i></h5>
                        </div>
                        <div class="ibox-content">
                            <h2>
                                Experiencia Laboral
                            </h2>
                            <p>
                                Modifica el dato que desees de una Experiencia Laboral.
                            </p>
                            <?php echo $this->mensaje;?>
                            <div class="alert alert-info">Todos los campos marcados con un (<span style="color: red;">*</span>) Son Obligatorios</div>
                            <?php
                                session_start(); 
                            ?>
                            <form class="m-t" role="form" id="form" action="<?php echo constant('URL') . "experiencia_laboral/editar/".$_SESSION['id_experiencia_tabla'].",".$_SESSION['id_tipo_experiencia'];?>" method="post" enctype="multipart/form-data">
                                <h1>Tipo de Experiencia</h1>
                                <fieldset>
                                    <h2>Datos Básicos de la Experiencia</h2>
                                    <input type="hidden" name="id_experiencia_tabla" value="<?php echo $_SESSION['id_experiencia_tabla'];?>">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Tipo de Experiencia <span style="color: red;">*</span></label>
                                                <input type="hidden" name="tipo_experiencia" value="<?php echo $_SESSION['id_tipo_experiencia'];?>">
                                                <select name="" id="tipo_experiencia" disabled style="width: 100%;" onChange="mostrar(this.value);" class="form-control required" tabindex="4">
                                                    <option value="">Seleccione el Tipo de Experiencia</option>
                                                    <?php 
                                                            foreach($this->tipos_experiencia as $row){
                                                            $tipo_experiencia=new Estructura();
                                                            $tipo_experiencia=$row;?> 
                                                        <option value="<?php echo $tipo_experiencia->id;?>" <?php if($tipo_experiencia->id==$this->experiencia->id_tipo_experiencia){ print "selected=selected";}?>><?php echo $tipo_experiencia->descripcion;?></option>
                                                        <?php }?>      
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Tipo de Entidad <span style="color: red;">*</span></label>
                                                <?php if($this->experiencia->id_institucion_tipo==1 && $this->experiencia->id_tipo_experiencia==1){ ?> 
                                                    <input type="hidden" name="institucion_tipo" value="<?php echo $this->experiencia->id_institucion_tipo;?>"> 
                                                <?php }?>
                                                <select name="institucion_tipo" id="institucion_tipo" style="width: 100%;" onChange="mostrar2(this.value);" class="form-control required administrativo select2_demo_3" tabindex="4">
                                                    <option value="">Seleccione el el tipo de institución</option>
                                                    <?php 
                                                            foreach($this->tipos_institucion as $row){
                                                            $tipo_institucion=new Estructura();
                                                            $tipo_institucion=$row;?> 
                                                    <option value="<?php echo $tipo_institucion->id;?>" <?php if($tipo_institucion->id==$this->experiencia->id_institucion_tipo){ print "selected=selected";}?>><?php echo $tipo_institucion->descripcion;?></option>
                                                    <?php }?>                                         
                                                </select>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <!-- TIPO EXPERIENCIA: TRABAJO ADMINISTRATIVO -->
                                    <div class="row" id="wadministrativo"  style="display: none;">
                                            
                                        <div class="col-lg-6" >
                                            <div class="form-group">
                                                <label>Período <span style="color: red;">*</span></label>
                                                <input type="text" value="<?php echo $this->experiencia->periodo; ?>" maxlength="80" name="periodo_admin" id="periodo_admin" class="form-control" placeholder="Ingrese el Titulo obtenido o por obtener">
                                            </div>
                                            </div>

                                        <!-- institucion publica  -->
                                        <div style="display: none;" class="row col-lg-6" id="wconstancia-publico">
                                        
                                            <?php if($this->experiencia->id_institucion_tipo==1){?>
                                                                
                                                <label>Ver Constancia de Trabajo <i>(Formato PDF)</i><span style="color: red;">*</span></label>
                                                <div class="form-group">
                                                    <a target="_blank" href="<?php echo constant('URL') . "experiencia_laboral/viewDocumento/" . $this->id_trabajo_administrativo;?>" class="btn btn-primary"><i class="fa fa-external-link"></i> Constancia</a>                                                         
                                                    <a href="<?php echo constant('URL').'src/documentos/experiencia_laboral/';?>"  download="<?php echo $this->descripcion_documento;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>
                                                </div>
                                            <?php } else{?>
                                                    <label>Cargar Constancia de Trabajo <i>(Formato PDF)</i><span style="color: red;">*</span></label>
                                                    <div class="form-group">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <span class="btn btn-default btn-file"><span class="fileinput-new">Seleccionar Archivo</span>
                                                            <span class="fileinput-exists">Cambiar</span><input type="file" id="file_publico"   accept=".pdf" name="file_publico"/></span>
                                                            <span class="fileinput-filename"></span>
                                                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">×</a>
                                                        </div>
                                                    </div>
                                            <?php }?>

                                        </div>
                                    </div>
                                    <!-- ///////////////////FIN ADMINISTRATIVO //////////////////////////// -->
                                    
                                    <script>
                                    

                                    </script>
                                    
                                    <!-- tipo experiencia: docencia previa-->
                                    <div class="row" id="wdocencia" style="display: none;">
                                        <div class="col-lg-6" >
                                            <div class="form-group">
                                                <label>Nombre de la Unidad Curricular <span style="color: red;">*</span></label>
                                                <input type="text" value="<?php echo $this->experiencia->materia; ?>" maxlength="80" name="unidad_c" id="unidad_c" class="form-control" placeholder="Ingrese el Nombre de la Unidad Curricular">
                                            </div>
                                            <div class="form-group">
                                                <label>Nivel Académico <span style="color: red;">*</span></label>
                                                <select name="nivel" id="nivel" style="width: 100%;" class="form-control select2_demo_2" tabindex="4">
                                                        <option value="">Seleccione el nivel académico</option>
                                                            <?php 
                                                            foreach($this->niveles as $row){
                                                            $nivel_academico=new Estructura();
                                                            $nivel_academico=$row;?> 
                                                        <option value="<?php echo $nivel_academico->id;?>" <?php if($nivel_academico->id==$this->experiencia->id_nivel_academico){ print "selected=selected";}?>><?php echo $nivel_academico->descripcion;?></option>
                                                        <?php }?>                                         
                                                </select>                                            
                                            </div>
                                            <div class="form-group">
                                                    <label>Modalidad <span style="color: red;">*</span></label>
                                                    <select name="modalidad" id="modalidad" style="width: 100%;" class="form-control select2_demo_2" tabindex="4">
                                                        <option value="">Seleccione la modalidad</option>
                                                            <?php 
                                                            foreach($this->modalidades as $row){
                                                            $modalidad=new Estructura();
                                                            $modalidad=$row;?> 
                                                        <option value="<?php echo $modalidad->id;?>" <?php if($modalidad->id==$this->experiencia->id_modalidad_estudio){ print "selected=selected";}?>><?php echo $modalidad->descripcion;?></option>
                                                        <?php }?>                                         
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Semestre<span style="color: red;">*</span></label>
                                                <input type="text" value="<?php echo $this->experiencia->semestre; ?>" maxlength="80" name="semestre" id="semestre" class="form-control number" placeholder="Ingrese el nro de semestre">
                                            </div>
                                            
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Horas Semanales<span style="color: red;">*</span></label>
                                                <input type="text" value="<?php echo $this->experiencia->horas_semanales;?>" maxlength="80" name="horas" id="horas" class="form-control number" placeholder="Ingrese el Nro. de Horas">
                                            </div>
                                            <div class="form-group">
                                                <label>Nro. Estudiantes Atendidos<span style="color: red;">*</span></label>
                                                <input type="text" value="<?php echo $this->experiencia->estudiantes_atendidos;?>" maxlength="80" name="nro_estudiantes" id="nro_estudiantes" class="form-control" placeholder="Ingrese el Nro. de Estudiantes Atendidos">
                                            </div>
                                            <div class="form-group">
                                                <label>Naturaleza <span style="color: red;">*</span></label>
                                                <input type="text" value="<?php echo $this->experiencia->naturaleza;?>" maxlength="80" name="naturaleza" id="naturaleza" class="form-control" placeholder="Ingrese la Naturaleza de la Unidad Curricular">
                                            </div>
                                            <?php 
                                                $periodo=array('I', 'II'); 
                                                $i=0;
                                            ?>
                                            <div class="form-group">
                                                <label>Período Lectivo <span style="color: red;">*</span></label>
                                                <select class="form-control m-b "  name="periodo_docencia" id="periodo_docencia">                                      
                                                    <option value="" selected="selected">Selecione el periodo</option>
                                                    <?php 
                                                        while($i<2){
                                                    ?>
                                                    <option value="<?php echo $periodo[$i];?>" <?php if($periodo[$i]==$this->experiencia->periodo_lectivo){ print "selected=selected";}?>>Período <?php echo $periodo[$i];?></option>

                                                    <?php 
                                                        $i++;
                                                    }
                                                    ?>
                                                </select>                                              
                                            </div>
                                        </div>
                                            </div>
                                    <!-- /////////////////// FIN DOCENCIA PREVIA //////////////////////////// -->



                                    <!-- tipo Experiencia comision gubernamental -->
                                    <div class="row" id="wcomision" style="display: none;">

                                        <div class="col-lg-12">
                                            <h2>Datos Básicos de la Comisión</h2>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Dirección <span style="color: red;">*</span></label>
                                                    <input type="text" value="<?php echo $this->experiencia->lugar;?>" maxlength="80" name="direccion" id="direccion" class="form-control" placeholder="Ingrese la Dirección del Lugar Donde Realizó la Comisión">
                                                </div>
                                        </div>
                                        <!-- /////////////////// FIN COMISION GUBERNAMENTAL //////////////////////////// -->

                                </fieldset>
                                <h1>Datos de la Experiencia</h1>
                                <fieldset>
                                    <h2>Datos Básicos de la Experiencia</h2>
                                    
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Cargo Experiencia <span style="color: red;">*</span></label>
                                                    <input type="text" value="<?php echo $this->experiencia->cargo; ?>" maxlength="80" autocomplete="off" name="cargo" id="cargo" class="form-control required typeahead_2" placeholder="Ingrese el Nombre del cargo que desempeño">
                                                </div>
                                                <div class="form-group">
                                                    <label>País <span style="color: red;">*</span></label>
                                                    <select name="pais" id="pais" style="width: 100%;"  class="form-control required" tabindex="4">
                                                        <option id="tpais"value="">Seleccione el País</option>
                                                        <!-- <?php 
                                                            foreach($this->paises as $row){
                                                            $pais=new Estructura();
                                                            $pais=$row;?> 
                                                        <option value="<?php echo $pais->id_pais;?>" <?php if($pais->id_pais==$this->experiencia->id_pais){ print "selected=selected";}?>><?php echo $pais->descripcion;?></option>
                                                        <?php }?> -->                                        
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Estado <span style="color: red;">*</span></label>
                                                    <select name="estado" id="estado" style="width: 100%;" class="form-control required" tabindex="4">
                                                        <option id="testado" value="">Seleccione el Estado</option>
                                                                                              
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                            <div class="form-group" id="data_2">
                                                    <label>Año de Realización <span style="color: red;">*</span></label>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        <input type="text" value="<?php echo $this->experiencia->ano_experiencia; ?>" style="margin-bottom: 5px;" name="ano_r" id="ano_r" class="form-control required number" placeholder="Seleccione el Año en que realizó el estudio">
                                                    </div>
                                                    <label id="ano_r-error" class="error" for="ano_r"></label>
                                                </div>
                                                <script>
                                                var mem = $('#data_2 .input-group.date').datepicker({
                                                    format: 'yyyy',
                                                    viewMode: "years",
                                                    minViewMode: "years",
                                                    autoclose: true
                                                });
                                                </script>
                                                <div class="form-group">
                                                    <label>Institución <span style="color: red;">*</span></label>
                                                    <input type="text" value="<?php echo $this->experiencia->institucion; ?>" maxlength="80" autocomplete="off" name="institucion" id="institucion" class="form-control typeahead_3 required" placeholder="Ingrese el Nombre de la Institución donde desempeñó sus labores">
                                                </div>
                                                <div class="form-group" id="data_5">
                                                <label class="font-normal">Fecha de Ingreso <span style="color: red;">*</span></label>
                                                <label class="font-normal" style="position: relative;left: 110px;">Fecha de Egreso <span style="color: red;">*</span></label>
                                                <div class="input-daterange input-group" id="datepicker">
                                                    <input type="text" value="<?php echo $this->experiencia->fecha_ingreso; ?>" class="form-control-sm form-control required" name="start" placeholder="<?php echo 'm/d/Y';?>">
                                                    <span class="input-group-addon">&nbsp;Hasta&nbsp;</span>    
                                                    <input type="text" value="<?php echo $this->experiencia->fecha_egreso; ?>" class="form-control-sm form-control required" name="end" placeholder="<?php echo 'm/d/Y';?>">
                                                </div>
                                                <span><i style="margin-right: 11.5em;">mm/dd/aaaa</i><i>mm/dd/aaaa</i></span>
                                            </div>
                                            <label id="start-error" class="error" for="start"></label>
                                            <label id="end-error" class="error" for="end"></label>
                                                <script>
                                                    $('#data_5 .input-daterange').datepicker({
                                                        keyboardNavigation: false,
                                                        forceParse: false,
                                                        autoclose: true
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                        <script>
                                    $(function(){

                                    // Lista de paises
                                    $.post( '<?php echo constant ('URL');?>models/comboubicacion-registro.php' ).done( function(respuesta)
                                    {
                                        $( '#pais' ).html( respuesta );
                                        
                                        $( '#pais option[value="'+<?php echo $this->experiencia->id_pais; ?>+'"]' ).attr("selected", true);

                                        if($("#pais").val() == '<?php echo $this->experiencia->id_pais;?>'){
                                            
                                            $('#tpais').append('<option value="<?php echo $this->experiencia->id_pais;?>" selected="selected"><?php echo $this->experiencia->descripcion_pais;?></option>');
                                        }


                                    });

                                    $.post( '<?php echo constant ('URL');?>models/comboubicacion-registro.php', { pais: <?php echo $this->experiencia->id_pais;?>} ).done( function( respuesta )
                                        {
                                            $( '#estado' ).html( respuesta );
                                            
                                            $( '#estado option[value="'+<?php echo $this->experiencia->id_estado; ?>+'"]' ).attr("selected", true);

                                            if($("#estado").val() == '<?php echo $this->experiencia->id_estado;?>'){
                                                $('#testado').append('<option value="<?php echo $this->experiencia->id_estado;?>" selected="selected"><?php echo $this->experiencia->descripcion_estado;?></option>');

                                            }
                                        });

                                    // lista de estados	
                                    $('#pais').change(function()
                                    {
                                        var id_pais = $(this).val();
                                        
                                        // Lista de estados
                                        $.post( '<?php echo constant ('URL');?>models/comboubicacion-registro.php', { pais: id_pais} ).done( function( respuesta )
                                        {
                                            $( '#estado' ).html( respuesta );
                                           
                                        });
                                    });


                                


                                        // lista de MUNICIPIOS	
                                        $('#estado').change(function()
                                        {
                                            var id_estado = $(this).val();
                                            
                                            // Lista de estados
                                            $.post( '<?php echo constant ('URL');?>models/comboubicacion-registro.php', { estado: id_estado} ).done( function( respuesta )
                                            {
                                                $( '#municipio' ).html( respuesta );
                                            });
                                        });

                                        // lista de PARROQUIAS	
                                        $('#municipio').change(function()
                                        {
                                            var id_municipio = $(this).val();
                                            
                                            // Lista de estados
                                            $.post( '<?php echo constant ('URL');?>models/comboubicacion-registro.php', { municipio: id_municipio} ).done( function( respuesta )
                                            {
                                                $( '#parroquia' ).html( respuesta );
                                            });
                                        });

                                        })
                                    </script> 

                                        <script> 
                                                    $(".select2_demo_1").select2({
                                                        placeholder: "Seleccione el pais donde realizó la Experiencia",
                                                        allowClear: true
                                                    });
                                                    $(".select2_demo_2").select2({
                                                        placeholder: "Seleccione el Estado donde realizó la Experiencia",
                                                        allowClear: true
                                                    });
                                                    $(".select2_demo_3").select2({
                                                        placeholder: "Seleccione el tipo de la Institución",
                                                        allowClear: true
                                                    });
                                                
                                                    $(document).ready(function(){

                                                        
                                                        $('.typeahead_3').typeahead({
                                                            source: [

                                                                
                                                                <?php 
                                                                    foreach($this->instituciones as $row){
                                                                        $institucion=new Estructura();
                                                                        $institucion=$row;
                                                                        ?>
                                                                {"name": "<?php echo $institucion->descripcion;?>", "code": "BR", "ccn0": "080"},
                                                                <?php }?>
                                                                
                                                            ]
                                                                                                            
                                                        });

                                                        $('.typeahead_2').typeahead({
                                                            source: [

                                                                
                                                                <?php 
                                                                    foreach($this->cargos_experiencia as $row){
                                                                        $cargo=new Estructura();
                                                                        $cargo=$row;
                                                                        ?>
                                                                {"name": "<?php echo $cargo->descripcion;?>", "code": "BR", "ccn0": "080"},
                                                                <?php }?>
                                                                
                                                            ]
                                                                                                            
                                                        });


                                                    });
                                                </script>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                                       
        <?php require 'views/footer.php'; ?>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>
    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    

    <!-- Sweet alert -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/sweetalert/sweetalert.min.js"></script>

   <!-- Input Mask-->
   <script src="<?php echo constant ('URL');?>src/js/plugins/jasny/jasny-bootstrap.min.js"></script>

       <!-- Select2 -->
       <script src="<?php echo constant ('URL');?>src/js/plugins/select2/select2.full.min.js"></script>

    <!-- Data picker -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>

    <!-- Typehead -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>

                <script type="text/javascript">
                        
                    $(document).ready(function(){
                        var id=$("#tipo_experiencia").val();
                        
                        var tipo=$("#institucion_tipo").val();
                        
                        mostrar2(tipo);

                        mostrar(id);
                    
                        


                    });

                    //muestra un form u otro dependiendo del tipo de estudio
                    function mostrar(id) {
                        

                       
                        //3=Idioma
                        if (id == "3") {

                            //mostrar
                            $("#wcomision").show();
                            $("#file_idioma").addClass('required');
                            $("#nivel_escritura").addClass('required');
                            $("#nivel_comprende").addClass('required');
                            $("#nivel_habla").addClass('required');
                            $("#nivel_lectura").addClass('required');

                            //ocultar
                            $("#wadministrativo").hide();
                            $("#wdocencia").hide();
                            $("#descripcion_entidad").removeClass('required');
                            $("#unidad_c").removeClass('required');
                            $("#idioma").addClass('required');
                            $("#file_no_conducente").removeClass('required');
                            $("#titulo_trabajo").removeClass('required');
                            $("#resumen").removeClass('required');
                            $("#file_publico").removeClass('required');

                        }
                        //1=Conducente
                        if (id == "1") {
                            //console.log(id);

                            $("#institucion_tipo").change(function(){

                                var tipo_entidad=$('#institucion_tipo').val();
                                var tipo_experiencia=$('#tipo_experiencia').val();
                                //si el tipo de entidad es publica (=1) y el tipo de experiencia es trabajo administrativo (=1)
                                if(tipo_entidad==1){
                                    //mostramos el contenedor de constancia de trabajo
                                    $("#wconstancia-publico").show();
                                    $("#file_publico").addClass('required');
                                }else{
                                    $("#wconstancia-publico").hide();
                                    $("#file_publico").removeClass('required');
                                }

                            });


                            //mostrar
                            $("#wadministrativo").show();
                            $("#titulo_trabajo").addClass('required');
                            $("#resumen").addClass('required');
                           

                            //ocultar
                            $("#wcomision").hide();
                            $("#wdocencia").hide();
                            $("#file_idioma").removeClass('required');
                            $("#descripcion_entidad").addClass('required');
                            $("#unidad_c").removeClass('required');
                            $("#idioma").removeClass('required');
                            $("#nivel_escritura").removeClass('required');
                            $("#nivel_comprende").removeClass('required');
                            $("#nivel_habla").removeClass('required');
                            $("#nivel_lectura").removeClass('required');
                            $("#file_no_conducente").removeClass('required');
                            
                            

                        }
                    //2=noconducente
                        if (id == "2") {
                           
                            //mostrar
                            $("#wdocencia").show();
                            $("#file_no_conducente").addClass('required');
                            
                            //ocultar
                            $("#wcomision").hide();
                            $("#wadministrativo").hide();
                            $("#file_idioma").removeClass('required');
                            $("#descripcion_entidad").removeClass('required');
                            $("#unidad_c").addClass('required');
                            $("#idioma").removeClass('required');
                            $("#nivel_escritura").removeClass('required');
                            $("#nivel_comprende").removeClass('required');
                            $("#nivel_habla").removeClass('required');
                            $("#nivel_lectura").removeClass('required');
                            $("#titulo_trabajo").removeClass('required');
                            $("#resumen").removeClass('required');
                            $("#file_publico").removeClass('required');
                            
                            

                        }

                        if (id == "") {
                            $("#wcomision").hide();
                            $("#wadministrativo").hide();
                            $("#wdocencia").hide();

                            $("#descripcion_entidad").removeClass('required');
                            $("#unidad_c").removeClass('required');
                            $("#idioma").removeClass('required');
                            $("#nivel_escritura").removeClass('required');
                            $("#nivel_comprende").removeClass('required');
                            $("#nivel_habla").removeClass('required');
                            $("#nivel_lectura").removeClass('required');
                            $("#file_idioma").removeClass('required');
                            $("#titulo_trabajo").removeClass('required');
                            $("#resumen").removeClass('required');
                            $("#file_publico").removeClass('required');

                        }
                        
                    }

                    //muestra un form u otro dependiendo de la respuesta SI/NO en cada uno de los casos 
                    function mostrar2(id){


                        var tipo_entidad=$('#institucion_tipo').val();
                        var tipo_experiencia=$('#tipo_experiencia').val();
                        //si el tipo de entidad es publica (=1) y el tipo de experiencia es trabajo administrativo (=1)
                        if(id==1 && tipo_experiencia==1){
                            //mostramos el contenedor de constancia de trabajo
                            $("#institucion_tipo").prop('disabled', true);
                            console.log($("#institucion_tipo").prop('disabled', true));
                            $("#wconstancia-publico").show();
                            $("#file_publico").addClass('required');
                        }else{
                            $("#wconstancia-publico").hide();
                            $("#file_publico").removeClass('required');
                        }

                   


                    }


                   
                </script>
       

    <!-- validar extensiones y pesos -->
    <script>
        $(document).ready(function(){
            var extensionesValidas = ".pdf";

            var pesoPermitido = 10000;
            // Cuando cambie #fichero
            $("#file_publico").change(function () {
                $('#texto').text('');
                if(validarExtension(this)) { 
                    if(validarPeso(this)) { 
                    }
                }  
            });
            $("#file_no_conducente").change(function () {
                $('#texto').text('');
                if(validarExtension(this)) { 
                    if(validarPeso(this)) { 
                    }
                }  
            });
            $("#file_idioma").change(function () {
                $('#texto').text('');
                if(validarExtension(this)) { 
                    if(validarPeso(this)) { 
                    }
                }  
            });
            
            
            // Validacion de extensiones permitidas
            function validarExtension(datos) {
                var ruta = datos.value;
                var extension = ruta.substring(ruta.lastIndexOf('.') + 1).toLowerCase();
                var extensionValida = extensionesValidas.indexOf(extension);
                if(extensionValida < 0) {
                        $('#texto').text('La extensión no es válida Su Archivo tiene de extensión: .'+ extension);
                        //alert('La extensión no es válida Su Archivo tiene de extensión: .'+ extension);
                    swal("Ha ocurrido un Error", "La extensión no es válida Su Archivo tiene de extensión: ."+extension, "error");  
                    document.getElementById("file_publico").value = "";
                    document.getElementById("file_no_conducente").value = "";
                    document.getElementById("file_idioma").value = "";
                    
                    return false;
                    } else {
                        return true;
                }
            }
            // Validacion de peso del fichero en kbs
            function validarPeso(datos) {
                if (datos.files && datos.files[0]) {
                    var pesoFichero = datos.files[0].size/10000;
                    if(pesoFichero > pesoPermitido) {
                        $('#texto').text('El peso maximo permitido del Archivo es: ' + pesoPermitido + ' KBs Su fichero tiene: '+ pesoFichero +' KBs');
                        //alert('El peso maximo permitido del Archivo es: ' + pesoPermitido + ' KBs Su Archivo tiene: '+ pesoFichero +' KBs');
                        
                        swal("Ha ocurrido un Error","El peso maximo permitido del Archivo es: " + pesoPermitido + " KBs Su Archivo tiene: "+ pesoFichero +" KBs", "error");    
                        document.getElementById("file_publico").value = "";
                        document.getElementById("file_no_conducente").value = "";
                        document.getElementById("file_idioma").value = "";
                        
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        });
    </script>
    <!-- form step-->
    <script>



            

        $(document).ready(function(){

            
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                   
                   
                    ////////////////////////////////////////       
                  

                    ////////////////////////////////////////   


                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                                ano_r: {
                                    esfecha: true
                                }
                        }
                       
                    });
       });
       
       $.validator.addMethod("esfecha", esFechaActual, "El Año de Realización no debe ser mayor al Año Actual");


        function esFechaActual(value, element, param) {

            
            var fechaActual = <?php echo date('Y');?>;

            if (value > fechaActual) {

                return false; //error de validación

            }

            else {

                return true; //supera la validación

            }

        }

    </script>

   

</body>
</html>
