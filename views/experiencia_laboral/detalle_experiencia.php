
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SIDTA | Detalle Experiencia</title>


 
    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">


</head>

<body>
 
   <?php require 'views/header.php'; ?>
   

   <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-7">
                    <h2>Detalle Experiencia </h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>home">Inicio</a>
                        </li>
                        
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>experiencia_laboral">Experiencia Laboral</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="">Listar Experiencia Laboral</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <a href="<?php echo constant ('URL');?>experiencia_laboral/viewAdd"><strong>Detalle</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-5">
                    <div class="title-action">
                        <a href="<?php echo constant ('URL');?>experiencia_laboral" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Volver</a>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight ">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>SIDTA</h5>
                        <div class="ibox-tools">

                 

                        </div>
                    </div>
                    <div class="ibox-content ">

                        <!--  contenido -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="tabs-container">
                                    <div class="tabs-right">
                                        <ul class="nav nav-tabs">
                                            <li><a class="nav-link active" data-toggle="tab" href="#tab-8"> Datos de la Experiencia</a></li>
                                            <li><a class="nav-link" data-toggle="tab" href="#tab-9">Institución</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div id="tab-8" class="tab-pane active">
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <h2><strong>Tipo de Experiencia</strong></h2>
                                                            <p><?php echo $this->experiencia->tipo_experiencia; ?></p>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <h2><strong>Estado de Verificación</strong></h2>
                                                            <p><?php if($this->experiencia->estatus_verificacion=='Sin Verificar'){ echo '<span class="label label-danger" style="font-size: 12px;">'.ucwords($this->experiencia->estatus_verificacion).'</span>'; } else{ echo '<span class="label label-success" style="font-size: 12px;">'.ucwords($this->experiencia->estatus_verificacion).'</span>';} ?></p>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <?php 
                                                        switch($this->experiencia->id_tipo_experiencia){ 
                                                            case 1:
                                                    ?>
                                                    <div class="row">
                                                        <?php if($this->experiencia->id_institucion_tipo==1){?>
                                                            <div class="col-lg-6">
                                                                <h2><strong>Constancia de Trabajo</strong></h2>

                                                                <a target="_blank" href="<?php echo constant('URL') . "experiencia_laboral/viewDocumento/" . $this->id_trabajo_administrativo;?>" class="btn btn-primary"><i class="fa fa-external-link"></i> Constancia</a>                                                         
                                                                <a href="<?php echo constant('URL').'src/documentos/experiencia_laboral/';?>"  download="<?php echo $this->descripcion_documento;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>
                                                            </div>    
                                                        <?php }?>
                                                        
                                                        <div class="col-lg-6">
                                                            <h2><strong>Período</strong></h2>
                                                            <p><?php echo $this->experiencia->periodo;?></p>
                                                        </div>
                                                        
                                                    </div>
                                                            
                                                            

                                                        <?php 
                                                                break;  
                                                            case 2:
                                                        ?>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <h3><strong>Unidad Curricular</strong></h3>
                                                                    <p><?php echo $this->experiencia->materia; ?></p>
                                                                    
                                                                    <h3><strong>Nivel Académico</strong></h3>
                                                                    <p><?php echo $this->experiencia->nivel_academico; ?></p>
                                                                    
                                                                    <h3><strong>Modalidad de Estudio</strong></h3>
                                                                    <p><?php echo $this->experiencia->modalidad_estudio; ?></p>
                                                                    <h3><strong>Semestre</strong></h3>
                                                                    <p><?php echo $this->experiencia->semestre; ?></p>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <h3><strong>Horas Semanales</strong></h3>
                                                                    <p><?php echo $this->experiencia->horas_semanales; ?></p>
                                                                    <h3><strong>Nro. de Estudiantes Atendidos</strong></h3>
                                                                    <p><?php echo $this->experiencia->estudiantes_atendidos; ?></p>
                                                                    <h3><strong>Naturaleza</strong></h3>
                                                                    <p><?php echo $this->experiencia->naturaleza; ?></p>
                                                                    <h3><strong>Período Lectivo</strong></h3>
                                                                    <p>Período <?php echo $this->experiencia->periodo_lectivo; ?></p>
                                                                </div>
                                                            </div>
                                                                

                                                                
                                                        <?php 
                                                                break; 
                                                            case 3:
                                                        ?>
                                                                <h2><strong>Lugar</strong></h2>
                                                                <p><?php echo $this->experiencia->lugar; ?></p>

                                                    <?php 
                                                                break;
                                                        }
                                                    ?>
                                                    <br>
                                                    <hr>
                                                     <div class="row">
                                                        <div class="col-lg-6">
                                                        <h3><strong>Cargo Ejercido</strong></h3>
                                                            <p><?php echo $this->experiencia->cargo; ?></p>
                                                            <h3><strong>Año de Realización</strong></h3>
                                                            <p><?php echo $this->experiencia->ano_experiencia; ?></p>
                                                            
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <h3><strong>Fecha de Ingreso</strong></h3>
                                                            <p><?php echo $this->experiencia->fecha_ingreso; ?></p>
                                                            <h3><strong>Fecha de Egreso</strong></h3>
                                                            <p><?php echo $this->experiencia->fecha_egreso; ?></p>

                                                        </div>
                                                    </div>
                                                </div>



                                            </div>
                                            <div id="tab-9" class="tab-pane">
                                                <div class="panel-body">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <h3><strong>Nombre de la Institución</strong></h3>
                                                                <p><?php echo $this->experiencia->institucion; ?> </p>
                                                                <h3><strong>País de Realización</strong></h3>
                                                                <p><?php echo $this->experiencia->descripcion_pais; ?></p>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <h3><strong>Estado</strong></h3>
                                                                <p><?php echo $this->experiencia->descripcion_estado; ?></p>
                                                                <h3><strong>Tipo de Institución</strong></h3>
                                                                <p><?php echo $this->experiencia->institucion_tipo; ?></p>

                                                            </div>
                                                        </div>
                                                    

                                                </div>
                                            </div>
                                            <div id="tab-10" class="tab-pane">
                                                <div class="panel-body">
                                                    
                                                    
                                                    
                                                   

                                                    
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!--  end contenido -->

                    </div>
                </div>
            </div>
            </div>
        </div>



    <?php require 'views/footer.php'; ?>

     

    <!-- dataTables Scripts -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>

   

</body>
</html>






