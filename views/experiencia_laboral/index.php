<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SIDTA | Experiencia Laboral</title>


 
    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

    <!-- FooTable -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/footable/footable.core.css" rel="stylesheet">

</head>

<body>
 
   <?php require 'views/header.php'; ?>
   

        <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-6">
                    <h2>Experiencia Laboral</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>experiencia_laboral">Experiencia Laboral</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Listar Experiencia Laboral Docente</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-6">
                    <div class="title-action">
                        <a href="<?php echo constant ('URL');?>experiencia_laboral/viewAdd" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar</a>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>SAETA</h5>
                        <div class="ibox-tools">

                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row" style="margin-bottom: 0.5em;">
                            
                            <div class="col-lg-6">
                                <label for="">Filtrar <i class="fa fa-filter"></i></label>
                                <div class="form-group">
                        
                                    <select name="" id="estudiosi" onChange="mostrar(this.value);" >
                                        <option value="">Seleccione la categoría que desea listar</option>
                                        <option value="administrativo">Trabajo Aministrativo</option>
                                        <option value="docencia">Docencia Previa</option>
                                        <option value="comision">Comisión Gubernamental</option>
                                    </select>
                                </div>
                            
                            </div>
                            
                        </div>

                        <!--  contenido -->

                        <div id="respuesta"><?php echo $this->mensaje; ?></div>
                        
                        <input type="text" class="form-control form-control-sm m-b-xs" id="filter"
                                   placeholder="Buscar">
                            <!-- tabla admin -->

                            <table id="tadmin" class="table table-striped table-hover" data-page-size="8" data-filter=#filter>
                                <thead>
                                <tr>
                                    <th>Institucion</th>
                                    <th>Tipo Institución</th>
                                    <th>Año</th>
                                    <th>Cargo</th>
                                    <th>Período</th>
                                    <th>Estado de Verificacion</th>
                                    <th>Acción</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                            foreach($this->administrativos as $row){
                                                $administrativo= new Estructura();
                                                $administrativo=$row; ?>
                                    <tr class="gradeX">
                                        <td><?php echo $administrativo->institucion; ?></td>
                                        <td><?php echo $administrativo->institucion_tipo; ?> </td>
                                        <td><?php echo $administrativo->ano_experiencia; ?> </td>
                                        <td><?php echo $administrativo->cargo; ?> </td>
                                        <td><?php echo $administrativo->periodo; ?> </td>
                                        <td><?php if($administrativo->estatus_verificacion=='Sin Verificar'){ echo '<span class="label label-danger" style="font-size: 12px;">'.ucwords($administrativo->estatus_verificacion).'</span>'; } else{ echo '<span class="label label-success" style="font-size: 12px;">'.ucwords($administrativo->estatus_verificacion).'</span>';} ?></td>

                                        <td> 
                                            <a class="btn btn-outline btn-success" href="<?php echo constant ('URL') . "experiencia_laboral/viewEdit/" . $administrativo->id_trabajo_administrativo.','.$administrativo->id_tipo_experiencia;?>" role="button"><i class="fa fa-edit"></i> Editar</a>&nbsp;
                                            <!--como incluyo mas a class por las variables de estatus y la foranea-->
                                            <a class="btn btn-outline btn-info" href="<?php echo constant ('URL') . "experiencia_laboral/viewDetail/" . $administrativo->id_trabajo_administrativo.','.$administrativo->id_tipo_experiencia;?>" role="button"><i class="fa fa-eye"></i> Ver</a>

                                            <a class="btn btn-outline btn-danger" href="<?php echo constant('URL') . 'experiencia_laboral/delete/' . $administrativo->id_trabajo_administrativo.','.$administrativo->id_tipo_experiencia.",".$administrativo->id_institucion_tipo.",".$administrativo->id_experiencia_docente;?>" role="button"><i class="fa fa-trash"></i> Remover</a> &nbsp;
                                            <!-- <button class="btn btn-outline btn-danger bEliminar" data-id="<?php echo $administrativo->id_area_conocimiento_opsu;?>"><i class="fa fa-trash"></i> Eliminar</button> -->
                                        </td>
                                    </tr>
                                    <?php }?>
                                
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="6">
                                            <ul class="pagination float-right"></ul>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                            <!-- fin tabla admin -->
                            <!-- tabla docencia -->
                            <table  id="tdocencia" class="table table-stripped table-hover" data-page-size="8" data-filter=#filter>
                                <thead>
                                <tr>
                                    <th>Institucion</th>
                                    <th>Tipo Institución</th>
                                    <th>Año</th>
                                    <th>Cargo</th>
                                    <th>Unidad Curricular</th>
                                    <th>Estado de Verificacion</th>
                                    <th>Acción</th>
                                    
                                </tr>
                                </thead>
                                <tbody>
                                <?php 
                                            foreach($this->docencias as $row){
                                                $docencia= new Estructura();
                                                $docencia=$row; ?>
                                    <tr class="gradeX">
                                        <td><?php echo $docencia->institucion; ?></td>
                                        <td><?php echo $docencia->institucion_tipo; ?> </td>
                                        <td><?php echo $docencia->ano_experiencia; ?> </td>
                                        <td><?php echo $docencia->cargo; ?> </td>
                                        <td><?php echo $docencia->materia; ?> </td>
                                        <td><?php if($docencia->estatus_verificacion=='Sin Verificar'){ echo '<span class="label label-danger" style="font-size: 12px;">'.ucwords($docencia->estatus_verificacion).'</span>'; } else{ echo '<span class="label label-success" style="font-size: 12px;">'.ucwords($docencia->estatus_verificacion).'</span>';} ?></td>

                                        <td> 
                                            <a class="btn btn-outline btn-success" href="<?php echo constant ('URL') . "experiencia_laboral/viewEdit/" . $docencia->id_docencia_previa.','.$docencia->id_tipo_experiencia;?>" role="button"><i class="fa fa-edit"></i> Editar</a>&nbsp;
                                            <a class="btn btn-outline btn-info" href="<?php echo constant ('URL') . "experiencia_laboral/viewDetail/" . $docencia->id_docencia_previa.','.$docencia->id_tipo_experiencia;?>" role="button"><i class="fa fa-eye"></i> Ver</a>
                                            <a class="btn btn-outline btn-danger" href="<?php echo constant('URL') . 'experiencia_laboral/delete/' . $docencia->id_docencia_previa.','.$docencia->id_tipo_experiencia.",".$docencia->id_institucion_tipo.",".$docencia->id_experiencia_docente;?>" role="button"><i class="fa fa-trash"></i> Remover</a> &nbsp;
                                        </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <ul class="pagination float-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                            <!-- fin tabla docencia -->
                            <!-- tabla comision -->
                            <table  id="tcomision" class="table table-stripped  table-hover" data-page-size="8" data-filter=#filter>
                                <thead>
                                <tr>
                                    <th>Institucion</th>
                                    <th>Tipo Institución</th>
                                    <th>Año</th>
                                    <th>Cargo</th>
                                    <th>Lugar</th>
                                    <th>Estado de Verificacion</th>

                                    <th>Acción</th>                                    
                                </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                            foreach($this->comisiones as $row){
                                                $comision= new Estructura();
                                                $comision=$row; ?>
                                    <tr class="gradeX">
                                        <td><?php echo $comision->institucion; ?></td>
                                        <td><?php echo $comision->institucion_tipo; ?> </td>
                                        <td><?php echo $comision->ano_experiencia; ?> </td>
                                        <td><?php echo $comision->cargo; ?> </td>
                                        <td><?php echo $comision->lugar; ?> </td>
                                        <td><?php if($comision->estatus_verificacion=='Sin Verificar'){ echo '<span class="label label-danger" style="font-size: 12px;">'.ucwords($comision->estatus_verificacion).'</span>'; } else{ echo '<span class="label label-success" style="font-size: 12px;">'.ucwords($comision->estatus_verificacion).'</span>';} ?></td>

                                        <td> 
                                            <a class="btn btn-outline btn-success" href="<?php echo constant ('URL') . "experiencia_laboral/viewEdit/" . $comision->id_comision_gubernamental.','.$comision->id_tipo_experiencia;?>" role="button"><i class="fa fa-edit"></i> Editar</a>&nbsp;
                                            <!--como incluyo mas a class por las variables de estatus y la foranea-->
                                            <a class="btn btn-outline btn-info" href="<?php echo constant ('URL') . "experiencia_laboral/viewDetail/" . $comision->id_comision_gubernamental.','.$comision->id_tipo_experiencia;?>" role="button"><i class="fa fa-eye"></i> Ver</a>

                                            <a class="btn btn-outline btn-danger" href="<?php echo constant('URL') . 'experiencia_laboral/delete/' . $comision->id_comision_gubernamental.','.$comision->id_tipo_experiencia.",".$comision->id_institucion_tipo.",".$comision->id_experiencia_docente;?>" role="button"><i class="fa fa-trash"></i> Remover</a> &nbsp;
                                        </td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <ul class="pagination float-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                            <!-- fin tabla comision -->

                            <!-- tabla blanco -->

                            <table  id="tblanco" class="table table-stripped table-hover" data-page-size="8" data-filter=#filter>
                                <thead>
                                <tr>
                                    <th>N/A</th>
                                    <th>N/A</th>
                                    <th>N/A</th>
                                    
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="gradeX">
                                    <td colspan="3"><div class="text-center alert alert-info">Selecciona una opción para filtrar</div></td>
                                    
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <ul class="pagination float-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                            <!-- fin tabla blanco -->

                        <!--  end contenido -->

                    </div>
                </div>
            </div>
            </div>
        </div>



    <?php require 'views/footer.php'; ?>

     

    <!-- dataTables Scripts -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- FooTable -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/footable/footable.all.min.js"></script>

    <!-- Page-Level Scripts -->
    <script>
                $(document).ready(function() {
                    var id=$("#estudiosi").val();

                    mostrar(id);

                });
        function mostrar(id){

            if(id=="administrativo"){

                //mostrar
                $("#tadmin").show();
                $("#tadmin").addClass('footable');
                $('.footable').footable();


                //ocultar
                $("#tdocencia").hide();
                $("#tdocencia").removeClass('footable');
                $("#tcomision").hide();
                $("#tblanco").hide();
            }

            if(id=="docencia"){
                
                //mostrar
                $("#tdocencia").show();
                $("#tdocencia").addClass('footable');
                $('.footable').footable();


                //ocultar
                $("#tadmin").hide();
                $("#tadmin").removeClass('footable');
                $("#tcomision").hide();
                $("#tblanco").hide();


            }


            if(id=="comision"){

                //mostrar
                $("#tcomision").show();
                $("#tcomision").addClass('footable');
                $('.footable').footable();


                //ocultar
                $("#tadmin").hide();
                $("#tadmin").removeClass('footable');
                $("#tdocencia").hide();
                $("#tdocencia").removeClass('footable');
                $("#tblanco").hide();
            }


            if(id==""){
                $("#tadmin").hide();
                $("#tdocencia").hide();
                $("#tcomision").hide();
               
                $("#tblanco").show();
            }

        }

    </script>

   

</body>
</html>
