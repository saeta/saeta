
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Sedes | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/toastr/toastr.min.css" rel="stylesheet">

    <!-- Gritter -->
    <link href="<?php echo constant ('URL');?>src/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

  <!-- c3 Charts -->
    <link href="<?php echo constant ('URL');?>/src/css/plugins/c3/c3.min.css" rel="stylesheet">
<!-- Toastr style -->
<link href="<?php echo constant('URL'); ?>src/css/plugins/toastr/toastr.min.css" rel="stylesheet">

</head>

<div class="spiner-example">
                                <div class="sk-spinner sk-spinner-three-bounce">
                                    <div class="sk-bounce1"></div>
                                    <div class="sk-bounce2"></div>
                                    <div class="sk-bounce3"></div>
                               
                                </div>
                            </div>


<body>
    <?php require 'views/header.php'; ?>


    
    <div id="main">
        <h1 class="center">Reporte actual del censo de Docentes UBV</h1>
    </div>



      <div class="wrapper wrapper-content animated fadeInRight">
        
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                   


                    
                    
                        <div class="ibox-title">
                        <h5>SIDTA</h5>
                        </div>
                        <div class="ibox-content">
                        <div id="value"></div>
                        <div id="value2"></div>
                            <div>
                            <?php if($_SESSION['id_perfil']==11 
        || $_SESSION['id_perfil']==4
        || $_SESSION['id_perfil']==4
        || $_SESSION['id_perfil']==5
        || $_SESSION['id_perfil']==6
        || $_SESSION['id_perfil']==7
        || $_SESSION['id_perfil']==9){?>




                
        

        <?php } ?>

                        <div class="table-responsive">
                        <?php include_once 'models/persona.php';
                            foreach($this->sedes as $row){
                                $sede= new Persona();
                                $sede=$row;
                             } ?>
                        <b>Total de Censados por Centro de Estudios para : <?php echo $sede->nombre;?></b> <br>
                        <a style="margin-left:90%;" href="<?php echo constant('URL') . 'reporte/';?>" class="btn btn-outline btn-success" >
                    <i class="fa fa-around"></i> Volver </a><br> <br>
                    <table  class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                    <?php echo $this->mensaje; ?>

                        <th>Centro de Estudios</th> 
                        <th>Cant Censados</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php include_once 'models/persona.php';
                            foreach($this->sedes as $row){
                                $sede= new Persona();
                                $sede=$row;
                                ?>
                    <tr class="gradeX">

                    <td><?php echo $sede->centro_e;?></td>
                    <td><?php echo $sede->total;?></td>
                    <td>  <a href="<?php echo constant('URL') . 'reporte/verpersona/' .$sede->centro_e.','.$sede->nombre;?>" class="btn btn-outline btn-success" >
                    <i class="fa fa-eye"></i> Ver
                    </a></td>
                        </tr>
                    <?php     }?>
                        
                        </tbody>
                    
                        </table>

                   
                      </div>

                   

       


        

<!--////////////////////////////////-->













                            </div>
                        
                   
                        </div>
                   
               


                </div>
            </div>
            </div>
        </div>





        

 <?php require 'views/footer.php'; ?>
    

    <!-- Tinycon -->
    <script src="<?php echo constant('URL'); ?>src/js/plugins/tinycon/tinycon.min.js"></script>



 <!-- ChartJS-->
 <script src="<?php echo constant ('URL');?>src/js/plugins/chartJs/Chart.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/demo/chartjs-demo.js"></script>

    <!-- d3 and c3 charts -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/d3/d3.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/c3/c3.min.js"></script>






   <?php 
   /*$this->varconcurso=789;
   $this->varascensos=500;
   $this->preypostgrado=197;
   $this->contratadosyordinarios=300;*/
   if($_SESSION['id_perfil']==2 
        || $_SESSION['id_perfil']==3
        || $_SESSION['id_perfil']==4
        || $_SESSION['id_perfil']==5
        || $_SESSION['id_perfil']==6
        || $_SESSION['id_perfil']==7
        || $_SESSION['id_perfil']==9){
   ?>
   

   <script>
           $(document).ready(function () {
   c3.generate({
                bindto: '#pie',
                data:{
                  
                    columns: [
                  
                        ['Total de Pre y Post Grado: <?php echo $this->preypostgrado;?>', <?php echo $this->preypostgrado;?>],
                        ['Estudios Conducentes: <?php echo $this->conducentes;?>', <?php echo $this->conducentes;?>],
                        ['Estudios  No Conducentes: <?php echo $this->noconducentes;?>', <?php echo $this->noconducentes;?>],
                        ['Estudios de Idioma: <?php echo $this->idioma;?>', <?php echo $this->idioma;?>]
                    ],
                    colors:{
                        data1: '#1ab394',
                        data2: '#BABABA'

                    },
                    type : 'pie'
                }
            });

        });


/*
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels:['Docentes que han Concursado', 'Cantidad que han Ascendido', 'Total de Pre y Post Grado', 'Contratados y Ordinarios'],
       
        datasets: [{
            label: '# of Votes',
            data: [<?php echo $this->varconcurso;?>, <?php echo $this->varascensos;?>,<?php echo $this->preypostgrado;?>,<?php echo $this->contratadosyordinarios;?>],
            backgroundColor: [
                '#e54476',
                '#e53600',
                '#236ace',
                '#00939c'
            
            ],
            borderColor: [
                '#e54476',
                '#e53600',
                '#236ace',
                '#00939c'
          
            ],
            borderWidth: 1
        }]
    },
    options: {
  

        

            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        
    }
});
*/

        <?php }
        
        if($_SESSION['id_perfil']==7 
        || $_SESSION['id_perfil']==2){ ?>
    $(document).ready(function() {
            function changeNumber() {
                value = $.post("<?php echo constant('URL') . 'home/notify';?>", function(htmlexterno){
                    $('#value').html(htmlexterno);
                });
            }
            setInterval(changeNumber, 3000);
        });
        <?php }
         
         if($_SESSION['id_perfil']==3){ ?>//notificacion propuestas de jurado sin a director
        $(document).ready(function() {
            function changeNumber() {
                value = $.post("<?php echo constant('URL') . 'home/notifySolicitud';?>", function(htmlexterno){
                    $('#value').html(htmlexterno);
                });
            }
            setInterval(changeNumber, 3000);
        });

        $(document).ready(function() {
                    function changeNumber() {
                        value = $.post("<?php echo constant('URL') . 'home/notifyMemos/15';?>", function(htmlexterno){
                            $('#value2').html(htmlexterno);
                        });
                    }
                    setInterval(changeNumber, 3000);
                });

        <?php }
         if($_SESSION['id_perfil']==4 || $_SESSION['id_perfil']==5){ ?>
            $(document).ready(function() {
                function changeNumber() {
                    value =$.post("<?php echo constant('URL') . 'home/notifyMemos/10';?>", function(htmlexterno){
                        $('#value').html(htmlexterno);
                    });
                }
                setInterval(changeNumber, 3000);
            });
        <?php }
         if($_SESSION['id_perfil']==6){ ?>
            $(document).ready(function() {
                function changeNumber() {
                    value =$.post("<?php echo constant('URL') . 'home/notifyMemos/11';?>", function(htmlexterno){
                        $('#value').html(htmlexterno);
                    });
                }
                setInterval(changeNumber, 3000);
            });

            $(document).ready(function() {
                function changeNumber() {
                    value =$.post("<?php echo constant('URL') . 'home/notifyMemos/12';?>", function(htmlexterno){
                        $('#value2').html(htmlexterno);
                    });
                }
                setInterval(changeNumber, 3000);
            });

            $(document).ready(function() {
                function changeNumber() {
                    value =$.post("<?php echo constant('URL') . 'home/notifyMemos/14';?>", function(htmlexterno){
                        $('#value3').html(htmlexterno);
                    });
                }
                setInterval(changeNumber, 3000);
            });

            $(document).ready(function() {
                function changeNumber() {
                    value =$.post("<?php echo constant('URL') . 'home/notifyMemos/16';?>", function(htmlexterno){
                        $('#value4').html(htmlexterno);
                    });
                }
                setInterval(changeNumber, 3000);
            });
        <?php }?>
        
    </script>
    <!-- end mesajito de bienvenida -->




    <script>

$(document).ready(function () {
   $('.spiner-example').fadeOut('slow');
   $('body').css({'overflow':'visible'});
});
</script>


    <script>

$(document).ready(function () {

    $('body').scrollspy({
        target: '#navbar',
        offset: 80
    });

    // Page scrolling feature
    $('a.page-scroll').bind('click', function(event) {
        var link = $(this);
        $('html, body').stop().animate({
            scrollTop: $(link.attr('href')).offset().top - 50
        }, 500);
        event.preventDefault();
        $("#navbar").collapse('hide');
    });
});

var cbpAnimatedHeader = (function() {
    var docElem = document.documentElement,
            header = document.querySelector( '.navbar-default' ),
            didScroll = false,
            changeHeaderOn = 200;
    function init() {
        window.addEventListener( 'scroll', function( event ) {
            if( !didScroll ) {
                didScroll = true;
                setTimeout( scrollPage, 250 );
            }
        }, false );
    }
    function scrollPage() {
        var sy = scrollY();
        if ( sy >= changeHeaderOn ) {
            $(header).addClass('navbar-scroll')
        }
        else {
            $(header).removeClass('navbar-scroll')
        }
        didScroll = false;
    }
    function scrollY() {
        return window.pageYOffset || docElem.scrollTop;
    }
    init();

})();

// Activate WOW.js plugin for animation on scrol
new WOW().init();

</script>


</body>
</html>
