<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Discapacidad | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

 
<!-- menu active -->
<script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>

</head>

<body>
    <?php require 'views/header.php'; ?>
    



    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><i class="fa fa-wheelchair"></i> Discapacidad</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="">Persona UBV</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Discapacidad</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Editar Discapacidad</strong>
                        </li>
                    </ol>
                </div>  
                <div class="col-lg-2">
                
                    <div class="title-action">
                    <?php if($_SESSION['Agregar']==true){?>
                        <a class="btn btn-primary" href="#myModal1" role="button" data-toggle="modal" data-id="<?php echo $discapacidad->id_discapacidad;?>" data-nac="<?php echo $discapacidad->descripcion;?>">
                        <i class="fa fa-plus"></i> Agregar</a> &nbsp;
                    <?php } ?>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Listar Discapacidades</h5>
                       
                        <div class="ibox-tools">
            


                        </div> 
                    </div>
                    
                    <div class="ibox-content">
                    <?php echo $this->mensaje; ?>
                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                    <!--<th>nro</th>-->
                        <th><i class="fa fa-wheelchair"></i> Discapacidad</th>
                        <th><i class="fa fa-wheelchair"></i> Tipo de Discapacidad</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php include_once 'models/persona.php';
                            foreach($this->paises as $row){
                                $discapacidad= new Persona();
                                $discapacidad=$row;?>
                    <tr class="gradeX">
                    <!--<td><?php echo $discapacidad->id_discapacidad; ?></td>-->
                    <td><?php echo $discapacidad->descripcion; ?></td>
                    <!--<td><?php echo $discapacidad->id_tipo_discapacidad; ?></td>-->
                    <td><?php echo $discapacidad->descripcion_n; ?></td>

                    <td>  
                    <?php if($_SESSION['Editar']==true){?>
                    <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal" data-tp="<?php echo $discapacidad->id_discapacidad; ?>"    data-naci="<?php echo $discapacidad->descripcion;?>">
                    <i class="fa fa-edit"></i> Editar</a> &nbsp;
                    <?php } 
                     if($_SESSION['Eliminar']==true){?>
                    <a href="<?php echo constant('URL') . 'discapacidad/verdiscapacidad/' .$discapacidad->id_discapacidad;?>"class="btn btn-outline btn-danger" onclick="return confirm('¿Desea  Remover este Registro?');">
                    <i class="fa fa-trash"></i> Remover</a>
                     <?php } ?>
                     </td>
    
                    </tr>
                            <?php }?>

                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>

        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Agregar Discapacidad</h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> Son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                           

                            <form id="form" action="<?php echo constant('URL');?>discapacidad/registrardiscapacidad"  method="post" class="wizard-big" >
                                <h1>Descripcion</h1>
                                <fieldset style="height:100px;">
                                    <h2>Discapacidad Información</h2>
                                        <div class="row">
                                            <div class="col-lg-8">
                                            <div class="form-group">
                                                <label>Discapacidad <span style="color: red;">*</span></label>

                                                <input style="text-align:center;" placeholder="Ingrese el Nombre de la Discapacidad" onkeypress="return soloLetras(event)" maxlength="45" id="discapacidad" name="discapacidad" type="text" class="form-control required">
                                            <br>
                                               <!--  <input id="discapacidad" name="discapacidad" type="text" class="form-control required" disabled >


                                               aqui me tengo que traer el nombre del discapacidad 
                                                
                                                <input type="hidden" id="id_discapacidad" name="id_discapacidad" value="<?php echo $discapacidad->id_discapacidad;?>" class="form-control" >-->
                                               
                                                <div class="form-group">
                                            <label>Tipo de discapacidad <span style="color: red;">*</span></label>
                                            <select class="form-control  required m-b " name="id_discapacidad" id="id_discapacidad">
                                       

                                            <option selected="selected" value="">Seleccione una discapacidad</option> 
                                            <?php include_once 'models/persona.php';
                                          foreach($this->paises2 as $row){
                                          $country=new Persona();
                                            $country=$row;?> 
                                            <option value="<?php echo $country->id_tipo_discapacidad;?>"><?php echo $country->descripcion;?></option>
                                            <?php }?>
                                               

                                            </select>   
                                           
                                            </div>




                                            </div>
                                        
                                         
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div >
                                                    <i class="fa fa-wheelchair" style="font-size:180px;color: #e5e5e5 "></i>
                                                    
                                            </div>
                                        
                                    </div>

                                </fieldset>
                          
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
         

        
             
<div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Editar Discapacidad</h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> Son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                           

                            <form id="form2" action="<?php echo constant('URL');?>discapacidad/Actualizardiscapacidad"  method="post" class="wizard-big" >
                                <h1>Descripcion</h1>
                                <fieldset style="height:100px;">
                                    <h2>Discapacidad Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <div class="form-group">
                                                <label>Discapacidad <span style="color: red;">*</span></label>
                                                <input style="text-align:center;" placeholder="Ingrese el Nombre de la Discapacidad" onkeypress="return soloLetras(event)" maxlength="45" id="discapacidad2" name="discapacidad2" type="text" class="form-control required">
                                                </div>
   
                                            <br>
                                               <!--  <input id="discapacidad" name="discapacidad" type="text" class="form-control required" disabled >


                                               aqui me tengo que traer el nombre del discapacidad 
                                                
                                                <input type="hidden" id="id_discapacidad" name="id_discapacidad" value="<?php echo $discapacidad->id_discapacidad;?>" class="form-control" >-->
                                               
                                          
                                            <div class="form-group" >
                                            <input type="hidden" id="id_discapacidad2" name="id_discapacidad2" value="<?php echo $discapacidad->id_discapacidad;?>" class="form-control" >
                                            <label>Tipo de Discapacidad <span style="color: red;">*</span></label>
                                            <select disabled class="form-control required m-b " name="id_discapacidad2" id="id_discapacidad2" readonly="readonly">
                                       
                                            <option selected="selected" value="<?php echo $country->id_discapacidad;?> "><?php echo $discapacidad->descripcion_n;?></option> 
                                            <?php include_once 'models/persona.php';
                                          foreach($this->paises as $row){
                                          $country=new Persona();
                                            $country=$row;?> 
                                            <option value="<?php echo $country->id_discapacidad;?>"><?php echo $country->descripcion_n;?></option>
                                           
                                            <?php 
                                        
                                        
                                        }?>
                                            
                                            </select>   
                                            
                                            </div>
                                            </div>
                                          
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div >
                                                    <i class="fa fa-wheelchair" style="font-size:180px;color: #e5e5e5 "></i>
                                                    
                                            </div>
                                        
                                    </div>

                                </fieldset>
                          
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
         








    <?php require 'views/footer.php'; ?>


   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

<!-- menu active -->
<script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>


<!-- Solo Letras -->
<script src="<?php echo constant ('URL');?>src/js/val_letras.js"></script>

    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>
    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
$('#myModal1').on('show.bs.modal', function(e) {
    
  var product = $(e.relatedTarget).data('id');
  $("#id_discapacidad").val(product);

  var product2 = $(e.relatedTarget).data('nac');
  $("#discapacidad").val(product2);


});
</script>


<script>
$('#myModal2').on('show.bs.modal', function(e) {

  var product = $(e.relatedTarget).data('naci');
  $("#discapacidad2").val(product);

  var product2 = $(e.relatedTarget).data('tp');
  $("#id_discapacidad2").val(product2);
});
</script>

    <!-- Page-Level Scripts -->
    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>




</body>
</html>
