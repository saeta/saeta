<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Nacionalidad | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
 <!-- menu active -->
<script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>


</head>

<body>
    <?php require 'views/header.php'; ?>
    



    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-9">
                    <h2><i class="fa fa-globe"></i> Nacionalidad</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="">Persona UBV</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Nacionalidad</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Editar Nacionalidad</strong>
                        </li>
                    </ol>
                </div>  
                <div class="col-lg-3">
                    <div class="title-action">
                    <?php if($_SESSION['Agregar']==true){?>
                        <a class="btn btn-primary" href="#myModal1" role="button" data-toggle="modal" data-id="<?php echo $pais->id_pais;?>" data-nac="<?php echo $pais->descripcion;?>"> 
                            <i class="fa fa-plus"></i> Agregar
                        </a> &nbsp;
                    <?php } ?>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Nacionalidades registradas</h5>
                       
                        <div class="ibox-tools">
            


                        </div> 
                    </div>
                    
                    <div class="ibox-content">
                    <?php echo $this->mensaje; ?>
                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                   <!-- <th>nro</th>-->
                        <th>País</th>
                        <th>Nacionalidad</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php include_once 'models/estructura.php';
                            foreach($this->paises as $row){
                                $pais= new Estructura();
                                $pais=$row;?>
                    <tr class="gradeX">
                   <!-- <td><?php echo $pais->id_pais; ?></td>-->
                    <td><?php echo $pais->descripcion; ?></td>
                    
                    <td><?php echo $pais->descripcion_n; ?></td>

                    <td>  
                    <?php if($_SESSION['Editar']==true){?>
                    <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal" data-pais5="<?php echo $pais->id_pais; ?>"  data-naci="<?php echo $pais->descripcion_n;?>"> 
                        <i class="fa fa-edit"></i> Editar
                    </a> &nbsp;
                    <?php } ?>
                    
                     </td>
    
                    </tr>
                            <?php }?>

                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>

        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Agregar Nacionalidad (Gentilicio)</h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> Son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                           

                            <form id="form" action="<?php echo constant('URL');?>nacionalidad/registrarNacionalidad"  method="post" class="wizard-big" >
                                <h1>Descripcion</h1>
                                <fieldset style="height:100px;">
                                    <h2>Nacionalidad Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <label>Nacionalidad <i>(Gentilicio)</i><span style="color: red;">*</span></label>

                                                <input id="nacionalidad"  placeholder="Ingrese el Nombre de la Nacionalidad" style="text-align:center;"  maxlength="45" name="nacionalidad"  type="text" class="form-control required">
                                            <br>
                                               <!--  <input id="pais" name="pais" type="text" class="form-control required" disabled >


                                               aqui me tengo que traer el nombre del pais 
                                                
                                                <input type="hidden" id="id_pais" name="id_pais" value="<?php echo $pais->id_pais;?>" class="form-control" >-->
                                               
                                                <div class="form-group">
                                            <label>País <span style="color: red;">*</span></label>
                                            <select class="chosen-select form-control required m-b" name="id_pais" id="id_pais">
                                       

                                            <option selected="selected" value="">Seleccione una País</option> 
                                            <?php include_once 'models/estructura.php';
                                          foreach($this->paises2 as $row){
                                          $country=new Estructura();
                                            $country=$row;?> 
                                            <option value="<?php echo $country->id_pais;?>"><?php echo $country->descripcion;?></option>
                                            <?php }?>
                                               

                                            </select>   
                                            
                                            </div>
                                                



                                            </div>
                                        
                                         
                                        </div>
                                        
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-globe" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </fieldset>
                          
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
         

        
             
<div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Editar Nacionalidad <i>(Gentilicio)</i></h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> Son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                           

                            <form id="form2" action="<?php echo constant('URL');?>nacionalidad/ActualizarNacionalidad"  method="post" class="wizard-big" >
                                <h1>Descripcion</h1>
                                <fieldset style="height:100px;">
                                    <h2>Nacionalidad Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                
                                                <div class="form-group">
                                                <label>Nacionalidad <i>(Gentilicio)</i><span style="color: red;">*</span></label>
                                                <input id="nacionalidad2"  style="text-align:center;" placeholder="Ingrese el Nombre de la Nacionalidad"  maxlength="45" name="nacionalidad2" type="text" class="form-control required">
                                                </div>
   
                                            <br>
                                               <!--  <input id="pais" name="pais" type="text" class="form-control required" disabled >


                                               aqui me tengo que traer el nombre del pais 
                                                
                                                <input type="hidden" id="id_pais" name="id_pais" value="<?php echo $pais->id_pais;?>" class="form-control" >-->
                                               
                                          
                                            <div class="form-group">
                                            <label>País <span style="color: red;">*</span></label>
                                            <select class="chosen-select form-control required m-b" name="id_pais5" id="id_pais5" readonly="readonly">
                                       
                                            <option selected="selected" value=" ">Seleccione una País</option> 
                                            <?php include_once 'models/estructura.php';
                                          foreach($this->paises as $row){
                                          $country=new Estructura();
                                            $country=$row;?> 
                                            <option value="<?php echo $country->id_pais;?>"><?php echo $country->descripcion;?></option>
                                            <?php 
                                        
                                       
                                        }?>
                                            
                                            </select>   
                                            
                                            </div>
                                            </div>
                                          
                                        </div>
                                        
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-globe" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </fieldset>
                          
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
         








    <?php require 'views/footer.php'; ?>

   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

<!-- menu active -->
<script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
   <!-- Chosen Select -->
   <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>                   
   <!-- Solo Letras -->
<script src="<?php echo constant ('URL');?>src/js/val_letras.js"></script>




    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>
    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
$('#myModal1').on('show.bs.modal', function(e) {
    $('.chosen-select').chosen({width: "100%"});
   

});
</script>


<script>
$('#myModal2').on('show.bs.modal', function(e) {

  var product = $(e.relatedTarget).data('naci');
  $("#nacionalidad2").val(product);

  var product2 = $(e.relatedTarget).data('pais5');
  $("#id_pais5").val(product2);
  $('.chosen-select').chosen({width: "100%"});
  $('#id_pais5').trigger('chosen:updated');
});
</script>

   <!-- Page-Level Scripts -->
   <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>




</body>
</html>
