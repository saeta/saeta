<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Género | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

 <!-- menu active -->
<script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>


</head>

<body>
    <?php require 'views/header.php'; ?>
    



    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Género</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="">Persona UBV</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Género</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Editar Género</strong>
                        </li>
                    </ol>
                </div>  
                <div class="col-lg-2">
                    <div class="title-action">
                    <?php if($_SESSION['Agregar']==true){?>
                        <a class="btn btn-primary" href="#myModal1" role="button" data-toggle="modal" data-id="<?php echo $generos->codigo;?>" data-cod="<?php echo $generos->id_genero;?>" data-tel="<?php echo $generos->descripcion;?>">
                        <i class="fa fa-plus"></i> Agregar</a> &nbsp;
                    <?php } ?>
                    
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Listar Géneros</h5>
                       
                        <div class="ibox-tools">
            
                       

                        </div> 
                    </div>
                    
                    <div class="ibox-content">
                    <?php echo $this->mensaje; ?>
                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                   <!-- <th>Nro</th>-->
                  <th>Tipo de Género</th>
                  <th>Código <i>(Acrónimo)</i></th>
                  <th>Opciones</th>
                        
                    </thead>
                    <tbody>
                 
                    <?php include_once 'models/persona.php';
                            foreach($this->generos as $row){
                                $genero= new Persona();
                                $genero=$row;?>
                    <tr class="gradeX">

                    
                  <!--  <td><?php echo $genero->id_genero; ?></td>-->
                    <td><?php echo $genero->descripcion; ?></td>
                    <td><?php echo $genero->codigo;?></td>
                    
                    <td>  
                    <?php if($_SESSION['Editar']==true){?>
                    <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal" data-cod2="<?php echo $genero->codigo;?>" data-id="<?php echo $genero->id_genero;?>"  data-tel2="<?php echo $genero->descripcion;?>"> 
                    <i class="fa fa-edit"></i> Editar</a> &nbsp;
                    <?php } 
                     if($_SESSION['Eliminar']==true){?>
                    <a href="<?php echo constant('URL') . 'genero/vergenero/' .$genero->id_genero;?>"class="btn btn-outline btn-danger" onclick="return confirm('¿Desea  Remover este Registro?');">
                    <i class="fa fa-trash"></i> Remover</a>
                     <?php } ?>
                     </td>
    
                    </tr>
                        

                    <?php }?>
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>

        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Agregar Género</h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> Son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                           

                            <form id="form" action="<?php echo constant('URL');?>genero/registrargenero"  method="post" class="wizard-big" >
                                <h1>Descripcion</h1>
                                <fieldset style="height:100px;">
                                    <h2>Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <label>Género <span style="color: red;">*</span></label>

                                                <input style="text-align:center;" placeholder="Ingrese el Nombre del Género"  onkeypress="return soloLetras(event)" maxlength="45" id="genero" name="genero" type="text" class="form-control required">
                                            <br>
                                            <label>Código <i>(Acrónimo)</i> del Género<span style="color: red;">*</span></label>
                                            
                                            <input style="text-align:center;" placeholder="Ingrese el Código del Género" onkeypress="return soloLetras(event)" maxlength="2"id="codigo" name="codigo" type="text" class="form-control required">
                                            </div>
                                        
                                         
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                 
                                                    <i class="fa fa-users" style="font-size:180px;color: #e5e5e5;margin-top:5%; "></i>
                                                </div>
                                            </div>
                                        
                                    </div>

                                </fieldset>
                          
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
         

        
             
<div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Editar Género</h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> Son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                           

                            <form id="form2" action="<?php echo constant('URL');?>genero/Actualizargenero"  method="post" class="wizard-big" >
                                <h1>Descripcion</h1>
                                <fieldset style="height:100px;">
                                    <h2>Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                
                                                <div class="form-group">
                                                <label>Género <span style="color: red;">*</span></label>
                                                <input style="text-align:center;" placeholder="Ingrese el Nombre del Género" onkeypress="return soloLetras(event)" maxlength="45"  id="genero2" name="genero2" type="text" class="form-control required">
                                                </div>
                                                <br>
                                                <label>Código <i>(Acrónimo)</i> del Género <span style="color: red;">*</span></label>
                                                <input style="text-align:center;" placeholder="Ingrese el Código del Género" onkeypress="return soloLetras(event)" maxlength="2" id="codigo2" name="codigo2" type="text" class="form-control required">

                                                <div class="form-group">
                                                <label><span style="color: red;"></span></label>
                                                <input id="id_genero2" name="id_genero2" type="hidden" class="form-control required">
                                                </div>
   
                                            <br>
                                            
                                               
                                          
                                        
                                            </div>
                                          
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                
                                                    <i class="fa fa-users" style="font-size:180px;color: #e5e5e5;margin-top:5%; "></i>
                                                </div>
                                            </div>
                                        
                                    </div>

                                </fieldset>
                          
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
         








    <?php require 'views/footer.php'; ?>

   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>
<!-- menu active -->
<script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>

<!-- Solo Letras -->
<script src="<?php echo constant ('URL');?>src/js/val_letras.js"></script>
    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>
    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
$('#myModal1').on('show.bs.modal', function(e) {
    
  var product = $(e.relatedTarget).data('id');
  $("#id_genero").val(product);

  var product2 = $(e.relatedTarget).data('tel');
  $("#genero").val(product2);

  var product3 = $(e.relatedTarget).data('tel');
  $("#codigo").val(product3);

});
</script>


<script>
$('#myModal2').on('show.bs.modal', function(e) {

  var product = $(e.relatedTarget).data('id');
  $("#id_genero2").val(product);

  var product2 = $(e.relatedTarget).data('tel2');
  $("#genero2").val(product2);

  var product3 = $(e.relatedTarget).data('cod2');
  $("#codigo2").val(product3);
});
</script>

    <!-- Page-Level Scripts -->
   <!-- Page-Level Scripts -->
   <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>




</body>
</html>
