<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin | Usuarios</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">


</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                <span style="color: #273479"><h2><b><?php echo $this->informacion->perfil; ?></b></h2></span>
                    
                </div>
                <div class="col-lg-2">
                <div class="title-action">
        <a href="<?php echo constant('URL') . 'usuarios/verUsuario/' .$_SESSION['id_persona'];?>" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i> Editar</a>
    </div>
                </div>
            </div>
<!-- ////////////////////////////-->





<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox selected">
                <div class="ibox-content">
                    <div class="tab-content">
                        <div id="contact-1" class="tab-pane active">
                            <div class="row m-b-lg">
                            <div class="col-sm-4"></div>
                                <div class="widget blue-bg p-lg col-lg-4 text-center" >
                                    <h2><?php echo $this->informacion->primer_nombre." ".$this->informacion->segundo_nombre; ?></h2>
                                    <div class="m-b-sm">
                                        <!-- <img alt="image" class="rounded-circle" src="img/a2.jpg"
                                        style="width: 62px">-->
                                        <br>
                                            <p class="text-center"><a href=""><i class="fa fa-user-circle big-icon"></i></a></p>
                                            <h2><?php echo $this->informacion->primer_apellido." ".$this->informacion->segundo_apellido;?></h2>
                                    </div>
                                </div>
                                <div class="col-sm-4"></div>
                            </div>
                            <div class="row m-b-lg">


                                    <div class="col-lg-6">
                                    <div class="client-detail" >
                                    <div class="full-height-scroll" >

                                        <strong>Datos Generales</strong>
                                        <div id="vertical-timeline" class="vertical-container dark-timeline">
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon navy-bg">
                                                    <i class="fa fa-header"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                <h4>Nombres :</h4>
                                                    <span style="color: #23a7c8">
                                                    <?php echo $this->informacion->primer_nombre; ?>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $this->informacion->segundo_nombre; ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon blue-bg">
                                                    <i class="fa fa-briefcase"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                    <h4>Apellidos :</h4>
                                                    <span style="color: #23a7c8" > 
                                                    <?php echo $this->informacion->primer_apellido; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <?php echo $this->informacion->segundo_apellido; ?>  </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon lazur-bg">
                                                    <i class="fa fa-language"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                <h4>Pais de Nacimiento :</h4>

                                                    <span style="color: #23a7c8">
                                                    <?php echo $this->informacion_n->pais; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                                    </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon navy-bg">
                                                    <i class="fa fa-vcard-o"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                <h4>Nacionalidad :</h4>

                                                    <span style="color: #23a7c8">
                                                    <?php echo $this->informacion->nacionalidad; ?></span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon blue-bg">
                                                    <i class="fa fa-clipboard"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                    <h4>Numero de Documento :</h4>

                                                    <span style="color: #23a7c8">
                                                    <?php echo $this->informacion->identificacion; ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon lazur-bg">
                                                    <i class="fa fa-book"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                    <h4>Fecha Nacimiento :</h4>

                                                    <span style="color: #23a7c8">
                                                    <?php echo $this->informacion->fecha_nacimiento; ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block" style="display: none;">
                                                <div class="vertical-timeline-icon lazur-bg">
                                                    <i class="fa fa-trophy"></i>
                                                </div>
                                                <?php if($this->informacion->etnia==NULL){?>
                                                <div class="vertical-timeline-content" >
                                                    <h4>Etnia :</h4>

                                                    <span style="color: #23a7c8">
                                                    <?php echo $this->informacion->etnia; ?>
                                                    </span>
                                                </div>
                                                    <?php } else{?>
                                                <div class="vertical-timeline-content">
                                                    <h4>Etnia :</h4>

                                                    <span style="color: #23a7c8">
                                                    <?php echo $this->informacion->etnia; ?>
                                                    </span>
                                                </div>
                                                    <?php }?>
                                                </div>

                                          
                                        <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon navy-bg">
                                                    <i class="fa fa-paint-brush"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                    <h4>Genero :</h4>

                                                    <span style="color: #23a7c8">
                                                    <?php echo $this->informacion->genero; ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon blue-bg">
                                                    <i class="fa fa-file-text"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                <h4>Estado Civil :</h4>

                                                    <span style="color: #23a7c8">
                                                    <?php echo $this->informacion->estado_civil; ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon lazur-bg">
                                                    <i class="fa fa-certificate"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                    <h4>Numero de Hijos :</h4>

                                                    <span style="color: #23a7c8">
                                                    <?php echo $this->informacion_n->numero_hijos; ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon navy-bg">
                                                    <i class="fa fa-university"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                    <h4>Telefono :</h4>

                                                    <span style="color: #23a7c8">
                                                    <?php echo $this->informacion->telefono_coodigo_area; ?>&nbsp;-&nbsp;<?php echo $this->informacion->telefono; ?>

                                                    </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon blue-bg">
                                                    <i class="fa fa-graduation-cap"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                    <h4>Correo :</h4>

                                                    <span style="color: #23a7c8">
                                                    <?php echo $this->informacion->correo; ?>
                                                    </span>
                                                </div>
                                            </div>
                                                
                                                <div class="vertical-timeline-block" style="display: none;">
                                                <div class="vertical-timeline-icon lazur-bg">
                                                    <i class="fa fa-trophy"></i>
                                                </div>
                                                <?php if($this->informacion_d->id_tipo_discapacidad==NULL){?>
                                                <div class="vertical-timeline-content">
                                                    <h4>Discapacidad :</h4>

                                                    <span style="color: #23a7c8">
                                                    <?php echo $this->informacion_d->id_tipo_discapacidad; ?>
                                                    </span>
                                                </div>
                                                    <?php } else{?>
                                                <div class="vertical-timeline-content">
                                                    <h4>Discapacidad :</h4>

                                                    <span style="color: #23a7c8">
                                                    <?php echo $this->informacion_d->id_tipo_discapacidad; ?>
                                                    </span>
                                                </div>
                                                    <?php }?>
                                                </div>
                                                <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon navy-bg">
                                                    <i class="fa fa-university"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                    <h4>Dirección :</h4>

                                                    <span style="color: #23a7c8">
                                                    <?php echo $this->informacion->domicilio_detalle; ?>

                                                    </span>
                                                </div>
                                            </div>

                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon lazur-bg">
                                                    <i class="fa fa-certificate"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                    <h4>Fecha Ingreso UBV:</h4>

                                                    <span style="color: #23a7c8">
                                                    <?php echo $this->informacion_n->fecha_ingreso; ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon navy-bg">
                                                    <i class="fa fa-university"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                    <h4>Clasificación :</h4>

                                                    <span style="color: #23a7c8">
                                                    <?php echo $this->informacion_n->clasificacion_docente; ?>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon blue-bg">
                                                    <i class="fa fa-graduation-cap"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                    <h4>Cargo :</h4>

                                                    <span style="color: #23a7c8">
                                                    <?php echo $this->informacion_n->cargo; ?>
                                                    </span>
                                                </div>
                                            </div>
                                                
                                                
                                                <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon navy-bg">
                                                    <i class="fa fa-university"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                    <h4>Dedicación :</h4>

                                                    <span style="color: #23a7c8">
                                                    <?php echo $this->informacion_n->dedicacion; ?>

                                                    </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon navy-bg">
                                                    <i class="fa fa-university"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                    <h4>Estatus :</h4>

                                                    <span style="color: #23a7c8">
                                                    <?php echo $this->informacion_n->estatus; ?>

                                                    </span>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                    <div class="col-lg-6">
                                    <div class="client-detail" >
                                    <div class="full-height-scroll" >

                                        <strong>Desempeño Docente UBV</strong>
                                        <div id="vertical-timeline" class="vertical-container dark-timeline">
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon navy-bg">
                                                    <i class="fa fa-header"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                
                                                <a href="<?php echo constant ('URL');?>diseno_uc"><span style="color: #23a7c8"><h4>Diseño Unidad Curricular Docente</h4></a>
                                                </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon blue-bg">
                                                    <i class="fa fa-briefcase"></i>
                                                </div>
                                                <div class="vertical-timeline-content"><a href="<?php echo constant ('URL');?>tutoria"><span style="color: #23a7c8"><h4>Tutoria</h4></a>
                                                    
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon lazur-bg">
                                                    <i class="fa fa-language"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                <a href="<?php echo constant ('URL');?>docencia_previa"><span style="color: #23a7c8"><h4>Docencia Previa UBV</h4></a>
                                                   </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon navy-bg">
                                                    <i class="fa fa-vcard-o"></i>
                                                </div>
                                                <div class="vertical-timeline-content">
                                                <a href="<?php echo constant ('URL');?>comision_excedencia"><span style="color: #23a7c8"><h4>Comisión Excedencia</h4></a>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon blue-bg">
                                                    <i class="fa fa-clipboard"></i>
                                                </div>
                                                <div class="vertical-timeline-content"><a href="<?php echo constant ('URL');?>articulo_revista"><span style="color: #23a7c8"><h4>Registrar Artículo de Revista</h4></a>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon lazur-bg">
                                                    <i class="fa fa-book"></i>
                                                </div>
                                                <div class="vertical-timeline-content"><a href="<?php echo constant ('URL');?>registro_libro"><span style="color: #23a7c8"><h4>Registrar libro</h4></a>
                                                  </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon navy-bg">
                                                    <i class="fa fa-paint-brush"></i>
                                                </div>
                                                <div class="vertical-timeline-content"><a href="<?php echo constant ('URL');?>arte_software"><span style="color: #23a7c8"><h4>Registrar Arte o Software</h4></a>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon blue-bg">
                                                    <i class="fa fa-file-text"></i>
                                                </div>
                                                <div class="vertical-timeline-content"><a href="<?php echo constant ('URL');?>registro_ponencia"><span style="color: #23a7c8"><h4>Registrar Ponencia</h4></a>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon lazur-bg">
                                                    <i class="fa fa-certificate"></i>
                                                </div>
                                                <div class="vertical-timeline-content"><a href="<?php echo constant ('URL');?>registro_reconocimiento"><span style="color: #23a7c8"><h4>Reconocimientos</h4></a>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon navy-bg">
                                                    <i class="fa fa-university"></i>
                                                </div>
                                                <div class="vertical-timeline-content"><a href="<?php echo constant ('URL');?>experiencia_laboral"><span style="color: #23a7c8"><h4>Experiencia Laboral</h4></a>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon blue-bg">
                                                    <i class="fa fa-graduation-cap"></i>
                                                </div>
                                                <div class="vertical-timeline-content"><a href="<?php echo constant ('URL');?>datos_academicos"><span style="color: #23a7c8"><h4>Datos Académicos e Idiomas</h4></a>
                                                    </span>
                                                </div>
                                            </div>
                                                <div class="vertical-timeline-block">
                                                <div class="vertical-timeline-icon lazur-bg">
                                                    <i class="fa fa-trophy"></i>
                                                </div>
                                                <div class="vertical-timeline-content"><a href="<?php echo constant ('URL');?>concurso"><span style="color: #23a7c8"><h4>Concursos</h4></a>
                                                    </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ///////////////////////////-->



       


<!--////////////////////////////////-->
  

    <?php require 'views/footer.php'; ?>


 
    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

<!-- menu active -->
<script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>


  <!-- iCheck -->
  <script src="<?php echo constant ('URL');?>src/js/plugins/iCheck/icheck.min.js"></script>
       









</body>
</html>
