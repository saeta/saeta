<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Detalle Usuario | SIDTA</title>


 
    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">


</head>
<style>
td:hover{
    background-color: #eed92eba;
    color: #111;
    cursor: default;
}
tr:hover{
    background-color: #cac0c08c;;
    color: #111;

}
</style>
<body>
 
   <?php require 'views/header.php'; ?>
   

   <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-5">
                    <h2>Mi Perfil de Usuario</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <a href="<?php echo constant ('URL') . "perfil/render/". $_SESSION['id_persona'];?>"><strong>Mi Perfil de Usuario</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-7">
                    <div class="title-action">
                        <a href="<?php echo constant ('URL') . "perfil/viewEdit/" . $_SESSION['id_persona'];?>" class="btn btn-primary"><i class="fa fa-edit"></i> Editar Perfil</a>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row m-b-lg m-t-lg">
        
                <div class="col-md-6">

                    <div class="profile-image">
                        <i style="font-size: 100px;" class="fa fa-user-circle-o"></i>
                    </div>
                    <div class="profile-info">
                        <div class="">
                            <div>
                                <h2 class="no-margins">
                                    <small>Usuario: </small> <?php echo "ubv".$this->informacion->identificacion;?>
                                </h2>
                                <h3><small>Perfil: </small> <?php echo $this->informacion->perfil;?></h3>
                                <p>
                                    <?php echo substr($this->informacion->documento_identidad, 0, 1) . "-".$this->informacion->identificacion . ", " . 
                                    $this->informacion->primer_nombre . " " . $this->informacion->primer_apellido. ". Reside en: " . $this->domicilio_p->municipio. ", ". $this->domicilio_p->parroquia. "."; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    
                    <table style="font-size: 16px;" class="table small m-b-xs">
                        <tbody>
                        <tr>
                            <td>
                                <strong>Roles de Usuario</strong>
                            </td>
                        </tr>
                        <?php 
                            $i=1;
                            while($i<=count($this->informacion_u)){
                        ?>
                        <tr>
                            <td>
                                <?php echo $this->rol[$i]['rol'.$i]; ?>
                            </td>
                            
                        </tr>
                                <?php 
                                $i++;
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-3">
                    <h4>Estatus del Usuario</h4>
                    <h3 class="no-margins"><?php if($this->informacion->usuario_estatus==0){ echo '<span class="label label-danger"> Inactivo </span>';} ?>
                    <?php if($this->informacion->usuario_estatus==1){ echo '<span class="label label-primary"> Activo </span>';} ?></h3>
                    <div id="sparkline1"></div>
                </div>
            </div>
            <div class="row ">
                
                <div class="col-lg-12 ">
                <?php echo $this->mensaje; ?>
                    <div  class="panel panel-primary">
                        <div style="color: black;" class="panel-heading">
                        <h3 class="text-center">Datos Personales<br><label style="margin-top: 4px;"><small >Documento de Identidad</small></label><br>
                            <i class="fa fa-address-card"></i> 
                                Identificación: <?php echo $this->informacion->identificacion;?> <br>Tipo de Documento de Identidad: <?php echo $this->informacion->documento_identidad; ?>
                        </h3>
                        </div>
                        <div class="panel-body row">
                        <div class="col-lg-6">
                            <table class="table table-bordered" style="width: 100%;">
                                    <tr>
                                        <td style="font-weight: 400;">Nombres:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->primer_nombre . " " . $this->informacion->segundo_nombre; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Apellidos:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->primer_apellido . " " . $this->informacion->segundo_apellido; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Estado Civil:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->estado_civil; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Género:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->genero; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Nacionalidad:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->nacionalidad; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Fecha de Nacimiento:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo date("d/m/Y", strtotime($this->informacion->fecha_nacimiento)); ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">País de Nacimiento:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->pais; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Número de Hijos:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->nro_hijos; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Fecha de Ingreso a la UBV:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo date("d/m/Y", strtotime($this->informacion->fecha_ingreso)); ?></strong></td>
                                    </tr>
                                </table>
                            </div>
                        <div class="col-lg-6">
                            <table class="table table-bordered" style="width: 100%;">
                                <tr>
                                    <td colspan="2" style="text-align: center; color: #000;"><b>Datos de Contacto</b></td>
                                </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Tipo de Teléfono:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->telefono_tipo; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Teléfono:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo "(".$this->informacion->telefono_coodigo_area . ") " . $this->informacion->telefono; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Correo Personal:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->correo_personal["correo"]; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Correo Institucional:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->correo_institucional["correo"]; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; color: #000;" colspan="2"><b>Domicilio</b></td>
                                        
                                    </tr>
                                     <tr>
                                        <td style="font-weight: 400;">Tipo de Domicilio:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->domicilio_p->tipo_domicilio; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;" colspan="2">Dirección:</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: justify; font-size: 12px;" colspan="2"><strong><?php echo $this->domicilio_p->pais. ", ".$this->domicilio_p->estado. ", ". $this->domicilio_p->municipio. ", ". $this->domicilio_p->parroquia. ", ".$this->domicilio_p->domicilio_detalle. "."; ?></strong></td>
                                    </tr>
                                </table>
                            </div>
                            <?php if(!empty($this->informacion->id_etnia)){?>
                                <div class="col-lg-3"></div>
                            <div class="col-lg-6">
                                <table class="table table-bordered" style="width: 100%;">
                                    <tr>
                                        <td style="font-weight: 400;">Etnia Indígena:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->etnia; ?></strong></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-lg-3"></div>

                            <?php } ?>
                        </div>
                    </div>
                   
                </div>
                <?php if(!empty($this->disc[1]['id_discapacidad1'])){?>
                <div class="col-lg-12">
                    <div  class="panel panel-info">
                        <div style="color: black;" class="panel-heading">
                        <h3 class="text-center">Datos de Discapacidad<br>
                            <label style="margin-top: 3px;"><b>Código CONAPDIS: <?php echo $this->disc[1]['codigo_conapdis1']; ?></b></label>
                        </h3>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered" style="width: 100%;">
                                <tr>
                                    <td style="font-weight: 400;">Tipo de Discapacidad:</td>
                                    <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->disc[1]['tipo_discapacidad1']; ?></strong></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: 400;">Discapacidad:</td>
                                    <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->disc[1]['discapacidad1']; ?></strong></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: 400;" colspan="2">Observación:</td>
                                </tr>
                                <tr>
                                    <td style="text-align: justify; font-size: 14px;" colspan="2"><strong><?php echo $this->disc[1]['observacion1']; ?></strong></td>
                                </tr>
                                <?php if(!empty($this->disc[2]['id_discapacidad2'])){?>

                                <tr>
                                    <td style="text-align: center; font-weight: 400;" colspan="2">---- ---- ---- ----</td>
                                </tr>
                                <tr>
                                    <td style="font-weight: 400;">Tipo de Discapacidad:</td>
                                    <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->disc[2]['tipo_discapacidad2']; ?></strong></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: 400;">Discapacidad:</td>
                                    <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->disc[2]['discapacidad2']; ?></strong></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: 400;" colspan="2">Observación:</td>
                                </tr>
                                <tr>
                                    <td style="text-align: justify; font-size: 14px;" colspan="2"><strong><?php echo $this->disc[2]['observacion2']; ?></strong></td>
                                </tr>
                                <?php } ?>
                            </table>     
                        </div>
                    </div>
                </div>
                <?php } ?>
                <?php if($this->informacion->id_perfil==1){//si es un docente?>
                
                <div class="col-lg-12">
                    <div  class="panel panel-success">
                        <div style="color: black;" class="panel-heading">
                        <h3 class="text-center">Datos del(la) Trabajador(a) Académico(a)
                        </h3>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered" style="width: 100%;">
                                <tr>
                                    <td style="font-weight: 400;">Programas de Formación:</td>
                                    <td style="text-align: right; font-size: 16px;">
                                        <table style="font-size: 16px;" class="table small m-b-xs">
                                            <tbody>
                                            <?php $j=1;
                                                while($j<=count($this->programas_docente)){ ?>
                                            <tr>
                                                <td>
                                                <?php echo $this->doc_prog[$j]['programa'.$j]; ?>
                                                </td>
                                            </tr>
                                            <?php 
                                                $j++;
                                            } ?>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: 400;">Centro de Estudio <i>(CE)</i>:</td>
                                    <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->centro_estudio; ?></strong></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: 400;">Eje Regional <i>(ER)</i>:</td>
                                    <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->eje_regional; ?></strong></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: 400;">Dedicación:</td>
                                    <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->docente_dedicacion; ?></strong></td>
                                </tr> 
                                <tr>
                                    <td style="font-weight: 400;">Estatus del Docente:</td>
                                    <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->docente_estatus; ?></strong></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: 400;">Clasificación:</td>
                                    <td style="text-align: right; font-size: 16px;"><strong><?php switch($this->informacion->clasificacion_docente){
                                                                    case 'DOCENTE': 
                                                                        echo $this->informacion->clasificacion_docente . " ORDINARIO";
                                                                        break;
                                                                    case 'JUBILADOS':
                                                                    echo "DOCENTE " . str_replace('S', '', $this->informacion->clasificacion_docente);
                                                                    break;
                                                                    case 'PENSIONADOS':
                                                                    echo "DOCENTE " . substr($this->informacion->clasificacion_docente, 0, 10);
                                                                    break;
                                                                    default:
                                                                    echo "DOCENTE " .$this->informacion->clasificacion_docente;
                                                                    break;
                                                                } ?></strong></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: 400;">Escalafón:</td>
                                    <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->escalafon; ?></strong></td>
                                </tr>
                               
                                
                                
                            </table>     
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        
        </div>



    <?php require 'views/footer.php'; ?>

     

    <!-- dataTables Scripts -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>

   

</body>
</html>










