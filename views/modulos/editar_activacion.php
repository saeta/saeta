<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SIDTA | Editar Experiencia</title>


    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">
  
    
    <!-- jasny input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <!--  style select2 -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/select2/select2.min.css" rel="stylesheet">
    <!-- datapicker -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <!-- sweetalert input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

</head>

<body>
 
    <div id="wrapper">
   <?php require 'views/header.php'; ?>
   

        <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-9">
                    <h2>Agregar Experiencia </h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>home">Inicio</a>
                        </li>
                        
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>experiencia_laboral">Experiencia Laboral</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="">Listar Experiencia Laboral</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <a href="<?php echo constant ('URL');?>experiencia_laboral/viewAdd"><strong>Editar</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-3">
                    <div class="title-action">
                        <a href="<?php echo constant ('URL');?>activacion/viewAscenso" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Volver</a>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5><i class="fa fa-angle-double-right"></i> SIDTA <i class="fa fa-angle-double-left"></i></h5>
                        </div>
                        <div class="ibox-content">
                            <h2>
                                Experiencia Laboral
                            </h2>
                            <p>
                                Modifica el dato que desees de una Experiencia Laboral.
                            </p>
                            <?php echo $this->mensaje;?>
                            <div class="alert alert-info">Todos los campos marcados con un (<span style="color: red;">*</span>) Son Obligatorios</div>

                           <form id="form" action="<?php echo constant('URL') . "activacion/actualizarAscenso/" . $this->id_activacion;?>"  method="post" class="wizard-big">
                                <h1>Control de tiempo</h1>
                                <fieldset>
                                    <h2> Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                                    <input type="hidden" name="id_activacion" id="id_activacion" value="<?php echo $this->id_activacion;?>">
                                                    <div class="form-group">
                                                    <label>Modulo <span style="color: red;">*</span></label>
                                                        <select class="form-control required m-b " name="tipo_activacion2" id="tipo_activacion2">                                      
                                                            <option value="">Selecione el modulo que desea activar</option>
                                                            <?php 
                                                                foreach($this->tipos_activacion as $row){
                                                                $tipo_activacion2=new Estructura();
                                                                $tipo_activacion2=$row;?> 
                                                            <option value="<?php echo $tipo_activacion2->id_tipo_activacion;?>" <?php if($this->activacion->id_tipo_activacion==$tipo_activacion2->id_tipo_activacion){ print 'selected=selected';}?>><?php echo $tipo_activacion2->descripcion;?></option>
                                                            <?php }?>     
                                                        </select>                                              
                                                    </div>
                                                    <div class="form-group">
                                                    <label>Período Lectivo <span style="color: red;">*</span></label>
                                                   
                                                    <select class="form-control required m-b "  name="periodo2" id="periodo2">                                      
                                                        <option value="" selected="selected">Selecione el período que desea activar</option>
                                                        <?php $periodos=array('I', 'II');
                                                        $i=0;
                                                        while($i<2){
                                                        ?>
                                                            <option value="<?php echo $periodos[$i];?>" <?php if($this->activacion->periodo_lectivo==$periodos[$i]){ print 'selected=selected';}?>>Período <?php echo $periodos[$i];?></option>
                                                        <?php $i++;
                                                        }?>  
                                                    </select>                                              
                                                    </div>
                                            <div class="form-group">
                                                <label>Motivo de Activación<span style="color: red;">*</span></label>
                                                <textarea id="descripcion2" name="descripcion2" type="text" class="form-control required" placeholder="Escriba una dirección..." aria-required="true" aria-invalid="false" onkeypress="return soloLetras(event)" maxlength="70" minlength="4"><?php echo $this->activacion->activacion;?></textarea>
                                            </div>
                                            <div class="form-group" id="data_6">
                                                <label class="font-normal">Fecha de Inicio <span style="color: red;">*</span></label>
                                                <label class="font-normal" style=" position: relative;left: 110px;">Fecha de Fin <span style="color: red;">*</span></label>
                                                <div class="input-daterange input-group" id="datepicker">
                                                    <input type="text" class="form-control-sm form-control required" name="starte" id="starte" placeholder="Ingrese la Fecha de Inicio" value="<?php echo $this->activacion->fecha_inicio;?>">
                                                    <span class="input-group-addon">&nbsp;Hasta&nbsp;</span>
                                                    <input type="text" class="form-control-sm form-control required" name="ende" placeholder="Ingrese la Fecha de Fin" id="ende" value="<?php echo $this->activacion->fecha_fin;?>" >
                                                </div>
                                                <span><i style="margin-right: 11.5em;">mm/dd/aaaa</i><i>mm/dd/aaaa</i></span>
                                            </div>
                                             </div>
                                             <script>
                                                                 $(document).ready(function(){

                                                                    $('#data_6 .input-daterange').datepicker({
                                                                    keyboardNavigation: false,
                                                                    forceParse: false,
                                                                    autoclose: true
                                                                    
                                                                });
                                                                 });
                                             </script>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-clock-o" style="font-size: 180px; color: #406bbd"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </fieldset>
                                    
                       
                            </form>

                        </div>
                    </div>
                    </div>
                </div>
                </div>
                                       
        <?php require 'views/footer.php'; ?>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>
    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    

    <!-- Sweet alert -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/sweetalert/sweetalert.min.js"></script>

   <!-- Input Mask-->
   <script src="<?php echo constant ('URL');?>src/js/plugins/jasny/jasny-bootstrap.min.js"></script>

       <!-- Select2 -->
       <script src="<?php echo constant ('URL');?>src/js/plugins/select2/select2.full.min.js"></script>

    <!-- Data picker -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>

    <!-- Typehead -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>

                <script type="text/javascript">
                        
                    $(document).ready(function(){
                        var id=$("#tipo_experiencia").val();
                        
                        var tipo=$("#institucion_tipo").val();
                        
                        mostrar2(tipo);

                        mostrar(id);
                    
                        $('#data_5 .input-daterange').datepicker({
                            keyboardNavigation: false,
                            forceParse: false,
                            autoclose: true
                            
                        });


                    });

                    
                   
                </script>
       

    <!-- validar extensiones y pesos -->
    <script>
                   

        $(document).ready(function(){

            
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                   
                   
                    ////////////////////////////////////////       
                  

                    ////////////////////////////////////////   


                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                                ano_r: {
                                    esfecha: true
                                }
                        }
                       
                    });
       });
       
       $.validator.addMethod("esfecha", esFechaActual, "El Año de Realización no debe ser mayor al Año Actual");


        function esFechaActual(value, element, param) {

            
            var fechaActual = <?php echo date('Y');?>;

            if (value > fechaActual) {

                return false; //error de validación

            }

            else {

                return true; //supera la validación

            }

        }

    </script>

   

</body>
</html>
