<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin | Periodo Académico </title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">



    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/cropper/cropper.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">






</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Periodo Académico </h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>periodo">Activación de Modulos</a>
                        </li>
                        <li class="breadcrumb-item active">
                        <strong> Periodo Académico </strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Listado de modulos  </h5>
                       
                        <div class="ibox-tools">
                        <!-- boton agregar-->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                        Activar Periodo Académico 
                                </button> 


                        </div>
                    </div>
                  
                    <div class="ibox-content">
                    <?php echo $this->mensaje; ?>
                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Periodo Académico</th>    
                        <th>Estatus </th>
                        <th>Calendario Académico</th>                              
                        <th>Opciones</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php include_once 'models/estructura.php';
                            foreach($this->periodoacademico as $row){
                                $periodo= new Estructura();
                                $periodo=$row;?>
                    <tr class="gradeX">
                    <td><?php echo $periodo->descripcion; ?></td>
                    <td>
                    <?php if($periodo->estatus==0){
                      echo"<center style=margin-top:3%;><span class='label label-danger' style='font-size: 12px;'>Inactivo</span></center>";
                        
                    }else{
                        echo"<center style=margin-top:3%;><span class='label label-primary' style='font-size: 12px;'>&nbsp; Activo &nbsp;</span></center>";
                    } ?>
                
                   </td>
                    <td> <i class="fa fa-calendar"></i> <?php echo date("d/m/Y", strtotime($periodo->calendario_academico)); ?></td>
                    <td> 
                      <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal" data-id="<?php echo $periodo->id_periodo_academico;?>" data-calendario="<?php echo  date("m/d/Y", strtotime($periodo->calendario_academico));?>" data-periodo="<?php echo $periodo->descripcion;?>"  data-estatus="<?php echo $periodo->estatus;?>"> Editar</a> &nbsp;
                  </td>
                    </tr>
                            <?php }?>
                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>

      


<!-- ///////////////Modal Agregar////////////////// -->
        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Activar Periodo Académico</h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            <h2>
                            Periodo Académico
                            </h2>
                            <p>
                            Activar el modulo para el proceso de inscripción de Nuevos Ingresos o carga de notas.
                            </p>

                            <form id="form" action="<?php echo constant('URL');?>periodo/ActivarPeriodoAcademico"  method="post" class="wizard-big">
                                <h1>Control de tiempo</h1>
                                <fieldset>
                                    <h2> Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">

                                        <div class="form-group">
                                        <label>Periodo Académico<span style="color: red;">*</span></label>
                                        <input id="periodo" name="periodo" type="text"  class="form-control required " maxlength='45' minlength="4" >
                                        </div>

                                           
                                          
                        
                            <div class="form-group" id="data_1">
                                <label class="font-normal">Calendario Académico</label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" class="form-control" name="calendario" id="calendario" value="<?php echo  date('m/d/Y');?>">
                                  
                                </div>
                               <center><span class="form-text">mm/dd/yyyy</span></center>
                            </div>
                                            
                                          
                            </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-calendar" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                 
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
  
<!-- ///////////////Modal editar////////////////// -->
 <div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                        <h4 class="modal-title">Editar Periodo Académico</h4>
                                        <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                    </div>
                                    <div class="modal-body" >
                                        
                                    <div class="ibox-content">
                        <h2>
                        Periodo Académico
                        </h2>
                        <p>
                        Activar el modulo para el proceso de inscripción de Nuevos Ingresos o carga de notas.
                        </p>

                        <form id="form2" action="<?php echo constant('URL');?>periodo/ActualizarPeriodoAcademico"  method="post" class="wizard-big">
                            <h1>Control de tiempo</h1>
                            <fieldset>
                                <h2> Información</h2>
                                <div class="row">
                                    <div class="col-lg-8">
                                    <input id="id_periodo_academico" name="id_periodo_academico" type="hidden"  class="form-control"  value="" >
                                  
                                    <div class="form-group">
                                    <label>Periodo Académico<span style="color: red;">*</span></label>
                                    <input id="periodoo" name="periodoo" type="text"  class="form-control required " maxlength='45' minlength="4" value="" >
                                    </div>

                                       
                                      
                    
                        <div class="form-group" id="data_1">
                            <label class="font-normal">Calendario Académico</label>
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" class="form-control" name="calendarioa" id="calendarioa" value="">
                              
                            </div>
                           <center><span class="form-text">mm/dd/yyyy</span></center>
                        </div>
                        <div class="form-group">
                                            <label>Estatus <span style="color: red;">*</span></label>
                                            <select class="form-control required m-b " name="estatusa" id="estatusa" >
                                                <option value ="1">Activo</option>
                                                <option value="0">Inactivo</option>
                                               
                                            </select>                                            
                                            </div>
                                        
                                      
                        </div>
                                    <div class="col-lg-4">
                                        <div class="text-center">
                                            <div style="margin-top: 20px">
                                                <i class="fa fa-calendar" style="font-size: 180px;color: #e5e5e5 "></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                             
                        </form>
                    </div>
                                    
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                              </div>
                          </div>
                     </div>
                 </div>       

<!--////////////////////////////////-->


  
    <?php require 'views/footer.php'; ?>


   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
   <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>
    <!-- Chosen Select -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script> 
        <!-- Solo Letras -->
    <script src="<?php echo constant ('URL');?>src/js/val_letras.js"></script>
        <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>

    


<!--comenzar desde aqui -->


<!-- Data picker -->
<script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>


 <!-- Image cropper -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/cropper/cropper.min.js"></script>

 
 <!-- Tags Input -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

<!-- <script>
     $(document).ready(function(){

         $('.tagsinput').tagsinput({
             tagClass: 'label label-primary'
         });

         $('#data_5 .input-daterange').datepicker({
             keyboardNavigation: false,
             forceParse: false,
             autoclose: true
         });
      
         
        });

 </script>-->


<script>
  $('#myModal1').on('shown.bs.modal', function () {  
             

         var mem = $('#data_1 .input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true
            });


       });


  $('#myModal2').on('show.bs.modal', function(e) {
 
    
  var product = $(e.relatedTarget).data('id');
  //$("#id_modulo_activacion").val(product);
  $("#id_periodo_academico").attr('value', product);


  var product2 = $(e.relatedTarget).data('periodo');
  //$("#moduloe").val(product2);
  $("#periodoo").attr('value', product2);

  var product3 = $(e.relatedTarget).data('calendario');
  $("#calendarioa").attr('value', product3);
  //$("#motivoe").attr('value', product3);


  var product4 = $(e.relatedTarget).data('estatus');
   //$("#estatusa").attr('value', product4);
   $('#estatusa > option[value='+product4+']').attr('selected', 'selected');


   
  var mem = $('#data_1 .input-group.date').datepicker({
    todayBtn: "linked",
    keyboardNavigation: false,
    forceParse: false,
    calendarWeeks: true,
    autoclose: true
});



});

</script>



    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>

















</body>
</html>
