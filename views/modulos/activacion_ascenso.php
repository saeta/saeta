<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Activar Ascensos | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">



    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/cropper/cropper.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">






</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Activar Modulo de Ascensos </h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href=""> Activación de Modulos</a>
                        </li>
                        <li class="breadcrumb-item active">
                        <strong>Activar Modulo de Ascensos </strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                   
                    <!-- boton agregar-->
                    <div class="title-action">
                        <?php if($_SESSION['Agregar']==true){?>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                            <i class="fa fa-plus"></i> Activar 
                        </button> 
                        <?php } ?>
                    </div>

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Listado de modulos  </h5>
                       
                        <div class="ibox-tools">
                        

                        </div>
                    </div>
                  
                    <div class="ibox-content">
                    <?php echo $this->mensaje; ?>
                    
                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Modulo</th>    
                        <th>Motivo de Activación</th>
                        <th>Período Lectivo</th>

                        <th>Fecha de inicio</th>
                        <th>Fecha de fin</th>
                        <th>Estatus</th>
                                       
                       <th>Opciones</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php $fecha_actual=date('Y-m-d'); 
                    
                            foreach($this->ascensos as $row){
                                $ascensos= new Estructura();
                                $ascensos=$row;?>
                    <tr class="gradeX">
                    <td><?php echo $ascensos->tipo_activacion; ?></td>
                    <td><?php echo substr($ascensos->motivo_activacion, 0, 40) . "..."; ?> </td>
                    <td><?php echo 'Período '.$ascensos->periodo_lectivo; ?></td>

                    <td><?php echo date("d/m/Y", strtotime($ascensos->fecha_inicio));  ?></td>
                    <td><?php echo date("d/m/Y", strtotime($ascensos->fecha_fin)); ?></td>
                    <td>
                        <?php 
                                if($fecha_actual>=date('Y-m-d', strtotime($ascensos->fecha_inicio)) 
                                    && $fecha_actual<=date('Y-m-d', strtotime($ascensos->fecha_fin))){
                                        echo '<span class="label label-primary" style=" font-size: 12px;"> Activo</span>';
                                } else{
                                    echo '<span class="label label-danger" style="font-size: 12px;"> Inactivo</span>';
                                }
                        ?>
                    </td>
                
                    
                    <td> 
                    

                    <!-- <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal" 
                        data-id_tipo_activacion="<?php echo $ascensos->id_tipo_activacion;?>" 
                        data-tipo_activacion="<?php echo $ascensos->tipo_activacion;?>" 
                        data-periodo="<?php echo $ascensos->periodo_lectivo;?>"  
                        data-finicio="<?php echo date("m/d/Y", strtotime($ascensos->fecha_inicio));?>" 
                        data-fin="<?php echo date("m/d/Y", strtotime($ascensos->fecha_fin));?>" 
                        data-motivo="<?php echo $ascensos->motivo_activacion;?>" 
                        data-id_activacion="<?php echo $ascensos->id_activacion;?>"><i class="fa fa-edit"></i> Editar</a>-->
                        <?php if($_SESSION['Editar']==true){?>
                        <a class="btn btn-outline btn-success" href="<?php echo constant ('URL') . "activacion/viewEdit/" . $ascensos->id_activacion;?>" role="button"><i class="fa fa-edit"></i> Editar</a>
                         &nbsp;
                        <?php } 
                        
                        if($_SESSION['Consultar']==true){?>

                        <a class="btn btn-outline btn-info" href="#myModal3" role="button" data-toggle="modal"  
                        data-id_activacion="<?php echo $ascensos->id_activacion;?>" 
                        data-id_tipo_activacion="<?php echo $ascensos->id_tipo_activacion;?>" 
                        data-motivo_activacion="<?php echo $ascensos->motivo_activacion;?>" 
                        data-tipo_activacion2="<?php echo $ascensos->tipo_activacion;?>" 
                        data-inicio="<?php echo date("d/m/Y", strtotime($ascensos->fecha_inicio));?>" 
                        data-fin="<?php echo date("d/m/Y", strtotime($ascensos->fecha_fin));?>" 
                        data-periodo2="<?php echo $ascensos->periodo_lectivo;?>"
                        data-estatus="<?php if(!($fecha_actual < $_SESSION['fecha_inicio'] 
                            || $fecha_actual > $_SESSION['fecha_fin'])){ 
                                echo 1;
                            } else{
                                    echo 0;}?>">
                        <i class="fa fa-search"></i> Ver</a> 

                            <?php } ?>
                                           
                     
                       
                       
                     
                       
                  </td>
                    </tr>
                            <?php }?>
                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>

      


<!-- ///////////////Modal Agregar////////////////// -->
        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Activar modulo</h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            <h2>
                            Activar Modulo
                            </h2>
                            <p>
                            Activar el modulo para el proceso de Ascenso de los Trabajadores Académicos.
                            </p>
                            <div class="alert alert-info">Todos los campos marcados con un (<span style="color: red;">*</span>) Son Obligatorios</div>

                            <form id="form" action="<?php echo constant('URL');?>activacion/ActivarAscenso"  method="post" class="wizard-big">
                                <h1>Control de tiempo</h1>
                                <fieldset>
                                    <h2> Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">

                                           
                                                    <div class="form-group">
                                                    <label>Modulo <span style="color: red;">*</span></label>
                                                        <select class="form-control required m-b " name="tipo_activacion" id="tipo_activacion">                                      
                                                            <option value="" selected="selected">Selecione el modulo que desea activar</option>
                                                            <?php 
                                                                foreach($this->tipos_activacion as $row){
                                                                $tipo_activacion=new Estructura();
                                                                $tipo_activacion=$row;?> 
                                                            <option value="<?php echo $tipo_activacion->id_tipo_activacion;?>"><?php echo $tipo_activacion->descripcion;?></option>
                                                            <?php }?>     
                                                        </select>                                              
                                                    </div>
                                                    <div class="form-group">
                                                    <label>Período Lectivo <span style="color: red;">*</span></label>
                                                    <select class="form-control required m-b "  name="periodo" id="periodo">                                      
                                                        <option value="" selected="selected">Selecione el período que desea activar</option>
                                                        <option value="I" >Período I</option>
                                                        <option value="II" >Período II</option>
                                                    </select>                                              
                                                    </div>
                                            <div class="form-group">
                                                <label>Motivo de Activación<span style="color: red;">*</span></label>
                                                <textarea id="descripcion" name="descripcion" type="text" class="form-control required" placeholder="Escriba una dirección..." aria-required="true" aria-invalid="false" onkeypress="return soloLetras(event)" maxlength="70" minlength="4"></textarea>
                                            </div>
                                            <div class="form-group" id="data_5">
                                                <label class="font-normal">Fecha de Inicio <span style="color: red;">*</span></label>
                                                <label class="font-normal" style=" position: relative;left: 110px;">Fecha de Fin <span style="color: red;">*</span></label>
                                                <div class="input-daterange input-group" id="datepicker">
                                                
                                                    <input type="text" class="form-control-sm form-control required" name="start" placeholder="<?php echo date('d/m/Y');?>">
                                                    <span class="input-group-addon">&nbsp;Hasta&nbsp;</span>    
                                                    <input type="text" class="form-control-sm form-control required" name="end" placeholder="<?php echo date('d/m/Y');?>">
                                                </div>
                                                <span><i style="margin-right: 11.5em;">mm/dd/aaaa</i><i>mm/dd/aaaa</i></span>
                                            </div>        
                                             </div>
                                             
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-clock-o" style="font-size: 180px; color: #406bbd"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </fieldset>
                                    
                       
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
  
<!-- ///////////////Modal editar////////////////// -->
<div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
            <h4 class="modal-title">Editar modulo</h4>
            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
        </div>
        <div class="modal-body" >
            
        <div class="ibox-content">
<h2>
Activar Modulo
</h2>
<p>
Activar el modulo para el proceso de inscripción de Nuevos Ingresos o carga de notas.
</p>

                            <form id="form2" action="<?php echo constant('URL');?>activacion/actualizarAscenso"  method="post" class="wizard-big">
                                <h1>Control de tiempo</h1>
                                <fieldset>
                                    <h2> Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">

                                                                    <input type="hidden" name="id_activacion" id="id_activacion">
                                                    <div class="form-group">
                                                    <label>Modulo <span style="color: red;">*</span></label>
                                                        <select class="form-control required m-b " name="tipo_activacion2" id="tipo_activacion2">                                      
                                                            <option value="">Selecione el modulo que desea activar</option>
                                                            <?php 
                                                                foreach($this->tipos_activacion as $row){
                                                                $tipo_activacion2=new Estructura();
                                                                $tipo_activacion2=$row;?> 
                                                            <option value="<?php echo $tipo_activacion2->id_tipo_activacion;?>"><?php echo $tipo_activacion2->descripcion;?></option>
                                                            <?php }?>     
                                                        </select>                                              
                                                    </div>
                                                    <div class="form-group">
                                                    <label>Período Lectivo <span style="color: red;">*</span></label>
                                                    <select class="form-control required m-b "  name="periodo2" id="periodo2">                                      
                                                        <option value="" selected="selected">Selecione el periodo que desea activar</option>
                                                        <option value="<?php echo date('Y') . " - I";?>" ><?php echo date('Y') . " - I";?></option>
                                                        <option value="<?php echo date('Y') . " - II";?>" ><?php echo date('Y') . " - II";?></option>
                                                    </select>                                              
                                                    </div>
                                            <div class="form-group">
                                                <label>Motivo de Activación<span style="color: red;">*</span></label>
                                                <textarea id="descripcion2" name="descripcion2" type="text" class="form-control required" placeholder="Escriba una dirección..." aria-required="true" aria-invalid="false" onkeypress="return soloLetras(event)" maxlength="70" minlength="4"></textarea>
                                            </div>
                                            <div class="form-group" id="data_6">
                                                <label class="font-normal">Fecha de Inicio <span style="color: red;">*</span></label>
                                                <label class="font-normal" style=" position: relative;left: 110px;">Fecha de Fin <span style="color: red;">*</span></label>
                                                <div class="input-daterange input-group" id="datepicker">
                                                    <input type="text" class="form-control-sm form-control required" name="starte" id="starte" value="">
                                                    <span class="input-group-addon">&nbsp;Hasta&nbsp;</span>
                                                    <input type="text" class="form-control-sm form-control required" name="ende"  id="ende" value="" >
                                                </div>
                                                <span><i style="margin-right: 11.5em;">mm/dd/aaaa</i><i>mm/dd/aaaa</i></span>
                                            </div>
                                             </div>
                                             
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-clock-o" style="font-size: 180px; color: #406bbd"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </fieldset>
                                    
                       
                            </form>
</div>
        
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
  </div>
</div>
</div>
</div>        

<!--////////////////////////////////-->


 
<!-- ///////////////Modal Mostrar////////////////// -->
<div class="modal inmodal fade " style="width: 100%;" id="myModal3" tabindex="-1" role="dialog"  aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content panel panel-success">
        <div class="modal-header panel-heading">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
            <h4 class="modal-title" > Detalle del Módulo de Ascensos</h4>
            <small class="font-bold"><h4 class="modal-title" id="detalle">  </h4></small>
        
         </div>
        <div class="modal-body panel-body" >
            
        <div class="ibox-content">

                          
                                
                                <!--<tr>
                                    <td>
                                        <img class="img-fluid" src="<?php echo constant ('URL');?>src/img/landing/header_one.jpg">
                                    </td>
                                </tr>-->
                    <div class="row jumbotron">
                    <div class="col-sm-6 b-r">
                    
                    
                        <h3 class="m-t-none m-b">Motivo de Activación</h3>
                        <p  id="motivo_activacion3"></p>
                        <h3 class="m-t-none m-b">Modulo / Tipo de Activación</h3>
                        <p  id="tipo_activacion3"></p>
                        <h3 class="m-t-none m-b">Fecha de Inicio / Fecha Fin</h3>
                       
                        <div class="input-daterange input-group" >
                        <i class="fa fa-calendar"></i>&nbsp; <p id="finicio"></p>
                        <span >&nbsp;&nbsp;</span>
                        <i class="fa fa-calendar">&nbsp;</i><p id="ffin"></p>   

                   
                    </div>

                    <h3 class="m-t-none m-b">Período Lectivo</h3>
                    <p>Período <strong  id="periodo3"></strong>
                    </p>
                        <h3 class="m-t-none m-b">Estatus</h3>
                        <span  id="estatus2"></span>

                        

                </div>
<div class="col-sm-6">
<div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-clock-o" style="font-size: 180px; color: #406bbd"></i>
                                                </div>
                                            </div>
</div>

                 




        </div>
        
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
  </div>
</div>
</div>

</div>
</div>        





<!--////////////////////////////////-->     


  
    <?php require 'views/footer.php'; ?>

        
   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
   <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>
    <!-- Chosen Select -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script> 
        <!-- Solo Letras -->
    <script src="<?php echo constant ('URL');?>src/js/val_letras.js"></script>
        <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>

    


<!--comenzar desde aqui -->


<!-- Data picker -->
<script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>


 <!-- Image cropper -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/cropper/cropper.min.js"></script>

 
 <!-- Tags Input -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

<!-- <script>
     $(document).ready(function(){

         $('.tagsinput').tagsinput({
             tagClass: 'label label-primary'
         });

         $('#data_5 .input-daterange').datepicker({
             keyboardNavigation: false,
             forceParse: false,
             autoclose: true
         });
      
         
        });

 </script>-->


<script>
  $('#myModal1').on('shown.bs.modal', function () {  
       $('.chosen-select').chosen({width: "100%"});


       $('.tagsinput').tagsinput({
             tagClass: 'label label-primary'
         });

         $('#data_5 .input-daterange').datepicker({
             keyboardNavigation: false,
             forceParse: false,
             autoclose: true
             
         });


       });


  $('#myModal2').on('show.bs.modal', function(e) {
    
  var product = $(e.relatedTarget).data('id_tipo_activacion');
  //$("#id_modulo_activacion").val(product);
  //alert(product);
  $("#tipo_activacion2").val(product);

  
  var product2 = $(e.relatedTarget).data('id_activacion');
  //$("#id_modulo_activacion").val(product);
  $("#id_activacion").attr('value', product2);


  var product3 = $(e.relatedTarget).data('motivo');
  $("#descripcion2").text(product3);
  //$("#motivoe").attr('value', product3);


  var product4 = $(e.relatedTarget).data('finicio');
   $("#starte").attr('value', product4);

   var product5 = $(e.relatedTarget).data('fin');
   $("#ende").attr('value', product5);


  var product6 = $(e.relatedTarget).data('periodo');
  //$("#aldeae").val(product6);
  $("#periodo2").val(product6);


  

  $('.chosen-select').chosen({width: "100%"});


  $('.tagsinput').tagsinput({
             tagClass: 'label label-primary'
         });

         $('#data_6 .input-daterange').datepicker({
             keyboardNavigation: false,
             forceParse: false,
             autoclose: true
         });



});

</script>


<script>
    $('#myModal3').on('show.bs.modal', function(e) {
    
        var pro = $(e.relatedTarget).data('id_');
       $("#detalle").html(pro);

       var pro1 = $(e.relatedTarget).data('motivo_activacion');
       $("#motivo_activacion3").text(pro1);


       var pro2 = $(e.relatedTarget).data('tipo_activacion2');
       $("#tipo_activacion3").html(pro2);
   
       var pro3 = $(e.relatedTarget).data('inicio');
       $("#finicio").html(pro3);

       var pro4 = $(e.relatedTarget).data('fin');
       $("#ffin").html(pro4);

       var pro5 = $(e.relatedTarget).data('periodo2');
       $("#periodo3").html(pro5);
      
       var pro6 = $(e.relatedTarget).data('aldea');
       $("#aldeaa").html(pro6);
         
       var pro7 = $(e.relatedTarget).data('cod-sucre');
       $("#codsucre").html(pro7);

        
       var pro8 = $(e.relatedTarget).data('estatus');

        if(pro8==0){
            $("#estatus2").text('Inactivo');
            $("#estatus2").attr('class', 'label label-danger');

        }else if(pro8==1){
            $("#estatus2").html('Activo');
            $("#estatus2").attr('class', 'label label-primary');

       }

       

      });
   
    

     
</script>


    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>

















</body>
</html>
