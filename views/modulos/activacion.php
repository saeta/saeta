<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin | Activar Modulo </title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">



    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/cropper/cropper.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">






</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Activar Modulo </h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>activacion">Activación de Modulos</a>
                        </li>
                        <li class="breadcrumb-item active">
                        <strong>Activar Modulo </strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Listado de modulos  </h5>
                       
                        <div class="ibox-tools">
                        <!-- boton agregar-->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                        Activar modulo 
                                </button> 


                        </div>
                    </div>
                  
                    <div class="ibox-content">
                    <?php echo $this->mensaje; ?>
                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                    <th>Modulo/Tipo Activación</th>    
                        <th>Motivo de Activación</th>
                        <th>Fecha de inicio</th>
                        <th>Fecha de fin</th>
                        <th>Oferta Academica</th>
                        <th>Aldea</th>
                                       
                       <th>Opciones</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php include_once 'models/estructura.php';
                            foreach($this->modulos as $row){
                                $modulo= new Estructura();
                                $modulo=$row;?>
                    <tr class="gradeX">
                    <td><?php echo $modulo->tipo_activacion; ?></td>
                    <td><?php echo $modulo->modulo_activacion; ?> </td>
                    <td><?php echo date("d/m/Y", strtotime($modulo->fecha_inicio)); ?></td>
                    <td><?php echo date("d/m/Y", strtotime($modulo->fecha_fin)); ?></td>
                    <td><?php echo $modulo->oferta_academica; ?></td>
                    <td><?php echo $modulo->aldea; ?></td>
                
                                    
                    
                    <td> 

                        <a class="btn btn-outline btn-info" href="#myModal3" role="button" data-toggle="modal"  data-id="<?php echo $modulo->tipo_activacion;?>" data-motivo="<?php echo $modulo->modulo_activacion;?>" data-tipo="<?php echo $modulo->tipo_activacion;?>" data-inicio="<?php echo date("d/m/Y", strtotime($modulo->fecha_inicio));?>" data-fin="<?php echo date("d/m/Y", strtotime($modulo->fecha_fin));?>" data-oferta-academica="<?php echo $modulo->oferta_academica;?>"data-aldea="<?php echo $modulo->aldea;?>" data-cod-sucre="<?php echo $modulo->codigo_sucre;?>" data-pestatus="<?php echo $modulo->periodo_academico_estatus;?>" data-periodo-academico="<?php echo $modulo->periodo_academico;?>" data-calendario="<?php echo $modulo->calendario_academico;?>"  data-carganotas="<?php echo $modulo->estatus_carga_notas;?>" data-turno-des="<?php echo $modulo->turno;?>" data-ambiente="<?php echo $modulo->ambiente;?>" data-ambiente-dir="<?php echo $modulo->ambiente_direccion;?>"  data-parroquia="<?php echo $modulo->parroquia;?>" data-ambiente-est="<?php echo $modulo->ambiente_estatus;?>" data-clatitud="<?php echo $modulo->coordenada_latitud;?>" data-clongitud="<?php echo $modulo->coordenada_longitd;?>" ><i class="fa fa-search"></i> Ver</a> &nbsp;
                                           
                     
                       <?php if($modulo->estatus_aldea==0){ ?>
                        <a class="btn btn-outline btn-danger" href="#" title="Aldea inactiva" >Inabilitado</a> &nbsp;
                        <?php }else{?>
                        <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal" data-id="<?php echo $modulo->id_modulo_activacion;?>" data-modulo="<?php echo $modulo->tipo_activacion;?>" data-motivo="<?php echo $modulo->modulo_activacion;?>"  data-finicio="<?php echo date("m/d/Y", strtotime($modulo->fecha_inicio));?>" data-fin="<?php echo date("m/d/Y", strtotime($modulo->fecha_fin));?>" data-aldea="<?php echo $modulo->id_aldea;?>" data-oferta-academica="<?php echo $modulo->id_oferta_academica;?>"> Editar</a> &nbsp;
                        <?php }?>
                     
                       
                  </td>
                    </tr>
                            <?php }?>
                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>

      


<!-- ///////////////Modal Agregar////////////////// -->
        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Activar modulo</h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            <h2>
                            Activar Modulo
                            </h2>
                            <p>
                            Activar el modulo para el proceso de inscripción de Nuevos Ingresos o carga de notas.
                            </p>

                            <form id="form" action="<?php echo constant('URL');?>activacion/ActivarModulo"  method="post" class="wizard-big">
                                <h1>Control de tiempo</h1>
                                <fieldset>
                                    <h2> Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">

                                           
                                                    <div class="form-group">
                                                    <label>Modulo <span style="color: red;">*</span></label>
                                                    <select class="form-control required m-b " name="modulo" id="modulo">                                      
                                                    <option value="" selected="selected">Selecione</option>
                                                    <option value="Inscripcion" >Inscripción</option>
                                                    <option value ="Carga de Nota">Carga de notas</option>                                               
                                                    </select>                                              
                                                    </div>

                                            <div class="form-group">
                                                <label>Motivo de Activación<span style="color: red;">*</span></label>
                                                <textarea id="motivo" name="motivo" type="text" class="form-control required" placeholder="Escriba una dirección..." aria-required="true" aria-invalid="false" onkeypress="return soloLetras(event)" maxlength="70" minlength="4"></textarea>
                                            </div>

                                            <div class="form-group" id="data_5">
                                                <label class="font-normal">Fecha de Inicio <span style="color: red;">*</span></label>
                                                <label class="font-normal" style=" position: relative;left: 110px;">Fecha de Fin <span style="color: red;">*</span></label>
                                                <div class="input-daterange input-group" id="datepicker">
                                                    <input type="text" class="form-control-sm form-control required" name="start" placeholder="<?php echo date('d/m/Y');?>">
                                                    <span class="input-group-addon">a</span>
                                                    <input type="text" class="form-control-sm form-control required" name="end" placeholder="<?php echo date('d/m/Y');?>">
                                                </div>
                                            </div>        
                                             </div>
                                             
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-clock-o" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </fieldset>
                                    <h1>Ubicación</h1>
                                        <fieldset>
                                            <h2>Información</h2>
                                            <div class="row">
                                                <div class="col-lg-8">
        
                                               
                                        <div class="form-group">
                                        <label>Aldea<span style="color: red;">*</span></label>
                                        <select class="chosen-select form-control required m-b " name="aldea" id="aldea">
                                        <option selected="selected" value="">Seleccione </option> 
                                        <?php include_once 'models/estructura.php';
                                            foreach($this->aldeas as $row){
                                            $aldea=new Estructura();
                                            $aldea=$row;?> 
                                        <option value="<?php echo $aldea->id_aldea;?>"><?php echo $aldea->descripcion;?></option>
                                        <?php }?>
                                        </select>                                               
                                        </div>

                                
                                        <div class="form-group">
                                        <label class="font-normal">Oferta academica</label>
                                        <div>
                                        <select data-placeholder="Selecione una oferta academica..." name="ofertas[]" class="chosen-select form-control required m-b" multiple style="width:350px;" tabindex="4">
                                    
                                        <?php include_once 'models/estructura.php';
                                            foreach($this->ofertas as $row){
                                            $oferta=new Estructura();
                                            $oferta=$row;?> 
                                        <option value="<?php echo $oferta->id_oferta_academica;?>"><?php echo $oferta->descripcion;?></option>
                                    <?php }?>
                                        </select>
                                        </div>
                                        </div>             
                
                                                      
                                    </div>
                                                <div class="col-lg-4">
                                                    <div class="text-center">
                                                        <div style="margin-top: 20px">
                                                            <i class="fa fa-clock-o" style="font-size: 180px;color: #e5e5e5 "></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                        </fieldset>
                       
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
  
<!-- ///////////////Modal editar////////////////// -->
<div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
            <h4 class="modal-title">Editar modulo</h4>
            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
        </div>
        <div class="modal-body" >
            
        <div class="ibox-content">
<h2>
Activar Modulo
</h2>
<p>
Activar el modulo para el proceso de inscripción de Nuevos Ingresos o carga de notas.
</p>

<form id="form2" action="<?php echo constant('URL');?>activacion/ActualizarModulo"  method="post" class="wizard-big">
<h1>Control de tiempo</h1>
<fieldset>
    <h2> Información</h2>
    <div class="row">
        <div class="col-lg-8">

        <input type="hidden" class="form-control-sm form-control" name="id_modulo_activacion" id="id_modulo_activacion" value="">
                  
                    <div class="form-group">
                    <label>Modulo <span style="color: red;">*</span></label>
                    <select class="form-control required m-b " name="moduloe" id="moduloe" value="">                                      
                <!--    <option value="" selected="selected">Selecione</option>-->
                    <option value="Inscripcion" >Inscripción</option>
                    <option value ="Carga de Nota">Carga de notas</option>                                               
                    </select>                                              
                    </div>

            <div class="form-group">
                <label>Motivo de Activación<span style="color: red;">*</span></label>
                <textarea id="motivoe" name="motivoe" type="text" class="form-control required" placeholder="Escriba una dirección..." aria-required="true" aria-invalid="false" onkeypress="return soloLetras(event)" maxlength="70" minlength="4" value=""></textarea>
            </div>

            <div class="form-group" id="data_6">
                <label class="font-normal">Fecha de Inicio <span style="color: red;">*</span></label>
                <label class="font-normal" style=" position: relative;left: 110px;">Fecha de Fin <span style="color: red;">*</span></label>
                <div class="input-daterange input-group" id="datepicker">
                    <input type="text" class="form-control-sm form-control required" name="starte" id="starte" value="">
                    <span class="input-group-addon">a</span>
                    <input type="text" class="form-control-sm form-control required" name="ende"  id="ende" value="" >
                </div>
            </div>
          
</div>
        <div class="col-lg-4">
            <div class="text-center">
                <div style="margin-top: 20px">
                    <i class="fa fa-clock-o" style="font-size: 180px;color: #e5e5e5 "></i>
                </div>
            </div>
        </div>
    </div>
    </fieldset>
    <h1>Ubicación</h1>
        <fieldset>
            <h2>Información</h2>
            <div class="row">
                <div class="col-lg-8">

               
        <div class="form-group">
        <label>Aldea<span style="color: red;">*</span></label>
        <select data-placeholder="Selecione una aldea..." class="chosen-select form-control required m-b " name="aldeae" id="aldeae" value="">
        <!--<option selected="selected" value="">Seleccione </option> -->
        <?php include_once 'models/estructura.php';
            foreach($this->aldeas as $row){
            $aldea=new Estructura();
            $aldea=$row;?> 
        <option value="<?php echo $aldea->id_aldea;?>"><?php echo $aldea->descripcion;?></option>
        <?php }?>
        </select>                                               
        </div>

        <div class="form-group">
        <label class="font-normal">Oferta academica</label>
        <div>
        <select data-placeholder="Selecione una oferta academica..." name="ofertae" id="ofertae" class="chosen-select form-control required m-b" value="">
    
        <?php include_once 'models/estructura.php';
            foreach($this->ofertas as $row){
            $oferta=new Estructura();
            $oferta=$row;?> 
        <option value="<?php echo $oferta->id_oferta_academica;?>"><?php echo $oferta->descripcion;?></option>
    <?php }?>
        </select>
        </div>
        </div>                         
                  
    </div>
                <div class="col-lg-4">
                    <div class="text-center">
                        <div style="margin-top: 20px">
                            <i class="fa fa-clock-o" style="font-size: 180px;color: #e5e5e5 "></i>
                        </div>
                    </div>
                </div>
            </div>
                        </fieldset>

</form>
</div>
        
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
  </div>
</div>
</div>
</div>        

<!--////////////////////////////////-->


 
<!-- ///////////////Modal Mostrar////////////////// -->
<div class="modal inmodal fade " style="width: 100%;" id="myModal3" tabindex="-1" role="dialog"  aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
            <h4 class="modal-title" > Dedalles modulo Activación</h4>
            <small class="font-bold"><h4 class="modal-title" id="detalle">  </h4></small>
        
         </div>
        <div class="modal-body" >
            
        <div class="ibox-content">

                          <table cellspacing="0" cellpadding="0">
                                <tbody>
                                
                                <!--<tr>
                                    <td>
                                        <img class="img-fluid" src="<?php echo constant ('URL');?>src/img/landing/header_one.jpg">
                                    </td>
                                </tr>-->
                    <div class="row">
                    <div class="col-sm-6 b-r">
                    
                    
                        <h3 class="m-t-none m-b">Motivo de Activación</h3>
                        <p  id="mo"></p>
                        <h3 class="m-t-none m-b">Modulo /Tipo de Activación</h3>
                        <p  id="ti"></p>
                        <h3 class="m-t-none m-b">Fecha de Inicio / Fecha Fin</h3>
                       
                        <div class="input-daterange input-group" >
                        <i class="fa fa-calendar"></i>&nbsp; <p id="finicio"></p>
                        <span >&nbsp;&nbsp;</span>
                        <i class="fa fa-calendar">&nbsp;</i><p id="ffin"></p>   

                   
                    </div>

                    <h3 class="m-t-none m-b">Oferta Academica</h3>
                        <p  id="oferta"></p>
                        <h3 class="m-t-none m-b">Aldea</h3>
                        <p  id="aldeaa"></p>

                        <h3 class="m-t-none m-b">Codigo Sucre</h3>
                        <p  id="codsucre"></p>

                        <h3 class="m-t-none m-b">Perido Academico</h3>
                        <p id="pacademico"></p> &nbsp;Estatus  &nbsp;<span class="label label-info" style="font-size: 12px;"  id="peridoestatus"></span>  


                </div>


                 <div class="col-sm-6">
                     
                      
                      
                        <h3 class="m-t-none m-b">Calendario Academico</h3>
                        <p  id="calen"></p>
                        
                        <h3 class="m-t-none m-b">Carga de Notas</h3>
                         &nbsp;Estatus  &nbsp;<span class="label label-info" style="font-size: 12px;"  id="enotas"></span>  

                         <h3 class="m-t-none m-b">Turno</h3>
                        <p  id="turno"></p>


                        <h3 class="m-t-none m-b">Ambiente</h3>
                        <p  id="ambientee"></p>
                         &nbsp;Estatus  &nbsp;<span class="label label-info" style="font-size: 12px;"  id="eambiente"></span>  


                        <h3 class="m-t-none m-b">Direcciòn</h3>
                        <p  id="direccion"></p>

                        <h3 class="m-t-none m-b">Coordenadas</h3>
                        Latitud : <p  id="latitud"></p> Longitud : <p  id="longitud"></p>

                        <h3 class="m-t-none m-b">Parroquia</h3>
                        <p  id="parroquiaa"></p>

                 </div>


                              </tbody>
                            </table>


        </div>
        
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
  </div>
</div>
</div>

</div>        





<!--////////////////////////////////-->     


  
    <?php require 'views/footer.php'; ?>

   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
   <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>
    <!-- Chosen Select -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script> 
        <!-- Solo Letras -->
    <script src="<?php echo constant ('URL');?>src/js/val_letras.js"></script>
        <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>

    


<!--comenzar desde aqui -->


<!-- Data picker -->
<script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>


 <!-- Image cropper -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/cropper/cropper.min.js"></script>

 
 <!-- Tags Input -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

<!-- <script>
     $(document).ready(function(){

         $('.tagsinput').tagsinput({
             tagClass: 'label label-primary'
         });

         $('#data_5 .input-daterange').datepicker({
             keyboardNavigation: false,
             forceParse: false,
             autoclose: true
         });
      
         
        });

 </script>-->


<script>
  $('#myModal1').on('shown.bs.modal', function () {  
       $('.chosen-select').chosen({width: "100%"});


       $('.tagsinput').tagsinput({
             tagClass: 'label label-primary'
         });

         $('#data_5 .input-daterange').datepicker({
             keyboardNavigation: false,
             forceParse: false,
             autoclose: true
         });


       });


  $('#myModal2').on('show.bs.modal', function(e) {
    
  var product = $(e.relatedTarget).data('id');
  //$("#id_modulo_activacion").val(product);
  $("#id_modulo_activacion").attr('value', product);


  var product2 = $(e.relatedTarget).data('modulo');
  //$("#moduloe").val(product2);
  $("#moduloe").attr('value', product2);

  var product3 = $(e.relatedTarget).data('motivo');
  $("#motivoe").text(product3);
  //$("#motivoe").attr('value', product3);


  var product4 = $(e.relatedTarget).data('finicio');
   $("#starte").attr('value', product4);

   var product5 = $(e.relatedTarget).data('fin');
   $("#ende").attr('value', product5);


  var product6 = $(e.relatedTarget).data('aldea');
  //$("#aldeae").val(product6);
  $("#aldeae").attr('value', product6);


  var product7 = $(e.relatedTarget).data('oferta-academica');
 // $("#ofertae").val(product7);
  $("#ofertae").attr('value', product7);

  $('.chosen-select').chosen({width: "100%"});
  $('#ofertae').trigger('chosen:updated');
  $('#aldeae').trigger('chosen:updated');


  $('.tagsinput').tagsinput({
             tagClass: 'label label-primary'
         });

         $('#data_6 .input-daterange').datepicker({
             keyboardNavigation: false,
             forceParse: false,
             autoclose: true
         });



});

</script>


<script>
    $('#myModal3').on('show.bs.modal', function(e) {
    
        var pro = $(e.relatedTarget).data('id');
       $("#detalle").html(pro);

       var pro1 = $(e.relatedTarget).data('motivo');
       $("#mo").html(pro1);


       var pro2 = $(e.relatedTarget).data('tipo');
       $("#ti").html(pro2);
   
       var pro3 = $(e.relatedTarget).data('inicio');
       $("#finicio").html(pro3);

       var pro4 = $(e.relatedTarget).data('fin');
       $("#ffin").html(pro4);

       var pro5 = $(e.relatedTarget).data('oferta-academica');
       $("#oferta").html(pro5);
      
       var pro6 = $(e.relatedTarget).data('aldea');
       $("#aldeaa").html(pro6);
         
       var pro7 = $(e.relatedTarget).data('cod-sucre');
       $("#codsucre").html(pro7);

        
       var pro8 = $(e.relatedTarget).data('pestatus');
        if(pro8==0){
            $("#peridoestatus").html('Inactivo');
            $("#peridoestatus").attr('class', 'label label-danger');

        }else if(pro8==1){
            $("#peridoestatus").html('Activo');
            $("#peridoestatus").attr('class', 'label label-primary');

       }

       //console.log(pro8);
       var pro9 = $(e.relatedTarget).data('periodo-academico');
       $("#pacademico").html(pro9);

       var pro10 = $(e.relatedTarget).data('calendario');
       $("#calen").html(pro10);

       var pro11 = $(e.relatedTarget).data('carganotas');
      //console.log(pro11);
        if(pro11==0){
            $("#enotas").html('Inactivo');
            $("#enotas").attr('class', 'label label-danger');

        }else if(pro11==1){
            $("#enotas").html('Activo');
            $("#enotas").attr('class', 'label label-primary');

       }
       var pro12 = $(e.relatedTarget).data('turno-des');
       $("#turno").html(pro12);
       var pro12 = $(e.relatedTarget).data('ambiente');
       $("#ambientee").html(pro12);

       var pro12 = $(e.relatedTarget).data('ambiente-dir');
       $("#direccion").html(pro12);

       var pro12 = $(e.relatedTarget).data('parroquia');
       $("#parroquiaa").html(pro12);

       


       var pro13 = $(e.relatedTarget).data('ambiente-est');
      //console.log(pro11);
        if(pro13==0){
            $("#eambiente").html('Inactivo');
            $("#eambiente").attr('class', 'label label-danger');

        }else if(pro13==1){
            $("#eambiente").html('Activo');
            $("#eambiente").attr('class', 'label label-primary');

       }

       var pro14 = $(e.relatedTarget).data('clongitud');
       $("#longitud").html(pro14);
       var pro15 = $(e.relatedTarget).data('clatitud');
       $("#latitud").html(pro15);


      });
   
    

     
</script>


    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>

















</body>
</html>
