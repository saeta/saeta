<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SIDTA | Editar Asociación</title>

 
    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">


    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">
    <!--  style select2 -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/select2/select2.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">


</head>

<body>
 
   <?php require 'views/header.php'; ?>
   

   <div class="row wrapper border-bottom white-bg page-heading">
   <div class="col-sm-7">
                <h2>Asociar Alto Nivel a Centros de Estudio</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            Admin. del Sistema
                        </li>
                        <li class="breadcrumb-item">
                            Datos Académicos
                        </li>
                        <li class="breadcrumb-item">
                            Centro de Estudios
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>centro_estudio/viewAssoc">Listar</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Editar</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-5">
                    <div class="title-action">
                    <!-- boton agregar-->
                        <a href="<?php echo constant ('URL');?>centro_estudio/viewAssoc"><button type="button" class="btn btn-primary">
                            <i class="fa fa-arrow-left"></i> Volver
                        </button> </a>    
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>SIDTA</h5>
                        <div class="ibox-tools">

                        </div>
                    </div>
                    <div class="ibox-content">

                        <!--  contenido -->

                            <h2>
                                Editar Asociación
                            </h2>
                            <p>
                                Modifica la opcion que deseas editar.
                            </p>
                            <?php echo $this->mensaje;?>
                            <div class="alert alert-info">Todos los campos marcados con un (<span style="color: red;">*</span>) Son Obligatorios</div>

                            <form id="form" action="<?php echo constant ('URL') . "centro_estudio/editarAssoc/" . $this->asociacion->id_persona_centro_estudio;?>" method="post" class="wizard-big">
                                <h1>Asociar</h1>
                                <fieldset>
                                    <h2>Asociar Alto Nivel a Centros de Estudio</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <label>Personas Disponibles para Asociar<span style="color: red;">*</span></label>
                                                <select class="form-control required m-b select2_demo_1" disabled data-placeholder="Seleccione las Personas disponibles para asociar" style="width: 100%" name="director" id="director">
                                                    <option value="">Seleccione</option> 
                                                    <?php  
                                                            foreach($this->dircoord as $row){
                                                                $dircoord= new Estructura();
                                                                $dircoord=$row;
                                                                ?>
                                                            <option value="<?php echo $dircoord->id_persona.",".$dircoord->id_perfil;?>" <?php if($dircoord->id_persona==$this->asociacion->id_persona){ echo "selected=selected"; }?>>CI: <?php echo $dircoord->identificacion . " | " . $dircoord->primer_nombre . " " . $dircoord->primer_apellido. " | " . $dircoord->perfil ;?></option>
                                                            <?php }?>
                                                </select> 
                                            </div>
                                            <div class="form-group">
                                                <label>Centros de Estudios Disponibles<span style="color: red;">*</span></label>
                                                <select class="form-control required m-b select2_demo_1" disabled style="width: 100%"  data-placeholder="Seleccione el Centro de Estudio" name="id_centro_estudio" id="id_centro_estudio">
                                                    <option value="">Seleccione el Centro de Estudio</option> 
                                                    <?php  
                                                            foreach($this->centros as $row){
                                                                $centro= new CentroEstudio();
                                                                $centro=$row;
                                                                ?>
                                                            <option value="<?php echo $centro->id_centro_estudio;?>" <?php if($centro->id_centro_estudio==$this->asociacion->id_centro_estudio){ echo "selected=selected"; }?>><?php echo $centro->descripcion;?></option>
                                                            <?php }?>
                                                </select>                                             
                                            </div>
                                            <div class="form-group">
                                                <label>Estatus<span style="color: red;">*</span></label>
                                                <select class="form-control required m-b select2_demo_1" style="width: 100%"  data-placeholder="Seleccione el Estatus" name="estatus" id="estatus">
                                                    <option value="">Seleccione el Estatus</option> 
                                                    <option value="Activo" <?php if($this->asociacion->estatus=='Activo'){ echo "selected=selected"; }?>>Activo</option>
                                                    <option value="Inactivo" <?php if($this->asociacion->estatus=='Inactivo'){ echo "selected=selected"; }?>>Inactivo</option>
                                                </select>                                             
                                            </div>
                                        </div>
                                        <script>
                                            $(".select2_demo_1").select2({
                                                placeholder: "Seleccione...",
                                                allowClear: true
                                            });
                                        </script>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-institution" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                                
                            </form>

                        <!--  end contenido -->

                    </div>
                </div>
            </div>
            </div>
        </div>



    <?php require 'views/footer.php'; ?>

     
    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>
       <!-- Select2 -->
       <script src="<?php echo constant ('URL');?>src/js/plugins/select2/select2.full.min.js"></script>

       <!-- Jquery Validate -->
       <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                   
                  

                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            
                        }
                    });
       });
        

    </script>

   

</body>
</html>










