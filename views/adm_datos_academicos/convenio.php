<!--
*
*  INSPINIA - Responsive Adfecha_convenio Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin | Convenio</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

 


</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                <h2>Registrar Convenio</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            Datos Académicos
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Registrar Convenio</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">                                                                                                                                                                                                                                                                                                                                     


                <div class="ibox ">                                                                                                                                                                 
                    <div class="ibox-title">
                        <h5>Lista de Convenios Registrados</h5>
                       
                        <div class="ibox-tools">                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                        <!-- boton agregar-->
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                            <i class="fa fa-plus"></i> Agregar Convenio
                            </button>                                                                                                 


                        </div>
                    </div>
                    
                    <div class="ibox-content">
                    <div id="respuesta"><?php echo $this->mensaje; ?></div>

                        <div class="table-responsive">
                        
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Convenio</th>
                        <th>Institución</th>
                        <th>Fecha de Vigencia</th>


                        <th>Acción</th>
                    </tr>
                    </thead >
                    <tbody id="tbody-convenios">
                    <?php include_once 'models/datosacademicos/convenio.php'; 
                    $i=0;
                            foreach($this->convenios as $row){
                                $convenio= new ConvenioI();
                                $convenio=$row; $i++;?>
                    <tr id ="fila-<?php echo $convenio->id_convenio; ?>" class="gradeX">
                        <td><?php echo $convenio->descripcion; ?></td>
                        <td><?php echo $convenio->institucion_convenio; ?> </td>
                        <td><?php echo $convenio->fecha_convenio; ?> </td>

                        <td> 
                            <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal" data-id_convenio="<?php echo $convenio->id_convenio;?>" data-convenio="<?php echo $convenio->descripcion;?>" data-institucion_convenio="<?php echo $convenio->institucion_convenio;?>" data-fecha_convenio="<?php echo $convenio->fecha_convenio;?>"><i class="fa fa-edit"></i> Editar</a>&nbsp;
                            <a class="btn btn-outline btn-primary" href="#myModal3" role="button" data-toggle="modal" data-id_convenio1="<?php echo $convenio->id_convenio;?>" data-convenio1="<?php echo $convenio->descripcion;?>" data-institucion_convenio1="<?php echo $convenio->institucion_convenio;?>" data-fecha_convenio1="<?php echo $convenio->fecha_convenio;?>"><i class="fa fa-search"></i> Ver</a>&nbsp;

                            <!-- <a class="btn btn-outline btn-danger" href="<?php echo constant('URL') . 'convenio/deleteconvenio/' . $convenio->id_convenio;?>" role="button"> Remover</a> &nbsp; -->
                            <!-- <button class="btn btn-outline btn-danger bEliminar" data-id="<?php echo $convenio->id_convenio;?>"><i class="fa fa-trash"></i> Eliminar</button> -->
                        </td>
                    </tr>
                            <?php }?>
                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
<!-- ///////////////Modal Agregar////////////////// -->
        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Registrar Convenio</h4>
                                            <small class="font-bold">Registrar los convenios educativos que mantiene la
universidad.
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox">
                        
                        <div class="ibox-content">
                            
                            <form id="form" method="post" action="<?php echo constant('URL');?>convenio/registrar" class="wizard-big">
                                <h1>Convenio</h1>
                                <fieldset>
                                    <h2>Información de la Convenio</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <label>Convenio <span style="color: red;">*</span></label>
                                                <input type="text" name="descripcion" id="descripcion" class="form-control required" placeholder="Ingrese el nombre del convenio">
                                                <!-- <select class="form-control required m-b" name="descripcion" id="descripcion">
                                                            
                                                            <option value="">Seleccione</option> 
                                                         
                                                            <option value="Exclusiva">Exclusiva</option> 
                                                            <option value="Tiempo Completo">Tiempo Completo</option> 
                                                            <option value="Medio Tiempo">Medio Tiempo</option> 
                                                            <option value="Convencional">Convencional</option> 
                                                        </select> -->
                                            </div>
                                            <div class="form-group">
                                                <label>Institución de Convenio <span style="color: red;">*</span></label>
                                                <input id="institucion_convenio" name="institucion_convenio" type="text" placeholder="Ingrese la Institución de Convenio" maxlength="70" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                                <label>Fecha de Vigencia <i></i><span style="color: red;">*</span></label>
                                                <input id="fecha_convenio" name="fecha_convenio" type="date" placeholder="Ingrese la Fecha de Vigencia" maxlength="70" class="form-control required">
                                            </div>
                                            
                                            
                                            
                                            <input type="hidden" name="registrar">
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                               

                                
                            </form>
                        </div>
                    </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
         <!-- ///////////////Modal Editar////////////////// -->

<div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Editar Convenio</h4>
                                            <small class="font-bold">Registrar los convenios educativos que mantiene la
universidad.
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            
                            <form id="form2" method="post" action="<?php echo constant('URL');?>convenio/editar" class="wizard-big">
                                <h1>Convenio</h1>
                                <fieldset>
                                    <h2>Información de la Convenio</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                        <div class="form-group">
                                        <input type="hidden" name="id_convenio" id="id_convenio">
                                        <label>Convenio <span style="color: red;">*</span></label>
                                                <input type="text" name="descripcion1" id="descripcion1" class="form-control required" placeholder="Ingrese el nombre del convenio">

                                            </div>
                                            <div class="form-group">
                                                <label>Institución Convenio <span style="color: red;">*</span></label>
                                                <input id="institucion_convenio1" name="institucion_convenio1" type="text" placeholder="Ingrese la Institución de Convenio" maxlength="70" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                                <label>Fecha de Vigencia <i></i><span style="color: red;">*</span></label>
                                                <input id="fecha_convenio1" name="fecha_convenio1" type="date" placeholder="Ingrese la Fecha de Vigencia" maxlength="70" class="form-control required">
                                            </div>
                                            
                                            
                                            
                                            <input type="hidden" name="registrar2">
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                               

                                
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>       
                    </div>


         <!-- ///////////////Modal Ver////////////////// -->

         <div class="modal inmodal fade " style="width: 100%;" id="myModal3" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Ver Convenio</h4>
                                            <small class="font-bold">Registrar los convenios educativos que mantiene la
universidad.
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            
                            <form id="form3" class="wizard-big">
                                <h1>Convenio</h1>
                                <fieldset>
                                    <h2>Información de la Convenio</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                        <div class="form-group">
                                                <label>Convenio <span style="color: red;">*</span></label>
                                                <input type="text" disabled id="descripcion2" class="form-control required" placeholder="Ingrese el nombre del convenio">

                                            </div>
                                            <div class="form-group">
                                                <label>Institución Convenio <span style="color: red;">*</span></label>
                                                <input id="institucion_convenio2" disabled name="institucion_convenio2" type="text" placeholder="Ingrese la Institución de Convenio" maxlength="70" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                                <label>Fecha de Vigencia <i></i><span style="color: red;">*</span></label>
                                                <input id="fecha_convenio2" disabled name="fecha_convenio2" type="date" placeholder="Ingrese la Fecha de Vigencia" maxlength="70" class="form-control required">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>       
                    </div>








    <?php require 'views/footer.php'; ?>

  

   <!-- dataTables -->
   <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
   <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
  
<!-- 
    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");  
    $("#re").addClass("active");
});   
</script>-->



<script>


$('#myModal2').on('show.bs.modal', function(e) {
    
    var product = $(e.relatedTarget).data('id_convenio');
    $("#id_convenio").val(product);


    var product1 = $(e.relatedTarget).data('convenio');
    $("#descripcion1").val(product1);
  
    var product2 = $(e.relatedTarget).data('institucion_convenio');
    $("#institucion_convenio1").val(product2);

    var product3 = $(e.relatedTarget).data('fecha_convenio');
    $("#fecha_convenio1").val(product3);

  });
  


$('#myModal3').on('show.bs.modal', function(e) {
    
    var product = $(e.relatedTarget).data('id_convenio1');
    $("#id_convenio1").val(product);


    var product1 = $(e.relatedTarget).data('convenio1');
    $("#descripcion2").val(product1);
  
    var product2 = $(e.relatedTarget).data('institucion_convenio1');
    $("#institucion_convenio2").val(product2);

    var product3 = $(e.relatedTarget).data('fecha_convenio1');
    $("#fecha_convenio2").val(product3);


  });

</script>


    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>
<!-- modal ver -->
<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form3").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    //form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>
    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>





</body>
</html>
