<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Comité académico PFA | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

 


</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Comité Académico PFA</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>comite_academico">Datos Academicos</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Comités</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Registros</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Listado de comités  </h5>
                       
                    </div>
                    
                    <div class="ibox-content">
                    <?php echo $this->mensaje; ?>
                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                      <th>Registro</th>
                      <th>Comité académico</th>
                      <th>Programa de Formación</th>     
                      <th>Tipo de comité</th> 
                      <th>Integrantes del comité académico</th>
                                          
              

                    </tr>
                    </thead>
                    <tbody>
                    <?php include_once 'models/estructura.php';
                            foreach($this->lista_comites as $row){
                                $comit= new Estructura();
                                $comit=$row;?>
                    <tr class="gradeX">
                    <td><i class="fa fa-calendar"></i> <?php echo date("d/m/Y", strtotime($comit->fecha_registro)); ?> </td>
                    <td><?php echo $comit->comite_nombre; ?> </td>
                    <td><?php echo $comit->comite_programa; ?></td>
                    <td> <?php echo $comit->comite_tipo; ?> </td>
                                     
                    <td>  
                    <?php if($_SESSION['Consultar']==true){?>

                    <a href="<?php echo constant('URL') . 'comite_academico/verComiteHistoricos/' .$comit->comite_nombre.",".$comit->fecha_registro;?>"><button type="button" class="btn btn-outline btn-info">&nbsp; Info &nbsp;</button></a>
                    <?PHP } ?>
                  </td>
                    </tr>
                            <?php }?>
                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            
            </div>
        </div>
<!-- ///////////////Modal Agregar////////////////// -->
        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Registrar comité académico</h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            <h2>
                            Registrar Aldea
                            </h2>
                            <p>
                            &nbsp;Permite al usuario registrar una sede de
                            la UBV o una aldea de la misión Sucre donde se
                            dicten programas de la UBV.
                            </p>

                            <form id="form" action="<?php echo constant('URL');?>comite_academico/registrarComite"  method="post" class="wizard-big">
                                <h1>Descripción</h1>
                                <fieldset style="height:380px;">
                                    <h2>Información </h2>
                                    <div class="row">
                                        <div class="col-lg-8">

                                   
                                         <div class="form-group">
                                            <label>listado de PFA de la Universidad
                                            Bolivariana de Venezuela (UBV)<span style="color: red;">*</span></label>                                        
                                            <select  name="id_programa" id="id_programa"  class="chosen-select form-control required m-b">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/estructura.php';
                                                foreach($this->pfas as $row){
                                                $pfa=new Estructura();
                                                $pfa=$row;?> 
                                            <option value="<?php echo $pfa->id_programa;?>"><?php echo $pfa->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>


                                       
                                         <div class="form-group">
                                            <label>Tipo de Comite <span style="color: red;">*</span></label>                                        
                                            <select  name="id_comite_tipo" id="id_comite_tipo"  class="chosen-select form-control required m-b">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/estructura.php';
                                                foreach($this->comites as $row){
                                                $comite=new Estructura();
                                                $comite=$row;?> 
                                            <option value="<?php echo $comite->id_comite_tipo;?>"><?php echo $comite->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>


                                            <div class="form-group">
                                            <label>Docentes <span style="color: red;">*</span></label>                                        
                                            <select data-placeholder="seleccione un docente..."   name="docentes[]" id="docentes"  class="chosen-select form-control required m-b" multiple style="width:350px;" tabindex="4">
                                            <?php include_once 'models/estructura.php';
                                                foreach($this->docentes as $row){
                                                $docente=new Estructura();
                                                $docente=$row;?> 
                                            <option value="<?php echo $docente->id_docente;?>"><?php echo $docente->identificacion." ".$docente->primer_nombre." ".$docente->primer_apellido;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>

                                            <div class="form-group">
                                        <label>Nombre del comité académico <span style="color: red;">*</span></label>
                                        <input id="nombre_comite" name="nombre_comite" type="text" class="form-control required" maxlength='70' minlength="3">
                                         </div>


                                          <!--  <div class="form-group">
                                            <label>Estatus <span style="color: red;">*</span></label>
                                            <select class="form-control required m-b " name="estatus" id="estatus">                                      
                                            <option value="0" selected="selected">Inactivo</option>
                                            <option value ="1">Activo</option>                                               
                                            </select>                                              
                                            </div>-->

                                         
                                    </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-users" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </fieldset>
                                      
                          
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
  
<!--////////////////////////////////-->
  


    <?php require 'views/footer.php'; ?>

        
   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

<!-- menu active -->
<script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>

<!-- Solo Letras -->
<script src="<?php echo constant ('URL');?>src/js/val_letras.js"></script>
  <!-- Chosen Select -->
  <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script> 
  




<script>
              
  $('#myModal1').on('shown.bs.modal', function () {  
       $('.chosen-select').chosen({width: "100%"});
       
       });

           

                

$('#myModal2').on('show.bs.modal', function(e) {
    
    var product1 = $(e.relatedTarget).data('comite');
  $("#id_para_comite_academico").val(product1);

  var product2 = $(e.relatedTarget).data('comite');
  $("#comite_academico").val(product2);
 
  var product3 = $(e.relatedTarget).data('pfa');
  $("#pfg").val(product3);
  console.log(product3);
  var product4 = $(e.relatedTarget).data('tipo');
  $("#comite_tipo").val(product4);

  $('.chosen-select').chosen({width: "100%"});
  $('#pfg').trigger('chosen:updated');
  $('#comite_tipo').trigger('chosen:updated');
  


});
</script>


    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>





</body>
</html>
