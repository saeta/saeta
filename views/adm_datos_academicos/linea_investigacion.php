<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Línea de Investigación | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  style chosen -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">


    <!--  style switch -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/switchery/switchery.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">



</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-9">
                <h2>Registrar Línea de Investigación</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            Datos Académicos
                        </li>
                        <li class="breadcrumb-item">
                            Centros de Estudio
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Registrar Línea de Investigación</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-3">
                    <div class="title-action">
                    <?php if($_SESSION['Agregar']==true){?>

                        <!-- boton agregar-->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                        <i class="fa fa-plus"></i> Agregar Línea
                        </button> 
                    <?php } ?>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Lista de Líneas de Investigación Registradas</h5>
                       
                        <div class="ibox-tools">
                        


                        </div>
                    </div>
                    
                    <div class="ibox-content">
                    <div id="respuesta"><?php echo $this->mensaje; ?></div>

                        <div class="table-responsive">
                        
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Línea de Investigación</th>
                        <th>Código</th>
                        <th>Especificación</th>
                        <th>Pertenece al Núcleo Académico</th>
                        <th>Acción</th>
                    </tr>
                    </thead>
                    <tbody id="tbody-lineas">
                    <?php include_once 'models/datosacademicos/lineainvestigacion.php';
                            foreach($this->lineas as $row){
                                $linea= new LineaInvestigacion();
                                $linea=$row;?>
                    <tr id ="fila-<?php echo $linea->id_linea_investigacion; ?>" class="gradeX">
                        <td><?php echo $linea->descripcion; ?> </td>
                        <td><?php echo $linea->codigo;?></td>
                        <td><?php echo substr($linea->especificacion, 0, 40) . "...";?></td>
                        <td><?php echo substr($linea->descripcion_nucleo, 0, 40) . "..."; ?></td>
                       
                        <td> 
                            <?php if($_SESSION['Editar']==true){?>

                            <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal" data-id_linea="<?php echo $linea->id_linea_investigacion;?>" data-linea="<?php echo $linea->descripcion;?>"  data-id_nucleo_academico="<?php echo $linea->id_nucleo_academico; ?>" data-linea="<?php echo $linea->descripcion_nucleo;?>" data-codigo="<?php echo $linea->codigo;?>" data-especificacion="<?php echo $linea->especificacion;?>"><i class="fa fa-edit"></i> Editar</a>&nbsp;
                            <?php } 
                                if($_SESSION['Consultar']==true){?>

                            <a class="btn btn-outline btn-primary" href="#myModal3" role="button" data-toggle="modal" data-id_linea1="<?php echo $linea->id_linea_investigacion;?>" data-linea1="<?php echo $linea->descripcion;?>"  data-id_nucleo_academico1="<?php echo $linea->id_nucleo_academico; ?>" data-linea1="<?php echo $linea->descripcion_nucleo;?>" data-codigo1="<?php echo $linea->codigo;?>" data-especificacion1="<?php echo $linea->especificacion;?>"><i class="fa fa-search"></i> Ver</a>&nbsp;
                            <!-- <button class="btn btn-outline btn-info bespecificacion" data-id1="<?php echo $linea->id_linea_investigacion;?>" data-especificacion2="<?php echo $linea->especificacion;?>"><i class="fa fa-pencil"></i> Cambiar especificacion</button> -->
                                <?php } ?>
                            <!-- <a class="btn btn-outline btn-danger" href="<?php echo constant('URL') . 'linea_investigacion/deletelinea_investigacion/' . $linea->id_linea_investigacion;?>" role="button"> Remover</a> &nbsp; -->
                            <!-- <button class="btn btn-outline btn-danger bEliminar" data-id="<?php echo $linea->id_linea_investigacion;?>"><i class="fa fa-trash"></i> Eliminar</button> -->
                        </td>
                    </tr>
                            <?php }?>
                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
<!-- ///////////////Modal Agregar////////////////// -->
        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Registrar Línea de Investigación</h4>
                                            <small class="font-bold">Registrar las lineas de investigación de un
programa, para su uso en los proyectos de
investigación.
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox">
                        
                        <div class="ibox-content">
                            
                            <form id="form" method="post" action="<?php echo constant('URL');?>linea_investigacion/registrar" class="wizard-big">
                                <h1>Línea de Investigación</h1>
                                <fieldset>
                                    <h2>Información del Línea de Investigación</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                        <div class="form-group">
                                                <label>Asociar con un Núcleo Académico<span style="color: red;">*</span></label>
                                                
                                                <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Choose a Country..." name="id_nucleo_academico" id="id_nucleo_academico">
                                                    <option value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/nucleo_academico.php'; 
                                                            foreach($this->nucleos as $row){
                                                                $nucleo= new Nucleo();
                                                                $nucleo=$row;
                                                                ?>
                                                            <option value="<?php echo $nucleo->id_nucleo_academico;?>"><?php echo $nucleo->descripcion;?></option>
                                                            <?php }?>
                                                </select> 

                                            </div>
                                            <div class="form-group">
                                                <label>Línea de Investigación <span style="color: red;">*</span></label>
                                                <input id="descripcion" name="descripcion" type="text" placeholder="Ingrese la descripción del Línea de Investigación" maxlength="70" class="form-control required">
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Código <span style="color: red;">*</span></label>
                                                <input id="codigo" name="codigo" type="text" placeholder="Ingrese el código" maxlength="5" class="form-control required number">
                                            </div>
                                            <div class="form-group">
                                            
                                                <label>Especificación<span style="color: red;">*</span></label>
                                                <textarea class="form-control required" name="especificacion" id="especificacion" placeholder="Ingrese el Objetivo de la línea de Investigación" cols="30" rows="3"></textarea>
                                                <!-- <input type="checkbox" class="js-switch_4" id="sw" name="especificacion" checked  />
                                                <input type="checkbox" class="js-switch_4" name="especificacion_check" checked /> -->
                                            </div>
                                            
                                            <div class="form-group">
                                                <input type="hidden" name="registrar">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                               

                                
                            </form>
                        </div>
                    </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
         <!-- ///////////////Modal Editar////////////////// -->

<div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Editar Línea de Investigación</h4>
                                            <small class="font-bold">Se refiere a la actualización de las lineas de investigación de un
programa, para su uso en los proyectos de
investigación.
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            
                            <form id="form2" method="post" action="<?php echo constant('URL');?>linea_investigacion/editar" class="wizard-big">
                                <h1>Línea de Investigación</h1>
                                <fieldset>
                                    <h2>Información del Línea de Investigación</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                        <div class="form-group">
                                                <label>Asociar con un Núcleo Académico<span style="color: red;">*</span></label>
                                                <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige el Área..." name="id_nucleo_academico1" id="id_nucleo_academico1">
                                                    <option value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/nucleo_academico.php'; 
                                                        
                                                            foreach($this->nucleos as $row){
                                                                $nucleo= new Nucleo();
                                                                $nucleo=$row;
                                                                ?>
                                                            <option value="<?php echo $nucleo->id_nucleo_academico;?>"><?php echo $nucleo->descripcion;?></option>
                                                            <?php }?>
                                                </select> 
                                                
                                                
                                            </div>
                                            <div class="form-group">
                                            <input id="id_linea_investigacion" name="id_linea_investigacion" type="hidden" class="form-control">

                                                <label>Línea de Investigación <span style="color: red;">*</span></label>
                                                <input id="descripcion1" name="descripcion1" type="text" placeholder="Ingrese la descripción del Línea de Investigación" maxlength="70" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                                <label>Código <span style="color: red;">*</span></label>
                                                <input id="codigo1" name="codigo1" type="text" placeholder="Ingrese el código" maxlength="5"class="form-control required number">
                                            </div>
                                            <div class="form-group">
                                                <label>Especificación<span style="color: red;">*</span></label>
                                                <textarea class="form-control required" name="especificacion1" id="especificacion1" placeholder="Ingrese el Objetivo de la línea de Investigación" cols="30" rows="3"></textarea>
                                            </div>
                                            
                                            
                                            <input type="hidden" name="registrar2">
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                    
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>       
                    </div>

<!-- /////////////////////////////// -->

<!-- ///////////////Modal Ver////////////////// -->

<div class="modal inmodal fade " style="width: 100%;" id="myModal3" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Ver Línea de Investigación</h4>
                                            <small class="font-bold">Se refiere a la actualización de un linea o titulo académico correspondiente a un programa de formación. <br>(Licenciado en sistemas, arquitecto, Técnico Superior Universitario en Informática etc.)
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            
                            <form id="form3"  class="wizard-big">
                                <h1>Línea de Investigación</h1>
                                <fieldset>
                                    <h2>Información del Línea de Investigación</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                        <div class="form-group">
                                                <label>Asociar con un Núcleo Académico<span style="color: red;">*</span></label>
                                                
                                                <select class="form-control required m-b" disabled id="id_nucleo_academico2">
                                                            
                                                    <option value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/nucleo_academico.php'; 
                                                        
                                                            foreach($this->nucleos as $row){
                                                                $nucleo= new Nucleo();
                                                                $nucleo=$row;
                                                                ?>
                                                            <option value="<?php echo $nucleo->id_nucleo_academico;?>"><?php echo $nucleo->descripcion;?></option>
                                                            <?php }?>
                                                </select> 
                                            </div>
                                            <div class="form-group">
                                            <input id="id_linea_investigacion1" disabled type="hidden" class="form-control">

                                                <label>Línea de Investigación <span style="color: red;">*</span></label>
                                                <input id="descripcion2" disabled type="text" placeholder="Ingrese la descripción del Línea de Investigación" maxlength="70" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                                <label>Código <span style="color: red;">*</span></label>
                                                <input id="codigo2"  type="text" disabled placeholder="Ingrese el código" maxlength="5"class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Especificación<span style="color: red;">*</span></label>
                                                <textarea class="form-control required" name="especificacion2" id="especificacion2" placeholder="Ingrese el Objetivo de la línea de Investigación" readonly cols="30" rows="3"></textarea>

                                            </div>
                                            
                                            
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                               

                                
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>       
                    </div>




    <?php require 'views/footer.php'; ?>

    
    <!-- ajax de eliminar -->
    <script type="text/javascript" src="<?php echo constant('URL');?>src/js/datos_academicos/eliminarlinea.js"></script>
    <script type="text/javascript" src="<?php echo constant('URL');?>src/js/datos_academicos/especificacionlinea.js"></script>

  

   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
  
    <!-- Switchery -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/switchery/switchery.js"></script>

        <!-- Chosen -->
        <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>



<!-- 
    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");  
    $("#re").addClass("active");
});   
</script>-->



<script>

    
    $('#myModal1').on('show.bs.modal', function(e) {
        $('.chosen-select').chosen({width: "100%"});
        /*
            var elem = document.querySelector('.js-switch_4');
            var switchery = new Switchery(elem, { color: '#1AB394' });
            */
            //alert(switchery);
    });
    //$('.js-switch_4').empty();

    $('#myModal2').on('show.bs.modal', function(e) {
            
        var product = $(e.relatedTarget).data('linea');
        $("#descripcion1").val(product);

        var product2 = $(e.relatedTarget).data('id_nucleo_academico');
        $("#id_nucleo_academico1").val(product2);
            
        var product3 = $(e.relatedTarget).data('codigo');
        $("#codigo1").val(product3);

        var product4 = $(e.relatedTarget).data('id_linea');
        $("#id_linea_investigacion").val(product4);
        
        var product5 = $(e.relatedTarget).data('especificacion');
        $("#especificacion1").val(product5);

        $('.chosen-select').chosen({width: "100%"});
        $('#id_nucleo_academico1').trigger('chosen:updated');


    });

    
</script>



<script>
$('#myModal3').on('show.bs.modal', function(e) {
    
  var product = $(e.relatedTarget).data('linea1');
  $("#descripcion2").val(product);

  var product2 = $(e.relatedTarget).data('id_nucleo_academico1');
  $("#id_nucleo_academico2").val(product2);
    
  var product3 = $(e.relatedTarget).data('codigo1');
  $("#codigo2").val(product3);

  var product4 = $(e.relatedTarget).data('id_linea1');
  $("#id_linea_investigacion1").val(product4);
  
  var product5 = $(e.relatedTarget).data('especificacion1');
  $("#especificacion2").val(product5);

});



</script>

    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<!-- modal ver --> 
<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form3").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    //form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>





</body>
</html>
