<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Tipo de Programa | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  style chosen -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

 


</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-9">
                <h2>Editar Tipo de Programa</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                        Datos Academicos
                        </li>
                        <li class="breadcrumb-item">
                        Programas
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Editar Tipo de Programa</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-3">
                    <div class="title-action">
                    <?php if($_SESSION['Agregar']==true){?>

                    <!-- boton agregar-->
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                            <i class="fa fa-plus"></i> Agregar Tipo
                            </button> 
                    <?php } ?>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Lista de Tipo de Programa Registrados</h5>
                       
                        <div class="ibox-tools">
                        


                        </div>
                    </div>
                    
                    <div class="ibox-content">
                    <div id="respuesta"><?php echo $this->mensaje; ?></div>

                        <div class="table-responsive">
                        
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Tipo de Programa</th>
                        <th>Codigo</th>
                        <th>Nivel de Programa</th>
                        <th>Acción</th>
                    </tr>
                    </thead >
                    <tbody id="tbody-tipo">
                    <?php include_once 'models/datosacademicos/programatipo.php';
                            foreach($this->tipos as $row){
                                $programa_tipo= new Tipo();
                                $programa_tipo=$row;?>
                    <tr id ="fila-<?php echo $programa_tipo->id_programa_tipo; ?>" class="gradeX">
                        <td><?php echo $programa_tipo->descripcion; ?> </td>
                        <td><?php if($programa_tipo->codigo == NULL){ echo "-"; } else{ echo $programa_tipo->codigo;} ?></td>
                        <td><?php echo $programa_tipo->descripcion_nivel; ?></td>
                        <td> 
                        <?php if($_SESSION['Editar']==true){?>

                            <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal" data-id_tipo="<?php echo $programa_tipo->id_programa_tipo;?>" data-tipo="<?php echo $programa_tipo->descripcion;?>" data-codigo="<?php echo $programa_tipo->codigo;?>" data-id_nivel="<?php echo $programa_tipo->id_programa_nivel; ?>" data-nivel="<?php echo $programa_tipo->descripcion_nivel;?>"><i class="fa fa-edit"></i> Editar</a>&nbsp;
                            
                        <?php } 
                        if($_SESSION['Consultar']==true){?>
                            <a class="btn btn-outline btn-primary" href="#myModal3" role="button" data-toggle="modal" data-id_tipo1="<?php echo $programa_tipo->id_programa_tipo;?>" data-tipo1="<?php echo $programa_tipo->descripcion;?>" data-codigo1="<?php echo $programa_tipo->codigo;?>" data-id_nivel1="<?php echo $programa_tipo->id_programa_nivel; ?>" data-nivel1="<?php echo $programa_tipo->descripcion_nivel;?>"><i class="fa fa-search"></i> Ver</a>&nbsp;
                        <?php } 
                         if($_SESSION['Eliminar']==true){?>

                            <a class="btn btn-outline btn-danger" href="<?php echo constant('URL') . 'programatipo/removerTipo/' . $programa_tipo->id_programa_tipo;?>" role="button" onclick="return confirm('¿Está seguro que desea Eliminar un TIpo de Programa de Formación?');"><i class="fa fa-trash"></i> Remover</a> &nbsp;
                         <?php } ?>
                            <!-- <button class="btn btn-outline btn-danger bEliminar" data-id="<?php echo $programa_tipo->id_programa_tipo;?>"><i class="fa fa-trash"></i> Eliminar</button> -->
                        </td>
                    </tr>
                            <?php }?>
                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
<!-- ///////////////Modal Agregar////////////////// -->
<div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Registrar Tipo de Programa</h4>
                                            <small class="font-bold">Se refiere al registro del tipo de programa académico correspondiente a un programa de formación. <br>(Licenciado en sistemas, arquitecto, Técnico Superior Universitario en Informática etc.).
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >  
                                        <div class="ibox">
                        <div class="ibox-content">    
                            <form id="form" method="post" action="<?php echo constant('URL');?>programatipo/registrarTipo" class="wizard-big">
                                <h1>Tipo de Programa</h1>
                                <fieldset>
                                    <h2>Información del Tipo de Programa</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                        <div class="form-group">
                                                <label>Tipo de Programa <span style="color: red;">*</span></label>
                                                <input id="descripcion" name="descripcion" type="text" placeholder="Ingrese la descripción del tipo de programa" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                                <label>Código <i>(Opcional)</i></label>
                                                <input id="codigo" name="codigo" type="text" placeholder="Ingrese el código" maxlength="5"class="form-control">
                                            </div>
                                            <div class="form-group">
                                            <label>Nivel de Programa <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige el nivel..." name="id_programa_nivel" id="id_programa_nivel">
                                        
                                                    <option selected="selected" value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/programanivel.php';
                                                        foreach($this->niveles as $row){
                                                        $programa_nivel=new ProgramaNivel();
                                                        $programa_nivel=$row;?> 
                                                        <option value="<?php echo $programa_nivel->id_programa_nivel;?>"><?php echo $programa_nivel->descripcion;?></option>
                                                        <?php }?>
                                                </select>  
                                                
                                            </div>
                                            <div class="form-group">
                                            <br clear="all"><br clear="all"><br clear="all">
                                            </div>
                                            <div class="form-group">
                                            <br clear="all"><br clear="all"><br clear="all">
                                            </div>
                                            <input type="hidden" name="registrar">
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset> 
                            </form>
                        </div>
                    </div>                    
                </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                         <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                </div>
        </div>
    </div>
</div>        
<!--////////////////////////////////-->
         <!-- ///////////////Modal Editar////////////////// -->

<div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Editar Tipo de Programa</h4>
                                            <small class="font-bold">Se refiere a la actualización de un tipo de programa académico correspondiente a un programa de formación. <br>(Licenciado en sistemas, arquitecto, Técnico Superior Universitario en Informática etc.)
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            
                        <form id="form2" action="<?php echo constant('URL');?>programatipo/editarTipo"  method="post" class="wizard-big" >
                                <h1>tipo Académico</h1>
                                <fieldset>
                                    <h2>Información del Tipo de Programa</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                            <input id="id_programa_tipo" name="id_programa_tipo" type="hidden" class="form-control">

                                                <label>Tipo de Programa <span style="color: red;">*</span></label>
                                                <input id="descripcion2" name="descripcion2" type="text" placeholder="Ingrese la descripción del tipo" maxlength="70" class="form-control required">
                                        </div>

                                        <div class="form-group">
                                                <label>Código <i>(Opcional)</i></label>
                                                <input id="codigo2" name="codigo2" type="text" placeholder="Ingrese el código" maxlength="5"class="form-control">
                                            </div>
                                        <div class="form-group">
                                                <label>Nivel de Programa <span style="color: red;">*</span></label>
                                                
                                                <select class="form-control required m-b" tabindex="2" data-placeholder="Elige el nivel..." name="id_programa_nivel2"  id="id_programa_nivel2">
                                        
                                                    <option value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/programanivel.php';
                                                        foreach($this->niveles as $row){
                                                        $programa_nivel=new ProgramaNivel();
                                                            $programa_nivel=$row;?> 
                                                            <option value="<?php echo $programa_nivel->id_programa_nivel;?>"><?php echo $programa_nivel->descripcion;?></option>
                                                            <?php }?>
                                                </select> 
                                                
                                            </div>
                                            
                                            <input type="hidden" name="registrar2">
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                               

                                
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>       
                    </div>

<!-- /////////////////////////////// -->

<!-- ///////////////Modal Ver////////////////// -->

<div class="modal inmodal fade " style="width: 100%;" id="myModal3" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Ver Tipo de Programa</h4>
                                           <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            
                            <form id="form3"  class="wizard-big">
                                <h1>Tipo de Programa</h1>
                                <fieldset>
                                    <h2>Información del Tipo de Programa</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                            <input id="id_programa_tipo1" disabled type="hidden" class="form-control">

                                                <label>Tipo de Programa <span style="color: red;">*</span></label>
                                                <input id="descripcion3" disabled type="text" placeholder="Ingrese la descripción del tipo" maxlength="70" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                                <label>Código <i>(Opcional)</i></label>
                                                <input id="codigo3" type="text" disabled placeholder="Ingrese el código" maxlength="5"class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Nivel de Programa <span style="color: red;">*</span></label>
                                                
                                                <select class="form-control required m-b " disabled id="id_programa_nivel3">
                                        
                                                    <option value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/programanivel.php';
                                                        foreach($this->niveles as $row){
                                                        $programa_nivel=new ProgramaNivel();
                                                            $programa_nivel=$row;?> 
                                                            <option value="<?php echo $programa_nivel->id_programa_nivel;?>"><?php echo $programa_nivel->descripcion;?></option>
                                                            <?php }?>
                                                </select> 
                                                
                                            </div>
                                            
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                               

                                
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>       
                    </div>




    <?php require 'views/footer.php'; ?>

    <!-- ajax de eliminarGrado -->
    <script type="text/javascript" src="<?php echo constant('URL');?>src/js/datos_academicos/eliminarProgramaTipo.js"></script>

  


   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    <!-- Chosen -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>

<!-- 
    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");  
    $("#re").addClass("active");
});   
</script>-->


<script>
    $('#myModal1').on('show.bs.modal', function(e) {
        $('.chosen-select').chosen({width: "100%"});
        /*var elem = document.querySelector('.js-switch_4');
        var switchery = new Switchery(elem, { color: '#1AB394' });*/

    });

</script>
<script>
$('#myModal2').on('show.bs.modal', function(e) {
    $('.chosen-select').chosen({width: "100%"});


  var product = $(e.relatedTarget).data('tipo');
  $("#descripcion2").val(product);

  var product2 = $(e.relatedTarget).data('codigo');
  $("#codigo2").val(product2);

  var product3 = $(e.relatedTarget).data('id_nivel');
  $("#id_programa_nivel2").val(product3);
   // alert(product2);
  var product4 = $(e.relatedTarget).data('id_tipo');
  $("#id_programa_tipo").val(product4);
  
});
</script>

<script>
$('#myModal3').on('show.bs.modal', function(e) {
    
  var product = $(e.relatedTarget).data('tipo1');
  $("#descripcion3").val(product);

  var product2 = $(e.relatedTarget).data('codigo1');
  $("#codigo3").val(product2);

  var product3 = $(e.relatedTarget).data('id_nivel1');
  $("#id_programa_nivel3").val(product3);
    
  var product4 = $(e.relatedTarget).data('id_tipo1');
  $("#id_programa_tipo1").val(product4);
  
});
</script>

    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<!-- modal ver --> 
<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form3").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    //form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>





</body>
</html>
