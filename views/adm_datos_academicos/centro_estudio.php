<!--
*
*  INSPINIA - centro de estudio
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Centro de Estudio | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

     <!--  style chosen lo lo puse -->
     <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

 


</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-9">
                <h2>Editar Centro de Estudio</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            Datos Académicos
                        </li>
                        <li class="breadcrumb-item">
                            Centros de Estudio
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Editar Centro de estudio</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-3">
                    <div class="title-action">
                    <?php if($_SESSION['Agregar']==true){?>

                        <!-- boton agregar-->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                        <i class="fa fa-plus"></i> Agregar Centro de Estudio
                        </button> 
                    <?php } ?>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">                                                                                                                                                                                                                                                                                                                                     


                <div class="ibox ">                                                                                                                                                                 
                    <div class="ibox-title">
                        <h5>Lista de Centros de estudios Registrados</h5>
                        <div class="ibox-tools">                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                                                                                                                        


                        </div>
                    </div>
                    
                    <div class="ibox-content">
                    <div id="respuesta"><?php echo $this->mensaje; ?></div>

                        <div class="table-responsive">
                        
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Codigo del Centro</th>
                        <th>Centro de Estudio</th>
                        <th>Estatus</th>
                        <th>Area Opsu Asociada</th>
                        <th>Acciones</th>
<!-- <th>Código</th> como hago aqui con la foranea q esta en esta tabla llamada id_area_conocimiento_opsu -->
                        
                    </tr>
                    </thead >
                    <tbody id="tbody-centroestudio">
                    <?php include_once 'models/datosacademicos/centroestudio.php'; 
                   // lo comente  $i=0;
                            foreach($this->centros as $row){
                                $centroestudio= new CentroEstudio();
                                $centroestudio=$row;?>
                    <tr id ="fila-<?php echo $centroestudio->id_centro_estudio; ?>" class="gradeX">
                     <td><?php echo $centroestudio->codigo; ?></td>
                        <td><?php echo $centroestudio->descripcion; ?></td>
                        <td style="text-align: center;"><?php if($centroestudio->estatus=='Activo'){ echo '<span class="label label-primary" style="font-size: 12px;"> '.$centroestudio->estatus.'</span>';} else{ echo '<span class="label label-danger" style="font-size: 12px;"> '.$centroestudio->estatus.'</span>';}?></td>
                        <td><?php echo $centroestudio->descripcion_opsu; ?></td>
                        
                        
                        <td> 

                        <?php if($_SESSION['Editar']==true){?>

                            <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal" data-id_centro="<?php echo $centroestudio->id_centro_estudio;?>"  data-centro="<?php echo $centroestudio->descripcion;?>"  data-codigo="<?php echo $centroestudio->codigo;?>" data-id_area="<?php echo $centroestudio->id_area_conocimiento_opsu;?>" data-areaopsu="<?php echo $centroestudio->descripcion_opsu;?>" data-estatus="<?php echo $centroestudio->estatus;?>"><i class="fa fa-edit"></i> Editar</a>&nbsp;
                        <?php } 
                            if($_SESSION['Consultar']==true){?>

                            <a class="btn btn-outline btn-primary" href="#myModal3" role="button" data-toggle="modal" data-id_centro1="<?php echo $centroestudio->id_centro_estudio;?>" data-centro1="<?php echo $centroestudio->descripcion;?>" data-codigo1="<?php echo $centroestudio->codigo;?>" data-id_area1="<?php echo $centroestudio->id_area_conocimiento_opsu;?>" data-areaopsu1="<?php echo $centroestudio->descripcion_opsu;?>" data-estatus1="<?php echo $centroestudio->estatus;?>"><i class="fa fa-search"></i> Ver</a>&nbsp;
<!--como incluyo mas a class por las variables de estatus y la foranea-->
                            <?php } ?>
                            <!-- <a class="btn btn-outline btn-danger" href="<?php echo constant('URL') . 'centro_estudio/deleteCentro/' . $centroestudio->id_centro_estudio;?>" role="button"> Remover</a> &nbsp; -->
                            <!-- <button class="btn btn-outline btn-danger bEliminar" data-id="<?php echo $centroestudio->id_centro_estudio;?>"><i class="fa fa-trash"></i> Eliminar</button>-->
                        </td>
                    </tr>
                            <?php }?>
                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
<!-- ///////////////Modal Agregar////////////////// -->
        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Registrar Centro de Estudio</h4>
                                            <small class="font-bold">Permite registrar los centros de estudio correspondiente a un programa de formación. 
                                
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox">
                        
                        <div class="ibox-content">
                            
                            <form id="form" method="post" action="<?php echo constant('URL');?>centro_estudio/registrarCentro" class="wizard-big">
                                <h1>Centro de estudio</h1>
                                <fieldset>
                                    <h2> Informacion del Centro de estudio</h2>
                                    <div class="row">
                                        <div class="col-lg-8">

                                        <div class="form-group">
                                                <label>Area Conocimiento Opsu <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige el Área..." name="id_area_conocimiento_opsu" id="id_area_opsu">
                                        
                                                    <option selected="selected" value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/areaopsu.php';
                                                        foreach($this->areasopsu as $row){
                                                        $area_opsu=new AreaOpsu();
                                                            $area_opsu=$row;?> 
                                                            <option value="<?php echo $area_opsu->id_area_conocimiento_opsu;?>"><?php echo $area_opsu->descripcion;?></option>
                                                            <?php }?>
                                                </select> 
                                                
                                            </div>

                                            <div class="form-group">
                                            <label>Centro de estudio <span style="color: red;">*</span></label>
                                                <input id="descripcion" name="descripcion" type="text" class="form-control required" placeholder="Ingrese el nombre del centro de estudio">
                                              
                                            </div>

                                            <div class="form-group">
                                                <label>Código<span style="color: red;">*</span></label>
                                                <input id="codigo" name="codigo" type="text" placeholder="Ingrese El Código de Área de Conocimiento OPSU" maxlength="20" class="form-control">
                                            </div>
                                            
                                       
                                    <div class="form-group">
                                                <label>Estatus<span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " name="estatus" id="estatus">
                                                    <option value="0">Seleccione</option>
                                                    <option value="Activo"> Activo</option>
                                                    <option value="Inactivo"> Inactivo</option>
                                                </select>
                                                <!-- <input type="checkbox" class="js-switch_4" name="estatus_check" checked /> -->
                                            </div>

                                            <input type="hidden" name="registrar">
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                            </fieldset>
                               
                            </form>
                        </div>
                    </div>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
         <!-- ///////////////Modal Editar////////////////// -->

<div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Editar Centro de Estudio</h4>
                                            <small class="font-bold">Se refiere a la actualizacion del centro de estudio
 <br>
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            
                            <form id="form2" method="post" action="<?php echo constant('URL');?>centro_estudio/editarCentro" class="wizard-big">
                                <h1>Centro de Estudio</h1>
                                <fieldset>
                                    <h2>Información del Centro de Estudio</h2>
                                    <div class="row">
                                      <div class="col-lg-8">
                                        <div class="form-group">



                                        <input id="id_centro_estudio" name="id_centro_estudio" type="hidden" class="form-control">                
                                        <div class="form-group">
                                                <label>Area opsu <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige el Área..." name="id_area_conocimiento_opsu2" id="id_area_conocimiento_opsu2">
                                                            <option value="">Seleccione</option> 
                                                            <?php include_once 'models/datosacademicos/areaopsu.php';
                                                                foreach($this->areasopsu as $row){
                                                                $area_opsu=new AreaOpsu();
                                                                    $area_opsu=$row;?> 
                                                                    <option value="<?php echo $area_opsu->id_area_conocimiento_opsu;?>"><?php echo $area_opsu->descripcion;?></option>
                                                                    <?php }?>
                                                        </select> 
                                                        </div>
                                             <div class="form-group">
                                                <label>Código <span style="color: red;">*</span></label>
                                                <input id="codigo1" name="codigo1" type="text" placeholder="Ingrese el código del Área de Conocimiento OPSU" maxlength="20" class="form-control required">
                                            </div>

                                            <div class="form-group">
                                        <label>Centro de Estudio <span style="color: red;">*</span></label>
                                                <input id="descripcion2" name="descripcion2" type="text" placeholder="Ingrese la descripción del centro" maxlength="70" class="form-control required">
                                            </div>
                                            </div>

                                         
                                           <!-- agrege este bloque status  -->                           
                                           <div class="form-group">
                                           <label>Estatus<span style="color: red;">*</span></label>
                                           <select class="form-control required m-b " name="estatus1" id="estatus1">
                                               <option value="">Seleccione</option>
                                               <option value="Activo"> Activo</option>
                                               <option value="Inactivo"> Inactivo</option>
                                           </select>
                                           <!-- <input type="checkbox" class="js-switch_4" name="estatus_check" checked /> -->
                                       </div>
                   


                                            <input type="hidden" name="registrar2">
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                               

                                
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>       
                    </div>


         <!-- ///////////////Modal Ver////////////////// -->

<div class="modal inmodal fade " style="width: 100%;" id="myModal3" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Ver centro de estudio</h4>
                                            <small class="font-bold">Se refiere a la visualizacion de un centro de estudio especifico
                                           
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            
                            <form id="form3" class="wizard-big">
                                <h1>Centro de Estudio</h1>
                                <fieldset>
                                    <h2>Información del Centro de Estudio</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                        <div class="form-group">


                                            <input id="id_centro_estudio1" disabled type="hidden" class="form-control">

                                            <div class="form-group">
                                                <label>area opsu <span style="color: red;">*</span></label>
                                                
                                                <select class="form-control required m-b" disabled id="id_area_conocimiento_opsu3">
                                                            
                                                    <option value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/areaopsu.php';
                                                        foreach($this->areasopsu as $row){
                                                        $area_opsu=new AreaOpsu();
                                                            $area_opsu=$row;?> 
                                                            <option value="<?php echo $area_opsu->id_area_conocimiento_opsu;?>"><?php echo $area_opsu->descripcion;?></option>
                                                            <?php }?>
                                                </select> 
                                                </div>
                                            <!---->
                                            <div class="form-group">
                                                <label>Código <span style="color: red;">*</span> </label>
                                                <input id="codigo3"  type="text" disabled placeholder="Ingrese el código" maxlength="8" class="form-control">
                                            </div>
                                       
                                            <div class="form-group">
                                                <label>Centro de Estudio <span style="color: red;">*</span></label>
                                                <input id="descripcion3" disabled type="text" placeholder="Ingrese la descripción del centro" maxlength="70" class="form-control required">
                                            </div>
                                            </div>              
                                        
                                            <div class="form-group">
                                        <label>Estatus<span style="color: red;">*</span></label>
                                        <select class="form-control required m-b " name="estatus2" id="estatus2" disabled id="estatus">
                                            <option value="">Seleccione</option>
                                            <option value="Activo"> Activo</option>
                                            <option value="Inactivo"> Inactivo</option>
                                        </select>
                                        <!-- <input type="checkbox" class="js-switch_4" name="estatus_check" checked /> -->
                                    </div>
                                    </div>

                                   
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                               

                                
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>       
                    </div>




    <?php require 'views/footer.php'; ?>

    <!-- ajax de eliminarGrado -->
    <script type="text/javascript" src="<?php echo constant('URL');?>src/js/datos_academicos/eliminarCentro.js"></script>

  

    
   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    <!-- Chosen -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>

<!-- 
    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");  
    $("#re").addClass("active");
});   
</script>-->


<script>
    $('#myModal1').on('show.bs.modal', function(e) {
        $('.chosen-select').chosen({width: "100%"});
        /*var elem = document.querySelector('.js-switch_4');
        var switchery = new Switchery(elem, { color: '#1AB394' });*/

    });

</script>
<script>
$('#myModal2').on('show.bs.modal', function(e) {
    $('.chosen-select').chosen({width: "100%"});


  var product = $(e.relatedTarget).data('centro');
  $("#descripcion2").val(product);

  var product2 = $(e.relatedTarget).data('id_area');
  $("#id_area_conocimiento_opsu2").val(product2);
      //alert(product2);

  var product3 = $(e.relatedTarget).data('codigo');
  $("#codigo1").val(product3);

  var product4 = $(e.relatedTarget).data('id_centro');
  $("#id_centro_estudio").val(product4);

  var product5 = $(e.relatedTarget).data('estatus');
  $("#estatus1").val(product5);

    $('.chosen-select').chosen({width: "100%"});
    $('#id_area_conocimiento_opsu2').trigger('chosen:updated');


});
</script>

<script>
$('#myModal3').on('show.bs.modal', function(e) {
    
  var product = $(e.relatedTarget).data('centro1');
  $("#descripcion3").val(product);

  var product2 = $(e.relatedTarget).data('id_area1');
  $("#id_area_conocimiento_opsu3").val(product2);
    
  var product3 = $(e.relatedTarget).data('codigo1');
  $("#codigo3").val(product3);

  var product4 = $(e.relatedTarget).data('id_centro1');
  $("#id_centro_estudio1").val(product4);

  var product5 = $(e.relatedTarget).data('estatus1');
  $("#estatus2").val(product5);
  
});
</script>

    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<!-- modal ver --> 
<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form3").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    //form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>





</body>
</html>
