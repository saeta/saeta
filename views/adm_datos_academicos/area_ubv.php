<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Área Conocimiento UBV | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  style chosen -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">


    <!--  style switch -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/switchery/switchery.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">



</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-9">
                <h2>Editar Área Conocimiento UBV</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            Datos Académicos
                        </li>
                        <li class="breadcrumb-item">
                            Áreas de Conocimiento
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Editar Área Conocimiento UBV</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-3">
                    <div class="title-action">
                    <?php if($_SESSION['Agregar']==true){?>

                     <!-- boton agregar-->
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                            <i class="fa fa-plus"></i> Agregar Área UBV
                            </button> 
                    <?php } ?>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Lista de Área Conocimiento UBV registradas</h5>
                       
                        <div class="ibox-tools">
                       


                        </div>
                    </div>
                    
                    <div class="ibox-content">
                    <div id="respuesta"><?php echo $this->mensaje; ?></div>

                        <div class="table-responsive">
                        
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Área Conocimiento UBV</th>
                        <th>Código INE</th>
                        <th>Estatus</th>
                        <th>Área Conocimiento OPSU Asociada</th>
                        <th>Acción</th>
                    </tr>
                    </thead >
                    <tbody id="tbody-areasubv">
                    <?php include_once 'models/datosacademicos/areasubv.php';
                            foreach($this->areasubv as $row){
                                $areaubv= new Areaubv();
                                $areaubv=$row;?>
                    <tr id ="fila-<?php echo $areaubv->id_area_conocimiento_ubv; ?>" class="gradeX">
                        <td><?php echo $areaubv->descripcion; ?> </td>
                        <td><?php echo $areaubv->codigo;?></td>
                        <td style="text-align: center;"><?php if($areaubv->estatus=='Activo'){ echo '<span class="label label-primary" style=" font-size: 12px;"> '.$areaubv->estatus.'</span>';} else{ echo '<span class="label label-danger" style="font-size: 12px;"> '.$areaubv->estatus.'</span>';}?></td>
                        <td><?php echo $areaubv->descripcion_opsu; ?></td>
                       
                        <td> 
                        <?php if($_SESSION['Editar']==true){?>

                            <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal" data-id_area_ubv="<?php echo $areaubv->id_area_conocimiento_ubv;?>" data-area_ubv="<?php echo $areaubv->descripcion;?>"  data-id_area_conocimiento_opsu="<?php echo $areaubv->id_area_conocimiento_opsu; ?>" data-area_ubv="<?php echo $areaubv->descripcion_opsu;?>" data-codigo="<?php echo $areaubv->codigo;?>" data-estatus="<?php echo $areaubv->estatus;?>"><i class="fa fa-edit"></i> Editar</a>&nbsp;
                        <?php } 
                        if($_SESSION['Consultar']==true){?>

                            <a class="btn btn-outline btn-primary" href="#myModal3" role="button" data-toggle="modal" data-id_area_ubv1="<?php echo $areaubv->id_area_conocimiento_ubv;?>" data-area_ubv1="<?php echo $areaubv->descripcion;?>"  data-id_area_conocimiento_opsu1="<?php echo $areaubv->id_area_conocimiento_opsu; ?>" data-area_ubv1="<?php echo $areaubv->descripcion_opsu;?>" data-codigo1="<?php echo $areaubv->codigo;?>" data-estatus1="<?php echo $areaubv->estatus;?>"><i class="fa fa-search"></i> Ver</a>&nbsp;
                            
                        <?php } ?>
                            <!-- <button class="btn btn-outline btn-info bEstatus" data-id1="<?php echo $areaubv->id_area_conocimiento_ubv;?>" data-estatus2="<?php echo $areaubv->estatus;?>"><i class="fa fa-pencil"></i> Cambiar Estatus</button> -->

                            <!-- <a class="btn btn-outline btn-danger" href="<?php echo constant('URL') . 'area_ubv/deleteArea_ubv/' . $areaubv->id_area_conocimiento_ubv;?>" role="button"> Remover</a> &nbsp; -->
                            <!-- <button class="btn btn-outline btn-danger bEliminar" data-id="<?php echo $areaubv->id_area_conocimiento_ubv;?>"><i class="fa fa-trash"></i> Eliminar</button> -->
                        </td>
                    </tr>
                            <?php }?>
                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
<!-- ///////////////Modal Agregar////////////////// -->
        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Registrar Área de Conocimiento UBV</h4>
                                            <small class="font-bold">Se refiere al registro del Área de Conocimiento UBV o titulo académico correspondiente a un programa de formación. <br>(Licenciado en sistemas, arquitecto, Técnico Superior Universitario en Informática etc.).
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox">
                        
                        <div class="ibox-content">
                            
                            <form id="form" method="post" action="<?php echo constant('URL');?>area_ubv/registrar" class="wizard-big">
                                <h1>Área de Conocimiento UBV</h1>
                                <fieldset>
                                    <h2>Información del Área de Conocimiento UBV</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                        <div class="form-group">
                                                <label>Asociar con un Área de Conocimiento OPSU<span style="color: red;">*</span></label>
                                                
                                                <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Choose a Country..." name="id_area_conocimiento_opsu" id="id_area_conocimiento_opsu">
                                                    <option value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/areaopsu.php'; 
                                                            foreach($this->areasopsu as $row){
                                                                $areaopsu= new AreaOpsu();
                                                                $areaopsu=$row;
                                                                ?>
                                                            <option value="<?php echo $areaopsu->id_area_conocimiento_opsu;?>"><?php echo $areaopsu->descripcion;?></option>
                                                            <?php }?>
                                                </select> 

                                            </div>
                                            <div class="form-group">
                                                <label>Área de Conocimiento UBV <span style="color: red;">*</span></label>
                                                <input id="descripcion" name="descripcion" type="text" placeholder="Ingrese la descripción del Área de Conocimiento UBV" maxlength="70" class="form-control required">
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Código INE <span style="color: red;">*</span></label>
                                                <input id="codigo" name="codigo" type="text" placeholder="Ingrese el código INE" maxlength="5" class="form-control required number">
                                            </div>
                                            <div class="form-group">
                                            
                                                <label>Estatus<span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " name="estatus" id="estatus">
                                                    <option value="">Seleccione</option>
                                                    <option value="Activo"> Activo</option>
                                                    <option value="Inactivo"> Inactivo</option>
                                                </select>
                                                <!-- <input type="checkbox" class="js-switch_4" id="sw" name="estatus" checked  />
                                                <input type="checkbox" class="js-switch_4" name="estatus_check" checked /> -->
                                            </div>
                                            
                                            <div class="form-group">
                                                <input type="hidden" name="registrar">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                               

                                
                            </form>
                        </div>
                    </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
         <!-- ///////////////Modal Editar////////////////// -->

<div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Editar Área de Conocimiento UBV</h4>
                                            <small class="font-bold">Se refiere a la actualización de un area_ubv o titulo académico correspondiente a un programa de formación. <br>(Licenciado en sistemas, arquitecto, Técnico Superior Universitario en Informática etc.)
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            
                            <form id="form2" method="post" action="<?php echo constant('URL');?>area_ubv/editar" class="wizard-big">
                                <h1>Área de Conocimiento UBV</h1>
                                <fieldset>
                                    <h2>Información del Área de Conocimiento UBV</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                        <div class="form-group">
                                                <label>Asociar con un Área de Conocimiento OPSU<span style="color: red;">*</span></label>
                                                <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige el Área..." name="id_area_conocimiento_opsu1" id="id_area_conocimiento_opsu1">
                                                    <option value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/areaopsu.php'; 
                                                        
                                                            foreach($this->areasopsu as $row){
                                                                $areaopsu= new AreaOpsu();
                                                                $areaopsu=$row;
                                                                ?>
                                                            <option value="<?php echo $areaopsu->id_area_conocimiento_opsu;?>"><?php echo $areaopsu->descripcion;?></option>
                                                            <?php }?>
                                                </select> 
                                                
                                                
                                            </div>
                                            <div class="form-group">
                                            <input id="id_area_conocimiento_ubv" name="id_area_conocimiento_ubv" type="hidden" class="form-control">

                                                <label>Área de Conocimiento UBV <span style="color: red;">*</span></label>
                                                <input id="descripcion1" name="descripcion1" type="text" placeholder="Ingrese la descripción del Área de Conocimiento UBV" maxlength="70" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                                <label>Código INE <span style="color: red;">*</span></label>
                                                <input id="codigo1" name="codigo1" type="text" placeholder="Ingrese el código INE" maxlength="5"class="form-control required number">
                                            </div>
                                            <div class="form-group">
                                                <label>Estatus<span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " name="estatus1" id="estatus1">
                                                    <option value="">Seleccione</option>
                                                    <option value="Activo"> Activo</option>
                                                    <option value="Inactivo"> Inactivo</option>
                                                </select>
                                            </div>
                                            
                                            
                                            <input type="hidden" name="registrar2">
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                    
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>       
                    </div>

<!-- /////////////////////////////// -->

<!-- ///////////////Modal Ver////////////////// -->

<div class="modal inmodal fade " style="width: 100%;" id="myModal3" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Ver Área de Conocimiento UBV</h4>
                                            <small class="font-bold">Se refiere a la actualización de un area_ubv o titulo académico correspondiente a un programa de formación. <br>(Licenciado en sistemas, arquitecto, Técnico Superior Universitario en Informática etc.)
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            
                            <form id="form3"  class="wizard-big">
                                <h1>Área de Conocimiento UBV</h1>
                                <fieldset>
                                    <h2>Información del Área de Conocimiento UBV</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                            <input id="id_area_conocimiento_ubv1" disabled type="hidden" class="form-control">

                                                <label>Área de Conocimiento UBV <span style="color: red;">*</span></label>
                                                <input id="descripcion2" disabled type="text" placeholder="Ingrese la descripción del Área de Conocimiento UBV" maxlength="70" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                                <label>Código INE <span style="color: red;">*</span></label>
                                                <input id="codigo2"  type="text" disabled placeholder="Ingrese el código INE" maxlength="5"class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Estatus<span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " disabled id="estatus2">
                                                    <option value="">Seleccione</option>
                                                    <option value="Activo"> Activo</option>
                                                    <option value="Inactivo"> Inactivo</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Asociar con un Área de Conocimiento OPSU<span style="color: red;">*</span></label>
                                                
                                                <select class="form-control required m-b" disabled id="id_area_conocimiento_opsu2">
                                                            
                                                    <option value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/areaopsu.php'; 
                                                        
                                                            foreach($this->areasopsu as $row){
                                                                $areaopsu= new AreaOpsu();
                                                                $areaopsu=$row;
                                                                ?>
                                                            <option value="<?php echo $areaopsu->id_area_conocimiento_opsu;?>"><?php echo $areaopsu->descripcion;?></option>
                                                            <?php }?>
                                                </select> 
                                            </div>
                                            
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                               

                                
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>       
                    </div>




    <?php require 'views/footer.php'; ?>

       
    <!-- ajax de eliminar -->
    <script type="text/javascript" src="<?php echo constant('URL');?>src/js/datos_academicos/eliminarAreaubv.js"></script>
    <script type="text/javascript" src="<?php echo constant('URL');?>src/js/datos_academicos/estatusAreaubv.js"></script>

  

  
   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
  
    <!-- Switchery -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/switchery/switchery.js"></script>

        <!-- Chosen -->
        <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>



<!-- 
    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");  
    $("#re").addClass("active");
});   
</script>-->



<script>

    
    $('#myModal1').on('show.bs.modal', function(e) {
        $('.chosen-select').chosen({width: "100%"});
        /*
            var elem = document.querySelector('.js-switch_4');
            var switchery = new Switchery(elem, { color: '#1AB394' });
            */
            //alert(switchery);
    });
    //$('.js-switch_4').empty();

    $('#myModal2').on('show.bs.modal', function(e) {
            
        var product = $(e.relatedTarget).data('area_ubv');
        $("#descripcion1").val(product);

        var product2 = $(e.relatedTarget).data('id_area_conocimiento_opsu');
        $("#id_area_conocimiento_opsu1").val(product2);
            
        var product3 = $(e.relatedTarget).data('codigo');
        $("#codigo1").val(product3);

        var product4 = $(e.relatedTarget).data('id_area_ubv');
        $("#id_area_conocimiento_ubv").val(product4);
        
        var product5 = $(e.relatedTarget).data('estatus');
        $("#estatus1").val(product5);

        $('.chosen-select').chosen({width: "100%"});
        $('#id_area_conocimiento_opsu1').trigger('chosen:updated');


    });

    
</script>



<script>
$('#myModal3').on('show.bs.modal', function(e) {
    
  var product = $(e.relatedTarget).data('area_ubv1');
  $("#descripcion2").val(product);

  var product2 = $(e.relatedTarget).data('id_area_conocimiento_opsu1');
  $("#id_area_conocimiento_opsu2").val(product2);
    
  var product3 = $(e.relatedTarget).data('codigo1');
  $("#codigo2").val(product3);

  var product4 = $(e.relatedTarget).data('id_area_ubv1');
  $("#id_area_conocimiento_ubv1").val(product4);
  
  var product5 = $(e.relatedTarget).data('estatus1');
  $("#estatus2").val(product5);

});



</script>

    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<!-- modal ver --> 
<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form3").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    //form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>





</body>
</html>
