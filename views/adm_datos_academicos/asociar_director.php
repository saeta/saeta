                <!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Asociar | SIDTA</title>


 
    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">


</head>

<body>
 
   <?php require 'views/header.php'; ?>
   

   <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-8">
                <h2>Asociar Alto Nivel a Centros de Estudio</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            Admin. del Sistema
                        </li>
                        <li class="breadcrumb-item">
                            Datos Académicos
                        </li>
                        <li class="breadcrumb-item">
                            Centro de Estudios
                        </li>
                        <li class="breadcrumb-item active">
                            <a href="<?php echo constant('URL');?>centro_estudio/viewAssoc"><strong>Asociar Alto Nivel</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-4">
                    <div class="title-action">
                    <?php if($_SESSION['Agregar']==true){?>

                    <!-- boton agregar-->
                        <a href="<?php echo constant ('URL');?>centro_estudio/viewAdd"><button type="button" class="btn btn-primary">
                            <i class="fa fa-plus"></i> Asociar
                        </button> </a>  
                    <?php } ?>  
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>SIDTA</h5>
                        <div class="ibox-tools">

                 

                        </div>
                    </div>
                    <div class="ibox-content">
                    <?php echo $this->mensaje;?>
                        <!--  contenido -->
                        <div class="table-responsive">
                        
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                        <tr>
                            <th><i class="fa fa-address-card"></i>  Identificación</th>
                            <th><i class="fa fa-user-circle"></i> Persona</th>
                            <th><i class="fa fa-user-circle"></i> Perfil</th>
                            <th><i class="fa fa-institution"></i> Centro de Estudio</th>
                            <th><i class="fa fa-circle"></i> Estatus</th>
                            <th><i class="fa fa-bars"></i> Acción</th>
                        </tr>
                        </thead >
                        <tbody id="tbody-aldeapro">
                        <?php 
                            foreach($this->asociaciones as $row){
                                $asociacion= new Estructura();
                                $asociacion=$row;?>
                        <tr id ="fila-<?php echo $asociacion->id_persona_centro_estudio; ?>" class="gradeX">
                            <td><?php echo $asociacion->nacionalidad . "-" .$asociacion->identificacion; ?></td>
                            <td><?php echo ucwords($asociacion->primer_nombre) . " " . ucwords($asociacion->primer_apellido);?></td>
                            <td><?php echo ucwords($asociacion->perfil); ?> </td>
                            <td><?php echo ucwords($asociacion->centro_estudio); ?> </td>
                            <td>
                                <?php if($asociacion->estatus=='Activo'){ 
                                echo '<span class="label label-primary"> '.$asociacion->estatus.'</span>';
                                } else{ 
                                echo '<span class="label label-danger"> '.$asociacion->estatus.'</span>';
                                }?>
                            </td>
                            <td> 
                            <?php if($_SESSION['Editar']==true){?>

                                <a class="btn btn-outline btn-success" href="<?php echo constant('URL') . "centro_estudio/viewEdit/" . $asociacion->id_persona_centro_estudio;?>"><i class="fa fa-edit"></i> Editar</a>
                            <?php } ?>
                            </td>
                        </tr>
                            <?php }?>
                        
                        </tbody>
                       
                        </table>
                            </div>
                        <!--  end contenido -->

                    </div>
                </div>
            </div>
            </div>
        </div>



    <?php require 'views/footer.php'; ?>

     

    <!-- dataTables Scripts -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>

   

</body>
</html>










