<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Nivel de Programa | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

 


</head>

<body>
    <?php require 'views/header.php'; ?>
    

    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-9">
                <h2>Editar Nivel de Programa</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            Datos Académicos
                        </li>
                        <li class="breadcrumb-item">
                            Programas
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Editar Nivel de Programa</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-3">
                    <div class="title-action">
                    <?php if($_SESSION['Agregar']==true){?>

                         <!-- boton agregar-->
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                            <i class="fa fa-plus"></i> Agregar Nivel de Programa
                            </button>
                    <?php } ?> 
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Lista de niveles de programa registrados</h5>
                       
                        <div class="ibox-tools">
                       
                        </div>
                    </div>
                    
                    <div class="ibox-content">
                    <div id="respuesta"><?php echo $this->mensaje; ?></div>

                        <div class="table-responsive">
                        
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                        <tr>
                            <th>Nivel de Programa</th>
                            <th>Estatus</th>
                            <th>Código INE</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody id="tbody-programas">
                    <?php include_once 'models/datosacademicos/programanivel.php';
                            foreach($this->programas as $row){
                                $programa= new ProgramaNivel();
                                $programa=$row;?>
                    <tr id ="fila-<?php echo $programa->id_programa_nivel; ?>" class="gradeX">
                        <td><?php echo $programa->descripcion; ?> </td>
                        <td style="text-align: center;"><?php if($programa->estatus=='Activo'){ echo '<span class="label label-primary" style="font-size: 12px;"> '.$programa->estatus.'</span>';} else{ echo '<span class="label label-danger" style="font-size: 12px;"> '.$programa->estatus.'</span>';}?> </td>
                        <td><?php echo $programa->codigo;?></td>
                        <td> 
                        <?php if($_SESSION['Editar']==true){?>

                            <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal" data-id_programa="<?php echo $programa->id_programa_nivel;?>" data-programa="<?php echo $programa->descripcion;?>" data-codigo="<?php echo $programa->codigo;?>" data-estatus="<?php echo $programa->estatus;?>"><i class="fa fa-edit"></i> Editar</a>&nbsp;

                        <?php } 
                         if($_SESSION['Consultar']==true){?>

                            <a class="btn btn-outline btn-primary" href="#myModal3" role="button" data-toggle="modal" data-id_programa1="<?php echo $programa->id_programa_nivel;?>" data-programa1="<?php echo $programa->descripcion;?>" data-codigo1="<?php echo $programa->codigo;?>" data-estatus1="<?php echo $programa->estatus;?>"><i class="fa fa-search"></i> Ver</a>&nbsp;
                         <?php } ?>
                            <!-- <a class="btn btn-out btn-danger" href="<?php echo constant('URL') . 'programa_nivel/deleteprograma/' . $nivel->id_programa_nivel;?>" role="button"> Remover</a> &nbsp; -->
                            <!-- <button class="btn btn-out btn-danger bEliminar" data-id="<?php echo $nivel->id_programa_nivel;?>"><i class="fa fa-trash"></i> Eliminar</button> -->
                        </td>
                    </tr>
                            <?php }?>
                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
<!-- ///////////////Modal Agregar////////////////// -->
        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Registrar Nivel de Programa</h4>
                                            <small class="font-bold">Permite registrar los niveles de programas
identificados en la UBV para facilitar la consulta y
presentación de la información relacionada.
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox">
                        
                        <div class="ibox-content">
                            
                            <form id="form" method="post" action="<?php echo constant('URL');?>programa_nivel/registrar" class="wizard-big">
                                <h1>Nivel de Programa</h1>
                                <fieldset>
                                    <h2>Información del Nivel de Programa</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <label>Nivel de Programa <span style="color: red;">*</span></label>
                                                <input id="descripcion" name="descripcion" type="text" placeholder="Ingrese la descripción del Nivel de Programa" maxlength="70" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                                <label>Estatus<span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " name="estatus" id="estatus">
                                                    <option value="">Seleccione</option>
                                                    <option value="Activo"> Activo</option>
                                                    <option value="Inactivo"> Inactivo</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Código INE<span style="color: red;">*</span></label>
                                                <input id="codigo" name="codigo" type="text" placeholder="Ingrese el código" maxlength="5" class="form-control required number">
                                            </div>
                                            <input type="hidden" name="registrar">
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                               

                                
                            </form>
                        </div>
                    </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        
<!--////////////////////////////////-->
         <!-- ///////////////Modal Editar////////////////// -->

<div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content panel panel-info">
                                        <div class="modal-header panel-heading">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Editar Nivel de Programa</h4>
                                            <small class="font-bold">Permite editar los niveles de programas
identificados en la UBV para facilitar la consulta y
presentación de la información relacionada.
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            
                            <form id="form2" method="post" action="<?php echo constant('URL');?>programa_nivel/editar" class="wizard-big">
                                <h1>Nivel de Programa</h1>
                                <fieldset>
                                    <h2>Información del Nivel de Programa</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                            <input id="id_programa_nivel" name="id_programa_nivel" type="hidden" class="form-control">

                                                <label>Nivel de Programa <span style="color: red;">*</span></label>
                                                <input id="descripcion2" name="descripcion2" type="text" placeholder="Ingrese la descripción del Nivel de Programa" maxlength="70" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                                <label>Estatus <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " name="estatus1" id="estatus1">
                                                    <option value="">Seleccione</option>
                                                    <option value="Activo"> Activo</option>
                                                    <option value="Inactivo"> Inactivo</option>
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Código INE<span style="color: red;">*</span></label>
                                                <input id="codigo2" name="codigo2" type="text" placeholder="Ingrese el código" maxlength="5"class="form-control">
                                            </div>
                                            <input type="hidden" name="registrar2">
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                               

                                
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>       
                    </div>

 <!-- ///////////////Modal VER////////////////// -->

 <div class="modal inmodal fade " style="width: 100%;" id="myModal3" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content panel panel-primary">
                                        <div class="modal-header panel-heading">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Ver Nivel de Programa</h4>
                                            <small class="font-bold">Permite ver los niveles de programas
identificados en la UBV para facilitar la consulta y
presentación de la información relacionada.
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            


                            <table class="table">
                                <tr>
                                    <td><strong>Nivel de Programa: </strong></td>
                                    <td><span id="descripcion3"></span></td>
                                </tr>
                                <tr>
                                    <td><strong>Estatus: </strong></td>
                                    <td><span id="estatus2" class="label"></span></td>
                                </tr>
                                <tr>
                                    <td><strong>Código INE: </strong></td>
                                    <td><span id="codigo3"></span></td>
                                </tr>
                        </table>

                           
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>       
                    </div>




    <?php require 'views/footer.php'; ?>

    <!-- ajax de eliminar -->
    <script type="text/javascript" src="<?php echo constant('URL');?>src/js/datos_academicos/eliminarProgramaNivel.js"></script>

  


   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
  
<!-- 
    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");  
    $("#re").addClass("active");
});   
</script>-->



<script>



    $('#myModal2').on('show.bs.modal', function(e) {
        
    var product = $(e.relatedTarget).data('programa');
    $("#descripcion2").val(product);

    var product3 = $(e.relatedTarget).data('codigo');
    $("#codigo2").val(product3);

    var product4 = $(e.relatedTarget).data('id_programa');
    $("#id_programa_nivel").val(product4);
    var product5 = $(e.relatedTarget).data('estatus');
    $("#estatus1").val(product5);
    //alert(product5);

    });


    $('#myModal3').on('show.bs.modal', function(e) {
        
    var product = $(e.relatedTarget).data('programa1');
    $("#descripcion3").text(product);

    var product3 = $(e.relatedTarget).data('codigo1');
    $("#codigo3").text(product3);

    var product4 = $(e.relatedTarget).data('id_programa1');
    $("#id_programa_nivel3").val(product4);
    
    var product5 = $(e.relatedTarget).data('estatus1');
    $("#estatus2").text(product5);
    // esto puede servir
    $("#estatus2").removeClass("label-");

    if(product5 == 'Activo'){
        $("#estatus2").removeClass("label-danger");
        $("#estatus2").addClass("label-primary");

        // buscar en casa como remover clases en jquery
    }
    if(product5 == 'Inactivo'){
        $("#estatus2").removeClass("label-primary");

        $("#estatus2").addClass("label-danger");
    }

    

    });
</script>

    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>


<!-- form3 ver -->
<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form3").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>





</body>
</html>
