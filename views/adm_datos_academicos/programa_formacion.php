<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Programa Formacion | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  style chosen -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

 


</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                <h2>Registrar Programa Formacion</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            Datos Académicos
                        </li>
                        <li class="breadcrumb-item">
                            Programas
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Editar Programa Formacion</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                    <div class="title-action">
                    <?php if($_SESSION['Agregar']==true){?>

                        <!-- boton agregar-->
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                            <i class="fa fa-plus"></i> Agregar Programa
                            </button>
                    <?php } ?>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Lista de los Programas de Formacion Registrados</h5>
                        <div class="ibox-tools">
                        
                        </div>
                    </div>
                    <div class="ibox-content">
                    <div id="respuesta"><?php echo $this->mensaje; ?></div>
                        <div class="table-responsive">

                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Programa Formacion</th>
                        <th>Código</th>
                        <th>Centro Estudios</th>
                        <th>Programa Tipo</th>
                        <th>Nivel Academico</th>
                        <th>Linea Investigacion</th>
                        <th>Estatus</th>
                        <th>Acción</th>
                    </tr>
                    </thead >
                    <tbody id="tbody-programa">
                    <?php include_once 'models/datosacademicos/programa_formacion.php';
                            foreach($this->programas as $row){
                                $programa= new Programa();
                                $programa=$row;?>
                    <tr id ="fila-<?php echo $programa->id_programa; ?>" class="gradeX">
                        <td><?php echo $programa->descripcion; ?> </td>
                        <td><?php if($programa->codigo == NULL){ echo "-"; } else{ echo $programa->codigo;} ?></td>
                        <td><?php echo $programa->descripcion_centro; ?> </td>
                        <td><?php echo $programa->descripcion_tipo; ?></td>
                        <td><?php echo $programa->descripcion_nivel; ?> </td>
                        <td><?php echo $programa->descripcion_linea; ?></td>
                        <td style="text-align: center;"><?php if($programa->estatus=='Activo'){ echo '<span style="font-size: 12px;" class="label label-primary"> '.$programa->estatus.'</span>';} else{ echo '<span class="label label-danger" style="font-size: 12px;"> '.$programa->estatus.'</span>';}?></td>
                        <td>
                        <?php if($_SESSION['Editar']==true){?>

                            <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal" data-id_programa="<?php echo $programa->id_programa;?>" data-programa="<?php echo $programa->descripcion;?>" data-codigo="<?php echo $programa->codigo;?>" data-id_centro="<?php echo $programa->id_centro_estudio; ?>" data-centro="<?php echo $programa->descripcion_centro;?>" data-id_tipo="<?php echo $programa->id_programa_tipo; ?>" data-tipo="<?php echo $programa->descripcion_tipo;?>" data-id_nivel="<?php echo $programa->id_nivel_academico; ?>" data-nivel="<?php echo $programa->descripcion_nivel;?>" data-id_linea="<?php echo $programa->id_linea_investigacion; ?>" data-linea="<?php echo $programa->descripcion_linea;?>" data-estatus="<?php echo $programa->estatus;?>"><i class="fa fa-edit"></i> Editar</a>&nbsp;
                        <?php } 
                         if($_SESSION['Consultar']==true){?>

                            <a class="btn btn-outline btn-primary" href="#myModal3" role="button" data-toggle="modal" data-id_programa1="<?php echo $programa->id_programa;?>" data-programa1="<?php echo $programa->descripcion;?>" data-codigo1="<?php echo $programa->codigo;?>" data-id_centro1="<?php echo $programa->id_centro_estudio; ?>" data-centro1="<?php echo $programa->descripcion_centro;?>" data-id_tipo1="<?php echo $programa->id_programa_tipo; ?>" data-tipo1="<?php echo $programa->descripcion_tipo;?>"  data-id_nivel1="<?php echo $programa->id_nivel_academico; ?>" data-nivel1="<?php echo $programa->descripcion_nivel;?>" data-id_linea1="<?php echo $programa->id_linea_investigacion; ?>" data-linea1="<?php echo $programa->descripcion_linea;?>" data-estatus1="<?php echo $programa->estatus;?>"><i class="fa fa-search"></i> Ver</a>&nbsp;
                         <?php } ?>
                            <!-- <a class="btn btn-outline btn-danger" href="<?php echo constant('URL') . 'programaformacion/deletePrograma/' . $programa->id_programa;?>" role="button"> Remover</a> &nbsp; -->
                            <!-- <button class="btn btn-outline btn-danger bEliminar" data-id="<?php echo $programa->id_programa;?>"><i class="fa fa-trash"></i> Eliminar</button> -->
                        </td>
                    </tr>
                            <?php }?>
                    </tbody>
                    </table>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
<!-- ///////////////Modal Agregar////////////////// -->
<div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                <h4 class="modal-title">Registrar Programa de Formacion</h4>
                <small class="font-bold">Se refiere al registro de un programa de formación. <br>(Licenciado en sistemas, arquitecto, Técnico Superior Universitario en Informática etc.).
                <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
            </div>
                <div class="modal-body" >
                    <div class="ibox">
                        <div class="ibox-content">
                            <form id="form" method="post" action="<?php echo constant('URL');?>programaformacion/registrarPrograma" class="wizard-big">
                                <h1>Programa de Formacion</h1>
                                <fieldset>
                                    <h2> Información del Programa de Formacion</h2>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>Programa de Formacion <span style="color: red;">*</span></label>
                                                <input id="descripcion" name="descripcion" type="text" placeholder="Ingrese la descripción del Programa" maxlength="70" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                                <label>Código <i>(Opcional)</i></label>
                                                <input id="codigo" name="codigo" type="text" placeholder="Ingrese el código" maxlength="5"class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Centro de Estudios <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige el Centro..." name="id_centro_estudio" id="id_centro_estudio">
                                                    <option selected="selected" value="">Seleccione</option>
                                                    <?php include_once 'models/datosacademicos/centro_estudio.php';
                                                        foreach($this->centros as $row){
                                                        $centro_estudio=new Centro();
                                                        $centro_estudio=$row;?>
                                                        <option value="<?php echo $centro_estudio->id_centro_estudio;?>"><?php echo $centro_estudio->descripcion;?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                            <br><br><br><br><br><br>
                                        </div>
                                    <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>Tipo de Programa<span style="color: red;">*</span></label>
                                                <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige el Programa..." name="id_programa_tipo" id="id_programa_tipo">
                                                    <option value="">Seleccione</option>
                                                    <?php include_once 'models/datosacademicos/programa_tipo.php';
                                                        foreach($this->tipos as $row){
                                                        $programa_tipo=new Tipo();
                                                        $programa_tipo=$row;?>
                                                        <option value="<?php echo $programa_tipo->id_programa_tipo;?>"><?php echo $programa_tipo->descripcion;?></option>
                                                        <?php }?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Nivel Académico <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige el Área..." name="id_nivel_academico" id="id_nivel_academico">
                                                    <option value="">Seleccione</option>
                                                    <?php include_once 'models/datosacademicos/nivel_academico.php';
                                                        foreach($this->niveles as $row){
                                                        $nivel_academico=new NivelAcademico();
                                                        $nivel_academico=$row;?>
                                                        <option value="<?php echo $nivel_academico->id_nivel_academico;?>"><?php echo $nivel_academico->descripcion;?></option>
                                                        <?php }?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Linea de Investigacion<span style="color: red;">*</span></label>
                                                <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige la Linea..." name="id_linea_investigacion" id="id_linea_investigacion">
                                                    <option value="">Seleccione</option>
                                                    <?php include_once 'models/datosacademicos/lineainvestigacion.php';
                                                        foreach($this->lineas as $row){
                                                        $linea_investigacion=new LineaInvestigacion();
                                                        $linea_investigacion=$row;?>
                                                        <option value="<?php echo $linea_investigacion->id_linea_investigacion;?>"><?php echo $linea_investigacion->descripcion;?></option>
                                                        <?php }?>
                                                </select>
                                            </div>
                                            <br><br><br><br><br><br>
                                        </div>
                                        
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                            <label>Estatus<span style="color: red;">*</span></label>
                                            <select class="form-control required m-b " name="estatus" id="estatus">
                                                <option value="">Seleccione</option>
                                                <option value="Activo"> Activo</option>
                                                <option value="Inactivo"> Inactivo</option>
                                            </select>
                                         </div>
                                        <div class="form-group">
                                            <input type="hidden" name="registrar">
                                        </div>
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                </div>
                            </form>
                        </div>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                    <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                </div>
        </div>
    </div>
</div>


<!--////////////////////////////////-->
         <!-- ///////////////Modal Editar////////////////// -->

<div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Editar Programa de Formacion</h4>
                                            <small class="font-bold">Se refiere a la actualización de un programa de formación. <br>(Licenciado en sistemas, arquitecto, Técnico Superior Universitario en Informática etc.)
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                <div class="modal-body" >
                    <div class="ibox">
                        <div class="ibox-content">
                            <form id="form2" method="post" action="<?php echo constant('URL');?>programaformacion/editarPrograma" class="wizard-big">
                                <h1>Programa de Formacion</h1>
                                <fieldset>
                                    <h2>Información del Programa de Formacion</h2>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <input id="id_programa" name="id_programa" type="hidden" class="form-control">
                                                <label>Programa de Formacion<span style="color: red;">*</span></label>
                                                <input id="descripcion2" name="descripcion2" type="text" placeholder="Ingrese la descripción de la programa" maxlength="70" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                                <label>Código <i>(Opcional)</i></label>
                                                <input id="codigo2" name="codigo2" type="text" placeholder="Ingrese el código" maxlength="5"class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Centro de Estudios <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " tabindex="2" data-placeholder="Elige el Centro..." name="id_centro_estudio2"  id="id_centro_estudio2">
                                                    <option value="">Seleccione</option>
                                                    <?php include_once 'models/datosacademicos/centro_estudio.php';
                                                        foreach($this->centros as $row){
                                                        $centro_estudio=new Centro();
                                                        $centro_estudio=$row;?>
                                                        <option value="<?php echo $centro_estudio->id_centro_estudio;?>"><?php echo $centro_estudio->descripcion;?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                            </div>
                                            <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>Tipo de Programa<span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " name="id_programa_tipo2" id="id_programa_tipo2">
                                                    <option value="">Seleccione</option>
                                                    <?php include_once 'models/datosacademicos/programa_tipo.php';
                                                        foreach($this->tipos as $row){
                                                        $programa_tipo=new tipo();
                                                        $programa_tipo=$row;?>
                                                        <option value="<?php echo $programa_tipo->id_programa_tipo;?>"><?php echo $programa_tipo ->descripcion;?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Nivel Académico <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b" tabindex="2" data-placeholder="Elige el Área..." name="id_nivel_academico2" id="id_nivel_academico2">
                                                    <option value="">Seleccione</option>
                                                    <?php include_once 'models/datosacademicos/nivel_academico.php';
                                                        foreach($this->niveles as $row){
                                                        $nivel_academico=new NivelAcademico();
                                                        $nivel_academico=$row;?>
                                                        <option value="<?php echo $nivel_academico->id_nivel_academico;?>"><?php echo $nivel_academico->descripcion;?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Linea de Investigacion<span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " name="id_linea_investigacion2" id="id_linea_investigacion2">
                                                    <option value="">Seleccione</option>
                                                    <?php include_once 'models/datosacademicos/lineainvestigacion.php';
                                                        foreach($this->lineas as $row){
                                                        $linea_investigacion=new LineaInvestigacion();
                                                        $linea_investigacion=$row;?>
                                                        <option value="<?php echo $linea_investigacion->id_linea_investigacion;?>"><?php echo $linea_investigacion->descripcion;?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                            <label>Estatus<span style="color: red;">*</span></label>
                                            <select class="form-control required m-b " name="estatus2" id="estatus2">
                                                <option value="">Seleccione</option>
                                                <option value="Activo"> Activo</option>
                                                <option value="Inactivo"> Inactivo</option>
                                            </select>
                                         </div>
                                        <div class="form-group">
                                            <input type="hidden" name="registrar2">
                                        </div>
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                </div>
        </div>
    </div>
</div>

<!-- /////////////////////////////// -->

<!-- ///////////////Modal Ver////////////////// -->

<div class="modal inmodal fade " style="width: 100%;" id="myModal3" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Ver Programa de Formacion</h4>
                                            <small class="font-bold">Se refiere a la actualización de un programa de formación. <br>(Licenciado en sistemas, arquitecto, Técnico Superior Universitario en Informática etc.)
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                        <div class="ibox-content">
                            <form id="form3"  class="wizard-big">
                                <h1>Programa de Formacion</h1>
                                <fieldset>
                                    <h2>Información del Programa de Formacion</h2>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <input id="id_programa1" disabled type="hidden" class="form-control">
                                                <label>Programa de Formacion<span style="color: red;">*</span></label>
                                                <input id="descripcion3" disabled type="text" placeholder="Ingrese la descripción del Programa" maxlength="70" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                                <label>Código <i>(Opcional)</i></label>
                                                <input id="codigo3" type="text" disabled placeholder="Ingrese el código" maxlength="5"class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>Centro de Estudios <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " disabled id="id_centro_estudio3">
                                                    <option value="">Seleccione</option>
                                                    <?php include_once 'models/datosacademicos/centro_estudio.php';
                                                        foreach($this->centros as $row){
                                                        $centro_estudio=new Centro();
                                                        $centro_estudio=$row;?>
                                                        <option value="<?php echo $centro_estudio->id_centro_estudio;?>"><?php echo $centro_estudio->descripcion;?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                            </div>
                                    <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>Tipo de Programa <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " disabled id="id_programa_tipo3">
                                                    <option value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/programa_tipo.php';
                                                        foreach($this->tipos as $row){
                                                        $programa_tipo=new Tipo();
                                                        $programa_tipo=$row;?>
                                                        <option value="<?php echo $programa_tipo->id_programa_tipo;?>"><?php echo $programa_tipo->descripcion;?></option>
                                                    <?php }?>
                                                </select>
                                        </div>
                                        <div class="form-group">
                                                <label>Nivel Académico <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b" disabled id="id_nivel_academico3">
                                                        <option value="">Seleccione</option>
                                                    <?php include_once 'models/datosacademicos/nivel_academico.php';
                                                        foreach($this->niveles as $row){
                                                        $nivel_academico=new NivelAcademico();
                                                        $nivel_academico=$row;?>
                                                        <option value="<?php echo $nivel_academico->id_nivel_academico;?>"><?php echo $nivel_academico->descripcion;?></option>
                                                    <?php }?>
                                                </select>
                                        </div>
                                        <div class="form-group">
                                                <label>Linea de Investigacion<span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " disabled id="id_linea_investigacion3">
                                                    <option value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/lineainvestigacion.php';
                                                        foreach($this->lineas as $row){
                                                        $linea_investigacion=new LineaInvestigacion();
                                                        $linea_investigacion=$row;?>
                                                        <option value="<?php echo $linea_investigacion->id_linea_investigacion;?>"><?php echo $linea_investigacion->descripcion;?></option>
                                                    <?php }?>
                                                </select>
                                        </div>
                                        </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label>Estatus<span style="color: red;">*</span></label>
                                            <select class="form-control required m-b " disabled id="estatus3">
                                                <option value="">Seleccione</option>
                                                <option value="Activo"> Activo</option>
                                                <option value="Inactivo"> Inactivo</option>
                                            </select>
                                        </div>
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
            </div>
        </div>
    </div>
</div>




    <?php require 'views/footer.php'; ?>

    <!-- ajax de eliminarGrado -->
    <script type="text/javascript" src="<?php echo constant('URL');?>src/js/datos_academicos/eliminarPrograma.js"></script>

  


   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    <!-- Chosen -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>

<!-- 
    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");  
    $("#re").addClass("active");
});   
</script>-->


<script>
    $('#myModal1').on('show.bs.modal', function(e) {
        $('.chosen-select').chosen({width: "100%"});
        /*var elem = document.querySelector('.js-switch_4');
        var switchery = new Switchery(elem, { color: '#1AB394' });*/

    });

</script>
<script>
$('#myModal2').on('show.bs.modal', function(e) {
    $('.chosen-select').chosen({width: "100%"});


  var product = $(e.relatedTarget).data('programa');
  $("#descripcion2").val(product);

  var product2 = $(e.relatedTarget).data('codigo');
  $("#codigo2").val(product2);

  var product3 = $(e.relatedTarget).data('id_centro');
  $("#id_centro_estudio2").val(product3);

  var product4 = $(e.relatedTarget).data('id_tipo');
  $("#id_programa_tipo2").val(product4);

  var product5 = $(e.relatedTarget).data('id_nivel');
  $("#id_nivel_academico2").val(product5);

  var product6 = $(e.relatedTarget).data('id_linea');
  $("#id_linea_investigacion2").val(product6);

  var product7 = $(e.relatedTarget).data('estatus');
  $("#estatus2").val(product7);

  var product8 = $(e.relatedTarget).data('id_programa');
  $("#id_programa").val(product8);

  
});
</script>

<script>
$('#myModal3').on('show.bs.modal', function(e) {
    
  var product = $(e.relatedTarget).data('programa1');
  $("#descripcion3").val(product);

  var product2 = $(e.relatedTarget).data('codigo1');
  $("#codigo3").val(product2);
    
  var product3 = $(e.relatedTarget).data('id_centro1');
  $("#id_centro_estudio3").val(product3);

  var product4 = $(e.relatedTarget).data('id_tipo1');
  $("#id_programa_tipo3").val(product4);

  var product5 = $(e.relatedTarget).data('id_nivel1');
  $("#id_nivel_academico3").val(product5);

  var product6 = $(e.relatedTarget).data('id_linea1');
  $("#id_linea_investigacion3").val(product6);

  var product7 = $(e.relatedTarget).data('estatus1');
  $("#estatus3").val(product7);
  
  var product8 = $(e.relatedTarget).data('id_programa1');
  $("#id_programa1").val(product8);

  
  
});
</script>

    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<!-- modal ver --> 
<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form3").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    //form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>





</body>
</html>
