<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Dedicación | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

 


</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-9">
                <h2>Editar Dedicación</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            Datos Académicos
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Editar Dedicación</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-3">
                    <div class="title-action">
                    <?php if($_SESSION['Agregar']==true){?>

                    <!-- boton agregar-->
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                            <i class="fa fa-plus"></i> Agregar Dedicación
                            </button>                                                                                                 
                    <?php } ?>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">                                                                                                                                                                                                                                                                                                                                     


                <div class="ibox ">                                                                                                                                                                 
                    <div class="ibox-title">
                        <h5>Lista de Dedicación Registradas</h5>
                       
                        <div class="ibox-tools">                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                        

                        </div>
                    </div>
                    
                    <div class="ibox-content">
                    <div id="respuesta"><?php echo $this->mensaje; ?></div>

                        <div class="table-responsive">
                        
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Dedicación</th>
                        <th>Total de Horas</th>
                        <th>Hs. de Docencia Mín.</th>

                        <th>Hs. de Docencia Máx.</th>

                        <th>Acción</th>
                    </tr>
                    </thead >
                    <tbody id="tbody-dedicaciones">
                    <?php include_once 'models/datosacademicos/dedicacion.php'; 
                    
                            foreach($this->dedicaciones as $row){
                                $dedicacion= new DedicacionD();
                                $dedicacion=$row; ?>
                    <tr id ="fila-<?php echo $dedicacion->id_docente_dedicacion; ?>" class="gradeX">
                        <td><?php echo $dedicacion->descripcion; ?></td>
                        <td><?php echo $dedicacion->total_horas; ?> </td>
                        <td><?php echo $dedicacion->hora_docencia_minima; ?> </td>
                        <td><?php echo $dedicacion->hora_docencia_maxima; ?> </td>

                        <td> 
                    <?php if($_SESSION['Editar']==true){?>

                            <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal" data-id_docente_dedicacion="<?php echo $dedicacion->id_docente_dedicacion;?>" data-dedicacion="<?php echo $dedicacion->descripcion;?>" data-total_h="<?php echo $dedicacion->total_horas;?>" data-min="<?php echo $dedicacion->hora_docencia_minima;?>" data-max="<?php echo $dedicacion->hora_docencia_maxima;?>"><i class="fa fa-edit"></i> Editar</a>&nbsp;
                    <?php } 
                     if($_SESSION['Consultar']==true){?>
                    
                            <a class="btn btn-outline btn-primary" href="#myModal3" role="button" data-toggle="modal" data-id_docente_dedicacion1="<?php echo $dedicacion->id_docente_dedicacion;?>" data-dedicacion1="<?php echo $dedicacion->descripcion;?>" data-total_h1="<?php echo $dedicacion->total_horas;?>" data-min1="<?php echo $dedicacion->hora_docencia_minima;?>" data-max1="<?php echo $dedicacion->hora_docencia_maxima;?>"><i class="fa fa-search"></i> Ver</a>&nbsp;
                     <?php } ?>
                            <!-- <a class="btn btn-outline btn-danger" href="<?php echo constant('URL') . 'dedicacion/deletededicacion/' . $dedicacion->id_docente_dedicacion;?>" role="button"> Remover</a> &nbsp; -->
                            <!-- <button class="btn btn-outline btn-danger bEliminar" data-id="<?php echo $dedicacion->id_docente_dedicacion;?>"><i class="fa fa-trash"></i> Eliminar</button> -->
                        </td>
                    </tr>
                            <?php }?>
                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
<!-- ///////////////Modal Agregar////////////////// -->
        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Registrar Dedicación Docente</h4>
                                            <small class="font-bold">Registrar el tipo de dedicación docente con los
respectivos números de horas en el cual se basa su
carga académica.
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox">
                        
                        <div class="ibox-content">
                            
                            <form id="form" method="post" action="<?php echo constant('URL');?>dedicacion/registrar" class="wizard-big">
                                <h1>Dedicación Docente</h1>
                                <fieldset>
                                    <h2>Información de la Dedicación Docente</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <label>Dedicación Docente <span style="color: red;">*</span></label>
                                                <input type="text" name="descripcion" id="descripcion" class="form-control required" placeholder="Ingrese el nombre de la dedicación">
                                                <!-- <select class="form-control required m-b" name="descripcion" id="descripcion">
                                                            
                                                            <option value="">Seleccione</option> 
                                                         
                                                            <option value="Exclusiva">Exclusiva</option> 
                                                            <option value="Tiempo Completo">Tiempo Completo</option> 
                                                            <option value="Medio Tiempo">Medio Tiempo</option> 
                                                            <option value="Convencional">Convencional</option> 
                                                        </select> -->
                                            </div>
                                            <div class="form-group">
                                                <label>Horas Totales <i>(Semanales)</i><span style="color: red;">*</span></label>
                                                <input id="horas_totales" name="horas_totales" type="text" placeholder="Ingrese la cantidad de horas" maxlength="70" class="form-control required number">
                                            </div>
                                            <div class="form-group">
                                                <label>Horas Mínimas de Docencia <i></i><span style="color: red;">*</span></label>
                                                <input id="horas_minimas" name="horas_minimas" type="text" placeholder="Ingrese la cantidad Horas mínimas de docencia" maxlength="70" class="form-control required number">
                                            </div>
                                            <div class="form-group">
                                                <label>Horas Máximas de Docencia <span style="color: red;">*</span></label>
                                                <input id="horas_maximas" name="horas_maximas" type="text" placeholder="Ingrese  la cantidad Horas maximas de docencia" maxlength="70" class="form-control required number">
                                            </div>
                                            
                                            
                                            <input type="hidden" name="registrar">
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                               

                                
                            </form>
                        </div>
                    </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
         <!-- ///////////////Modal Editar////////////////// -->

<div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Editar Dedicación Docente</h4>
                                            <small class="font-bold">Registrar el tipo de dedicación docente con los
respectivos <br>números de horas en el cual se basa su 
carga académica.
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            
                            <form id="form2" method="post" action="<?php echo constant('URL');?>dedicacion/editar" class="wizard-big">
                                <h1>Dedicación Docente</h1>
                                <fieldset>
                                    <h2>Información de la Dedicación Docente</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                        <div class="form-group">
                                        <input type="hidden" name="id_docente_dedicacion" id="id_docente_dedicacion">
                                                <label>Dedicación Docente <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b" name="descripcion1" id="descripcion1">
                                                            
                                                            <option value="">Seleccione</option> 
                                                            <?php 
                                                                include_once 'models/datosacademicos/dedicacion.php'; 
                                                                foreach($this->dedicaciones as $row1){
                                                                    $dedicacion1= new DedicacionD();
                                                                    $dedicacion1=$row1;
                                                            ?>
                                                             <option value="<?php echo $dedicacion1->descripcion;?>"><?php echo $dedicacion1->descripcion;?></option>                                                             
                                                             <?php }?>
                                                             
                                                        </select> 
                                            </div>
                                            <div class="form-group">
                                                <label>Horas Totales <i>(Semanales)</i><span style="color: red;">*</span></label>
                                                <input id="horas_totales1" name="horas_totales1" type="text" placeholder="Ingrese la cantidad de horas" maxlength="70" class="form-control required number">
                                            </div>
                                            <div class="form-group">
                                                <label>Horas Mínimas de Docencia <i></i><span style="color: red;">*</span></label>
                                                <input id="horas_minimas1" name="horas_minimas1" type="text" placeholder="Ingrese la cantidad Horas mínimas de docencia" maxlength="70" class="form-control required number">
                                            </div>
                                            <div class="form-group">
                                                <label>Horas Máximas de Docencia <span style="color: red;">*</span></label>
                                                <input id="horas_maximas1" name="horas_maximas1" type="text" placeholder="Ingrese  la cantidad Horas maximas de docencia" maxlength="70" class="form-control required number">
                                            </div>
                                            
                                            
                                            <input type="hidden" name="registrar2">
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                               

                                
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>       
                    </div>


         <!-- ///////////////Modal Ver////////////////// -->

         <div class="modal inmodal fade " style="width: 100%;" id="myModal3" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Ver Dedicación Docente</h4>
                                            <small class="font-bold">Se refiere a la actualización de un turno o titulo académico correspondiente a un programa de formación. <br>(Licenciado en sistemas, arquitecto, Técnico Superior Universitario en Informática etc.)
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            
                            <form id="form3" class="wizard-big">
                                <h1>Dedicación Docente</h1>
                                <fieldset>
                                    <h2>Información de la Dedicación Docente</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                        <div class="form-group">
                                                <label>Dedicación Docente <span style="color: red;">*</span></label>
                                                        <select class="form-control  required m-b" disabled name="descripcion2" id="descripcion2">
                                                            
                                                            <option value="">Seleccione</option> 
                                                            <?php 
                                                                include_once 'models/datosacademicos/dedicacion.php'; 
                                                                foreach($this->dedicaciones as $row2){
                                                                    $dedicacion2= new DedicacionD();
                                                                    $dedicacion2=$row2;
                                                            ?>
                                                             <option value="<?php echo $dedicacion2->descripcion;?>"><?php echo $dedicacion2->descripcion;?></option>                                                             
                                                             <?php }?> 
                                                        </select> 
                                            </div>
                                            <div class="form-group">
                                                <label>Horas Totales <i>(Semanales)</i><span style="color: red;">*</span></label>
                                                <input id="horas_totales2" disabled name="horas_totales2" type="text" placeholder="Ingrese la cantidad de horas" maxlength="70" class="form-control required number">
                                            </div>
                                            <div class="form-group">
                                                <label>Horas Mínimas de Docencia <i></i><span style="color: red;">*</span></label>
                                                <input id="horas_minimas2" disabled name="horas_minimas2" type="text" placeholder="Ingrese la cantidad Horas mínimas de docencia" maxlength="70" class="form-control required number">
                                            </div>
                                            <div class="form-group">
                                                <label>Horas Máximas de Docencia <span style="color: red;">*</span></label>
                                                <input id="horas_maximas2" disabled name="horas_maximas2" type="text" placeholder="Ingrese  la cantidad Horas maximas de docencia" maxlength="70" class="form-control required number">
                                            </div>
                                            
                                            
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                               

                                
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>       
                    </div>








    <?php require 'views/footer.php'; ?>


    <!-- ajax de eliminarturno -->
    <script type="text/javascript" src="<?php echo constant('URL');?>src/js/datos_academicos/eliminarDedicacion.js"></script>

  

   
   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
  
<!-- 
    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");  
    $("#re").addClass("active");
});   
</script>-->



<script>


$('#myModal2').on('show.bs.modal', function(e) {
    
    var product = $(e.relatedTarget).data('id_docente_dedicacion');
    $("#id_docente_dedicacion").val(product);


    var product1 = $(e.relatedTarget).data('dedicacion');
    $("#descripcion1").val(product1);
  
    var product2 = $(e.relatedTarget).data('total_h');
    $("#horas_totales1").val(product2);

    var product3 = $(e.relatedTarget).data('min');
    $("#horas_minimas1").val(product3);

    var product4 = $(e.relatedTarget).data('max');
    $("#horas_maximas1").val(product4);

  });
  


$('#myModal3').on('show.bs.modal', function(e) {
    
    var product = $(e.relatedTarget).data('id_docente_dedicacion1');
    $("#id_docente_dedicacion1").val(product);


    var product1 = $(e.relatedTarget).data('dedicacion1');
    $("#descripcion2").val(product1);
  
    var product2 = $(e.relatedTarget).data('total_h1');
    $("#horas_totales2").val(product2);

    var product3 = $(e.relatedTarget).data('min1');
    $("#horas_minimas2").val(product3);

    var product4 = $(e.relatedTarget).data('max1');
    $("#horas_maximas2").val(product4);

  });

</script>


    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>
<!-- modal ver -->
<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form3").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    //form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>
    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>





</body>
</html>
