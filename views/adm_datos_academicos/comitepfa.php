<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin | Comite PFA Integrante</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  style chosen -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

 


</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                <h2>Registrar Integrante al Comite PFA </h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            Datos Académicos
                        </li>
                        <li class="breadcrumb-item">
                            Comites
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Registrar Integrantes al Comite PFA</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Lista de los Integrantes del Comites PFA Registrados</h5>
                       
                        <div class="ibox-tools">
                        <!-- boton agregar-->
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                            <i class="fa fa-plus"></i> Agregar Integrante al Comite PFA 
                            </button> 


                        </div>
                    </div>
                    
                    <div class="ibox-content">
                    <div id="respuesta"><?php echo $this->mensaje; ?></div>

                        <div class="table-responsive">
                        
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Docente</th>
                        <th>Programa</th>
                        <th>Tipo de Comite</th>
                        <th>Fecha Inicio</th>
                        <th>Fecha Fin</th>
                        <th>Estatus</th>
                        
                        <th>Acción</th>
                    </tr>
                    </thead >
                    <tbody id="tbody-comitepfa">
                    <?php include_once 'models/datosacademicos/comitepfa_integrante.php';
                            foreach($this->comitepfas as $row){
                                $comitepfa_integrante= new Comitepfa();
                                $comitepfa_integrante=$row;?>
                    <tr id ="fila-<?php echo $comitepfa_integrante->id_comitepfa_integrante; ?>" class="gradeX">
                        <td><?php echo $comitepfa_integrante->fecha_inicio; ?> </td>
                        <td><?php echo $comitepfa_integrante->fecha_fin; ?></td>
                        <td><?php if($comitepfa_integrante->estatus=='Activo'){ echo '<span class="label label-primary"> '.$comitepfa_integrante->estatus.'</span>';} else{ echo '<span class="label label-danger"> '.$comitepfa_integrante->estatus.'</span>';}?></td>
                        <td><?php echo $comitepfa_integrante->descripcion_programa; ?></td>
                        <td><?php echo $comitepfa_integrante->id_docente; ?> </td>
                        <td><?php echo $comitepfa_integrante->descripcion_comite_tipo; ?></td>
                        
                        <td> 
                            <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal" data-id_comitepfa="<?php echo $comitepfa_integrante->id_comitepfa_integrante;?>" data-fecha_inicio="<?php echo $comitepfa_integrante->fecha_inicio;?>" data-fecha_fin="<?php echo $comitepfa_integrante->fecha_fin;?>" data-estatus="<?php echo $comitepfa_integrante->estatus;?>" data-id_programa="<?php echo $comitepfa_integrante->id_programa; ?>" data-programa="<?php echo $comitepfa_integrante->descripcion_programa;?>" data-id_docente="<?php echo $comitepfa_integrante->id_docente; ?>" data-id_comite_tipo="<?php echo $comitepfa_integrante->id_comite_tipo; ?>" data-comite_tipo="<?php echo $comitepfa_integrante->descripcion_comite_tipo;?>" ><i class="fa fa-edit"></i> Editar</a>&nbsp;
                            <a class="btn btn-outline btn-primary" href="#myModal3" role="button" data-toggle="modal" data-id_comitepfa1="<?php echo $comitepfa_integrante->id_comitepfa_integrante;?>" data-fecha_inicio1="<?php echo $comitepfa_integrante->fecha_inicio;?>" data-fecha_fin1="<?php echo $comitepfa_integrante->fecha_fin;?>" data-estatus1="<?php echo $comitepfa_integrante->estatus;?>" data-id_programa1="<?php echo $comitepfa_integrante->id_programa; ?>" data-programa1="<?php echo $comitepfa_integrante->descripcion_programa;?>" data-id_docente1="<?php echo $comitepfa_integrante->id_docente; ?>" data-id_comite_tipo1="<?php echo $comitepfa_integrante->id_comite_tipo; ?>" data-comite_tipo1="<?php echo $comitepfa_integrante->descripcion_comite_tipo;?>" ><i class="fa fa-search"></i> Ver</a>&nbsp;

                            <!-- <a class="btn btn-outline btn-danger" href="<?php echo constant('URL') . 'comitepfacurricular/deletecomitepfa/' . $comitepfa_integrante->id_comitepfa_integrante;?>" role="button"> Remover</a> &nbsp; -->
                            <button class="btn btn-outline btn-danger bEliminar" data-id="<?php echo $comitepfa_integrante->id_comitepfa_integrante;?>"><i class="fa fa-trash"></i> Eliminar</button>
                        </td>
                    </tr>
                            <?php }?>
                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
<!-- ///////////////Modal Agregar////////////////// -->
        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Registrar Integrante del Comite PFA</h4>
                                            <small class="font-bold">Se refiere al registro de un integrante del comite correspondiente a un programa de formación academica.
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox">
                        
                        <div class="ibox-content">
                            
                            <form id="form" method="post" action="<?php echo constant('URL');?>comitepfacurricular/registrarComitepfa" class="wizard-big">
                                <h1>Integrante del Comite PFA</h1>
                                <fieldset>
                                    <h2>Información Integrante del Comite PFA</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                        <div class="form-group">
                                                <label>Docente <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige al docente..." name="id_docente" id="id_docente">
                                        
                                                    <option value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/docente.php';
                                                        foreach($this->docentes as $row){
                                                        $docente=new Docente();
                                                        $docente=$row;?> 
                                                        <option value="<?php echo $docente->id_docente;?>"><?php echo $docente->id_docente;?></option>
                                                        <?php }?>
                                                </select> 
                                                
                                            </div>    
                                        <div class="form-group">
                                                <label>Programa Formación <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige el Programa..." name="id_programa" id="id_programa">
                                        
                                                    <option value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/programa_formacion.php';
                                                        foreach($this->programas as $row){
                                                        $programa=new ProgramaFormacion();
                                                        $programa=$row;?> 
                                                        <option value="<?php echo $programa->id_programa;?>"><?php echo $programa->descripcion;?></option>
                                                        <?php }?>
                                                </select> 
                                                
                                            </div>
                                            <div class="form-group">
                                                <label>Tipo de Comite <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige el Programa..." name="id_programa" id="id_programa">
                                        
                                                    <option value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/comitetipo.php';
                                                        foreach($this->tipos as $row){
                                                        $comite_tipo=new Comitetipo();
                                                        $comite_tipo=$row;?> 
                                                        <option value="<?php echo $comite_tipo->id_comite_tipo;?>"><?php echo $comite_tipo->descripcion;?></option>
                                                        <?php }?>
                                                </select> 
                                                
                                            </div>
                                            <div class="form-group">
                                                <label>Fecha de Inicio <span style="color: red;">*</span></label>
                                                <input id="fecha_inicio" name="fecha_inicio" type="text" placeholder="Ingrese la fecha de inicio en el comite pfa" maxlength="70" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                                <label>Fecha fin <i>(Opcional)</i></label>
                                                <input id="fecha_fin" name="fecha_fin" type="text" placeholder="Ingrese la fecha fin del comite" maxlength="5"class="form-control">
                                            </div>
                
                                            <div class="form-group">
                                            
                                            <label>Estatus<span style="color: red;">*</span></label>
                                            <select class="form-control required m-b " name="estatus" id="estatus">
                                                <option value="">Seleccione</option>
                                                <option value="Activo"> Activo</option>
                                                <option value="Inactivo"> Inactivo</option>
                                            </select>
                                            </div>
                                            
                                           
                                        
                                        <div class="form-group">
                                            <input type="hidden" name="registrar">
                                        </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                               

                                
                            </form>
                        </div>
                    </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
         <!-- ///////////////Modal Editar////////////////// -->

<div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Editar Integrante del Comite PFA</h4>
                                            <small class="font-bold">Se refiere al registro de un integrante del comite correspondiente a un programa de formación academica.
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            
                            <form id="form2" method="post" action="<?php echo constant('URL');?>comitepfacurricular/editarComitepfa" class="wizard-big">
                                <h1>Integrante del Comite PFA</h1>
                                <fieldset>
                                    <h2>Información del Integrante del Comite PFA</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                                <div class="form-group">
                                                <input id="id_comitepfa_integrante" name="id_comitepfa_integrante" type="hidden" class="form-control">
                                                <label>Fecha inicio <span style="color: red;">*</span></label>
                                                <input id="fecha_inicio2" name="fecha_inicio2" type="text" placeholder="Ingrese la fecha inicio en el comite pfa" maxlength="70" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                                <label>Código <i>(Opcional)</i></label>
                                                <input id="fecha_fin2" name="fecha_fin2" type="text" placeholder="Ingrese el código" maxlength="5"class="form-control">
                                            </div>
                
                                                           
                                       
                                            <div class="form-group">
                                                <label>docente comitepfa <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " name="id_docente2" id="id_docente2">
                                        
                                                    <option value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/docente.php';
                                                        foreach($this->docentes as $row){
                                                        $docente=new Docente();
                                                            $docente=$row;?> 
                                                            <option value="<?php echo $docente->id_docente;?>"><?php echo $docente->descripcion;?></option>
                                                            <?php }?>
                                                </select> 
                                                
                                            </div>
                                            <div class="form-group">
                                                <label>Programa Formación <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " name="id_programa2" id="id_programa2">
                                        
                                                    <option value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/programa_formacion.php';
                                                        foreach($this->programas as $row){
                                                        $programa=new ProgramaFormacion();
                                                            $programa=$row;?> 
                                                            <option value="<?php echo $programa->id_programa;?>"><?php echo $programa->descripcion;?></option>
                                                            <?php }?>
                                                </select> 
                                                
                                            </div>
                                            <div class="form-group">
                                            
                                            <label>Estatus<span style="color: red;">*</span></label>
                                            <select class="form-control required m-b " name="estatus2" id="estatus2">
                                                <option value="">Seleccione</option>
                                                <option value="Activo"> Activo</option>
                                                <option value="Inactivo"> Inactivo</option>
                                            </select>
                                         </div>
                                        
                                        <div class="form-group">
                                            <input type="hidden" name="registrar2">
                                        </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                               

                                
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>       
                    </div>

<!-- /////////////////////////////// -->

<!-- ///////////////Modal Ver////////////////// -->

<div class="modal inmodal fade " style="width: 100%;" id="myModal3" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Ver Integrante del Comite PFA</h4>
                                            <small class="font-bold">Se refiere a la actualización de un grado o titulo académico correspondiente a un programa de formación. <br>(Licenciado en sistemas, arquitecto, Técnico Superior Universitario en Informática etc.)
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            
                            <form id="form3"  class="wizard-big">
                                <h1>Integrante del Comite PFA</h1>
                                <fieldset>
                                    <h2>Información del Integrante del Comite PFA</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <input id="id_comitepfa_integrante1" disabled type="hidden" class="form-control">
                                                <label>Integrante del Comite PFA <span style="color: red;">*</span></label>
                                                <input id="descripcion3" disabled type="text" placeholder="Ingrese la descripción del grado" maxlength="70" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                                <label>Código <i>(Opcional)</i></label>
                                                <input id="fecha_fin3" type="text" disabled placeholder="Ingrese el código" maxlength="5"class="form-control">
                                            </div>
                                        <div class="form-group">
                                            <label>Salida Intermedia<span style="color: red;">*</span></label>
                                            <select class="form-control required m-b " disabled id="salida_intermedia3">
                                                <option value="">Seleccione</option>
                                                <option value="Activo"> Activo</option>
                                                <option value="Inactivo"> Inactivo</option>
                                            </select>
                                        </div>    
                                        <div class="form-group">
                                                <label>docente comitepfa <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " disabled id="id_docente3">
                                        
                                                    <option value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/docentecomitepfa.php';
                                                        foreach($this->docentees as $row){
                                                        $docente=new docentecomitepfa();
                                                            $docente=$row;?> 
                                                            <option value="<?php echo $docente->id_docente;?>"><?php echo $docente->descripcion;?></option>
                                                            <?php }?>
                                                </select> 
                                        </div>
                                        <div class="form-group">
                                                <label>Programa Formación <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " disabled id="id_programa3">
                                        
                                                    <option value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/programa_formacion.php';
                                                        foreach($this->programas as $row){
                                                        $programa=new ProgramaFormacion();
                                                            $programa=$row;?> 
                                                            <option value="<?php echo $programa->id_programa;?>"><?php echo $programa->descripcion;?></option>
                                                            <?php }?>
                                                </select> 
                                        </div>
                                        <div class="form-group">
                                            <label>Estatus<span style="color: red;">*</span></label>
                                            <select class="form-control required m-b " disabled id="estatus3">
                                                <option value="">Seleccione</option>
                                                <option value="Activo"> Activo</option>
                                                <option value="Inactivo"> Inactivo</option>
                                            </select>
                                        </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                               

                                
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>       
                    </div>




    <?php require 'views/footer.php'; ?>

        
    <!-- ajax de eliminarGrado -->
    <script type="text/javascript" src="<?php echo constant('URL');?>src/js/datos_academicos/eliminarComitepfa.js"></script>

  

   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    <!-- Chosen -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>

<!-- 
    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");  
    $("#re").addClass("active");
});   
</script>-->


<script>
    $('#myModal1').on('show.bs.modal', function(e) {
        $('.chosen-select').chosen({width: "100%"});
        /*var elem = document.querySelector('.js-switch_4');
        var switchery = new Switchery(elem, { color: '#1AB394' });*/

    });

</script>
<script>
$('#myModal2').on('show.bs.modal', function(e) {
    $('.chosen-select').chosen({width: "100%"});


  var product = $(e.relatedTarget).data('comitepfa');
  $("#descripcion2").val(product);

  var product2 = $(e.relatedTarget).data('fecha_fin');
  $("#fecha_fin2").val(product2);
    
  var product3 = $(e.relatedTarget).data('salida_intermedia');
  $("#salida_intermedia2").val(product3);

  var product4 = $(e.relatedTarget).data('id_docente');
  $("#id_docente2").val(product4);

  var product5 = $(e.relatedTarget).data('id_programa');
  $("#id_programa2").val(product5);

  var product6 = $(e.relatedTarget).data('id_comitepfa');
  $("#id_comitepfa_integrante").val(product6);

  var product7 = $(e.relatedTarget).data('estatus');
  $("#estatus2").val(product7);

  
});
</script>

<script>
$('#myModal3').on('show.bs.modal', function(e) {
    
  var product = $(e.relatedTarget).data('comitepfa1');
  $("#descripcion3").val(product);

  var product2 = $(e.relatedTarget).data('fecha_fin1');
  $("#fecha_fin3").val(product2);
    
  var product3 = $(e.relatedTarget).data('salida_intermedia1');
  $("#salida_intermedia3").val(product3);

  var product4 = $(e.relatedTarget).data('id_docente1');
  $("#id_docente3").val(product4);

  var product5 = $(e.relatedTarget).data('id_programa1');
  $("#id_programa3").val(product5);

  var product6 = $(e.relatedTarget).data('id_comitepfa1');
  $("#id_comitepfa_integrante1").val(product6);

  var product7 = $(e.relatedTarget).data('estatus1');
  $("#estatus3").val(product7);
  
});
</script>

    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<!-- modal ver --> 
<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form3").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    //form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>





</body>
</html>
