<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Unidad Curricular | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">




</head>

<body>
    <?php require 'views/header.php'; ?>


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-9">
                <h2>Registrar Unidad Curricular</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            Datos Académicos
                        </li>
                        <li class="breadcrumb-item">
                            Unidades Curriculares
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Registrar Unidad Curricular</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-3">
                    <div class="title-action">
                    <?php if($_SESSION['Agregar']==true){?>
                    <!-- boton agregar-->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                           <i class="fa fa-plus"></i> Agregar Unidad
                        </button>
                    <?php } ?>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Lista de Unidad Curricular registrados</h5>
                        <div class="ibox-tools">
                        

                        </div>
                    </div>
                    <div class="ibox-content">
                    <div id="respuesta"><?php echo $this->mensaje; ?></div>

                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Unidad Curricular</th>
                        <th>Estatus</th>
                        <th>Acción</th>
                    </tr>
                    </thead >
                    <tbody id="tbody-unidad">
                    <?php include_once 'models/datosacademicos/unidad_curricular.php'; 
                    
                            foreach($this->unidades as $row){
                                $unidad_curricular= new Unidad();
                                ?>
                    <tr id ="fila-<?php echo $unidad_curricular->id_unidad_curricular; ?>" class="gradeX">
                      
                        <td><?php echo $unidad_curricular->descripcion; ?> </td>
                        <td style="text-align: center;"><?php if($unidad_curricular->estatus=='Activo'){ echo '<span style="font-size: 12px;" class="label label-primary"> '.$unidad_curricular->estatus.'</span>';} else{ echo '<span class="label label-danger" style="font-size: 12px;"> '.$unidad_curricular->estatus.'</span>';}?></td>
                        <td>
                    <?php if($_SESSION['Editar']==true){?>

                            <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal" data-id_unidad_curricular="<?php echo $unidad_curricular->id_unidad_curricular;?>" data-unidad_curricular="<?php echo $unidad_curricular->descripcion;?>" data-estatus="<?php echo $unidad_curricular->estatus;?>" ><i class="fa fa-edit"></i> Editar</a>&nbsp;
                    <?php } 
                     if($_SESSION['Consultar']==true){?>

                            <a class="btn btn-outline btn-primary" href="#myModal3" role="button" data-toggle="modal" data-id_unidad_curricular1="<?php echo $unidad_curricular->id_unidad_curricular;?>" data-unidad_curricular1="<?php echo $unidad_curricular->descripcion;?>" data-estatus1="<?php echo $unidad_curricular->estatus;?>" ><i class="fa fa-search"></i> Ver</a>&nbsp;
                     <?php } 
                     if($_SESSION['Eliminar']==true){?>

                            <!-- <a class="btn btn-outline btn-danger" href="<?php echo constant('URL') . 'unidadcurricular/deleteUnidad/' . $unidad_curricular->id_unidad_curricular;?>" role="button"> Remover</a> &nbsp; -->
                            <button class="btn btn-outline btn-danger bEliminar" data-id="<?php echo $unidad_curricular->id_unidad_curricular;?>"><i class="fa fa-trash"></i> Eliminar</button>
                     <?php } ?>
                        </td>
                    </tr>
                            <?php }?>
                    </tbody>

                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
<!-- ///////////////Modal Agregar////////////////// -->
<div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                    <h4 class="modal-title">Registrar la Unidad Curricular</h4>
                    <small class="font-bold">Indique la descripcion de la Unidad   <br>
                        Curricular, ejemplo, Unidad Básica Integradora, UC
                        Obligatorias, UC electivas, etc.
                    <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
            </div>
                <div class="modal-body" >
                    <div class="ibox">
                        <div class="ibox-content">
                            <form id="form" method="post" action="<?php echo constant('URL');?>unidadcurricular/registrarUnidad" class="wizard-big">
                                <h1>unidad_curricular</h1>
                                <fieldset>
                                    <h2>Información de Unidad Curricular</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <label>Unidad Curricular <span style="color: red;">*</span></label>
                                                <input id="descripcion" name="descripcion" type="text" placeholder="Ingrese la descripción del unidad curricular" maxlength="70" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                                <label>Estatus<span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " name="estatus" id="estatus">
                                                    <option value="">Seleccione</option>
                                                    <option value="Activo"> Activo</option>
                                                    <option value="Inactivo"> Inactivo</option>
                                                </select>
                                         </div>
                                            <input type="hidden" name="registrar">
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                            </form>
                        </div>
                    </div>

                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>

<!--////////////////////////////////-->
         <!-- ///////////////Modal Editar////////////////// -->

<div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Editar la Unidad Curricular</h4>
                                            <small class="font-bold">Indique la descripcion de la Unidad<br>
                                                Curricular, ejemplo, Unidad Básica Integradora, UC
                                                Obligatorias, UC electivas, etc.
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                        <div class="ibox-content">

                            <form id="form2" method="post" action="<?php echo constant('URL');?>unidadcurricular/editarUnidad" class="wizard-big">
                                <h1>Unidad Curricular</h1>
                                <fieldset>
                                    <h2>Información de Unidad Curricular</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                            <input id="id_unidad_curricular" name="id_unidad_curricular" type="hidden" class="form-control">

                                                <label>Unidad Curricular <span style="color: red;">*</span></label>
                                                <input id="descripcion2" name="descripcion2" type="text" placeholder="Ingrese la descripción de la Unidad Curricular" maxlength="70" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                            <label>Estatus<span style="color: red;">*</span></label>
                                            <select class="form-control required m-b " name="estatus2" id="estatus2">
                                                <option value="">Seleccione</option>
                                                <option value="Activo"> Activo</option>
                                                <option value="Inactivo"> Inactivo</option>
                                            </select>
                                         </div>
                                         <div class="form-group">
                                            <input type="hidden" name="registrar2">
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                            </form>
                        </div>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                    </div>


<!-- ///////////////Modal Ver////////////////// -->

<div class="modal inmodal fade " style="width: 100%;" id="myModal3" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title"><i class="fa fa-search"></i>Ver la Unidad Curricular</h4>
                                            <small class="font-bold">Indique la descripcion de la Unidad<br>
                                                Curricular, ejemplo, Unidad Básica Integradora, UC
                                                Obligatorias, UC electivas, etc.
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                        <div class="ibox-content">
                            <form id="form3" class="wizard-big">
                                <h1>Unidad Curricular</h1>
                                <fieldset>
                                    <h2>Información de Unidad Curricular</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                            <input id="id_unidad_curricular1" type="hidden" class="form-control">

                                                <label>Unidad Curricular <span style="color: red;">*</span></label>
                                                <input id="descripcion3" disabled type="text" placeholder="Ingrese la descripción de la Unidad Curricular" maxlength="70" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                            <label>Estatus<span style="color: red;">*</span></label>
                                            <select class="form-control required m-b " disabled id="estatus3">
                                                <option value="">Seleccione</option>
                                                <option value="Activo"> Activo</option>
                                                <option value="Inactivo"> Inactivo</option>
                                            </select>
                                        </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                            </form>
                        </div>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                    </div>






    <?php require 'views/footer.php'; ?>

       
    <!-- ajax de eliminar tipo_uc -->
    <script type="text/javascript" src="<?php echo constant('URL');?>src/js/datos_academicos/eliminarUnidad.js"></script>


  
   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>

<!--
    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");
    $("#re").addClass("active");
});
</script>-->



<script>
$('#myModal2').on('show.bs.modal', function(e) {

  var product = $(e.relatedTarget).data('unidad_curricular');
  $("#descripcion2").val(product);

  var product2 = $(e.relatedTarget).data('estatus');
  $("#estatus2").val(product2);

  var product3 = $(e.relatedTarget).data('id_unidad_curricular');
  $("#id_unidad_curricular").val(product3);

});

$('#myModal3').on('show.bs.modal', function(e) {

    var product = $(e.relatedTarget).data('unidad_curricular1');
    $("#descripcion3").val(product);

    var product2 = $(e.relatedTarget).data('estatus1');
    $("#estatus3").val(product2);

    var product3 = $(e.relatedTarget).data('id_unidad_curricular1');
    $("#id_unidad_curricular1").val(product3);

  });
</script>


    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>
<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form3").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    //form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>
    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [

                ]

            });

        });

    </script>





</body>
</html>
