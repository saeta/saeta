<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Historia de Asociación Programa y Aldea | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  style chosen -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

 


</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                <h2>Historia Asociar Programa Aldea</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            Datos Académicos
                        </li>
                        <li class="breadcrumb-item">
                            Historicos
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Historia Asociar Programa Aldea</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Lista de Modificaciones en Asociar Programa ∈ Aldea</h5>
                       
                        <div class="ibox-tools">
                        </div>
                    </div>
                    
                    <div class="ibox-content">
                    <div id="respuesta"><?php echo $this->mensaje; ?></div>

                        <div class="table-responsive">
                        
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Código OPSU</th>
                        <th>Aldea</th>
                        <th>Programa</th>
                        <th>Estatus</th>
                        <th>Tiempo de Modificación</th>
                        <th>Acción</th>
                    </tr>
                    </thead >
                    <tbody id="tbody-aldeapro">
                    <?php include_once 'models/datosacademicos/aldea_programa.php';
                            foreach($this->aldeaprogramas as $row){
                                $aldea_programa= new AldeaPrograma();
                                $aldea_programa=$row;?>
                    <tr id ="fila-<?php echo $aldea_programa->id_aldea_programa; ?>" class="gradeX">
                        <td><?php echo $aldea_programa->codigo_opsu;?></td>
                        <td><?php echo $aldea_programa->descripcion_aldea; ?> </td>
                        <td><?php echo $aldea_programa->descripcion_programa; ?></td>
                        

                        <td style="text-align: center;"><?php if($aldea_programa->estatus=='Activo'){ echo '<span class="label label-primary" style="font-size: 12px;"> '.$aldea_programa->estatus.'</span>';} else{ echo '<span class="label label-danger" style="font-size: 12px;"> '.$aldea_programa->estatus.'</span>';}?></td>
                        <td><?php echo $aldea_programa->tiempo; ?></td>
                        <td> 
                        <?php if($_SESSION['Consultar']==true){?>

                            <a class="btn btn-outline btn-primary" href="#myModal3" role="button" data-toggle="modal" data-id_aldea_programa1="<?php echo $aldea_programa->id_aldea_programa;?>" data-codigo_opsu1="<?php echo $aldea_programa->codigo_opsu;?>" data-id_aldea1="<?php echo $aldea_programa->id_aldea; ?>" data-aldea1="<?php echo $aldea_programa->descripcion_aldea;?>" data-id_programa1="<?php echo $aldea_programa->id_programa; ?>" data-programa1="<?php echo $aldea_programa->descripcion_programa;?>" data-estatus1="<?php echo $aldea_programa->estatus;?>" data-tiempo="<?php echo $aldea_programa->tiempo;?>"><i class="fa fa-search"></i> Ver</a>&nbsp;
                        <?php } ?>
                            <!-- <a class="btn btn-outline btn-danger" href="<?php echo constant('URL') . 'aldea_programa/deleteAldeaP/' . $aldea_programa->id_aldea_programa;?>" role="button"> Remover</a> &nbsp; -->
                            <!-- <button class="btn btn-outline btn-danger bEliminar" data-id="<?php echo $aldea_programa->id_aldea_programa;?>"><i class="fa fa-trash"></i> Eliminar</button> -->
                        </td>
                    </tr>
                            <?php }?>
                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>


<!-- ///////////////Modal Ver////////////////// -->

<div class="modal inmodal fade " style="width: 100%;" id="myModal3" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content panel panel-info">
                                        <div class="modal-header panel-heading">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Ver Aldea Asociada al Programa</h4>
                                            <small class="font-bold">En cada aldea se deben identificar los programas
que están activos
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            <div class="jumbotron">
                                                <h1>Información de la Aldea Programa</h1>
                                                <h3>Código OPSU</h3>
                                                <p id="codigo_opsu3"></p>
                                                <h3>Aldea</h3>
                                                <p id="id_aldea3"> </p>
                                                <h3>Programa</h3>
                                                <p id="id_programa3"></p>
                                                <h3>Estatus</h3>
                                                <p id="estatus3"></p>
                                                <h3>Tiempo de Modificación</h3>
                                                <p id="tiempo"></p>
                                                </p>
                                            </div>
                                        
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>       
                    </div>




    <?php require 'views/footer.php'; ?>

      
    <!-- ajax de eliminarGrado -->
    <script type="text/javascript" src="<?php echo constant('URL');?>src/js/datos_academicos/eliminarAldeapro.js"></script>

  

   
   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    <!-- Chosen -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>

<!-- 
    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");  
    $("#re").addClass("active");
});   
</script>-->



<script>
$('#myModal3').on('show.bs.modal', function(e) {
    
  

  var product2 = $(e.relatedTarget).data('codigo_opsu1');
  $("#codigo_opsu3").text(product2);
    
  var product4 = $(e.relatedTarget).data('aldea1');
  $("#id_aldea3").text(product4);

  var product5 = $(e.relatedTarget).data('programa1');
  $("#id_programa3").text(product5);

  var product6 = $(e.relatedTarget).data('id_aldea_programa1');
  $("#id_aldea_programa1").val(product6);

  var product7 = $(e.relatedTarget).data('estatus1');
  $("#estatus3").text(product7);
  
  var product8 = $(e.relatedTarget).data('tiempo');
  $("#tiempo").text(product8);

});
</script>

    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<!-- modal ver --> 
<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form3").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    //form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>





</body>
</html>
