<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Asociar Aldea Programa | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  style chosen -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

 


</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-9">
                <h2>Asociar Aldea Programa</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            Datos Académicos
                        </li>
                        <li class="breadcrumb-item">
                            Programas
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Asociar Aldea Programa</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-3">
                    <div class="title-action">
                    <?php if($_SESSION['Agregar']==true){?>

                    <!-- boton agregar-->
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                            <i class="fa fa-plus"></i> Asociar Aldea Programa
                            </button> 
                    <?php } ?>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Lista de las Aldeas Asociadas a los Programas</h5>
                       
                        <div class="ibox-tools">
                        


                        </div>
                    </div>
                    
                    <div class="ibox-content">
                    <div id="respuesta"><?php echo $this->mensaje; ?></div>

                        <div class="table-responsive">
                        
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Código OPSU</th>
                        <th>Aldea</th>
                        <th>Programa</th>
                        <th>Estatus</th>
                        <th>Acción</th>
                    </tr>
                    </thead >
                    <tbody id="tbody-aldeapro">
                    <?php include_once 'models/datosacademicos/aldea_programa.php';
                            foreach($this->aldeaprogramas as $row){
                                $aldea_programa= new AldeaPrograma();
                                $aldea_programa=$row;?>
                    <tr id ="fila-<?php echo $aldea_programa->id_aldea_programa; ?>" class="gradeX">
                        <td><?php echo $aldea_programa->codigo_opsu;?></td>
                        <td><?php echo $aldea_programa->descripcion_aldea; ?> </td>
                        <td><?php echo $aldea_programa->descripcion_programa; ?></td>
                        <td><?php if($aldea_programa->estatus=='Activo'){ echo '<span class="label label-primary"> '.$aldea_programa->estatus.'</span>';} else{ echo '<span class="label label-danger"> '.$aldea_programa->estatus.'</span>';}?></td>
                    
                        <td> 
                        <?php if($_SESSION['Editar']==true){?>

                            <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal" data-id_aldea_programa="<?php echo $aldea_programa->id_aldea_programa;?>" data-codigo_opsu="<?php echo $aldea_programa->codigo_opsu;?>" data-id_aldea="<?php echo $aldea_programa->id_aldea; ?>" data-aldea="<?php echo $aldea_programa->descripcion_aldea;?>"data-id_programa="<?php echo $aldea_programa->id_programa; ?>" data-programa="<?php echo $aldea_programa->descripcion_programa;?>" data-estatus="<?php echo $aldea_programa->estatus;?>"><i class="fa fa-edit"></i> Editar</a>&nbsp;
                        <?php } 
                         if($_SESSION['Consultar']==true){?>

                            <a class="btn btn-outline btn-primary" href="#myModal3" role="button" data-toggle="modal" data-id_aldea_programa1="<?php echo $aldea_programa->id_aldea_programa;?>" data-codigo_opsu1="<?php echo $aldea_programa->codigo_opsu;?>" data-id_aldea1="<?php echo $aldea_programa->id_aldea; ?>" data-aldea1="<?php echo $aldea_programa->descripcion_aldea;?>" data-id_programa1="<?php echo $aldea_programa->id_programa; ?>" data-programa1="<?php echo $aldea_programa->descripcion_programa;?>" data-estatus1="<?php echo $aldea_programa->estatus;?>"><i class="fa fa-search"></i> Ver</a>&nbsp;
                         <?php } ?>
                            <!-- <a class="btn btn-outline btn-danger" href="<?php echo constant('URL') . 'aldea_programa/deleteAldeaP/' . $aldea_programa->id_aldea_programa;?>" role="button"> Remover</a> &nbsp; -->
                            <!-- <button class="btn btn-outline btn-danger bEliminar" data-id="<?php echo $aldea_programa->id_aldea_programa;?>"><i class="fa fa-trash"></i> Eliminar</button> -->
                        </td>
                    </tr>
                            <?php }?>
                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
<!-- ///////////////Modal Agregar////////////////// -->
        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Asociar Aldea Programa</h4>
                                            <small class="font-bold">Se refiere al registro de una Aldea Programa correspondiente a un programa de formación. <br>(Licenciado en sistemas, arquitecto, Técnico Superior Universitario en Informática etc.).
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox">
                        
                        <div class="ibox-content">
                            
                            <form id="form" method="post" action="<?php echo constant('URL');?>aldea_programa/registrar" class="wizard-big">
                                <h1>Aldea Programa</h1>
                                <fieldset>
                                    <h2>Información de la Aldea Programa</h2>
                                    <div class="row">
                                        <div class="col-lg-8">

                                        
                                            <div class="form-group">
                                                <label>Código OPSU </label>
                                                <input id="codigo_opsu" name="codigo_opsu" type="text" placeholder="Ingrese el código OPSU" maxlength="5"class="form-control">
                                            </div>
                
                                            <div class="form-group">
                                                <label>Aldea <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige la aldea..." name="id_aldea" id="id_aldea">
                                        
                                                    <option value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/aldea.php';
                                                        foreach($this->aldeas as $row){
                                                        $aldea=new Aldea();
                                                        $aldea=$row;?> 
                                                        <option value="<?php echo $aldea->id_aldea;?>"><?php echo $aldea->descripcion;?></option>
                                                        <?php }?>
                                                </select> 
                                                
                                            </div>
                                            <div class="form-group">
                                                <label>Programa Formación <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b chosen-select" tabindex="2" data-placeholder="Elige el Programa..." name="id_programa" id="id_programa">
                                        
                                                    <option value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/programa_formacion.php';
                                                        foreach($this->programas as $row){
                                                        $programa=new Programa();
                                                        $programa=$row;?> 
                                                        <option value="<?php echo $programa->id_programa;?>"><?php echo $programa->descripcion;?></option>
                                                        <?php }?>
                                                </select> 
                                            </div>
                                            <div class="form-group">
                                            
                                            <label>Estatus<span style="color: red;">*</span></label>
                                            <select class="form-control required m-b " name="estatus" id="estatus">
                                                <option value="">Seleccione</option>
                                                <option value="Activo"> Activo</option>
                                                <option value="Inactivo"> Inactivo</option>
                                            </select>
                                         </div>
                                        
                                        <div class="form-group">
                                            <input type="hidden" name="registrar">
                                        </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                               

                                
                            </form>
                        </div>
                    </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
         <!-- ///////////////Modal Editar////////////////// -->

<div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content panel panel-success">
                                        <div class="modal-header panel-heading">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Editar Aldea Programa</h4>
                                            <small class="font-bold">Se refiere a la actualización de un grado o titulo académico correspondiente a un programa de formación. <br>(Licenciado en sistemas, arquitecto, Técnico Superior Universitario en Informática etc.)
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            
                            <form id="form2" method="post" action="<?php echo constant('URL');?>aldea_programa/editar" class="wizard-big">
                                <h1>Aldea Programa</h1>
                                <fieldset>
                                    <h2>Información de la Aldea Programa</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            
                                                <input id="id_aldea_programa" name="id_aldea_programa" type="hidden" class="form-control">

                                            <div class="form-group">
                                                <label>Código OPSU </label>
                                                <input id="codigo_opsu2" name="codigo_opsu2" type="text" placeholder="Ingrese el código OPSU" maxlength="5"class="form-control">
                                            </div>
                                
                                            <div class="form-group">
                                                <label>aldea  <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " name="id_aldea2" id="id_aldea2">
                                        
                                                    <option value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/aldea.php';
                                                        foreach($this->aldeas as $row){
                                                        $aldea=new Aldea();
                                                            $aldea=$row;?> 
                                                            <option value="<?php echo $aldea->id_aldea;?>"><?php echo $aldea->descripcion;?></option>
                                                            <?php }?>
                                                </select> 
                                                
                                            </div>
                                            <div class="form-group">
                                                <label>Programa Formación <span style="color: red;">*</span></label>
                                                <select class="form-control required m-b " name="id_programa2" id="id_programa2">
                                        
                                                    <option value="">Seleccione</option> 
                                                    <?php include_once 'models/datosacademicos/programa_formacion.php';
                                                        foreach($this->programas as $row){
                                                        $programa=new Programa();
                                                            $programa=$row;?> 
                                                            <option value="<?php echo $programa->id_programa;?>"><?php echo $programa->descripcion;?></option>
                                                            <?php }?>
                                                </select> 
                                                
                                            </div>
                                            <div class="form-group">
                                            
                                            <label>Estatus<span style="color: red;">*</span></label>
                                            <select class="form-control required m-b " name="estatus2" id="estatus2">
                                                <option value="">Seleccione</option>
                                                <option value="Activo"> Activo</option>
                                                <option value="Inactivo"> Inactivo</option>
                                            </select>
                                         </div>
                                        
                                        <div class="form-group">
                                            <input type="hidden" name="registrar2">
                                        </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-book" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                               

                                
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>       
                    </div>

<!-- /////////////////////////////// -->

<!-- ///////////////Modal Ver////////////////// -->

<div class="modal inmodal fade " style="width: 100%;" id="myModal3" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content panel panel-info">
                                        <div class="modal-header panel-heading">
                                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Ver Aldea Asociada al Programa</h4>
                                            <small class="font-bold">En cada aldea se deben identificar los programas
que están activos
                                            <br>Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            <div class="jumbotron">
                                                <h1>Información de la Aldea Programa</h1>
                                                <h3>Código OPSU</h3>
                                                <p id="codigo_opsu3"></p>
                                                <h3>Aldea</h3>
                                                <p id="id_aldea3"> </p>
                                                <h3>Programa</h3>
                                                <p id="id_programa3"></p>
                                                <h3>Estatus</h3>
                                                <p ><span id="estatus3" class="label "></span></p>

                                                </p>
                                            </div>
                                        
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>       
                    </div>




    <?php require 'views/footer.php'; ?>

       
   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    <!-- Chosen -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>

<!-- 
    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");  
    $("#re").addClass("active");
});   
</script>-->


<script>
    $('#myModal1').on('show.bs.modal', function(e) {
        $('.chosen-select').chosen({width: "100%"});
        /*var elem = document.querySelector('.js-switch_4');
        var switchery = new Switchery(elem, { color: '#1AB394' });*/

    });

</script>
<script>
$('#myModal2').on('show.bs.modal', function(e) {
    $('.chosen-select').chosen({width: "100%"});

  var product2 = $(e.relatedTarget).data('codigo_opsu');
  $("#codigo_opsu2").val(product2);

  var product4 = $(e.relatedTarget).data('id_aldea');
  $("#id_aldea2").val(product4);
  

    var product5 = $(e.relatedTarget).data('id_programa');
    $("#id_programa2").val(product5);

    var product6 = $(e.relatedTarget).data('id_aldea_programa');
    $("#id_aldea_programa").val(product6);

  var product7 = $(e.relatedTarget).data('estatus');
  $("#estatus2").val(product7);

  
});
</script>

<script>
$('#myModal3').on('show.bs.modal', function(e) {
    
  

  var product2 = $(e.relatedTarget).data('codigo_opsu1');
  $("#codigo_opsu3").text(product2);
    
  var product4 = $(e.relatedTarget).data('aldea1');
  $("#id_aldea3").text(product4);

  var product5 = $(e.relatedTarget).data('programa1');
  $("#id_programa3").text(product5);

  var product6 = $(e.relatedTarget).data('id_aldea_programa1');
  $("#id_aldea_programa1").val(product6);

  var product7 = $(e.relatedTarget).data('estatus1');
  $("#estatus3").text(product7);
  

  if(product7 == 'Activo'){
        $("#estatus3").removeClass("label-danger");
        $("#estatus3").addClass("label-primary");

        // buscar en casa como remover clases en jquery
    }
    if(product7 == 'Inactivo'){
        $("#estatus3").removeClass("label-primary");

        $("#estatus3").addClass("label-danger");
    }


});
</script>

    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<!-- modal ver --> 
<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form3").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    //form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>





</body>
</html>
