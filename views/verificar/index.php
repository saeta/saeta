<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title> Verificación Datos Generales | SIDTA</title>


 
    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">


</head>

<body>
 

   <?php require 'views/header.php'; ?>
   

   <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-5">
                    <h2>Verificación de Datos Generales</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a  href="<?php echo constant ('URL');?>verificar">Verificación de Datos</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Lista de Docentes</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-7">
                    <div class="title-action">
                        <!-- <a href="" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar Verificacion</a> -->

                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Listado de Docentes en espera</h5>
                        <div class="ibox-tools">

                 

                        </div>
                    </div>
                    <div class="ibox-content">

                        <!--  contenido -->
                        <div class="table-responsive">
                        
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                        <tr>
                            <th>Identificación</th>
                            <th>Nombre y Apellido</th>

                           

                            <th>Acciones</th>
    <!-- <th>Código</th> como hago aqui con la foranea q esta en esta tabla llamada id_area_conocimiento_opsu -->
                            
                        </tr>
                        </thead >
                        <tbody id="tbody-docentes">
                        <?php 
                       // lo comente  $i=0;
                                foreach($this->docentes as $row){
                                    $docentes= new Estructura();
                                    $docentes=$row;?>
                        <tr id ="fila-<?php echo $docentes->id_docente; ?>" class="gradeX">
                        <td><?php echo $docentes->nacionalidad . " - ".$docentes->identificacion; ?></td>
                         <td><?php echo $docentes->primer_nombre . " " . $docentes->segundo_nombre; ?></td>
                            
                         

                            
                            
                            <td> 
                            <?php if($_SESSION['Editar']==true){?>
                                <a class="btn btn-outline btn-info" href="<?php echo constant ('URL') . "verificar/detalle/" . $docentes->id_docente;?>" role="button"><i class="fa fa-info-circle"></i> Ver</a>&nbsp;
    <!--como incluyo mas a class por las variables de estatus y la foranea-->
                            <?php } ?>
                                <!-- <a class="btn btn-outline btn-danger" href="<?php echo constant('URL') . 'centro_estudio/deleteCentro/' . $docentes->id_docente;?>" role="button"> Remover</a> &nbsp; -->
                                <!-- <button class="btn btn-outline btn-danger bEliminar" data-id="<?php echo $docentes->id_docente;?>"><i class="fa fa-trash"></i> Eliminar</button>-->
                            </td>
                        </tr>
                                <?php }?>
                        
                        </tbody>
                       
                        </table>
                            </div>
                            <!-- end contenido -->

                    </div>
                </div>
            </div>
            </div>
        </div>



<?php require 'views/footer.php'; ?>

        
    <!-- Data tables Scripts -->

    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

   
    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>

   

</body>
</html>
