<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SIDTA | Verificación Datos Generales</title>


 
    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">
    <!-- FooTable -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/footable/footable.core.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <!--  style select2 -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/select2/select2.min.css" rel="stylesheet">

</head>

<body>
 

   <?php require 'views/header.php'; ?>
   

   <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-6">
                    <h2>Verificación de Datos Generales</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a  href="<?php echo constant ('URL');?>verificar">Verificación de Datos</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>verificar">Lista de Docentes</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Detalle Docente</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-6">
                    <div class="title-action">
                    <a href="<?php echo constant ('URL');?>verificar" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Volver</a> 

                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Listado de Experiencia Laboral del Docente en espera de Verificación <br><br><i class="fa fa-address-card-o"></i> <?php echo $this->detalleDocente->nacionalidad . "-" .$this->detalleDocente->identificacion. ' <br><i class="fa fa-user"></i> ' . $this->detalleDocente->primer_nombre . " ".$this->detalleDocente->primer_apellido;?></h5>
                        <div class="ibox-tools">

                 

                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="row" style="margin-bottom: 0.5em;">
                            
                            <div class="col-lg-6">
                                <label for=""><i class="fa fa-filter"></i> Filtrar Por: </label>
                                <div class="form-group">
                        
                                    <select name="" class="select2_demo_1" id="estudiosi" style="width: 100%;" onChange="mostrar(this.value);" >
                                        <option value="">Seleccione la categoría que desea listar</option>
                                        <option value="conducente">Estudios Conducentes a Grado</option>
                                        <option value="noconducente">Estudios No Conducentes a Grado</option>
                                        <option value="idiomas">Idiomas</option>
                                        <option value="administrativo">Trabajo Administrativo</option>
                                        <option value="docencia">Docencia Previa</option>
                                        <option value="comision">Comisión Gubernamental</option>
                                        <option value="ponencia">Ponencia</option>
                                        <option value="libro">Libro</option>
                                        <option value="artesoftware">Arte o Software</option>
                                        <option value="revista">Articulos o Revistas</option>
                                        <option value="reconocimiento">Reconocimientos</option>
                                        <option value="diseno">Diseño Unidad Curricular Docente</option>
                                        <option value="tutoria">Tutoria o Pasantias</option>
                                        <option value="docenciaubv">Docencia Previa UBV</option>
                                        <option value="comisionexc">Comision o Excedencia</option>
                                    </select>
                                </div>
                            
                            </div>
                            
                        </div>
                        
                        <!--  contenido -->
                        
                        <input type="text" class="form-control form-control-sm m-b-xs" id="filter"
                                   placeholder="&#128269; Buscar por cualquier Campo">
                            <!-- tabla estudios no conducentes a grado -->
                            <table id="testudio" class="table table-stripped table-hover" data-page-size="8" data-filter=#filter>
                                <thead>
                                    <tr>
                                        <th>Estudio</th>
                                        <th>Estado de Verificación</th>
                                        <th>Acción</th>
                                        <th data-hide="phone,tablet">Nombre Estudio</th>
                                        <th data-hide="phone,tablet">Modalidad de Estudio</th>
                                        <th data-hide="phone,tablet">Año de Estudio</th>
                                        <th data-hide="phone,tablet">Tipo de Estudio</th>
                                        <th data-hide="phone,tablet">Institución donde Realizó el Estudio</th>
                                        <th data-hide="phone,tablet">País de Realización</th>
                                        <th data-hide="phone,tablet">Tipo de Institución</th>
                                        <th data-hide="phone,tablet">Título Obtenido</th>
                                        <th data-hide="phone,tablet">Estado del Estudio</th>
                                        <th data-hide="phone,tablet">Título del TEG</th>
                                        <th data-hide="phone,tablet">Resumen del TEG</th>
                                        <th data-hide="phone,tablet">Verificar</th>
                                    </tr>
                        </thead>
                        <tbody>
                        <?php 
                                        foreach($this->datos_academicos as $row){
                                            $dato_academico= new Estructura();
                                            $dato_academico=$row;?>
                                    <tr class="gradeX">
                                        <td><?php echo $dato_academico->estudio; ?></td>
                                        <td id="<?php echo $dato_academico->id_estudio_conducente;?>">
                                        <?php if($dato_academico->estatus_verificacion=='Sin Verificar'){ echo '<span class="label label-danger" style="font-size: 12px;"><i class="fa fa-times"></i> '.ucwords($dato_academico->estatus_verificacion).'</span>'; } else{ echo '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> '.ucwords($dato_academico->estatus_verificacion).'</span>';} ?>                                        
                                        </td>
                                        <td>
                                        <?php if($dato_academico->estatus=="concluido"){?>
                                            <a target="_blank" href="<?php echo constant('URL') . "datos_academicos/viewDocumento/" . $dato_academico->id_estudio;?>" class="btn btn-primary"><i class="fa fa-external-link"></i> Certificado</a>                                                         
                                            <a  target="_blank" href="<?php echo constant('URL').'src/documentos/datos_academicos_idiomas/'. $dato_academico->descripcion_documento;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>                                        
                                        <?php } else{?>
                                            <span class="label label-warning" style="font-size: 12px;">En Curso...</span>
                                            <?php } ?>

                                        </td>
                                        <td><?php echo $dato_academico->estudio; ?></td>
                                        <td class="center"><?php echo $dato_academico->modalidad_estudio; ?></td>
                                        <td class="center"><?php echo $dato_academico->ano_estudio; ?></td>
                                        <td class="center"><?php echo $dato_academico->tipo_estudio; ?></td>
                                        <td class="center"><?php echo $dato_academico->institucion; ?></td>
                                        <td class="center"><?php echo $dato_academico->pais; ?></td>
                                        <td class="center"><?php echo $dato_academico->institucion_tipo; ?></td>
                                        <td class="center"><?php echo $dato_academico->titulo_obtenido; ?></td>
                                        <td class="center"><?php if($dato_academico->estatus=='concluido'){ echo '<span class="label label-primary" style="font-size: 12px;">'.ucwords($dato_academico->estatus).'</span>'; } else{ echo '<span class="label label-warning" style="font-size: 12px;">'.ucwords($dato_academico->estatus).'</span>';} ?></td>
                                        <?php if($dato_academico->id_tipo_estudio==1 && $dato_academico->estatus=='concluido'){?>
                                            <td class="center"><?php echo $dato_academico->titulo; ?></td>
                                            <td class="center"><?php echo $dato_academico->resumen; ?></td>
                                        <?php }else{ ?>
                                            <td>                                                
                                                <span class="label label-warning" style="font-size: 12px;">En Curso...</span>
                                            </td>
                                            <td>                                                
                                                <span class="label label-warning" style="font-size: 12px;">En Curso...</span>
                                            </td>
                                        <?php } ?>
                                            <td class="center">
                                            <button class="verificar btn btn-outline btn-success btn-sm"
                                                data-id="<?php echo $dato_academico->id_estudio_conducente;?>"
                                                data-id_estudio="<?php echo $dato_academico->id_estudio;?>" value="<?php echo $dato_academico->id_estudio;?>">
                                                <i class="fa fa-check"></i> Verificar
                                            </button>

                                            <!--<input type="button" class="verificar" value="Verificar">-->
                                            </td>
                                    </tr>
                                <?php 
                                    
                            }?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5">
                                        <ul class="pagination float-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                            <!-- fin tabla estudios no conducentes a grado -->

                        <!-- tabla estudios conducentes a grado -->
                        <table id="testudio-conducente" class="table table-stripped table-hover" data-page-size="8" data-filter=#filter>
                                <thead>
                                    <tr>
                                        <th>Estudio</th>
                                        <th>Estado de Verificación</th>
                                        <th>Acción</th>
                                        <th data-hide="phone,tablet">Nombre Estudio</th>
                                        <th data-hide="phone,tablet">Modalidad de Estudio</th>
                                        <th data-hide="phone,tablet">Nivel Académico</th>
                                        <th data-hide="phone,tablet">Tipo de Alto Nivel</th>
                                        <th data-hide="phone,tablet">Año de Estudio</th>
                                        <th data-hide="phone,tablet">Tipo de Estudio</th>
                                        <th data-hide="phone,tablet">Institución donde Realizó el Estudio</th>
                                        <th data-hide="phone,tablet">País de Realización</th>
                                        <th data-hide="phone,tablet">Tipo de Institución</th>
                                        <th data-hide="phone,tablet">Título Obtenido</th>
                                        <th data-hide="phone,tablet">Estado del Estudio</th>
                                        <th data-hide="phone,tablet">Título del TEG</th>
                                        <th data-hide="phone,tablet">Resumen del TEG</th>
                                        <th data-hide="phone,tablet">Verificar</th>
                                    </tr>
                        </thead>
                        <tbody>
                        <?php 
                                        foreach($this->conducentes as $row){
                                            $conducente= new Estructura();
                                            $conducente=$row;?>
                                    <tr class="gradeX">
                                        <td><?php echo $conducente->estudio; ?></td>
                                        <td id="<?php echo $conducente->id_estudio_conducente;?>">
                                        <?php if($conducente->estatus_verificacion=='Sin Verificar'){ echo '<span class="label label-danger" style="font-size: 12px;"><i class="fa fa-times"></i> '.ucwords($conducente->estatus_verificacion).'</span>'; } else{ echo '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> '.ucwords($conducente->estatus_verificacion).'</span>';} ?>                                        
                                        </td>
                                        <td>
                                        <?php if($conducente->estatus=="concluido"){?>
                                            <a target="_blank" href="<?php echo constant('URL') . "datos_academicos/viewDocumento/" . $conducente->id_estudio;?>" class="btn btn-primary"><i class="fa fa-external-link"></i> Certificado</a>                                                         
                                            <a  target="_blank" href="<?php echo constant('URL').'src/documentos/datos_academicos_idiomas/'. $conducente->descripcion_documento;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>                                        
                                        <?php } else{?>
                                            <span class="label label-warning" style="font-size: 12px;">En Curso...</span>
                                            <?php } ?>
                                        </td>
                                        <td><?php echo $conducente->estudio; ?></td>
                                        <td class="center"><?php echo $conducente->modalidad_estudio; ?></td>
                                        <td class="center"><?php echo $conducente->nivel_academico; ?></td>
                                        <td class="center"><?php echo $conducente->programa_nivel; ?></td>
                                        <td class="center"><?php echo $conducente->ano_estudio; ?></td>
                                        <td class="center"><?php echo $conducente->tipo_estudio; ?></td>
                                        <td class="center"><?php echo $conducente->institucion; ?></td>
                                        <td class="center"><?php echo $conducente->pais; ?></td>
                                        <td class="center"><?php echo $conducente->institucion_tipo; ?></td>
                                        <td class="center"><?php echo $conducente->titulo_obtenido; ?></td>
                                        <td class="center"><?php if($conducente->estatus=='concluido'){ echo '<span class="label label-primary" style="font-size: 12px;">'.ucwords($conducente->estatus).'</span>'; } else{ echo '<span class="label label-warning" style="font-size: 12px;">'.ucwords($conducente->estatus).'</span>';} ?></td>
                                        <?php if($conducente->id_tipo_estudio==1 && $conducente->estatus=='concluido'){?>
                                            <td class="center"><?php echo $conducente->titulo; ?></td>
                                            <td class="center"><?php echo $conducente->resumen; ?></td>
                                        <?php }else{ ?>
                                            <td>
                                                <span class="label label-warning" style="font-size: 12px;">En Curso...</span>
                                            </td>
                                            <td>
                                                <span class="label label-warning" style="font-size: 12px;">En Curso...</span>
                                            </td>
                                        <?php } ?>
                                            <td class="center">
                                            <button class="verificar btn btn-outline btn-success btn-sm"
                                                data-id="<?php echo $conducente->id_estudio_conducente;?>"
                                                data-id_estudio="<?php echo $conducente->id_estudio;?>" value="<?php echo $conducente->id_estudio;?>">
                                                <i class="fa fa-check"></i> Verificar
                                            </button>

                                            <!--<input type="button" class="verificar" value="Verificar">-->
                                            </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5">
                                        <ul class="pagination float-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                            <!-- fin tabla estudios conducentes a grado -->


                            <!-- tabla idiomas -->
                            <table id="tidioma" class="table table-stripped table-hover" data-page-size="8" data-filter=#filter>
                                <thead>
                                    <tr class="gradeX">
                                        <th>Idioma</th>
                                        <th>Estado de Verificación</th>
                                        <th>Acción</th>
                                        <th data-hide="phone,tablet">Nombre del Estudio</th>
                                        <th data-hide="phone,tablet">Modalidad de Estudio</th>
                                        <th data-hide="phone,tablet">Año de Estudio</th>
                                        <th data-hide="phone,tablet">Tipo de Estudio</th>
                                        <th data-hide="phone,tablet">Institución donde Realizó el Estudio</th>
                                        <th data-hide="phone,tablet">País de Realización</th>
                                        <th data-hide="phone,tablet">Tipo de Institución</th>
                                        <th data-hide="phone,tablet">Idioma Estudiado</th>
                                        <th data-hide="phone,tablet">Estado del Estudio</th>
                                        <th data-hide="phone,tablet">Nivel de Lectura</th>
                                        <th data-hide="phone,tablet">Nivel de Habla</th>
                                        <th data-hide="phone,tablet">Nivel de Escritura</th>
                                        <th data-hide="phone,tablet">Nivel de Comprensión</th>
                                        <th data-hide="phone,tablet">Verificar</th>
                                    </tr>
                                </thead>

                                <tbody>
                                <?php 
                                        foreach($this->idiomas as $row){
                                            $idioma= new Estructura();
                                            $idioma=$row;?>

                                    <tr class="gradeX">
                                        <td><?php echo $idioma->estudio; ?></td>
                                        <td id="<?php echo $idioma->id_estudio_idioma;?>">
                                        <?php if($idioma->estatus_verificacion=='Sin Verificar'){ echo '<span class="label label-danger" style="font-size: 12px;"><i class="fa fa-times"></i> '.ucwords($idioma->estatus_verificacion).'</span>'; } else{ echo '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> '.ucwords($idioma->estatus_verificacion).'</span>';} ?>                                        
                                        </td>
                                        <td>
                                        <?php if($idioma->estatus=="concluido"){?>
                                            <a target="_blank" href="<?php echo constant('URL') . "datos_academicos/viewDocumento/" . $idioma->id_estudio;?>" class="btn btn-primary"><i class="fa fa-external-link"></i> Certificado</a>                                                         
                                            <a  target="_blank" href="<?php echo constant('URL').'src/documentos/datos_academicos_idiomas/'.$idioma->descripcion_documento;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>                                        
                                        <?php } else{?>
                                            <span class="label label-warning" style="font-size: 12px;">En Curso...</span>
                                            <?php } ?>

                                        </td>
                                        <td><?php echo $idioma->estudio; ?></td>
                                        <td class="center"><?php echo $idioma->modalidad_estudio; ?></td>
                                        <td class="center"><?php echo $idioma->ano_estudio; ?></td>
                                        <td class="center"><?php echo $idioma->tipo_estudio; ?></td>
                                        <td class="center"><?php echo $idioma->institucion; ?></td>
                                        <td class="center"><?php echo $idioma->pais; ?></td>
                                        <td class="center"><?php echo $idioma->institucion_tipo; ?></td>
                                        <td class="center"><?php echo $idioma->idioma; ?></td>
                                        <td><?php if($idioma->estatus=='concluido'){ echo '<span class="label label-primary" style="font-size: 12px;">'.ucwords($idioma->estatus).'</span>'; } else{ echo '<span class="label label-warning" style="font-size: 12px;">'.ucwords($idioma->estatus).'</span>';} ?></td>

                                        <td class="center"><?php echo $idioma->nivel_lectura; ?></td>
                                        <td class="center"><?php echo $idioma->nivel_habla; ?></td>
                                        <td class="center"><?php echo $idioma->nivel_escritura; ?></td>
                                        <td class="center"><?php echo $idioma->nivel_comprende; ?></td>

                                       
                                            <td class="center">
                                            <button class="verificar btn btn-outline btn-success btn-sm"
                                                data-id="<?php echo $idioma->id_estudio_idioma;?>"
                                                data-id_estudio="<?php echo $idioma->id_estudio;?>" value="<?php echo $idioma->id_estudio;?>">
                                                <i class="fa fa-check"></i> Verificar
                                            </button>

                                            <!--<input type="button" class="verificar" value="Verificar"> -->
                                            </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5">
                                        <ul class="pagination float-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                            <!-- fin tabla idiomas -->


                        <!-- tabla administrativo -->

                        <table id="tadmin" class="table table-stripped" data-page-size="8" data-filter=#filter>
                                <thead>
                                <tr>
                                    <th>Cargo Ejercido</th>
                                    <th>Período</th>
                                    <th>Fecha Ingreso</th>
                                    <th>Fecha Egreso</th>
                                    <th>Estado de Verificación</th>
                                    <th>Acción</th>
                                    <th data-hide="phone,tablet">Tipo de Experiencia</th>
                                    <th data-hide="phone,tablet">Período</th>
                                    <th data-hide="phone,tablet">Cargo Ejercido</th>
                                    <th data-hide="phone,tablet">Año de Realización</th>
                                    <th data-hide="phone,tablet">Nombre de la Institución</th>
                                    <th data-hide="phone,tablet">Tipo de Institución</th>
                                    <th data-hide="phone,tablet">Estado donde se Ubica la Institución</th>
                                    <th data-hide="phone,tablet">Constancia de Trabajo</th>
                                    <th data-hide="phone,tablet">Fecha de Ingreso</th>
                                    <th data-hide="phone,tablet">Fecha de Egreso</th>
                                    <th data-hide="phone,tablet">Verificar</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        foreach($this->administrativos as $row){
                                            $admin= new Estructura();
                                            $admin=$row;?>
                                    <tr class="gradeX">
                                        <td><?php echo $admin->cargo; ?></td>
                                        <td><?php echo $admin->periodo; ?></td>
                                        <td><?php echo $admin->fecha_ingreso; ?></td>
                                        <td><?php echo $admin->fecha_egreso; ?></td>
                                        <td id="admin<?php echo $admin->id_trabajo_administrativo;?>">
                                            <?php if($admin->estatus_verificacion=='Sin Verificar'){
                                                    echo '<span class="label label-danger" style="font-size: 12px;"><i class="fa fa-check"></i> '.ucwords($admin->estatus_verificacion).'</span>'; 
                                                } else{
                                                    echo '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> '.ucwords($admin->estatus_verificacion).'</span>';
                                                } 
                                            ?>
                                        </td>
                                        <td><?php if($admin->id_institucion_tipo==1){?>
                                            <a target="_blank" href="<?php echo constant('URL') . "experiencia_laboral/viewDocumento/" . $admin->id_trabajo_administrativo;?>" class="btn btn-primary"><i class="fa fa-external-link"></i> Constancia</a>                                                         
                                            <a target="_blank"  href="<?php echo constant('URL').'src/documentos/experiencia_laboral/'. $admin->descripcion_documento;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>                                        
                                        <?php } else{?>
                                            N/A
                                            <?php } ?>
                                        </td>
                                        <td class="center"><?php echo $admin->tipo_experiencia; ?></td>
                                        <td class="center"><?php echo $admin->periodo; ?></td>
                                        <td class="center"><?php echo $admin->cargo; ?></td>
                                        <td class="center"><?php echo $admin->ano_experiencia; ?></td>
                                        <td class="center"><?php echo $admin->institucion; ?></td>
                                        <td class="center"><?php echo $admin->institucion_tipo; ?></td>
                                        <td class="center"><?php echo $admin->descripcion_estado; ?></td>
                                        <td class="center"><?php if($admin->id_institucion_tipo==1){echo "Haz Click en los botones de la Derecha para acceder";} else{echo "N/A";}?></td>
                                        <td class="center"><?php echo $admin->fecha_ingreso; ?></td>
                                        <td class="center"><?php echo $admin->fecha_egreso; ?></td>
                                        <td class="center">
                                            <button id="showsimple" class="verificar-exp btn btn-outline btn-success btn-sm"
                                                data-id_tabla="<?php echo $admin->id_trabajo_administrativo;?>"
                                                data-id_exp="<?php echo $admin->id_experiencia_docente;?>" 
                                                data-id_tipo_exp="<?php echo $admin->id_tipo_experiencia; ?>">
                                                <i class="fa fa-check"></i> Verificar
                                            </button>
                                        </td>

                                    </tr>
                                    <?php } ?>
     
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <ul class="pagination float-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                            <!-- fin tabla administrativo -->
                            <!-- tabla docencia -->

                            <table id="tdocencia" class="table table-stripped" data-page-size="8" data-filter=#filter>
                                <thead>
                                <tr>
                                <tr>
                                    <th>Institucion</th>
                                    <th>Tipo Institución</th>
                                    <th>Año</th>
                                    <th>Cargo</th>
                                    <th>Unidad Curricular</th>
                                    <th>Estado de Verificacion</th>

                                    <th data-hide="phone,tablet">Tipo de Experiencia</th>

                                    <th data-hide="phone,tablet">Cargo Experiencia</th>
                                    <th data-hide="phone,tablet">Año de Realización</th>
                                    <th data-hide="phone,tablet">Institución</th>
                                    <th data-hide="phone,tablet">Tipo de Institución</th>

                                    <th data-hide="phone,tablet">Estado</th>
                                    <th data-hide="phone,tablet">Fecha de Ingreso</th>
                                    <th data-hide="phone,tablet">Fecha de Egreso</th>

                                    <th data-hide="phone,tablet">Unidad Curricular</th>
                                    <th data-hide="phone,tablet">Nivel Académico</th>
                                    <th data-hide="phone,tablet">Modalidad de Estudio</th>
                                    <th data-hide="phone,tablet">Semestre</th>
                                    <th data-hide="phone,tablet">Horas Semanales</th>
                                    <th data-hide="phone,tablet">Nro. de Estudiantes Atendidos</th>
                                    <th data-hide="phone,tablet">Naturaleza</th>
                                    <th data-hide="phone,tablet">Período Lectivo</th>
                                    <th data-hide="phone,tablet">Verificar</th>

                                </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        foreach($this->docenciaspr as $row){
                                            $docencia= new Estructura();
                                            $docencia=$row;
                                    ?>
                                    <tr class="gradeX">
                                        <td><?php echo $docencia->institucion;?></td>
                                        <td><?php echo $docencia->institucion_tipo;?></td>
                                        <td><?php echo $docencia->ano_experiencia;?></td>
                                        <td><?php echo $docencia->cargo;?></td>
                                        <td><?php echo $docencia->materia;?></td>
                                        <td id="docencia<?php echo $docencia->id_docencia_previa;?>">
                                            <?php if($docencia->estatus_verificacion=='Sin Verificar'){
                                                    echo '<span class="label label-danger" style="font-size: 12px;"><i class="fa fa-check"></i> '.ucwords($docencia->estatus_verificacion).'</span>'; 
                                                } else{
                                                    echo '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> '.ucwords($docencia->estatus_verificacion).'</span>';
                                                } 
                                            ?>
                                        </td>
                                        <td class="center"><?php echo $docencia->tipo_experiencia;?></td>
                                        <td class="center"><?php echo $docencia->cargo;?></td>
                                        <td class="center"><?php echo $docencia->ano_experiencia;?></td>
                                        <td class="center"><?php echo $docencia->institucion;?></td>
                                        <td class="center"><?php echo $docencia->institucion_tipo;?></td>
                                        <td class="center"><?php echo $docencia->descripcion_estado;?></td>
                                        <td class="center"><?php echo $docencia->fecha_ingreso;?></td>
                                        <td class="center"><?php echo $docencia->fecha_egreso;?></td>
                                        <td class="center"><?php echo $docencia->materia;?></td>
                                        <td class="center"><?php echo $docencia->nivel_academico;?></td>
                                        <td class="center"><?php echo $docencia->modalidad_estudio;?></td>
                                        <td class="center"><?php echo $docencia->semestre;?></td>
                                        <td class="center"><?php echo $docencia->horas_semanales;?></td>
                                        <td class="center"><?php echo $docencia->estudiantes_atendidos;?></td>
                                        <td class="center"><?php echo $docencia->naturaleza;?></td>
                                        <td class="center">Período <?php echo $docencia->periodo_lectivo;?></td>
                                        <td class="center">
                                            <button class="verificar-exp btn btn-outline btn-success btn-sm"
                                                data-id_exp="<?php echo $docencia->id_experiencia_docente;?>"
                                                data-id_tabla="<?php echo $docencia->id_docencia_previa;?>"
                                                data-id_tipo_exp="<?php echo $docencia->id_tipo_experiencia;?>">
                                                <i class="fa fa-check"></i> Verificar
                                            </button>
                                        </td>
                                    </tr>
                                <?php } ?>

                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="7">
                                        <ul class="pagination float-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                            <!-- fin tabla docencia -->

                            <!-- tabla comisiones -->
                            <table id="tcomision" class="table table-stripped" data-page-size="8" data-filter=#filter>
                                <thead>
                                <tr>
                                    <th>Institucion</th>
                                    <th>Tipo Institución</th>
                                    <th>Año</th>
                                    <th>Cargo</th>
                                    <th>Lugar</th>
                                    <th>Estado de Verificacion</th>
                                    <th data-hide="phone,tablet">Tipo de Experiencia</th>
                                    <th data-hide="phone,tablet">Lugar de Prestación de Servicio</th>

                                    <th data-hide="phone,tablet">Cargo Experiencia</th>
                                    <th data-hide="phone,tablet">Año de Realización</th>
                                    <th data-hide="phone,tablet">Institución</th>
                                    <th data-hide="phone,tablet">Tipo de Institución</th>

                                    <th data-hide="phone,tablet">Estado</th>
                                    <th data-hide="phone,tablet">Fecha de Ingreso</th>
                                    <th data-hide="phone,tablet">Fecha de Egreso</th>
                                    <th data-hide="phone,tablet">Verificar</th>

                                </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        foreach($this->comisionesG as $row){
                                            $comision= new Estructura();
                                            $comision=$row;
                                    ?>
                                        <tr class="gradeX">
                                            <td><?php echo $comision->institucion;?></td>
                                            <td><?php echo $comision->institucion_tipo;?></td>
                                            <td><?php echo $comision->ano_experiencia;?></td>
                                            <td><?php echo $comision->cargo;?></td>
                                            <td><?php echo $comision->lugar;?></td>
                                            <td id="comision<?php echo $comision->id_comision_gubernamental;?>">
                                                <?php if($comision->estatus_verificacion=='Sin Verificar'){
                                                        echo '<span class="label label-danger" style="font-size: 12px;"><i class="fa fa-check"></i> '.ucwords($comision->estatus_verificacion).'</span>'; 
                                                    } else{
                                                        echo '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> '.ucwords($comision->estatus_verificacion).'</span>';
                                                    } 
                                                ?> 
                                            </td>
                                            <td class="center"><?php echo $comision->tipo_experiencia;?></td>
                                            <td class="center"><?php echo $comision->lugar;?></td>
                                            <td class="center"><?php echo $comision->cargo;?></td>
                                            <td class="center"><?php echo $comision->ano_experiencia;?></td>
                                            <td class="center"><?php echo $comision->institucion;?></td>
                                            <td class="center"><?php echo $comision->institucion_tipo;?></td>
                                            <td class="center"><?php echo $comision->descripcion_estado;?></td>
                                            <td class="center"><?php echo $comision->fecha_ingreso;?></td>
                                            <td class="center"><?php echo $comision->fecha_egreso;?></td>
                                            <td class="center">
                                                <button class="verificar-exp btn btn-outline btn-success btn-sm"
                                                    data-id_exp="<?php echo $comision->id_experiencia_docente;?>"
                                                    data-id_tabla="<?php echo $comision->id_comision_gubernamental;?>"
                                                    data-id_tipo_exp="<?php echo $comision->id_tipo_experiencia;?>">
                                                    <i class="fa fa-check"></i> Verificar
                                                </button>
                                            </td>

                                        </tr>
                                    <?php } ?>

                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5">
                                        <ul class="pagination float-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                            <!-- fin tabla comisiones -->




  <!-- tabla libro -->

  <table id="tlibro" class="table table-stripped table-hover" data-page-size="8" data-filter=#filter>
                                <thead>
                                     <tr class="gradeX">
                                        <th>Nombre del Libro</th>
                                        <th>Estado de Verificación</th>
                                        
                                        
                                        <th>Acción</th>
                                      
                                        <th data-hide="phone,tablet">Editorial</th>
                                        <th data-hide="phone,tablet">ISBN del Libro</th>
                                        <th data-hide="phone,tablet">Año de publicación del Libro</th>
                                        <th data-hide="phone,tablet">Ciudad de publicación Libro</th>
                                        <th data-hide="phone,tablet">Volumen del Libro</th>
                                        <th data-hide="phone,tablet">Número de paginas del Libro</th>
                                        <th data-hide="phone,tablet">URL del Libro</th>
                                        <th data-hide="phone,tablet">Verificar</th>
                                        
                                     </tr>
                                </thead>

                                <tbody>
                                <?php 
                                          foreach($this->libro as $row){
                                            $registrolibro= new Estructura();
                                            $registrolibro=$row;
                                            ?>

                                    <tr class="gradeX">
                                    
                                        <td><?php echo $registrolibro->nombre_libro; ?></td>
                                        <td id="libro<?php echo $registrolibro->id_libro;?>">
                                        <?php if($registrolibro->estatus_verificacion=='Sin Verificar'){ echo '<span class="label label-danger" style="font-size: 12px;"><i class="fa fa-times"></i> '.ucwords($registrolibro->estatus_verificacion).'</span>'; } else{ echo '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> '.ucwords($registrolibro->estatus_verificacion).'</span>';} ?>                                        
                                        </td>
                                        <td>
                                            N/A
                
                                        </td>
                                      
                                        <td class="center"><?php echo $registrolibro->editorial; ?></td>
                                        <td class="center"><?php echo $registrolibro->isbn_libro; ?></td>
                                        <td class="center"><?php echo $registrolibro->ano_publicacion; ?></td>
                                        <td class="center"><?php echo $registrolibro->ciudad_publicacion; ?></td>
                                        <td class="center"><?php echo $registrolibro->volumenes; ?></td>
                                        <td class="center"><?php echo $registrolibro->nro_paginas; ?></td>
                                        <td class="center"><?php echo $registrolibro->url_libro; ?></td>
                                 

                                       
                                            <td class="center">
                                            <button class="verificar-libro btn btn-outline btn-success btn-sm"
                                              
                                                data-id_libro="<?php echo $registrolibro->id_libro;?>" value="<?php echo $registrolibro->id_libro;?>">
                                                <i class="fa fa-check"></i> Verificar
                                            </button>

                                            <!--<input type="button" class="verificar" value="Verificar"> -->
                                            </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5">
                                        <ul class="pagination float-right"></ul>
                                    </td>
                                </tr>


                                </tfoot>
                            </table>
                <!-- fin tabla libro -->




  <!-- tabla articulo o revista -->

  <table id="trevista" class="table table-stripped table-hover" data-page-size="8" data-filter=#filter>
                                <thead>
                                     <tr class="gradeX">
                                     <th>Nombre del Artículo o Revista</th>
                                        <th>Estado de Verificación</th>
                                        
                                        
                                        <th>Acción</th>
                                        
                                        <th data-hide="phone,tablet">Título</th>
                                        <th data-hide="phone,tablet">ISNN de la revista</th>
                                        <th data-hide="phone,tablet">Año de la revista</th>
                                        <th data-hide="phone,tablet">Número de la revista</th>
                                        <th data-hide="phone,tablet">Numero de página inicial</th>
                                        <th data-hide="phone,tablet">URL de la revista</th>
                                        <th data-hide="phone,tablet">Verificar</th>
                                        
                                     </tr>
                                </thead>

                                <tbody>
                                <?php 
                                          foreach($this->revista as $row){
                                            $articulorevista= new Estructura();
                                            $articulorevista=$row;
                                            ?>

                                    <tr class="gradeX">
                                    
                                        <td><?php echo $articulorevista->nombre_revista; ?></td>
                                        <td id="revista<?php echo $articulorevista->id_articulo_revista;?>">
                                        <?php if($articulorevista->estatus_verificacion=='Sin Verificar'){ echo '<span class="label label-danger" style="font-size: 12px;"><i class="fa fa-times"></i> '.ucwords($articulorevista->estatus_verificacion).'</span>'; } else{ echo '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> '.ucwords($articulorevista->estatus_verificacion).'</span>';} ?>                                        
                                        </td>
                                        <td>
                                            N/A
                
                                        </td>
                                        
                                        <td class="center"><?php echo $articulorevista->titulo_articulo; ?></td>
                                        <td class="center"><?php echo $articulorevista->issn_revista; ?></td>
                                        <td class="center"><?php echo $articulorevista->ano_revista; ?></td>
                                        <td class="center"><?php echo $articulorevista->nro_revista; ?></td>
                                        <td class="center"><?php echo $articulorevista->nro_pag_inicial; ?></td>
                                        
                                        <td class="center"><?php echo $articulorevista->url_revista; ?></td>
                                 

                                       
                                            <td class="center">
                                            <button class="verificar-revista btn btn-outline btn-success btn-sm"
                                              
                                                data-id_articulo_revista="<?php echo $articulorevista->id_articulo_revista;?>" value="<?php echo $articulorevista->id_articulorevista;?>">
                                                <i class="fa fa-check"></i> Verificar
                                            </button>

                                            <!--<input type="button" class="verificar" value="Verificar"> -->
                                            </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5">
                                        <ul class="pagination float-right"></ul>
                                    </td>
                                </tr>


                                </tfoot>
                            </table>
                <!-- fin tabla articulo revista -->




<!-- tabla arte o software -->

<table id="tartesoftware" class="table table-stripped table-hover" data-page-size="8" data-filter=#filter>
                                <thead>
                                     <tr class="gradeX">
                                        <th>Arte o Software</th>
                                        <th>Estado de Verificación</th>
                                        <th>Acción</th>
                                       
                                        <th data-hide="phone,tablet">Año de realización</th>
                                        <th data-hide="phone,tablet">Entidad Promotora</th>
                                        <th data-hide="phone,tablet">URL (Opcional)</th>
                                        <th data-hide="phone,tablet">Verificar</th>
                                        
                                     </tr>
                                </thead>

                                <tbody>
                                <?php 
                                     foreach($this->arte as $row){
                                        $artesoftware= new Estructura();
                                        $artesoftware=$row;?>

                                    <tr class="gradeX">
                                    
                                        <td><?php echo $artesoftware->nombre_arte; ?></td>
                                        <td id="artesoftware<?php echo $artesoftware->id_arte_software;?>">
                                        <?php if($artesoftware->estatus_verificacion=='Sin Verificar'){ echo '<span class="label label-danger" style="font-size: 12px;"><i class="fa fa-times"></i> '.ucwords($artesoftware->estatus_verificacion).'</span>'; } else{ echo '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> '.ucwords($artesoftware->estatus_verificacion).'</span>';} ?>                                        
                                        </td>
                                        <td>
                                            N/A
                
                                        </td>
                                        
                                        <td class="center"><?php echo $artesoftware->ano_arte; ?></td>
                                        <td class="center"><?php echo $artesoftware->entidad_promotora; ?></td>
                                        <td class="center"><?php echo $artesoftware->url_otro; ?></td>
                                       
                                 

                                       
                                            <td class="center">
                                            <button class="verificar-artesoftware btn btn-outline btn-success btn-sm"
                                              
                                                data-id_arte_software="<?php echo $artesoftware->id_arte_software;?>" value="<?php echo $artesoftware->id_arte_software;?>">
                                                <i class="fa fa-check"></i> Verificar
                                            </button>

                                            <!--<input type="button" class="verificar" value="Verificar"> -->
                                            </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5">
                                        <ul class="pagination float-right"></ul>
                                    </td>
                                </tr>


                                </tfoot>
                            </table>
                <!-- fin tabla arte o software -->



<!-- tabla ponencia -->

<table id="tponencia" class="table table-stripped table-hover" data-page-size="8" data-filter=#filter>
                                <thead>
                                     <tr class="gradeX">
                                        <th>Ponencia</th>
                                        <th>Estado de Verificación</th>
                                        <th>Acción</th>
                                      
                                        <th data-hide="phone,tablet">Nombre del evento</th>
                                        <th data-hide="phone,tablet">Año de la ponencia</th>
                                        <th data-hide="phone,tablet">Ciudad de la ponencia</th>
                                        <th data-hide="phone,tablet">Título de la memoria del Evento</th>
                                        <th data-hide="phone,tablet">Volumen</th>
                                        <th data-hide="phone,tablet">ISBN o ISSN</th>
                                        <th data-hide="phone,tablet">Página Inicial</th>
                                        <th data-hide="phone,tablet">Formato de la ponencia</th>
                                        <th data-hide="phone,tablet">URL (Opcional)</th>
                                        <th data-hide="phone,tablet">Verificar</th>
                                        
                                     </tr>
                                </thead>

                                <tbody>
                                <?php 
                                          foreach($this->ponencia as $row){
                                            $registroponencia= new Estructura();
                                            $registroponencia=$row;
                                            ?>

                                    <tr class="gradeX">
                                    
                                        <td><?php echo $registroponencia->nombre_ponencia; ?></td>
                                        <td id="ponencia<?php echo $registroponencia->id_ponencia;?>">
                                        <?php if($registroponencia->estatus_verificacion=='Sin Verificar'){ echo '<span class="label label-danger" style="font-size: 12px;"><i class="fa fa-times"></i> '.ucwords($registroponencia->estatus_verificacion).'</span>'; } else{ echo '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> '.ucwords($registroponencia->estatus_verificacion).'</span>';} ?>                                        
                                        </td>
                                        <td>
                                            N/A
                
                                        </td>
                                        
                                        <td class="center"><?php echo $registroponencia->nombre_evento; ?></td>
                                        <td class="center"><?php echo $registroponencia->ano_ponencia; ?></td>
                                        <td class="center"><?php echo $registroponencia->ciudad; ?></td>
                                        <td class="center"><?php echo $registroponencia->titulo_memoria; ?></td>
                                        <td class="center"><?php echo $registroponencia->volumen; ?></td>
                                        <td class="center"><?php echo $registroponencia->identificador; ?></td>
                                        <td class="center"><?php echo $registroponencia->pag_inicial; ?></td>
                                        <td class="center"><?php echo $registroponencia->formato; ?></td>
                                        <td class="center"><?php echo $registroponencia->url_ponencia; ?></td>
                                 

                                       
                                            <td class="center">
                                            <button class="verificar-ponencia btn btn-outline btn-success btn-sm"
                                              
                                                data-id_ponencia="<?php echo $registroponencia->id_ponencia;?>" value="<?php echo $registroponencia->id_ponencia;?>">
                                                <i class="fa fa-check"></i> Verificar
                                            </button>

                                            <!--<input type="button" class="verificar" value="Verificar"> -->
                                            </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5">
                                        <ul class="pagination float-right"></ul>
                                    </td>
                                </tr>


                                </tfoot>
                            </table>
                <!-- fin tabla ponencia -->

                <!-- tabla Diseño Unidad Curricular Docente -->

<table id="tdiseno" class="table table-stripped table-hover" data-page-size="8" data-filter=#filter>
                                <thead>
                                     <tr class="gradeX">
                                        <th>Diseño Unidad Curricular Docente</th>
                                        <th>Estado de Verificación</th>
                                        <th>Acción</th>
                                        <th data-hide="phone,tablet">Año de Creación</th>
                                        <th data-hide="phone,tablet">Tramo</th>
                                        <th data-hide="phone,tablet">Unidad Curricular Tipo</th>
                                        <th data-hide="phone,tablet">Programa de Formación</th>
                                        <th data-hide="phone,tablet">Area Conocimiento UBV</th>
                                        <th data-hide="phone,tablet">Documento</th>
                                        <th data-hide="phone,tablet">Verificar</th>
                                        
                                     </tr>
                                </thead>

                                <tbody>
                                <?php 
                                     foreach($this->disenos as $row){
                                        $diseno_uc_docente= new Estructura();
                                        $diseno_uc_docente=$row;?>

                                    <tr class="gradeX">
                                    
                                        <td><?php echo $diseno_uc_docente->nombre; ?></td>
                                        <td id="diseno<?php echo $diseno_uc_docente->id_diseno_uc;?>">
                                        <?php if($diseno_uc_docente->estatus_verificacion=='Sin Verificar'){ echo '<span class="label label-danger" style="font-size: 12px;"><i class="fa fa-times"></i> '.ucwords($diseno_uc_docente->estatus_verificacion).'</span>'; } else{ echo '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> '.ucwords($diseno_uc_docente->estatus_verificacion).'</span>';} ?>                                        
                                        </td>
                                        <td><?php if($admin->id_institucion_tipo==1){?>
                                            <a target="_blank" href="<?php echo constant('URL') . "diseno_uc/viewDocumento/" . $this->disenos->id_diseno_uc;?>" class="btn btn-primary"><i class="fa fa-external-link"></i> Constancia</a>                                                         
                                            <a target="_blank" href="<?php echo constant('URL').'src/documentos/diseno/'.$this->disenos->descripcion_documento;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>
                                            
                                            <?php } else{?>
                                            N/A
                                            <?php } ?>
                                        </td>
                                        <td class="center"><?php echo $diseno_uc_docente->ano_creacion; ?></td>
                                        <td class="center"><?php echo $diseno_uc_docente->descripcion_tramo; ?></td>
                                        <td class="center"><?php echo $diseno_uc_docente->descripcion_tipo_uc; ?></td>
                                        <td class="center"><?php echo $diseno_uc_docente->descripcion_programa; ?></td>
                                        <td class="center"><?php echo $diseno_uc_docente->descripcion_area_ubv; ?></td>
                                        <td class="center"><?php echo $diseno_uc_docente->descripcion_documento; ?></td>

                                       
                                            <td class="center">
                                            <button class="verificar-diseno btn btn-outline btn-success btn-sm"
                                              
                                                data-id_diseno_uc="<?php echo $diseno_uc_docente->id_diseno_uc;?>" value="<?php echo $diseno_uc_docente->id_diseno_uc;?>">
                                                <i class="fa fa-check"></i> Verificar
                                            </button>

                                            <!--<input type="button" class="verificar" value="Verificar"> -->
                                            </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5">
                                        <ul class="pagination float-right"></ul>
                                    </td>
                                </tr>


                                </tfoot>
                            </table>
                <!-- fin tabla Diseño Unidad Curricular Docente --> 
                
                 <!-- tabla Tutoria o Pasantia -->

<table id="ttutoria" class="table table-stripped table-hover" data-page-size="8" data-filter=#filter>
                                <thead>
                                     <tr class="gradeX">
                                        <th>Tutoria o Pasantia</th>
                                        <th>Estado de Verificación</th>
                                        <th>Acción</th>
                                        <th data-hide="phone,tablet">Año de Tutoria</th>
                                        <th data-hide="phone,tablet">Periodo Lectivo</th>
                                        <th data-hide="phone,tablet">Area Conocimiento UBV</th>
                                        <th data-hide="phone,tablet">Linea de Investigación</th>
                                        <th data-hide="phone,tablet">Nucleo Academico</th>
                                        <th data-hide="phone,tablet">Centro de Estudios</th>
                                        <th data-hide="phone,tablet">Programa de Formación</th>
                                        <th data-hide="phone,tablet">Eje Regional</th>
                                        <th data-hide="phone,tablet">Numero de Estudiantes</th>
                                        <th data-hide="phone,tablet">Verificar</th>
                                        
                                     </tr>
                                </thead>

                                <tbody>
                                <?php 
                                     foreach($this->tutorias as $row){
                                        $tutoria= new Estructura();
                                        $tutoria=$row;?>

                                    <tr class="gradeX">
                                    
                                        <td><?php echo $tutoria->descripcion; ?></td>
                                        <td id="tutoria<?php echo $tutoria->id_tutoria;?>">
                                        <?php if($tutoria->estatus_verificacion=='Sin Verificar'){ echo '<span class="label label-danger" style="font-size: 12px;"><i class="fa fa-times"></i> '.ucwords($tutoria->estatus_verificacion).'</span>'; } else{ echo '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> '.ucwords($tutoria->estatus_verificacion).'</span>';} ?>                                        
                                        </td>
                                        <td>
                                            N/A
                
                                        </td>
                                        <td class="center"><?php echo $tutoria->ano_tutoria; ?></td>
                                        <td class="center"><?php echo $tutoria->periodo_lectivo; ?></td>
                                        <td class="center"><?php echo $tutoria->descripcion_area_ubv; ?></td>
                                        <td class="center"><?php echo $tutoria->descripcion_linea; ?></td>
                                        <td class="center"><?php echo $tutoria->descripcion_nucleo; ?></td>
                                        <td class="center"><?php echo $tutoria->descripcion_centro; ?></td>
                                        <td class="center"><?php echo $tutoria->descripcion_programa; ?></td>
                                        <td class="center"><?php echo $tutoria->descripcion_eje_regional; ?></td>
                                        <td class="center"><?php echo $tutoria->nro_estudiantes; ?></td>

                                       
                                            <td class="center">
                                            <button class="verificar-tutoria btn btn-outline btn-success btn-sm"
                                              
                                                data-id_tutoria="<?php echo $tutoria->id_tutoria;?>" value="<?php echo $tutoria->id_tutoria;?>">
                                                <i class="fa fa-check"></i> Verificar
                                            </button>

                                            <!--<input type="button" class="verificar" value="Verificar"> -->
                                            </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5">
                                        <ul class="pagination float-right"></ul>
                                    </td>
                                </tr>


                                </tfoot>
                            </table>
                <!-- fin tabla Tutoria o Pasantia -->                            

                 <!-- tabla Docencia Previa UBV -->

                            <table id="tdocenciaubv" class="table table-stripped table-hover" data-page-size="8" data-filter=#filter>
                                <thead>
                                     <tr class="gradeX">
                                        <th>Docencia Previa UBV</th>
                                        <th>Estado de Verificación</th>
                                        <th>Acción</th>
                                        <th data-hide="phone,tablet">Area Conocimiento UBV</th>
                                        <th data-hide="phone,tablet">Programa de Formación</th>
                                        <th data-hide="phone,tablet">Eje Regional</th>
                                        <th data-hide="phone,tablet">Eje Municipal</th>
                                        <th data-hide="phone,tablet">Aldea</th>
                                        <th data-hide="phone,tablet">Centro de Estudios</th>
                                        <th data-hide="phone,tablet">Fecha de Ingreso</th>
                                        <th data-hide="phone,tablet">Verificar</th>
                                        
                                     </tr>
                                </thead>

                                <tbody>
                                <?php 
                                     foreach($this->docencias as $row){
                                        $docencia_previa_ubv= new Estructura();
                                        $docencia_previa_ubv=$row;?>

                                    <tr class="gradeX">
                                    
                                        <td><?php echo $docencia_previa_ubv->descripcion_area_ubv; ?></td>
                                        <td id="docenciaubv<?php echo $docencia_previa_ubv->id_docencia_previa_ubv;?>">
                                        <?php if($docencia_previa_ubv->estatus_verificacion=='Sin Verificar'){ echo '<span class="label label-danger" style="font-size: 12px;"><i class="fa fa-times"></i> '.ucwords($docencia_previa_ubv->estatus_verificacion).'</span>'; } else{ echo '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> '.ucwords($docencia_previa_ubv->estatus_verificacion).'</span>';} ?>                                        
                                        </td>
                                        <td>
                                            N/A
                
                                        </td>
                                        <td class="center"><?php echo $docencia_previa_ubv->descripcion_programa; ?></td>
                                        <td class="center"><?php echo $docencia_previa_ubv->descripcion_eje_regional; ?></td>
                                        <td class="center"><?php echo $docencia_previa_ubv->descripcion_eje_municipal; ?></td>
                                        <td class="center"><?php echo $docencia_previa_ubv->descripcion_aldea; ?></td>
                                        <td class="center"><?php echo $docencia_previa_ubv->descripcion_centro; ?></td>
                                        <td class="center"><?php echo $docencia_previa_ubv->fecha_ingreso; ?></td>
                                        

                                       
                                            <td class="center">
                                            <button class="verificar-docencia btn btn-outline btn-success btn-sm"
                                              
                                                data-id_docencia_previa_ubv="<?php echo $docencia_previa_ubv->id_docencia_previa_ubv;?>" value="<?php echo $docencia_previa_ubv->id_docencia_previa_ubv;?>">
                                                <i class="fa fa-check"></i> Verificar
                                            </button>

                                            <!--<input type="button" class="verificar" value="Verificar"> -->
                                            </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5">
                                        <ul class="pagination float-right"></ul>
                                    </td>
                                </tr>


                                </tfoot>
                            </table>
                <!-- fin tabla Docencia Previa UBV -->                            

  <!-- tabla Comision o Excedencia -->

  <table id="tcomisionexc" class="table table-stripped table-hover" data-page-size="8" data-filter=#filter>
                                <thead>
                                     <tr class="gradeX">
                                        <th>Comision o Excedencia</th>
                                        <th>Estado de Verificación</th>
                                        <th>Acción</th>
                                        <th data-hide="phone,tablet">Lugar</th>
                                        <th data-hide="phone,tablet">Desde</th>
                                        <th data-hide="phone,tablet">Hasta</th>
                                        <th data-hide="phone,tablet">Designación de Función</th>
                                        <th data-hide="phone,tablet">Causa</th>
                                        <th data-hide="phone,tablet">Aprobación</th>
                                        <th data-hide="phone,tablet">Verificar</th>
                                        
                                     </tr>
                                </thead>

                                <tbody>
                                <?php 
                                     foreach($this->comisiones as $row){
                                        $comision_excedencia= new Estructura();
                                        $comision_excedencia=$row;?>

                                    <tr class="gradeX">
                                    
                                        <td><?php echo $comision_excedencia->descripcion_tipo_info; ?></td>
                                        <td id="comisionexc<?php echo $comision_excedencia->id_comision_excedencia;?>">
                                        <?php if($comision_excedencia->estatus_verificacion=='Sin Verificar'){ echo '<span class="label label-danger" style="font-size: 12px;"><i class="fa fa-times"></i> '.ucwords($comision_excedencia->estatus_verificacion).'</span>'; } else{ echo '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> '.ucwords($comision_excedencia->estatus_verificacion).'</span>';} ?>                                        
                                        </td>
                                        <td>
                                            N/A
                
                                        </td>
                                        <td class="center"><?php echo $comision_excedencia->lugar; ?></td>
                                        <td class="center"><?php echo $comision_excedencia->desde; ?></td>
                                        <td class="center"><?php echo $comision_excedencia->hasta; ?></td>
                                        <td class="center"><?php echo $comision_excedencia->designacion_funcion; ?></td>
                                        <td class="center"><?php echo $comision_excedencia->causa; ?></td>
                                        <td class="center"><?php echo $comision_excedencia->aprobacion; ?></td>

                                       
                                            <td class="center">
                                            <button class="verificar-comisionexc btn btn-outline btn-success btn-sm"
                                              
                                                data-id_comision_excedencia="<?php echo $comision_excedencia->id_comision_excedencia;?>" value="<?php echo $comision_excedencia->id_comision_excedencia;?>">
                                                <i class="fa fa-check"></i> Verificar
                                            </button>

                                            <!--<input type="button" class="verificar" value="Verificar"> -->
                                            </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5">
                                        <ul class="pagination float-right"></ul>
                                    </td>
                                </tr>


                                </tfoot>
                            </table>
                <!-- fin tabla Comision o Excedencia --> 


<!-- tabla Reconocimiento -->

<table id="treconocimiento" class="table table-stripped table-hover" data-page-size="8" data-filter=#filter>
                                <thead>
                                     <tr class="gradeX">
                                     <th>Tipo de reconocimiento</th>
                                       
                                        <th>Sin Verificación</th>
                                        <th>Acción</th>
                                        <th data-hide="phone,tablet">Nombre del reconocimiento</th>
                                        <th data-hide="phone,tablet">Entidad promotora</th>
                                        <th data-hide="phone,tablet">Año del Reconocimiento</th>
                                        <th data-hide="phone,tablet">Carácter</th>
                                        <th data-hide="phone,tablet">Documento</th>
                                        
                                        <th data-hide="phone,tablet">Verificar</th>
                                        
                                     </tr>
                                </thead>

                                <tbody>
                                <?php 
                                          foreach($this->tipos_reconocimiento as $row){
                                                    $tipo_reconocimiento=new Estructura();
                                                    $tipo_reconocimiento=$row;?> 

                                    <tr class="gradeX">
                                 
                                        <td><?php echo $tipo_reconocimiento->descripcion_tipo; ?></td>
                                        <td id="reconocimiento<?php echo $tipo_reconocimiento->id_reconocimiento;?>">
                                        <?php if($tipo_reconocimiento->estatus_verificacion=='Sin Verificar'){ echo '<span class="label label-danger" style="font-size: 12px;"><i class="fa fa-times"></i> '.ucwords($tipo_reconocimiento->estatus_verificacion).'</span>'; } else{ echo '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> '.ucwords($tipo_reconocimiento->estatus_verificacion).'</span>';} ?>                                        
                                        </td>
                                        <td>
                                            <a target="_blank" href="<?php echo constant('URL') . "registro_reconocimiento/viewDocumento/" . $tipo_reconocimiento->id_reconocimiento;?>" class="btn btn-primary"><i class="fa fa-external-link"></i> Ver Documento</a>                                                         
                                            <a target="_blank" href="<?php echo constant('URL').'src/documentos/reconocimientos/'. $tipo_reconocimiento->descripcion_reconocimiento;?>"  class="btn btn-info"><i class="fa fa-download"></i> Descargar</a>

                                        </td>
                                        <td><?php echo $tipo_reconocimiento->descripcion; ?></td>
                                        <td class="center"><?php echo $tipo_reconocimiento->entidad_promotora; ?></td>
                                        <td class="center"><?php echo $tipo_reconocimiento->ano_reconocimiento; ?></td>
                                        <td class="center"><?php echo $tipo_reconocimiento->caracter; ?></td>
                                        <td class="center"><?php echo $tipo_reconocimiento->id_documento; ?></td>
                                       
                                       
                             
                                            <td class="center">
                                            <button class="verificar-reconocimiento btn btn-outline btn-success btn-sm"
                                              
                                                data-id_reconocimiento="<?php echo $tipo_reconocimiento->id_reconocimiento;?>" value="<?php echo $tipo_reconocimiento->id_reconocimiento;?>">
                                                <i class="fa fa-check"></i> Verificar
                                            </button>

                                            <!--<input type="button" class="verificar" value="Verificar"> -->
                                            </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5">
                                        <ul class="pagination float-right"></ul>
                                    </td>
                                </tr>


                                </tfoot>
                            </table>
                <!-- fin tabla Reconocimiento -->


                            <!-- tabla blanco -->

                            <table id="tblanco" class="table table-stripped" data-page-size="8" data-filter=#filter>
                                <thead>
                                <tr>
                                    <th>N/A</th>
                                    <th>N/A</th>
                                    <th>N/A</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="gradeX">
                                    <td colspan="3"><div class="text-center alert alert-info">Selecciona una opción para filtrar</div></td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5">
                                        <ul class="pagination float-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                            <!-- fin tabla blanco -->


                            <!-- end contenido -->

                    </div>
                </div>
            </div>
            </div>
        </div>



<?php require 'views/footer.php'; ?>

    <!-- FooTable -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/footable/footable.all.min.js"></script>

    <!-- Toastr script -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/toastr/toastr.min.js"></script>
       <!-- Select2 -->
       <script src="<?php echo constant ('URL');?>src/js/plugins/select2/select2.full.min.js"></script>

    
    <!-- Page-Level Scripts -->
    <script>


        
        $(document).ready(function() {
            
           
                            $(".select2_demo_1").select2({
                                placeholder: "Seleccione la Categoría que desea Verificar",
                                allowClear: true
                            });
                   
            //obtener valor del select para obtener la 
            var id=$("#estudiosi").val();
            mostrar(id);
            
            
            $(".verificar").click(function(){
                var product = $(this).data('id_estudio');
                var id_estudio_tabla = $(this).data('id');



                $.post("<?php echo constant ('URL');?>verificar/cambiar", {estatus: "Verificado", id: product}, function(htmlexterno){
                    $("td#"+id_estudio_tabla).html(htmlexterno);
                    toastr.success('Verificación Realizada Exitosamente','¡Notificación!')
                    toastr.options= {
                            progressBar: true,
                            positionClass: 'toast-bottom-right'
                        };
                });
               
            });

            $(".verificar-exp").click(function(){
                var id_experiencia_docente = $(this).data('id_exp');//para cambiar el estatus
                var id_tabla = $(this).data('id_tabla');//para pintar el campo
                var id_tipo_experiencia = $(this).data('id_tipo_exp');

                $.post("<?php echo constant ('URL');?>verificar/cambiarExp", {estatus: "Verificado", id: id_experiencia_docente}, function(htmlexterno){
                   
                
                    switch(id_tipo_experiencia){
                        case 1:
                            $("td#admin"+id_tabla).html(htmlexterno);
                            break;
                        case 2:
                            $("td#docencia"+id_tabla).html(htmlexterno);
                            break;
                        case 3:
                            $("td#comision"+id_tabla).html(htmlexterno);
                            break;
                    }

                    // Display a success toast, with a title
                    toastr.success('Verificación Realizada Exitosamente','¡Notificación!')
                    toastr.options= {
                        progressBar: true,
                        positionClass: 'toast-bottom-right'
                    };
                });
            });



            $(".verificar-libro").click(function(){
                console.log("click");
                var id_libro = $(this).data('id_libro');//para cambiar el estatus
                console.log(id_libro);
                

                $.post("<?php echo constant ('URL');?>verificar/cambiarLibro", {estatus: "Verificado", id: id_libro}, function(htmlexterno){
                    $("td#libro"+id_libro).html(htmlexterno);



                
                    // Display a success toast, with a title
                    toastr.success('Verificación Realizada Exitosamente','¡Notificación!')
                    toastr.options= {
                        progressBar: true,
                        positionClass: 'toast-bottom-right'
                    };
                });
            });


            $(".verificar-revista").click(function(){
                console.log("click");
                var id_articulo_revista = $(this).data('id_articulo_revista');//para cambiar el estatus
                console.log(id_articulo_revista);
                

                $.post("<?php echo constant ('URL');?>verificar/cambiarRevista", {estatus: "Verificado", id: id_articulo_revista}, function(htmlexterno){
                    $("td#revista"+id_articulo_revista).html(htmlexterno);


                    // Display a success toast, with a title
                    toastr.success('Verificación Realizada Exitosamente','¡Notificación!')
                    toastr.options= {
                        progressBar: true,
                        positionClass: 'toast-bottom-right'
                    };
                });
            });


            $(".verificar-artesoftware").click(function(){
                console.log("click");
                var id_arte_software = $(this).data('id_arte_software');//para cambiar el estatus
                console.log(id_arte_software);
                

                $.post("<?php echo constant ('URL');?>verificar/cambiarArtesoftware", {estatus: "Verificado", id: id_arte_software}, function(htmlexterno){
                    $("td#artesoftware"+id_arte_software).html(htmlexterno);


                    // Display a success toast, with a title
                    toastr.success('Verificación Realizada Exitosamente','¡Notificación!')
                    toastr.options= {
                        progressBar: true,
                        positionClass: 'toast-bottom-right'
                    };
                });
            });


            $(".verificar-ponencia").click(function(){
                console.log("click");
                var id_ponencia = $(this).data('id_ponencia');//para cambiar el estatus
                console.log(id_ponencia);
                

                $.post("<?php echo constant ('URL');?>verificar/cambiarPonencia", {estatus: "Verificado", id: id_ponencia}, function(htmlexterno){
                    $("td#ponencia"+id_ponencia).html(htmlexterno);



                
                    // Display a success toast, with a title
                    toastr.success('Verificación Realizada Exitosamente','¡Notificación!')
                    toastr.options= {
                        progressBar: true,
                        positionClass: 'toast-bottom-right'
                    };
                });
            });


            $(".verificar-diseno").click(function(){
                console.log("click");
                var id_diseno_uc = $(this).data('id_diseno_uc');//para cambiar el estatus
                //console.log(id_diseno_uc);
                
                $.post("<?php echo constant ('URL');?>verificar/cambiarDiseno", {estatus: "Verificado", id: id_diseno_uc}, function(htmlexterno){
                    $("td#diseno"+id_diseno_uc).html(htmlexterno);


                    // Display a success toast, with a title
                    toastr.success('Verificación Realizada Exitosamente','¡Notificación!')
                    toastr.options= {
                        progressBar: true,
                        positionClass: 'toast-bottom-right'
                    };
                });
            });


            $(".verificar-reconocimiento").click(function(){
                console.log("click");
                var id_reconocimiento = $(this).data('id_reconocimiento');//para cambiar el estatus
                console.log(id_reconocimiento);
                

                $.post("<?php echo constant ('URL');?>verificar/cambiarReconocimiento", {estatus: "Verificado", id: id_reconocimiento}, function(htmlexterno){
                    $("td#reconocimiento"+id_reconocimiento).html(htmlexterno);

                    // Display a success toast, with a title
                    toastr.success('Verificación Realizada Exitosamente','¡Notificación!')
                    toastr.options= {
                        progressBar: true,
                        positionClass: 'toast-bottom-right'
                    };
                });
            });


            $(".verificar-tutoria").click(function(){
                console.log("click");
                var id_tutoria = $(this).data('id_tutoria');//para cambiar el estatus
                //console.log(id_tutoria);
                

                $.post("<?php echo constant ('URL');?>verificar/cambiarTutoria", {estatus: "Verificado", id: id_tutoria}, function(htmlexterno){
                    $("td#tutoria"+id_tutoria).html(htmlexterno);


                    // Display a success toast, with a title
                    toastr.success('Verificación Realizada Exitosamente','¡Notificación!')
                    toastr.options= {
                        progressBar: true,
                        positionClass: 'toast-bottom-right'
                    };
                });
            });
            


            $(".verificar-docencia").click(function(){
                console.log("click");
                var id_docencia_previa_ubv = $(this).data('id_docencia_previa_ubv');//para cambiar el estatus
                //console.log(id_docencia_previa_ubv);
                

                $.post("<?php echo constant ('URL');?>verificar/cambiarDocencia", {estatus: "Verificado", id: id_docencia_previa_ubv}, function(htmlexterno){
                    $("td#docenciaubv"+id_docencia_previa_ubv).html(htmlexterno);


                    // Display a success toast, with a title
                    toastr.success('Verificación Realizada Exitosamente','¡Notificación!')
                    toastr.options= {
                        progressBar: true,
                        positionClass: 'toast-bottom-right'
                    };
                });
            });

            $(".verificar-comisionexc").click(function(){
                console.log("click");
                var id_comision_excedencia = $(this).data('id_comision_excedencia');//para cambiar el estatus
                //console.log(id_comision_excedencia);
                

                $.post("<?php echo constant ('URL');?>verificar/cambiarComisionexc", {estatus: "Verificado", id: id_comision_excedencia}, function(htmlexterno){
                    $("td#comisionexc"+id_comision_excedencia).html(htmlexterno);


                    // Display a success toast, with a title
                    toastr.success('Verificación Realizada Exitosamente','¡Notificación!')
                    toastr.options= {
                        progressBar: true,
                        positionClass: 'toast-bottom-right'
                    };
                });
            });
        });


        function mostrar(id){

            if(id=="conducente"){

                //mostrar
                $("#testudio-conducente").show();
                $("#testudio-conducente").addClass('footable');
                $('.footable').footable();

                //ocultar
                $("#testudio").hide();
                $("#testudio").removeClass('footable');
                $("#tidioma").hide();
                $("#tidioma").removeClass('footable');
                $("#tadmin").hide();
                $("#tadmin").removeClass('footable');
                $("#tblanco").hide();
                $("#tdocencia").hide();
                $("#tdocencia").removeClass('footable');
                $("#tcomision").hide();
                $("#tcomision").removeClass('footable');
                $("#tlibro").hide();
                $("#tlibro").removeClass('footable');
                $("#trevista").hide();
                $("#trevista").removeClass('footable');
                $("#tartesoftware").hide();
                $("#tartesoftware").removeClass('footable');
                $("#tponencia").hide();
                $("#tponencia").removeClass('footable');
                $("#treconocimiento").hide();
                $("#treconocimiento").removeClass('footable');
                $("#tdocenciaubv").hide();
                $("#tdocenciaubv").removeClass('footable');
                $("#tdiseno").hide();
                $("#tdiseno").removeClass('footable');
                $("#ttutoria").hide();
                $("#ttutoria").removeClass('footable');
                $("#tcomisionexc").hide();
                $("#tcomisionexc").removeClass('footable');
            }

            if(id=="noconducente"){

                //mostrar
                $("#testudio").show();
                $("#testudio").addClass('footable');
                $('.footable').footable();

                //ocultar
                $("#tidioma").hide();
                $("#tidioma").removeClass('footable');
                $("#tadmin").hide();
                $("#tadmin").removeClass('footable');
                $("#tblanco").hide();
                $("#tdocencia").hide();
                $("#tdocencia").removeClass('footable');
                $("#tcomision").hide();
                $("#tcomision").removeClass('footable');
                $("#tlibro").hide();
                $("#tlibro").removeClass('footable');
                $("#trevista").hide();
                $("#trevista").removeClass('footable');
                $("#tartesoftware").hide();
                $("#tartesoftware").removeClass('footable');
                $("#tponencia").hide();
                $("#tponencia").removeClass('footable');
                $("#treconocimiento").hide();
                $("#treconocimiento").removeClass('footable');
                $("#tdocenciaubv").hide();
                $("#tdocenciaubv").removeClass('footable');
                $("#tdiseno").hide();
                $("#tdiseno").removeClass('footable');
                $("#ttutoria").hide();
                $("#ttutoria").removeClass('footable');
                $("#tcomisionexc").hide();
                $("#tcomisionexc").removeClass('footable');
                $("#testudio-conducente").hide();
                $("#testudio-conducente").removeClass('footable');

            }

            if(id=="idiomas"){
                
                //mostrar
                $("#tidioma").show();
                $("#tidioma").addClass('footable');
                $('.footable').footable();

                //ocultar
                $("#testudio").hide();
                $("#testudio").removeClass('footable');
                $("#tadmin").hide();
                $("#tadmin").removeClass('footable');
                $("#tblanco").hide();
                $("#tdocencia").hide();
                $("#tdocencia").removeClass('footable');
                $("#tcomision").hide();
                $("#tcomision").removeClass('footable');
                $("#tlibro").hide();
                $("#tlibro").removeClass('footable');
                $("#trevista").hide();
                $("#trevista").removeClass('footable');
                $("#tartesoftware").hide();
                $("#tartesoftware").removeClass('footable');
                $("#tponencia").hide();
                $("#tponencia").removeClass('footable');
                $("#treconocimiento").hide();
                $("#treconocimiento").removeClass('footable');
                $("#tdiseno").hide();
                $("#tdiseno").removeClass('footable');
                $("#tdocenciaubv").hide();
                $("#tdocenciaubv").removeClass('footable');
                $("#ttutoria").hide();
                $("#ttutoria").removeClass('footable');
                $("#tcomisionexc").hide();
                $("#tcomisionexc").removeClass('footable');
                $("#testudio-conducente").hide();
                $("#testudio-conducente").removeClass('footable');

            }


            if(id=="administrativo"){

                //mostrar
                $("#tadmin").show();
                $("#tadmin").addClass('footable');
                $('.footable').footable();

                //ocultar
                $("#testudio").hide();
                $("#testudio").removeClass('footable');
                $("#tidioma").hide();
                $("#tidioma").removeClass('footable');
                $("#tblanco").hide();
                $("#tdocencia").hide();
                $("#tdocencia").removeClass('footable');
                $("#tcomision").hide();
                $("#tcomision").removeClass('footable');
                $("#tlibro").hide();
                $("#tlibro").removeClass('footable');
                $("#trevista").hide();
                $("#trevista").removeClass('footable');
                $("#tartesoftware").hide();
                $("#tartesoftware").removeClass('footable');
                $("#tponencia").hide();
                $("#tponencia").removeClass('footable');
                $("#treconocimiento").hide();
                $("#treconocimiento").removeClass('footable');
                $("#tdiseno").hide();
                $("#tdiseno").removeClass('footable');
                $("#tdocenciaubv").hide();
                $("#tdocenciaubv").removeClass('footable');
                $("#ttutoria").hide();
                $("#ttutoria").removeClass('footable');
                $("#tcomisionexc").hide();
                $("#tcomisionexc").removeClass('footable');
                $("#testudio-conducente").hide();
                $("#testudio-conducente").removeClass('footable');

            }

            if(id=="docencia"){

                //mostrar
                $("#tdocencia").show();
                $("#tdocencia").addClass('footable');
                $('.footable').footable();

                //ocultar
                $("#tadmin").hide();
                $("#tadmin").removeClass('footable');
                $("#testudio").hide();
                $("#testudio").removeClass('footable');
                $("#tidioma").hide();
                $("#tidioma").removeClass('footable');
                $("#tblanco").hide();
                $("#tcomision").hide();
                $("#tcomision").removeClass('footable');
                $("#tlibro").hide();
                $("#tlibro").removeClass('footable');
                $("#trevista").hide();
                $("#trevista").removeClass('footable');
                $("#tartesoftware").hide();
                $("#tartesoftware").removeClass('footable');
                $("#tponencia").hide();
                $("#tponencia").removeClass('footable');
                $("#treconocimiento").hide();
                $("#treconocimiento").removeClass('footable');
                $("#tdiseno").hide();
                $("#tdiseno").removeClass('footable');
                $("#tdocenciaubv").hide();
                $("#tdocenciaubv").removeClass('footable');
                $("#ttutoria").hide();
                $("#ttutoria").removeClass('footable');
                $("#tcomisionexc").hide();
                $("#tcomisionexc").removeClass('footable');
                $("#testudio-conducente").hide();
                $("#testudio-conducente").removeClass('footable');

            }

            if(id=="comision"){

                //mostrar
                $("#tcomision").show();
                $("#tcomision").addClass('footable');
                $('.footable').footable();

                //ocultar
                $("#tadmin").hide();
                $("#tadmin").removeClass('footable');
                $("#testudio").hide();
                $("#testudio").removeClass('footable');
                $("#tidioma").hide();
                $("#tidioma").removeClass('footable');
                $("#tblanco").hide();
                $("#tdocencia").hide();
                $("#tdocencia").removeClass('footable');
                $("#tlibro").hide();
                $("#tlibro").removeClass('footable');
                $("#trevista").hide();
                $("#trevista").removeClass('footable');
                $("#tartesoftware").hide();
                $("#tartesoftware").removeClass('footable');
                $("#tponencia").hide();
                $("#tponencia").removeClass('footable');
                $("#treconocimiento").hide();
                $("#treconocimiento").removeClass('footable');
                $("#tdiseno").hide();
                $("#tdiseno").removeClass('footable');
                $("#tdocenciaubv").hide();
                $("#tdocenciaubv").removeClass('footable');
                $("#ttutoria").hide();
                $("#ttutoria").removeClass('footable');
                $("#tcomisionexc").hide();
                $("#tcomisionexc").removeClass('footable');
                $("#testudio-conducente").hide();
                $("#testudio-conducente").removeClass('footable');

            }


            if(id=="libro"){
                
                //mostrar
                $("#tlibro").show();
                $("#tlibro").addClass('footable');
                $('.footable').footable();

                //ocultar
                $("#testudio").hide();
                $("#testudio").removeClass('footable');
                $("#tadmin").hide();
                $("#tadmin").removeClass('footable');

                $("#tblanco").hide();
                $("#tdocencia").hide();
                $("#tdocencia").removeClass('footable');
                $("#tcomision").hide();
                $("#tcomision").removeClass('footable');
                $("#tidioma").hide();
                $("#tidioma").removeClass('footable');
                $("#trevista").hide();
                $("#trevista").removeClass('footable');
                $("#tartesoftware").hide();
                $("#tartesoftware").removeClass('footable');
                $("#tponencia").hide();
                $("#tponencia").removeClass('footable');
                $("#treconocimiento").hide();
                $("#treconocimiento").removeClass('footable');
                $("#tdiseno").hide();
                $("#tdiseno").removeClass('footable');
                $("#tdocenciaubv").hide();
                $("#tdocenciaubv").removeClass('footable');
                $("#ttutoria").hide();
                $("#ttutoria").removeClass('footable');
                $("#tcomisionexc").hide();
                $("#tcomisionexc").removeClass('footable');
                $("#testudio-conducente").hide();
                $("#testudio-conducente").removeClass('footable');

            }



            if(id=="revista"){
                
                //mostrar
                $("#trevista").show();
                $("#trevista").addClass('footable');
                $('.footable').footable();

                //ocultar
                $("#testudio").hide();
                $("#testudio").removeClass('footable');
                $("#tadmin").hide();
                $("#tadmin").removeClass('footable');

                $("#tblanco").hide();
                $("#tdocencia").hide();
                $("#tdocencia").removeClass('footable');
                $("#tcomision").hide();
                $("#tcomision").removeClass('footable');
                $("#tidioma").hide();
                $("#tidioma").removeClass('footable');
                $("#tlibro").hide();
                $("#tlibro").removeClass('footable');
                $("#tartesoftware").hide();
                $("#tartesoftware").removeClass('footable');
                $("#tponencia").hide();
                $("#tponencia").removeClass('footable');
                $("#treconocimiento").hide();
                $("#treconocimiento").removeClass('footable');
                $("#tdiseno").hide();
                $("#tdiseno").removeClass('footable');
                $("#tdocenciaubv").hide();
                $("#tdocenciaubv").removeClass('footable');
                $("#ttutoria").hide();
                $("#ttutoria").removeClass('footable');
                $("#tcomisionexc").hide();
                $("#tcomisionexc").removeClass('footable');
                $("#testudio-conducente").hide();
                $("#testudio-conducente").removeClass('footable');

            }


            if(id=="artesoftware"){
                
                //mostrar
                $("#tartesoftware").show();
                $("#tartesoftware").addClass('footable');
                $('.footable').footable();

                //ocultar
                $("#testudio").hide();
                $("#testudio").removeClass('footable');
                $("#tadmin").hide();
                $("#tadmin").removeClass('footable');

                $("#tblanco").hide();
                $("#tdocencia").hide();
                $("#tdocencia").removeClass('footable');
                $("#tcomision").hide();
                $("#tcomision").removeClass('footable');
                $("#tidioma").hide();
                $("#tidioma").removeClass('footable');
                $("#tlibro").hide();
                $("#tlibro").removeClass('footable');
                $("#trevista").hide();
                $("#trevista").removeClass('footable');
                $("#tponencia").hide();
                $("#tponencia").removeClass('footable');
                $("#treconocimiento").hide();
                $("#treconocimiento").removeClass('footable');
                $("#tdiseno").hide();
                $("#tdiseno").removeClass('footable');
                $("#tdocenciaubv").hide();
                $("#tdocenciaubv").removeClass('footable');
                $("#ttutoria").hide();
                $("#ttutoria").removeClass('footable');
                $("#tcomisionexc").hide();
                $("#tcomisionexc").removeClass('footable');
                $("#testudio-conducente").hide();
                $("#testudio-conducente").removeClass('footable');

            }


            if(id=="ponencia"){
                
                //mostrar
                $("#tponencia").show();
                $("#tponencia").addClass('footable');
                $('.footable').footable();

                //ocultar
                $("#testudio").hide();
                $("#testudio").removeClass('footable');
                $("#tadmin").hide();
                $("#tadmin").removeClass('footable');

                $("#tblanco").hide();
                $("#tdocencia").hide();
                $("#tdocencia").removeClass('footable');
                $("#tcomision").hide();
                $("#tcomision").removeClass('footable');
                $("#tidioma").hide();
                $("#tidioma").removeClass('footable');
                $("#tlibro").hide();
                $("#tlibro").removeClass('footable');
                $("#trevista").hide();
                $("#trevista").removeClass('footable');
                $("#tartesoftware").hide();
                $("#tartesoftware").removeClass('footable');
                $("#treconocimiento").hide();
                $("#treconocimiento").removeClass('footable');
                $("#tdiseno").hide();
                $("#tdiseno").removeClass('footable');
                $("#tdocenciaubv").hide();
                $("#tdocenciaubv").removeClass('footable');
                $("#ttutoria").hide();
                $("#ttutoria").removeClass('footable');
                $("#tcomisionexc").hide();
                $("#tcomisionexc").removeClass('footable');
                $("#testudio-conducente").hide();
                $("#testudio-conducente").removeClass('footable');

            }

            if(id=="reconocimiento"){
                
                //mostrar
                $("#treconocimiento").show();
                $("#treconocimiento").addClass('footable');
                $('.footable').footable();

                //ocultar
                $("#testudio").hide();
                $("#testudio").removeClass('footable');
                $("#tadmin").hide();
                $("#tadmin").removeClass('footable');

                $("#tblanco").hide();
                $("#tdocencia").hide();
                $("#tdocencia").removeClass('footable');
                $("#tcomision").hide();
                $("#tcomision").removeClass('footable');
                $("#tidioma").hide();
                $("#tidioma").removeClass('footable');
                $("#tlibro").hide();
                $("#tlibro").removeClass('footable');
                $("#trevista").hide();
                $("#trevista").removeClass('footable');
                $("#tartesoftware").hide();
                $("#tartesoftware").removeClass('footable');
                $("#tponencia").hide();
                $("#tponencia").removeClass('footable');
                $("#tdiseno").hide();
                $("#tdiseno").removeClass('footable');
                $("#tdocenciaubv").hide();
                $("#tdocenciaubv").removeClass('footable');
                $("#ttutoria").hide();
                $("#ttutoria").removeClass('footable');
                $("#tcomisionexc").hide();
                $("#tcomisionexc").removeClass('footable');
                $("#testudio-conducente").hide();
                $("#testudio-conducente").removeClass('footable');

            }


            if(id=="diseno"){
                
                //mostrar
                $("#tdiseno").show();
                $("#tdiseno").addClass('footable');
                $('.footable').footable();

                //ocultar
                $("#testudio").hide();
                $("#testudio").removeClass('footable');
                $("#tadmin").hide();
                $("#tadmin").removeClass('footable');

                $("#tblanco").hide();
                $("#tdocencia").hide();
                $("#tdocencia").removeClass('footable');
                $("#tcomision").hide();
                $("#tcomision").removeClass('footable');
                $("#tidioma").hide();
                $("#tidioma").removeClass('footable');
                $("#tlibro").hide();
                $("#tlibro").removeClass('footable');
                $("#trevista").hide();
                $("#trevista").removeClass('footable');
                $("#tponencia").hide();
                $("#tponencia").removeClass('footable');
                $("#ttutoria").hide();
                $("#ttutoria").removeClass('footable');
                $("#tdocenciaubv").hide();
                $("#tdocenciaubv").removeClass('footable');
                $("#tcomisionexc").hide();
                $("#tcomisionexc").removeClass('footable');
                $("#treconocimiento").hide();
                $("#treconocimiento").removeClass('footable');
                $("#tartesoftware").hide();
                $("#tartesoftware").removeClass('footable');
                $("#testudio-conducente").hide();
                $("#testudio-conducente").removeClass('footable');

                

            }

            if(id=="tutoria"){
                
                //mostrar
                $("#ttutoria").show();
                $("#ttutoria").addClass('footable');
                $('.footable').footable();

                //ocultar
                $("#testudio").hide();
                $("#testudio").removeClass('footable');
                $("#tadmin").hide();
                $("#tadmin").removeClass('footable');

                $("#tblanco").hide();
                $("#tdocencia").hide();
                $("#tdocencia").removeClass('footable');
                $("#tcomision").hide();
                $("#tcomision").removeClass('footable');
                $("#tidioma").hide();
                $("#tidioma").removeClass('footable');
                $("#tlibro").hide();
                $("#tlibro").removeClass('footable');
                $("#trevista").hide();
                $("#trevista").removeClass('footable');
                $("#tponencia").hide();
                $("#tponencia").removeClass('footable');
                $("#tdiseno").hide();
                $("#tdiseno").removeClass('footable');
                $("#tdocenciaubv").hide();
                $("#tdocenciaubv").removeClass('footable');
                $("#tcomisionexc").hide();
                $("#tcomisionexc").removeClass('footable');

                $("#treconocimiento").hide();
                $("#treconocimiento").removeClass('footable');
                $("#tartesoftware").hide();
                $("#tartesoftware").removeClass('footable');
                $("#testudio-conducente").hide();
                $("#testudio-conducente").removeClass('footable');

            }

            if(id=="docenciaubv"){
                
                //mostrar
                $("#tdocenciaubv").show();
                $("#tdocenciaubv").addClass('footable');
                $('.footable').footable();

                //ocultar
                $("#testudio").hide();
                $("#testudio").removeClass('footable');
                $("#tadmin").hide();
                $("#tadmin").removeClass('footable');
                $("#tblanco").hide();
                $("#tdocencia").hide();
                $("#tdocencia").removeClass('footable');
                $("#tcomision").hide();
                $("#tcomision").removeClass('footable');
                $("#tidioma").hide();
                $("#tidioma").removeClass('footable');
                $("#tlibro").hide();
                $("#tlibro").removeClass('footable');
                $("#trevista").hide();
                $("#trevista").removeClass('footable');
                $("#tponencia").hide();
                $("#tponencia").removeClass('footable');
                $("#tdiseno").hide();
                $("#tdiseno").removeClass('footable');
                $("#ttutoria").hide();
                $("#ttutoria").removeClass('footable');
                $("#tcomisionexc").hide();
                $("#tcomisionexc").removeClass('footable');
                $("#treconocimiento").hide();
                $("#treconocimiento").removeClass('footable');
                $("#tartesoftware").hide();
                $("#tartesoftware").removeClass('footable');
                $("#testudio-conducente").hide();
                $("#testudio-conducente").removeClass('footable');

            }

            if(id=="comisionexc"){
                
                //mostrar
                $("#tcomisionexc").show();
                $("#tcomisionexc").addClass('footable');
                $('.footable').footable();

                //ocultar
                $("#testudio").hide();
                $("#testudio").removeClass('footable');
                $("#tadmin").hide();
                $("#tblanco").hide();
                $("#tdocencia").hide();
                $("#tcomision").hide();
                $("#tidioma").hide();
                $("#tidioma").removeClass('footable');
                $("#tlibro").hide();
                $("#tlibro").removeClass('footable');
                $("#trevista").hide();
                $("#trevista").removeClass('footable');
                $("#tponencia").hide();
                $("#tponencia").removeClass('footable');
                $("#tdiseno").hide();
                $("#tdiseno").removeClass('footable');
                $("#ttutoria").hide();
                $("#ttutoria").removeClass('footable');
                $("#tdocenciaubv").hide();
                $("#tdocenciaubv").removeClass('footable');
                
                $("#treconocimiento").hide();
                $("#treconocimiento").removeClass('footable');
                $("#tartesoftware").hide();
                $("#tartesoftware").removeClass('footable');
                $("#testudio-conducente").hide();
                $("#testudio-conducente").removeClass('footable');

            }

            if(id==""){
                //mostrar
                $("#blanco").show();
                $("#tblanco").show();

                //ocultar
                $("#testudio").hide();
                $("#tidioma").hide();
                $("#tadmin").hide();
                $("#tdocencia").hide();
                $("#tcomision").hide();
                $("#tlibro").hide();
                $("#trevista").hide();
                $("#tartesoftware").hide();
                $("#tponencia").hide();
                $("#treconocimiento").hide();
                $("#tdiseno").hide();
                $("#ttutoria").hide();
                $("#tdocenciaubv").hide();
                $("#tcomisionexc").hide();
                $("#testudio-conducente").hide();

                $("#blanco").show();
                $("#tblanco").show();
                
             
            }

        }

    </script>

   

</body>
</html>

