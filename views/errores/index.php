<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Admin | 404 Error</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">


    <div class="middle-box text-center animated fadeInDown">
        <h1>404</h1>
        <h3 class="font-bold" style="font-size: 20px;">Página no encontrada</h3>

        <div class="error-desc" style="font-size: 16px;" >
                         Lo sentimos pero no pudimos encontrar esta página.
                         <p>Esta página que estás buscando no existe. </p>
                         </br>
          <form action="<?php echo constant ('URL');?>home" class="form-inline m-t" role="form">
            
                <button type="submit" class="btn btn-primary" style="position:absolute;
        left: 41%;" >Ir al Inicio</button>
            </form>
        </div>
    </div>
<script language="javascript">
setTimeout("location.href='<?php echo constant ('URL');?>home'", 9000);
</script>
    <!-- Mainly scripts -->
    <script src="<?php echo constant ('URL');?>src/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/popper.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/bootstrap.js"></script>

</body>

</html>
