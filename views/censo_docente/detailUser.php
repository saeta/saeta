<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Detalle Usuario | SIDTA</title>


 
    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">


</head>
<style>
td:hover{
    background-color: #eed92eba;
    color: #111;
    cursor: default;
}
tr:hover{
    background-color: #cac0c08c;
    color: #111;

}
</style>
<body>
 
   <?php require 'views/header.php'; ?>
   

   <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-5">
                    <h2>Detalle de Usuario</h2>
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>censo_docente">Listar Usuarios</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <a href="<?php echo constant ('URL') . "censo_docente/verUsuarioInfo/". $this->id_persona;?>"><strong>Detalle de Usuario</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-7">
                    <div class="title-action">
           <style>
           .familiar{
            background-color: #1c84c6;
           }
           .familiar:hover{
            background-color: #273479;
           }
           </style>
                        <a href="<?php echo constant ('URL');?>censo_docente" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Volver</a>
                        <a href="<?php echo constant ('URL').'censo_docente/familiar/'.$this->informacion->identificacion.'/'.$this->id_persona;?>" class="btn btn-success familiar"><i class="fa fa-plus"></i> Añadir Familiar</a>
                 
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row m-b-lg m-t-lg">
                <div class="col-md-6">

                    <div class="profile-image">
                        <i style="font-size: 100px;" class="fa fa-user-circle-o"></i>
                    </div>
                    <div class="profile-info">
                        <div class="">
                            <div>
                                
                                <h3><small>Perfil: </small> <?php echo $this->informacion->perfil;?></h3>
                                <p>
                                    <?php echo substr($this->informacion->documento_identidad, 0, 1) . "-".$this->informacion->identificacion . ", " . 
                                    $this->informacion->primer_nombre . " " . $this->informacion->primer_apellido. ". Reside en: " . $this->domicilio_p->municipio. ", ". $this->domicilio_p->parroquia. "."; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="row ">
               
               <!-- <div class="col-lg-12 ">
               
                    <div  class="panel panel-primary">
                        <div style="color: black;" class="panel-heading">

                        <h3 class="text-center">Datos Personales<br><label style="margin-top: 4px;"><small >Documento de Identidad</small></label><br>
                            <i class="fa fa-address-card"></i> 
                                Identificación: <?php echo $this->informacion->identificacion;?> <br>Tipo de Documento de Identidad: <?php echo $this->informacion->documento_identidad; ?>
                        </h3>
                        </div>
                        <div class="panel-body row">
                        <div class="col-lg-6">
                            <table class="table table-bordered" style="width: 100%;">
                                    <tr>
                                        <td style="font-weight: 400;">Nombres:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->primer_nombre . " " . $this->informacion->segundo_nombre; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Apellidos:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->primer_apellido . " " . $this->informacion->segundo_apellido; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Estado Civil:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->estado_civil; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Género:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->genero; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Nacionalidad:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->nacionalidad; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Fecha de Nacimiento:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo date("d/m/Y", strtotime($this->informacion->fecha_nacimiento)); ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">País de Nacimiento:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->pais; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Número de Hijos:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->nro_hijos; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Fecha de Ingreso a la UBV:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo date("d/m/Y", strtotime($this->informacion->fecha_ingreso)); ?></strong></td>
                                    </tr>
                                </table>
                            </div>
                        <div class="col-lg-6">
                            <table class="table table-bordered" style="width: 100%;">
                                <tr>
                                    <td colspan="2" style="text-align: center; color: #000;"><b>Datos de Contacto</b></td>
                                </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Tipo de Teléfono:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->telefono_tipo; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Teléfono:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo "(".$this->informacion->telefono_coodigo_area . ") " . $this->informacion->telefono; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Correo Personal:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->correo_personal["correo"]; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Correo Institucional:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->correo_institucional["correo"]; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; color: #000;" colspan="2"><b>Domicilio</b></td>
                                        
                                    </tr>
                                     <tr>
                                        <td style="font-weight: 400;">Tipo de Domicilio:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->domicilio_p->tipo_domicilio; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;" colspan="2">Dirección:</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: justify; font-size: 12px;" colspan="2"><strong><?php echo $this->domicilio_p->pais. ", ".$this->domicilio_p->estado. ", ". $this->domicilio_p->municipio. ", ". $this->domicilio_p->parroquia. ", ".$this->domicilio_p->domicilio_detalle. "."; ?></strong></td>
                                    </tr>
                                </table>
                            </div>
                            <?php if(!empty($this->informacion->id_etnia)){?>
                                <div class="col-lg-3"></div>
                            <div class="col-lg-6">
                                <table class="table table-bordered" style="width: 100%;">
                                    <tr>
                                        <td style="font-weight: 400;">Etnia Indígena:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->etnia; ?></strong></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-lg-3"></div>

                            <?php } ?>
                        </div>
                    </div>
                   
                </div>
                <?php if(!empty($this->disc[1]['id_discapacidad1'])){?>
                <div class="col-lg-12">
                    <div  class="panel panel-info">
                        <div style="color: black;" class="panel-heading">
                        <h3 class="text-center">Datos de Discapacidad<br>
                            <label style="margin-top: 3px;"><b>Código CONAPDIS: <?php echo $this->disc[1]['codigo_conapdis1']; ?></b></label>
                        </h3>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered" style="width: 100%;">
                                <tr>
                                    <td style="font-weight: 400;">Tipo de Discapacidad:</td>
                                    <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->disc[1]['tipo_discapacidad1']; ?></strong></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: 400;">Discapacidad:</td>
                                    <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->disc[1]['discapacidad1']; ?></strong></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: 400;" colspan="2">Observación:</td>
                                </tr>
                                <tr>
                                    <td style="text-align: justify; font-size: 14px;" colspan="2"><strong><?php echo $this->disc[1]['observacion1']; ?></strong></td>
                                </tr>
                                <?php if(!empty($this->disc[2]['id_discapacidad2'])){?>

                                <tr>
                                    <td style="text-align: center; font-weight: 400;" colspan="2">---- ---- ---- ----</td>
                                </tr>
                                <tr>
                                    <td style="font-weight: 400;">Tipo de Discapacidad:</td>
                                    <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->disc[2]['tipo_discapacidad2']; ?></strong></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: 400;">Discapacidad:</td>
                                    <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->disc[2]['discapacidad2']; ?></strong></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: 400;" colspan="2">Observación:</td>
                                </tr>
                                <tr>
                                    <td style="text-align: justify; font-size: 14px;" colspan="2"><strong><?php echo $this->disc[2]['observacion2']; ?></strong></td>
                                </tr>
                                <?php } ?>
                            </table>     
                        </div>
                    </div>
                </div>
                <?php } ?>-->
                <?php if($this->informacion->id_perfil==1){//si es un docente?>
                
                <div class="col-lg-12">
                <?php echo $this->mensaje;?> 
                    <div  class="panel panel-success">
                        <div style="color: black;" class="panel-heading">
                        <h3 class="text-center">Datos del(la) Trabajador(a) Académico(a)
                        </h3>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered" style="width: 100%;">
                                <tr>
                                    <td style="font-weight: 400;">Programa de Formación:</td>
                                    <td style="text-align: right; font-size: 16px;"> 
                                        <table style="font-size: 16px;" class="table small m-b-xs">
                                            <tbody>
                                            <?php $j=1;
                                                while($j<=count($this->programas_docente)){ ?>
                                            <tr>
                                                <td>
                                                <?php echo $this->doc_prog[$j]['programa'.$j]; ?>
                                                </td>
                                            </tr>
                                            <?php 
                                                $j++;
                                            } ?>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: 400;">Centro de Estudio <i>(CE)</i>:</td>
                                    <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->centro_estudio; ?></strong></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: 400;">Eje Municipal <i>(EM)</i>:</td>
                                    <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->eje_municipal; ?></strong></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: 400;">Dedicación:</td>
                                    <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->docente_dedicacion; ?></strong></td>
                                </tr> 
                                <tr>
                                    <td style="font-weight: 400;">Estatus del Docente:</td>
                                    <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->docente_estatus; ?></strong></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: 400;">Clasificación:</td>
                                    <td style="text-align: right; font-size: 16px;"><strong>
                                        <?php switch($this->informacion->clasificacion_docente){
                                                                    case 'DOCENTE': 
                                                                        echo $this->informacion->clasificacion_docente . " ORDINARIO";
                                                                        break;
                                                                    case 'JUBILADOS':
                                                                    echo "DOCENTE " . str_replace('S', '', $this->informacion->clasificacion_docente);
                                                                    break;
                                                                    case 'PENSIONADOS':
                                                                    echo "DOCENTE " . substr($this->informacion->clasificacion_docente, 0, 10);
                                                                    break;
                                                                    default:
                                                                    echo "DOCENTE " .$this->informacion->clasificacion_docente;
                                                                    break;
                                                                } ?></strong></td>
                                </tr>
                                <tr>
                                    <td style="font-weight: 400;">Escalafón:</td>
                                    <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->informacion->escalafon; ?></strong></td>
                                </tr>
                               
                                
                                
                            </table>     
                        </div>
                    </div>
                </div>
                <?php } ?>






                    <!-- CARGA FAMILIAR -->

                    <?php //if(!empty($this->vista)){//si Posee familiares registrados?>
                
                <div class="col-lg-12">
                    <div  class="panel panel-success">
                        <div style="color: black;" class="panel-heading">
                        <h3 class="text-center">Datos de Familiares del Trabajador(a) Académico(a)
                        </h3>
                        </div>
                        <div class="panel-body">

                        <?php if(empty($this->familiares)){?>
                           <center><h4>No se encontraron registros.</h4></center>
                        <?php }else{ ?>
                           
                            <table class="table table-bordered" style="width: 100%;">
                            <?php include_once 'models/persona.php';
                            foreach($this->familiares as $row){
                                $familiar= new Persona();
                                $familiar=$row;?>
                                <tr>
                                    <td style="font-weight: 400;">
                                    Nombres : <b><?php echo $familiar->pnombre.' '.$familiar->papellido; ?></b></td>
                                    <td> Cedula : <b><?php if($familiar->cedfam !=0){ echo $familiar->cedfam; }?></b> </td>
                                    <td>Telefono : <b><?php echo $familiar->telefono; ?></b> </td>
                                   <td>Estatus : 
                                    <?php if($familiar->estatus==0){
                                   echo"<center style=margin-top:3%;><span class='label label-danger' style='font-size: 12px;'>Inactivo</span></center>";
                                     }else{
                                   echo"<center style=margin-top:3%;><span class='label label-primary' style='font-size: 12px;'>&nbsp; Activo &nbsp;</span></center>";
                                      }  ?></td>
                                    <td style="text-align: right; font-size: 16px;">
                                    <a href="<?php echo constant ('URL').'censo_docente/VerDetalleFamiliar/'.$familiar->id_sno_familia.'/'.$this->id_persona.'/1';?>"><button type="button" class="btn btn-outline btn-info"><i class="fa fa-eye"></i>&nbsp;&nbsp;&nbsp;Ver&nbsp;&nbsp;&nbsp;</button></a>
                                    <a href="<?php echo constant ('URL').'censo_docente/VerDetalleFamiliar/'.$familiar->id_sno_familia.'/'.$this->id_persona;?>"><button type="button" class="btn btn-outline btn-success"><i class="fa fa-edit"></i> Editar</button></a>
                                    </td>
                                </tr>
                            <?php }?>

                            </table>     
                            <?php }?>

                        </div>
                    </div>
                </div>
                <?php //} ?>
                    <!-- //////////////-->








            </div>
        
        </div>



    <?php require 'views/footer.php'; ?>

     

    <!-- dataTables Scripts -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>

   

</body>
</html>










