<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Editar Familiar | SIDTA</title>


    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">
  
    
    <!-- jasny input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <!--  style select2 -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/select2/select2.min.css" rel="stylesheet">
    <!-- datapicker -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <!-- sweetalert input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- chosen -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">

</head>

<body>
 
    <div id="wrapper">
   <?php require 'views/header.php'; ?>
   

        <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-9">
                    <h2>Editar Familiar</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL').'censo_docente/verUsuarioInfo/'.$this->id_persona;?>"> Detalle de Usuario</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <a href="#"><strong>Editar</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-3">
                    <div class="title-action">
                        <a href="<?php echo constant ('URL').'censo_docente/verUsuarioInfo/'.$this->id_persona;?>" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Volver</a>
                    </div>
                </div>
            </div>

            <style>
            .req{
            color: red;
            }
            </style>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5><i class="fa fa-angle-double-right"></i> SIDTA <i class="fa fa-angle-double-left"></i></h5>
                        </div>
                        <div class="ibox-content">
                        <h2>Complete el formulario para editar un familiar del trabajador: <br><br><?php echo "CI: " . $this->informacion->identificacion. " / " . $this->informacion->primer_nombre . " " . $this->informacion->primer_apellido; ?></h2>
                            <p>Los campos identificados con <span class="req">*</span> son obligatorios </p>
                      
                           
                            <div class="alert alert-info">Todos los campos marcados con un (<span style="color: red;">*</span>) Son Obligatorios</div>

              


                            <?php echo $this->mensaje; ?>
                            <form id="form" action="<?php echo constant('URL').'censo_docente/ActualizarFamiliar/'.$this->id_persona;?>"  method="post" class="wizard-big">
                   
                   
                   <!--<input type="hidden" name="minorguniadm" value="<?php echo $this->persona->minorguniadm; ?>">
                   <input type="hidden" name="ofiuniadm" value="<?php echo $this->persona->ofiuniadm; ?>">
                   <input type="hidden" name="uniuniadm" value="<?php echo $this->persona->uniuniadm; ?>">
                   <input type="hidden" name="codper" value="<?php echo $this->persona->codper; ?>">-->



                   <input type="hidden" name="cedper" value="<?php echo $this->persona->cedper; ?>">


                            
                               <h1>Datos del personales</h1>
                               <fieldset>
                               <div class="row">
                                   <div class="col-lg-6">

                         <input type="hidden" name="id_sno_familia" value="<?php echo $this->persona->id_sno_familia;?>">
                                           <div class="form-group">
                                               <label>Primer Nombre <span class="req">*</span></label>
                                               <input type="text" class="form-control required" placeholder="Ingrese el Primer Nombre"  name="pnombre" id="pnombre" onkeypress="return soloLetras(event)" value="<?php echo $this->persona->pnombre;?>">   
                                           </div>
                                           <div class="form-group">
                                               <label>Segundo Nombre</label>
                                               <input type="text" class="form-control" placeholder="Ingrese el Segundo Nombre"  name="snombre" id="snombre" onkeypress="return soloLetras(event)" value="<?php echo $this->persona->snombre;?>">   
                                           </div>
                                           <div class="form-group row">
                                             <div class="col-md-6">
                                               <div class="form-group" id="data_1">
                                                 <label class="font-normal">Fecha de Nacimiento <span class="req">*</span></label>
                                                 <div class="input-group date">
                                                     <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                     <input name="fec_nac" type="text" id="fec_nac" class="form-control required" placeholder="Mes/Día/Año" size="15" maxlength="10" onKeyDown="javascript:ue_formato_fecha(this,'/',patron,true,event);" onBlur="javascript: ue_validar_formatofecha(this);" datepicker="true" value="<?php echo date("m/d/Y", strtotime($this->persona->fecha_nac));?>">
                                                 </div>
                                               </div>

                                               <script>
                                              /* $(".select2_demo_3").select2({
                                                   placeholder: "Seleccione",
                                                   width: "100%",
                                                   dropdownAutoWidth: true,
                                                   allowClear: true
                                               });*/
                                                var mem = $('#data_1 .input-group.date').datepicker({
                                                   todayBtn: "linked",
                                                   keyboardNavigation: false,
                                                   forceParse: false,
                                                   calendarWeeks: true,
                                                   autoclose: true
                                               });
                                               </script>
                                             </div>
                                             <div class="col-md-6">
                                               <div class="form-group">
                                                 <label>Estado Civil <span class="req">*</span></label>
                                                 <select name="est_civil" id="est_civil" class="select2_demo_4 form-control required">
                                                   <option value="">Seleccione</option>
                                                   <option value="Soltero" <?=($this->persona->estado_civil=="Soltero"?"selected":"");?> >Soltero</option>
                                                   <option value="Casado" <?=($this->persona->estado_civil=="Casado"?"selected":"");?> >Casado</option>
                                                   <option value="Viudo" <?=($this->persona->estado_civil=="Viudo"?"selected":"");?> >Viudo</option>
                                                   <option value="Divorciado" <?=($this->persona->estado_civil=="Divorciado"?"selected":"");?> >Divorciado</option>
                                                   <option value="En Union Estable" <?=($this->persona->estado_civil=="En Union Estable"?"selected":"");?> >En Union Estable</option>
                                                 </select>
                                               </div>
                                             </div>


                                             
                                           </div>

                                         
                                    <div class="form-group">
                                     <label>Cédula del Familiar </label>
                                     <input type="text" class="form-control number" placeholder="Ingrese la cédula del familiar"  name="cedfam" id="cedfam" value="<?php if($this->persona->cedfam !=0){ echo $this->persona->cedfam;} ?>">   
                                   </div>

                                           
                                   </div>
                                   <div class="col-md-6">
                                           <div class="form-group">
                                               <label>Primer Apellido <span class="req">*</span></label>
                                               <input type="text" class="form-control required" placeholder="Ingrese el Primer Apellido"  name="papellido" id="papellido" onkeypress="return soloLetras(event)" value="<?php echo $this->persona->papellido; ?>">   
                                           </div>
                                           <div class="form-group">
                                               <label>Segundo Apellido</label>
                                               <input type="text" class="form-control" placeholder="Ingrese el Segundo Apellido"  name="sapellido" id="sapellido" onkeypress="return soloLetras(event)" value="<?php echo $this->persona->sapellido; ?>">   
                                           </div>


                                           
                                           <div class="form-group row">
                                             <div class="col-md-6">
                                               <div class="form-group">
                                                 <label>Nacionalidad <span class="req">*</span></label>
                                                 <select name="nacionalidad" id="nacionalidad" class="select2_demo_4 form-control required">
                                                   <option value="">Seleccione</option>
                                                   <option value="Venezolano" <?=($this->persona->nacionalidad=="Venezolano"?"selected":"");?>>Venezolano</option>
                                                   <option value="Extranjero" <?=($this->persona->nacionalidad=="Extranjero"?"selected":"");?>>Extranjero</option>
                                                 </select>
                                               </div>
                                             </div>
                                             <div class="col-md-6">
                                               <div class="form-group">
                                                 <label>Género <span class="req">*</span></label>
                                                 <select name="genero" id="genero" class="select2_demo_4 form-control required">
                                                   <option value="">Seleccione</option>
                                                   <option value="F" <?=($this->persona->sexfam=="F"?"selected":"");?>>Femenino</option>
                                                   <option value="M" <?=($this->persona->sexfam=="M"?"selected":"");?> >Masculino</option>
                                                 </select>
                                               </div>
                                             </div>
                                             
                                           </div>






                                           <div class="form-group row">
                                          <div class="col-md-6">
                                               <div class="form-group">
                                                 <label>Parentesco <span class="req">*</span></label>
                                                 <select name="parentesco" id="parentesco" class="select2_demo_4 form-control required">
                                                   <option value="">Seleccione</option>
                                                   <option value="Titular" <?=($this->persona->parentesco=="Titular"?"selected":"");?>>Titular</option>
                                                   <option value="Cónyuge" <?=($this->persona->parentesco=="Cónyuge"?"selected":"");?>>Cónyuge</option>
                                                   <option value="Madre" <?=($this->persona->parentesco=="Madre"?"selected":"");?>>Madre</option>
                                                   <option value="Padre" <?=($this->persona->parentesco=="Padre"?"selected":"");?>>Padre</option>
                                                   <option value="Hija" <?=($this->persona->parentesco=="Hija"?"selected":"");?>>Hija</option>
                                                   <option value="Hijo" <?=($this->persona->parentesco=="Hijo"?"selected":"");?>>Hijo</option>
                                                   <option value="Sobreviviente" <?=($this->persona->parentesco=="Sobreviviente"?"selected":"");?>>Sobreviviente</option>
                                                   <option value="Titular Sobreviviente" <?=($this->persona->parentesco=="Titular Sobreviviente"?"selected":"");?>>Titular Sobreviviente</option>
                                                 </select>
                                               </div>
                                           </div>

                                 <div class="col-md-6">
                                 <div class="form-group">
                                       <label>Tipo de Trabajador </label>
                                       <select name="tipo_trabajador" id="tipo_trabajador" class="select2_demo_4 form-control ">
                                         <option value="">Seleccione</option>
                                         <option value="Docente" <?=($this->persona->tipo_trabajador=="Docente"?"selected":"");?>>Docente</option>
                                         <option value="Administrativo" <?=($this->persona->tipo_trabajador=="Administrativo"?"selected":"");?>>Administrativo</option>
                                         <option value="Obrero" <?=($this->persona->tipo_trabajador=="Obrero"?"selected":"");?>>Obrero</option>
                                       </select>
                                     </div>
                                 </div>
                                 </div>

                                   </div>
                               </div>
                             

                               </fieldset>
                               <h1>Datos de ubicación/contacto</h1>
                               <fieldset>
                                   <h2>Información Personal del Familiar</h2>

                                   <div class="row">
                                       <div class="col-lg-6">
                                       <div class="form-group">
                                       <label>País  <span class="req">*</span></label>
                                       <select class="select2_demo_3 form-control required m-b" name="pais" id="pais">                                    
                                       <option selected="selected" value="">Seleccione</option> 
                                       </select>   
                                       </div>
                                    <!-- //////////////////////////-->    
                                    <!--   <div class="form-group row">
                                         <div class=" col-md-6">
                                           <div class="form-group">

                                             <label>País  <span class="req">*</span></label>
                                             <select class="select2_demo_3 form-control required m-b" name="pais" id="pais">                                    
                                               <option selected="selected" value="">Seleccione</option> 
                                             </select>   
                                           </div>
                                         </div>
                                         <div class="col-md-6">
                                           <div class="form-group">
                                             <label>Estado  <span class="req">*</span></label>
                                             <select class="select2_demo_3 form-control required m-b" name="estado" id="estado">                                    
                                               <option selected="selected" value="">Seleccione</option> 
                                             </select> 
                                           </div>
                                         </div>
                                       


                                       </div>-->
                                    

                                       <div class="form-group">
                                               <label> Correo Electr&oacute;nico </label>
                                               <input id="correo" name="correo" type="email"  size="30" maxlength="60" class="form-control " placeholder="Ingrese el correo electrónico del familiar" value="<?php echo $this->persona->correo;?>">
                                           </div>

                                     

                                           <div class="form-group">
                                             <label> Ciudad de Residencia <span class="req">*</span></label>
                                             <input id="ciudad" name="ciudad" type="text"  size="30" maxlength="60" class="form-control required" placeholder="Ingrese la ciudad donde reside el familiar" value="<?php echo $this->persona->ciudad_residencia;?>">
                                           </div>

                                   <!-- //////////////////////////-->


                                       </div>


                                       <div class="col-lg-6">

                                       <div class="form-group">
                                             <label>Estado  <span class="req">*</span></label>
                                             <select class="select2_demo_3 form-control required m-b" name="estado" id="estado">                                    
                                               <option selected="selected" value="">Seleccione</option> 
                                             </select> 
                                           </div>

                                     <!--  <div class="form-group row">
                                       <div class="col-md-6">
                                           <div class="form-group">

                                             <label>Municipio  <span class="req">*</span></label>
                                             <select class="select2_demo_3 form-control required m-b" name="municipio" id="municipio">                                    
                                               <option selected="selected" value="">Seleccione </option> 
                                             </select> 
                                           </div>
                                         </div>

                                         <div class="col-md-6">
                                           <div class="form-group">

                                             <label>Parroquia  <span class="req">*</span></label>
                                               <select class="select2_demo_3 form-control required m-b" name="parroquia" id="parroquia">                                    
                                                 <option selected="selected" value="">--Seleccione--</option> 
                                               </select> 
                                           </div>
                                         </div>
                                        
                                       </div> -->


                                           <div class="form-group">
                                               <label>Teléfono <span class="req">*</span></label>
                                               <input id="telefono" data-mask="(9999) 999-9999" name="telefono" type="text"  size="30" maxlength="60" class="form-control required" placeholder="Ingrese el nro de teléfono del familiar" value="<?php echo $this->persona->telefono;?>">
                                           </div>




                                       <!-- habilitado inhabilitado-->


                                       <label>Estatus del familiar <span class="req">*</span></label>
                                       <div class="i-checks col-sm-10"  >                       
                                       <label class="i-checks checkbox-inline" >Activo
                                       <input type="radio"  value="1" id="optionsRadios1" name="estatus"  class="form-control required"   <?=($this->persona->estatus=='1'?"checked":"");?> onChange="ActivosInactivos(this)">
                                       </label> 
                                       <label class="i-checks checkbox-inline" >Inactivo                               
                                       <input type="radio" value="0" id="optionsRadios2" name="estatus"  class="form-control required"  <?=($this->persona->estatus=='0'?"checked":"");?>  onChange="ActivosInactivos(this)">
                                       </label>
                                       </div>
                                     <!-- UBICACION PERSONAL -->

                                     
                                   <!-- //////////////////////////-->

                                           
                                         </div>                                      
                                     <div class="col-lg-12">
                                   
                                       <div class="form-group">
                                         <label>Dirección <span class="req">*</span></label>
                                         <textarea id="dir" name="dir" type="text" placeholder="Ingrese la dirección de vivienda del familiar" class="form-control required valid" placeholder="Escriba su dirección..." aria-="true" aria-invalid="false"  size="40" maxlength="250"><?php echo $this->persona->direccion;?></textarea>
                                       </div>


                                       <div class="form-group" id="obser" style="<?php  if($this->persona->estatus=='0'){ echo "display:;"; }else{ echo "display:none;";  } ?>">
                                         <label>Observaciones <span class="req">*</span></label>
                                         <textarea id="observacion" name="observacion" type="text" placeholder="Ingrese sus observaciones" class="form-control"  aria-="true" aria-invalid="false"  size="40" maxlength="250"><?php echo $this->persona->observacion;?></textarea>
                                       </div>


                                     </div>
                                   </div>
                                   


                                   <!-- COMBO DEPENDIENTE DE UBICACION -->
                                       <script>



function ActivosInactivos(sel) {
                                               if (sel.value=="1"){

                                                   divT = document.getElementById("obser");
                                                   divT.style.display = "none";
                                                   
                                                   myElemento7 = document.getElementById("observacion");
                                                   myElemento7.classList.remove('required');
                                           
                                                   var inputNombre = document.getElementById("observacion");
                                                   inputNombre.value = ""; //Borra del input la cedula al seleccionar NO
                                               }else{

                                                   divC = document.getElementById("obser");
                                                   divC.style.display="";
                                           
                                                   //agrega clase required 
                                                   myElemento8 = document.getElementById("observacion");
                                                   myElemento8.classList.add('required');


                                               }
                                       

                                           }





                                   $(function(){

                               // Lista de paises
                               $.post( '<?php echo constant ('URL');?>usuarios/getByPais' ).done( function(respuesta)
                               {    
                                   $( '#pais' ).html( respuesta );
                                   valor="<?php echo $this->persona->codpai;?>";
                                 
                                   // $("#pais option[value="+ <?php echo $this->persona->codpai;?> +"]").attr("selected",true);
                                 
                                   $("#pais option[value="+ valor +"]").attr("selected",true);
                                 
                                   if ($("#pais").val() == '<?php echo $this->persona->codpai;?>') {
                                    
                                   $('#estado').append('<option value="<?php echo $this->persona->codest;?>" selected="selected"><?php echo $this->persona->desest;?></option>');
                                   //$('#municipio').append('<option value="<?php echo $this->persona->codmun;?>" selected="selected"><?php echo $this->persona->denmun;?></option>');
                                   //$('#parroquia').append('<option value="<?php echo $this->persona->codpar;?>" selected="selected"><?php echo $this->persona->denpar;?></option>');
                             
                                   }




                       //SE COLOCAN ACA ESTOS SELECT DEBIDO A COMFLICTOS CON LOS DEPENDIENTES 07/01/21
                                                   $(".select2_demo_3").select2({
                                                       placeholder: "Seleccione",
                                                       width: "100%",
                                                       dropdownAutoWidth: true,
                                                       allowClear: true
                                                   });
                                                   //select no search
                                                 $(".select2_demo_4").select2({
                                                       placeholder: "Seleccione",
                                                       width: "100%",
                                                       dropdownAutoWidth: true,
                                                       allowClear: true,
                                                       minimumResultsForSearch: -1
                                                   });

                         ////////////////////////////////////////////////////////////////////////////                                


                                });


                               // lista de estados	
                               $('#pais').change(function()
                               {
                                
                                   var id_pais = $(this).val();
                                   
                                   // Lista de estados
                                   $.post( '<?php echo constant ('URL');?>usuarios/getEstadosPais', { pais: id_pais} ).done( function( respuesta )
                                   {
                                       $( '#estado' ).html( respuesta );
                                       
                                     //  $('#estado').append('<option value="foo" selected="selected">Foo</option>');
                              // $("#estado option[value="+ <?php echo $this->informacion->id_estado;?> +"]").attr("selected",true);
                            
                                   });
                               });

                              

                               // lista de municipios	
                               $('#estado').change(function()
                               {
                                   var id_estado = $(this).val();
                                
                                   // Lista de municipios
                                   $.post( '<?php echo constant ('URL');?>usuarios/getMunicipiosEstado', { municipio: id_estado} ).done( function( respuesta )
                                   {
                                       $( '#municipio' ).html( respuesta );
                                   });
                               });


                               // lista de parroquias	
                            /*   $('#municipio').change(function()
                               {
                                   var id_municipio = $(this).val();
                                   
                                   // Lista de parroquias
                                   $.post( '<?php echo constant ('URL');?>censo/getByMunicipioParroquia', { parroquia: id_municipio} ).done( function( respuesta )
                                   {
                                       $( '#parroquia' ).html( respuesta );
                                   });
                               });*/





                               })


                               
                          </script>    


                               </fieldset>

                               

                           </form>









           
                        </div>

                        
                    </div>

                    
                    </div>
                    

                </div>
                
                </div>



        <?php require 'views/footer.php'; ?>

        
    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>
    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    
    <!-- Chosen -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>

    <!-- Sweet alert -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/sweetalert/sweetalert.min.js"></script>

   <!-- Input Mask-->
   <script src="<?php echo constant ('URL');?>src/js/plugins/jasny/jasny-bootstrap.min.js"></script>

       <!-- Select2 -->
       <script src="<?php echo constant ('URL');?>src/js/plugins/select2/select2.full.min.js"></script>
    <!-- Data picker -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>

    <!-- Typehead -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>

    
    <!-- iCheck -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/iCheck/icheck.min.js"></script>
    
          <!-- Solo Letras -->
    <script src="<?php echo constant ('URL');?>src/js/val_letras.js"></script>

        
    <!-- form step-->
    <script>

        function mostrar2(id){
            //inicio radiobutton
            if(id == "si"){
                $("#wdiscapacidades").show();
                $("#tdiscapacidad0").addClass('required');
                $("#discapacidad0").addClass('required');
                $("#conadis").addClass('required');
                $("#observaciones0").addClass('required');

                
            }

            if(id == "no"){
                $("#wdiscapacidades").hide();
                $("#tdiscapacidad0").removeClass('required');
                $("#discapacidad0").removeClass('required');
                $("#conadis").removeClass('required');
                $("#observaciones0").removeClass('required');

            }
            //fin radiobutton
            //inicio select
            if(id == "1"){//sies igual a docente muestro esto 
                $("#wdoc").show();
                $("#tdoc").show();
                $("#programa").addClass('required');
                $("#eje_regional").addClass('required');
                $("#centro_estudio").addClass('required');
                $("#dedicacion").addClass('required');
                $("#escalafon").addClass('required');
                $("#clasificacion").addClass('required');
                $("#estatus_d").addClass('required');

            }else{//sino oculto el div
                $("#wdoc").hide();
                $("#tdoc").hide();
                $("#programa").removeClass('required');
                $("#eje_regional").removeClass('required');
                $("#centro_estudio").removeClass('required');
                $("#dedicacion").removeClass('required');
                $("#escalafon").removeClass('required');
                $("#clasificacion").removeClass('required');
                $("#estatus_d").removeClass('required');

            }
            //fin select


        }

        $(document).ready(function(){

            
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                   
                   
                    ////////////////////////////////////////       
                    $('.chosen-select').chosen({width: "100%"});

                    
                    
                    ////////////////////////////////////////   


                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            
                                ano_r: {

                                    esfecha: true

                                }
                        }
                       
                    });
       });
       
       $.validator.addMethod("esfecha", esFechaActual, "El Año de Realización no debe ser mayor al Año Actual");


        function esFechaActual(value, element, param) {

            
            var fechaActual = <?php echo date('Y');?>;

            if (value > fechaActual) {

                return false; //error de validación

            }

            else {

                return true; //supera la validación

            }

        }

    </script>

   

</body>
</html>
