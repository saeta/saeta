<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Detalle Familiar | SIDTA</title>


 
    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">


</head>
<style>
td:hover{
    background-color: #eed92eba;
    color: #111;
    cursor: default;
}
tr:hover{
    background-color: #cac0c08c;;
    color: #111;

}
</style>
<body>
 
   <?php require 'views/header.php'; ?>
   

   <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-5">
                    <h2>Detalle de Familiar</h2>
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <a href="<?php echo constant ('URL') . "censo_docente/verUsuarioInfo/". $this->id_persona;?>">Detalle de Usuario</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#"><strong>Detalle de Familiar</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-7">
                    <div class="title-action">
           
                        <a href="<?php echo constant ('URL') . "censo_docente/verUsuarioInfo/". $this->id_persona;?>" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Volver</a>
                       
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
       <br>
            <div class="row ">
                
               
              

                    <!-- CARGA FAMILIAR -->

                    <?php //if(!empty($this->vista)){//si Posee familiares registrados?>
                
                <div class="col-lg-12">
                    <div  class="panel panel-success">
                        <div style="color: black;" class="panel-heading">
                        <h3 class="text-center">Datos de Familiar del Trabajador(a) Académico(a) C.I.
                        <?php echo substr($this->informacion->documento_identidad, 0, 1) . "-".$this->informacion->identificacion . ", " . 
                                    $this->informacion->primer_nombre . " " . $this->informacion->primer_apellido; ?>
                        </h3>
                        </div>
                        
                        <?php if(empty($this->persona)){?>
                           <center><h4>No se encontraron registros.</h4></center>
                        <?php }else{ ?>
                           
                            <div class="panel-body row">
                        <div class="col-lg-6">
                            <table class="table table-bordered" style="width: 100%;">
                                     <tr>
                                        <td style="font-weight: 400;">C.I.:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php if($this->persona->cedfam !=0){ echo $this->persona->cedfam;} ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Nombres:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->persona->pnombre . " " . $this->persona->snombre; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Apellidos:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->persona->papellido . " " . $this->persona->sapellido; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Estado Civil:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->persona->estado_civil; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Género:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->persona->sexfam; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Nacionalidad:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->persona->nacionalidad; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Fecha de Nacimiento:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo date("d/m/Y", strtotime($this->persona->fecha_nac)); ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Parentesco:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->persona->parentesco; ?></strong></td>
                                    </tr>
                                   
                                    <tr>
                                        <td style="font-weight: 400;">Tipo de Trabajador:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->persona->tipo_trabajador; ?></strong></td>
                                    </tr>
                                </table>
                            </div>
                        <div class="col-lg-6">
                            <table class="table table-bordered" style="width: 100%;">
                                <tr>
                                    <td colspan="2" style="text-align: center; color: #000;"><b>Datos de Contacto</b></td>
                                </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Teléfono:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->persona->telefono; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Correo:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->persona->correo; ?></strong></td>
                                    </tr>
                                   
                                    <tr>
                                        <td style="text-align: center; color: #000;" colspan="2"><b>Domicilio</b></td>
                                        
                                    </tr>
                                     <tr>
                                        <td style="font-weight: 400;">Pais:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->persona->pais; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;">Estado:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->persona->desest; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;"> Ciudad de Residencia:</td>
                                        <td style="text-align: right; font-size: 16px;"><strong><?php echo $this->persona->ciudad_residencia; ?></strong></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: 400;" colspan="2">Dirección:</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: justify; font-size: 12px;" colspan="2"><strong><?php echo $this->persona->direccion; ?></strong></td>
                                    </tr>
                                    
                                </table>

                    </div>



                    <div class="col-lg-12">
                    <table class="table table-bordered" style="width: 100%;">
                           
                           <tr>
                             
                               <td>Estatus : 
                               <?php if($this->persona->estatus==0){
                              echo"<center><span class='label label-danger' style='font-size: 12px;'>Inactivo</span></center>";
                                }else{
                              echo"<center><span class='label label-primary' style='font-size: 12px;'>&nbsp; Activo &nbsp;</span></center>";
                                 }  ?>
                                 </td>

                            <?php if(!empty($this->persona->observacion)){ ?>
                                <td>
                               Observaciones : <b><?php echo $this->persona->observacion; ?>
                               </td> 
                            <?php } ?>

                           </tr>
                       

                       </table>     
                       </div>


                    <?php }?>
                </div>
                <?php //} ?>
                    <!-- //////////////-->








            </div>
        
        </div>



    <?php require 'views/footer.php'; ?>

     

    <!-- dataTables Scripts -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>

   

</body>
</html>










