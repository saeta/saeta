<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Censo Docente | SIDTA</title>


    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">
  
    
    <!-- jasny input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <!--  style select2 -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/select2/select2.min.css" rel="stylesheet">
    <!-- datapicker -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <!-- sweetalert input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- chosen -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">

</head>

<body>
 
    <div id="wrapper">
   <?php require 'views/header.php'; ?>
   

        <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-9">
                    <h2>Censo Docente</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>censo_docente"> Listar Usuarios</strong></a>
                        </li>
                        <li class="breadcrumb-item active">
                            <a href="#>"><strong>Censo Docente</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-3">
                    <div class="title-action">
                        <a href="<?php echo constant ('URL');?>censo_docente" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Volver</a>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5><i class="fa fa-angle-double-right"></i> SIDTA <i class="fa fa-angle-double-left"></i></h5>
                        </div>
                        <div class="ibox-content">
                        
                            <h2>
                                Censar Usuario (a) SIDTA
                            </h2>
                            <p>
                                Edita la información de un Usuario dentro de SIDTA.
                            </p>
                            <?php echo $this->mensaje; ?>
                            <div class="alert alert-info">Todos los campos marcados con un (<span style="color: red;">*</span>) Son Obligatorios</div>
                            <form class="m-t" <?php if($this->action=="editUser"){ //para identificar cuando estamos editando un usuario existente?>
                                                action="<?php echo constant ('URL') . "censo_docente/editUser/" . $this->id_persona;?>"
                                                <?php }elseif($this->action=="addUser"){//sino existe el usuario lo añadimos a sidta ?>
                                                    action="<?php echo constant ('URL');?>censo_docente/addUser"
                                                <?php } ?> role="form" id="form" method="post">
                            <h1>Nacionalidad</h1>
                                <fieldset>
                                    <h2>Nacionalidad</h2>
                                    <div class="msj" class="alert"></div>
                                    <div class="row">
                                        
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Nro. de Identificación <span style="color: red;">*</span></label>
                                                <input type="text" minlength="5" maxlength="10" name="ndocumento" id="ndocumento" class="form-control required number" value="<?php echo $this->informacion->identificacion; ?>" placeholder="Ingrese su nro de Identificación (Cédula, Pasaporte, etc.)">
                                                <span><i>(Cédula, Pasaporte, etc.)</i> </span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Estado Civil <span style="color: red;">*</span></label>                                        
                                                <select  name="estadocivil" id="estadocivil"  style="width: 100%;" class="form-control required m-b">                                    
                                                    <option value="">Seleccione </option> 
                                                    <?php include_once 'models/persona.php';
                                                        foreach($this->estadosCivil as $row){
                                                        $estado=new Persona();
                                                        $estado=$row;?> 
                                                    <option value="<?php echo $estado->id;?>" <?php if($estado->id == $this->informacion->id_estado_civil){ print "selected=selected"; }?>><?php echo $estado->descripcion;?></option>
                                                <?php }?>                                            
                                                </select>   
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Tipo de Documento <span style="color: red;">*</span></label>                                        
                                                <select  name="tdocumento" id="tdocumento"  style="width: 100%;" class="form-control required m-b">                                    
                                                    <option selected="selected" value="">Seleccione </option> 
                                                    <?php include_once 'models/persona.php';
                                                        foreach($this->documentoidentidad as $row){
                                                        $documento=new Persona();
                                                        $documento=$row;?> 
                                                    <option value="<?php echo $documento->id;?>" <?php if($documento->id == $this->informacion->id_documento_identidad_tipo){ print "selected=selected"; }?>><?php echo $documento->descripcion;?></option>
                                                <?php }?>             
                                                </select>   
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Género<span style="color: red;">*</span></label>                                        
                                                <select  style="width: 100%;" class="form-control required m-b" name="genero" id="genero">                                    
                                                    <option selected="selected" value="">Seleccione</option> 
                                                    <?php include_once 'models/persona.php';
                                                        foreach($this->generos as $row){
                                                        $genero=new Persona();
                                                        $genero=$row;?> 
                                                    <option value="<?php echo $genero->id;?>" <?php if($genero->id == $this->informacion->id_genero){ print "selected=selected"; }?>><?php echo $genero->descripcion;?></option>
                                                <?php }?>                                            
                                                </select>   
                                            </div>
                                            <div class="form-group">
                                                <label>Nacionalidad <span style="color: red;">*</span></label>     
                                                <select class="form-control required m-b" name="nacionalidad" id="nacionalidad">        
                                                <option selected="selected" value="">Seleccione </option> 
                                                <?php include_once 'models/persona.php';
                                                    foreach($this->nacionalidades as $row){
                                                    $nacionalidad=new Persona();
                                                    $nacionalidad=$row;?> 
                                                <option value="<?php echo $nacionalidad->id_nacionalidad;?>" <?php if($nacionalidad->id_nacionalidad == $this->informacion->id_nacionalidad){ print "selected=selected"; }?>><?php echo $nacionalidad->descripcion;?></option>
                                                <?php }?>
                                                </select> 
                                            </div>    
                                        </div>
                                        
                                    </div>
                                </fieldset>
                                <h1>Datos de la Persona</h1>
                                <fieldset>
                                    <h2>Datos Personales</h2>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Primer Nombre <span style="color: red;">*</span></label>
                                                <input id="pnombre" name="pnombre" type="text" class="form-control required" maxlength='29' minlength="3" value="<?php echo $this->informacion->primer_nombre; ?>" placeholder="Ingrese el Primer Nombre">
                                            </div>
                                            <div class="form-group">
                                                <label>Primer Apellido <span style="color: red;">*</span></label>
                                                <input id="papellido" name="papellido" value="<?php echo $this->informacion->primer_apellido; ?>" type="text" class="form-control required" maxlength='29' minlength="3" placeholder="Ingrese el Primer Apellido">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Segundo Nombre <i>(Opcional)</i></label>
                                                <input id="snombre" name="snombre" value="<?php echo $this->informacion->segundo_nombre; ?>" type="text" class="form-control" maxlength='29' minlength="3" placeholder="Ingrese el Segundo Nombre">
                                                </div>  
                                                <div class="form-group">
                                                <label>Segundo Apellido <i>(Opcional)</i></label>
                                                <input id="sapellido" name="sapellido" value="<?php echo $this->informacion->segundo_apellido; ?>" type="text" class="form-control" maxlength='29' minlength="3"  onkeypress="return soloLetras(event)" placeholder="Escriba su Segundo Apellido">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            
                                            <div class="form-group" id="data_1">
                                                <label class="font-normal">Fecha de Nacimiento <span style="color: red;">*</span><label id="fnac-error" class="error" for="fnac"></label></label>
                                                <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <input type="text" value="<?php echo $this->informacion->fecha_nacimiento;?>" name="fnac" id="fnac" class="form-control required" placeholder="mm/dd/aaaa">
                                                </div>
                                                
                                                <span><i>mm/dd/aaaa</i></span>
                                                
                                            </div>
                                            <script>
                                                var mem = $('#data_1 .input-group.date').datepicker({
                                                    todayBtn: "linked",
                                                    keyboardNavigation: false,
                                                    forceParse: false,
                                                    calendarWeeks: true,
                                                    autoclose: true
                                                });
                                            </script>
                                            
                                        </div>
                                        <div class="col-lg-6">
                                            
                                            <div class="form-group">
                                                <label>País de Nacimiento <span style="color: red;">*</span></label>                                        
                                                <select class="form-control required m-b" name="paisnac" id="paisnac">                                    
                                                <option selected="selected" value="">Seleccione </option>
                                                <?php 
                                                        foreach($this->paises as $row){
                                                        $pais=new Estructura();
                                                        $pais=$row;?> 
                                                    <option value="<?php echo $pais->id_pais;?>" <?php if($pais->id_pais==$this->informacion->id_pais){ print "selected=selected";}?>><?php echo $pais->descripcion;?></option>
                                                    <?php }?>    
                                                </select> 
                                                  
                                            </div>
                                           
                                            <script>
                                                $(".select2_demo_3").select2({
                                                    placeholder: "Seleccione",
                                                    width: "100%",
                                                    dropdownAutoWidth: true,
                                                    allowClear: true
                                                });
                                            </script>
                                            
                                        </div>
                                    </div>
                                    
                                </fieldset> 
                                 
                                <h1>Contacto</h1>
                                <fieldset>
                                    <h2>Datos de Contacto</h2>
                                    <div class="msj" class="alert"></div>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Tipo de Teléfono <span style="color: red;">*</span></label>                                        
                                                    <select class="select2_demo_3 form-control required m-b" name="tipotelf" id="tipotelf">                                    
                                                    <option selected="selected" value="">Seleccione </option> 
                                                    <?php include_once 'models/persona.php';
                                                        foreach($this->telefono_tipo as $row){
                                                        $telefono=new Persona();
                                                        $telefono=$row;?> 
                                                    <option value="<?php echo $telefono->id;?>" <?php if($telefono->id == $this->informacion->id_telefono_tipo){ print "selected=selected"; }?>><?php echo $telefono->descripcion;?></option>
                                                    <?php }?>                                            
                                                </select>   
                                            </div>   
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label> Código de Área<span style="color: red;">*</span></label>  
                                                <select class="form-control required m-b" name="cod_area" id="cod_area">                                    
                                                    <option selected="selected" value="">----- </option> 
                                                    <?php include_once 'models/persona.php';
                                                        foreach($this->cod_area_telefono as $row){
                                                        $cod_area=new Persona();
                                                        $cod_area=$row;?> 
                                                    <option value="<?php echo $cod_area->id;?>" <?php if($cod_area->id == $this->informacion->id_telefono_codigo_area){ print "selected=selected"; }?>><?php echo $cod_area->descripcion;?></option>
                                                    <?php }?>                                            
                                                </select>                                        </div>

                                            <div class="form-group">
                                            <label>Correo Personal <span style="color: red;">*</span></label> 
                                            <input type="hidden" name="id_persona_correo" value="<?php echo $this->informacion->correo_personal["id_persona_correo"]; ?>">
                                            <input type="hidden" name="id_correo_personal" value="<?php echo $this->informacion->correo_personal["id_correo_tipo"]; ?>">
                                            <input type="hidden" name="desc_personal" value="<?php echo $this->informacion->correo_personal["correo_tipo"]; ?>"> 
                                            <!-- hay dos casos diferentes aca uno para cuando el usuario esta registrado y otro cuando no -->
                                            <input id="correo_personal" value="<?php if(is_array($this->informacion->correo_personal)){ echo $this->informacion->correo_personal["correo"]; }else{ echo $this->informacion->correo_personal; } ?>" name="correo_personal" type="email" class="form-control mail required" maxlength='49' minlength="7" Placeholder="Ingrese el Correo Electrónico Personal">  
                                            </div>    
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label> Teléfono <span style="color: red;">*</span></label>  
                                                <input name="numero" value="<?php echo $this->informacion->telefono; ?>" minlength="7" maxlength="7" placeholder="Ingrese el Número de Teléfono" id="numero"   class="form-control number required " aria-required="true" type="text">
                                            </div> 
                                            <div class="form-group">
                                                <label>Correo Institucional <i>(Opcional)</i></label>
                                                <!-- hay dos casos diferentes aca uno para cuando el usuario esta registrado y otro cuando no -->
                                                <input id="correo_institucional" value="<?php if(!empty($this->informacion->correo_institucional)){ 
                                                                                                if(is_array($this->informacion->correo_institucional)){ //cuando existe el usuario esto viene como array
                                                                                                    echo $this->informacion->correo_institucional["correo"]; 
                                                                                                }else{//cuando el usuario no existe viene como un string
                                                                                                    echo $this->informacion->correo_institucional;
                                                                                                }
                                                                                            } ?>" name="correo_institucional" type="email" class="form-control mail" maxlength='49' minlength="7" Placeholder="Ingrese el Correo Electrónico Institucional">
                                                <input type="hidden" name="id_correo_institucional" value="<?php if(!empty($this->informacion->correo_institucional)){ echo $this->informacion->correo_institucional["id_correo_tipo"]; } ?>">
                                                <input type="hidden" name="desc_institucional" value="<?php if(!empty($this->informacion->correo_institucional)){ echo $this->informacion->correo_institucional["correo_tipo"];} ?>">                                       
                                                <input type="hidden" name="id_persona_institucional" value="<?php if(!empty($this->informacion->correo_institucional)){echo $this->informacion->correo_institucional["id_persona_correo"]; }?>">

                                            </div>
                                        </div>
                                    </div>
                                </fieldset> 
                                <h1>Domicilio</h1>
                                <fieldset>
                                    <h2>Domicilio</h2>
                                    <div class="msj" class="alert"></div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>País donde Reside <span style="color: red;">*</span></label>                                        
                                                <select class="select2_demo_3 form-control required m-b" name="pais" id="pais">                                    
                                                <option selected="selected" value="">Seleccione </option>
                                                <?php 
                                                        foreach($this->paises as $row){
                                                        $pais=new Estructura();
                                                        $pais=$row;?> 
                                                    <option value="<?php echo $pais->id_pais;?>" <?php if($pais->id_pais==$this->domicilio_p->id_pais){ print "selected=selected";}?>><?php echo $pais->descripcion;?></option>
                                                    <?php }?>    
                                                </select> 
                                                
                                            </div>
                                            <div class="form-group">
                                                <label>Municipio donde Reside <span style="color: red;">*</span></label>

                                                <select name="municipio" style="width: 100%;" id="municipio" class="form-control required">
                                                    <option id="tmunicipio" value="">Seleccione el municipio donde habita</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Estado donde Reside <span style="color: red;">*</span></label>
                                                <select name="estado" style="width: 100%;" id="estado" class="form-control  required">
                                                    <option id="testado" value="">Seleccione el Estado donde habita</option>
                                                </select>
                                                
                                            </div>
                                            <div class="form-group">
                                                <label>Parroquia donde Reside <span style="color: red;">*</span></label>
                                                <select name="parroquia" style="width: 100%;" id="parroquia" class="form-control required">
                                                    <option id="tparroquia" value="">Seleccione la parroquia donde reside</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Tipo de Domicilio <span style="color: red;">*</span></label>                                        
                                                <select  name="tdomicilio" id="tdomicilio"  class="select2_demo_3 form-control required m-b">                                    
                                                    <option selected="selected" value="">Seleccione </option> 
                                                    <?php 
                                                        foreach($this->domicilios as $row){
                                                        $domicilio=new Estructura();
                                                        $domicilio=$row;?> 
                                                    <option value="<?php echo $domicilio->id;?>" <?php if($domicilio->id == $this->domicilio_p->id_domicilio_detalle_tipo){ print "selected=selected"; }?>><?php echo $domicilio->descripcion;?></option>
                                                    <?php }?>                                            
                                                </select>   
                                            </div>
                                            <div class="form-group">
                                                <label>Dirección<span style="color: red;">*</span></label>
                                                <textarea id="direccion" name="direccion" type="text" class="form-control required" placeholder="Escriba una dirección..." aria-required="true" aria-invalid="false"  maxlength='150' minlength="4"><?php if($this->action=="editUser"){//para identificar cuando estamos editando un usuario existente
                                                            echo $this->domicilio_p->domicilio_detalle; 
                                                        }elseif($this->action=="addUser"){//sino existe el usuario lo añadimos a sidta
                                                            echo $this->informacion->direccion; 
                                                        }?></textarea>
                                                        
                                                                
                                                            
                                            </div>
                                        </div>
                                        <script>
                                            $('#pais').change(function(){
                                                var id_pais = $(this).val();
                                                console.log(id_pais);
                                                $.post( '<?php echo constant ('URL');?>usuarios/getEstadosPais', { pais: id_pais} ).done( function( respuesta )
                                                {
                                                    $( '#estado' ).html( respuesta );
                                                    
                                                });
                                            });

                                            // lista de MUNICIPIOS	
                                            $('#estado').change(function()
                                            {
                                                var id_estado = $(this).val();
                                                
                                                // Lista de estados
                                                $.post( '<?php echo constant ('URL');?>usuarios/getMunicipiosEstado', { estado: id_estado} ).done( function( respuesta )
                                                {
                                                    $( '#municipio' ).html( respuesta );
                                                });
                                            });

                                            // lista de municipios	
                                            $('#municipio').change(function()
                                            {
                                                var id_municipio = $(this).val();
                                                
                                                
                                                $.post( '<?php echo constant ('URL');?>usuarios/getParroquiasMunicipio', { municipio: id_municipio} ).done( function( respuesta )
                                                {
                                                    $( '#parroquia' ).html( respuesta );
                                                });
                                            });

                                            <?php if($this->action=="editUser"){//para identificar cuando estamos editando un usuario existente ?>
                                            //editar estado
                                            $.post( '<?php echo constant ('URL');?>usuarios/getEstadosPais', { pais: <?php echo $this->domicilio_p->id_pais;?>} ).done( function( respuesta )
                                            {
                                                $( '#estado' ).html( respuesta );
                                                
                                                $( '#estado option[value="'+<?php echo $this->domicilio_p->id_estado; ?>+'"]' ).attr("selected", true);

                                                if($("#estado").val() == '<?php echo $this->domicilio_p->id_estado;?>'){
                                                    $('#testado').append('<option value="<?php echo $this->domicilio_p->id_estado;?>" selected="selected"><?php echo $this->domicilio_p->estado;?></option>');

                                                }
                                            });

                                            

                                            //editar municipio
                                            $.post( '<?php echo constant ('URL');?>usuarios/getMunicipiosEstado', { estado: <?php echo $this->domicilio_p->id_estado;?>} ).done( function( respuesta )
                                            {
                                                $( '#municipio' ).html( respuesta );
                                                
                                                $( '#municipio option[value="'+<?php echo $this->domicilio_p->id_municipio; ?>+'"]' ).attr("selected", true);

                                                if($("#municipio").val() == '<?php echo $this->domicilio_p->id_municipio;?>'){
                                                    $('#tmunicipio').append('<option value="<?php echo $this->domicilio_p->id_municipio;?>" selected="selected"><?php echo $this->domicilio_p->municipio;?></option>');

                                                }
                                            });
                                            
                                            
                                            // editar municipio
                                            $.post( '<?php echo constant ('URL');?>usuarios/getParroquiasMunicipio', { municipio: <?php echo $this->domicilio_p->id_municipio;?>} ).done( function( respuesta )
                                            {
                                                $( '#parroquia' ).html( respuesta );
                                                
                                                $( '#parroquia option[value="'+<?php echo $this->domicilio_p->id_parroquia; ?>+'"]' ).attr("selected", true);

                                                if($("#parroquia").val() == '<?php echo $this->domicilio_p->id_parroquia;?>'){
                                                    $('#tparroquia').append('<option value="<?php echo $this->domicilio_p->id_parroquia;?>" selected="selected"><?php echo $this->domicilio_p->parroquia;?></option>');

                                                }
                                            }); 

                                            <?php } ?>
                                        </script>
                                    </div>
                                </fieldset> 
                                <h1>Datos de Interes</h1>
                                <fieldset>
                                    <h2>Datos de Interes</h2>
                                    <div class="msj" class="alert"></div>

                                    <div class="row">
                                    <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Número de Hijos <span style="color: red;">*</span></label>  
                                                <input name="nhijos" value="<?php echo $this->informacion->nro_hijos; ?>" placeholder="Ingrese el Número de Hijos" id="nhijos" class="form-control number required " aria-required="true" type="number">
                                            </div> 
                                        </div>
                                        <div class="col-lg-6 row">
                                            <div class="col-md-6"><div class="form-group" id="data_2">
                                                <label class="font-normal">Fecha de Ingreso a la UBV <span style="color: red;">*</span></label>
                                                <label id="fingreso-error" class="error" for="fingreso"></label>

                                                <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <input type="text"  name="fingreso" value="<?php echo $this->informacion->fecha_ingreso;?>" id="fingreso" class="form-control required" placeholder="mm/dd/aaaa">
                                                </div>
                                                
                                                <span><i>mm/dd/aaaa</i></span>
                                             </div></div>
                                            <div class="col-md-6">
                                                <div class="bg-success p-xs b-r-md"> Si existe algún inconveniente con la <strong>fecha de ingreso</strong>, por favor dirijase a <strong>Talento Humano.</strong></div>
                                            </div>
                                            <script>
                                                $('#fingreso').attr('readonly', true).css({cursor: "not-allowed" });
                                            </script>
                                        </div>
                                        <div class="col-lg-6">
                                           
                                            <div class="form-group">
                                            <label>Etnia Indígena <i>(Opcional)</i></label>                                        
                                                <select class="select2_demo_3 form-control m-b" name="etnia" id="etnia">                                    
                                                    <option value="">Seleccione </option> 
                                                    <?php include_once 'models/estructura.php';
                                                        foreach($this->etnias as $row){
                                                        $etnia=new Estructura();
                                                        $etnia=$row;?> 
                                                    <option value="<?php echo $etnia->id;?>"<?php if($etnia->id == $this->informacion->id_etnia){ print "selected=selected"; }?>><?php echo $etnia->descripcion;?></option>
                                                    <?php } ?>    
                                                </select>   
                                            </div> 
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>¿Posee alguna discapacidad? <span style="color: red;">*</span> </label>
                                                <div class="i-checks"><label> <input type="radio" id="optionsRadios1" <?php if(!empty($this->disc[1]['id_discapacidad1'])){ echo 'checked="" value="si"'; }?> onChange="mostrar_radio('si');"  name="pdisc"> <i></i> - Si </label></div>
                                                <div class="i-checks"><label> <input type="radio" id="optionsRadios2" <?php if(empty($this->disc[1]['id_discapacidad1'])){ echo 'checked="" value="no"'; }elseif(!empty($this->disc[1]['id_discapacidad1'])){ echo 'disabled="disabled"'; }?> onChange="mostrar_radio('no');"  name="pdisc"> <i></i> - No </label></div>
                                            </div>
                                            
                                        </div>
                                       
                                        <script>
                                            $(document).ready(function () {
                                                var valor= $('#optionsRadios1').val();
                                                var valor2= $('#optionsRadios2').val();

                                                if(valor=="si" || valor2=="si" ){
                                                    mostrar_radio('si');
                                                }else if(valor2=="no" || valor=="no"){
                                                   
                                                    mostrar_radio('no');
                                                }

                                            });
                                        </script>
                                    </div>
                                    <div id="wdiscapacidades" class="row">
                                        
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Tipo de discapacidad <span style="color: red;">*</span></label>                                        
                                                <select  class="select2_demo_3 form-control required m-b " name="tdiscapacidad" id="tdiscapacidad0">              
                                                    <option selected="selected" value="">Seleccione </option>
                                                    <?php 
                                                        foreach($this->tipo_discapacidades as $row){
                                                        $tipo_discapacidad=new Estructura();
                                                        $tipo_discapacidad=$row;?> 
                                                    <option value="<?php echo $tipo_discapacidad->id;?>" <?php if($tipo_discapacidad->id == $this->disc[1]['id_tipo_discapacidad1']){ print "selected=selected"; }?>><?php echo $tipo_discapacidad->descripcion;?></option>
                                                    <?php }?> 
                                                </select> 
                                            </div> 
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Discapacidad <span style="color: red;">*</span></label>                                        
                                                <select class="required form-control m-b" name="discapacidad" id="discapacidad0">                                    
                                                <option id="tdiscapacidad" selected="selected" value="">Seleccione </option> 
                                                </select>   
                                            </div>  
                                            
                                        </div>
                                        
                                        <script>
                                            $('#tdiscapacidad0').change(function(){
                                                var id_tipo_discapacidad = $(this).val();
                                                console.log(id_tipo_discapacidad);
                                                // Lista de discapacidades
                                                $.post( '<?php echo constant ('URL');?>usuarios/getDiscapacidadesTipo', { tipo_discapacidad: id_tipo_discapacidad} ).done( function( respuesta )
                                                {
                                                    $( '#discapacidad0' ).html( respuesta );
                                                });
                                            });

                                            // editar discapacidad
                                            <?php if(!empty($this->disc[1]['id_discapacidad1'])){?>
                                            $.post( '<?php echo constant ('URL');?>usuarios/getDiscapacidadesTipo', { tipo_discapacidad: <?php echo $this->disc[1]['id_tipo_discapacidad1'];?>} ).done( function( respuesta )
                                            {
                                                $( '#discapacidad0' ).html( respuesta );
                                                
                                                $( '#discapacidad0 option[value="'+<?php echo $this->disc[1]['id_discapacidad1']; ?>+'"]' ).attr("selected", true);

                                                if($("#discapacidad0").val() == '<?php echo $this->disc[1]['id_discapacidad1'];?>'){
                                                    $('#tdiscapacidad').append('<option value="<?php echo $this->disc[1]['id_discapacidad1'];?>" selected="selected"><?php echo $this->disc[1]['discapacidad1'];?></option>');

                                                }
                                            });
                                        <?php } ?>
                                        </script>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Código CONAPDIS</label>
                                                <input id="conadis" value="<?php echo $this->disc[1]['codigo_conapdis1']; ?>" name="conadis" type="text" class="form-control number" maxlength='10' minlength="4" placeholder="Ingrese el Código CONAPDIS">
                                            </div>  
                                            <div class="form-group">
                                                <label>Observaciones</label>
                                                <textarea id="observaciones0" name="observaciones" type="text" class="form-control" placeholder="Escriba una observación..." maxlength='200' minlength="10"><?php echo $this->disc[1]['observacion1']; ?></textarea>
                                                <?php if(count($this->informacion_d)!=2){ ?>
                                                <a style='cursor: pointer;' onClick="muestra_oculta('contenido')" title="" class="boton_mostrar">Agregar discapacidad / Ocultar</a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <!-- ////////////////////////////Div de Otra discapacidad//////////////////////////////////////////////7-->
                                    <div id="contenido" <?php if(count($this->informacion_d)==2){ echo "style='display: none;'"; }?>>
                                        <h2>Discapacidad</h2>

                                        <div class="form-row">

                                            <div class="form-group col-md-6">
                                            <div id="seccion_Discapacidad1" style="display:none;"></div>
                                            <div class="form-group"  id="seccion_inputs_Tipo_Discapacidad1" style="display:;">
                                            <label>Tipo de discapacidad</label>                                        
                                            <select  class="select2_demo_3 form-control  m-b " name="tdiscapacidad1" id="tdiscapacidad1">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                                    <option selected="selected" value="">Seleccione </option>
                                                    <?php 
                                                        foreach($this->tipo_discapacidades as $row1){
                                                        $tipo_discapacidad1=new Estructura();
                                                        $tipo_discapacidad1=$row1;?> 
                                                    <option value="<?php echo $tipo_discapacidad1->id;?>" <?php if($tipo_discapacidad1->id == $this->disc[2]['id_tipo_discapacidad2']){ print "selected=selected"; }?>><?php echo $tipo_discapacidad1->descripcion;?></option>
                                                    <?php }?> 
                                            </select> 
                                            </div>                                                            
                                            </div>

                                            <div class="form-group col-md-6">                                 
                                            <div id="seccion_Tipo_Discapacidad1" style="display:none;"></div>
                                            <div class="form-group "  id="seccion_inputs_Discapacidad1" style="display:;">
                                            <label>Discapacidad</label>                                        
                                            <select class="form-control m-b" name="discapacidad1" id="discapacidad1">                                    
                                            <option id="tdisc1" selected="selected" value="">Seleccione </option> 
                                            </select>   
                                            </div>   
                                            </div>
                                            <script>
                                                $('#tdiscapacidad1').change(function(){
                                                    var id_tipo_discapacidad = $(this).val();
                                    
                                                    // Lista de estados
                                                    $.post( '<?php echo constant ('URL');?>usuarios/getDiscapacidadesTipo', { tipo_discapacidad: id_tipo_discapacidad} ).done( function( respuesta )
                                                    {
                                                        $( '#discapacidad1' ).html( respuesta );
                                                    });
                                                });
                                                // editar discapacidad
                                            <?php if(count($this->informacion_d)==2){?>
                                            $.post( '<?php echo constant ('URL');?>usuarios/getDiscapacidadesTipo', { tipo_discapacidad: <?php echo $this->disc[2]['id_tipo_discapacidad2'];?>} ).done( function( respuesta )
                                            {
                                                $( '#discapacidad1' ).html( respuesta );
                                                
                                                $( '#discapacidad1 option[value="'+<?php echo $this->disc[2]['id_discapacidad2']; ?>+'"]' ).attr("selected", true);

                                                if($("#discapacidad1").val() == '<?php echo $this->disc[2]['id_discapacidad2'];?>'){
                                                    $('#tdisc1').append('<option value="<?php echo $this->disc[2]['id_discapacidad2'];?>" selected="selected"><?php echo $this->disc[2]['discapacidad2'];?></option>');

                                                }
                                            });
                                        <?php } ?>
                                            </script>
                                    
                                        </div>
                                        <div id="seccion_Observaciones1" style="display:none;"></div>
                                        <div class="form-group"  id="seccion_inputs_Observaciones1" style="display:;">
                                            <label>Observaciones</label>
                                            <textarea id="observaciones1" name="observaciones1" type="text" class="form-control" placeholder="Escriba una observación..." aria-required="true" aria-invalid="false" onkeypress="return soloLetras(event)"  maxlength='200' minlength="10"><?php echo $this->disc[2]['observacion2']; ?></textarea>
                                        </div>


                                    </div>

                                    <script>

function muestra_oculta(id){
if (document.getElementById){ //se obtiene el id
var el = document.getElementById(id); //se define la variable "el" igual a nuestro div
el.style.display = (el.style.display == 'none') ? 'block' : 'none'; //damos un atributo display:none que oculta el div
//console.log(el.style.display );

//agregamos clases required en caso de que el ancla sea mostrado
        if(el.style.display=='block'){
                    myElemento = document.getElementById("tdiscapacidad1");
                    myElemento.classList.add('required');
                    myElemento2 = document.getElementById("discapacidad1");
                    myElemento2.classList.add('required');
                    myElemento3 = document.getElementById("observaciones1");
                    myElemento3.classList.add('required');
        }else{
                    myElemento = document.getElementById("tdiscapacidad1");
                    myElemento.classList.remove('required');
                    myElemento2 = document.getElementById("discapacidad1");
                    myElemento2.classList.remove('required');
                    myElemento3 = document.getElementById("observaciones1");
                    myElemento3.classList.remove('required');

        }
 //para aumentar tamaño del fieldset
 divC = document.getElementById("ver");
 divC.style="775px";
}


}
window.onload = function(){/*hace que se cargue la función lo que predetermina que div estará oculto hasta llamar a la función nuevamente*/
muestra_oculta('contenido');/* "contenido_a_mostrar" es el nombre que le dimos al DIV */

}


</script>

                                </fieldset> 
                                <h1>Datos de Interes</h1>
                                <fieldset>
                                  

                                    <!-- en caso de que el usuario sea un docente  none-->
                                    <h2 id="tdoc" style="display: none;">Datos del (la) Trabajador(a) Académico</h2>
                                   <br>
                                    <div class="row" id="wdoc" style="display:none ;">
                                    <input type="hidden" name="id_docente" id="id_docente" value="<?php echo $this->informacion->id_docente; ?>">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>PFG, PNF o PFA <span style="color: red;">*</span>&nbsp;<span id="programa-error" class="error" for="programa"></span></label>
                                                
                                                <select  name="programa[]" id="programa" data-placeholder="Seleccione los Programas de Formación" class="chosen-select required form-control m-b" multiple style="width:350px;" tabindex="4">
                                                    <?php 
                                                        foreach($this->programas as $row){
                                                        $programa=new Estructura();
                                                        $programa=$row;?> 
                                                    <option value="<?php echo $programa->id_programa;?>" <?php 
                                                            $j=1;
                                                            while($j<=count($this->programas_docente)){
                                                                
                                                                if($programa->id_programa==$this->doc_prog[$j]['id_programa'.$j]){ 
                                                                    echo "selected='selected'"; 
                                                                }
                                                                $j++;
                                                            } ?>><?php echo $programa->descripcion;?></option>
                                                    <?php }?>                                        
                                                </select> 
                                            </div>
                                            <div class="form-row">
                                            
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Eje Regional <i>(ER)</i> <span style="color: red;">*</span></label>
                                                        <select  name="eje_regional" style="width: 100%;" id="eje_regional"  class="select2_demo_3 form-control m-b">                                    
                                                            <option selected="selected" value="">Seleccione el ER al que Pertenece</option> 
                                                                <?php 
                                                                    foreach($this->ejes_regionales as $row){
                                                                    $eje_regional=new Estructura();
                                                                    $eje_regional=$row;?> 
                                                            <option value="<?php echo $eje_regional->id_eje_regional;?>" <?php if($this->informacion->id_eje_regional==$eje_regional->id_eje_regional){echo "selected='selected'"; }?>><?php echo $eje_regional->descripcion;?></option>
                                                                <?php }?>                                            
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Eje Municipal <i>(EM)</i> <span style="color: red;">*</span></label>
                                                        <select  name="eje_municipal" style="width: 100%;" id="eje_municipal"  class="required form-control m-b">                                    
                                                            <option id="teje_municipal"selected="selected" value="">Seleccione el Eje Municipal</option> 
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                            <script>


                                                $('#eje_regional').change(function(){
                                                    var id_eje_regional = $(this).val();
                                                    
                                                    $.post( '<?php echo constant ('URL');?>usuarios/getEjeMunipalbyER', { eje_regional: id_eje_regional} ).done( function( respuesta )
                                                    {
                                                        $( '#eje_municipal' ).html( respuesta );
                                                    });
                                                });
                                                <?php if($this->action=="editUser"){//para identificar cuando estamos editando un usuario existente ?>
                                                //editar eje municipal
                                                $.post( '<?php echo constant ('URL');?>usuarios/getEjeMunipalbyER', { eje_regional: <?php echo $this->informacion->id_eje_regional;?>} ).done( function( respuesta )
                                                {
                                                    $( '#eje_municipal' ).html( respuesta );
                                                    
                                                    $( '#eje_municipal option[value="'+<?php echo $this->informacion->id_eje_municipal; ?>+'"]' ).attr("selected", true);

                                                    if($("#eje_municipal").val() == '<?php echo $this->informacion->id_eje_municipal;?>'){
                                                        $('#teje_municipal').append('<option value="<?php echo $this->informacion->id_eje_municipal;?>" selected="selected"><?php echo $this->informacion->eje_municipal;?></option>');
                                                    }
                                                });
                                                <?php } ?>
                                            </script>
                                            <div class="form-group">
                                                <label>Estatus del Docente <span style="color: red;">*</span></label>
                                                <select  name="estatus_d" style="width: 100%;" id="estatus_d"  class="form-control select2_demo_3 m-b">                                    
                                                    <option selected="selected" value="">Seleccione el Estatus </option> 
                                                    <?php 
                                                        foreach($this->docente_estatus as $row){
                                                            $d_estatus=new Estructura();
                                                            $d_estatus=$row;?> 
                                                        <option value="<?php echo $d_estatus->id;?>" <?php if($this->informacion->id_docente_estatus==$d_estatus->id){echo "selected='selected'"; }?>><?php echo $d_estatus->descripcion;?></option>
                                                    <?php }?>               
                                                </select> 
                                            </div>

                                            <div class="form-group">
                                                <label>Escalafon del Trabajador Académico <span style="color: red;">*</span></label>
                                                <select  name="escalafon" style="width: 100%;" id="escalafon"  class="form-control select2_demo_3 m-b">                                    
                                                    <option selected="selected" value="">Seleccione el Escalafón </option> 
                                                    <?php 
                                                        foreach($this->escalafones as $row){
                                                            $escalafon=new Estructura();
                                                            $escalafon=$row;?> 
                                                        <option value="<?php echo $escalafon->id;?>" <?php if($this->informacion->id_escalafon==$escalafon->id){echo "selected='selected'"; }?>><?php echo $escalafon->descripcion;?></option>
                                                    <?php }?>               
                                                </select> 
                                            </div>

                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Centro de Estudio <i>(CE)</i> <span style="color: red;">*</span></label>
                                                <select  name="centro_estudio" style="width: 100%;" id="centro_estudio"  class="select2_demo_3 form-control m-b">                                    
                                                    <option selected="selected" value="">Seleccione el CE al que pertenece </option> 
                                                    <?php 
                                                        foreach($this->centros as $row){
                                                            $centro_estudio=new Estructura();
                                                            $centro_estudio=$row;?> 
                                                        <option value="<?php echo $centro_estudio->id_centro_estudio;?>" <?php if($this->informacion->id_centro_estudio==$centro_estudio->id_centro_estudio){echo "selected='selected'"; }?>><?php echo $centro_estudio->descripcion;?></option>
                                                    <?php }?>                                            
                                                </select> 
                                            </div>  

                                            

                                            <div class="form-group">
                                                <label class="bg-success p-xs b-r-md">Tú dedicación actual es:
                                                    <strong ><?php echo $this->informacion->docente_dedicacion; ?></strong>
                                                </label>
                                                <script>
                                                    $('#desdedicacion').attr('readonly', true).css({cursor: "not-allowed" });
                                                </script>
                                                <label>Dedicación del Trabajador Académico <span style="color: red;">*</span></label>
                                                <select  name="dedicacion" style="width: 100%;" id="dedicacion"  class="form-control select2_demo_3 m-b">                                    
                                                    <option selected="selected" value="">Seleccione la Dedicación </option> 
                                                    <?php 
                                                        foreach($this->dedicaciones as $row){
                                                            $dedicacion=new Estructura();
                                                            $dedicacion=$row;?> 
                                                        <option value="<?php echo $dedicacion->id;?>" <?php if($this->informacion->id_docente_dedicacion==$dedicacion->id){echo "selected='selected'"; }?>><?php echo $dedicacion->descripcion;?></option>
                                                    <?php }?>               
                                                </select> 
                                            </div>
                                            <div class="form-group">
                                                <label>Aldea del Docente <i>(Opcional)</i></label>
                                                <input id="aldea" name="aldea" autocomplete="off" type="text" class="form-control typeahead_3" maxlength='80' minlength="3" value="<?php if(!empty($this->informacion->aldea_docente)){ echo $this->informacion->aldea_docente; } ?>" placeholder="Ingrese la Aldea del Docente">
                                            </div>
                                            <div class="form-group">
                                                <label>Clasificación del Trabajador Académico <span style="color: red;">*</span></label>
                                                <select  name="clasificacion" style="width: 100%;" id="clasificacion"  class="form-control select2_demo_3 m-b">                                    
                                                    <option selected="selected" value="">Seleccione la Clasificación </option> 
                                                    <?php 
                                                        foreach($this->clasificaciones as $row){
                                                            $clasificacion=new Estructura();
                                                            $clasificacion=$row;?> 
                                                        <option value="<?php echo $clasificacion->id;?>" <?php if($this->informacion->id_clasificacion_docente==$clasificacion->id){echo "selected='selected'"; }?>>
                                                        <?php switch($clasificacion->descripcion){
                                                                    case 'DOCENTE': 
                                                                        echo $clasificacion->descripcion . " ORDINARIO";
                                                                        break;
                                                                    case 'JUBILADOS':
                                                                    echo "DOCENTE " . str_replace('S', '', $clasificacion->descripcion);
                                                                    break;
                                                                    case 'PENSIONADOS':
                                                                    echo "DOCENTE " . substr($clasificacion->descripcion, 0, 10);
                                                                    break;
                                                                    default:
                                                                    echo "DOCENTE " .$clasificacion->descripcion;
                                                                    break;
                                                                } ?>
                                                        </option>
                                                    <?php }?>               
                                                </select> 
                                            </div>
                                        </div>
                                        <script>
                                            $(document).ready(function(){
                                                $('.typeahead_3').typeahead({
                                                    source: [
                                                        <?php 
                                                            foreach($this->aldeas_ubv as $row){
                                                                $aldea_ubv=new Estructura();
                                                                $aldea_ubv=$row; ?>
                                                            {"name": "<?php echo $aldea_ubv->descripcion;?>", "code": "BR", "ccn0": "080"},
                                                        <?php } ?>
                                                        ]
                                                });
                                            });
                                        </script>
                                    </div>
                                    <!-- en caso de que el usuario sea un docente -->

                                </fieldset> 
                               <!-- <h1>Seguridad</h1>
                                <fieldset>
                                    <h2>Seguridad de la Cuenta</h2>
                                    <div class="msj" class="alert"></div>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Contraseña <span style="color: red;">*</span></label>
                                                <input type="password" maxlength="8" name="password" value="<?php echo $this->informacion->password;?>" id="password" class="form-control" placeholder="Ingrese su Contraseña">
                                            </div>
                                            
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Confirmar Contraseña <span style="color: red;">*</span></label>
                                                <input type="password" maxlength="8" name="confirm" value="<?php echo $this->informacion->password;?>" id="confirm" class="form-control" placeholder="Confirme su Contraseña">
                                            </div>
                                            
                                        </div>
                                    </div>
                                </fieldset>-->

                                <h1>Confirmación</h1>
                                <fieldset>
                                    <h2>Confirme su registro</h2>
                                    <div class="row">
                                    <div class="col-lg-12">


                                    <label style="font-size: 18px;"> ¿Existe alguna inconsistencia con sus datos? <span style="color: red;">*</span></label>
                                        <div class="i-checks col-sm-10"  >                       
                                        <label class="i-checks checkbox-inline" >Si
                                        <input type="radio"  value="1" id="optionsRadios1" name="datos"  class="form-control required"   onChange="ActivosInactivos(this)"  <?=(!empty($this->informacion->observaciones) ?"checked":"");?> >
                                        </label> 
                                        <label class="i-checks checkbox-inline" >No                                
                                        <input type="radio" value="0" id="optionsRadios2" name="datos"  class="form-control required"   onChange="ActivosInactivos(this)" <?php if(empty($this->informacion->observaciones) && !empty($this->informacion->censo_estatus) ){ echo "checked"; }?>>
                                        </label>
                                        </div>

                                    <div class="form-group" id="obser" <?php if(!empty($this->informacion->observaciones)){ echo 'style="display:"'; }else{ echo 'style="display:none"'; } ?> >
                                                <label>observaciones  <span style="color: red;">*</span></label>
                                                <textarea id="observacion" name="observacion" type="text" placeholder="Ingrese sus observaciones" class="form-control"  aria-="true" aria-invalid="false"  size="40" maxlength="250" ><?php print $this->informacion->observaciones;?></textarea>
                                     </div>

                                     <br><br>
                                     <div class="form-group" style="font-size: 18px;">
                                     Yo  <b><?php print $this->informacion->primer_nombre . " ";?><?php print $this->informacion->primer_apellido;?></b>, C.I. <?php echo $this->informacion->identificacion; ?> . Declaro bajo Fe de Juramento que la informaci&oacute;n suministrada es cierta y autorizo a la UBV a trav&eacute;s de la Direcci&oacute;n General de Talento Humano a comprobar la veracidad de lo aqu&iacute; declarado. En el caso de no ser cierta la informaci&oacute;n suministrada, asumo todas las consecuencias legales y administrativas que diera lugar.
                                    </div>
                                     
                                     </div>
                                 
                                            
                                </fieldset>



                            <script>
                            //Scrip para colocar observacion solo si existe inconsistencia


                            function ActivosInactivos(sel) {
                                if (sel.value=="1"){
                                    
                                    divC = document.getElementById("obser");
                                    divC.style.display="";
                            
                                    //agrega clase required 
                                    myElemento8 = document.getElementById("observacion");
                                    myElemento8.classList.add('required');

                                }else{

                                    divT = document.getElementById("obser");
                                    divT.style.display = "none";
                                    
                                    myElemento7 = document.getElementById("observacion");
                                    myElemento7.classList.remove('required');
                            
                                    var inputNombre = document.getElementById("observacion");
                                    inputNombre.value = ""; //Borra del input la cedula al seleccionar NO





                                }
                            }


                            
                            </script>









                            </form>

           
                        </div>

                        
                    </div>

                    
                    </div>
                    

                </div>
                
                </div>



        <?php require 'views/footer.php'; ?>

        
    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>
    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    
    <!-- Chosen -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>

    <!-- Sweet alert -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/sweetalert/sweetalert.min.js"></script>

   <!-- Input Mask-->
   <script src="<?php echo constant ('URL');?>src/js/plugins/jasny/jasny-bootstrap.min.js"></script>

       <!-- Select2 -->
       <script src="<?php echo constant ('URL');?>src/js/plugins/select2/select2.full.min.js"></script>
    <!-- Data picker -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>

    <!-- Typehead -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>

    
    <!-- iCheck -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/iCheck/icheck.min.js"></script>
    
                
    <!-- form step-->
    <script>

        function mostrar_radio(dato){
            
            //inicio radiobutton
            if(dato == "si"){
                $("#wdiscapacidades").show();
                $("#tdiscapacidad0").addClass('required');
                $("#discapacidad0").addClass('required');
                
            }

            if(dato == "no"){
                $("#wdiscapacidades").hide();
                $("#tdiscapacidad0").removeClass('required');
                $("#discapacidad0").removeClass('required');

            }
            //fin radiobutton
        }

       
        function mostrar2(id){
            
            //inicio select
            if(id == "1"){//sies igual a docente muestro esto 
                $("#wdoc").show();
                $("#tdoc").show();
                $("#programa").addClass('required');
                $("#eje_regional").addClass('required');
                $("#centro_estudio").addClass('required');
                $("#dedicacion").addClass('required');
                $("#escalafon").addClass('required');
                $("#clasificacion").addClass('required');
                $("#estatus_d").addClass('required');

            }else{//sino oculto el div
                $("#wdoc").hide();
                $("#tdoc").hide();
                $("#programa").removeClass('required');
                $("#eje_regional").removeClass('required');
                $("#centro_estudio").removeClass('required');
                $("#dedicacion").removeClass('required');
                $("#escalafon").removeClass('required');
                $("#clasificacion").removeClass('required');
                $("#estatus_d").removeClass('required');

            }
            //fin select


        }

        $(document).ready(function(){

            mostrar2('1');
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                   
                   
                    ////////////////////////////////////////       
                    $('.chosen-select').chosen({width: "100%"});

                    
                    
                    ////////////////////////////////////////   
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);
                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";
                    if (currentIndex === 5){
                        
                        if($("#programa").val().length==0){
                            $("#programa-error").text("El Programa de Formacion es Requerido.").css("color", "#8a1f11");   
                        }else{
                            $("#programa-error").text("");
                            return form.valid();
                        }
                    }else{
                        // Start validation; Prevent going forward if false
                        return form.valid();
                    }
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            },
                            ano_r: {
                                esfecha: true
                            }
                        }
                       
                    });
       });
       
       $.validator.addMethod("esfecha", esFechaActual, "El Año de Realización no debe ser mayor al Año Actual");


        function esFechaActual(value, element, param) {

            
            var fechaActual = <?php echo date('Y');?>;

            if (value > fechaActual) {

                return false; //error de validación

            }

            else {

                return true; //supera la validación

            }

        }

    </script>

   

</body>
</html>
