<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Censo | Docente</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="<?php echo constant ('URL');?>src/img/favicon.png"/> 
    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">


</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-7">
                    <h2>Censo Docente</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>censo_docente">Censo Docente</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Listar Usuarios</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong> Listar</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-5">
                    <div class="title-action">
                    
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Listado de Usuarios </h5>
                       
                        <div class="ibox-tools">
                    
                        </div>
                    </div>
                    
                    <div class="ibox-content">
                    <?php echo $this->mensaje; ?>
                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Cédula</th>
                        <th>Nombres</th>
                        <th>Teléfono</th>
                        <th><i class="fa fa-calendar"></i> Fecha de Ingreso</th>   
                        <!--<th><i class="fa fa-drivers-license-o" aria-hidden="true"></i> Perfil</th>  -->
                        <th><i class="fa fa-user-o" aria-hidden="true"></i> Correo</th>
                        <th>Estatus</th>   
                        <th>Opciones</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php include_once 'models/persona.php';
                            foreach($this->usuarios as $row){
                                $usuario= new Persona();
                                $usuario=$row;?>
                    <tr class="gradeX">
                    <td><?php echo substr($usuario->nacionalidad, 0, 1). "-". $usuario->identificacion; ?></td>
                    <td><?php    list($apellidos,$nombres) = explode(",", $usuario->primer_nombre); echo $nombres." ".$apellidos; ?></td>
                   <td><?php echo preg_replace('([^A-Za-z0-9])', '', $usuario->telefono) ; ?></td>
                    <td>  <?php echo date("d/m/Y", strtotime($usuario->fecha_ingreso)); ?></td>
                    <td><?php echo $usuario->correo; ?></td>
                   
                       <td> <?php if($usuario->censo_estatus==1){
                           echo"<center style=margin-top:3%;><span class='label label-primary' style='font-size: 12px;'>&nbsp;&nbsp;&nbsp;&nbsp; Censado &nbsp;&nbsp;&nbsp;</span></center>";
                         }else{?>
                        <center style=margin-top:3%;><span class='label label-danger' style='font-size: 12px;'>&nbsp; No Censado &nbsp;</span></center>
                         <?php }  ?>
                    </td>                 
                    <td> 
                    <?php if($_SESSION['Consultar']==true && $usuario->censo_estatus==1){ ?>
                    <a href="<?php echo constant('URL') . 'censo_docente/verUsuarioInfo/' .$usuario->id_persona;?>" title='Ver Familiares'><button type="button" class="btn btn-outline btn-info"><i class="fa fa-eye"></i> Familiares</button></a>
                    <?php } 
                    if($_SESSION['Editar']==true){?>
                    &nbsp;
                    <a href="<?php echo constant('URL') . 'censo_docente/verUsuario/' .$usuario->identificacion;?>" title='Realizar Censo'><button type="button" class="btn btn-outline btn-success"><i class="fa fa-edit"></i>&nbsp;&nbsp;&nbsp;Censar&nbsp;&nbsp;&nbsp;</button></a>
                    <?php } ?>
                    
                    </td>
                        </tr>
                    <?php }?>
                        
                        </tbody>
                    
                        </table>

                   
                      </div>

                    </div>
                </div>
            </div>

       


            </div>

        </div>



<!--////////////////////////////////-->
  



  
    <?php require 'views/footer.php'; ?>
   

   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>

    <!-- iCheck -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/iCheck/icheck.min.js"></script>
        



    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>



</body>
</html>
