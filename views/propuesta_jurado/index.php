<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Propuesta de Jurado | SIDTA</title>


    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">
  
    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
<!-- jasny input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <!-- datapicker -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    
    <!--  style select2 -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/select2/select2.min.css" rel="stylesheet">

</head>

<body>
 
    <div id="wrapper">
   <?php require 'views/header.php'; ?>
   

        <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-5">
                    <h2><i class="fa fa-legal"></i> Propuesta de Jurado</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>solicitar_ascenso/viewAdmin">Ascensos</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL') ."solicitar_ascenso/viewDetail/".$this->id_solicitud_ascenso.",".$this->id_docente;?>">Detalle Solicitud</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Propuesta de Jurado</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-7">
                    <div class="title-action">
                        <a href="<?php echo constant ('URL')."solicitar_ascenso/viewDetail/".$this->id_solicitud_ascenso.",".$this->id_docente;?>" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Volver</a>

                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
           
          
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>
                                <i class="fa fa-angle-double-right"></i> SIDTA <i class="fa fa-angle-double-left"></i>
                            </h5>
                        </div>
                        <div class="ibox-content">
                            <h2>
                                <i class="fa fa-legal"></i> Proponer Jurado
                            </h2>
                            <p>
                                Completa el formulario para formalizar la Propuesta.
                            </p>
                            <?php echo $this->mensaje;?>
                            <div class="alert alert-info">Todos los campos marcados con un (<span style="color: red;">*</span>) Son Obligatorios</div>

                            <form class="m-t" role="form" id="form" method="post" action="<?php echo constant('URL') . "solicitar_ascenso/registrarPropuesta/" . $this->id_solicitud_ascenso . "," . $this->id_docente;?>" enctype="multipart/form-data">
                               <!-- <h1>Propuesta de Jurado</h1>
                                <fieldset>
                                    
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>Area de Conocimiento UBV <span style="color: red;">*</span></label>
                                                    <select name="escalafon_next" id="escalafon_next" style="width: 100%;"  class="form-control select2_demo_1 required" tabindex="4">
                                                        <option value="">Seleccione el area de conocimiento ubv.</option>
                                                        <?php 
                                                            foreach($this->areas_ubv as $row){
                                                                $area_ubv=new Estructura();
                                                                $area_ubv=$row;?> 
                                                        <option value="<?php echo $area_ubv->id_area_conocimiento_ubv;?>"><?php echo $area_ubv->descripcion;?></option>
                                                        <?php }?>                                         
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6"> 
                                                <div class="form-group">
                                                    <label>Escalafón <span style="color: red;">*</span></label>
                                                    <select name="escalafon_next" id="escalafon_next" style="width: 100%;"  class="form-control select2_demo_1 required" tabindex="4">
                                                        <option value="">Seleccione el escalafón.</option>
                                                        <?php 
                                                            foreach($this->escalafones as $row){
                                                                $escalafon=new Estructura();
                                                                $escalafon=$row;?> 
                                                        <option value="<?php echo $escalafon->id;?>"><?php echo $escalafon->descripcion;?></option>
                                                        <?php }?>                                         
                                                    </select>
                                                </div>
                                                
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Eje Regional <span style="color: red;">*</span></label>
                                                    <select name="escalafon_next" id="escalafon_next" style="width: 100%;"  class="form-control select2_demo_1 required" tabindex="4">
                                                        <option value="">Seleccione el escalafón.</option>
                                                        <?php 
                                                            foreach($this->ejes_regionales as $row){
                                                                $eje_regional=new Estructura();
                                                                $eje_regional=$row;?> 
                                                        <option value="<?php echo $eje_regional->id_eje_regional;?>"><?php echo $eje_regional->descripcion;?></option>
                                                        <?php }?>                                         
                                                    </select>
                                                </div>
                                            </div>
                                           
                                        </div>
                                        
                                 </fieldset>-->
                                 <script>
                                                 $(".select2_demo_1").select2({
                                                        placeholder: "Seleccione.",
                                                        allowClear: true
                                                    });
                                                 </script>
                                <h1>Jurado Principal <i class="fa fa-legal"></i></h1>
                                 <fieldset> 
                                    <h2><i class="fa fa-users"></i> Jurado Principal</h2>
                                    <p>Selecciona los Docentes Postulados para el Jurado Principal</p>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label style="font-size: 14px;"><i class="fa fa-star"></i><strong> Coordinador(a) del Jurado</strong><span style="color: red;">*</span></label>
                                                <select name="docenteP1" id="docenteP1" style="width: 100%;"  class="form-control select2_demo_1 required" tabindex="4">
                                                    <option value="">Seleccione.</option>
                                                    <?php 
                                                        foreach($this->docentes as $row){
                                                            $docente=new Estructura();
                                                            $docente=$row;?> 
                                                    <option value="<?php echo $docente->id_docente;?>" <?php if(!empty($this->docenteP1) && $docente->id_docente == $this->docenteP1){ echo "selected=selected"; }?>>
                                                        <?php echo $docente->identificacion 
                                                    . " | " . ucwords($docente->primer_nombre)
                                                    . " | " . ucwords($docente->primer_apellido)
                                                    . " | " . ucwords($docente->escalafon);?>
                                                    </option>
                                                    <?php }?>                                         
                                                </select>
                                                <span><small><i>Cédula | Nompre y Apellido | Escalafón</i></small></span>                                            
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label><i class="fa fa-star-o"></i> Jurado Principal N°2 <span style="color: red;">*</span></label>
                                                <select name="docenteP2" id="docenteP2" style="width: 100%;"  class="form-control select2_demo_1 required" tabindex="4">
                                                    <option value="">Seleccione.</option>
                                                    <?php 
                                                        foreach($this->docentes as $row){
                                                            $docente=new Estructura();
                                                            $docente=$row;?> 
                                                    <option value="<?php echo $docente->id_docente;?>" <?php if(!empty($this->docenteP2) && $docente->id_docente == $this->docenteP2){ echo "selected=selected"; }?>>
                                                        <?php echo $docente->identificacion 
                                                    . " | " . ucwords($docente->primer_nombre)
                                                    . " | " . ucwords($docente->primer_apellido)
                                                    . " | " . ucwords($docente->escalafon);?>
                                                    </option>
                                                    <?php }?>                                         
                                                </select>
                                                <span><small><i>Cédula | Nompre y Apellido | Escalafón</i></small></span>                                                                                        
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label><i class="fa fa-star-o"></i> Jurado Principal N°3 <span style="color: red;">*</span></label>
                                                <select name="docenteP3" id="docenteP3" style="width: 100%;"  class="form-control select2_demo_1 required" tabindex="4">
                                                    <option value="">Seleccione.</option>
                                                    <?php 
                                                        foreach($this->docentes as $row){
                                                            $docente=new Estructura();
                                                            $docente=$row;?> 
                                                    <option value="<?php echo $docente->id_docente;?>" <?php if(!empty($this->docenteP3) && $docente->id_docente == $this->docenteP3){ echo "selected=selected"; }?>>
                                                        <?php echo $docente->identificacion 
                                                    . " | " . ucwords($docente->primer_nombre)
                                                    . " | " . ucwords($docente->primer_apellido)
                                                    . " | " . ucwords($docente->escalafon);?>
                                                    </option>
                                                    <?php }?>                                         
                                                </select>    
                                                <span><small><i>Cédula | Nompre y Apellido | Escalafón</i></small></span>                                                                                    
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <h1>Jurado Suplente <i class="fa fa-legal"></i></h1>
                                 <fieldset> 
                                    <h2>Jurado Suplente</h2>
                                    <p>Selecciona los Docentes Postulados para Suplantar al Jurado Principal</p>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label><i class="fa fa-star-o"></i> Suplente N°1 <span style="color: red;">*</span></label>
                                                <select name="docenteS1" id="docenteS1" style="width: 100%;"  class="form-control select2_demo_1 required" tabindex="4">
                                                    <option value="">Seleccione.</option>
                                                    <?php 
                                                        foreach($this->docentes as $row){
                                                            $docente=new Estructura();
                                                            $docente=$row;?> 
                                                    <option value="<?php echo $docente->id_docente;?>" <?php if(!empty($this->docenteS1) && $docente->id_docente == $this->docenteS1){ echo "selected=selected"; }?>>
                                                        <?php echo $docente->identificacion 
                                                    . " | " . ucwords($docente->primer_nombre)
                                                    . " | " . ucwords($docente->primer_apellido)
                                                    . " | " . ucwords($docente->escalafon);?>
                                                    </option>
                                                    <?php }?>                                         
                                                </select>    
                                                <span><small><i>Cédula | Nompre y Apellido | Escalafón</i></small></span>                                                                                    
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label><i class="fa fa-star-o"></i> Suplente N°2 <span style="color: red;">*</span></label>
                                                <select name="docenteS2" id="docenteS2" style="width: 100%;"  class="form-control select2_demo_1 required" tabindex="4">
                                                    <option value="">Seleccione.</option>
                                                    <?php 
                                                        foreach($this->docentes as $row){
                                                            $docente=new Estructura();
                                                            $docente=$row;?> 
                                                    <option value="<?php echo $docente->id_docente;?>" <?php if(!empty($this->docenteS2) && $docente->id_docente == $this->docenteS2){ echo "selected=selected"; }?>>
                                                        <?php echo $docente->identificacion 
                                                    . " | " . ucwords($docente->primer_nombre)
                                                    . " | " . ucwords($docente->primer_apellido)
                                                    . " | " . ucwords($docente->escalafon);?>
                                                    </option>
                                                    <?php }?>                                         
                                                </select> 
                                                <span><small><i>Cédula | Nompre y Apellido | Escalafón</i></small></span>                                                                                       
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label><i class="fa fa-star-o"></i> Suplente N°3 <span style="color: red;">*</span></label>
                                                <select name="docenteS3" id="docenteS3" style="width: 100%;"  class="form-control select2_demo_1 required" tabindex="4">
                                                    <option value="">Seleccione.</option>
                                                    <?php 
                                                        foreach($this->docentes as $row){
                                                            $docente=new Estructura();
                                                            $docente=$row;?> 
                                                    <option value="<?php echo $docente->id_docente;?>" <?php if(!empty($this->docenteS3) && $docente->id_docente == $this->docenteS3){ echo "selected=selected"; }?>>
                                                        <?php echo $docente->identificacion 
                                                    . " | " . ucwords($docente->primer_nombre)
                                                    . " | " . ucwords($docente->primer_apellido)
                                                    . " | " . ucwords($docente->escalafon);?>
                                                    </option>
                                                    <?php }?>                                         
                                                </select> 
                                                <span><small><i>Cédula | Nompre y Apellido | Escalafón</i></small></span>                                                                                       
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>

           
                        </div>

                        
                    </div>

                    
                    </div>
                    

                </div>
                
                </div>







            







        <?php require 'views/footer.php'; ?>

        
    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>
    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>

    <!-- Chosen Select -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script> 
   <!-- Input Mask-->
   <script src="<?php echo constant ('URL');?>src/js/plugins/jasny/jasny-bootstrap.min.js"></script>

       <!-- Data picker -->
       <script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>
       <!-- Select2 -->
       <script src="<?php echo constant ('URL');?>src/js/plugins/select2/select2.full.min.js"></script>


    
       <script>
            function mostrar2(id_radio){

                console.log(id_radio);
                switch(id_radio){
                    case "si":
                    $("#wno").hide();
                    $("#escalafon_next").removeClass('required');

                    break;
                    case "no":
                    $("#wno").show();
                    $("#escalafon_next").addClass('required');
                    break;

                }
            }


        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                   
                  

                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#passwd"
                            }
                        }
                    });
       });
    </script>

   

</body>
</html>


                       
                        


