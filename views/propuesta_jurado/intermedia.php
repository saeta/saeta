<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Cuadro de Jurados | SIDTA</title>


 
    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">


</head>
<style>
table{
    color: #000;
    cursor: default;
}
tr:hover{
    background-color: #61646182;
}
td:hover{
    background-color: #1ab394;
}
</style>
<body>
 
   <?php require 'views/header.php'; ?>
   

   <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-5">
                    <h2>Cuadro de Jurados</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Ascensos</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Solicitud de Ascenso</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Cuadro de Jurados</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-7">
                    <div class="title-action">


        <form  target="_blank" name="form" action="<?php echo constant('URL');?>views/propuesta_jurado/print_pdf.php" method="post">
            <input type="hidden" name="ascenso_solicitado" value="<?php echo $this->solicitud_escsig->escalafon_siguiente;?>">
            <input type="hidden" name="eje_regional" value="<?php echo $this->solicitud_escsig->eje_regional;?>">
            <input type="hidden" name="fecha_ingreso" value="<?php echo $this->solicitud_escsig->fecha_ingreso;?>">
            <input type="hidden" name="fecha_concurso" value="<?php echo $this->solicitud_escsig->fecha_concurso;?>">
            <input type="hidden" name="fecha_consulta" value="<?php echo $this->solicitud_escsig->fecha_consulta;?>">
            <input type="hidden" name="identificacion" value="<?php echo $this->solicitud_escsig->identificacion;?>">
            <input type="hidden" name="primer_nombre" value="<?php echo $this->solicitud_escsig->primer_nombre;?>">
            <input type="hidden" name="segundo_nombre" value="<?php echo $this->solicitud_escsig->segundo_nombre;?>">
            <input type="hidden" name="primer_apellido" value="<?php echo $this->solicitud_escsig->primer_apellido;?>">
            <input type="hidden" name="segundo_apellido" value="<?php echo $this->solicitud_escsig->segundo_apellido;?>">
            <input type="hidden" name="titulo" value="<?php echo $this->solicitud_escsig->titulo;?>">
            <input type="hidden" name="primer_apellido" value="<?php echo $this->solicitud_escsig->primer_apellido;?>">
            <input type="hidden" name="centro_estudio" value="<?php echo $this->solicitud_escsig->centro_estudio;?>">
            
            <input type="hidden" name="escalafon_actual" value="<?php echo $this->escalafon_actual->escalafon_actual;?>">
            
            <?php 
                $cont=1;
                foreach($this->jurado_docentep as $row){
                    $juradop= new Estructura();
                    $juradop=$row;
            ?>
                <input type="hidden" name="identificacionj<?php echo $cont;?>" value="<?php echo $juradop->identificacion;?>">
                <input type="hidden" name="primer_nombrej<?php echo $cont;?>" value="<?php echo $juradop->primer_nombre;?>">
                <input type="hidden" name="segundo_nombrej<?php echo $cont;?>" value="<?php echo $juradop->segundo_nombre;?>">
                <input type="hidden" name="primer_apellidoj<?php echo $cont;?>" value="<?php echo $juradop->primer_apellido;?>">
                <input type="hidden" name="segundo_apellidoj<?php echo $cont;?>" value="<?php echo $juradop->segundo_apellido;?>">
                <input type="hidden" name="escalafonj<?php echo $cont;?>" value="<?php echo $juradop->escalafon_jurado;?>">
            <?php 
                    $cont++;
                }
            ?>
            <input type="hidden" name="cont" value="<?php echo $cont?>">


            <?php 
                $contS=1;
                foreach($this->jurado_docenteS as $row){
                    $juradoS= new Estructura();
                    $juradoS=$row;
            ?>
                <input type="hidden" name="identificacionjs<?php echo $contS;?>" value="<?php echo $juradoS->identificacion;?>">
                <input type="hidden" name="primer_nombrejs<?php echo $contS;?>" value="<?php echo $juradoS->primer_nombre;?>">
                <input type="hidden" name="segundo_nombrejs<?php echo $contS;?>" value="<?php echo $juradoS->segundo_nombre;?>">
                <input type="hidden" name="primer_apellidojs<?php echo $contS;?>" value="<?php echo $juradoS->primer_apellido;?>">
                <input type="hidden" name="segundo_apellidojs<?php echo $contS;?>" value="<?php echo $juradoS->segundo_apellido;?>">
                <input type="hidden" name="escalafonjs<?php echo $contS;?>" value="<?php echo $juradoS->escalafon_jurado;?>">
            <?php 
                    $contS++;
                }
            ?>
            <input type="hidden" name="contS" value="<?php echo $contS?>">


            <?php 
                $contC=1;
                foreach($this->jurado_docenteC as $row){
                    $juradoC= new Estructura();
                    $juradoC=$row;
            ?>
                <input type="hidden" name="identificacionjc<?php echo $contC;?>" value="<?php echo $juradoC->identificacion;?>">
                <input type="hidden" name="primer_nombrejc<?php echo $contC;?>" value="<?php echo $juradoC->primer_nombre;?>">
                <input type="hidden" name="segundo_nombrejc<?php echo $contC;?>" value="<?php echo $juradoC->segundo_nombre;?>">
                <input type="hidden" name="primer_apellidojc<?php echo $contC;?>" value="<?php echo $juradoC->primer_apellido;?>">
                <input type="hidden" name="segundo_apellidojc<?php echo $contC;?>" value="<?php echo $juradoC->segundo_apellido;?>">
                <input type="hidden" name="escalafonjc<?php echo $contC;?>" value="<?php echo $juradoC->escalafon_jurado;?>">
                <input type="hidden" name="tipojc<?php echo $contC;?>" value="<?php echo $juradoC->tipo_jurado;?>">

            <?php 
                    $contC++;
                }
            ?>
            <input type="hidden" name="contC" value="<?php echo $contC?>">


        
            <input class="btn btn-success" type="submit" name="pdf" value="Generar PDF"/>
            &nbsp;
            <a href="<?php echo constant('URL')."solicitar_ascenso/viewDetail/".$this->id_solicitud_ascenso.",".$this->id_docente;?>" class="btn btn-primary">
                <i class="fa fa-arrow-left"></i> Volver
            </a>
        
            
            
        </form>
                        
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>SIDTA</h5>
                        <div class="ibox-tools">

                 

                        </div>
                    </div>
                    <div class="ibox-content">

                        <!--  contenido -->
                        
                        <div class="row">
                        <div class="col-lg-12">
                            <h1>Jurado</h1>
                            <p>Ahora el Director del CE deberá Verificar la Propuesta del Jurado</p>
                        <?php echo $this->mensaje; ?>
                        </div>

        <?php switch($_SESSION['id_perfil']){
        case 3:
       ?>

<?php if($this->estatus_propuesta=='Verificado'){ ?>
                
                <div class="col-lg-12">
                <div class="alert alert-warning">
                    <i class="fa fa-exclamation-triangle"></i> Propuesta Aprobada Exitosamente.
                </div>
                </div>
                
            <?php }else{ ?>
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-list"></i> Lista de Verificación
                </div>
                <div class="panel-body tooltip-demo">
                    <form id="form1" action="<?php echo constant('URL')."solicitar_ascenso/updatePropuesta/".$this->id_solicitud_ascenso; ?>" method="post">
                        
                        <div class="checkbox checkbox-primary">
                            <input id="checkbox3" class="required" name="propuesta" type="checkbox">
                            <label for="checkbox3">
                            Propuesta de Jurado
                            </label>
                            
                        </div>
                        <label id="propuesta-error" class="error" for="propuesta"></label>
                        <button type="submit" class="btn btn-block btn-primary"><i class="fa fa-check"></i> Aprobar Propuesta</button>&nbsp;
                    </form>
                    <a href="" class="btn btn-outline btn-block btn-danger" data-toggle="modal" data-target="#btnD"><i class="fa fa-times"></i> Declinar Propuesta</a>

                </div>
            </div>
                                                    <div class="modal inmodal" id="btnD" tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content animated bounceInRight panel panel-warning">
                                                            

                                                                <div class="modal-header panel-heading">
                                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                                                    <h4 class="modal-title"><i class="fa fa-exclamation-triangle"></i> Declinar Propuesta</h4>
                                                                    <small class="font-bold">Completa el Formulario para declinar la Propuesta.</small>
                                                                </div>
                                                                <div class="modal-body text-center">
                                                                <form id="form-D" action="<?php echo constant ('URL') . "solicitar_ascenso/declinar/".$this->id_solicitud_ascenso.",".$this->id_docente;?>" method="post">
                                                                    <div style="margin: 16px;" class="row jumbotron">
                                                                        <div class="col-sm-12">
                                                                            <div class="form-group ">
                                                                                <label>¿Por qué decide Declinar la Propuesta?<span style="color: red;">*</span></label>
                                                                                <div class="form-group">
                                                                                    <textarea name="motivo" id="motivo" maxlength="250" cols="30" rows="5" placeholder="Ingrese el Motivo por el Cuál se Declina la Propuesta." class="form-control required"></textarea>
                                                                                </div>
                                                                                <!-- tipo de declinacion de propuesta de jurado-->
                                                                                <input type="hidden" name="tipo_declinar" value="2">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                                                    <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i> Declinar</button>
                                                                    </form>
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
        </div>
        <div class="col-lg-2"></div>
    <?php }
     break;
    default:
    ?>
    
            <?php if($this->estatus_propuesta=='Verificado'){ ?>
                
                <div class="col-lg-12">
                <div class="alert alert-success">
                    Propuesta Aprobada.
                </div>
                </div>
                
            <?php }elseif($this->estatus_propuesta=='En Espera'){?>
                
                <div class="col-lg-12">
                <div class="alert alert-warning">
                    <i class="fa fa-info"></i> Su Propuesta Aún no ha sido aprobada por el Director del Centro de Estudio
                </div>
                </div>
                
                <?php } 
            break;
        } 
            
            ?>
    
    <div class="col-lg-12">
        
        <br>
        <table class="table table-bordered" >
            <tr class="text-center" style="font-weight: bold; border: 1px solid black;">
                <th colspan="3" style="border: 1px solid black;">Solicitud de Jurado Principal y Suplente para Trabajo de Ascenso</th>
            </tr>
            <tr >
                <td style="font-weight: bold; border: 1px solid black;">Ascenso solicitado</td>
                <td class="text-center" colspan="2" style="border: 1px solid black;"><?php echo ucwords($this->solicitud_escsig->escalafon_siguiente);?></td>
            </tr>
            <tr>
                <td style="font-weight: bold; border: 1px solid black;">Eje Geopolítico Regional</td>
                <td class="text-center" colspan="2" style="border: 1px solid black;"><?php echo ucwords($this->solicitud_escsig->eje_regional);?></td>
            </tr>
            <tr>
                <td style="font-weight: bold; border: 1px solid black;">Fecha de primera contratación UBV</td>
                <td class="text-center" colspan="2" style="border: 1px solid black;"><?php echo ucwords($this->solicitud_escsig->fecha_ingreso);?></td>
            </tr>
            <tr>
                <td style="font-weight: bold; border: 1px solid black;">Fecha  Concurso de Oposición y categoría académica  aprobada
            </td>
                <td class="text-center" colspan="2" style="border: 1px solid black;"><?php echo ucwords($this->solicitud_escsig->fecha_concurso);?></td>
            </tr>
            <tr>
                <td style="font-weight: bold; border: 1px solid black;">Fecha y categoría académica del  último ascenso</td>
                <td class="text-center" colspan="2" style="border: 1px solid black;">NONE</td>
            </tr>
            <tr>
                <td style="font-weight: bold; border: 1px solid black;">Fecha consignación de documentos y trabajo de ascenso</td>
                <td class="text-center" colspan="2" style="border: 1px solid black;"><?php echo ucwords($this->solicitud_escsig->fecha_consulta);?></td>
            </tr>
            <tr class="text-center">
                <td colspan="3" style="font-weight: bold; border: 1px solid black;">Información Personal del Solicitante</td>
            </tr>
            <tr>
                <td style="font-weight: bold; border: 1px solid black;">Cédula de Identidad</td>
                <td class="text-center" colspan="2" style="border: 1px solid black;"><?php echo ucwords($this->solicitud_escsig->identificacion);?></td>
            </tr>
            <tr>
                <td style="font-weight: bold; border: 1px solid black;">Nombres</td>
                <td class="text-center" colspan="2" style="border: 1px solid black;"><?php echo ucwords($this->solicitud_escsig->primer_nombre) . " " . ucwords($this->solicitud_escsig->segundo_nombre);?></td>
            </tr>
            <tr>
                <td style="font-weight: bold; border: 1px solid black;">Apellidos</td>
                <td class="text-center" colspan="2" style="border: 1px solid black;"><?php echo ucwords($this->solicitud_escsig->primer_apellido) . " " . ucwords($this->solicitud_escsig->segundo_apellido);?></td>
            </tr>
            <tr>
                <td style="font-weight: bold; border: 1px solid black;">Categoría Académica</td>
                <td class="text-center" colspan="2" style="border: 1px solid black;"><?php echo ucwords($this->escalafon_actual->escalafon_actual);?></td>
            </tr>
            <tr style="text-align: center; ">
                <td colspan="3" style="font-weight: bold; border: 1px solid black;">Información del Trabajo de Ascenso</td>
            </tr>
            <tr>
                <td style="font-weight: bold; border: 1px solid black;">Propuesta Pedagógica Titulada:</td>
                <td class="text-center" colspan="2" style="border: 1px solid black;"><?php echo ucwords($this->solicitud_escsig->titulo);?></td>
            </tr>
            <tr class="text-center">
                <td colspan="3" style="font-weight: bold; border: 1px solid black;">Jurado Principal</td>
            </tr>
            <tr class="text-center">
                <td style="font-weight: bold; border: 1px solid black;">Cédula de Identidad</td>
                <td style="font-weight: bold; border: 1px solid black;">Nombres</td>
                <td style="font-weight: bold;border: 1px solid black;">Categoría Académica</td>
            </tr>
            <?php 
                $contC=1;
                foreach($this->jurado_docenteC as $row){
                    $juradoC= new Estructura();
                    $juradoC=$row;
            ?>
            <tr class="text-center">
                <td style="border: 1px solid black;">(<?php echo $juradoC->tipo_jurado;?> del Jurado) <?php echo $juradoC->identificacion;?></td>
                <td style="border: 1px solid black;"><?php echo $juradoC->primer_nombre ." ".$juradoC->segundo_nombre .", ".$juradoC->primer_apellido." ". $juradoC->segundo_apellido;?></td>
                <td style="border: 1px solid black;"><?php echo $juradoC->escalafon_jurado;?></td>
            </tr>
            <?php 
                    $contC++;
                }
            ?>
            <?php 
                $cont=1;
                foreach($this->jurado_docentep as $row){
                    $juradop= new Estructura();
                    $juradop=$row;
            ?>
            <tr class="text-center">
                <td style="border: 1px solid black;"><?php echo $juradop->identificacion;?></td>
                <td style="border: 1px solid black;"><?php echo $juradop->primer_nombre ." ".$juradop->segundo_nombre .", ".$juradop->primer_apellido." ". $juradop->segundo_apellido;?></td>
                <td style="border: 1px solid black;"><?php echo $juradop->escalafon_jurado;?></td>
            </tr>
            <?php 
                    $cont++;
                }
            ?>
            <tr style="text-align: center; ">
                <td colspan="3" style="font-weight: bold; border: 1px solid black;">Jurado Suplente</td>
            </tr>
            <tr class="text-center">
                <td style="font-weight: bold; border: 1px solid black;">Cédula de Identidad</td>
                <td style="font-weight: bold; border: 1px solid black;">Nombres</td>
                <td style="font-weight: bold;border: 1px solid black;">Categoría Académica</td>
            </tr>
            <?php 
                $contS=1;
                foreach($this->jurado_docenteS as $row){
                    $juradoS= new Estructura();
                    $juradoS=$row;
            ?>
            <tr class="text-center">
                <td style="border: 1px solid black;"><?php echo $juradoS->identificacion;?></td>
                <td style="border: 1px solid black;"><?php echo $juradoS->primer_nombre ." ".$juradoS->segundo_nombre .", ".$juradoS->primer_apellido." ". $juradoS->segundo_apellido;?></td>
                <td style="border: 1px solid black;"><?php echo $juradoS->escalafon_jurado;?></td>
            </tr>
            <?php 
                    $contS++;
                }
            ?>
        </table>
    </div>
        

    
</div>
                        <!--  end contenido -->

                    </div>
                </div>
            </div>
            </div>
        </div>



    <?php require 'views/footer.php'; ?>

     

    <!-- dataTables Scripts -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

 <!-- Jquery Validate -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>
 <script>
$(document).ready(function(){
    $("#form1").validate();

});
</script>
    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>

   

</body>
</html>










<!-- 
MODIFICADO EL 20-03-2020: Modificando la vista de cuadro de jurados
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SIDTA | Cuadro de Jurados</title>


 
    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">


</head>

<body class="ibox-content">
 
   <!--  contenido 
   <div class="row">
       <div class="col-lg-2"></div>
       <div class="col-lg-8"><h1>Jurado</h1>
        
        <p>Ahora el Director del CE deberá Verificar la Propuesta del Jurado</p></div>
        <div class="col-lg-2"></div>

   </div>

<div class="row">
    

        <?php switch($_SESSION['id_perfil']){
        case 3:
       ?>

<?php if($this->estatus_propuesta=='Verificado'){ ?>
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                <div class="alert alert-success">
                    Propuesta Aprobada.
                </div>
                </div>
                <div class="col-lg-2"></div>
            <?php }else{ ?>
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-list"></i> Lista de Verificación
                </div>
                <div class="panel-body tooltip-demo">
                    <form id="form1" action="<?php echo constant('URL')."solicitar_ascenso/updatePropuesta/".$this->id_solicitud_ascenso; ?>" method="post">
                        
                        <div class="checkbox checkbox-primary">
                            <input id="checkbox3" class="required" name="propuesta" type="checkbox">
                            <label for="checkbox3">
                            Propuesta de Jurado
                            </label>
                            
                        </div>
                        <label id="propuesta-error" class="error" for="propuesta"></label>
                        <button type="submit" class="btn btn-block btn-primary"><i class="fa fa-check"></i> Verificar Propuesta</button>
                    </form>
                </div>
            </div>
            
        </div>
        <div class="col-lg-2"></div>
    <?php }
     break;
    default:
    ?>
    
            <?php if($this->estatus_propuesta=='Verificado'){ ?>
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                <div class="alert alert-success">
                    Propuesta Aprobada.
                </div>
                </div>
                <div class="col-lg-2"></div>
            <?php }elseif($this->estatus_propuesta=='En Espera'){?>
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                <div class="alert alert-warning">
                    <i class="fa fa-info"></i> Su Propuesta Aún no ha sido aprobada por el Director del Centro de Estudio
                </div>
                </div>
                <div class="col-lg-2"></div>
                <?php } 
            break;} 
            
            ?>
    <div class="col-lg-2"></div>
    <div class="col-lg-8">
        
        

        <form  target="_blank" name="form" action="<?php echo constant('URL');?>views/propuesta_jurado/print_pdf.php" method="post">
            <input type="hidden" name="ascenso_solicitado" value="<?php echo $this->solicitud_escsig->escalafon_siguiente;?>">
            <input type="hidden" name="eje_regional" value="<?php echo $this->solicitud_escsig->eje_regional;?>">
            <input type="hidden" name="fecha_ingreso" value="<?php echo $this->solicitud_escsig->fecha_ingreso;?>">
            <input type="hidden" name="fecha_concurso" value="<?php echo $this->solicitud_escsig->fecha_concurso;?>">
            <input type="hidden" name="fecha_consulta" value="<?php echo $this->solicitud_escsig->fecha_consulta;?>">
            <input type="hidden" name="identificacion" value="<?php echo $this->solicitud_escsig->identificacion;?>">
            <input type="hidden" name="primer_nombre" value="<?php echo $this->solicitud_escsig->primer_nombre;?>">
            <input type="hidden" name="segundo_nombre" value="<?php echo $this->solicitud_escsig->segundo_nombre;?>">
            <input type="hidden" name="primer_apellido" value="<?php echo $this->solicitud_escsig->primer_apellido;?>">
            <input type="hidden" name="segundo_apellido" value="<?php echo $this->solicitud_escsig->segundo_apellido;?>">
            <input type="hidden" name="titulo" value="<?php echo $this->solicitud_escsig->titulo;?>">
            <input type="hidden" name="primer_apellido" value="<?php echo $this->solicitud_escsig->primer_apellido;?>">
            <input type="hidden" name="centro_estudio" value="<?php echo $this->solicitud_escsig->centro_estudio;?>">
            
            <input type="hidden" name="escalafon_actual" value="<?php echo $this->escalafon_actual->escalafon_actual;?>">
            
            <?php 
                $cont=1;
                foreach($this->jurado_docentep as $row){
                    $juradop= new Estructura();
                    $juradop=$row;
            ?>
                <input type="hidden" name="identificacionj<?php echo $cont;?>" value="<?php echo $juradop->identificacion;?>">
                <input type="hidden" name="primer_nombrej<?php echo $cont;?>" value="<?php echo $juradop->primer_nombre;?>">
                <input type="hidden" name="segundo_nombrej<?php echo $cont;?>" value="<?php echo $juradop->segundo_nombre;?>">
                <input type="hidden" name="primer_apellidoj<?php echo $cont;?>" value="<?php echo $juradop->primer_apellido;?>">
                <input type="hidden" name="segundo_apellidoj<?php echo $cont;?>" value="<?php echo $juradop->segundo_apellido;?>">
                <input type="hidden" name="escalafonj<?php echo $cont;?>" value="<?php echo $juradop->escalafon_jurado;?>">
            <?php 
                    $cont++;
                }
            ?>
            <input type="hidden" name="cont" value="<?php echo $cont?>">


            <?php 
                $contS=1;
                foreach($this->jurado_docenteS as $row){
                    $juradoS= new Estructura();
                    $juradoS=$row;
            ?>
                <input type="hidden" name="identificacionjs<?php echo $contS;?>" value="<?php echo $juradoS->identificacion;?>">
                <input type="hidden" name="primer_nombrejs<?php echo $contS;?>" value="<?php echo $juradoS->primer_nombre;?>">
                <input type="hidden" name="segundo_nombrejs<?php echo $contS;?>" value="<?php echo $juradoS->segundo_nombre;?>">
                <input type="hidden" name="primer_apellidojs<?php echo $contS;?>" value="<?php echo $juradoS->primer_apellido;?>">
                <input type="hidden" name="segundo_apellidojs<?php echo $contS;?>" value="<?php echo $juradoS->segundo_apellido;?>">
                <input type="hidden" name="escalafonjs<?php echo $contS;?>" value="<?php echo $juradoS->escalafon_jurado;?>">
            <?php 
                    $contS++;
                }
            ?>
            <input type="hidden" name="contS" value="<?php echo $contS?>">


            <?php 
                $contC=1;
                foreach($this->jurado_docenteC as $row){
                    $juradoC= new Estructura();
                    $juradoC=$row;
            ?>
                <input type="hidden" name="identificacionjc<?php echo $contC;?>" value="<?php echo $juradoC->identificacion;?>">
                <input type="hidden" name="primer_nombrejc<?php echo $contC;?>" value="<?php echo $juradoC->primer_nombre;?>">
                <input type="hidden" name="segundo_nombrejc<?php echo $contC;?>" value="<?php echo $juradoC->segundo_nombre;?>">
                <input type="hidden" name="primer_apellidojc<?php echo $contC;?>" value="<?php echo $juradoC->primer_apellido;?>">
                <input type="hidden" name="segundo_apellidojc<?php echo $contC;?>" value="<?php echo $juradoC->segundo_apellido;?>">
                <input type="hidden" name="escalafonjc<?php echo $contC;?>" value="<?php echo $juradoC->escalafon_jurado;?>">
                <input type="hidden" name="tipojc<?php echo $contC;?>" value="<?php echo $juradoC->tipo_jurado;?>">

            <?php 
                    $contC++;
                }
            ?>
            <input type="hidden" name="contC" value="<?php echo $contC?>">


        
            <input class="btn btn-success" type="submit" name="pdf" value="Generar PDF"/>
            <a href="<?php echo constant('URL')."solicitar_ascenso/viewDetail/".$this->id_solicitud_ascenso.",".$this->id_docente;?>" class="btn btn-default"><i class="fa fa-arrow-left"></i> Volver</a>
        
            
            
        </form><br>
        <table style="margin-left: 12mm;border-collapse: collapse; border: 1px solid black;">
            <tr style="text-align: center;  font-weight: bold; border: 1px solid black;">
                <th colspan="3" style="border: 1px solid black;">Solicitud de Jurado Principal y Suplente para Trabajo de Ascenso</th>
            </tr>
            <tr >
                <td style="font-weight: bold; border: 1px solid black;">Ascenso solicitado</td>
                <td colspan="2" style="border: 1px solid black;"><?php echo ucwords($this->solicitud_escsig->escalafon_siguiente);?></td>
            </tr>
            <tr>
                <td style="font-weight: bold; border: 1px solid black;">Eje Geopolítico Regional</td>
                <td colspan="2" style="border: 1px solid black;"><?php echo ucwords($this->solicitud_escsig->eje_regional);?></td>
            </tr>
            <tr>
                <td style="font-weight: bold; border: 1px solid black;">Fecha de primera contratación UBV</td>
                <td colspan="2" style="border: 1px solid black;"><?php echo ucwords($this->solicitud_escsig->fecha_ingreso);?></td>
            </tr>
            <tr>
                <td style="font-weight: bold; border: 1px solid black;">Fecha  Concurso de Oposición y categoría académica  aprobada
            </td>
                <td colspan="2" style="border: 1px solid black;"><?php echo ucwords($this->solicitud_escsig->fecha_concurso);?></td>
            </tr>
            <tr>
                <td style="font-weight: bold; border: 1px solid black;">Fecha y categoría académica del  último ascenso</td>
                <td colspan="2" style="border: 1px solid black;">NONE</td>
            </tr>
            <tr>
                <td style="font-weight: bold; border: 1px solid black;">Fecha consignación de documentos y trabajo de ascenso</td>
                <td colspan="2" style="border: 1px solid black;"><?php echo ucwords($this->solicitud_escsig->fecha_consulta);?></td>
            </tr>
            <tr style="text-align: center; ">
                <td colspan="3" style="font-weight: bold; border: 1px solid black;">Información Personal del Solicitante</td>
            </tr>
            <tr>
                <td style="font-weight: bold; border: 1px solid black;">Cédula de Identidad</td>
                <td colspan="2" style="border: 1px solid black;"><?php echo ucwords($this->solicitud_escsig->identificacion);?></td>
            </tr>
            <tr>
                <td style="font-weight: bold; border: 1px solid black;">Nombres</td>
                <td colspan="2" style="border: 1px solid black;"><?php echo ucwords($this->solicitud_escsig->primer_nombre) . " " . ucwords($this->solicitud_escsig->segundo_nombre);?></td>
            </tr>
            <tr>
                <td style="font-weight: bold; border: 1px solid black;">Apellidos</td>
                <td colspan="2" style="border: 1px solid black;"><?php echo ucwords($this->solicitud_escsig->primer_apellido) . " " . ucwords($this->solicitud_escsig->segundo_apellido);?></td>
            </tr>
            <tr>
                <td style="font-weight: bold; border: 1px solid black;">Categoría Académica</td>
                <td colspan="2" style="border: 1px solid black;"><?php echo ucwords($this->escalafon_actual->escalafon_actual);?></td>
            </tr>
            <tr style="text-align: center; ">
                <td colspan="3" style="font-weight: bold; border: 1px solid black;">Información del Trabajo de Ascenso</td>
            </tr>
            <tr>
                <td style="font-weight: bold; border: 1px solid black;">Propuesta Pedagógica Titulada:</td>
                <td colspan="2" style="border: 1px solid black;"><?php echo ucwords($this->solicitud_escsig->titulo);?></td>
            </tr>
            <tr style="text-align: center; ">
                <td colspan="3" style="font-weight: bold; border: 1px solid black;">Jurado Principal</td>
            </tr>
            <tr>
                <td style="font-weight: bold; border: 1px solid black;">Nombres</td>
                <td style="font-weight: bold; border: 1px solid black;">Cédula de Identidad</td>
                <td style="font-weight: bold;border: 1px solid black;">Categoría Académica</td>
            </tr>
            <?php 
                $contC=1;
                foreach($this->jurado_docenteC as $row){
                    $juradoC= new Estructura();
                    $juradoC=$row;
            ?>
            <tr>
                <td style="border: 1px solid black;">(<?php echo $juradoC->tipo_jurado;?> del Jurado) <?php echo $juradoC->identificacion;?></td>
                <td style="border: 1px solid black;"><?php echo $juradoC->primer_nombre ." ".$juradoC->segundo_nombre .", ".$juradoC->primer_apellido." ". $juradoC->segundo_apellido;?></td>
                <td style="border: 1px solid black;"><?php echo $juradoC->escalafon_jurado;?></td>
            </tr>
            <?php 
                    $contC++;
                }
            ?>
            <?php 
                $cont=1;
                foreach($this->jurado_docentep as $row){
                    $juradop= new Estructura();
                    $juradop=$row;
            ?>
            <tr>
                <td style="border: 1px solid black;"><?php echo $juradop->identificacion;?></td>
                <td style="border: 1px solid black;"><?php echo $juradop->primer_nombre ." ".$juradop->segundo_nombre .", ".$juradop->primer_apellido." ". $juradop->segundo_apellido;?></td>
                <td style="border: 1px solid black;"><?php echo $juradop->escalafon_jurado;?></td>
            </tr>
            <?php 
                    $cont++;
                }
            ?>
            <tr style="text-align: center; ">
                <td colspan="3" style="font-weight: bold; border: 1px solid black;">Jurado Suplente</td>
            </tr>
            <tr>
                <td style="font-weight: bold; border: 1px solid black;">Nombres</td>
                <td style="font-weight: bold; border: 1px solid black;">Cédula de Identidad</td>
                <td style="font-weight: bold;border: 1px solid black;">Categoría Académica</td>
            </tr>
            <?php 
                $contS=1;
                foreach($this->jurado_docenteS as $row){
                    $juradoS= new Estructura();
                    $juradoS=$row;
            ?>
            <tr>
                <td style="border: 1px solid black;"><?php echo $juradoS->identificacion;?></td>
                <td style="border: 1px solid black;"><?php echo $juradoS->primer_nombre ." ".$juradoS->segundo_nombre .", ".$juradoS->primer_apellido." ". $juradoS->segundo_apellido;?></td>
                <td style="border: 1px solid black;"><?php echo $juradoS->escalafon_jurado;?></td>
            </tr>
            <?php 
                    $contS++;
                }
            ?>
        </table>
    </div>
        <div class="col-lg-2"></div>

    
</div>
<div style="margin-top: 4em;" class="row">
    <div class="col-lg-12">
        <div class="footer" style=" background-color: #1c84c6;" >
                <div class="float-right">
                    Independencia y <strong>Patria Socialista</strong> Viviremos y Venceremos.
                </div>
                <div>
                    <strong>UBV</strong> Univeridad Bolivariana de Venezuela 2020
                </div>
            </div>

    </div>
</div>
    
<script>

window.onload=function(){

            // Una vez cargada la página, el formulario se enviara automáticamente.

    //document.forms["form"].submit();

}

</script>

    
    
                    <!--  end contenido 

                        
                    <script src="<?php echo constant ('URL');?>src/js/jquery-3.1.1.min.js"></script>

 <!-- Jquery Validate 
 <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>
 <script>
$(document).ready(function(){
    $("#form1").validate();

});
</script>

</body>
</html>

-->