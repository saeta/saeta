<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Solicitud</title>
</head>
<body>
    <table style="margin-left: 40px;">
        <tr>
            <td ><img style="width: 80mm;" src="../../src/img/logo-ubv.png" alt="" srcset=""></td>
            <td style="font-weight: bold; text-align: left; font-size: 15px; border-left: 2px solid; ">
                <p style="margin-left: 25; margin-bottom: 0;">
                  República Bolivariana de Venezuela
                  <br>Universidad Bolivariana de Venezuela 
                  <br>Dirección General de Desarrollo de las 
                  <br>y los Trabajadores Académicos
                  <br>Vicerrectorado
                </p>
                    
            </td>
        </tr>
</table><br><br><br><br>
<table style="margin-left: 12mm;border-collapse: collapse; border: 1px solid black;">
  <tr style="text-align: center;  font-weight: bold; border: 1px solid black;">
    <th colspan="3" style="border: 1px solid black;">Solicitud de Jurado Principal y Suplente para Trabajo de Ascenso</th>
  </tr>
  <tr>
    <td style="font-weight: bold; border: 1px solid black;">Ascenso solicitado</td>
    <td colspan="2" style="border: 1px solid black;"><?php echo ucwords($_POST['ascenso_solicitado']);?></td>
  </tr>
  <tr>
    <td style="font-weight: bold; border: 1px solid black;">Eje Geopolítico Regional</td>
    <td colspan="2" style="border: 1px solid black;"><?php echo ucwords($_POST['eje_regional']);?></td>
  </tr>
  <tr>
    <td style="font-weight: bold; border: 1px solid black;">Fecha de primera contratación UBV</td>
    <td colspan="2" style="border: 1px solid black;"><?php echo ucwords($_POST['fecha_ingreso']);?></td>
  </tr>
  <tr>
    <td style="font-weight: bold; border: 1px solid black;">Fecha  Concurso de Oposición y categoría académica  aprobada
  </td>
    <td colspan="2" style="border: 1px solid black;"><?php echo ucwords($_POST['fecha_concurso']);?></td>
  </tr>
  <tr>
    <td style="font-weight: bold; border: 1px solid black;">Fecha y categoría académica del  último ascenso</td>
    <td colspan="2" style="border: 1px solid black;"> - </td>
  </tr>
  <tr>
    <td style="font-weight: bold; border: 1px solid black;">Fecha consignación de documentos y trabajo de ascenso</td>
    <td colspan="2" style="border: 1px solid black;"><?php echo ucwords($_POST['fecha_consulta']);?></td>
  </tr>
  <tr style="text-align: center; ">
    <td colspan="3" style="font-weight: bold; border: 1px solid black;">Información Personal del Solicitante</td>
  </tr>
  <tr>
    <td style="font-weight: bold; border: 1px solid black;">Cédula de Identidad</td>
    <td colspan="2" style="border: 1px solid black;"><?php echo ucwords($_POST['identificacion']);?></td>
  </tr>
  <tr>
    <td style="font-weight: bold; border: 1px solid black;">Nombres</td>
    <td colspan="2" style="border: 1px solid black;"><?php echo ucwords($_POST['primer_nombre']) . " " . ucwords($_POST['segundo_nombre']);?></td>
  </tr>
  <tr>
    <td style="font-weight: bold; border: 1px solid black;">Apellidos</td>
    <td colspan="2" style="border: 1px solid black;"><?php echo ucwords($_POST['primer_apellido']) . " " . ucwords($_POST['segundo_apellido']);?></td>
  </tr>
  <tr>
    <td style="font-weight: bold; border: 1px solid black;">Categoría Académica</td>
    <td colspan="2" style="border: 1px solid black;"><?php echo ucwords($_POST['escalafon_actual']);?></td>
  </tr>
  <tr style="text-align: center; ">
    <td colspan="3" style="font-weight: bold; border: 1px solid black;">Información del Trabajo de Ascenso</td>
  </tr>
  <tr>
    <td style="font-weight: bold; border: 1px solid black;">Propuesta Pedagógica Titulada:</td>
    <td colspan="2" style="border: 1px solid black;"><?php echo ucwords($_POST['titulo']);?></td>
  </tr>
  <tr style="text-align: center; ">
    <td colspan="3" style="font-weight: bold; border: 1px solid black;">Jurado Principal</td>
  </tr>
  <tr>
    <td style="font-weight: bold; border: 1px solid black;">Nombres</td>
    <td style="font-weight: bold; border: 1px solid black;">Cédula de Identidad</td>
    <td style="font-weight: bold;border: 1px solid black;">Categoría Académica</td>
  </tr>
  <?php $i0=1;while($i0<$_POST['contC']){ ?>
  <tr>
    <td style="border: 1px solid black;">(<?php echo $_POST['tipojc'.$i0];?> Del Jurado)<br> <?php echo $_POST['primer_nombrejc'.$i0] . " ". $_POST['segundo_nombrejc'.$i0]. ", ".$_POST['primer_apellidojc'.$i0] . " ". $_POST['segundo_apellidojc'.$i0];?></td>
    <td style="border: 1px solid black;"><?php echo $_POST['identificacionjc'.$i0];?></td>
    <td style="border: 1px solid black;"><?php echo $_POST['escalafonjc'.$i0];?></td>
  </tr>
  <?php 
                $i0++;
            }
        ?>
  <?php $i=1;while($i<$_POST['cont']){ ?>
  <tr>
    <td style=" border: 1px solid black;"><?php echo $_POST['primer_nombrej'.$i] . " ". $_POST['segundo_nombrej'.$i]. ", ".$_POST['primer_apellidoj'.$i] . " ". $_POST['segundo_apellidoj'.$i];?></td>
    <td style=" border: 1px solid black;"><?php echo $_POST['identificacionj'.$i];?></td>
    <td style="border: 1px solid black;"><?php echo $_POST['escalafonj'.$i];?></td>
  </tr>
  <?php 
                $i++;
            }
        ?>
  <tr style="text-align: center; ">
    <td colspan="3" style="font-weight: bold; border: 1px solid black;">Jurado Suplente</td>
  </tr>
  <tr>
    <td style="font-weight: bold; border: 1px solid black;">Nombres</td>
    <td style="font-weight: bold; border: 1px solid black;">Cédula de Identidad</td>
    <td style="font-weight: bold;border: 1px solid black;">Categoría Académica</td>
  </tr>
  <?php $i2=1;while($i2<$_POST['contS']){?>
  <tr>
    <td style=" border: 1px solid black;"><?php echo $_POST['primer_nombrejs'.$i2] . " ". $_POST['segundo_nombrejs'.$i2]. ", ".$_POST['primer_apellidojs'.$i2] . " ". $_POST['segundo_apellidojs'.$i2];?></td>
    <td style=" border: 1px solid black;"><?php echo   $_POST['identificacionjs'.$i2];?></td>
    <td style="border: 1px solid black;"><?php echo   $_POST['escalafonjs'.$i2];?></td>
  </tr>
  <?php 
                $i2++;
            }
        ?>
</table>
        <table style="margin-top: 70mm;">
          <tr>
            <td>
              <p style="margin-left: 105mm;">Av. Leonardo Da Vinci, Edf. UBV, Piso
              <br>10, Anexo A, Los Chaguaramos 
              <br>Teléfono: 0212-6063159 
              <br>RIF. G-20003773-3
              <br><a target="_blank" href="http://ubv.edu.ve">www.ubv.edu.ve</a> @ubv
              </p>
            </td>
            <td><img style="margin-left: 5mm; margin-top: 8mm; width: 20mm;" src="../../src/img/eu.png" alt="" srcset=""></td>
          </tr>
          
        </table>
          
</body>
</html>