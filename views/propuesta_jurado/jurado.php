<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>SIDTA | Historico de Jurados</title>


 
    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">


</head>

<body>
 
   <?php require 'views/header.php'; ?>
   

   <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-5">
                    <h2>Historico de Jurados</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>propuesta_jurado">Historico de Jurados</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Listar</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-7">
                    <div class="title-action">
                        
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>SIDTA</h5>
                        <div class="ibox-tools">

                        </div>
                    </div>
                    <div class="ibox-content">

                        <!--  contenido -->
                        <div class="table-responsive">
                        
                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Fecha de Solicitud</th>
                        <th>Docente</th>
                        <th>Escalafón del Docente</th>
                        <th>Estatus de la Solicitud donde Participo</th>
                        <th>Ciudad</th>

                        
                    </tr>
                    </thead >
                    <tbody id="tbody-diseno">
                    <?php 
                            foreach($this->jurados as $row){
                                $jurado= new Estructura();
                                $jurado=$row;?>
                    <tr id ="fila-<?php echo $jurado->id_jurado; ?>" class="gradeX">
                        <td><?php echo date("d-m-Y",strtotime($jurado->fecha_solicitud)); ?> </td>
                        <td>CI: <?php echo $jurado->identificacion.", ". $jurado->primer_nombre." ".$jurado->segundo_nombre.", ".$jurado->primer_apellido." ".$jurado->segundo_apellido; ?></td>
                        <td><?php echo $jurado->escalafon; ?></td>
                        <td><?php echo $jurado->estatus_ascenso; ?></td>
                        <td><?php echo $jurado->ciudad; ?></td>
                        
                    </tr>
                            <?php }?>
                    </tbody>
                    </table>
                        <!--  contenido -->

                            </div>

                        <!--  end contenido -->

                    </div>
                </div>
            </div>
            </div>
        </div>



    <?php require 'views/footer.php'; ?>

     

    <!-- dataTables Scripts -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>

   

</body>
</html>










