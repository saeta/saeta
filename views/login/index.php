
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Iniciar Sesión | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">
      <!-- favicon de sidta -->
      <link rel="shortcut icon" type="image/png" href="<?php echo constant ('URL');?>src/img/favicon_sidta.png"/>

</head>

<body class="green-bg">

    <div class="loginColumns animated fadeInDown">
        <div class="row">

            <div class="col-md-6">
                
                <div class="img_SIEA">
                <img src="<?php echo constant ('URL');?>src/css/patterns/target.png" alt="Captcha Image">

                
                </div>
                <div class="row" style="color: black">
                <h3> Sistema Integrado de Desarrollo de las Trabajadoras y los Trabajadores Académicos <b>SIDTA</b></h3>
</div>
            </div>
            <div class="col-md-6">
                <div class="ibox-content">
                    <div id="alerts"><?php echo $this->mensaje;?></div>  
                    <form id="form" method="post" class="m-t" role="form" action="<?php echo constant ('URL');?>login/iniciar">
                       
                        <div class="form-group">                        
                            <input type="text" name="usuario" id="usuario" class="form-control required" placeholder="Ingrese su Nombre de Usuario" maxlength='15' minlength="3">
                        </div>


                        <div class="form-group">                            
                            <input type="password" name="password" id="password" class="form-control required" placeholder="Ingrese su Contraseña" maxlength='15' minlength="3">
                        </div>
                        
                        <!-- <center> Para continuar, escriba los caracteres que ve en la imagen.</center><br>
                            <div class="form-group" style="text-align: center;">    

                                <img src="<?php echo constant ('URL');?>src/captcha/captcha.php" alt="Captcha Image">
                            </div>   
                            <div class="form-group" style="text-align: center;">
                                <input type="text" name="captcha"   placeholder="Caracteres" class="form-control required">

                            </div> --> 
                                <div class="form-group">

                                    <label>Verifica que no eres un robot</label>

                                    <center>
                                            
                                        <img id='myimage' src="<?php echo constant ('URL');?>src/captcha/captcha.php" alt="Captcha Image">
                                        <a style="color: #fff;" title="Refrescar imagen"  onclick="reloadImage('myimage')" class="btn btn-primary btn-lg" ><i class="fa fa-rotate-right"></i> </a>
                                    </center>
                                </div>
                                <div class="form-group">
                                    <label class="block" for="val-captcha">Ingresa los Caracteres que Aparecen en la Imagen. <span style="color: red;">*</span></label>
                                                                                   
                                    <!-- validamos que el captcha coincida con el de la imagen para enviar el formulario se le coloco display:none porque con hidden no se optuvo el mismo resultado 16/05/20-->                                       
                                    <input id="password-2" name="password2" type="text" class="form-control required" readonly style="display:none">
                                    <input id="confirm-2" name="confirm2" type="text" class="form-control required"  maxlength="8" title="Escriba los Caracteres que Muestra la Imagen" placeholder="Escriba los Caracteres que Muestra la Imagen">
                                </div>
                                <script>
                                
                                    $(function(){
                                        // validacion captcha
                                        $.post( '<?php echo constant ('URL');?>login/validacionCaptcha' ).done(function(respuesta)
                                        {   
                                            console.log($('#password-2').val(respuesta));
                                            $('#password-2').val(respuesta);
                                        });
                                    })
                                </script>                     
                        

                        <!-- style="text-align:center;"-->

                        
                        <button type="submit" class="btn btn-success block full-width m-b">Iniciar</button>

                        <a href="<?php echo constant('URL');?>login/viewRecuperar">
                            <center>¿Olvidaste tu contraseña? Click Aquí</center>
                        </a>

                        <p class="text-muted text-center">
                            <center>Eres Trabajador Académico en la UBV y ¿No tienes una cuenta?</center>
                        </p>
                        <a class="btn btn-sm btn-white btn-block" style="color: black" href="<?php echo constant ('URL');?>login/viewRegistrar">Crear Cuenta</a>
                    </form>

                    <p class="m-t" >
                     <center> Sistema Desarrollado bajo las políticas del Software Libre 2020</center>
                    </p>
                </div>
            </div>
        </div>
<hr style="border-color:black;">
        <div class="row" style="color: black"> 
            <div class="col-md-6">
            <h3>  Dirección General de Tecnologías de Información y Telecomunicaciones (DGTIT) </h3>
            </div>
            <div class="col-md-6 text-right">
             <h3>  Universidad Bolivariana de Venezuela 2020 </h3>
            </div>
        </div>
    </div>






  <!-- Mainly scripts -->
  <script src="<?php echo constant ('URL');?>src/js/jquery-3.1.1.min.js"></script>
 <!-- Jquery Validate -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>


 
<script type="text/javascript">
    $(function(){
        $('#form').validate({
            rules: {
                password: {
                         required: true,
                         minlength: 8                                                
                     }
            },
            confirm2: {
                equalTo: "#password-2"//revisar
            }
        });     
    });
//<!-- actualizar imagen -->                                                                                             
function reloadImage(imageId)
    {
        //ACTUALIZAMOS INPUT
    $(function(){
    // Lista de paises
    $.post( '<?php echo constant ('URL');?>login/validacionCaptcha' ).done( function(respuesta)
    {    
    $('#password-2').val(respuesta);
        //$( '#captcha' ).html( respuesta );
    });
    })
        //ACTUALIZAMOS IMAGEN
    path = '<?php echo constant ('URL');?>src/captcha/captcha.php?cache='; //for example
    imageObject = document.getElementById(imageId);
    imageObject.src = path + (new Date()).getTime();
    }

    
    </script>

</body>
</html>
