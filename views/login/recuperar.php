<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Recuperar Contraseña | SIDTA</title>

    <link href="<?php echo constant('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo constant('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant('URL');?>src/css/style.css" rel="stylesheet">
      <!-- favicon de sidta -->
      <link rel="shortcut icon" type="image/png" href="<?php echo constant ('URL');?>src/img/favicon_sidta.png"/>

</head>

<body class="gray-bg">

    <div class="passwordBox animated fadeInDown">
        <div class="row">
        
            <div class="col-md-12">
                <div class="ibox-content">

                    <h2 class="font-bold">¿Haz Olvidado tu Contraseña?</h2>

                    <p>
                        Ingrese su dirección de correo electrónico y su contraseña se restablecerá y se la enviaremos por correo electrónico.
                    </p>

                    <div class="row">

                        <div class="col-lg-12">
                            <form class="m-t" role="form" action="index.html">
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Ingrese su Dirección de correo electrónico" required="">
                                </div>

                                <button type="submit" class="btn btn-primary block full-width m-b">Enviar nueva contraseña</button>

                            </form>
                            <a href="<?php echo constant('URL');?>login">
                                <button type="button" class="btn btn-success block full-width m-b">Iniciar Sesión</button>
                            </a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                Universidad Bolivariana de Venezuela - DGTIT
            </div>
            <div class="col-md-6 text-right">
               <small>© 2020 - Copyright</small>
            </div>
        </div>
    </div>

</body>

</html>
