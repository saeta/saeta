<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Registrar Cuenta | SIDTA</title>

    <link href="<?php echo constant('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo constant('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo constant('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">
    <link href="<?php echo constant('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant('URL');?>src/css/style.css" rel="stylesheet">
    <!--  style chosen -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
    
    <!--  style select2 -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/select2/select2.min.css" rel="stylesheet">
      <!-- favicon de sidta -->
      <link rel="shortcut icon" type="image/png" href="<?php echo constant ('URL');?>src/img/favicon_sidta.png"/>

</head>

<body>

    <div id="wrapper">

    

        
        <div class="wrapper wrapper-content animated fadeInRight">
            
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <div class="ibox">
                        <div class="text-center ibox-title">
                        <img src="<?php echo constant ('URL');?>src/css/patterns/target.png" width="300">
                            <p>Crea tu cuenta para gestionar tu experiencia.</p>
                            <div class="ibox-tools">
                                <a href="<?php echo constant('URL');?>login"><button class="btn btn-success"><i class="fa fa-arrow-left"></i> Volver</button></a>
                            </div>
                        </div>
                        
                        <div class="ibox-content">
                        
                            <h2>
                                Completa el Formulario
                            </h2>
                            <?php echo $this->mensaje; ?>
                            <form class="m-t" role="form" id="form" method="post" action="<?php echo constant('URL');?>login/registrar">
                                <h1>Cuenta</h1>
                                <fieldset>
                                    <h2>Información de Personal y de Vivienda</h2>
                                    <div class="row">
                                        <div class="col-lg-12">
                                        <div class="form-group">
                                                <label>Nro. de Identificación <span style="color: red;">*</span></label>
                                                <input type="text" minlength="5" maxlength="10" name="cedula" id="cedula" class="form-control required number" placeholder="Ingrese su nro de Identificación (Cédula, Pasaporte, etc.)">
                                                <span><i>(Cédula, Pasaporte, etc.)</i> </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                        <input type="hidden" name="admin-user" value="0">
                                            <div class="form-group">
                                                <label>País donde Reside <span style="color: red;">*</span></label>
                                                <select name="pais" id="pais" style="width: 100%;"  class="form-control required select2_demo_1" tabindex="4">
                                                    <option value="">Seleccione el pais</option>
                                                    <?php 
                                                        foreach($this->paises as $row){
                                                        $pais=new Estructura();
                                                        $pais=$row;?> 
                                                    <option value="<?php echo $pais->id_pais;?>"><?php echo $pais->descripcion;?></option>
                                                    <?php }?>                                         
                                                </select>
                                                
                                            </div>
                                            <div class="form-group">
                                                <label>Estado donde Reside <span style="color: red;">*</span></label>
                                                <select name="estado" style="width: 100%;" id="estado" class="form-control select2_demo_2 required">
                                                    <option value="">Seleccione el Estado donde habita</option>
                                                </select>
                                                
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                        
                                            <div class="form-group">
                                                <label>Municipio donde Reside <span style="color: red;">*</span></label>

                                                <select name="municipio" style="width: 100%;" id="municipio" class="form-control select2_demo_3 required">
                                                    <option value="">Seleccione el municipio donde habita</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Parroquia donde Reside <span style="color: red;">*</span></label>
                                                <select name="parroquia" style="width: 100%;" id="parroquia" class="form-control select2_demo_4 required">
                                                    <option value="">Seleccione la parroquia donde reside</option>
                                                </select>
                                            </div>
                                            

                                        </div>
                                        <script>
                                                    $(".select2_demo_1").select2({
                                                        placeholder: "Seleccione el país donde vive",
                                                        allowClear: true
                                                    });
                                                    $(".select2_demo_2").select2({
                                                        placeholder: "Seleccione el estado donde vive",
                                                        allowClear: true
                                                    });
                                                    $(".select2_demo_3").select2({
                                                        placeholder: "Seleccione el municipio donde vive",
                                                        allowClear: true
                                                    });
                                                    $(".select2_demo_4").select2({
                                                        placeholder: "Seleccione la parroquia donde vive",
                                                        allowClear: true
                                                    });

                                                </script>
                                    </div>
<!-- combo select país, nacinalidad,estado, ciudad -->
<script>
                                    $(function(){

                                // Lista de paises
                                $.post( '<?php echo constant ('URL');?>models/comboubicacion-registro.php' ).done( function(respuesta)
                                {
                                    $( '#pais' ).html( respuesta );
                                });


                                // lista de estados	
                                $('#pais').change(function()
                                {
                                    var id_pais = $(this).val();
                                    
                                    // Lista de estados
                                    $.post( '<?php echo constant ('URL');?>models/comboubicacion-registro.php', { pais: id_pais} ).done( function( respuesta )
                                    {
                                        $( '#estado' ).html( respuesta );
                                    });
                                });


                                


                                // lista de MUNICIPIOS	
                                $('#estado').change(function()
                                {
                                    var id_estado = $(this).val();
                                    
                                    // Lista de estados
                                    $.post( '<?php echo constant ('URL');?>models/comboubicacion-registro.php', { estado: id_estado} ).done( function( respuesta )
                                    {
                                        $( '#municipio' ).html( respuesta );
                                    });
                                });

                                // lista de PARROQUIAS	
                                $('#municipio').change(function()
                                {
                                    var id_municipio = $(this).val();
                                    
                                    // Lista de estados
                                    $.post( '<?php echo constant ('URL');?>models/comboubicacion-registro.php', { municipio: id_municipio} ).done( function( respuesta )
                                    {
                                        $( '#parroquia' ).html( respuesta );
                                    });
                                });

                                })
                                    </script> 
                                </fieldset>
                                <h1>Cuenta</h1>
                                <fieldset>
                                <h2>Información de Personal</h2>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Programa de Formación <span style="color: red;">*</span></label>
                                                <select  name="programa[]" id="programa" data-placeholder="Seleccione los Programas de Formación" style="width: 100%;" class="chosen-select form-control m-b" multiple style="width:350px;" tabindex="4">
                                                    <?php 
                                                        foreach($this->programas as $row){
                                                        $programa=new Programa();
                                                        $programa=$row;?> 
                                                    <option value="<?php echo $programa->id_programa;?>"><?php echo $programa->descripcion;?></option>
                                                    <?php }?>                                            
                                                </select> 
                                            </div>
                                            <script>
                                                $('.chosen-select').chosen({width: "100%"});
                                            </script> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                          
                                                    <div class="form-group">
                                                        <label>Eje Regional <i>(ER)</i> <span style="color: red;">*</span></label>
                                                        <select  name="eje_regional" style="width: 100%;" id="eje_regional"  class="select2_demo_6 form-control required m-b">                                    
                                                            <option selected="selected" value="">Seleccione el ER al que Pertenece</option> 
                                                            <?php 
                                                                foreach($this->ejes_regionales as $row){
                                                                $eje_regional=new EjeRegional();
                                                                $eje_regional=$row;?> 
                                                            <option value="<?php echo $eje_regional->id_eje_regional;?>"><?php echo $eje_regional->descripcion;?></option>
                                                            <?php }?>                                            
                                                        </select>
                                                        
                                                    </div>
                                          
                                            <script>
                                                $('#eje_regional').change(function(){
                                                    var id_eje_regional = $(this).val();
                                                    
                                                    $.post( '<?php echo constant ('URL');?>login/getEjeMunipalbyER', { eje_regional: id_eje_regional} ).done( function( respuesta )
                                                    {
                                                        $( '#eje_municipal' ).html( respuesta );
                                                    });
                                                });
                                            </script>

                                                <div class="form-group">
                                                    <label>Centro de Estudio <i>(CE)</i> <span style="color: red;">*</span></label>
                                                    <select  name="centro_estudio" style="width: 100%;" id="centro_estudio"  class="select2_demo_7 form-control required m-b">                                    
                                                        <option selected="selected" value="">Seleccione el CE al que pertenece </option> 
                                                        <?php 
                                                            foreach($this->centros as $row){
                                                                $centro_estudio=new CentroEstudio();
                                                                $centro_estudio=$row;?> 
                                                        <option value="<?php echo $centro_estudio->id_centro_estudio;?>"><?php echo $centro_estudio->descripcion;?></option>
                                                        <?php }?>                                            
                                                    </select> 
                                                </div>  
                                        </div>
                                        
                                        <div class="col-lg-6">
                                        <div class="form-group">
                                                        <label>Eje Municipal <i>(EM)</i> <span style="color: red;">*</span></label>
                                                        <select  name="eje_municipal" style="width: 100%;" id="eje_municipal"  class=" form-control m-b">                                    
                                                            <option selected="selected" value="">Seleccione el Eje Municipal</option> 
                                                        </select>
                                                    </div>
                                            <div class="form-group">
                                                <label>Aldea del Docente <i>(Opcional)</i></label>
                                                <input id="aldea" name="aldea" autocomplete="off" type="text" class="form-control typeahead_3" maxlength='80' minlength="3" placeholder="Ingrese la Aldea del Docente">
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                            $(document).ready(function(){
                                                $('.typeahead_3').typeahead({
                                                    source: [
                                                        <?php 
                                                            foreach($this->aldeas_ubv as $row){
                                                                $aldea_ubv=new Estructura();
                                                                $aldea_ubv=$row; ?>
                                                            {"name": "<?php echo $aldea_ubv->descripcion;?>", "code": "BR", "ccn0": "080"},
                                                        <?php } ?>
                                                        ]
                                                });
                                            });
                                        $(".select2_demo_5").select2({
                                            placeholder: "Seleccione el Programa de Formación alque pertenece",
                                            allowClear: true
                                        });
                                        $(".select2_demo_6").select2({
                                            placeholder: "Seleccione el eje regional al que pertenece",
                                            allowClear: true
                                        });
                                        $(".select2_demo_7").select2({
                                            placeholder: "Seleccione el centro de estudio al que pertenece",
                                            allowClear: true
                                        });
                                       

                                    </script>
                                </fieldset>

                                <h1>Seguridad</h1>
                                <fieldset>
                                <h2>Información de Seguridad</h2>
                                <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Pregunta de Recuperación 1<span style="color: red;">*</span></label>
                                                <select  name="pregunta1" id="pregunta1" style="width: 100%;" class="select2_demo_8 form-control required m-b">                                    
                                                        <option selected="selected" value="">Seleccione la pregunta que desee </option> 
                                                        <?php 
                                                            foreach($this->preguntas as $row){
                                                                $pregunta=new Pregunta();
                                                                $pregunta=$row;?> 
                                                        <option value="<?php echo $pregunta->id_pregunta;?>"><?php echo $pregunta->descripcion;?></option>
                                                        <?php }?>                                            
                                                    </select> 
                                            </div>
                                            <div class="form-group">
                                                <label>Respuesta de Seguridad 1<span style="color: red;">*</span></label>
                                                <input id="respuesta1" maxlength="60" name="respuesta1" placeholder="Ingrese la Respuesta a la Pregunta 1" type="text" class="form-control required">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Preguntas de Recuperación 2<span style="color: red;">*</span></label>
                                                <select  name="pregunta2" id="pregunta2" style="width: 100%;" class="select2_demo_9 form-control required m-b">                                    
                                                        <option selected="selected" value="">Seleccione la pregunta que desee </option> 
                                                        <?php 
                                                            foreach($this->preguntas as $row){
                                                                $pregunta2=new Pregunta();
                                                                $pregunta2=$row;?> 
                                                        <option value="<?php echo $pregunta2->id_pregunta;?>"><?php echo $pregunta2->descripcion;?></option>
                                                        <?php }?>                                            
                                                    </select>  
                                            </div>
                                            <div class="form-group">
                                                <label>Respuesta 2<span style="color: red;">*</span></label>
                                                <input id="address" maxlength="60" name="respuesta2" placeholder="Ingrese la Respuesta a la Pregunta 2" type="text" class="form-control required">
                                            </div>
                                            
                                            
                                        </div>
                                    </div>
                                    <script>

                                        $(".select2_demo_8").select2({
                                            placeholder: "Seleccione la Pregunta de Seguridad 1",
                                            allowClear: true
                                        });
                                        $(".select2_demo_9").select2({
                                            placeholder: "Seleccione Pregunta de Seguridad 2",
                                            allowClear: true
                                        });
                                                    
                                    </script>
                                    
                                </fieldset>

                                <h1>Acceso</h1>
                                <fieldset>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Contraseña <span style="color: red;">*</span></label>
                                            <input type="password" maxlength="8" minlength="8" name="password" id="password" class="form-control required" placeholder="Ingrese su Contraseña">
                                        </div>
                                        <div class="form-group">
                                            <label>Confirmar Contraseña <span style="color: red;">*</span></label>
                                            <input type="password" maxlength="8" minlength="8" name="confirm" id="confirm" class="form-control required" placeholder="Confirme su Contraseña">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                    <!-- 10 de septiembre 2020: agregando captcha -->
                                        
                                        <div class="form-group">

                                            <label>Verifica que no eres un robot</label>

                                            <center>
                                            
                                                <img id='myimage' src="<?php echo constant ('URL');?>src/captcha/captcha.php" alt="Captcha Image">
                                            <button title="Refrescar imagen"  onclick="reloadImage('myimage')" class="btn btn-primary btn-lg" ><i class="fa fa-rotate-right"></i> </button>
                                            </center>
                                        </div>
                                        <div class="form-group">
                                            <label class="block" for="val-captcha">Ingresa los Caracteres que Aparecen en la Imagen. <span style="color: red;">*</span></label>
                                                                                   
                                            <!-- validamos que el captcha coincida con el de la imagen para enviar el formulario se le coloco display:none porque con hidden no se optuvo el mismo resultado 16/05/20-->                                       
                                            <input id="password-2" name="password2" type="text" class="form-control required" readonly style="display:none">
                                            <input id="confirm-2" name="confirm2" type="text" class="form-control required"  maxlength="8" title="Escriba los Caracteres que Muestra la Imagen" placeholder="Escriba los Caracteres que Muestra la Imagen">
                                        </div>
                                        <script>
                                            $(function(){
                                                // Lista de paises
                                                $.post( '<?php echo constant ('URL');?>login/validacionCaptcha' ).done( function(respuesta)
                                                {   
                                                    $('#password-2').val(respuesta);
                                                    //$( '#captcha' ).html( respuesta );
                                                });
                                            })
                                        </script> 
                                        <!-- 
                                            <div class="panel panel-info">
                                                <div class="panel-heading">
                                                    <i class="fa fa-info-circle"></i> Información
                                                </div>
                                                <div class="panel-body">
                                                    <p>El Resto de Datos serán suministrados por el SIGAD</p>
                                                </div>

                                            </div>
                                        -->
                                            
                                    </div>
                                </div>
                                    
                                </fieldset>
                            </form>
                        </div>
                        <div class="ibox-footer">
                            <div class="float-right">
                                Independencia y  <strong>patria socialista</strong> Viviremos y venceremos.
                            </div>
                            <div>
                                <strong>Dirección General de Tecnologías de Información y Telecomunicaciones (DGTIT) 2020</strong>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="col-lg-2"></div>

                </div>
            </div>
        

        </div>
        </div>



    <!-- Mainly scripts -->
    <script src="<?php echo constant('URL');?>src/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo constant('URL');?>src/js/popper.min.js"></script>
    <script src="<?php echo constant('URL');?>src/js/bootstrap.js"></script>
    <script src="<?php echo constant('URL');?>src/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo constant('URL');?>src/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo constant('URL');?>src/js/inspinia.js"></script>
    <script src="<?php echo constant('URL');?>src/js/plugins/pace/pace.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- Chosen -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>

    <!-- Select2 -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/select2/select2.full.min.js"></script>
    <!-- Typehead -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>

    <script>

        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            },
                            confirm2: {
                                equalTo: "#password-2"//revisar
                            }
                        }
                    });
       });
       
       //<!-- actualizar imagen -->                                                                                             
    function reloadImage(imageId)
    {
        //ACTUALIZAMOS INPUT
    $(function(){
    // Lista de paises
    $.post( '<?php echo constant ('URL');?>login/validacionCaptcha' ).done( function(respuesta)
    {    
    $('#password-2').val(respuesta);
        //$( '#captcha' ).html( respuesta );
    });
    })
        //ACTUALIZAMOS IMAGEN
    path = '<?php echo constant ('URL');?>src/captcha/captcha.php?cache='; //for example
    imageObject = document.getElementById(imageId);
    imageObject.src = path + (new Date()).getTime();
    }
    </script>

</body>

</html>
