<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <!--Sistema que se encargara de la asignación de proyectos según la ubicación  geográfica de un Estudiante -->

  <!-- Meta, title, CSS, favicons, etc.


-->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
<div class="titulo"><title>SIDTA</title></div>
 

 <!-- CSS propiedades del boton -->
  <link href="<?php echo constant('URL'); ?>src/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo constant('URL'); ?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

<link href="<?php echo constant('URL'); ?>src/css/animate.css" rel="stylesheet">
<link href="<?php echo constant('URL'); ?>src/css/style.css" rel="stylesheet">

  <!-- CSS del Bienvenido, imagen de fondo y algunas propiedades del boton -->


      <!-- favicon de sidta -->
      <link rel="shortcut icon" type="image/png" href="<?php echo constant ('URL');?>src/img/favicon_sidta.png"/>


</head>


  <body class="gray-bg" style=" background-image: url('<?php echo constant('URL'); ?>src/img/landing/fondo.jpg')">
  <div style="margin-top: 10em; background-color: rgba(162, 165, 163, 0.4); max-width: 960px; padding: 10px; color: black; width: 200%;" class="middle-box  text-center animated fadeInDown">
        <h1><img style="width: 450px;"src="<?php echo constant ('URL');?>src/css/patterns/target.png" alt="Captcha Image"></h1>
        <h3 class="font-bold">Sistema Integrado de Desarrollo de las Trabajadoras y los Trabajadores Académicos <br><b> - SIDTA -</b></h3>

        <div class="error-desc">

        <a class="btn btn-success" href="<?php echo constant('URL'); ?>login" rel="stylesheet">Bienvenido</a> <br/>
                <img style="margin-top: 15px; margin-bottom: 15px; width: 10%;" src="<?php echo constant ('URL');?>src/img/ubvlogo.png" alt="Captcha Image"><br/>
        &#169; 2020 - Universidad Bolivariana de Venezuela, Dirección General de Tecnologías de Información y Telecomunicaciones - DGTIT.</p>
        </div>
    </div>
                  
          

                        
   
    <script src="<?php echo constant('URL'); ?>src/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo constant('URL'); ?>src/js/popper.min.js"></script>
    <script src="<?php echo constant('URL'); ?>src/js/bootstrap.js"></script>

         
  
    
  

    <!-- Custom and plugin javascript -->
    <script src="<?php echo constant('URL'); ?>src/js/inspinia.js"></script>
    <script src="<?php echo constant('URL'); ?>src/js/plugins/pace/pace.min.js"></script>

    <!-- Tinycon -->
    <script src="<?php echo constant('URL'); ?>src/js/plugins/tinycon/tinycon.min.js"></script>


</body>
</html>