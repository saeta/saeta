<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Registrar Usuario | SIDTA</title>


    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">
  
    
    <!-- jasny input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <!--  style select2 -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/select2/select2.min.css" rel="stylesheet">
    <!-- datapicker -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <!-- sweetalert input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- chosen -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

</head>

<body>
 
    <div id="wrapper">
   <?php require 'views/header.php'; ?>
   

        <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-9">
                    <h2>Agregar Estudio </h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>home">Inicio</a>
                        </li>
                        
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>usuarios">Listar Usuarios</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <a href="<?php echo constant ('URL');?>usuarios/viewAdd"><strong>Agregar</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-3">
                    <div class="title-action">
                        <a href="<?php echo constant ('URL');?>usuarios" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Volver</a>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5><i class="fa fa-angle-double-right"></i> SIDTA <i class="fa fa-angle-double-left"></i></h5>
                        </div>
                        <div class="ibox-content">
                            <h2>
                                Agregar Usuario a SIDTA
                            </h2>
                            <p>
                                Completa el formulario para registrar un Usuario dentro de SIDTA.
                            </p>
                            <?php echo $this->mensaje;?>
                            <div class="alert alert-info">Todos los campos marcados con un (<span style="color: red;">*</span>) Son Obligatorios</div>

                            <form class="m-t" role="form" id="form" method="post">
                            <h1>Perfil de Usuario</h1>
                                <fieldset>
                                    <h2>Información del Perfil</h2>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="">Perfil de Usuario <span style="color: red;">*</span></label>
                                                <select name="id_perfil" id="id_perfil" onchange="mostrar(this.value)" style="width: 100%;" class="select2_demo_10 form-control required">
                                                    <option value="">Seleccione el Perfil del Usuario</option>
                                                    <?php 
                                                            foreach($this->perfiles as $row){
                                                                $perfil=new Persona();
                                                                $perfil=$row;?> 
                                                        <option value="<?php echo $perfil->id;?>"><?php echo $perfil->descripcion;?></option>
                                                        <?php }?>  
                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- en caso de 1=trabajador academico  insertamos estos camposparte 1- -->
                                <div class="academico2" style="display: none">
                                <h2>Información de Personal y de Vivienda</h2>
                                    <!-- input hidden para manejar el action al que va el formulario -->
                                    <input type="hidden" name="admin-user" value="1">
                                    <div class="row">
                                        <div class="col-lg-12">
                                        <div class="form-group">
                                                <label>Nro. de Identificación <span style="color: red;">*</span></label>
                                                <input type="text" minlength="5" maxlength="10" name="cedula" id="cedula" class="form-control required number" placeholder="Ingrese su nro de Identificación (Cédula, Pasaporte, etc.)">
                                                <span><i>(Cédula, Pasaporte, etc.)</i> </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            
                                            <div class="form-group">
                                                <label>País donde Reside <span style="color: red;">*</span></label>
                                                <select name="pais" id="pais" style="width: 100%;"  class="form-control required select2_demo_1" tabindex="4">
                                                    <option value="">Seleccione el pais</option>
                                                    <?php 
                                                        foreach($this->paises as $row){
                                                        $pais=new Estructura();
                                                        $pais=$row;?> 
                                                    <option value="<?php echo $pais->id_pais;?>"><?php echo $pais->descripcion;?></option>
                                                    <?php }?>                                         
                                                </select>
                                                
                                            </div>
                                            <div class="form-group">
                                                <label>Estado donde Reside <span style="color: red;">*</span></label>
                                                <select name="estado" style="width: 100%;" id="estado" class="form-control select2_demo_2 required">
                                                    <option value="">Seleccione el Estado donde habita</option>
                                                </select>
                                                
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                        
                                            <div class="form-group">
                                                <label>Municipio donde Reside <span style="color: red;">*</span></label>

                                                <select name="municipio" style="width: 100%;" id="municipio" class="form-control select2_demo_3 required">
                                                    <option value="">Seleccione el municipio donde habita</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Parroquia donde Reside <span style="color: red;">*</span></label>
                                                <select name="parroquia" style="width: 100%;" id="parroquia" class="form-control select2_demo_4 required">
                                                    <option value="">Seleccione la parroquia donde reside</option>
                                                </select>
                                            </div>
                                            

                                        </div>
                                        <script>
                                                    $(".select2_demo_1").select2({
                                                        placeholder: "Seleccione el país donde vive",
                                                        allowClear: true
                                                    });
                                                    $(".select2_demo_2").select2({
                                                        placeholder: "Seleccione el estado donde vive",
                                                        allowClear: true
                                                    });
                                                    $(".select2_demo_3").select2({
                                                        placeholder: "Seleccione el municipio donde vive",
                                                        allowClear: true
                                                    });
                                                    $(".select2_demo_4").select2({
                                                        placeholder: "Seleccione la parroquia donde vive",
                                                        allowClear: true
                                                    });

                                                </script>
                                    </div>
<!-- combo select país, nacinalidad,estado, ciudad -->
<script>
                                    $(function(){

                                // Lista de paises
                                $.post( '<?php echo constant ('URL');?>models/comboubicacion-registro.php' ).done( function(respuesta)
                                {
                                    $( '#pais' ).html( respuesta );
                                });


                                // lista de estados	
                                $('#pais').change(function()
                                {
                                    var id_pais = $(this).val();
                                    
                                    // Lista de estados
                                    $.post( '<?php echo constant ('URL');?>models/comboubicacion-registro.php', { pais: id_pais} ).done( function( respuesta )
                                    {
                                        $( '#estado' ).html( respuesta );
                                    });
                                });


                                


                                // lista de MUNICIPIOS	
                                $('#estado').change(function()
                                {
                                    var id_estado = $(this).val();
                                    
                                    // Lista de estados
                                    $.post( '<?php echo constant ('URL');?>models/comboubicacion-registro.php', { estado: id_estado} ).done( function( respuesta )
                                    {
                                        $( '#municipio' ).html( respuesta );
                                    });
                                });

                                // lista de PARROQUIAS	
                                $('#municipio').change(function()
                                {
                                    var id_municipio = $(this).val();
                                    
                                    // Lista de estados
                                    $.post( '<?php echo constant ('URL');?>models/comboubicacion-registro.php', { municipio: id_municipio} ).done( function( respuesta )
                                    {
                                        $( '#parroquia' ).html( respuesta );
                                    });
                                });

                                })
                                    </script>
                                    </div>
                                <!-- end trabajador academico parte 1- -->
<!-- comienzo de otro tipo de perfil de usuario que no sea trabajador academico parte 1- -->
                                    <div class="row otro" style="display: none;">
                                        <div class="col-lg-6">

                                        <div class="form-group">
                                        <label>Primer nombre<span style="color: red;">*</span></label>
                                        <input id="pnombre" name="pnombre" type="text" class="form-control required" maxlength='29' minlength="3"  onkeypress="return soloLetras(event)" placeholder="Escriba su primer nombre">
                                        </div>

                                        <div class="form-group">
                                        <label>Segundo Nombre</label>
                                        <input id="snombre" name="snombre" type="text" class="form-control " maxlength='29' minlength="3"  onkeypress="return soloLetras(event)"  placeholder="Escriba su segundo nombre">
                                        </div>                                    

                                        

                                        </div>
                                        <div class="col-lg-6">
                                            
                                            <div class="form-group">
                                        <label>Primer apellido <span style="color: red;">*</span></label>
                                        <input id="papellido" name="papellido" type="text" class="form-control required" maxlength='29' minlength="3"  onkeypress="return soloLetras(event)"  placeholder="Escriba su primer apellido">
                                        </div>
                                        <div class="form-group">
                                        <label>Segundo apellido</label>
                                        <input id="sapellido" name="sapellido" type="text" class="form-control" maxlength='29' minlength="3"  onkeypress="return soloLetras(event)" placeholder="Escriba su segundo apellido">
                                        </div>
                                           
                                        </div>
                                    </div>
                                    <!-- end otro perfil parte 1-->
                            </fieldset>
                            <h1>Cuenta</h1>
                                <fieldset>
                                    <!-- en caso de 1=trabajador academico  insertamos estos campos parte 2-->
                                    <div class="academico2" style="display: none">
                                        <div class="row">
                                        <h2>Información de Trabajador Académico</h2>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                    <label>Programa de Formación <span style="color: red;">*</span></label>
                                                    
                                                    <select  name="programa" id="programa" style="width: 100%;" class="select2_demo_5 form-control m-b">                                    
                                                        <option selected="selected" value="">Seleccione el PF al que Pertenece</option> 
                                                        <?php 
                                                            foreach($this->programas as $row){
                                                            $programa=new Estructura();
                                                            $programa=$row;?> 
                                                            <option value="<?php echo $programa->id_programa;?>"><?php echo $programa->descripcion;?></option>
                                                        <?php }?>                                            
                                                    </select> 
                                                    </div>
                                            </div>
                                        </div>
                                        
                                    
                                    <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Eje Regional <i>(ER)</i> <span style="color: red;">*</span></label>
                                            <select  name="eje_regional" style="width: 100%;" id="eje_regional"  class="select2_demo_6 form-control m-b">                                    
                                                <option selected="selected" value="">Seleccione el ER al que Pertenece</option> 
                                                <?php 
                                                    foreach($this->ejes_regionales as $row){
                                                    $eje_regional=new Estructura();
                                                    $eje_regional=$row;?> 
                                                <option value="<?php echo $eje_regional->id_eje_regional;?>"><?php echo $eje_regional->descripcion;?></option>
                                                <?php }?>                                            
                                            </select>
                                             
                                        </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                            <label>Centro de Estudio <i>(CE)</i> <span style="color: red;">*</span></label>
                                            <select  name="centro_estudio" style="width: 100%;" id="centro_estudio"  class="select2_demo_7 form-control m-b">                                    
                                                <option selected="selected" value="">Seleccione el CE al que pertenece </option> 
                                                <?php 
                                                    foreach($this->centros as $row){
                                                        $centro_estudio=new Estructura();
                                                        $centro_estudio=$row;?> 
                                                <option value="<?php echo $centro_estudio->id_centro_estudio;?>"><?php echo $centro_estudio->descripcion;?></option>
                                                <?php }?>                                            
                                            </select> 
                                        </div>    

                                </div>
                                    </div>
                                    <script>
                                        $(".select2_demo_5").select2({
                                            placeholder: "Seleccione el Programa de Formación alque pertenece",
                                            allowClear: true
                                        });
                                        $(".select2_demo_6").select2({
                                            placeholder: "Seleccione el eje regional al que pertenece",
                                            allowClear: true
                                        });
                                        $(".select2_demo_7").select2({
                                            placeholder: "Seleccione el centro de estudio al que pertenece",
                                            allowClear: true
                                        });
                                        $(".select2_demo_10").select2({
                                            placeholder: "Seleccione el perfil del usuario",
                                            allowClear: true
                                        });
                                        $(".select2_demo_11").select2({
                                            placeholder: "Seleccione la Etnía a la que Pertenece",
                                            allowClear: true
                                        });
                                        $(".select2_demo_12").select2({
                                            placeholder: "Seleccione la o las Discapacidades que posee",
                                            allowClear: true
                                        });
                                    </script>
                                    
                                    </div>
                                    <!-- end trabajador academico parte 2 -->
                                    
                            <!-- comienzo otro perfil parte 2 -->
                        <div class="row otro">
                                        <div class="col-lg-6">


                            <div class="form-group" id="data_1">
                                <label class="font-normal">Fecha de Nacimiento<span style="color: red;">*</span></label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" name="fnac" id="fnac" class="form-control required" placeholder="<?php echo  date('m/d/Y');?>">
                                </div>
                            </div>


                                        
                                           <div class="form-group">
                                            <label>Estado Civil<span style="color: red;">*</span></label>                                        
                                            <select  name="estadocivil" id="estadocivil"  style="width: 100%;" class="select2_demo_3 form-control required m-b">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/persona.php';
                                                foreach($this->estadosCivil as $row){
                                                $estado=new Persona();
                                                $estado=$row;?> 
                                            <option value="<?php echo $estado->id;?>"><?php echo $estado->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>


                                            <div class="form-group">
                                            <label>Tipo de Documento<span style="color: red;">*</span></label>                                        
                                            <select  name="tdocumento" id="tdocumento"  style="width: 100%;" class="select2_demo_3 form-control required m-b">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/persona.php';
                                                foreach($this->documentoidentidad as $row){
                                                $documento=new Persona();
                                                $documento=$row;?> 
                                            <option value="<?php echo $documento->id;?>"><?php echo $documento->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>


                                        </div>
                                        <div class="col-lg-6">

                                      
                                      
                                        
                                        <div class="form-group">
                                        <label>Genero<span style="color: red;">*</span></label>                                        
                                            <select  style="width: 100%;" class="select2_demo_3 form-control required m-b" name="genero" id="genero">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/persona.php';
                                                foreach($this->generos as $row){
                                                $genero=new Persona();
                                                $genero=$row;?> 
                                            <option value="<?php echo $genero->id;?>"><?php echo $genero->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>


                                            <div class="form-group">
                                        <label>Etnia</label>                                        
                                            <select  style="width: 100%;" class="select2_demo_3 form-control  m-b" name="etnia" id="etnia">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/persona.php';
                                                foreach($this->etnias as $row){
                                                $etnia=new Persona();
                                                $etnia=$row;?> 
                                            <option value="<?php echo $etnia->id;?>"><?php echo $etnia->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>      

                                       
                                        <div class="form-group">
                                        <label>Numero de Documento<span style="color: red;">*</span></label>
                                        <input id="ndocumento" name="ndocumento" type="text" class="form-control required"  maxlength='10' minlength="7" onkeypress="return valSoloNumeros(event)" placeholder="Escriba su numero de documento">
                                        </div>
                                       


                                        </div>                                       
                                    </div>
<!-- fin otro perfil parte 2-->
                                    
                                    
                                     </fieldset> 
                                    <h1>Datos Personales</h1>
                                 <fieldset>
                                <h2>Ubicación</h2>

                                <!-- comienzo academico parte 3-->
                                <h2>Información de Seguridad</h2>
                                <div class="row academico2">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Pregunta de Recuperación 1<span style="color: red;">*</span></label>
                                                <select  name="pregunta1" id="pregunta1" style="width: 100%;" class="10 form-control required m-b">                                    
                                                        <option selected="selected" value="">Seleccione la pregunta que desee </option> 
                                                        <?php 
                                                            foreach($this->preguntas as $row){
                                                                $pregunta=new Estructura();
                                                                $pregunta=$row;?> 
                                                        <option value="<?php echo $pregunta->id_pregunta;?>"><?php echo $pregunta->descripcion;?></option>
                                                        <?php }?>                                            
                                                    </select> 
                                            </div>
                                            <div class="form-group">
                                                <label>Respuesta de Seguridad 1<span style="color: red;">*</span></label>
                                                <input id="respuesta1" maxlength="60" name="respuesta1" placeholder="Ingrese la Respuesta a la Pregunta 1" type="text" class="form-control required">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Preguntas de Recuperación 2<span style="color: red;">*</span></label>
                                                <select  name="pregunta2" id="pregunta2" style="width: 100%;" class="select2_demo_9 form-control required m-b">                                    
                                                        <option selected="selected" value="">Seleccione la pregunta que desee </option> 
                                                        <?php 
                                                            foreach($this->preguntas as $row){
                                                                $pregunta2=new Estructura();
                                                                $pregunta2=$row;?> 
                                                        <option value="<?php echo $pregunta2->id_pregunta;?>"><?php echo $pregunta2->descripcion;?></option>
                                                        <?php }?>                                            
                                                    </select>  
                                            </div>
                                            <div class="form-group">
                                                <label>Respuesta 2<span style="color: red;">*</span></label>
                                                <input id="address" maxlength="60" name="respuesta2" placeholder="Ingrese la Respuesta a la Pregunta 2" type="text" class="form-control required">
                                            </div>
                                            
                                            
                                        </div>
                                    </div>
                                    <script>

                                        $(".10").select2({
                                            placeholder: "Seleccione la Pregunta de Seguridad 1",
                                            allowClear: true
                                        });
                                        $(".select2_demo_9").select2({
                                            placeholder: "Seleccione Pregunta de Seguridad 2",
                                            allowClear: true
                                        });
                                                    
                                    </script>
                       <!-- fin academico parte 3-->
             
                            <!-- comienzo otro perfil parte 3-->

                                <div class="row otro">
                                        <div class="col-lg-6">
                                               
                                           <div class="form-group">
                                           <label>País<span style="color: red;">*</span></label>                                        
                                            <select class="select2_demo_3 form-control required m-b" name="pais" id="pais2">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            </select>   
                                            </div>
                                                <!-- por peticion de Edgar se saca nacionalidad de combo depoendiente Dom/24/11/19-->
                                            <div class="form-group">
                                            <label>Nacionalidad<span style="color: red;">*</span></label>                                        
                                            <select class="select2_demo_3 form-control required m-b" name="nacionalidad" id="nacionalidad">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/persona.php';
                                                foreach($this->nacionalidades as $row){
                                                $nacionalidad=new Persona();
                                                $nacionalidad=$row;?> 
                                            <option value="<?php echo $nacionalidad->id_nacionalidad;?>"><?php echo $nacionalidad->descripcion;?></option>
                                            <?php }?>
                                            </select> 
                                            </div>    


                                            <div class="form-group">
                                            <label>Tipo de Domicilio<span style="color: red;">*</span></label>                                        
                                            <select  name="tdomicilio" id="tdomicilio"  class="select2_demo_3 form-control required m-b">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/persona.php';
                                                foreach($this->domicilios as $row){
                                                $domicilio=new Persona();
                                                $domicilio=$row;?> 
                                            <option value="<?php echo $domicilio->id;?>"><?php echo $domicilio->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>


                                        </div>
                                        <div class="col-lg-6">
                                            
                                        <div class="form-group">
                                            <label>Estado<span style="color: red;">*</span></label>                                        
                                            <select  name="estado" id="estado2"  class="select2_demo_3 form-control required m-b">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            </select>   
                                            </div>

                                        <div class="form-group">
                                            <label>Municipio<span style="color: red;">*</span></label>                                        
                                            <select  name="municipio" id="municipio2"  class="select2_demo_3 form-control required m-b">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            </select>   
                                            </div>

                                        <div class="form-group">
                                        <label>Parroquia<span style="color: red;">*</span></label>                                        
                                            <select class="select2_demo_3 form-control required m-b" name="parroquia" id="parroquia2">                                    
                                            <option selected="selected" value="">Seleccione </option>                                         
                                            </select>   
                                            </div>



                                        </div>  
                                    </div>

                                    <div class="otro form-group">
                                        <label>Dirección<span style="color: red;">*</span></label>
                                        <textarea id="direccion" name="direccion" type="text" class="form-control required valid" placeholder="Escriba una dirección..." aria-required="true" aria-invalid="false"  maxlength='145' minlength="5"></textarea>
                                        </div>
                                                
                                    <!-- combo select país, nacinalidad,estado, ciudad -->
                                    <script>
                                    $(function(){

                                // Lista de paises
                                $.post( '<?php echo constant ('URL');?>models/comboubicacion.php' ).done( function(respuesta)
                                {
                                    $( '#pais2' ).html( respuesta );
                                });


                                // lista de estados	
                                $('#pais2').change(function()
                                {
                                    var id_pais = $(this).val();
                                    
                                    // Lista de estados
                                    $.post( '<?php echo constant ('URL');?>models/comboubicacion.php', { pais: id_pais} ).done( function( respuesta )
                                    {
                                        $( '#estado2' ).html( respuesta );
                                    });
                                });


                                // lista de nacinalidades	
                               /* $('#pais').change(function()
                                {
                                    var id_pais = $(this).val();
                                    
                                    // Lista de nacinalidades
                                    $.post( '<?php echo constant ('URL');?>models/comboubicacion.php', { nacionalidad: id_pais} ).done( function( respuesta )
                                    {
                                        $( '#nacionalidad' ).html( respuesta );
                                    });
                                });*/


                                // lista de municipios	
                                $('#estado2').change(function()
                                {
                                    var id_estado = $(this).val();
                                    
                                    // Lista de municipios
                                    $.post( '<?php echo constant ('URL');?>models/comboubicacion.php', { municipio: id_estado} ).done( function( respuesta )
                                    {
                                        $( '#municipio2' ).html( respuesta );
                                    });
                                });


                                // lista de parroquias	
                                $('#municipio2').change(function()
                                {
                                    var id_municipio = $(this).val();
                                    
                                    // Lista de parroquias
                                    $.post( '<?php echo constant ('URL');?>models/comboubicacion.php', { parroquia: id_municipio} ).done( function( respuesta )
                                    {
                                        $( '#parroquia2' ).html( respuesta );
                                    });
                                });





                                })
                                    </script> 
                                <!-- fin otro perfil parte 3-->

                               
                                    
                                </fieldset>
                               
                               <h1>Datos Personales</h1>
                                <fieldset>
                               <!-- comienzo academico parte 4-->
                               <div class="row academico2">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Contraseña <span style="color: red;">*</span></label>
                                            <input type="password" maxlength="8" minlength="8" name="password" id="password" class="form-control" placeholder="Ingrese su Contraseña">
                                        </div>
                                        <div class="form-group">
                                            <label>Confirmar Contraseña <span style="color: red;">*</span></label>
                                            <input type="password" maxlength="8" minlength="8" name="confirm" id="confirm" class="form-control" placeholder="Confirme su Contraseña">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <i class="fa fa-info-circle"></i> Información
                                            </div>
                                            <div class="panel-body">
                                                <p>El Resto de Datos serán suministrados por el SIGAD</p>
                                                <p>El Resto de Datos serán suministrados por el SIGAD</p>
                                            </div>

                                        </div>
                                        
                                            
                                    </div>
                                </div>
                                    <!-- fin academico parte 4-->
                                    <!-- comienzo otro perfil parte 4 -->
                                    <h2 class="otro">Contacto e Información personal</h2> 
                                    <div class="row otro">
                                        <div class="col-lg-6">


                                        <div class="form-group">
                                        <label>Tipo Correo<span style="color: red;">*</span></label>                                        
                                            <select class="select2_demo_3 form-control required m-b" name="tcorreo" id="tcorreo">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/persona.php';
                                                foreach($this->tcorreos as $row){
                                                $correo=new Persona();
                                                $correo=$row;?> 
                                            <option value="<?php echo $correo->id;?>"><?php echo $correo->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>       




                                            <div class="form-group">
                                        <label>Tipo Telefono<span style="color: red;">*</span></label>                                        
                                            <select class="select2_demo_3 form-control required m-b" name="tipotelf" id="tipotelf">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/persona.php';
                                                foreach($this->telefono_tipo as $row){
                                                $telefono=new Persona();
                                                $telefono=$row;?> 
                                            <option value="<?php echo $telefono->id;?>"><?php echo $telefono->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>   

                                           

                                            

                            <div class="form-group row">
                            <label class="col-sm-8 col-form-label">¿Posee alguna discapacidad? <span style="color: red;">*</span> </label>

                            <div class="i-checks col-sm-15"  >                                
                            <label>Si
                            <input type="radio"  value="Si" id="optionsRadios1" name="optionsRadios" onChange="DiscapacidadOnChange(this)">
                            </label> 
                            <label class="i-checks checkbox-inline" >No                                
                            <input type="radio" value="No" id="optionsRadios2" name="optionsRadios" onChange="DiscapacidadOnChange(this)">
                            </label>

                            </div>
                            </div>
                    <!-- <script>
                        $(document).ready(function () {
                            $('.i-checks').iCheck({
                                checkboxClass: 'icheckbox_square-green',
                                radioClass: 'iradio_square-green',
                            });
                        });
                    </script>-->

               <!-- para ocultar o mostrar discapacidades -->
               <script type="text/javascript">
                    function DiscapacidadOnChange(sel) {
                        console.log(sel);
                        if (sel.value=="No"){
                            divC = document.getElementById("seccion_Observaciones");
                            divC.style.display = "";
                            divC = document.getElementById("seccion_Tipo_Discapacidad");
                            divC.style.display = "";
                            divC = document.getElementById("seccion_Discapacidad");
                            divC.style.display = "";

                            divT = document.getElementById("seccion_inputs_Observaciones");
                            divT.style.display = "none";

                            divT = document.getElementById("seccion_inputs_Tipo_Discapacidad");
                            divT.style.display = "none";

                            divT = document.getElementById("seccion_inputs_Discapacidad");
                            divT.style.display = "none";

                            divT = document.getElementById("seccion_inputs_Codigo");
                            divT.style.display = "none";

                            
                            myElemento7 = document.getElementById("conadis");
                            myElemento7.classList.remove('required');

                            myElemento3 = document.getElementById("tdiscapacidad0");
                            myElemento3.classList.remove('required');
                            myElemento4 = document.getElementById("discapacidad0");
                            myElemento4.classList.remove('required');
                            myElemento5 = document.getElementById("observaciones0");
                            myElemento5.classList.remove('required');


                        //aqui removemos los requires de discapacidad 1
                            myElemento = document.getElementById("tdiscapacidad1");
                            myElemento.classList.remove('required');
                            myElemento2 = document.getElementById("discapacidad1");
                            myElemento2.classList.remove('required');
                            myElemento3 = document.getElementById("observaciones1");
                            myElemento3.classList.remove('required');
                        }else{

                            divC = document.getElementById("seccion_Observaciones");
                            divC.style.display="none";

                            divC = document.getElementById("seccion_Tipo_Discapacidad");
                            divC.style.display = "none";

                            divC = document.getElementById("seccion_Discapacidad");
                            divC.style.display = "none";


                            divT = document.getElementById("seccion_inputs_Observaciones");
                            divT.style.display = "";
                            divT = document.getElementById("seccion_inputs_Tipo_Discapacidad");
                            divT.style.display = "";
                            divT = document.getElementById("seccion_inputs_Discapacidad");
                            divT.style.display = "";
                            divT = document.getElementById("seccion_inputs_Codigo");
                            divT.style.display = "";
                      
                      //agrega clase required 
                            myElemento7 = document.getElementById("conadis");
                            myElemento7.classList.add('required');

                            myElemento3 = document.getElementById("tdiscapacidad0");
                            myElemento3.classList.add('required');
                            myElemento4 = document.getElementById("discapacidad0");
                            myElemento4.classList.add('required');
                            myElemento5 = document.getElementById("observaciones0");
                            myElemento5.classList.add('required');


                        }
                  
                    

                    }
                    </script>




                  </div>

                                        <div class="col-lg-6">
                                       
                                        <div class="form-group">
                                        <label>Correo <span style="color: red;">*</span></label>
                                        <input id="correo" name="correo" type="email" class="form-control required" maxlength='49' minlength="7" Placeholder="Escriba su correo">
                                          </div>


                                        <div class="form-row">
                                        <div class="form-group col-md-4">
                                        <label> Cod. Area<span style="color: red;">*</span></label>  
                                        <select class="select2_demo_3 form-control required m-b" name="cod_area" id="cod_area">                                    
                                            <option selected="selected" value="">----- </option> 
                                            <?php include_once 'models/persona.php';
                                                foreach($this->cod_area_telefono as $row){
                                                $cod_area=new Persona();
                                                $cod_area=$row;?> 
                                            <option value="<?php echo $cod_area->id;?>"><?php echo $cod_area->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>                                           </div>
                                        <div class="form-group col-md-8">
                                        <label> Telefono <span style="color: red;">*</span></label>  
                                            <input name="numero" minlength="7" maxlength="15" placeholder="Escriba su numero" id="numero"  onkeypress="return valSoloNumeros(event)"  class="form-control required " aria-required="true" type="text" placeholder="Escriba su numero de Telefono">
                                      
                                        </div>                                        
                                    </div>

 
                                    <div class="form-group"  id="seccion_inputs_Codigo" style="display:;">
                                        <label>Codigo Conadis</label>
                                        <input id="conadis" name="conadis" type="text" class="form-control " maxlength='10' minlength="4" onkeypress="return valSoloNumeros(event)" placeholder="Escriba su codigo conadis">
                                   </div>  
          
                                        </div>
                                    </div>



                <div>
                    <div class="form-row">

                    <div class="form-group col-md-6">
                    <div id="seccion_Discapacidad" style="display:none;"></div>
                    <div class="form-group"  id="seccion_inputs_Tipo_Discapacidad" style="display:;">
                    <label>Tipo de discapacidad</label>                                        
                    <select  class="select2_demo_3 form-control  m-b " name="tdiscapacidad" id="tdiscapacidad0">                                    
                    <option selected="selected" value="">Seleccione </option> 
                    </select> 
                    </div>                                                            
                    </div>


                    <div class="form-group col-md-6">                                 
                    <div id="seccion_Tipo_Discapacidad" style="display:none;"></div>
                    <div class="form-group "  id="seccion_inputs_Discapacidad" style="display:;">
                    <label>Discapacidad</label>                                        
                    <select class="select2_demo_3 form-control m-b" name="discapacidad" id="discapacidad0">                                    
                    <option selected="selected" value="">Seleccione </option> 
                    </select>   
                    </div>   
                    </div>
                    
                   <!-- <button type="button" onclick="nuevo();">Agregar</button> -->                     
                  </div>
               
                                <div id="seccion_Observaciones" style="display:none;"></div>
                                <div class="form-group"  id="seccion_inputs_Observaciones" style="display:;">
                                <label>Observaciones</label>
                                <textarea id="observaciones0" name="observaciones" type="text" class="form-control" placeholder="Escriba una observación..." aria-required="true" aria-invalid="false" onkeypress="return soloLetras(event)"  maxlength='200' minlength="10"></textarea>
                              
                                <a style='cursor: pointer;' onClick="muestra_oculta('contenido')" title="" class="boton_mostrar">Agregar discapacidad / Ocultar</a>
                                <br><br>
                            
                 




          <!-- ////////////////////////////Div de Otra discapacidad//////////////////////////////////////////////7-->
            <div id="contenido" >
                    <div class="form-row">

                    <div class="form-group col-md-6">
                    <div id="seccion_Discapacidad1" style="display:none;"></div>
                    <div class="form-group"  id="seccion_inputs_Tipo_Discapacidad1" style="display:;">
                    <label>Tipo de discapacidad</label>                                        
                    <select  class="select2_demo_3 form-control  m-b " name="tdiscapacidad1" id="tdiscapacidad1">                                    
                    <option selected="selected" value="">Seleccione </option> 
                    </select> 
                    </div>                                                            
                    </div>


                    <div class="form-group col-md-6">                                 
                    <div id="seccion_Tipo_Discapacidad1" style="display:none;"></div>
                    <div class="form-group "  id="seccion_inputs_Discapacidad1" style="display:;">
                    <label>Discapacidad</label>                                        
                    <select class="select2_demo_3 form-control m-b" name="discapacidad1" id="discapacidad1">                                    
                    <option selected="selected" value="">Seleccione </option> 
                    </select>   
                    </div>   
                    </div>
              
              
                </div>
                                 <div id="seccion_Observaciones1" style="display:none;"></div>
                                <div class="form-group"  id="seccion_inputs_Observaciones1" style="display:;">
                                <label>Observaciones</label>
                                <textarea id="observaciones1" name="observaciones1" type="text" class="form-control" placeholder="Escriba una observación..." aria-required="true" aria-invalid="false" onkeypress="return soloLetras(event)"  maxlength='200' minlength="10"></textarea>
                                </div>

            </div>

            <script>

function muestra_oculta(id){
if (document.getElementById){ //se obtiene el id
var el = document.getElementById(id); //se define la variable "el" igual a nuestro div

el.style.display = (el.style.display == 'none') ? 'block' : 'none'; //damos un atributo display:none que oculta el div
//agregamos clases required en caso de que el ancla sea mostrado
        if(el.style.display=='block'){
                    myElemento = document.getElementById("tdiscapacidad1");
                    myElemento.classList.add('required');
                    myElemento2 = document.getElementById("discapacidad1");
                    myElemento2.classList.add('required');
                    myElemento3 = document.getElementById("observaciones1");
                    myElemento3.classList.add('required');
        }else{
                    myElemento = document.getElementById("tdiscapacidad1");
                    myElemento.classList.remove('required');
                    myElemento2 = document.getElementById("discapacidad1");
                    myElemento2.classList.remove('required');
                    myElemento3 = document.getElementById("observaciones1");
                    myElemento3.classList.remove('required');

        }
 //para aumentar tamaño del fieldset
 divC = document.getElementById("ver");
 divC.style="775px";
}


}
window.onload = function(){/*hace que se cargue la función lo que predetermina que div estará oculto hasta llamar a la función nuevamente*/
muestra_oculta('contenido');/* "contenido_a_mostrar" es el nombre que le dimos al DIV */

}


</script>

                <!-- estos div son de onchange para ocultar los-->
           </div>
                             
                             </div>

          <!-- //////////////////////////////////////////////////////////////////////////7-->

                            <!-- combo select tipo discapacidad,discapacidad -->
                            <script>
                                $(function(){

                            // Lista de tipos de discapacidad
                            $.post( '<?php echo constant ('URL');?>models/combodiscapacidad.php' ).done( function(respuesta)
                            {
                                $( '#tdiscapacidad0' ).html( respuesta );
                            });


                            // lista de discapacidad	
                            $('#tdiscapacidad0').change(function()
                            {
                                var id_tipo_discapacidad = $(this).val();
                                
                                // Lista de estados
                                $.post( '<?php echo constant ('URL');?>models/combodiscapacidad.php', { discapacidad: id_tipo_discapacidad} ).done( function( respuesta )
                                {
                                    $( '#discapacidad0' ).html( respuesta );
                                });
                            });




                    // div otra Lista de tipos de discapacidad
                    $.post( '<?php echo constant ('URL');?>models/combodiscapacidadd.php' ).done( function(respuesta)
                                                {
                                $( '#tdiscapacidad1' ).html( respuesta );
                            });


                            // lista de discapacidad	
                            $('#tdiscapacidad1').change(function()
                            {
                                var id_tipo_discapacidad1 = $(this).val();
                                
                                // Lista de estados
                                $.post( '<?php echo constant ('URL');?>models/combodiscapacidadd.php', { discapacidad1: id_tipo_discapacidad1} ).done( function( respuesta )
                                {
                                    $( '#discapacidad1' ).html( respuesta );
                                });
                            });

                    ////////////////////////////////////////////

                            })
                                </script>

                                    <!-- fin otro perfil  parte 4-->

                               </fieldset>
                                <h1 class="deleteAca">Perfil</h1>
                                 <fieldset class="deleteAca">
                                 
                                 
                                 <h2 class="otro">Perfil de usuario</h2>
                                    <p  class="otro">
                                        &nbsp;En esta sección podra agregar un tipo de usuario y los privilegios que este tendra dentro del sistema.
                                        </p>
                                    <div class="row otro">
                                    
                                        <div class="col-lg-6">

                                       <!-- <div class="form-group">
                                        <label>Perfil<span style="color: red;">*</span></label>
                                        <input id="perfil" name="perfil" type="text" class="form-control required" maxlength='30' minlength="3" Placeholder="Escriba un tipo de usuario">
                                         </div>-->     


                                         <div class="form-group">
                                            <label>Perfil<span style="color: red;">*</span></label>                                        
                                            <select  name="perfil" id="perfil"  class="select2_demo_3 form-control required m-b">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/persona.php';
                                                foreach($this->perfiles as $row){
                                                $perfil=new Persona();
                                                $perfil=$row;?> 
                                            <option value="<?php echo $perfil->id;?>"><?php echo $perfil->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>


                                        
                                         <div class="form-group">
                                         <label>Roles <span style="color: red;">*</span></label>
                                         <select  name="roles[]" id="roles"   data-placeholder="Seleccione sus roles" class="chosen-select form-control required m-b" multiple style="width:350px;" tabindex="4">
                                            <?php include_once 'models/persona.php';
                                                foreach($this->roles as $row){
                                                $rol=new Persona();
                                                $rol=$row;?> 
                                            <option value="<?php echo $rol->id;?>"><?php echo $rol->descripcion;?></option>
                                            <?php }?>             
                                         </select>                                              
                                         </div>

                                        </div>
                                        <div class="col-lg-6">
                                                                     
                                            <div class="form-group">
                                            <label>Estatus </label>
                                            <select class="select2_demo_3 form-control required m-b " name="estatus" id="estatus">                                      
                                            <option value="0" >Inactivo</option>
                                            <option value ="1"selected="selected">Activo</option>                                               
                                            </select>                                              
                                            </div>

                                          
                                        </div>
                                    </div>

                                    
                                </fieldset>

                                
                               </form>

           
                        </div>

                        
                    </div>

                    
                    </div>
                    

                </div>
                
                </div>



        <?php require 'views/footer.php'; ?>

        
    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>
    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    
    <!-- Chosen -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>

    <!-- Sweet alert -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/sweetalert/sweetalert.min.js"></script>

   <!-- Input Mask-->
   <script src="<?php echo constant ('URL');?>src/js/plugins/jasny/jasny-bootstrap.min.js"></script>

       <!-- Select2 -->
       <script src="<?php echo constant ('URL');?>src/js/plugins/select2/select2.full.min.js"></script>

    <!-- Data picker -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>

    <!-- Typehead -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>

    

                <script type="text/javascript">
                    //muestra un form u otro dependiendo del tipo de estudio
                    function mostrar(id) {

                        
                        switch(id){
                            case "1"://1=Trabajador academico
                                //mostrar, agregar
                                $(".academico2").show();
                                $("#programa").addClass('required');
                                $("#eje_regional").addClass('required');
                                $("#centro_estudio").addClass('required');
                                $("#form").attr('action', '<?php echo constant('URL') . "login/registrar"; ?>');
                                
                                //ocultar, eliminar
                                $(".otro").hide();
                                $("fieldset").remove(".deleteAca");
                                $("h1").remove(".deleteAca");
                                break;

                            default:
                                //mostrar, agregar
                                $(".otro").show();
                                $("#form").attr('action', '<?php echo constant('URL') . "usuarios/RegistarUsuario"; ?>');

                                //ocultar, eliminar
                                $(".academico2").hide();
                                $("#programa").removeClass('required');
                                $("#eje_regional").removeClass('required');
                                $("#centro_estudio").removeClass('required');

                        }

                    }

                    //muestra un form u otro dependiendo de la respuesta SI/NO en cada uno de los casos 
                    function mostrar2(id_radio){

                        

                        switch(id_radio){
                            case "si-etnia":
                                $("#wetnia").show();
                                $("#etnia").addClass('required');
                                break;
                            case "no-etnia":
                                $("#wetnia").hide();
                                $("#etnia").removeClass('required');
                            case "si-disc":
                                $("#wdisc").show();
                                $("#discapacidad").addClass('required');
                                break;
                            case "no-disc":
                                $("#wdisc").hide();
                                $("#discapacidad").removeClass('required');
                                break;
                        }

                        


                    }
                </script>
       

   
    <!-- form step-->
    <script>


        $(document).ready(function(){

            
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                   
                   
                    ////////////////////////////////////////       
                    $('.chosen-select').chosen({width: "100%"});

                    $(".select2_demo_3").select2({
                        placeholder: "Seleccione",
                        width: "100%",
                        dropdownAutoWidth: true,
                        allowClear: true
                    });
                    var mem = $('#data_1 .input-group.date').datepicker({
                        todayBtn: "linked",
                        keyboardNavigation: false,
                        forceParse: false,
                        calendarWeeks: true,
                        autoclose: true
                    });
                    ////////////////////////////////////////   


                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            
                                ano_r: {

                                    esfecha: true

                                }
                        }
                       
                    });
       });
       
       $.validator.addMethod("esfecha", esFechaActual, "El Año de Realización no debe ser mayor al Año Actual");


        function esFechaActual(value, element, param) {

            
            var fechaActual = <?php echo date('Y');?>;

            if (value > fechaActual) {

                return false; //error de validación

            }

            else {

                return true; //supera la validación

            }

        }

    </script>

   

</body>
</html>
