<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Registrar Usuario | SIDTA</title>


    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">
  
    
    <!-- jasny input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <!--  style select2 -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/select2/select2.min.css" rel="stylesheet">
    <!-- datapicker -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <!-- sweetalert input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- chosen -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">

</head>

<body>
 
    <div id="wrapper">
   <?php require 'views/header.php'; ?>
   

        <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-9">
                    <h2>Registrar Usuario</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>home">Inicio</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>usuarios">Listar Usuarios</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <a href="<?php echo constant ('URL');?>usuarios/viewAdd"><strong>Agregar</strong></a>
                        </li>
                    </ol>
                </div>
                <div class="col-sm-3">
                    <div class="title-action">
                        <a href="<?php echo constant ('URL');?>usuarios" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Volver</a>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5><i class="fa fa-angle-double-right"></i> SIDTA <i class="fa fa-angle-double-left"></i></h5>
                        </div>
                        <div class="ibox-content">
                            <h2>
                                Agregar Usuario a SIDTA
                            </h2>
                            <p>
                                Completa el formulario para registrar un Usuario dentro de SIDTA.
                            </p>
                            <?php echo $this->mensaje;?>
                            <div class="alert alert-info">Todos los campos marcados con un (<span style="color: red;">*</span>) Son Obligatorios</div>

                            <form class="m-t" action="<?php echo constant ('URL');?>usuarios/addUser" role="form" id="form" method="post">
                            <h1>Nacionalidad</h1>
                                <fieldset>
                                    <h2>Nacionalidad</h2>
                                    <div class="msj" class="alert"></div>
                                    <div class="row">
                                        
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Nro. de Identificación <span style="color: red;">*</span></label>
                                                <input type="text" minlength="5" maxlength="10" name="ndocumento" id="ndocumento" class="form-control required number" placeholder="Ingrese su nro de Identificación (Cédula, Pasaporte, etc.)">
                                                <span><i>(Cédula, Pasaporte, etc.)</i> </span>
                                            </div>
                                        </div>
                                        
                                        <script>
                                            $('#ndocumento').change(function()
                                            {
                                                var cedula = $(this).val();
                                                
                                                $.post( '<?php echo constant ('URL');?>usuarios/nomina', { ndocumento: cedula} ).done( function( respuesta )
                                                {
                                                    //console.log(respuesta);
                                                    var bandera=respuesta.trim();
                                                    
                                                    if(bandera=='false'){
                                                        $("#form").trigger("reset");
                                                        $(".msj").text("La Cédula no Existe en la Nómina Docente de SIGAD. No se Logro Autocompletar la Información.");
                                                        $(".msj").removeClass("alert-success");
                                                        $(".msj").addClass("alert-warning");
                                                        $('#ndocumento').val(cedula);

                                                    }else{
                                                        $("#form").trigger("reset");
                                                        $(".msj").text("La Cédula del Docente Existe en la Nómina de SIGAD. Se han autocompletado algunos datos, por favor Verifique.");
                                                        $(".msj").removeClass("alert-warning");
                                                        $(".msj").addClass("alert-success");

                                                        var arrayDeCadenas = respuesta.split("|");
                                                        $('#estadocivil').val(arrayDeCadenas[0].trim());
                                                        $('#genero').val(arrayDeCadenas[1].trim());
                                                        $('#tdocumento').val(arrayDeCadenas[2].trim());
                                                        $('#nacionalidad').val(arrayDeCadenas[3].trim());
                                                        $('#pnombre').val(arrayDeCadenas[4].trim());
                                                        $('#snombre').val(arrayDeCadenas[5].trim());
                                                        $('#papellido').val(arrayDeCadenas[6].trim());
                                                        $('#sapellido').val(arrayDeCadenas[7].trim());
                                                        $('#fnac').val(arrayDeCadenas[8].trim());
                                                        $('#paisnac').val(arrayDeCadenas[9].trim());
                                                        $('#cod_area').val(arrayDeCadenas[10].trim());
                                                        $('#numero').val(arrayDeCadenas[11].trim());
                                                        $('#tcorreo').val(arrayDeCadenas[12].trim());
                                                        $('#correo').val(arrayDeCadenas[13].trim());
                                                        $('#nhijos').val(arrayDeCadenas[14].trim());
                                                        $('#fingreso').val(arrayDeCadenas[15].trim());
                                                        $('#dedicacion').val(arrayDeCadenas[16].trim());
                                                        $('#estatus_d').val(arrayDeCadenas[17].trim());
                                                        $('#clasificacion').val(arrayDeCadenas[18].trim());
                                                        $('#escalafon').val(arrayDeCadenas[19].trim());
                                                        $('#ndocumento').val(arrayDeCadenas[20].trim());
                                                        $('#direccion').val(arrayDeCadenas[21].trim());

                                                    }
                                                    
                                                });
                                            });
                                        </script>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Estado Civil <span style="color: red;">*</span></label>                                        
                                                <select  name="estadocivil" id="estadocivil"  style="width: 100%;" class="form-control required m-b">                                    
                                                    <option value="">Seleccione </option> 
                                                    <?php include_once 'models/persona.php';
                                                        foreach($this->estadosCivil as $row){
                                                        $estado=new Persona();
                                                        $estado=$row;?> 
                                                    <option value="<?php echo $estado->id;?>"><?php echo $estado->descripcion;?></option>
                                                <?php }?>                                            
                                                </select>   
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Tipo de Documento <span style="color: red;">*</span></label>                                        
                                                <select  name="tdocumento" id="tdocumento"  style="width: 100%;" class="form-control required m-b">                                    
                                                    <option selected="selected" value="">Seleccione </option> 
                                                    <?php include_once 'models/persona.php';
                                                        foreach($this->documentoidentidad as $row){
                                                        $documento=new Persona();
                                                        $documento=$row;?> 
                                                    <option value="<?php echo $documento->id;?>"><?php echo $documento->descripcion;?></option>
                                                <?php }?>             
                                                </select>   
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Género<span style="color: red;">*</span></label>                                        
                                                <select  style="width: 100%;" class="form-control required m-b" name="genero" id="genero">                                    
                                                    <option selected="selected" value="">Seleccione</option> 
                                                    <?php include_once 'models/persona.php';
                                                        foreach($this->generos as $row){
                                                        $genero=new Persona();
                                                        $genero=$row;?> 
                                                    <option value="<?php echo $genero->id;?>"><?php echo $genero->descripcion;?></option>
                                                <?php }?>                                            
                                                </select>   
                                            </div>
                                            <div class="form-group">
                                                <label>Nacionalidad <span style="color: red;">*</span></label>     
                                                <select class="form-control required m-b" name="nacionalidad" id="nacionalidad">        
                                                <option selected="selected" value="">Seleccione </option> 
                                                <?php include_once 'models/persona.php';
                                                    foreach($this->nacionalidades as $row){
                                                    $nacionalidad=new Persona();
                                                    $nacionalidad=$row;?> 
                                                <option value="<?php echo $nacionalidad->id_nacionalidad;?>"><?php echo $nacionalidad->descripcion;?></option>
                                                <?php }?>
                                                </select> 
                                            </div> 
                                        </div>
                                        
                                    </div>
                                </fieldset>
                                <h1>Datos de la Persona</h1>
                                <fieldset>
                                    <h2>Datos Personales</h2>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Primer Nombre <span style="color: red;">*</span></label>
                                                <input id="pnombre" name="pnombre" type="text" class="form-control required" maxlength='29' minlength="3" placeholder="Ingrese el Primer Nombre">
                                            </div>
                                            <div class="form-group">
                                                <label>Primer Apellido <span style="color: red;">*</span></label>
                                                <input id="papellido" name="papellido" type="text" class="form-control required" maxlength='29' minlength="3" placeholder="Ingrese el Primer Apellido">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Segundo Nombre <i>(Opcional)</i></label>
                                                <input id="snombre" name="snombre" type="text" class="form-control" maxlength='29' minlength="3" placeholder="Ingrese el Segundo Nombre">
                                                </div>  
                                                <div class="form-group">
                                                <label>Segundo Apellido <i>(Opcional)</i></label>
                                                <input id="sapellido" name="sapellido" type="text" class="form-control" maxlength='29' minlength="3"  onkeypress="return soloLetras(event)" placeholder="Escriba su Segundo Apellido">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            
                                            <div class="form-group" id="data_1">
                                                <label class="font-normal">Fecha de Nacimiento <span style="color: red;">*</span><label id="fnac-error" class="error" for="fnac"></label></label>
                                                <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <input type="text" name="fnac" id="fnac" class="form-control required" placeholder="mm/dd/aaaa">
                                                </div>
                                                
                                                <span><i>mm/dd/aaaa</i></span>
                                                
                                            </div>
                                            <script>
                                                var mem = $('#data_1 .input-group.date').datepicker({
                                                    todayBtn: "linked",
                                                    keyboardNavigation: false,
                                                    forceParse: false,
                                                    calendarWeeks: true,
                                                    autoclose: true
                                                });
                                            </script>
                                            
                                        </div>
                                        <div class="col-lg-6">
                                            
                                            <div class="form-group">
                                                <label>País de Nacimiento <span style="color: red;">*</span></label>                                        
                                                <select class="form-control select2_demo_3 required m-b" name="paisnac" id="paisnac">                                    
                                                <option selected="selected" value="">Seleccione </option>
                                                <?php 
                                                        foreach($this->paises as $row){
                                                        $pais=new Estructura();
                                                        $pais=$row;?> 
                                                    <option value="<?php echo $pais->id_pais;?>"><?php echo $pais->descripcion;?></option>
                                                    <?php }?>    
                                                </select> 
                                                  
                                            </div>
                                           
                                            <script>
                                                $(".select2_demo_3").select2({
                                                    placeholder: "Seleccione",
                                                    width: "100%",
                                                    dropdownAutoWidth: true,
                                                    allowClear: true
                                                });
                                            </script>
                                            
                                        </div>
                                    </div>
                                    
                                </fieldset> 
                                 
                                <h1>Contacto</h1>
                                <fieldset>
                                    <h2>Datos de Contacto</h2>
                                    <div class="msj" class="alert"></div>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Tipo de Telefono <span style="color: red;">*</span></label>                                        
                                                    <select class="select2_demo_3 form-control required m-b" name="tipotelf" id="tipotelf">                                    
                                                    <option selected="selected" value="">Seleccione </option> 
                                                    <?php include_once 'models/persona.php';
                                                        foreach($this->telefono_tipo as $row){
                                                        $telefono=new Persona();
                                                        $telefono=$row;?> 
                                                    <option value="<?php echo $telefono->id;?>"><?php echo $telefono->descripcion;?></option>
                                                    <?php }?>                                            
                                                </select>   
                                            </div>   
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label> Código de Área<span style="color: red;">*</span></label>  
                                                <select class="form-control required m-b" name="cod_area" id="cod_area">                                    
                                                    <option selected="selected" value="">----- </option> 
                                                    <?php include_once 'models/persona.php';
                                                        foreach($this->cod_area_telefono as $row){
                                                        $cod_area=new Persona();
                                                        $cod_area=$row;?> 
                                                    <option value="<?php echo $cod_area->id;?>"><?php echo $cod_area->descripcion;?></option>
                                                    <?php }?>                                            
                                                </select>                                        </div>

                                      <!--  <div class="form-group">
                                        <label>Tipo de Correo <span style="color: red;">*</span></label>                                        
                                            <select class="form-control required m-b" name="tcorreo" id="tcorreo">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/persona.php';
                                                foreach($this->tcorreos as $row){
                                                $correo=new Persona();
                                                $correo=$row;?> 
                                            <option value="<?php echo $correo->id;?>"><?php echo $correo->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>
                                            </div>-->

                                        

   

                                                <div class="form-group">
                                            <label>Correo Personal <span style="color: red;">*</span></label> 
                                            <input id="correo_personal"  name="correo_personal" type="email" class="form-control mail required" maxlength='49' minlength="7" Placeholder="Ingrese el Correo Electrónico Personal">  
                                            </div>  


                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label> Telefono <span style="color: red;">*</span></label>  
                                                <input name="numero" minlength="7" maxlength="7" placeholder="Ingrese el Número de Teléfono" id="numero"   class="form-control number required " aria-required="true" type="text">
                                            </div> 
                                          <!--  <div class="form-group">
                                                <label>Correo Electrónico<span style="color: red;">*</span></label>
                                                <input id="correo" name="correo" type="email" class="form-control mail required" maxlength='49' minlength="7" Placeholder="Ingrese el Correo Electrónico">
                                            </div>-->

                                            <div class="form-group">
                                                <label>Correo Institucional <i>(Opcional)</i></label>
                                                <input id="correo_institucional"  name="correo_institucional" type="email" class="form-control mail" maxlength='49' minlength="7" Placeholder="Ingrese el Correo Electrónico Institucional">
                    
                                            </div>

                                        </div>
                                    </div>
                                </fieldset> 
                                <h1>Domicilio</h1>
                                <fieldset>
                                    <h2>Domicilio</h2>
                                    <div class="msj" class="alert"></div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>País donde Reside <span style="color: red;">*</span></label>                                        
                                                <select class="select2_demo_3 form-control required m-b" name="pais" id="pais">                                    
                                                <option selected="selected" value="">Seleccione </option>
                                                <?php 
                                                        foreach($this->paises as $row){
                                                        $pais=new Estructura();
                                                        $pais=$row;?> 
                                                    <option value="<?php echo $pais->id_pais;?>"><?php echo $pais->descripcion;?></option>
                                                    <?php }?>    
                                                </select> 
                                                
                                            </div>
                                            <div class="form-group">
                                                <label>Municipio donde Reside <span style="color: red;">*</span></label>

                                                <select name="municipio" style="width: 100%;" id="municipio" class="form-control required">
                                                    <option value="">Seleccione el municipio donde habita</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Estado donde Reside <span style="color: red;">*</span></label>
                                                <select name="estado" style="width: 100%;" id="estado" class="form-control  required">
                                                    <option value="">Seleccione el Estado donde habita</option>
                                                </select>
                                                
                                            </div>
                                            <div class="form-group">
                                                <label>Parroquia donde Reside <span style="color: red;">*</span></label>
                                                <select name="parroquia" style="width: 100%;" id="parroquia" class="form-control required">
                                                    <option value="">Seleccione la parroquia donde reside</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Tipo de Domicilio <span style="color: red;">*</span></label>                                        
                                                <select  name="tdomicilio" id="tdomicilio"  class="select2_demo_3 form-control required m-b">                                    
                                                    <option selected="selected" value="">Seleccione </option> 
                                                    <?php 
                                                        foreach($this->domicilios as $row){
                                                        $domicilio=new Estructura();
                                                        $domicilio=$row;?> 
                                                    <option value="<?php echo $domicilio->id;?>"><?php echo $domicilio->descripcion;?></option>
                                                    <?php }?>                                            
                                                </select>   
                                            </div>
                                            <div class="form-group">
                                                <label>Dirección<span style="color: red;">*</span></label>
                                                <textarea id="direccion" name="direccion" type="text" class="form-control required" placeholder="Escriba una dirección..." aria-required="true" aria-invalid="false"  maxlength='150' minlength="4"></textarea>
                                                        
                                                                
                                                            
                                            </div>
                                        </div>
                                        <script>
                                            $('#pais').change(function(){
                                                var id_pais = $(this).val();
                                                
                                                $.post( '<?php echo constant ('URL');?>usuarios/getEstadosPais', { pais: id_pais} ).done( function( respuesta )
                                                {
                                                    $( '#estado' ).html( respuesta );
                                                });
                                            });

                                            // lista de MUNICIPIOS	
                                            $('#estado').change(function()
                                            {
                                                var id_estado = $(this).val();
                                                
                                                // Lista de estados
                                                $.post( '<?php echo constant ('URL');?>usuarios/getMunicipiosEstado', { estado: id_estado} ).done( function( respuesta )
                                                {
                                                    $( '#municipio' ).html( respuesta );
                                                });
                                            });

                                            // lista de PARROQUIAS	
                                            $('#municipio').change(function()
                                            {
                                                var id_municipio = $(this).val();
                                                
                                                // Lista de estados
                                                $.post( '<?php echo constant ('URL');?>usuarios/getParroquiasMunicipio', { municipio: id_municipio} ).done( function( respuesta )
                                                {
                                                    $( '#parroquia' ).html( respuesta );
                                                });
                                            });
                                        </script>
                                        
                                    </div>
                                </fieldset> 
                                <h1>Datos de Interes</h1>
                                <fieldset>
                                    <h2>Datos de Interes</h2>
                                    <div class="msj" class="alert"></div>

                                    <div class="row">
                                    <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Número de Hijos <span style="color: red;">*</span></label>  
                                                <input name="nhijos" placeholder="Ingrese el Número de Hijos" id="nhijos" class="form-control number required " aria-required="true" type="number">
                                            </div> 
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group" id="data_2">
                                                <label class="font-normal">Fecha de Ingreso a la UBV <span style="color: red;">*</span><label id="fingreso-error" class="error" for="fingreso"></label></label>
                                                <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <input type="text" name="fingreso" id="fingreso" class="form-control required" placeholder="mm/dd/aaaa">
                                                </div>
                                                
                                                <span><i>mm/dd/aaaa</i></span>
                                                
                                            </div>
                                            <script>
                                                var mem = $('#data_2 .input-group.date').datepicker({
                                                    todayBtn: "linked",
                                                    keyboardNavigation: false,
                                                    forceParse: false,
                                                    calendarWeeks: true,
                                                    autoclose: true
                                                });
                                            </script>
                                        </div>
                                        <div class="col-lg-6">
                                           
                                            <div class="form-group">
                                            <label>Etnia Indígena <i>(Opcional)</i></label>                                        
                                                <select class="select2_demo_3 form-control m-b" name="etnia" id="etnia">                                    
                                                    <option value="">Seleccione </option> 
                                                    <?php include_once 'models/estructura.php';
                                                        foreach($this->etnias as $row){
                                                        $etnia=new Estructura();
                                                        $etnia=$row;?> 
                                                    <option value="<?php echo $etnia->id;?>"><?php echo $etnia->descripcion;?></option>
                                                    <?php } ?>    
                                                </select>   
                                            </div> 
                                        </div>
                                        <div class="col-lg-6">
                                        
                                            <div class="form-group">
                                                <label>¿Posee alguna discapacidad? <span style="color: red;">*</span> </label>
                                                
                                                <div class="i-checks"><label> <input type="radio" value="si" checked="" onChange="mostrar2(this.value);"  name="pdisc"> <i></i> - Si </label></div>
                                                <div class="i-checks"><label> <input type="radio"  value="no" onChange="mostrar2(this.value);"  name="pdisc"> <i></i> - No </label></div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div id="wdiscapacidades" class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Tipo de discapacidad <span style="color: red;">*</span></label>                                        
                                                <select  class="select2_demo_3 form-control required m-b " name="tdiscapacidad" id="tdiscapacidad0">              
                                                    <option selected="selected" value="">Seleccione </option>
                                                    <?php 
                                                        foreach($this->tipo_discapacidades as $row){
                                                        $tipo_discapacidad=new Estructura();
                                                        $tipo_discapacidad=$row;?> 
                                                    <option value="<?php echo $tipo_discapacidad->id;?>"><?php echo $tipo_discapacidad->descripcion;?></option>
                                                    <?php }?> 
                                                </select> 
                                            </div> 
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Discapacidad <span style="color: red;">*</span></label>                                        
                                                <select class="required form-control m-b" name="discapacidad" id="discapacidad0">                                    
                                                <option selected="selected" value="">Seleccione </option> 
                                                </select>   
                                            </div>  
                                            
                                        </div>
                                        <script>
                                            $('#tdiscapacidad0').change(function(){
                                                var id_tipo_discapacidad = $(this).val();
                                
                                                // Lista de estados
                                                $.post( '<?php echo constant ('URL');?>usuarios/getDiscapacidadesTipo', { tipo_discapacidad: id_tipo_discapacidad} ).done( function( respuesta )
                                                {
                                                    $( '#discapacidad0' ).html( respuesta );
                                                });
                                            });
                                        </script>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Código CONAPDIS</label>
                                                <input id="conadis" name="conadis" type="text" class="form-control number" maxlength='10' minlength="4" placeholder="Ingrese el Código CONAPDIS">
                                            </div>  
                                            <div class="form-group">
                                                <label>Observaciones</label>
                                                <textarea id="observaciones0" name="observaciones" type="text" class="form-control" placeholder="Escriba una observación..." maxlength='200' minlength="10"></textarea>
                                                <a style='cursor: pointer;' onClick="muestra_oculta('contenido')" title="" class="boton_mostrar">Agregar discapacidad / Ocultar</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ////////////////////////////Div de Otra discapacidad//////////////////////////////////////////////7-->
                                    <div id="contenido" >
                                        <div class="form-row">

                                            <div class="form-group col-md-6">
                                            <div id="seccion_Discapacidad1" style="display:none;"></div>
                                            <div class="form-group"  id="seccion_inputs_Tipo_Discapacidad1" style="display:;">
                                            <label>Tipo de discapacidad</label>                                        
                                            <select  class="select2_demo_3 form-control  m-b " name="tdiscapacidad1" id="tdiscapacidad1">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                                    <option selected="selected" value="">Seleccione </option>
                                                    <?php 
                                                        foreach($this->tipo_discapacidades as $row1){
                                                        $tipo_discapacidad1=new Estructura();
                                                        $tipo_discapacidad1=$row1;?> 
                                                    <option value="<?php echo $tipo_discapacidad1->id;?>"><?php echo $tipo_discapacidad1->descripcion;?></option>
                                                    <?php }?> 
                                            </select> 
                                            </div>                                                            
                                            </div>


                                            <div class="form-group col-md-6">                                 
                                            <div id="seccion_Tipo_Discapacidad1" style="display:none;"></div>
                                            <div class="form-group "  id="seccion_inputs_Discapacidad1" style="display:;">
                                            <label>Discapacidad</label>                                        
                                            <select class="form-control m-b" name="discapacidad1" id="discapacidad1">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            </select>   
                                            </div>   
                                            </div>
                                            <script>
                                                $('#tdiscapacidad1').change(function(){
                                                    var id_tipo_discapacidad = $(this).val();
                                    
                                                    // Lista de estados
                                                    $.post( '<?php echo constant ('URL');?>usuarios/getDiscapacidadesTipo', { tipo_discapacidad: id_tipo_discapacidad} ).done( function( respuesta )
                                                    {
                                                        $( '#discapacidad1' ).html( respuesta );
                                                    });
                                                });
                                            </script>
                                    
                                        </div>
                                        <div id="seccion_Observaciones1" style="display:none;"></div>
                                        <div class="form-group"  id="seccion_inputs_Observaciones1" style="display:;">
                                            <label>Observaciones</label>
                                            <textarea id="observaciones1" name="observaciones1" type="text" class="form-control" placeholder="Escriba una observación..." aria-required="true" aria-invalid="false" onkeypress="return soloLetras(event)"  maxlength='200' minlength="10"></textarea>
                                        </div>


                                    </div>

                                    <script>

                                function muestra_oculta(id){
                                if (document.getElementById){ //se obtiene el id
                                var el = document.getElementById(id); //se define la variable "el" igual a nuestro div

                                el.style.display = (el.style.display == 'none') ? 'block' : 'none'; //damos un atributo display:none que oculta el div
                                //agregamos clases required en caso de que el ancla sea mostrado
                                        if(el.style.display=='block'){
                                                    myElemento = document.getElementById("tdiscapacidad1");
                                                    myElemento.classList.add('required');
                                                    myElemento2 = document.getElementById("discapacidad1");
                                                    myElemento2.classList.add('required');
                                                    myElemento3 = document.getElementById("observaciones1");
                                                    myElemento3.classList.add('required');
                                        }else{
                                                    myElemento = document.getElementById("tdiscapacidad1");
                                                    myElemento.classList.remove('required');
                                                    myElemento2 = document.getElementById("discapacidad1");
                                                    myElemento2.classList.remove('required');
                                                    myElemento3 = document.getElementById("observaciones1");
                                                    myElemento3.classList.remove('required');

                                        }
                                //para aumentar tamaño del fieldset
                                divC = document.getElementById("ver");
                                divC.style="775px";
                                }


                                }
                                window.onload = function(){/*hace que se cargue la función lo que predetermina que div estará oculto hasta llamar a la función nuevamente*/
                                muestra_oculta('contenido');/* "contenido_a_mostrar" es el nombre que le dimos al DIV */

                                }


                                </script>

                                </fieldset> 
                                <h1>Datos de Interes</h1>
                                <fieldset>
                                    <h2>Datos de Usuario</h2>
                                    <div class="msj" class="alert"></div>

                                    <p>
                                        &nbsp;En esta sección podra agregar un tipo de usuario y los privilegios que este tendra dentro del sistema.
                                        </p>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Roles <span style="color: red;">*</span></label>
                                                <select  name="roles[]" id="roles"   data-placeholder="Seleccione sus roles" class="chosen-select form-control required m-b" multiple style="width:350px;" tabindex="4">
                                                    
                                                    <?php include_once 'models/persona.php';
                                                        foreach($this->roles as $row){
                                                        $rol=new Persona();
                                                        $rol=$row;?> 
                                                    <option value="<?php echo $rol->id;?>"><?php echo $rol->descripcion;?></option>
                                                    <?php }?>             
                                                </select> 
                                                <script>
                                                $('.chosen-select').chosen({width: "100%"});
                                                </script>                                             
                                            </div>
                                            <div class="form-group">
                                                <label>Perfil<span style="color: red;">*</span></label>                                        
                                                <select  name="perfil" id="perfil"  class="select2_demo_3 form-control required m-b">                                    
                                                    <option selected="selected" value="">Seleccione </option> 
                                                    <?php 
                                                        foreach($this->perfiles as $row){
                                                        $perfil=new Persona();
                                                        $perfil=$row;?> 
                                                    <option value="<?php echo $perfil->id;?>"><?php echo $perfil->descripcion;?></option>
                                                <?php }?>                                            
                                                </select>   
                                            </div>
                                            
                                            
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Estatus </label>
                                                <select class="select2_demo_3 form-control required m-b " name="estatus" id="estatus">                                      
                                                    <option value="0" >Inactivo</option>
                                                    <option value ="1"selected="selected">Activo</option>                                               
                                                </select>                                              
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        $('#perfil').change(function()
                                            {
                                                var perfil = $(this).val();
                                                mostrar2(perfil);
                                            });
                                        </script>

                                    <!-- en caso de que el usuario sea un docente -->
                                    <h2 id="tdoc" style="display: none;">Datos del (la) Trabajador(a) Académico</h2>
                                    <div class="row" id="wdoc" style="display: none;">
                                        
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>PFG, PNF o PFA <span style="color: red;">*</span></label>
                                                <select  name="programa[]" id="programa" data-placeholder="Seleccione los Programas de Formación" style="width: 100%;" class="chosen-select form-control m-b" multiple style="width:350px;" tabindex="4">
                                                    <?php 
                                                        foreach($this->programas as $row){
                                                        $programa=new Estructura();
                                                        $programa=$row;?> 
                                                    <option value="<?php echo $programa->id_programa;?>"><?php echo $programa->descripcion;?></option>
                                                    <?php } ?>                                        
                                                </select> 
                                            </div>  
                                            <div class="form-row">
                                            
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                            <label>Eje Regional <i>(ER)</i> <span style="color: red;">*</span></label>
                                                            <select  name="eje_regional" style="width: 100%;" id="eje_regional"  class="select2_demo_3 form-control m-b">                                    
                                                                <option selected="selected" value="">Seleccione el ER al que Pertenece</option> 
                                                                <?php 
                                                                    foreach($this->ejes_regionales as $row){
                                                                    $eje_regional=new Estructura();
                                                                    $eje_regional=$row;?> 
                                                                <option value="<?php echo $eje_regional->id_eje_regional;?>"><?php echo $eje_regional->descripcion;?></option>
                                                                <?php }?>                                            
                                                            </select>
                                                            
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Eje Municipal <i>(EM)</i> <span style="color: red;">*</span></label>
                                                        <select  name="eje_municipal" style="width: 100%;" id="eje_municipal"  class=" form-control m-b">                                    
                                                            <option id="teje_municipal"selected="selected" value="">Seleccione el Eje Municipal</option> 
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Estatus del Docente <span style="color: red;">*</span></label>
                                                <select  name="estatus_d" style="width: 100%;" id="estatus_d"  class="form-control m-b">                                    
                                                    <option selected="selected" value="">Seleccione el Estatus </option> 
                                                    <?php 
                                                        foreach($this->docente_estatus as $row){
                                                            $d_estatus=new Estructura();
                                                            $d_estatus=$row;?> 
                                                        <option value="<?php echo $d_estatus->id;?>"><?php echo $d_estatus->descripcion;?></option>
                                                    <?php }?>               
                                                </select> 
                                            </div>

                                            <div class="form-group">
                                                <label>Escalafon del Trabajador Académico <span style="color: red;">*</span></label>
                                                <select  name="escalafon" style="width: 100%;" id="escalafon"  class="form-control m-b">                                    
                                                    <option selected="selected" value="">Seleccione el Escalafon </option> 
                                                    <?php 
                                                        foreach($this->escalafones as $row){
                                                            $escalafon=new Estructura();
                                                            $escalafon=$row;?> 
                                                        <option value="<?php echo $escalafon->id;?>"><?php echo $escalafon->descripcion;?></option>
                                                    <?php }?>               
                                                </select> 
                                            </div>

                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Centro de Estudio <i>(CE)</i> <span style="color: red;">*</span></label>
                                                <select  name="centro_estudio" style="width: 100%;" id="centro_estudio"  class="select2_demo_3 form-control m-b">                                    
                                                    <option selected="selected" value="">Seleccione el CE al que pertenece </option> 
                                                    <?php 
                                                        foreach($this->centros as $row){
                                                            $centro_estudio=new Estructura();
                                                            $centro_estudio=$row;?> 
                                                        <option value="<?php echo $centro_estudio->id_centro_estudio;?>"><?php echo $centro_estudio->descripcion;?></option>
                                                    <?php }?>                                            
                                                </select> 
                                            </div>  
                                            <div class="form-group">
                                                <label>Dedicación del Trabajador Académico <span style="color: red;">*</span></label>
                                                <select  name="dedicacion" style="width: 100%;" id="dedicacion"  class="form-control m-b">                                    
                                                    <option selected="selected" value="">Seleccione la Dedicación </option> 
                                                    <?php 
                                                        foreach($this->dedicaciones as $row){
                                                            $dedicacion=new Estructura();
                                                            $dedicacion=$row;?> 
                                                        <option value="<?php echo $dedicacion->id;?>"><?php echo $dedicacion->descripcion;?></option>
                                                    <?php }?>               
                                                </select> 
                                            </div>
                                            <div class="form-group">
                                                <label>Aldea del Docente <i>(Opcional)</i></label>
                                                <input id="aldea" name="aldea" autocomplete="off" type="text" class="form-control typeahead_3" maxlength='80' minlength="3" placeholder="Ingrese la Aldea del Docente">
                                            </div>
                                            <div class="form-group">
                                                <label>Clasificación del Trabajador Académico <span style="color: red;">*</span></label>
                                                <select  name="clasificacion" style="width: 100%;" id="clasificacion"  class="form-control m-b">                                    
                                                    <option selected="selected" value="">Seleccione la Clasificación </option> 
                                                    <?php 
                                                        foreach($this->clasificaciones as $row){
                                                            $clasificacion=new Estructura();
                                                            $clasificacion=$row;?> 
                                                        <option value="<?php echo $clasificacion->id;?>"><?php echo $clasificacion->descripcion;?></option>
                                                    <?php }?>               
                                                </select> 
                                            </div>
                                        </div>
                                        <script>
                                            $(document).ready(function(){
                                                $('.typeahead_3').typeahead({
                                                    source: [
                                                        <?php 
                                                            foreach($this->aldeas_ubv as $row){
                                                                $aldea_ubv=new Estructura();
                                                                $aldea_ubv=$row; ?>
                                                            {"name": "<?php echo $aldea_ubv->descripcion;?>", "code": "BR", "ccn0": "080"},
                                                        <?php } ?>
                                                        ]
                                                });
                                            });
                                            $('#eje_regional').change(function(){
                                                    var id_eje_regional = $(this).val();
                                                    
                                                    $.post( '<?php echo constant ('URL');?>usuarios/getEjeMunipalbyER', { eje_regional: id_eje_regional} ).done( function( respuesta )
                                                    {
                                                        $( '#eje_municipal' ).html( respuesta );
                                                    });
                                                });
                                        </script>
                                    </div>
                                    <!-- en caso de que el usuario sea un docente -->

                                </fieldset> 
                                <h1>Seguridad</h1>
                                <fieldset>
                                    <h2>Seguridad de la Cuenta</h2>
                                    <div class="msj" class="alert"></div>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Pregunta de Recuperación 1<span style="color: red;">*</span></label>
                                                <select  name="pregunta1" id="pregunta1" style="width: 100%;" class="select2_demo_3 form-control required m-b">                                    
                                                        <option selected="selected" value="">Seleccione la pregunta que desee </option> 
                                                        <?php 
                                                            foreach($this->preguntas as $row){
                                                                $pregunta=new Estructura();
                                                                $pregunta=$row;?> 
                                                        <option value="<?php echo $pregunta->id_pregunta;?>"><?php echo $pregunta->descripcion;?></option>
                                                        <?php }?>                                            
                                                    </select> 
                                            </div>
                                            <div class="form-group">
                                                <label>Respuesta de Seguridad 1<span style="color: red;">*</span></label>
                                                <input id="respuesta1" maxlength="60" name="respuesta1" placeholder="Ingrese la Respuesta a la Pregunta 1" type="text" class="form-control required">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Preguntas de Recuperación 2<span style="color: red;">*</span></label>
                                                <select  name="pregunta2" id="pregunta2" style="width: 100%;" class="select2_demo_3 form-control required m-b">                                    
                                                        <option selected="selected" value="">Seleccione la pregunta que desee </option> 
                                                        <?php 
                                                            foreach($this->preguntas as $row){
                                                                $pregunta2=new Estructura();
                                                                $pregunta2=$row;?> 
                                                        <option value="<?php echo $pregunta2->id_pregunta;?>"><?php echo $pregunta2->descripcion;?></option>
                                                        <?php }?>                                            
                                                    </select>  
                                            </div>
                                            <div class="form-group">
                                                <label>Respuesta 2<span style="color: red;">*</span></label>
                                                <input id="address" maxlength="60" name="respuesta2" placeholder="Ingrese la Respuesta a la Pregunta 2" type="text" class="form-control required">
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>

           
                        </div>

                        
                    </div>

                    
                    </div>
                    

                </div>
                
                </div>



        <?php require 'views/footer.php'; ?>

        
    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>
    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    
    <!-- Chosen -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>

    <!-- Sweet alert -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/sweetalert/sweetalert.min.js"></script>

   <!-- Input Mask-->
   <script src="<?php echo constant ('URL');?>src/js/plugins/jasny/jasny-bootstrap.min.js"></script>

       <!-- Select2 -->
       <script src="<?php echo constant ('URL');?>src/js/plugins/select2/select2.full.min.js"></script>
    <!-- Data picker -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>

    <!-- Typehead -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/typehead/bootstrap3-typeahead.min.js"></script>

    
    <!-- iCheck -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/iCheck/icheck.min.js"></script>
    
                
    <!-- form step-->
    <script>

        function mostrar2(id){
            //inicio radiobutton
            if(id == "si"){
                $("#wdiscapacidades").show();
                $("#tdiscapacidad0").addClass('required');
                $("#discapacidad0").addClass('required');                
            }

            if(id == "no"){
                $("#wdiscapacidades").hide();
                $("#tdiscapacidad0").removeClass('required');
                $("#discapacidad0").removeClass('required');
            }
            //fin radiobutton
            //inicio select
            if(id == "1"){//sies igual a docente muestro esto 
                $("#wdoc").show();
                $("#tdoc").show();
                $("#programa").addClass('required');
                $("#eje_regional").addClass('required');
                $("#centro_estudio").addClass('required');
                $("#dedicacion").addClass('required');
                $("#escalafon").addClass('required');
                $("#clasificacion").addClass('required');
                $("#estatus_d").addClass('required');

            }else{//sino oculto el div
                $("#wdoc").hide();
                $("#tdoc").hide();
                $("#programa").removeClass('required');
                $("#eje_regional").removeClass('required');
                $("#centro_estudio").removeClass('required');
                $("#dedicacion").removeClass('required');
                $("#escalafon").removeClass('required');
                $("#clasificacion").removeClass('required');
                $("#estatus_d").removeClass('required');

            }
            //fin select


        }

        $(document).ready(function(){

            
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                   
                   
                    ////////////////////////////////////////       
                    $('.chosen-select').chosen({width: "100%"});

                    
                    
                    ////////////////////////////////////////   


                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            
                                ano_r: {

                                    esfecha: true

                                }
                        }
                       
                    });
       });
       
       $.validator.addMethod("esfecha", esFechaActual, "El Año de Realización no debe ser mayor al Año Actual");


        function esFechaActual(value, element, param) {

            
            var fechaActual = <?php echo date('Y');?>;

            if (value > fechaActual) {

                return false; //error de validación

            }

            else {

                return true; //supera la validación

            }

        }

    </script>

   

</body>
</html>
