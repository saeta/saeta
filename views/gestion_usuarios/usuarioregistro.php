<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin | Usuarios</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="<?php echo constant ('URL');?>src/img/favicon.png"/> 
    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/select2/select2.min.css" rel="stylesheet">
  
    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">



</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Usuarios</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>usuarios">Gestion de Usuario</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Usuarios</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Registrar Usuario </strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                  
                    
                    <div class="ibox-content">
                    <?php echo $this->mensaje; ?>
                      
                    <!-- /////////////////////////////////////////-->  
                            <h2>
                            Registrar Usuario 
                            </h2>
                    
                        <p>Los campos identificados con <span style="color: red;">*</span> son obligatorios </p>
                       
                        <form id="form" action="<?php echo constant('URL');?>usuarios/RegistarUsuario"  method="post" class="wizard-big">
                                <h1>Datos Basicos</h1>
                                <!-- fieldset style="height:500px;"-->
                             <fieldset >
                                    <h2>Información personal </h2>
                                    <div class="row">


                                 
                                        <div class="col-lg-8">

                                        <div class="form-group">
                                        <label>Primer nombre<span style="color: red;">*</span></label>
                                        <input id="pnombre" name="pnombre" type="text" class="form-control required" maxlength='29' minlength="3"  onkeypress="return soloLetras(event)" placeholder="Escriba su primer nombre">
                                         </div>

                                         <div class="form-group">
                                        <label>Segundo Nombre</label>
                                        <input id="snombre" name="snombre" type="text" class="form-control " maxlength='29' minlength="3"  onkeypress="return soloLetras(event)"  placeholder="Escriba su segundo nombre">
                                         </div>                                    

                                        <div class="form-group">
                                        <label>Primer apellido <span style="color: red;">*</span></label>
                                        <input id="papellido" name="papellido" type="text" class="form-control required" maxlength='29' minlength="3"  onkeypress="return soloLetras(event)"  placeholder="Escriba su primer apellido">
                                        </div>
                                        <div class="form-group">
                                        <label>Segundo apellido</label>
                                        <input id="sapellido" name="sapellido" type="text" class="form-control" maxlength='29' minlength="3"  onkeypress="return soloLetras(event)" placeholder="Escriba su segundo apellido">
                                          </div>

                            </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-users" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </fieldset>
                             <h1>Descripción</h1>
                                             <!-- fieldset style="height:500px;"-->
                                <fieldset style="height:385px;">
                                    <h2>Información</h2>
                                    <div class="row">
                                        <div class="col-lg-6">


                            <div class="form-group" id="data_1">
                                <label class="font-normal">Fecha de Nacimiento<span style="color: red;">*</span></label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" name="fnac" id="fnac" class="form-control required" placeholder="<?php echo  date('m/d/Y');?>">
                                </div>
                            </div>


                                        
                                           <div class="form-group">
                                            <label>Estado Civil<span style="color: red;">*</span></label>                                        
                                            <select  name="estadocivil" id="estadocivil"  class="select2_demo_3 form-control required m-b">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/persona.php';
                                                foreach($this->estadosCivil as $row){
                                                $estado=new Persona();
                                                $estado=$row;?> 
                                            <option value="<?php echo $estado->id;?>"><?php echo $estado->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>


                                            <div class="form-group">
                                            <label>Tipo de Documento<span style="color: red;">*</span></label>                                        
                                            <select  name="tdocumento" id="tdocumento"  class="select2_demo_3 form-control required m-b">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/persona.php';
                                                foreach($this->documentoidentidad as $row){
                                                $documento=new Persona();
                                                $documento=$row;?> 
                                            <option value="<?php echo $documento->id;?>"><?php echo $documento->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>


                                        </div>
                                        <div class="col-lg-6">

                                      
                                      
                                        
                                        <div class="form-group">
                                        <label>Genero<span style="color: red;">*</span></label>                                        
                                            <select class="select2_demo_3 form-control required m-b" name="genero" id="genero">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/persona.php';
                                                foreach($this->generos as $row){
                                                $genero=new Persona();
                                                $genero=$row;?> 
                                            <option value="<?php echo $genero->id;?>"><?php echo $genero->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>


                                            <div class="form-group">
                                        <label>Etnia</label>                                        
                                            <select class="select2_demo_3 form-control  m-b" name="etnia" id="etnia">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/persona.php';
                                                foreach($this->etnias as $row){
                                                $etnia=new Persona();
                                                $etnia=$row;?> 
                                            <option value="<?php echo $etnia->id;?>"><?php echo $etnia->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>      

                                       
                                        <div class="form-group">
                                        <label>Numero de Documento<span style="color: red;">*</span></label>
                                        <input id="ndocumento" name="ndocumento" type="text" class="form-control required"  maxlength='10' minlength="7" onkeypress="return valSoloNumeros(event)" placeholder="Escriba su numero de documento">
                                        </div>
                                       


                                        </div>                                       
                                    </div>
                                </fieldset>

                                <h1>Ubicación</h1>
                                <fieldset>
                                    <h2> Ubicación</h2>
                                    <div class="row">
                                        <div class="col-lg-6">
                                               
                                           <div class="form-group">
                                           <label>País<span style="color: red;">*</span></label>                                        
                                            <select class="select2_demo_3 form-control required m-b" name="pais" id="pais">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            </select>   
                                            </div>
                                                <!-- por peticion de Edgar se saca nacionalidad de combo depoendiente Dom/24/11/19-->
                                            <div class="form-group">
                                            <label>Nacionalidad<span style="color: red;">*</span></label>                                        
                                            <select class="select2_demo_3 form-control required m-b" name="nacionalidad" id="nacionalidad">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/persona.php';
                                                foreach($this->nacionalidades as $row){
                                                $nacionalidad=new Persona();
                                                $nacionalidad=$row;?> 
                                            <option value="<?php echo $nacionalidad->id_nacionalidad;?>"><?php echo $nacionalidad->descripcion;?></option>
                                            <?php }?>
                                            </select> 
                                            </div>    


                                            <div class="form-group">
                                            <label>Tipo de Domicilio<span style="color: red;">*</span></label>                                        
                                            <select  name="tdomicilio" id="tdomicilio"  class="select2_demo_3 form-control required m-b">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/persona.php';
                                                foreach($this->domicilios as $row){
                                                $domicilio=new Persona();
                                                $domicilio=$row;?> 
                                            <option value="<?php echo $domicilio->id;?>"><?php echo $domicilio->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>


                                        </div>
                                        <div class="col-lg-6">
                                            
                                        <div class="form-group">
                                            <label>Estado<span style="color: red;">*</span></label>                                        
                                            <select  name="estado" id="estado"  class="select2_demo_3 form-control required m-b">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            </select>   
                                            </div>

                                        <div class="form-group">
                                            <label>Municipio<span style="color: red;">*</span></label>                                        
                                            <select  name="municipio" id="municipio"  class="select2_demo_3 form-control required m-b">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            </select>   
                                            </div>

                                        <div class="form-group">
                                        <label>Parroquia<span style="color: red;">*</span></label>                                        
                                            <select class="select2_demo_3 form-control required m-b" name="parroquia" id="parroquia">                                    
                                            <option selected="selected" value="">Seleccione </option>                                         
                                            </select>   
                                            </div>



                                        </div>  
                                    </div>

                                    <div class="form-group">
                                        <label>Dirección<span style="color: red;">*</span></label>
                                        <textarea id="direccion" name="direccion" type="text" class="form-control required valid" placeholder="Escriba una dirección..." aria-required="true" aria-invalid="false"  maxlength='145' minlength="5"></textarea>
                                        </div>

                                    <!-- combo select país, nacinalidad,estado, ciudad -->
                                    <script>
                                    $(function(){

                                // Lista de paises
                                $.post( '<?php echo constant ('URL');?>models/comboubicacion.php' ).done( function(respuesta)
                                {
                                    $( '#pais' ).html( respuesta );
                                });


                                // lista de estados	
                                $('#pais').change(function()
                                {
                                    var id_pais = $(this).val();
                                    
                                    // Lista de estados
                                    $.post( '<?php echo constant ('URL');?>models/comboubicacion.php', { pais: id_pais} ).done( function( respuesta )
                                    {
                                        $( '#estado' ).html( respuesta );
                                    });
                                });


                                // lista de nacinalidades	
                               /* $('#pais').change(function()
                                {
                                    var id_pais = $(this).val();
                                    
                                    // Lista de nacinalidades
                                    $.post( '<?php echo constant ('URL');?>models/comboubicacion.php', { nacionalidad: id_pais} ).done( function( respuesta )
                                    {
                                        $( '#nacionalidad' ).html( respuesta );
                                    });
                                });*/


                                // lista de municipios	
                                $('#estado').change(function()
                                {
                                    var id_estado = $(this).val();
                                    
                                    // Lista de municipios
                                    $.post( '<?php echo constant ('URL');?>models/comboubicacion.php', { municipio: id_estado} ).done( function( respuesta )
                                    {
                                        $( '#municipio' ).html( respuesta );
                                    });
                                });


                                // lista de parroquias	
                                $('#municipio').change(function()
                                {
                                    var id_municipio = $(this).val();
                                    
                                    // Lista de parroquias
                                    $.post( '<?php echo constant ('URL');?>models/comboubicacion.php', { parroquia: id_municipio} ).done( function( respuesta )
                                    {
                                        $( '#parroquia' ).html( respuesta );
                                    });
                                });





                                })
                                    </script>  
                                </fieldset>

                                <h1>Contacto</h1>
                                <fieldset>
                                    <h2>Contacto e Información personal</h2>
                                    <div class="row">
                                        <div class="col-lg-6">


                                        <div class="form-group">
                                        <label>Tipo Correo<span style="color: red;">*</span></label>                                        
                                            <select class="select2_demo_3 form-control required m-b" name="tcorreo" id="tcorreo">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/persona.php';
                                                foreach($this->tcorreos as $row){
                                                $correo=new Persona();
                                                $correo=$row;?> 
                                            <option value="<?php echo $correo->id;?>"><?php echo $correo->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>       




                                            <div class="form-group">
                                        <label>Tipo Telefono<span style="color: red;">*</span></label>                                        
                                            <select class="select2_demo_3 form-control required m-b" name="tipotelf" id="tipotelf">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/persona.php';
                                                foreach($this->telefono_tipo as $row){
                                                $telefono=new Persona();
                                                $telefono=$row;?> 
                                            <option value="<?php echo $telefono->id;?>"><?php echo $telefono->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>   

                                           

                                            

                            <div class="form-group row">
                            <label class="col-sm-8 col-form-label">¿Posee alguna discapacidad? <span style="color: red;">*</span> </label>

                            <div class="i-checks col-sm-15"  >                                
                            <label>Si
                            <input type="radio"  value="Si" id="optionsRadios1" name="optionsRadios" onChange="DiscapacidadOnChange(this)">
                            </label> 
                            <label class="i-checks checkbox-inline" >No                                
                            <input type="radio" value="No" id="optionsRadios2" name="optionsRadios" onChange="DiscapacidadOnChange(this)">
                            </label>

                            </div>
                            </div>
                    <!-- <script>
                        $(document).ready(function () {
                            $('.i-checks').iCheck({
                                checkboxClass: 'icheckbox_square-green',
                                radioClass: 'iradio_square-green',
                            });
                        });
                    </script>-->

               <!-- para ocultar o mostrar discapacidades -->
               <script type="text/javascript">
                    function DiscapacidadOnChange(sel) {
                        if (sel.value=="No"){
                            divC = document.getElementById("seccion_Observaciones");
                            divC.style.display = "";
                            divC = document.getElementById("seccion_Tipo_Discapacidad");
                            divC.style.display = "";
                            divC = document.getElementById("seccion_Discapacidad");
                            divC.style.display = "";

                            divT = document.getElementById("seccion_inputs_Observaciones");
                            divT.style.display = "none";

                            divT = document.getElementById("seccion_inputs_Tipo_Discapacidad");
                            divT.style.display = "none";

                            divT = document.getElementById("seccion_inputs_Discapacidad");
                            divT.style.display = "none";

                            divT = document.getElementById("seccion_inputs_Codigo");
                            divT.style.display = "none";

                            
                            myElemento7 = document.getElementById("conadis");
                            myElemento7.classList.remove('required');

                            myElemento3 = document.getElementById("tdiscapacidad0");
                            myElemento3.classList.remove('required');
                            myElemento4 = document.getElementById("discapacidad0");
                            myElemento4.classList.remove('required');
                            myElemento5 = document.getElementById("observaciones0");
                            myElemento5.classList.remove('required');


                        //aqui removemos los requires de discapacidad 1
                            myElemento = document.getElementById("tdiscapacidad1");
                            myElemento.classList.remove('required');
                            myElemento2 = document.getElementById("discapacidad1");
                            myElemento2.classList.remove('required');
                            myElemento3 = document.getElementById("observaciones1");
                            myElemento3.classList.remove('required');
                        }else{

                            divC = document.getElementById("seccion_Observaciones");
                            divC.style.display="none";

                            divC = document.getElementById("seccion_Tipo_Discapacidad");
                            divC.style.display = "none";

                            divC = document.getElementById("seccion_Discapacidad");
                            divC.style.display = "none";


                            divT = document.getElementById("seccion_inputs_Observaciones");
                            divT.style.display = "";
                            divT = document.getElementById("seccion_inputs_Tipo_Discapacidad");
                            divT.style.display = "";
                            divT = document.getElementById("seccion_inputs_Discapacidad");
                            divT.style.display = "";
                            divT = document.getElementById("seccion_inputs_Codigo");
                            divT.style.display = "";
                      
                      //agrega clase required 
                            myElemento7 = document.getElementById("conadis");
                            myElemento7.classList.add('required');

                            myElemento3 = document.getElementById("tdiscapacidad0");
                            myElemento3.classList.add('required');
                            myElemento4 = document.getElementById("discapacidad0");
                            myElemento4.classList.add('required');
                            myElemento5 = document.getElementById("observaciones0");
                            myElemento5.classList.add('required');


                        }
                  
                    

                    }
                    </script>




                  </div>

                                        <div class="col-lg-6">
                                       
                                        <div class="form-group">
                                        <label>Correo <span style="color: red;">*</span></label>
                                        <input id="correo" name="correo" type="email" class="form-control required" maxlength='49' minlength="7" Placeholder="Escriba su correo">
                                          </div>


                                        <div class="form-row">
                                        <div class="form-group col-md-4">
                                        <label> Cod. Area<span style="color: red;">*</span></label>  
                                        <select class="select2_demo_3 form-control required m-b" name="cod_area" id="cod_area">                                    
                                            <option selected="selected" value="">----- </option> 
                                            <?php include_once 'models/persona.php';
                                                foreach($this->cod_area_telefono as $row){
                                                $cod_area=new Persona();
                                                $cod_area=$row;?> 
                                            <option value="<?php echo $cod_area->id;?>"><?php echo $cod_area->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>                                           </div>
                                        <div class="form-group col-md-8">
                                        <label> Telefono <span style="color: red;">*</span></label>  
                                            <input name="numero" minlength="7" maxlength="7" id="numero"  onkeypress="return valSoloNumeros(event)"  class="form-control required " aria-required="true" type="text" placeholder="Escriba su número de Teléfono">
                                      
                                        </div>                                        
                                    </div>

 
                                    <div class="form-group"  id="seccion_inputs_Codigo" style="display:;">
                                        <label>Codigo Conapdis</label>
                                        <input id="conadis" name="conadis" type="text" class="form-control " maxlength='10' minlength="4" onkeypress="return valSoloNumeros(event)" placeholder="Escriba su codigo conadis">
                                   </div>  
          
                                        </div>
                                    </div>



<div>
                    <div class="form-row">

                    <div class="form-group col-md-6">
                    <div id="seccion_Discapacidad" style="display:none;"></div>
                    <div class="form-group"  id="seccion_inputs_Tipo_Discapacidad" style="display:;">
                    <label>Tipo de discapacidad</label>                                        
                    <select  class="select2_demo_3 form-control  m-b " name="tdiscapacidad" id="tdiscapacidad0">                                    
                    <option selected="selected" value="">Seleccione </option> 
                    </select> 
                    </div>                                                            
                    </div>


                    <div class="form-group col-md-6">                                 
                    <div id="seccion_Tipo_Discapacidad" style="display:none;"></div>
                    <div class="form-group "  id="seccion_inputs_Discapacidad" style="display:;">
                    <label>Discapacidad</label>                                        
                    <select class="select2_demo_3 form-control m-b" name="discapacidad" id="discapacidad0">                                    
                    <option selected="selected" value="">Seleccione </option> 
                    </select>   
                    </div>   
                    </div>
                    
                   <!-- <button type="button" onclick="nuevo();">Agregar</button> -->                     
                  </div>
               
                                <div id="seccion_Observaciones" style="display:none;"></div>
                                <div class="form-group"  id="seccion_inputs_Observaciones" style="display:;">
                                <label>Observaciones</label>
                                <textarea id="observaciones0" name="observaciones" type="text" class="form-control" placeholder="Escriba una observación..." aria-required="true" aria-invalid="false" onkeypress="return soloLetras(event)"  maxlength='200' minlength="10"></textarea>
                              
                                <a style='cursor: pointer;' onClick="muestra_oculta('contenido')" title="" class="boton_mostrar">Agregar discapacidad / Ocultar</a>
                                <br><br>
                            
                 




          <!-- ////////////////////////////Div de Otra discapacidad//////////////////////////////////////////////7-->
            <div id="contenido" >
                    <div class="form-row">

                    <div class="form-group col-md-6">
                    <div id="seccion_Discapacidad1" style="display:none;"></div>
                    <div class="form-group"  id="seccion_inputs_Tipo_Discapacidad1" style="display:;">
                    <label>Tipo de discapacidad</label>                                        
                    <select  class="select2_demo_3 form-control  m-b " name="tdiscapacidad1" id="tdiscapacidad1">                                    
                    <option selected="selected" value="">Seleccione </option> 
                    </select> 
                    </div>                                                            
                    </div>


                    <div class="form-group col-md-6">                                 
                    <div id="seccion_Tipo_Discapacidad1" style="display:none;"></div>
                    <div class="form-group "  id="seccion_inputs_Discapacidad1" style="display:;">
                    <label>Discapacidad</label>                                        
                    <select class="select2_demo_3 form-control m-b" name="discapacidad1" id="discapacidad1">                                    
                    <option selected="selected" value="">Seleccione </option> 
                    </select>   
                    </div>   
                    </div>
              
              
                </div>
                                 <div id="seccion_Observaciones1" style="display:none;"></div>
                                <div class="form-group"  id="seccion_inputs_Observaciones1" style="display:;">
                                <label>Observaciones</label>
                                <textarea id="observaciones1" name="observaciones1" type="text" class="form-control" placeholder="Escriba una observación..." aria-required="true" aria-invalid="false" onkeypress="return soloLetras(event)"  maxlength='200' minlength="10"></textarea>
                                </div>

            </div>

            <script>

function muestra_oculta(id){
if (document.getElementById){ //se obtiene el id
var el = document.getElementById(id); //se define la variable "el" igual a nuestro div

el.style.display = (el.style.display == 'none') ? 'block' : 'none'; //damos un atributo display:none que oculta el div
//agregamos clases required en caso de que el ancla sea mostrado
        if(el.style.display=='block'){
                    myElemento = document.getElementById("tdiscapacidad1");
                    myElemento.classList.add('required');
                    myElemento2 = document.getElementById("discapacidad1");
                    myElemento2.classList.add('required');
                    myElemento3 = document.getElementById("observaciones1");
                    myElemento3.classList.add('required');
        }else{
                    myElemento = document.getElementById("tdiscapacidad1");
                    myElemento.classList.remove('required');
                    myElemento2 = document.getElementById("discapacidad1");
                    myElemento2.classList.remove('required');
                    myElemento3 = document.getElementById("observaciones1");
                    myElemento3.classList.remove('required');

        }
 //para aumentar tamaño del fieldset
 divC = document.getElementById("ver");
 divC.style="775px";
}


}
window.onload = function(){/*hace que se cargue la función lo que predetermina que div estará oculto hasta llamar a la función nuevamente*/
muestra_oculta('contenido');/* "contenido_a_mostrar" es el nombre que le dimos al DIV */

}


</script>

                <!-- estos div son de onchange para ocultar los-->
           </div>
                             
                             </div>

          <!-- //////////////////////////////////////////////////////////////////////////7-->

                            <!-- combo select tipo discapacidad,discapacidad -->
                            <script>
                                $(function(){

                            // Lista de tipos de discapacidad
                            $.post( '<?php echo constant ('URL');?>models/combodiscapacidad.php' ).done( function(respuesta)
                            {
                                $( '#tdiscapacidad0' ).html( respuesta );
                            });


                            // lista de discapacidad	
                            $('#tdiscapacidad0').change(function()
                            {
                                var id_tipo_discapacidad = $(this).val();
                                
                                // Lista de estados
                                $.post( '<?php echo constant ('URL');?>models/combodiscapacidad.php', { discapacidad: id_tipo_discapacidad} ).done( function( respuesta )
                                {
                                    $( '#discapacidad0' ).html( respuesta );
                                });
                            });




                    // div otra Lista de tipos de discapacidad
                    $.post( '<?php echo constant ('URL');?>models/combodiscapacidadd.php' ).done( function(respuesta)
                                                {
                                $( '#tdiscapacidad1' ).html( respuesta );
                            });


                            // lista de discapacidad	
                            $('#tdiscapacidad1').change(function()
                            {
                                var id_tipo_discapacidad1 = $(this).val();
                                
                                // Lista de estados
                                $.post( '<?php echo constant ('URL');?>models/combodiscapacidadd.php', { discapacidad1: id_tipo_discapacidad1} ).done( function( respuesta )
                                {
                                    $( '#discapacidad1' ).html( respuesta );
                                });
                            });

                    ////////////////////////////////////////////

                            })
                                </script>            
                                                                            
            </fieldset>
                               
                                <h1>Perfil</h1>
                                <fieldset style="height:385px;">
                                    <h2>Perfil de usuario</h2>
                                    <p>
                                        &nbsp;En esta sección podra agregar un tipo de usuario y los privilegios que este tendra dentro del sistema.
                                        </p>
                                    <div class="row">
                                        <div class="col-lg-6">

                                       <!-- <div class="form-group">
                                        <label>Perfil<span style="color: red;">*</span></label>
                                        <input id="perfil" name="perfil" type="text" class="form-control required" maxlength='30' minlength="3" Placeholder="Escriba un tipo de usuario">
                                         </div>-->     


                                         <div class="form-group">
                                            <label>Perfil<span style="color: red;">*</span></label>                                        
                                            <select  name="perfil" id="perfil"  class="select2_demo_3 form-control required m-b">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/persona.php';
                                                foreach($this->perfiles as $row){
                                                $perfil=new Persona();
                                                $perfil=$row;?> 
                                            <option value="<?php echo $perfil->id;?>"><?php echo $perfil->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>


                                        
                                         <div class="form-group">
                                         <label>Roles <span style="color: red;">*</span></label>
                                         <select  name="roles[]" id="roles"   data-placeholder="Seleccione sus roles" class="chosen-select form-control required m-b" multiple style="width:350px;" tabindex="4">
                                            <?php include_once 'models/persona.php';
                                                foreach($this->roles as $row){
                                                $rol=new Persona();
                                                $rol=$row;?> 
                                            <option value="<?php echo $rol->id;?>"><?php echo $rol->descripcion;?></option>
                                            <?php }?>             
                                         </select>                                              
                                         </div>

                                        </div>
                                        <div class="col-lg-6">
                                                                     
                                            <div class="form-group">
                                            <label>Estatus </label>
                                            <select class="select2_demo_3 form-control required m-b " name="estatus" id="estatus">                                      
                                            <option value="0" >Inactivo</option>
                                            <option value ="1"selected="selected">Activo</option>                                               
                                            </select>                                              
                                            </div>

                                          
                                        </div>
                                    </div>
                                </fieldset>
                          
                            </form>
                     






                    


                   <!-- /////////////////////////////////////////////////////////////7-->

                            <button    onclick="location.href='<?php echo constant('URL');?>usuarios';"class="btn btn-white btn-arrow" style="margin-left:45%;"><i class="fa fa-arrow-left"></i> Volver</button>
                      </div>
                </div>
            </div>





            </div>
        </div>



      

<!--////////////////////////////////-->
 

        <!-- Mainly scripts -->
    <script src="<?php echo constant ('URL');?>src/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/popper.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/bootstrap.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>


    <?php require 'views/footer.php'; ?>
    <!-- Custom and plugin javascript -->
    <script src="<?php echo constant ('URL');?>src/js/inspinia.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/pace/pace.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>

    <!-- Solo Letras -->
    <script src="<?php echo constant ('URL');?>src/js/val_letras.js"></script>
    <!-- Chosen Select -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script> 

   <!-- Select2 -->
       <script src="<?php echo constant ('URL');?>src/js/plugins/select2/select2.full.min.js"></script>

    <!-- Data picker -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>

    <!-- iCheck -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/iCheck/icheck.min.js"></script>
        
                      


    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                   
                   
                ////////////////////////////////////////       
                $('.chosen-select').chosen({width: "100%"});

                $(".select2_demo_3").select2({
                                placeholder: "Seleccione",
                                width: "100%",
                                dropdownAutoWidth: true,
                                allowClear: true
                            });
                
                var mem = $('#data_1 .input-group.date').datepicker({
                        todayBtn: "linked",
                        keyboardNavigation: false,
                        forceParse: false,
                        calendarWeeks: true,
                        autoclose: true
                    });


                ////////////////////////////////////////   


                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>



</body>
</html>
