<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Listar Usuarios | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="<?php echo constant ('URL');?>src/img/favicon.png"/> 
    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">


</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-7">
                    <h2>Listar Usuarios</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>usuarios">Gestion de Usuarios</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Listar Usuarios</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong> Listar</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-5">
                    <div class="title-action">
                    <?php if($_SESSION['Agregar']==true){ ?>

                        <!-- boton agregar-->
                        <a href="<?php echo constant('URL') . 'usuarios/viewAdd';?>"><button type="button" class="btn btn-primary"><i class="fa fa-plus"></i> &nbsp;Añadir Usuario&nbsp;</button></a>
                    <?php } ?>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Listado de Usuarios </h5>
                       
                        <div class="ibox-tools">
                    
                        </div>
                    </div>
                    
                    <div class="ibox-content">
                    <?php echo $this->mensaje; ?>
                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Cédula</th>
                        <th>Nombres</th>
                        <th><i class="fa fa-calendar"></i> Fecha de Registro</th>   
                        <th><i class="fa fa-drivers-license-o" aria-hidden="true"></i> Perfil</th>  
                        <th><i class="fa fa-user-o" aria-hidden="true"></i> Usuario</th>
                        <th>Estatus</th>   
                        <th>Opciones</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php include_once 'models/persona.php';
                            foreach($this->usuarios as $row){
                                $usuario= new Persona();
                                $usuario=$row;?>
                    <tr class="gradeX">
                    <td><?php echo substr($usuario->tipo_documento, 0, 1). "-". $usuario->identificacion; ?></td>
                    <td><?php echo $usuario->primer_nombre . " " . $usuario->primer_apellido; ?></td>
                    <td>  <?php echo date("d/m/Y", strtotime($usuario->registro)); ?></td>
                    <td> <?php echo $usuario->perfil; ?> </td>
                    <td><?php echo $usuario->usuario; ?></td>
                   
                       <td> <?php if($usuario->estatus==0){
                           echo"<center style=margin-top:3%;><span class='label label-danger' style='font-size: 12px;'>Inactivo</span></center>";
                     
                    }else{
                        echo"<center style=margin-top:3%;><span class='label label-primary' style='font-size: 12px;'>&nbsp; Activo &nbsp;</span></center>";
                
                    }  ?>
                    
                    </td>                 
                    
                    <td> 
                    <?php if($_SESSION['Consultar']==true){ ?>
                    <a href="<?php echo constant('URL') . 'usuarios/verUsuarioInfo/' .$usuario->id_persona;?>"><button type="button" class="btn btn-outline btn-info"><i class="fa fa-eye"></i> Ver</button></a>
                    <?php } 
                    if($_SESSION['Editar']==true){?>
                    &nbsp;
                    <a href="<?php echo constant('URL') . 'usuarios/verUsuario/' .$usuario->id_persona;?>"><button type="button" class="btn btn-outline btn-success"><i class="fa fa-edit"></i> Editar</button></a>
                    <?php } ?>
                    
                    </td>
                        </tr>
                    <?php }?>
                        
                        </tbody>
                    
                        </table>

                   
                      </div>

                    </div>
                </div>
            </div>

       


            </div>

        </div>



<!--////////////////////////////////-->
  



  
    <?php require 'views/footer.php'; ?>
   

   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>

    <!-- iCheck -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/iCheck/icheck.min.js"></script>
        



    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>



</body>
</html>
