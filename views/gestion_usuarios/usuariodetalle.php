<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin | Usuarios</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/select2/select2.min.css" rel="stylesheet">

</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Usuarios</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>usuarios">Gestion de Usuario</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Usuarios</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong> Registro de Usuarios</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                    <div class="title-action">
                    <button onclick="location.href='<?php echo constant('URL');?>usuarios';"class="btn btn-primary btn-arrow" ><i class="fa fa-arrow-left"></i> Volver</button>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                  
                    
                    <div class="ibox-content">
                    <?php echo $this->mensaje; ?>
                      
                      





                    <form id="form" action="<?php echo constant('URL');?>usuarios/ActualizarUsuario"  method="post" class="wizard-big">
                                <h1>Datos Basicos</h1>
                                <!-- fieldset style="height:500px;"-->
                             <fieldset >
                                    <h2>Información personal </h2>
                                    <div class="row">


                                 
                                        <div class="col-lg-8">
                                        <div class="form-group">
                                        <input id="id_persona" name="id_persona" type="hidden" value="<?php echo $this->informacion->id_persona; ?>" class="form-control">
                                        <input id="id_persona" name="id_persona" type="hidden" value="<?php echo $this->informacion_u->id_persona; ?>" class="form-control">
                                        <input id="id_persona" name="id_persona" type="hidden" value="<?php echo $this->informacion_d->id_persona; ?>" class="form-control">
                                        <input id="id_persona" name="id_persona" type="hidden" value="<?php echo $this->informacion_n->id_persona; ?>" class="form-control">
                                        <label>Primer nombre<span style="color: red;">*</span></label>
                                        <input id="pnombre" name="pnombre" type="text" class="form-control required" maxlength='15' minlength="3"  onkeypress="return soloLetras(event)" placeholder="Escriba su primer nombre" value="<?php echo $this->informacion->primer_nombre; ?>">
                                         </div>

                                         <div class="form-group">
                                        <label>Segundo Nombre</label>
                                        <input id="snombre" name="snombre" type="text" class="form-control " maxlength='15' minlength="3"  onkeypress="return soloLetras(event)"  placeholder="Escriba su segundo nombre" value="<?php echo $this->informacion->segundo_nombre; ?>">
                                         </div>                                    

                                        <div class="form-group">
                                        <label>Primer apellido <span style="color: red;">*</span></label>
                                        <input id="papellido" name="papellido" type="text" class="form-control required" maxlength='15' minlength="3"  onkeypress="return soloLetras(event)"  placeholder="Escriba su primer apellido" value="<?php echo $this->informacion->primer_apellido; ?>">
                                        </div>
                                        <div class="form-group">
                                        <label>Segundo apellido</label>
                                        <input id="sapellido" name="sapellido" type="text" class="form-control" maxlength='15' minlength="3"  onkeypress="return soloLetras(event)" placeholder="Escriba su segundo apellido" value="<?php echo $this->informacion->segundo_apellido; ?>">
                                          </div>
                            </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-users" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </fieldset>
                             <h1>Descripción</h1>
                                             <!-- fieldset style="height:500px;"-->
                                <fieldset style="height:385px;">
                                    <h2>Información</h2>
                                    <div class="row">
                                        <div class="col-lg-6">


                            <div class="form-group" id="data_1">
                                <label class="font-normal">Fecha de Nacimiento<span style="color: red;">*</span></label>
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" name="fnac" id="fnac" class="form-control required" value="<?php echo $this->informacion->fecha_nacimiento; ?>">
                                </div>
                            </div>


                            <script>
                            $('.chosen-select').chosen({width: "100%"});




                            var mem = $('#data_1 .input-group.date').datepicker({
                                    todayBtn: "linked",
                                    keyboardNavigation: false,
                                    forceParse: false,
                                    calendarWeeks: true,
                                    autoclose: true
                                });
                            </script>
         
                                           <div class="form-group">
                                            <label>Estado Civil<span style="color: red;">*</span></label>                                        
                                            <select  name="estadocivil" id="estadocivil"  class="chosen-select form-control required m-b">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/estructura.php';
                                                foreach($this->estadosCivil as $row){
                                                $estado=new Estructura();
                                                $estado=$row;?> 
                                            <option value="<?php echo $estado->id;?>"<?php if($estado->id == $this->informacion->id_estado_civil){ print "selected=selected"; }?>><?php echo $estado->descripcion;?></option>
                                            <?php }?>
                                            </select>
                                            </div>


                                            <div class="form-group">
                                            <label>Tipo de Documento<span style="color: red;">*</span></label>                                        
                                            <select  name="tdocumento" id="tdocumento"  class="chosen-select form-control required m-b">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/estructura.php';
                                                foreach($this->documentoidentidad as $row){
                                                $documento=new Estructura();
                                                $documento=$row;?> 
                                            <option value="<?php echo $documento->id;?>"<?php if($documento->id == $this->informacion->id_documento_identidad_tipo){ print "selected=selected"; }?>><?php echo $documento->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>






                                        </div>
                                        <div class="col-lg-6">


                                        <div class="form-group">
                                        <label>Genero<span style="color: red;">*</span></label>                                        
                                            <select class="chosen-select form-control required m-b" name="genero" id="genero">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/estructura.php';
                                                foreach($this->generos as $row){
                                                $genero=new Estructura();
                                                $genero=$row;?> 
                                            <option value="<?php echo $genero->id;?>"<?php if($genero->id == $this->informacion->id_genero){ print "selected=selected"; }?>><?php echo $genero->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>


                                            <div class="form-group">
                                        <label>Etnia Indígena</label>                                        
                                            <select class="chosen-select form-control m-b" name="etnia" id="etnia">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/estructura.php';
                                                foreach($this->etnias as $row){
                                                $etnia=new Estructura();
                                                $etnia=$row;?> 
                                            <option value="<?php echo $etnia->id;?>"<?php if($etnia->id == $this->informacion->id_etnia){ print "selected=selected"; }?>><?php echo $etnia->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>      

                                       
                                        <div class="form-group">
                                        <label>Numero de Documento<span style="color: red;">*</span></label>
                                        <input id="ndocumento" name="ndocumento" type="text" class="form-control required" value="<?php echo $this->informacion->identificacion; ?>"  maxlength='10' minlength="3" onkeypress="return valSoloNumeros(event)" placeholder="Escriba su numero de documento">
                                        </div>
                                       


                                 



                                        </div>                                       
                                    </div>
                                </fieldset>

                                <h1>Ubicación</h1>
                                <fieldset>
                                    <h2> Ubicación</h2>
                                    <div class="row">
                                        <div class="col-lg-6">
                                               

                                            <div class="form-group">
                                                    <label>País <span style="color: red;">*</span></label>
                                                    <select name="pais" id="pais" style="width: 100%;"  class="form-control required" tabindex="4">
                                                        <option id="tpais"value="">Seleccione el País</option>
                                                        <!-- <?php 
                                                            foreach($this->paises as $row){
                                                            $pais=new Estructura();
                                                            $pais=$row;?> 
                                                        <option value="<?php echo $pais->id_pais;?>" <?php if($pais->id_pais==$this->informacion->id_pais){ print "selected=selected";}?>><?php echo $pais->descripcion;?></option>
                                                        <?php }?> -->                                        
                                                    </select>
                                                </div>

                                            <div class="form-group">
                                        <label>Nacionalidad<span style="color: red;">*</span></label>                                        
                                            <select class="chosen-select form-control required m-b" name="nacionalidad" id="nacionalidad">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/estructura.php';
                                                foreach($this->nacionalidades as $row){
                                                $nacionalidad=new Estructura();
                                                $nacionalidad=$row;?> 
                                            <option data-cod="<?php echo $nacionalidad->id_nacionalidad;?>" value="<?php echo $nacionalidad->id_nacionalidad;?>"<?php if($nacionalidad->id_nacionalidad == $this->informacion->id_nacionalidad){ print "selected=selected"; }?>><?php echo $nacionalidad->descripcion;?></option>
                                            <?php }?>                                            
                                            </select> 
                                            </div>    





                                            <div class="form-group">
                                            <label>Tipo de Domicilio<span style="color: red;">*</span></label>                                        
                                            <select  name="tdomicilio" id="tdomicilio"  class="chosen-select form-control required m-b">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/estructura.php';
                                                foreach($this->domicilios as $row){
                                                $domicilio=new Estructura();
                                                $domicilio=$row;?> 
                                            <option value="<?php echo $domicilio->id;?>"<?php if($domicilio->id == $this->informacion->id_domicilio_detalle_tipo){ print "selected=selected"; }?>><?php echo $domicilio->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>


                                        </div>
                                        <div class="col-lg-6">
                                            
                                        <div class="form-group">
                                            <label>Estado <span style="color: red;">*</span></label>
                                                <select name="estado" id="estado" style="width: 100%;" class="form-control required" tabindex="4">
                                                    <option id="testado" value="">Seleccione el Estado</option>
                                                                                              
                                                </select>
                                        </div>

                                       
                                            <div class="form-group">
                                            <label>Municipio <span style="color: red;">*</span></label>
                                                <select name="municipio" id="municipio" style="width: 100%;" class="form-control required" tabindex="4">
                                                    <option id="tmunicipio" value="">Seleccione la Municipio</option>
                                                                                              
                                                </select>
                                        </div>
                                        

                                        <div class="form-group">
                                            <label>Parroquia <span style="color: red;">*</span></label>
                                                <select name="parroquia" id="parroquia" style="width: 100%;" class="form-control required" tabindex="4">
                                                    <option id="tparroquia" value="">Seleccione la Parroquia</option>
                                                                                              
                                                </select>
                                        </div>               

                                        </div>  
                                    </div>

                                    <div class="form-group">
                                        <label>Dirección<span style="color: red;">*</span></label>
                                        <textarea id="direccion" name="direccion" type="text" class="form-control required valid" placeholder="Escriba una dirección..." aria-required="true" aria-invalid="false"  maxlength='70' minlength="4"><?php echo $this->informacion->domicilio_detalle; ?></textarea>
                                                
                                                        
                                                    
                                    </div>

                                </fieldset>
                                <script>
                                    $(function(){

                                    // Lista de paises
                                    $.post( '<?php echo constant ('URL');?>models/comboubicacion-registro.php' ).done( function(respuesta)
                                    {
                                        $( '#pais' ).html( respuesta );
                                        
                                        $( '#pais option[value="'+<?php echo $this->informacion->id_pais; ?>+'"]' ).attr("selected", true);

                                        if($("#pais").val() == '<?php echo $this->informacion->id_pais;?>'){
                                            
                                            $('#tpais').append('<option value="<?php echo $this->informacion->id_pais;?>" selected="selected"><?php echo $this->informacion->pais;?></option>');
                                        }


                                    });

                                    $.post( '<?php echo constant ('URL');?>models/comboubicacion-registro.php', { pais: <?php echo $this->informacion->id_pais;?>} ).done( function( respuesta )
                                        {
                                            $( '#estado' ).html( respuesta );
                                            
                                            $( '#estado option[value="'+<?php echo $this->informacion->id_estado; ?>+'"]' ).attr("selected", true);

                                            if($("#estado").val() == '<?php echo $this->informacion->id_estado;?>'){
                                                $('#testado').append('<option value="<?php echo $this->informacion->id_estado;?>" selected="selected"><?php echo $this->informacion->estado;?></option>');

                                            }
                                        });

                                    $.post( '<?php echo constant ('URL');?>models/comboubicacion-registro.php', { estado: <?php echo $this->informacion->id_estado;?>} ).done( function( respuesta )
                                        {
                                            $( '#municipio' ).html( respuesta );
                                            
                                            $( '#municipio option[value="'+<?php echo $this->informacion->id_municipio; ?>+'"]' ).attr("selected", true);

                                            if($("#municipio").val() == '<?php echo $this->informacion->id_municipio;?>'){
                                                $('#tmunicipio').append('<option value="<?php echo $this->informacion->id_municipio;?>" selected="selected"><?php echo $this->informacion->municipio;?></option>');

                                            }
                                        });    

                                    $.post( '<?php echo constant ('URL');?>models/comboubicacion-registro.php', { municipio: <?php echo $this->informacion->id_municipio;?>} ).done( function( respuesta )
                                        {
                                            $( '#parroquia' ).html( respuesta );
                                            
                                            $( '#parroquia option[value="'+<?php echo $this->informacion->id_parroquia; ?>+'"]' ).attr("selected", true);

                                            if($("#parroquia").val() == '<?php echo $this->informacion->id_parroquia;?>'){
                                                $('#tparroquia').append('<option value="<?php echo $this->informacion->id_parroquia;?>" selected="selected"><?php echo $this->informacion->parroquia;?></option>');

                                            }
                                        });    

                                    // lista de estados	
                                    $('#pais').change(function()
                                    {
                                        var id_pais = $(this).val();
                                        
                                        // Lista de estados
                                        $.post( '<?php echo constant ('URL');?>models/comboubicacion-registro.php', { pais: id_pais} ).done( function( respuesta )
                                        {
                                            $( '#estado' ).html( respuesta );
                                           
                                        });
                                    });


                                


                                        // lista de MUNICIPIOS	
                                        $('#estado').change(function()
                                        {
                                            var id_estado = $(this).val();
                                            
                                            // Lista de estados
                                            $.post( '<?php echo constant ('URL');?>models/comboubicacion-registro.php', { estado: id_estado} ).done( function( respuesta )
                                            {
                                                $( '#municipio' ).html( respuesta );
                                            });
                                        });

                                        // lista de PARROQUIAS	
                                        $('#municipio').change(function()
                                        {
                                            var id_municipio = $(this).val();
                                            
                                            // Lista de estados
                                            $.post( '<?php echo constant ('URL');?>models/comboubicacion-registro.php', { municipio: id_municipio} ).done( function( respuesta )
                                            {
                                                $( '#parroquia' ).html( respuesta );
                                            });
                                        });

                                        })
                                    </script> 
                                    


                                <h1>Contacto</h1>
                                <fieldset>
                                    <h2>Contacto e Información personal</h2>
                                    <div class="row">
                                        <div class="col-lg-6">


                                        <div class="form-group">
                                        <label>Tipo Correo<span style="color: red;">*</span></label>                                        
                                            <select class="chosen-select form-control required m-b" name="tcorreo" id="tcorreo">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/estructura.php';
                                                foreach($this->tcorreos as $row){
                                                $correo=new Estructura();
                                                $correo=$row;?> 
                                            <option value="<?php echo $correo->id;?>"<?php if($correo->id == $this->informacion->id_correo_tipo){ print "selected=selected"; }?>><?php echo $correo->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>       



                                            <div class="form-group">
                                        <label>Tipo Telefono<span style="color: red;">*</span></label>                                        
                                            <select class="chosen-select form-control required m-b" name="tipotelf" id="tipotelf">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/estructura.php';
                                                foreach($this->telefono_tipo as $row){
                                                $telefono=new Estructura();
                                                $telefono=$row;?> 
                                            <option value="<?php echo $telefono->id;?>"<?php if($telefono->id == $this->informacion->id_telefono_tipo){ print "selected=selected"; }?>><?php echo $telefono->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>  

                        
                            <div class="form-group row">
                            <label class="col-sm-8 col-form-label">¿Posee alguna discapacidad? <span style="color: red;">*</span> </label>

                            <div class="i-checks col-sm-15"  >                                
                            <label>Si
                            <input type="radio"  value="Si" id="optionsRadios1" name="optionsRadios" onChange="DiscapacidadOnChange(this.value)">
                            </label> 
                            <label class="i-checks checkbox-inline" >No                                
                            <input type="radio" <?php if(count($this->informacion_d->id_discapacidad)==0){ echo 'checked=""'; }?> value="No" id="optionsRadios2" name="optionsRadios" onChange="DiscapacidadOnChange(this.value)">
                            </label>

                            </div>
                            </div>
          <script>
            $(document).ready(function () {
                var valor1= $('#optionsRadios1').val();
                var valor2= $('#optionsRadios2').val();
                
                DiscapacidadOnChange(valor1);
                DiscapacidadOnChange(valor2);
            });
        </script>


<script type="text/javascript">
function DiscapacidadOnChange(sel) {
      if (sel=="No"){
           divC = document.getElementById("seccion_Observaciones");
           divC.style.display = "";
           divC = document.getElementById("seccion_Tipo_Discapacidad");
           divC.style.display = "";
           divC = document.getElementById("seccion_Discapacidad");
           divC.style.display = "";

           

           divT = document.getElementById("seccion_inputs_Observaciones");
           divT.style.display = "none";

           divT = document.getElementById("seccion_inputs_Tipo_Discapacidad");
           divT.style.display = "none";

           divT = document.getElementById("seccion_inputs_Discapacidad");
           divT.style.display = "none";

           divT = document.getElementById("seccion_inputs_Codigo");
           divT.style.display = "none";

           
           
      }else{

           divC = document.getElementById("seccion_Observaciones");
           divC.style.display="none";

           divC = document.getElementById("seccion_Tipo_Discapacidad");
           divC.style.display = "none";

           divC = document.getElementById("seccion_Discapacidad");
           divC.style.display = "none";
           
           




           divT = document.getElementById("seccion_inputs_Observaciones");
           divT.style.display = "";
           divT = document.getElementById("seccion_inputs_Tipo_Discapacidad");
           divT.style.display = "";
           divT = document.getElementById("seccion_inputs_Discapacidad");
           divT.style.display = "";
           divT = document.getElementById("seccion_inputs_Codigo");
           divT.style.display = "";
      }
}
</script>





 

                                        </div>
                                        <div class="col-lg-6">
                                       
                                        <div class="form-group">
                                        <label>Correo <span style="color: red;">*</span></label>
                                        <input id="correo" name="correo" type="email" class="form-control" value="<?php echo $this->informacion->correo; ?>" maxlength='30' minlength="5" Placeholder="Escriba su correo">
                                          </div>


                                        <div class="form-row">
                                        <div class="form-group col-md-4">
                                        <label> Cod. Area<span style="color: red;">*</span></label>  
                                        <select class=" form-control required m-b" name="cod_area" id="cod_area">                                    
                                            <option selected="selected" value="">----- </option> 
                                            <?php include_once 'models/estructura.php';
                                                foreach($this->cod_area_telefono as $row){
                                                $cod_area=new Estructura();
                                                $cod_area=$row;?> 
                                            <option value="<?php echo $cod_area->id;?>"<?php if($cod_area->id == $this->informacion->id_telefono_codigo_area){ print "selected=selected"; }?>><?php echo $cod_area->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>                                           </div>
                                        <div class="form-group col-md-8">
                                        <label> Telefono <span style="color: red;">*</span></label>  
                                            <input name="numero" minlength="4" maxlength="8" placeholder="" id="numero" value="<?php echo $this->informacion->telefono; ?>" onkeypress="return valSoloNumeros(event)"  class="form-control required " aria-required="true" type="text" placeholder="Escriba su numero de Telefono">
                                      
                                        </div>
                                        
                                    </div>

 
                                    <div class="form-group"  id="seccion_inputs_Codigo" style="display:;">
                                        <label>Codigo Conadis</label>
                                        <input id="conadis" name="conadis" type="text" class="form-control " value="<?php echo $this->informacion_d->codigo_conadis; ?>" maxlength='15' minlength="3" onkeypress="return valSoloNumeros(event)" placeholder="Escriba su codigo conadis">
                                         </div>  
          

                                 
                                           

                                        </div>


                                    </div>




<div class="form-row">
<div class="form-group col-md-6">
                                        
<div id="seccion_Discapacidad" style="display:none;"></div>

                                 
 
<div class="form-group"  id="seccion_inputs_Tipo_Discapacidad" style="display:;">
<label>Tipo de discapacidad<span style="color: red;">*</span></label>                                        
    <select  class="chosen-select form-control  m-b " name="tdiscapacidad" id="tdiscapacidad">                                    
    <option selected="selected" value="">Seleccione </option> 
    <?php include_once 'models/estructura.php';
        foreach($this->tipo_discapacidades as $row){
        $tipo_discapacidad=new Estructura();
        $tipo_discapacidad=$row;?> 
    <option data-cod="<?php echo $tipo_discapacidad->id;?>" value="<?php echo $tipo_discapacidad->id;?>"<?php if($tipo_discapacidad->id == $this->informacion_d->id_tipo_discapacidad){ print "selected=selected"; }?>><?php echo $tipo_discapacidad->descripcion;?></option>
    <?php }?>                                            
    </select> 
    </div>      
                                    
                                    
   </div>
  <div class="form-group col-md-6">
                                    
<div id="seccion_Tipo_Discapacidad" style="display:none;"></div>



<div class="form-group "  id="seccion_inputs_Discapacidad" style="display:;">
<label>Discapacidad<span style="color: red;">*</span></label>                                        

    <select class="chosen-select form-control m-b" name="discapacidad" id="discapacidad">                                    
    <option selected="selected" value="">Seleccione </option> 
    <?php include_once 'models/estructura.php';
        foreach($this->discapacidades as $row){
        $discapacidad=new Estructura();
        $discapacidad=$row;?> 
    <option data-cod="<?php echo $discapacidad->id;?>" value="<?php echo $discapacidad->id;?>"<?php if($discapacidad->id == $this->informacion_d->discapacidad){ print "selected=selected"; }?>><?php echo $discapacidad->descripcion;?></option>
    <?php }?>                                            
    </select>   
    </div>   
    
                                        
                                    
       </div>
                            
     </div>
                                     
                                     

 




                                <div id="seccion_Observaciones" style="display:none;"></div>

                                <div class="form-group"  id="seccion_inputs_Observaciones" style="display:;">
                                <label>Observaciones<span style="color: red;">*</span></label>
                                <textarea id="observaciones" name="observaciones" type="text" class="form-control" placeholder="Escriba una observación..." aria-required="true" aria-invalid="false" onkeypress="return soloLetras(event)"  maxlength='70' minlength="4"><?php echo $this->informacion_d->observacion; ?></textarea>
                                </div>
                                                
            </fieldset>
                               
                               
                                <h1>Perfil</h1>
                                <fieldset style="height:385px;">
                                    <h2>Perfil de usuario</h2>
                                    <p>
                                        &nbsp;En esta sección podra agregar un tipo de usuario y los privilegios que este tendra dentro del sistema.
                                        </p>
                                    <div class="row">
                                        <div class="col-lg-6">

                                         <div class="form-group">
                                            <label>Perfil<span style="color: red;">*</span></label>                                        
                                            <select  name="perfil" id="perfil"  class="select2_demo_3 form-control required m-b">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/persona.php';
                                                foreach($this->perfiles as $row){
                                                $perfil=new Persona();
                                                $perfil=$row;?> 
                                            <option value="<?php echo $perfil->id;?>" <?php if($this->informacion->id_perfil==$perfil->id){echo "selected='selected'"; }?>><?php echo $perfil->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>   
                                        <script>
                                            $(".select2_demo_3").select2({
                                                placeholder: "Seleccione",
                                                width: "100%",
                                                dropdownAutoWidth: true,
                                                allowClear: true
                                            });
                                        </script>
                                     
                                        
                                         <div class="form-group">
                                         <label>Roles <span style="color: red;">*</span></label>
                                         <select  name="roles[]" id="roles"   data-placeholder="Seleccione sus roles"  class="chosen-select form-control required m-b" multiple style="width:350px;" tabindex="4">
                                            <?php include_once 'models/persona.php';
                                                foreach($this->roles as $row){
                                                $rol=new Persona();
                                                $rol=$row;?> 
                                            <option value="<?php echo $rol->id;?>" <?php if($this->informacion->id_rol==$rol->id){echo "selected='selected'"; }?>><?php echo $rol->descripcion;?></option>
                                            <?php }?>  
                                         </select> 
                                        </div>




                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Contraseña <span style="color: red;">*</span></label>
                                                <input type="password"  placeholder="Ingrese Su Contraseña" name="password" id="password" value="<?php echo $this->informacion->password;?>" class="form-control required">
                                            </div>
                                            <div class="form-group">
                                                <label>Confirme la Contraseña <span style="color: red;">*</span></label>
                                                <input type="password" placeholder="Ingrese Su Contraseña" name="confirm" value="<?php echo $this->informacion->password;?>" id="confirm" class="form-control required">
                                            </div>
                                           
                                            <div class="form-group">
                                            <label>Estatus <span style="color: red;">*</span></label>
                                            <select class="form-control required m-b " name="estatus" id="estatus">                 
                                            <option value="">Seleccione</option>                         
                                            <option value="0" <?php if($this->informacion->usuario_estatus==0){ echo "selected='selected'";} ?>>Inactivo</option>
                                            <option value="1" <?php if($this->informacion->usuario_estatus==1){ echo "selected='selected'";} ?>>Activo</option>                                               
                                            </select>                                              
                                            </div>
                                                       
                                            


                                          
                                        </div>
                                    </div>
                                </fieldset>
                                           
                                                               
                                
                          
                            </form>

                           
                 











                    </div>
                </div>
            </div>





            </div>
        </div>



      

<!--////////////////////////////////-->
  


    <?php require 'views/footer.php'; ?>

   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

<!-- menu active -->
<script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>

<!-- Solo Letras -->
<script src="<?php echo constant ('URL');?>src/js/val_letras.js"></script>
  <!-- Chosen Select -->
  <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script> 
  

   <!-- Data picker -->
   <script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>

  <!-- iCheck -->
  <script src="<?php echo constant ('URL');?>src/js/plugins/iCheck/icheck.min.js"></script>
          <!-- Select2 -->
          <script src="<?php echo constant ('URL');?>src/js/plugins/select2/select2.full.min.js"></script>



                                <!-- combo select de discapacidad tipo-->

                                      





    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>














</body>
</html>
