<!--
*
*  INSPINIA - Diseño de Unidad Curricular Docente
*  version 2.8
*
-->
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin | Comisión o Excedencia </title>
    
    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/all.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">



    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/cropper/cropper.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">






</head>

<body>
    <?php require 'views/header.php'; ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Registrar Comisión o Excedencia</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="<?php echo constant('URL');?>home">Inicio</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="<?php echo constant ('URL');?>comision_excedencia">Desempeño Docente UBV</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Comisión o Excedencia</strong>
                </li>
                <li class="breadcrumb-item active">
                    <a href="<?php echo constant ('URL');?>comision_excedencia/viewEditar"><strong>Editar</strong></a>
                </li>
                    </ol>
                </div>
<div class="col-lg-2">
    <div class="title-action">
        <a href="<?php echo constant ('URL');?>comision_excedencia" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Volver</a>
    </div>

</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
    <div class="ibox ">
    <div class="ibox-title">
        <h5>SIDTA</h5>
    </div>
<div class="ibox-content">
    <div id="respuesta"><?php echo $this->mensaje; ?></div>
    <div class="alert alert-info">Todos los campos marcados con un (<span style="color: red;">*</span>) Son Obligatorios</div>

    <form id="form" method="post" action="<?php echo constant('URL');?>comision_excedencia/registrarComision" class="wizard-big">
        <h1>La Comisión o Excedencia</h1>
    <fieldset>
        <h2>Información del La Comisión o Excedencia</h2>
        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <label>Tipo de Información <span style="color: red;">*</span></label>
                    <select class="form-control m-b chosen-select" tabindex="2" data-placeholder="Elige el Tipo..." name="id_tipo_info" id="id_tipo_info">
                        <option selected="selected" value="">Seleccione</option>
                            <?php include_once 'models/datosacademicos/tipo_info.php';
                                foreach($this->infos as $row){
                                    $tipo_info=new TipoInfo();
                                    $tipo_info=$row;?>
                        <option value="<?php echo $tipo_info->id_tipo_info;?>"><?php echo $tipo_info->descripcion;?></option>
                            <?php }?>
                    </select>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group" id="data_6">
                    <label class="font-normal">Desde <span style="color: red;">*</span></label>
                    <label class="font-normal" style=" position: relative;left: 185px;">Hasta <span style="color: red;">*</span></label>
                    <div class="input-daterange input-group" id="datepicker">
                    <input type="text" class="form-control-sm form-control required" id="desde" name="desde"  placeholder="<?php echo date('d/m/Y');?>" >
                    <span class="input-group-addon">&nbsp;&nbsp;</span>
                    <input type="text" class="form-control-sm form-control required" id="hasta" name="hasta" placeholder="<?php echo date('d/m/Y');?>"  >
                </div>
                    <label id="desde-error" class="error" for="desde"></label>
                    <label id="hasta-error" class="error" for="hasta"></label>
                    <span><i style="margin-right: 12.5em;">mm/dd/aaaa</i><i>mm/dd/aaaa</i></span>
                </div>
                <script>
                    $(document).ready(function(){
                    $('#data_6 .input-daterange').datepicker({
                        keyboardNavigation: false,
                        format: "yyyy/mm/dd",
                        forceParse: false,
                        autoclose: true
                    });
                });
                </script>
            </div>
                <div class="col-lg-4">
                <input type="hidden" name="registrar">
                    <div class="text-center">
                        <div style="margin-top: 20px">
                            <i class="fa fa-vcard-o" style="font-size: 180px;color:  #265491  "></i>
                        </div>
                    </div>
            </div>
        </div>
    </fieldset>
            <h1>La Comisión o Excedencia</h1>
    <fieldset>
        <h2>Información del La Comisión o Excedencia</h2>
        <div class="row">
            <div class="col-lg-4">
            <div class="form-group">
                    <label>Lugar <span style="color: red;">*</span></label>
                    <textarea id="lugar" name="lugar" type="text" placeholder="Ingrese la descripción del lugar" maxlength="70" class="form-control required"></textarea>
                </div>
                <div class="form-group">
                    <label>Designación de la Función<span style="color: red;">*</span></label>
                    <textarea id="designacion_funcion" name="designacion_funcion" type="text" placeholder="Ingrese la descripcion de designacion de función" maxlength="70" class="form-control required"></textarea>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label>Causa<span style="color: red;">*</span></label>
                    <textarea id="causa" name="causa" type="text" placeholder="Ingrese la descripción de la causa" maxlength="70" class="form-control required"></textarea>
                </div>
                <div class="form-group">
                <div class="text-center">
                    <label>Aprobación <span style="color: red;">*</span></label>
                        <div class="radio radio-info">
                            <label for="SI">
                            <input type="radio" value="SI" id="aprobacion" name="aprobacion"  checked=""> <i></i> 
                                SI
                            </label>
                        </div>
                        <div class="radio radio-info">
                        <label for="NO">
                        <input type="radio" value="NO" id="aprobacion" name="aprobacion"  > <i></i> 
                                NO
                        </label>
                        </div>
                </div>
                </div>
                                            <!--   <div class="form-group">
                                    <label>Estatus Verificación<span style="color: red;">*</span></label>
                                        <select class="form-control required m-b " name="estatus_verificacion" id="estatus-verificacion">
                                            <option value="">Seleccione</option>
                                            <option value="Activo"> Verificado</option>
                                            <option value="Inactivo"> Sin Verificar</option>
                                        </select>
                                </div> -->
            </div>
            <div class="col-lg-4">
                <input type="hidden" name="registrar">
                    <div class="text-center">
                        <div style="margin-top: 20px">
                            <i class="fa fa-vcard-o" style="font-size: 180px;color:  #265491  "></i>
                        </div>
                    </div>
            </div>
        </div>

    </fieldset>
    </form>
                </div>
            </div>
        </div>
    </div>
</div>

    <?php require 'views/footer.php'; ?>

    
   <!-- dataTables -->
   <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    <!-- Chosen -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>

     <!-- Sweet alert -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/sweetalert/sweetalert.min.js"></script>

<!-- Input Mask-->
<script src="<?php echo constant ('URL');?>src/js/plugins/jasny/jasny-bootstrap.min.js"></script>

    <!-- Select2 -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/select2/select2.full.min.js"></script>

 <!-- Data picker -->
 <script src="<?php echo constant ('URL');?>src/js/plugins/datapicker/bootstrap-datepicker.js"></script>
                                                    
<!--
    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");
    $("#re").addClass("active");
});
</script>-->



    <script>
        $('#myModal1').on('shown.bs.modal', function () {  
       $('.chosen-select').chosen({width: "100%"});


       $('.tagsinput').tagsinput({
             tagClass: 'label label-primary'
         });

         $('#data_5 .input-daterange').datepicker({
             keyboardNavigation: false,
             forceParse: false,
             autoclose: true
             
         });

         $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green'
            });

       });
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<!-- modal ver -->
<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form3").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    //form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [

                ]

            });

        });

    </script>
</body>
</html>
