<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin | Comision o Excedencia</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

 


</head>

<body>
    <?php require 'views/header.php'; ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Registrar La Comision o Excedencia</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="<?php echo constant('URL');?>home">Inicio</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="<?php echo constant ('URL');?>comision_excedencia">Desempeño Docente UBV</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Comisión o Excedencia</strong>
                </li>
                <li class="breadcrumb-item active">
                    <a href="<?php echo constant ('URL');?>comision_excedencia/viewRegistrar"><strong>Registrar</strong></a>
                </li>
            </ol>
    </div>
<div class="col-lg-2">
    <div class="title-action">
        <a href="<?php echo constant ('URL');?>comision_excedencia/viewRegistrar" class="btn btn-primary"><i class="fa fa-plus"></i> Agregar Comisión</a>
    </div>
</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>SIDTA</h5>
            </div>
        <div class="ibox-content">
            <div id="respuesta"><?php echo $this->mensaje; ?></div>
            <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Tipo Información</th>
                        <th>Lugar</th>
                        <th>Desde</th>
                        <th>Hasta</th>
                        <th>Designación de Función</th>
                        <th>Causa</th>
                        <th>Aprobación</th>
                        <th>Estatus</th>
                        <th>Acción</th>
                    </tr>
                    </thead>
                    <tbody id="tbody-comisiones">
                    <?php include_once 'models/datosacademicos/comision_excedencia.php';
                       $fecha_actual=date('Y-m-d');
                            foreach($this->comisiones as $row){
                                $comision_excedencia= new Comision();
                                $comision_excedencia=$row; ?>
                    <tr id ="fila-<?php echo $comision_excedencia->id_comision_excedencia; ?>" class="gradeX">
                        <td><?php echo $comision_excedencia->descripcion_tipo_info; ?> </td>
                        <td><?php echo $comision_excedencia->lugar; ?> </td>
                        <td><?php echo date("d/m/Y", strtotime($comision_excedencia->desde)); ?> </td>
                        <td><?php echo date("d/m/Y", strtotime($comision_excedencia->hasta)); ?> </td>
                        <td><?php echo $comision_excedencia->designacion_funcion; ?> </td>
                        <td><?php echo $comision_excedencia->causa; ?> </td>
                        <td><?php echo $comision_excedencia->aprobacion; ?> </td>
                        <td style="text-align: center;"><?php if($comision_excedencia->estatus_verificacion=='Verificado'){ echo '<span style="font-size: 12px;" class="label label-primary"> '.$comision_excedencia->estatus_verificacion.'</span>';} else{ echo '<span class="label label-danger" style="font-size: 12px;"> '.$comision_excedencia->estatus_verificacion.'</span>';}?></td>
                        <td>
                        <a class="btn btn-outline btn-success" href="<?php echo constant('URL').'comision_excedencia/viewEditar/'.$comision_excedencia->id_comision_excedencia;?>"  role="button"><i class="fa fa-edit"></i> Editar</a>&nbsp;
                            <a class="btn btn-outline btn-primary" href="<?php echo constant('URL').'comision_excedencia/viewVer/'.$comision_excedencia->id_comision_excedencia;?>"  role="button"><i class="fa fa-search"></i> Ver</a>&nbsp;

                            <!-- <a class="btn btn-outline btn-danger" href="<?php echo constant('URL') . 'comision_excedencia/deletecomision/' . $comision_excedencia->id_comision_excedencia;?>" role="button"> Remover</a> &nbsp; -->
                            <!-- <button class="btn btn-outline btn-danger bEliminar" data-id="<?php echo $comision_excedencia->id_comision_excedencia;?>"><i class="fa fa-trash"></i> Eliminar</button> -->
                        </td>
                    </tr>
                            <?php }?>
                    </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


    <?php require 'views/footer.php'; ?>

        <!-- Mainly scripts -->
    <script src="<?php echo constant ('URL');?>src/js/jquery-3.1.1.min.js"></script>
    <!-- ajax de eliminar tipo_info -->
    <script type="text/javascript" src="<?php echo constant('URL');?>src/js/datos_academicos/eliminarComision.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/popper.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/bootstrap.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo constant ('URL');?>src/js/inspinia.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/pace/pace.min.js"></script>

   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>

<!--
    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");
    $("#re").addClass("active");
});
</script>-->





    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>
<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form3").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    //form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>
    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>





</body>
</html>
