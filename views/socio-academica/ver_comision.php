
<!--
*
*  INSPINIA - Diseño de Unidad Curricular Docente
*  version 2.8
*
-->
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin | Comision o Excedencia  </title>
    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

     <!--  style chosen lo lo puse -->
     <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

    <!-- jasny input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">
    <!--  style select2 -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/select2/select2.min.css" rel="stylesheet">
    <!-- datapicker -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <!-- sweetalert input mask-->
    <link href="<?php echo constant ('URL');?>src/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

</head>

<body>
    <?php require 'views/header.php'; ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-7">
        <h2>Detalle  Comision o Excedencia </h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="<?php echo constant('URL');?>home">Inicio</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="<?php echo constant ('URL');?>comision_excedencia">Desempeño Docente UBV</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="">Listar Comision o Excedencia</a>
                </li>
                <li class="breadcrumb-item active">
                    <a href="<?php echo constant ('URL');?>comision_excedencia/viewVer"> <strong>Detalle </strong></a>
                </li>
            </ol>
    </div>
<div class="col-lg-5">
    <div class="title-action">
        <a href="<?php echo constant ('URL');?>comision_excedencia" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Volver</a>
    </div>
</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>SIDTA</h5>
                    <div class="ibox-tools">
                    </div>
                </div>
                <div class="ibox-content">
                    <!--  contenido -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="tabs-container">
                                <div class="tabs-right">
                                    <ul class="nav nav-tabs">
                                        <li><a class="nav-link active" data-toggle="tab" href="#tab-8"> Datos de la Comisión</a></li>
                                        <li><a class="nav-link" data-toggle="tab" href="#tab-9">Observación</a></li>
                                <div class="col-lg-12">
                                    <div class="text-center">
                                        <div style="margin-top: 30px">
                                            <i class="fa fa-vcard-o" style="font-size: 180px;color: #265491 "></i>
                                        </div>
                                    </div>
                                </div>
                                    </ul>
                            <div class="tab-content">
                                <div id="tab-8" class="tab-pane active">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <h3> Tipo Información:</h3>
                                                <p><?php echo $this->comisiones->descripcion_tipo_info; ?></p>
                                            </div>
                                            <div class="col-lg-6">
                                                <h3> Aprobación:</h3>
                                                <p><?php echo $this->comisiones->aprobacion; ?></p>
                                            </div>
                                        </div>
                                            <hr>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <h3> Desde</h3>
                                                <p><?php echo $this->comisiones->desde; ?></p>
                                                <p></p>
                                            </div>
                                            <div class="col-lg-6">
                                                <h3> Hasta</h3>
                                                <p><?php echo $this->comisiones->hasta; ?></p>
                                                <p></p>
                                            </div>
                                        </div>
                                            <hr>
                                    </div>
                                </div>
                                <div id="tab-9" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                    <h3> Lugar:</h3>
                                                    <p><?php echo $this->comisiones->lugar; ?></p>
                                            </div>
                                            <div class="col-lg-6">
                                                    <h3> Causa:</h3>
                                                    <p><?php echo $this->comisiones->causa; ?></p>
                                            </div>
                                        </div>
                                            <hr>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <h3> Designación de Función:</h3>
                                                <p><?php echo $this->comisiones->designacion_funcion; ?></p>
                                            </div>
                                            <div class="col-lg-6">
                                                <h3> Estatus Verificación:</h3>
                                                <p><?php echo $this->comisiones->estatus_verificacion; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                        <input id="id_comision_excedencia" name="id_comision_excedencia"  disabled type="hidden"  value="<?php echo $this->docencia->id_docencia_previa_ubv;  ?>">
                                        <input type="hidden" name="registrar">
                                </div>
                                <div id="tab-10" class="tab-pane">
                                    <div class="panel-body">
                                    </div>
                                </div>
                            </div>
                            </div>
                                </div>
                            </div>
                        <!--  end contenido -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <?php require 'views/footer.php'; ?>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    <!-- Chosen -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script>

<!--
    <script>
$(function(){
    $("li").removeClass("active");
    $("#Extructura").addClass("active");
    $("#re").addClass("active");
});
</script>-->



    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {true
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";true

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {true
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        }/*,
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            },
                            ano_arte:{
                                required: true
                            },
                            entidad_promotora: {
                                required: true
                            },
                            url_otro: {
                                url: true
                            }

                        }*/
                    });
       });
    </script>


</body>
</html>
