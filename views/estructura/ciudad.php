<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin | Registrar Ciudad</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">




</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Ciudad</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">
                            <a href="<?php echo constant ('URL');?>ciudad">Extructura UBV</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Registrar Estructura</a>
                        </li>
                        <li class="breadcrumb-item ">
                            <strong>Registrar Ciudad</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Listado de Ciudades Registradas</h5>
                       
                        <div class="ibox-tools">
                        <!-- boton agregar-->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                        Registrar Ciudad
                                </button> 


                        </div>
                    </div>
                    
                    <div class="ibox-content">
                    <?php echo $this->mensaje; ?>
                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Código INE</th>
                        <th>Ciudad</th>
                        <th>Estado de adscripción</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>

                    
                    <?php include_once 'models/estructura.php';
                            foreach($this->ciudades as $row){
                                $ciudad= new Estructura();
                                $ciudad=$row;?>
                    <tr class="gradeX">
                    <td><?php echo $ciudad->codigo; ?> </td>
                    <td><?php echo $ciudad->ciudad; ?></td>
                    <td><?php echo $ciudad->estado; ?></td>
                    <td class="center">
                    <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal" data-id="<?php echo $ciudad->id_ciudad;?>" data-nombre="<?php echo  $ciudad->ciudad;?>"  data-cod="<?php echo $ciudad->codigo; ?>"data-estado="<?php echo $ciudad->id_estado; ?>" >&nbsp; Editar &nbsp;</a> &nbsp;
                
                    </td>
                    </tr>
                            <?php }?>
                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
<!-- Modal para Agregar estados-->
        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Registrar Ciudad </h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            <h2>
                            Registrar Ciudad 
                            </h2>
                            <p>
                            &nbsp;Registrar las ciudades como división política
                            intermedia entre el estado y el municipio pero solo
                            en las áreas urbanas ya que es requerida para
                            registrar la dirección completa de una institución o
                            persona.     </p>

                            <form id="form" action="<?php echo constant('URL');?>ciudad/registrarCiudad"  method="post" class="wizard-big">
                                <h1>Descripción</h1>
                                <fieldset>
                                    <h2>Ciudad Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">

                                        <div class="form-group">
                                            <label>Estado de adscripción <span style="color: red;">*</span></label>
                                            <select class="chosen-select form-control required m-b " name="estado" id="estado">
                                           <option selected="selected" value="">Seleccione</option> 
                                            <?php include_once 'models/estructura.php';
                                          foreach($this->estados as $row){
                                          $estado=new Estructura();
                                            $estado=$row;?> 
                                            <option value="<?php echo $estado->id_estado;?>"><?php echo $estado->descripcion;?></option>
                                            <?php }?>                                               
                                            </select>                                         
                                            </div>

                                            <div class="form-group">
                                                <label>Código INE <span style="color: red;">*</span></label>
                                                <input id="codigoine" name="codigoine" type="text" class="form-control required number" maxlength='6' minlength="3">
                                            </div>
                                            <div class="form-group">
                                                <label>Ciudad <span style="color: red;">*</span></label>
                                                <input id="ciudad" name="ciudad" type="text" class="form-control required" onkeypress="return soloLetras(event)"  maxlength='70' minlength="4">
                                            </div>
                                       
                                            

                                           
                                        </div>
                                        
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-globe" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                                
                          
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
         

        
             
<!-- Modal para Editar estados-->
        <div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Editar  Ciudad </h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                                        <h2>
                                        
                                        </h2>
                                        <p> </p>

                            <form id="form2" action="<?php echo constant('URL');?>ciudad/ActualizarCiudad"  method="post" class="wizard-big">
                                <h1>Descripción</h1>
                                <fieldset>
                                    <h2>País Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">

                                        <div class="form-group">
                                            <label>Estado de adscripción <span style="color: red;">*</span></label>
                                            <select class="chosen-select form-control required m-b " name="estadoedit" id="estadoedit">
                                            <option selected="selected" value="">Seleccione</option> 
                                            <?php include_once 'models/estructura.php';
                                          foreach($this->estados as $row){
                                          $estado=new Estructura();
                                            $estado=$row;?> 
                                            <option value="<?php echo $estado->id_estado;?>"><?php echo $estado->descripcion;?></option>
                                            <?php }?>                                               
                                            </select>   
                                          </div>

                                            <div class="form-group">
                                            <input id="id_ciudad" name="id_ciudad" type="hidden" class="form-control">
                                                <label>Código INE <span style="color: red;">*</span></label>
                                                <input id="codigo" name="codigo" type="text" class="form-control required number" maxlength='6' minlength="3">
                                            </div>
                                            <div class="form-group">
                                                <label>Ciudad <span style="color: red;">*</span></label>
                                                <input id="ciudadedit" name="ciudadedit" type="text" class="form-control required" onkeypress="return soloLetras(event)"  maxlength='70' minlength="4">
                                            </div>
                                       
                                       

                                        </div>
                                    
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-globe" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                                
                          
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
    




   
    <?php require 'views/footer.php'; ?>

   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
        <!-- Solo Letras -->
        <script src="<?php echo constant ('URL');?>src/js/val_letras.js"></script>
  
           <!-- Chosen Select -->
   <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script> 

    <script>
  $('#myModal1').on('shown.bs.modal', function () {  
       $('.chosen-select').chosen({width: "100%"});
       });

$('#myModal2').on('show.bs.modal', function(e) {
  var product = $(e.relatedTarget).data('id');
  $("#id_ciudad").val(product);

  var product2 = $(e.relatedTarget).data('nombre');
  $("#ciudadedit").val(product2);

  var product3 = $(e.relatedTarget).data('cod');
  $("#codigo").val(product3);

  var product4 = $(e.relatedTarget).data('estado');
  $("#estadoedit").val(product4);

  $('.chosen-select').chosen({width: "100%"});
  $('#estadoedit').trigger('chosen:updated');

});
</script>


    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>



    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>




</body>
</html>
