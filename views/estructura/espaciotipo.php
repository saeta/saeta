<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Registrar Tipo de Espacio | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">




</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-8">
                    <h2>Tipo de Espacio</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>espaciotipo">Estructura UBV</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Registrar Estructura</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Registrar Tipo de Espacio</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-4">
                    <div class="title-action">
                    <?php  if($_SESSION['Agregar']==true){?>

                    <!-- boton agregar-->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                        <i class="fa fa-plus"></i> Registrar tipos de espacios
                                </button> 
                    <?php } ?>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Listado de Espacios Según su Tipo </h5>
                       
                        <div class="ibox-tools">
                        


                        </div>
                    </div>
                    
                    <div class="ibox-content">
                    <?php echo $this->mensaje; ?>
                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Tipo de Espacio</th>
                      
                        <th>Acciones</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php include_once 'models/estructura.php';
                            foreach($this->espacios as $row){
                                $espacio= new Estructura();
                                $espacio=$row;?>
                    <tr class="gradeX">
                    <td><?php echo $espacio->descripcion; ?> </td>
                                   
                    
                    <td> 
                    <?php  if($_SESSION['Editar']==true){?>

                    <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal"   data-id="<?php echo $espacio->id_espacio_tipo;?>" data-espacio="<?php echo $espacio->descripcion; ?>" >&nbsp; Editar &nbsp;</a> &nbsp;
                    <?php } ?>
                  </td>
                    </tr>
                            <?php }?>
                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
<!-- ///////////////Modal Agregar////////////////// -->
        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Registrar Tipo de Espacio</h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            <h2>
                            Registrar Tipo de Espacio
                            </h2>
                            <p>
                            &nbsp;Le permite al usuario registrar los distintos
tipos de espacios con los que cuenta la UBV.
                            </p>

                            <form id="form" action="<?php echo constant('URL');?>espaciotipo/registrarEspacio"  method="post" class="wizard-big">
                                <h1>Descripción</h1>
                                <fieldset>
                                    <h2>Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                            <label>Espacio <span style="color: red;">*</span></label>
                                            <select class="chosen-select form-control required m-b " name="espacio" id="espacio">
                                            <option value="" selected="selected">Seleccione</option>
                                            <option value ="Usos Múltiples">Usos Múltiples</option>   
                                            <option value ="Aula">Aula</option>      
                                            <option value ="Oficina">Oficina</option>      
                                            <option value ="Especial">Especial</option>      
                                            <option value ="Laboratorio">Laboratorio</option>    
                                            <option value ="Cancha">Cancha</option>    
                                            <option value ="Espacio">Espacio</option>    
                                            <option value ="deportiva">deportiva</option>                                              
                                            </select>                                              
                                            </div>
                                            
                                          
                            </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-linode" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                               
                                
                          
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
  
<!-- ///////////////Modal Agregar////////////////// -->
<!-- ///////////////Modal editar////////////////// -->
<div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                <h4 class="modal-title">Editar Tipo de Espacio   </h4>
                <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
            </div>
            <div class="modal-body" >
                
            <div class="ibox-content">
            <h2>
            Editar Tipo de Espacio
            </h2>
            <p>
            &nbsp;Le permite al usuario actualizar los distintos
            tipos de espacios con los que cuenta la UBV.
            </p>

<form id="form2" action="<?php echo constant('URL');?>espaciotipo/ActualizarEspacio"  method="post" class="wizard-big">
    <h1>Descripción</h1>
    <fieldset>
        <h2>Información</h2>
        <div class="row">
            <div class="col-lg-8">
               
                <input id="id_espacio_tipo" name="id_espacio_tipo" type="hidden" class="form-control">
                   
                                           <div class="form-group">
                                            <label>Espacio <span style="color: red;">*</span></label>
                                            <select class="chosen-select form-control required m-b " name="espaciotipo" id="espaciotipo">
                                            <option value="" selected="selected">Seleccione</option>
                                            <option value ="Usos Múltiples">Usos Múltiples</option>   
                                            <option value ="Aula">Aula</option>      
                                            <option value ="Oficina">Oficina</option>      
                                            <option value ="Especial">Especial</option>      
                                            <option value ="Laboratorio">Laboratorio</option>    
                                            <option value ="Cancha">Cancha</option>    
                                            <option value ="Espacio">Espacio</option>    
                                            <option value ="deportiva">deportiva</option>                                              
                                            </select>                                              
                                            </div>


                </div>
            <div class="col-lg-4">
                <div class="text-center">
                    <div style="margin-top: 20px">
                        <i class="fa fa-linode" style="font-size: 180px;color: #e5e5e5 "></i>
                    </div>
                </div>
            </div>
        </div>

    </fieldset>

</form>
</div>
            
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                <!--<button type="button" class="btn btn-primary">Save changes</button>-->
      </div>
  </div>
</div>
</div>        

     

<!--////////////////////////////////-->        




    <?php require 'views/footer.php'; ?>

    
   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    <!-- Chosen Select -->
   <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script> 
   



<script>
        $('#myModal1').on('shown.bs.modal', function () {  
       $('.chosen-select').chosen({width: "100%"});
       });
            
$('#myModal2').on('show.bs.modal', function(e) {
    
  var product = $(e.relatedTarget).data('id');
  $("#id_espacio_tipo").val(product);

  var product2 = $(e.relatedTarget).data('espacio');
  $("#espaciotipo").val(product2);
  $('.chosen-select').chosen({width: "100%"});
  $('#espaciotipo').trigger('chosen:updated');


});
</script>


    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>





</body>
</html>
