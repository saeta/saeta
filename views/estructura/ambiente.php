<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Ambiente | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

 


</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-9">
                    <h2><i class="fa fa-globe"></i> Ambiente</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>ambiente">Extructura UBV</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Ubicación Geografica</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Editar Ambiente</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-3">
                    <div class="title-action">
                    <?php  if($_SESSION['Agregar']==true){?>

                        <!-- boton agregar-->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                            <i class="fa fa-plus"></i> Registrar Ambiente
                        </button>
                    <?php } ?> 
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5><i class="fa fa-globe"></i> Listado de Ambientes </h5>
                       
                        <div class="ibox-tools">
                        


                        </div>
                    </div>
                    
                    <div class="ibox-content">
                    <?php echo $this->mensaje; ?>
                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Código (INE)</th>
                        <th>Ambiente</th>
                        <th><i class="fa fa-globe"></i> Parroquia</th>
                        <th><i class="fa fa-location-arrow"></i> Dirección</th>
                        <th>Estatus</th>
                        <th><i class="fa fa-globe"></i> Coordenadas Geograficas</th>
                        <th>Acciones</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php include_once 'models/estructura.php';
                            foreach($this->ambientes as $row){
                                $ambiente= new Estructura();
                                $ambiente=$row;?>
                    <tr class="gradeX">
                    <td><?php echo $ambiente->codigo; ?> </td>
                    <td><?php echo $ambiente->ambiente; ?></td>
                    <td><?php echo $ambiente->parroquia; ?></td>
                    <td><?php echo $ambiente->direccion; ?></td>
                    <td> <?php if($ambiente->estatus==0){
                      echo"<center style=margin-top:3%;><span class='label label-danger' style='font-size: 12px;'>Inactivo</span></center>";
                    
                    }else{
                        echo"<center style=margin-top:3%;><span class='label label-primary' style='font-size: 12px;'>&nbsp; Activo &nbsp;</span></center>";         } ?>
                    
                    </td>
                    <td><?php echo "Latitud : ".$ambiente->coordenada_latitud." Longitud : ".$ambiente->coordenada_longitd; ?></td>
                 
                  
                    
                    <td> 
                    <?php  if($_SESSION['Editar']==true){?>

                    <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal" data-id="<?php echo $ambiente->id_ambiente;?>" data-cod="<?php echo $ambiente->codigo;?>"   data-ambiente="<?php echo $ambiente->ambiente;?>"  data-estatus="<?php echo $ambiente->estatus;?>"  data-parroquia="<?php echo $ambiente->id_parroquia;?>" data-latitud="<?php echo $ambiente->coordenada_latitud;?>" data-logitud="<?php echo $ambiente->coordenada_longitd;?>" data-direccion="<?php echo $ambiente->direccion;?>"><i class="fa fa-edit"></i> Editar</a> 
                    <?php } ?>
                  </td>
                    </tr>
                            <?php }?>
                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
<!-- ///////////////Modal Agregar////////////////// -->
        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Registar Ambiente </h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            <h2>
                            Registar Ambiente
                            </h2>
                            <p>
                            &nbsp;Permite al usuario registrar un ambiente donde existe una o mas aldeas de la UBV o de la
                          misión Sucre donde se dicten programas de la UBV.
                            </p>

                            <form id="form" action="<?php echo constant('URL');?>ambiente/registrarAmbiente"  method="post" class="wizard-big">
                                <h1>Descripción</h1>
                                <fieldset>
                                    <h2>Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <label>Código de Ambiente <span style="color: red;">*</span></label>
                                                <input id="codigoambiente" placeholder="Ingrese el Código del Ambiente" name="codigoambiente" type="text" class="form-control required number"  maxlength='6' minlength="3">
                                            </div>
                                            
                                            <div class="form-group">
                                                <label> Ambiente <span style="color: red;">*</span></label>
                                                <input id="ambiente" placeholder="Ingrese el Nombre del Ambiente" name="ambiente" type="text" class="form-control required"  onkeypress="return soloLetras(event)"  maxlength='70' minlength="4">
                                            </div>
                                            <div class="form-group">
                                            <label>Estatus <span style="color: red;">*</span></label>
                                            <select class="form-control required m-b " name="estatus" id="estatus">
                                               <option value ="1">Activo</option>
                                                <option selected="selected" value="0">Inactivo</option>
                                               
                                            </select>   
                                            </select>
                                            </div>
                                          
                            </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-snowflake-o" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                                <h1>Ubicación</h1>
                                <fieldset>
                                    <h2>Información</h2>
                                    <div class="form-group">
                                            <label>Parroquia<span style="color: red;">*</span></label>
                                        
                                            <select class="form-control required m-b " name="parroquia" id="parroquia">
                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/estructura.php';
                                                foreach($this->parroquias as $row){
                                                $parroquia=new Estructura();
                                                $parroquia=$row;?> 
                                            <option value="<?php echo $parroquia->id_parroquia;?>"><?php echo $parroquia->parroquia;?></option>
                                            <?php }?>
                                            
                                            </select>   
                                            </select>

                                            
                                            </div>
                                    <div class="row">
                                        
                                        <div class="col-lg-6">                                    
                                            <div class="form-group">
                                                <label>Latitud</label>
                                                <input id="latitud" name="latitud" placeholder="Ingrese la Latitud del Ambiente" type="number" class="form-control required">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                       

                                            <div class="form-group">
                                                <label>Longitud</label>
                                                <input id="logitud" name="logitud" type="number" placeholder="Ingrese la Longitud del Ambiente" class="form-control required">
                                            </div>
                                           
                                        </div>
                                        
                                    </div>
                                    <div class="form-group">
                                                <label>Dirección</label>
                                            
                                                <textarea id="direccion" name="direccion" type="text" class="form-control required" placeholder="Ingrese la Dirección Geográfica del Ambiente..."  maxlength='255' minlength="5"></textarea>
                                         
                                            </div>
                                </fieldset>
                                
                          
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
  
<!-- ///////////////Modal Agregar////////////////// -->
<div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Editar Ambiente </h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                                        <h2>
                                        Editar Ambiente
                                        </h2>
                                        <p>
                                        &nbsp;Permite al usuario actualizar un ambiente donde existe una o mas aldeas de la UBV o de la
                                      misión Sucre donde se dicten programas de la UBV.
                                        </p>

                            <form id="form2" action="<?php echo constant('URL');?>ambiente/ActualizarAmbiente"  method="post" class="wizard-big">
                                <h1>Descripción</h1>
                                <fieldset>
                                    <h2>Registar Ambiente </h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                            <input id="id_ambiente" name="id_ambiente" type="hidden" class="form-control">
 

                                                <label>Código de Ambiente <span style="color: red;">*</span></label>
                                                <input id="codigoambienteedit" placeholder="Ingrese el Código del Ambiente" name="codigoambienteedit" type="text" class="form-control required number"  maxlength='6' minlength="3">
                                            </div>
                                            
                                            <div class="form-group">
                                                <label> Ambiente <span style="color: red;">*</span></label>
                                                <input id="ambienteedit" placeholder="Ingrese el Nombre del Ambiente" name="ambienteedit" type="text" class="form-control required"  onkeypress="return soloLetras(event)"  maxlength='70' minlength="4">
                                            </div>
                                            <div class="form-group">
                                            <label>Estatus <span style="color: red;">*</span></label>
                                            <select class="form-control required m-b " name="estatusedit" id="estatusedit">
                                                <option value ="1">Activo</option>
                                                <option selected="selected" value="0">Inactivo</option>
                                               
                                            </select>   
                                            </select>
                                            </div>
                                          
                            </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-snowflake-o" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                                <h1>Ubicación</h1>
                                <fieldset>
                                    <h2>Información</h2>
                                    <div class="form-group">
                                            <label>Parroquia<span style="color: red;">*</span></label>
                                        
                                            <select class="form-control required m-b " name="parroquiaedit" id="parroquiaedit">
                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/estructura.php';
                                                foreach($this->parroquias as $row){
                                                $parroquia=new Estructura();
                                                $parroquia=$row;?> 
                                            <option value="<?php echo $parroquia->id_parroquia;?>"><?php echo $parroquia->parroquia;?></option>
                                            <?php }?>
                                            
                                            </select>   
                                            </select>

                                            
                                            </div>
                                    <div class="row">
                                        
                                        <div class="col-lg-6">                                    
                                            <div class="form-group">
                                                <label>Latitud</label>
                                                <input id="latitudedit" placeholder="Ingrese la Latitud del Ambiente"  name="latitudedit" type="number" class="form-control required">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                       

                                            <div class="form-group">
                                                <label>Longitud</label>
                                                <input id="logitudedit" placeholder="Ingrese la Longitud del Ambiente" name="logitudedit" type="number" class="form-control required">
                                            </div>
                                           
                                        </div>
                                        
                                    </div>
                                    <div class="form-group">
                                                <label>Dirección</label>
                                            
                                                <textarea id="direccionedit" name="direccionedit" type="text" class="form-control required" placeholder="Ingrese la Dirección del Ambiente"   maxlength='255' minlength="5"></textarea>
                                         
                                            </div>
                                </fieldset>
                                
                          
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->        




    <?php require 'views/footer.php'; ?>

       
   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

<!-- menu active -->
<script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
  
 <!-- Solo Letras -->
 <script src="<?php echo constant ('URL');?>src/js/val_letras.js"></script>


<script>
                        
            
$('#myModal2').on('show.bs.modal', function(e) {
    
  var product = $(e.relatedTarget).data('id');
  $("#id_ambiente").val(product);

  var product2 = $(e.relatedTarget).data('cod');
  $("#codigoambienteedit").val(product2);

  var product2 = $(e.relatedTarget).data('ambiente');
  $("#ambienteedit").val(product2);

  var product3 = $(e.relatedTarget).data('estatus');
  $("#estatusedit").val(product3);

  var product4 = $(e.relatedTarget).data('parroquia');
  $("#parroquiaedit").val(product4);

  var product5 = $(e.relatedTarget).data('latitud');
  $("#latitudedit").val(product5);

  var product6 = $(e.relatedTarget).data('logitud');
  $("#logitudedit").val(product6);

  var product7 = $(e.relatedTarget).data('direccion');
  $("#direccionedit").val(product7);



});
</script>


    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>





</body>
</html>
