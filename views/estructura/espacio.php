<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Espacio | SIDTA</title>
    <link rel="shortcut icon" type="image/png" href="<?php echo constant ('URL');?>src/img/favicon.png"/> 
    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">



</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Espacio</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>espacio">Estructura UBV</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Ubicación Geográfica</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Editar Espacio</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                    <div class="title-action">
                    <?php  if($_SESSION['Agregar']==true){?>

                        <!-- boton agregar-->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                            <i class="fa fa-plus"></i> Registrar Espacio
                        </button> 
                    <?php } ?>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Listado de Espacios </h5>
                       
                        <div class="ibox-tools">
                        


                        </div>
                    </div>
                  
                    <div class="ibox-content">
                    <?php echo $this->mensaje; ?>
                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Espacio</th>
                        <th>Tipo de espacio</th>
                        <th>Capacidad</th>
                        <th>Ubicación (dentro de la Estructura física)</th>
                        <th>Ambiente</th>
                        <th>Estatus</th>
                    
                        <th>Acciones</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php include_once 'models/estructura.php';
                            foreach($this->espacios as $row){
                                $espacio= new Estructura();
                                $espacio=$row;?>
                    <tr class="gradeX">
                    <td><?php echo $espacio->espacio; ?> </td>
                    <td><?php echo $espacio->tipoespacio; ?></td>
                    <td><?php echo $espacio->capacidad; ?></td>
                    <td><?php echo $espacio->estructuraf; ?></td>
                    <td><?php echo $espacio->ambiente; ?></td>
                  
                    <td> <?php if($espacio->estatus_ambiente_espacio==0){
                       echo"<center style=margin-top:3%;><span class='label label-danger' style='font-size: 12px;'>Inactivo</span></center>";
                    }else{
                        echo"<center style=margin-top:3%;><span class='label label-primary' style='font-size: 12px;'>&nbsp; Activo &nbsp;</span></center>";
                
                    } ?>
                    
                    </td>                   
                    
                    <td> 

                      <!--  <a class="btn btn-outline btn-info" href="#myModal3" role="button" data-toggle="modal" data-id="<?php echo $espacio->id_ambiente_espacio;?>" data-espacio="<?php echo $espacio->espacio;?>"  data-tipo="<?php echo $espacio->id_espacio_tipo;?>"   data-capacidad="<?php echo $espacio->capacidad;?>"  data-estructuraf="<?php echo $espacio->id_ambiente_detalle;?>"  data-ambiente="<?php echo $espacio->id_ambiente;?>" data-estatus="<?php echo $espacio->estatus;?>" > Ver</a> &nbsp;-->
                       <?php if($espacio->estatus_estructuraf==0){ ?>
                        <a class="btn btn-outline btn-danger" href="#" title="Extructura física inactiva" >Inactivo</a> &nbsp;
                        <?php }else{
                             if($_SESSION['Editar']==true){?>

                        <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal" data-id="<?php echo $espacio->id_ambiente_espacio;?>" data-espacio="<?php echo $espacio->espacio;?>"  data-tipo="<?php echo $espacio->id_espacio_tipo;?>"   data-capacidad="<?php echo $espacio->capacidad;?>"  data-estructuraf="<?php echo $espacio->id_ambiente_detalle;?>"  data-ambiente="<?php echo $espacio->id_ambiente;?>" data-estatus="<?php echo $espacio->estatus_ambiente_espacio;?>" >&nbsp;<i class="fa fa-edit"></i>  Editar &nbsp;</a> &nbsp;

                        <?php }
                        }?>
                     

                  </td>
                    </tr>
                            <?php }?>
                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
<!-- ///////////////Modal Agregar////////////////// -->
        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title"> Registrar Espacio</h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            <h2>
                            Registrar Espacio
                            </h2>
                            <p>
                            &nbsp;Le permite al usuario registrar los distintos espacios
                             con los que cuenta la aldea o sede de la UBV.
                            </p>

                            <form id="form" action="<?php echo constant('URL');?>espacio/registrarEspacio"  method="post" class="wizard-big">
                                <h1>Descripción</h1>
                                <fieldset>
                                    <h2> Información </h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                        <div class="form-group">
                                            <label>Tipo de Espacio<span style="color: red;">*</span></label>
                                            <select class="chosen-select form-control required m-b " name="espaciotipo" id="espaciotipo">
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/estructura.php';
                                                foreach($this->espaciotipos as $row){
                                                $espacio=new Estructura();
                                                $espacio=$row;?> 
                                            <option value="<?php echo $espacio->id_espacio_tipo;?>"><?php echo $espacio->descripcion;?></option>
                                            <?php }?>
                                            </select>                                               
                                            </div>

                                            <div class="form-group">
                                                <label>Espacio <span style="color: red;">*</span></label>
                                                <input id="espacio" placeholder="Ingrese el Nombre del Espacio" name="espacio" type="text" class="form-control required" onkeypress="return soloLetras(event)"  maxlength='70' minlength="4">
                                            </div>
                                         
                                            <div class="form-group">
                                                <label> Capacidad <span style="color: red;">*</span></label>
                                                <input id="capacidad" placeholder="Ingrese el Capacidad del Espacio" name="capacidad" type="number" class="form-control required" min="1" max="30">
                                            </div>
                                            
                                          
                            </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-linode" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                                <h1>Ubicación</h1>
                                <fieldset>
                                    <h2> Información </h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                      
                                        <div class="form-group content_select">
                                            <label>  Ubicación dentro de la Estructura física<span style="color: red;">*</span></label>
                                            <select class="chosen-select form-control required m-b " name="ubicacion" id="ubicacion">
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/estructura.php';
                                                foreach($this->estructuras as $row){
                                                $estructura=new Estructura();
                                                $estructura=$row;?> 
                                            <option data-cod="<?php echo $estructura->id_ambiente_detalle;?>" value="<?php echo $estructura->id_ambiente_detalle;?>"><?php echo $estructura->ambiente_detalle;?></option>
                                            <?php }?>
                                            </select>                                               
                                            </div>
                                            
                                            <div class="form-group">
                                            <label>Ambiente al que pertenece<span style="color: red;"></span></label>
                                        
                                            <select class="form-control required m-b " name="ambiente" id="ambiente" disabled="">
                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/estructura.php';
                                                foreach($this->estructuras as $row){
                                                $ambiente=new Estructura();
                                                $ambiente=$row;?> 
                                            <option data-cod="<?php echo $ambiente->id_ambiente_detalle;?>" value="<?php echo $ambiente->id_ambiente;?>"><?php echo $ambiente->ambiente;?></option>
                                            <?php }?>
                                            
                                            </select>   
                                            
                                                                            
                                            <script>
                                            var wrapper_sel = $(".content_select");

                                            $(wrapper_sel).find('select').change(function(e){
                                            e.preventDefault(); 	 											
                                            var cod = $(this).find("option:selected").attr("data-cod");

                                            $("#ambiente option[data-cod="+cod+"]")[0].selected = true;
                                            });
                                            </script>

                                           </div>


                                            <div class="form-group">
                                            <label>Estatus <span style="color: red;">*</span></label>
                                            <select class="form-control required m-b " name="estatus" id="estatus">
                                                <option value ="1">Activo</option>
                                                <option value="0" selected="selected">Inactivo</option>
                                            </select>                                                                 
                                            </div>
                                          
                            </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-linode" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
  
<!-- ///////////////Modal editar////////////////// -->
<div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Editar Espacio </h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                                        <h2>
                                        Editar Espacio
                                        </h2>
                                        <p>
                                        &nbsp;Le permite al usuario actualizar los distintos espacios
                                         con los que cuenta la aldea o sede de la UBV.
                                        </p>

                            <form id="form2" action="<?php echo constant('URL');?>espacio/ActualizarEspacio"  method="post" class="wizard-big">
                                <h1>Descripción</h1>
                                <fieldset>
                                    <h2> Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                        <input id="id_ambiente_espacio" name="id_ambiente_espacio" type="hidden" class="form-control">
                                        <div class="form-group">
                                    <label>Tipo de Espacio<span style="color: red;">*</span></label>
                                    <select class="chosen-select form-control required m-b " name="espaciotipoedit" id="espaciotipoedit">
                                    <option selected="selected" value="">Seleccione </option> 
                                    <?php include_once 'models/estructura.php';
                                        foreach($this->espaciotipos as $row){
                                        $espacio=new Estructura();
                                        $espacio=$row;?> 
                                    <option value="<?php echo $espacio->id_espacio_tipo;?>"><?php echo $espacio->descripcion;?></option>
                                    <?php }?>
                                    </select>                                               
                                    </div>
                                        <div class="form-group">
                                        <label>Espacio <span style="color: red;">*</span></label>
                                        <input id="espacioedit" placeholder="Ingrese el Nombre del Espacio" name="espacioedit" type="text" class="form-control required" onkeypress="return soloLetras(event)"  maxlength='70' minlength="4">
                                    </div>
                                   
                                    <div class="form-group">
                                        <label> Capacidad <span style="color: red;">*</span></label>
                                        <input id="capacidadedit" placeholder="Ingrese el Capacidad del Espacio" name="capacidadedit" type="number" class="form-control required" min="1" max="30">
                                    </div>
                            </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-linode" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                                <h1>Ubicación</h1>
                                <fieldset>
                                <h2> Información </h2>
                                <div class="row">
                                    <div class="col-lg-8">
                                  
                                    <div class="form-group content_select">
                                        <label>  Ubicación dentro de la Estructura física<span style="color: red;">*</span></label>
                                    
                                        <select class="chosen-select form-control required m-b " name="estructuraf" id="estructuraf">
                                
                                        <option selected="selected" value="">Seleccione </option> 
                                        <?php include_once 'models/estructura.php';
                                            foreach($this->estructuras as $row){
                                            $estructura=new Estructura();
                                            $estructura=$row;?> 
                                        <option data-cod="<?php echo $estructura->id_ambiente_detalle;?>" value="<?php echo $estructura->id_ambiente_detalle;?>"><?php echo $estructura->ambiente_detalle;?></option>
                                        <?php }?>
                                        
                                        </select>                                               
                                        </div>
                                        
                                        <div class="form-group">
                                        <label>Ambiente al que pertenece<span style="color: red;"></span></label>
                                    
                                        <select class="form-control required m-b " name="ambientee" id="ambientee" disabled="">
                                
                                        <option selected="selected" value="">Seleccione </option> 
                                        <?php include_once 'models/estructura.php';
                                            foreach($this->estructuras as $row){
                                            $ambiente=new Estructura();
                                            $ambiente=$row;?> 
                                        <option data-cod="<?php echo $ambiente->id_ambiente_detalle;?>" value="<?php echo $ambiente->id_ambiente;?>"><?php echo $ambiente->ambiente;?></option>
                                        <?php }?>
                                        
                                        </select>   
                                        
                                                                        
                                        <script>
                                        var wrapper_sel = $(".content_select");

                                        $(wrapper_sel).find('select').change(function(e){
                                        e.preventDefault(); 	 											
                                        var cod = $(this).find("option:selected").attr("data-cod");

                                        $("#ambientee option[data-cod="+cod+"]")[0].selected = true;
                                        });
                                        </script>

                                       </div>


                                        <div class="form-group">
                                        <label>Estatus <span style="color: red;">*</span></label>
                                        <select class="form-control required m-b " name="estatusedit" id="estatusedit">
                                            <option value ="1">Activo</option>
                                            <option value="0" selected="selected">Inactivo</option>
                                        </select>                                                                 
                                        </div>
                                      
                        </div>
                                    <div class="col-lg-4">
                                        <div class="text-center">
                                            <div style="margin-top: 20px">
                                                <i class="fa fa-linode" style="font-size: 180px;color: #e5e5e5 "></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </fieldset>
                                
                          
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->       

 
<!-- ///////////////Modal Mostrar////////////////// -->
<div class="modal inmodal fade " style="width: 100%;" id="myModal3" tabindex="-1" role="dialog"  aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
            <h4 class="modal-title"> Espacio </h4>
            <small class="font-bold"> Detalles </small>
        </div>
        <div class="modal-body" >
            
        <div class="ibox-content">




<h2>
Validation Wizard Form
</h2>
<p>
This example show how to use Steps with jQuery Validation plugin.
</p>





</div>
        
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
  </div>
</div>
</div>
</div>        

<!--////////////////////////////////-->       



   
        <!-- Mainly scripts -->
    <script src="<?php echo constant ('URL');?>src/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/popper.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/bootstrap.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <?php require 'views/footer.php'; ?>
    <!-- Custom and plugin javascript -->
    <script src="<?php echo constant ('URL');?>src/js/inspinia.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/pace/pace.min.js"></script>

   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>
   <!-- Chosen Select -->
   <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script> 
      <!-- Solo Letras -->
      <script src="<?php echo constant ('URL');?>src/js/val_letras.js"></script>

    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    



<script>
  $('#myModal1').on('shown.bs.modal', function () {  
       $('.chosen-select').chosen({width: "100%"});
       });

$('#myModal2').on('show.bs.modal', function(e) {
    
  var product = $(e.relatedTarget).data('id');
  $("#id_ambiente_espacio").val(product);

  var product2 = $(e.relatedTarget).data('espacio');
  $("#espacioedit").val(product2);

  var product3 = $(e.relatedTarget).data('tipo');
  $("#espaciotipoedit").val(product3);

  var product4 = $(e.relatedTarget).data('capacidad');
  $("#capacidadedit").val(product4);

  var product5 = $(e.relatedTarget).data('estructuraf');
  $("#estructuraf").val(product5);

  var product6 = $(e.relatedTarget).data('ambiente');
  $("#ambientee").val(product6);

  var product7 = $(e.relatedTarget).data('estatus');
  $("#estatusedit").val(product7);
//console.log(product7);
  $('.chosen-select').chosen({width: "100%"});
  $('#espaciotipoedit').trigger('chosen:updated');
  $('#estructuraf').trigger('chosen:updated');
  $('#ambientee').trigger('chosen:updated');


});
</script>


    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>





</body>
</html>
