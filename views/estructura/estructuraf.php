<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Registrar Estructura Física | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

 


</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-8">
                    <h2>Registrar Estructura</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>estructuraf">EStructura UBV</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Registrar Estructura</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Registrar Estructura Física</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-4">
                    <div class="title-action">
                    <?php  if($_SESSION['Agregar']==true){?>

                        <!-- boton agregar-->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                        <i class="fa fa-plus"></i> Registrar Estructura Física
                                </button> 
                    <?php } ?>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Listado de Estructuras </h5>
                       
                        <div class="ibox-tools">
                       

                        </div>
                    </div>
                    
                    <div class="ibox-content">
                    <?php echo $this->mensaje; ?>
                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Estrutura física</th>
                        <th>Ambiente</th>
                
                    
                        <th>Estatus</th>
                        
                        <th>Acciones</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php include_once 'models/estructura.php';
                            foreach($this->estructuras as $row){
                                $estructura= new Estructura();
                                $estructura=$row;?>
                    <tr class="gradeX">
                    <td><?php echo $estructura->estrutura_fisica; ?> </td>
                  
                    <td><?php echo $estructura->ambiente; ?></td>
             
                   
                    <td> <?php if($estructura->estatus==0){
                        echo"<center style=margin-top:3%;><span class='label label-danger' style='font-size: 12px;'>Inactivo</span></center>";
                    }else{
                        echo"<center style=margin-top:3%;><span class='label label-primary' style='font-size: 12px;'>&nbsp; Activo &nbsp;</span></center>";
                    }  ?>
                    
                    </td>
                   
                  
                    
                    <td> 
                    <?php  if($_SESSION['Editar']==true){?>

                    <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal"   data-id="<?php echo $estructura->id_ambiente_detalle;?>" data-estructura="<?php echo $estructura->estrutura_fisica;?>"   data-ambiente="<?php echo $estructura->id_ambiente;?>" data-idestatus="<?php echo $estructura->estatus;?>" >&nbsp; Editar &nbsp;</a> &nbsp;
                    <?php } ?>
                  </td>
                    </tr>
                            <?php }?>
                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
<!-- ///////////////Modal Agregar////////////////// -->
        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Registrar Estructura Física</h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            <h2>
                            Registrar Estructura Física
                            </h2>
                            <p>
                            &nbsp;Le permite al usuario registrar la estructura física de un ambiente
                            especifico con diferentes características.
                            </p>

                            <form id="form" action="<?php echo constant('URL');?>estructuraf/registrarEstrutura"  method="post" class="wizard-big">
                                <h1>Descripción</h1>
                                <fieldset>
                                    <h2>Informción</h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            

                                            <div class="form-group">
                                            <label>Estructura Física<span style="color: red;">*</span></label>
                                            <select class="chosen-select form-control required m-b " name="estructura" id="estructura">
                                            <option value="" selected="selected">Seleccione</option>
                                            <option value ="Edificio">Edificio</option>   
                                            <option value ="Sector">Sector</option>      
                                            <option value ="Piso">Piso</option>      
                                            <option value ="Nivel">Nivel</option>      
                                            <option value ="Área">Área</option>                                              
                                            </select>                                              
                                            </div>
                                            

                                            <div class="form-group">
                                            <label>Ambiente<span style="color: red;">*</span></label>                                        
                                            <select class="chosen-select form-control required m-b " name="id_ambiente" id="id_ambiente">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/estructura.php';
                                                foreach($this->ambientes as $row){
                                                $ambiente=new Estructura();
                                                $ambiente=$row;?> 
                                            <option value="<?php echo $ambiente->id_ambiente;?>"><?php echo $ambiente->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>

                                            <div class="form-group">
                                            <label>Estatus <span style="color: red;">*</span></label>
                                            <select class="form-control required m-b " name="estatus" id="estatus">                                      
                                            <option value="0" selected="selected">Inactivo</option>
                                            <option value ="1">Activo</option>                                               
                                            </select>                                              
                                            </div>
                                            
                                          
                            </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-linode" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                               
                                
                          
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
  
<!-- ///////////////Modal Agregar////////////////// -->
<!-- ///////////////Modal editar////////////////// -->
<div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                <h4 class="modal-title">Editar Estructura Física   </h4>
                <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
            </div>
            <div class="modal-body" >
                
            <div class="ibox-content">
            <h2>
            Editar Estructura Física
            </h2>
            <p>
            &nbsp;Le permite al usuario actualizar la estructura física de un ambiente
            especifico con diferentes características.
            </p>


<form id="form2" action="<?php echo constant('URL');?>estructuraf/ActualizarEstructura"  method="post" class="wizard-big">
    <h1>Descripción</h1>
    <fieldset>
        <h2>Información</h2>
        <div class="row">
            <div class="col-lg-8">
               
                <input id="id_ambiente_detalle" name="id_ambiente_detalle" type="hidden" class="form-control">
                   
                                   <div class="form-group">
                                            <label>Estructura Física<span style="color: red;">*</span></label>
                                            <select class="chosen-select form-control required m-b " name="estruct" id="estruct">
                                            <option value="" selected="selected">Seleccione</option>
                                            <option value ="Edificio">Edificio</option>   
                                            <option value ="Sector">Sector</option>      
                                            <option value ="Piso">Piso</option>      
                                            <option value ="Nivel">Nivel</option>      
                                            <option value ="Área">Área</option>                                              
                                            </select>                                              
                                            </div>
                                            
               
  

                                           <div class="form-group">
                                            <label>Ambiente <span style="color: red;">*</span></label>         
                                            <select  name="id_ambienteedit" id="id_ambienteedit" class="chosen-select form-control required m-b" >
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/estructura.php';
                                          foreach($this->ambientes as $row){
                                          $ambiente=new Estructura();
                                            $ambiente=$row;?> 
                                            <option value="<?php echo $ambiente->id_ambiente;?>"><?php echo $ambiente->descripcion;?></option>
                                            <?php }?>
                                               
                                            </select>   
                                            </select>
                                            </div>

                                                      
                                                    
                                           <div class="form-group">
                                            <label>Estatus <span style="color: red;">*</span></label>
                                            <select class="form-control  m-b " name="estatuss" id="estatuss">
                                            <option  value="0" >Inactivo</option>
                                            <option  value ="1">Activo</option>
                                            </select>  
                                            </div>
                </div>
            <div class="col-lg-4">
                <div class="text-center">
                    <div style="margin-top: 20px">
                        <i class="fa fa-linode" style="font-size: 180px;color: #e5e5e5 "></i>
                    </div>
                </div>
            </div>
        </div>

    </fieldset>

</form>
</div>
            
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                <!--<button type="button" class="btn btn-primary">Save changes</button>-->
      </div>
  </div>
</div>
</div>        

     

<!--////////////////////////////////-->        




    <?php require 'views/footer.php'; ?>

        
   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

<!-- menu active -->
<script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
   <!-- Chosen Select -->
   <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script> 
 



<script>
       
       $('#myModal1').on('shown.bs.modal', function () {  
       $('.chosen-select').chosen({width: "100%"});
       });   
$('#myModal2').on('show.bs.modal', function(e) {
    
  var product = $(e.relatedTarget).data('id');
  $("#id_ambiente_detalle").val(product);

  var product2 = $(e.relatedTarget).data('estructura');
  $("#estruct").val(product2);

  var product3 = $(e.relatedTarget).data('ambiente');
  $("#id_ambienteedit").val(product3);

  var estatus = $(e.relatedTarget).data('idestatus');
//console.log(estatus);
  $("#estatuss").val(estatus);

  $('.chosen-select').chosen({width: "100%"});
  $('#estruct').trigger('chosen:updated');
  $('#id_ambienteedit').trigger('chosen:updated');



});
</script>


    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>





</body>
</html>
