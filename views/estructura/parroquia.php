<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Parroquia | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

 


</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-9">
                    <h2><i class="fa fa-globe"></i> Parroquia</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>parroquia">Extructura UBV</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Ubicación Geografica</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Editar Parroquia</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-3">
                    <div class="title-action">
                    <?php  if($_SESSION['Agregar']==true){?>

                    <!-- boton agregar-->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                            <i class="fa fa-plus"></i> Registrar Parroquia
                        </button> 
                    <?php } ?>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5><i class="fa fa-globe"></i> Listado de Parroquias </h5>
                       
                        <div class="ibox-tools">
                        

                        </div>
                    </div>
                    
                    <div class="ibox-content">
                    <?php echo $this->mensaje; ?>
                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Código (INE)</th>
                        <th><i class="fa fa-globe"></i> Parroquia</th>
                        <th><i class="fa fa-globe"></i> Eje Municipal de adscripción</th>
                        <th><i class="fa fa-globe"></i> Municipio</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php include_once 'models/estructura.php';
                            foreach($this->parroquias as $row){
                                $parroquia= new Estructura();
                                $parroquia=$row;?>
                    <tr class="gradeX">
                    <td><?php echo $parroquia->codigo; ?> </td>
                    <td><?php echo $parroquia->parroquia; ?></td>
                    <td><?php echo $parroquia->eje_municipal; ?></td>
                    <td><?php echo $parroquia->municipio; ?></td>
                    
                    <td> 
                        <?php if($parroquia->eje_municipal_estutus==0){?>
                    <a class="btn btn-outline btn-danger" href="#" title="Eje Municipal Inactivo">Inactivo</a> &nbsp;
                        <?php }else{ 
                            if($_SESSION['Editar']==true){?>
                                             
                            <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal" data-id="<?php echo $parroquia->id_parroquia;?>" data-cod="<?php echo $parroquia->codigo;?>"   data-parroquia="<?php echo $parroquia->parroquia;?>"  data-idmunicipio="<?php echo $parroquia->id_municipio;?>"  data-idejemunicipal="<?php echo $parroquia->id_eje_municipal;?>"><i class="fa fa-edit"></i> Editar </a> 
                   
                        <?php }
                        }?>
                  </td>
                    </tr>
                            <?php }?>
                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
<!-- ///////////////Modal Agregar////////////////// -->
        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Registar Parroquia </h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            <h2>
                            Registrar Parroquia
                            </h2>
                            <p>
                            &nbsp;Permite al usuario registrar una parroquia perteneciente a un municipio especifico y
                             asociarla a la estructura territorial de la UBV.
                            </p>

                            <form id="form" action="<?php echo constant('URL');?>parroquia/registrarParroquia"  method="post" class="wizard-big">
                                <h1>Descripción</h1>
                                <fieldset>
                                    <h2> Información </h2>
                                    <div class="row">
                                        <div class="col-lg-8">
                                        <div class="form-group">
                                            <label>Eje Municipal de adscripción  <span style="color: red;">*</span></label>
                                        
                                            <select class="chosen-select form-control required m-b " name="eje_municipal" id="eje_municipal">
                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/estructura.php';
                                            foreach($this->ejesMunicipal as $row){
                                            $eje=new Estructura();
                                            $eje=$row;?> 
                                            <option value="<?php echo $eje->id_eje_municipal;?>"><?php echo $eje->descripcion;?></option>
                                            <?php }?>
                                            
                                            </select>                                       
                                            </div>
              

                                            <div class="form-group">
                                            <label>Municipio al que pertenece <span style="color: red;">*</span></label>
                                        
                                            <select class="chosen-select form-control required m-b " name="municipio" id="municipio">
                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/estructura.php';
                                                foreach($this->municipios as $row){
                                                $municipio=new Estructura();
                                                $municipio=$row;?> 
                                            <option value="<?php echo $municipio->id_municipio;?>"><?php echo $municipio->descripcion;?></option>
                                            <?php }?>
                                            
                                            </select>                                              
                                            </div>


                                            <div class="form-group">
                                                <label>Código Ine <span style="color: red;">*</span></label>
                                                <input id="codigoine" placeholder="Ingrese el Código de la Parroquia" name="codigoine" type="text" class="form-control required number"  maxlength='6' minlength="3">
                                            </div>
                                            
                                            <div class="form-group">
                                                <label> Parroquia <span style="color: red;">*</span></label>
                                                <input id="parroquia" placeholder="Ingrese el Nombre de la Parroquia"  name="parroquia" type="text" class="form-control required" onkeypress="return soloLetras(event)"  maxlength='50' minlength="4">
                                            </div>

                                           
                


                                          
                            </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-university" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                          
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
    <!-- ///////////////Modal editar////////////////// -->
    <div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                <h4 class="modal-title">Editar Parroquia </h4>
                <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
            </div>
            <div class="modal-body" >
                
            <div class="ibox-content">
            <h2>
            Editar Parroquia
            </h2>
            <p>
            &nbsp;Permite al usuario actualizar una parroquia perteneciente a un municipio especifico y
             asociarla a la estructura territorial de la UBV.
            </p>
<form id="form2" action="<?php echo constant('URL');?>parroquia/ActualizarParroquia"  method="post" class="wizard-big">
    <h1>Descripción</h1>
    <fieldset>
        <h2> Información</h2>
        <div class="row">
            <div class="col-lg-8">
            <div class="form-group">
                                            <label>Eje Municipal de adscripción  <span style="color: red;">*</span></label>
                                        
                                            <select class="chosen-select form-control required m-b " name="id_eje_municipaledit" id="id_eje_municipaledit">
                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/estructura.php';
                                            foreach($this->ejesMunicipal as $row){
                                            $eje=new Estructura();
                                            $eje=$row;?> 
                                            <option value="<?php echo $eje->id_eje_municipal;?>"><?php echo $eje->descripcion;?></option>
                                            <?php }?>
                                            
                                            </select>                                               
                                            </div>
              

                                            <div class="form-group">
                                            <label>Municipio al que pertenece <span style="color: red;">*</span></label>
                                        
                                            <select class="chosen-select form-control required m-b " name="id_municipioedit" id="id_municipioedit">
                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/estructura.php';
                                                foreach($this->municipios as $row){
                                                $municipio=new Estructura();
                                                $municipio=$row;?> 
                                            <option value="<?php echo $municipio->id_municipio;?>"><?php echo $municipio->descripcion;?></option>
                                            <?php }?>
                                            
                                            </select>
                                            </div>


                <div class="form-group">
                    <label>Código Ine<span style="color: red;">*</span></label>
                    <input id="id_parroquiaedit" name="id_parroquiaedit" type="hidden" class="form-control">
                                      
                    <input id="codigoedita" placeholder="Ingrese el Código de la Parroquia" name="codigoedita" type="text" class="form-control required number"  maxlength='6' minlength="3">
                </div>
               
                <div class="form-group">
                    <label>Parroquia<span style="color: red;">*</span></label>
                    <input id="parroquiaedit" placeholder="Ingrese el Nombre de la Parroquia" name="parroquiaedit" type="text" class="form-control required" onkeypress="return soloLetras(event)"  maxlength='50' minlength="4">
                </div>
              
              
                                          
                



                </div>
            <div class="col-lg-4">
                <div class="text-center">
                    <div style="margin-top: 20px">
                        <i class="fa fa-university" style="font-size: 180px;color: #e5e5e5 "></i>
                    </div>
                </div>
            </div>
        </div>

    </fieldset>

</form>
</div>
            
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                <!--<button type="button" class="btn btn-primary">Save changes</button>-->
      </div>
  </div>
</div>
</div>        




    <?php require 'views/footer.php'; ?>

        
   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>
    <!-- Chosen Select -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script> 
     <!-- Solo Letras -->
     <script src="<?php echo constant ('URL');?>src/js/val_letras.js"></script>
    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    



<script>
      $('#myModal1').on('shown.bs.modal', function () {  
       $('.chosen-select').chosen({width: "100%"});
       });
     
            
$('#myModal2').on('show.bs.modal', function(e) {
    
  var product = $(e.relatedTarget).data('id');
  $("#id_parroquiaedit").val(product);

  var product2 = $(e.relatedTarget).data('cod');
  $("#codigoedita").val(product2);

  var product2 = $(e.relatedTarget).data('parroquia');
  $("#parroquiaedit").val(product2);

  var product3 = $(e.relatedTarget).data('idmunicipio');
  $("#id_municipioedit").val(product3);

  var product5 = $(e.relatedTarget).data('idejemunicipal');
  $("#id_eje_municipaledit").val(product5);

  $('.chosen-select').chosen({width: "100%"});
  $('#id_municipioedit').trigger('chosen:updated');
  $('#id_eje_municipaledit').trigger('chosen:updated');




});
</script>


    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>





</body>
</html>
