<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Aldea Teléfono | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">



</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-9">
                    <h2>Aldea Teléfono</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>aldeatelf">Estructura UBV</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Ubicación Geografica</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Editar Aldea Teléfono</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-3">
                    <div class="title-action">
                    <?php  if($_SESSION['Agregar']==true){?>

                    <!-- boton agregar-->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                                  <i class="fa fa-plus"></i> Registrar Aldea Teléfono
                                </button> 
                    <?php } ?>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Listado de Teléfonos  </h5>
                       
                        <div class="ibox-tools">
                        


                        </div>
                    </div>
                  
                    <div class="ibox-content">
                    <?php echo $this->mensaje; ?>
                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Aldea</th>
                        <th>Código Área Teléfono</th>
                        <th>Número Teléfono</th>                        
                        <th>Acciones</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php include_once 'models/estructura.php';
                            foreach($this->aldeatelefonos as $row){
                                $aldeatelefono= new Estructura();
                                $aldeatelefono=$row;?>
                    <tr class="gradeX">
                    <td><?php echo $aldeatelefono->aldea; ?> </td>
                    <td><?php echo $aldeatelefono->telefono_codigo_area; ?> </td>
                    <td><?php echo $aldeatelefono->aldea_telefono; ?> </td>
                                     
                    
                    <td> 
                    <?php  if($_SESSION['Editar']==true){?>

                    <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal" data-id="<?php echo $aldeatelefono->id_aldea_telefono;?>" data-codigo="<?php echo $aldeatelefono->id_telefono_codigo_area;?>" data-numero="<?php echo $aldeatelefono->aldea_telefono;?>"  data-aldea="<?php echo $aldeatelefono->id_aldea;?>">&nbsp; Editar &nbsp;</a> &nbsp;
                    <?php } ?>
                  </td>
                    </tr>
                            <?php }?>
                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
<!-- ///////////////Modal Agregar////////////////// -->
        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title"> Registrar Aldea Teléfono</h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                           
                            <form id="form" action="<?php echo constant('URL');?>aldeatelf/registrarAldeaTelf"  method="post" class="wizard-big">
                                <h1>Descripción</h1>
                                <fieldset>
                                    <h2> Aldea Teléfono </h2>
                                    <div class="row">
                                       
                                        <div class="col-lg-8">
                                            
                                        <div class="form-group">
                                        <label>Aldea al que pertenece <span style="color: red;">*</span></label>
                                        <select class="chosen-select form-control required m-b " name="aldea" id="aldea">
                                        <option selected="selected" value="">Seleccione </option> 
                                        <?php include_once 'models/estructura.php';
                                            foreach($this->aldeas as $row){
                                            $aldea=new Estructura();
                                            $aldea=$row;?> 
                                        <option value="<?php echo $aldea->id_aldea;?>"><?php echo $aldea->descripcion;?></option>
                                        <?php }?>
                                        
                                        </select>                                               
                                        </div>

                                   <div class="form-group">
                                    <label>Código de Área <span style="color: red;">*</span></label>
                                    <select class="chosen-select form-control required m-b " name="codigoarea" id="codigoarea" >
                                    <option selected="selected" value="">Seleccione </option> 
                                    <?php include_once 'models/estructura.php';
                                        foreach($this->codigos as $row){
                                        $codigo=new Estructura();
                                        $codigo=$row;?> 
                                    <option value="<?php echo $codigo->id_telefono_codigo_area;?>"><?php echo $codigo->descripcion;?></option>
                                    <?php }?>
                                    
                                    </select>                                               
                                    </div>

                                        
                                        <div class="form-group">
                                        <label>Teléfono<span style="color: red;">*</span></label>
                                        <input id="numero" placeholder="Ingrese el Número de Tlf. de la Aldea" name="numero" type="text"  class="form-control required number" maxlength='7' minlength="7">
                                         </div>



                                      
                                            
                                          
                            </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-phone" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                                
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
  
<!-- ///////////////Modal editar////////////////// -->
<div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
            <h4 class="modal-title"> Editar Aldea Teléfono</h4>
            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
        </div>
        <div class="modal-body" >
            
        <div class="ibox-content">


<form id="form2" action="<?php echo constant('URL');?>aldeatelf/ActualizarAldeaTelf"  method="post" class="wizard-big">
<h1>Descripción</h1>
<fieldset>
    <h2>Aldea Teléfono </h2>
    <div class="row">
       
        <div class="col-lg-8">

        <input id="id_aldea_telefono" name="id_aldea_telefono" type="hidden"  class="form-control ">
       
        <div class="form-group">
        <label>Aldea al que pertenece <span style="color: red;">*</span></label>
        <select class="chosen-select form-control required m-b " name="aldeaa" id="aldeaa">
        <option selected="selected" value="">Seleccione </option> 
        <?php include_once 'models/estructura.php';
            foreach($this->aldeas as $row){
            $aldea=new Estructura();
            $aldea=$row;?> 
        <option value="<?php echo $aldea->id_aldea;?>"><?php echo $aldea->descripcion;?></option>
        <?php }?>
        
        </select>                                               
        </div>
   <div class="form-group">
    <label>Codigo de Area <span style="color: red;">*</span></label>
    <select class="chosen-select form-control required m-b " name="codigoareaa" id="codigoareaa">
    <option selected="selected" value="">Seleccione </option> 
    <?php include_once 'models/estructura.php';
        foreach($this->codigos as $row){
        $codigo=new Estructura();
        $codigo=$row;?> 
    <option value="<?php echo $codigo->id_telefono_codigo_area;?>"><?php echo $codigo->descripcion;?></option>
    <?php }?>
    
    </select>                                               
    </div>

        
        <div class="form-group">
        <label>Teléfono<span style="color: red;">*</span></label>
        <input id="numeroo" placeholder="Ingrese el Número de Tlf. de la Aldea" name="numeroo" type="text"class="form-control required number" maxlength='7' minlength="7">
         </div>



    
            
          
</div>
        <div class="col-lg-4">
            <div class="text-center">
                <div style="margin-top: 20px">
                    <i class="fa fa-phone" style="font-size: 180px;color: #e5e5e5 "></i>
                </div>
            </div>
        </div>
    </div>

</fieldset>

</form>
</div>
        
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
  </div>
</div>
</div>
</div>        

<!--////////////////////////////////-->




    <?php require 'views/footer.php'; ?>

        
   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

   <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
   <!-- Chosen Select -->
   <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script> 
  




<script>
   $('#myModal1').on('shown.bs.modal', function () {  
       $('.chosen-select').chosen({width: "100%"});
       });
   
                
         
$('#myModal2').on('show.bs.modal', function(e) {
    
  var product = $(e.relatedTarget).data('id');
  $("#id_aldea_telefono").val(product);

  var product2 = $(e.relatedTarget).data('codigo');
  $("#codigoareaa").val(product2);

  var product3 = $(e.relatedTarget).data('numero');
  $("#numeroo").val(product3);

  var product4 = $(e.relatedTarget).data('aldea');
  $("#aldeaa").val(product4);


  $('.chosen-select').chosen({width: "100%"});
  $('#codigoareaa').trigger('chosen:updated');
  $('#aldeaa').trigger('chosen:updated');

});
</script>


    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>





</body>
</html>
