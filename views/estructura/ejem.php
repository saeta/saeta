<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Eje regional | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

 


</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-9">
                    <h2><i class="fa fa-globe"></i> Eje Municipal </h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>ejem">Estructura UBV</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Ubicación Geografica</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>Editar Eje Municipal</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-3">
                    <div class="title-action">
                    <?php  if($_SESSION['Agregar']==true){?>

                        <!-- boton agregar-->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                            <i class="fa fa-plus"></i> Registrar Eje Municipal
                        </button> 
                    <?php } ?>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5><i class="fa fa-globe"></i> Listado de Eje Municipal </h5>
                       
                        <div class="ibox-tools">
                        


                        </div>
                    </div>
                    
                    <div class="ibox-content">
                    <?php echo $this->mensaje; ?>
                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Código del Eje Municipal</th>
                        <th><i class="fa fa-globe"></i> Eje Municipal</th>                        
                        <th><i class="fa fa-globe"></i> Eje regional de adscripción</th>
                        <th>Estatus</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php include_once 'models/estructura.php';
                            foreach($this->ejesMunicipales as $row){
                                $eje= new Estructura();
                                $eje=$row;?>
                    <tr class="gradeX">
                    <td><?php echo $eje->codigo; ?> </td>
                    <td><?php echo $eje->eje_municipal; ?></td>
                    <td><?php echo $eje->eje_regional; ?></td>
                    <td>
                    <?php if($eje->estatus_eje==0){
                    echo"<center style=margin-top:3%;><span class='label label-danger' style='font-size: 12px;'>Inactivo</span></center>";
                    }else{
                        echo"<center style=margin-top:3%;><span class='label label-primary' style='font-size: 12px;'>&nbsp; Activo &nbsp;</span></center>";
                
                    } ?>
                    </td>
                    <td> 
                    <?php if($eje->estatus_eje_regional==0){?>
                    <a class="btn btn-outline btn-danger" href="#" role="button" title="Eje Regional Inactivo">Inactivo</a> &nbsp;
                    <?php }else{
                                           if($_SESSION['Editar']==true){?>
                                          

                    <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal" data-id="<?php echo $eje->id_eje_municipal;?>" data-cod="<?php echo $eje->codigo;?>"   data-ejem="<?php echo $eje->eje_municipal;?>"  data-ejer="<?php echo $eje->id_eje_regional;?>"  data-estatus="<?php echo $eje->estatus_eje;?>" ><i class="fa fa-edit"></i> Editar </a> 
                    <?php }
                    }?>
                  </td>
                    </tr>
                            <?php }?>
                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
<!-- ///////////////Modal Agregar////////////////// -->
        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Registar Eje Municipal </h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            <h2>
                            Registar Eje Municipal
                            </h2>
                            <p>
                            &nbsp;Permite al usuario registrar un Eje
                            Municipal perteneciente a la estructura territorial de
                            la UBV
                                </p>

                            <form id="form" action="<?php echo constant('URL');?>ejem/registrarEjeMunicipal"  method="post" class="wizard-big">
                                <h1>Descripción</h1>
                                <fieldset>
                                    <h2>Información </h2>
                                    <div class="row">
                                        <div class="col-lg-8">

                                        <div class="form-group">
                                            <label>Eje regional de adscripción <span style="color: red;">*</span></label>         
                                            <select  name="ejeregional" id="ejeregional" class="chosen-select form-control required m-b" >
                                            <option selected="selected" value="">Seleccione un Eje regional</option> 
                                            <?php include_once 'models/estructura.php';
                                          foreach($this->ejesRegionales as $row){
                                          $ejesRegionale=new Estructura();
                                            $ejesRegionale=$row;?> 
                                            <option value="<?php echo $ejesRegionale->id_eje_regional;?>"><?php echo $ejesRegionale->descripcion;?></option>
                                            <?php }?>
                                               
                                            </select>                                           
                                            </div>


                                            <div class="form-group">
                                                <label>Código Eje Municipal <span style="color: red;">*</span></label>
                                                <input id="codigoejem" placeholder="Ingrese el Código del Eje Municipal" name="codigoejem" type="text" class="form-control required number"  maxlength='6' minlength="3">
                                            </div>
                                            
                                            <div class="form-group">
                                                <label>Eje Municipal <span style="color: red;">*</span></label>
                                                <input id="ejemunicipal" name="ejemunicipal" placeholder="Ingrese el Nombre del Eje Municipal"  type="text" class="form-control required" onkeypress="return soloLetras(event)"  maxlength='70' minlength="4">
                                            </div>
                                         
                                            <div class="form-group">
                                            <label>Estatus <span style="color: red;">*</span></label>
                                            <select class="form-control required m-b " name="estatus" id="estatus">
                                                <option value ="1">Activo</option>
                                                <option selected="selected" value="0">Inactivo</option>
                                               
                                            </select>                                             
                                            </div>


                                          
                            </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-university" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                          
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
    <!-- ///////////////Modal editar////////////////// -->
    <div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                <h4 class="modal-title">Editar Eje Municipal </h4>
                <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
            </div>
            <div class="modal-body" >
                
            <div class="ibox-content">
            <h2>
            Editar Eje Municipal
            </h2>
            <p>
            &nbsp;Permite al usuario actualizar un Eje
            Municipal perteneciente a la estructura territorial de
            la UBV
                </p>
<form id="form2" action="<?php echo constant('URL');?>ejem/ActualizarEjeMunicipal"  method="post" class="wizard-big">
    <h1>Descripción</h1>
    <fieldset>
        <h2>Información</h2>
        <div class="row">
            <div class="col-lg-8">


                                           <div class="form-group">
                                            <label>Eje regional de adscripción <span style="color: red;">*</span></label>         
                                            <select  name="ejeregionaledit" id="ejeregionaledit" class="chosen-select form-control required m-b" >
                                            <option selected="selected" value="">Seleccione un Eje regional</option> 
                                            <?php include_once 'models/estructura.php';
                                          foreach($this->ejesRegionales as $row){
                                          $ejesRegionale=new Estructura();
                                            $ejesRegionale=$row;?> 
                                            <option value="<?php echo $ejesRegionale->id_eje_regional;?>"><?php echo $ejesRegionale->descripcion;?></option>
                                            <?php }?>
                                               
                                            </select>                      
                                            </div>


                <div class="form-group">
                    <label>Código Eje Municipa<span style="color: red;">*</span></label>
                    <input id="id_eje_municipaledit" name="id_eje_municipaledit" type="hidden" class="form-control">
                                      
                    <input id="codigoejemedit" name="codigoejemedit" placeholder="Ingrese el Código del Eje Municipal" type="text" class="form-control required number"  maxlength='6' minlength="3">
                </div>
               
                <div class="form-group">
                    <label>Eje Municipal <span style="color: red;">*</span></label>
                    <input id="ejemunicipaledit" name="ejemunicipaledit" type="text" placeholder="Ingrese el Nombre del Eje Municipal" class="form-control required" onkeypress="return soloLetras(event)"  maxlength='70' minlength="4">
                </div>

                                          
                                                      
                                           <div class="form-group">
                                            <label>Estatus <span style="color: red;">*</span></label>
                                            <select class="form-control required m-b " name="estatusejeedit" id="estatusejeedit">
                                              <option value ="1">Activo</option>
                                                <option  selected="selected" value="0">Inactivo</option>
                                               
                                            </select>   
                                            </select>
                                            </div>
                </div>
            <div class="col-lg-4">
                <div class="text-center">
                    <div style="margin-top: 20px">
                        <i class="fa fa-university" style="font-size: 180px;color: #e5e5e5 "></i>
                    </div>
                </div>
            </div>
        </div>

    </fieldset>

</form>
</div>
            
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                <!--<button type="button" class="btn btn-primary">Save changes</button>-->
      </div>
  </div>
</div>
</div>        




    <?php require 'views/footer.php'; ?>

       
   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>
  <!-- Chosen Select -->
  <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script> 
  
    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>
    
  <!-- Solo Letras -->
  <script src="<?php echo constant ('URL');?>src/js/val_letras.js"></script>


<script>

$('#myModal1').on('shown.bs.modal', function () {  
       $('.chosen-select').chosen({width: "100%"});
       });  
            
$('#myModal2').on('show.bs.modal', function(e) {
    
  var product = $(e.relatedTarget).data('id');
  $("#id_eje_municipaledit").val(product);

  var product2 = $(e.relatedTarget).data('cod');
  $("#codigoejemedit").val(product2);

  var product3 = $(e.relatedTarget).data('ejem');
  $("#ejemunicipaledit").val(product3);

  var product4 = $(e.relatedTarget).data('ejer');
  $("#ejeregionaledit").val(product4);

  var product5 = $(e.relatedTarget).data('estatus');
  $("#estatusejeedit").val(product5);

  $('.chosen-select').chosen({width: "100%"});
  $('#ejeregionaledit').trigger('chosen:updated');

});
</script>


    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>





</body>
</html>
