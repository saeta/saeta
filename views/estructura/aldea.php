<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Aldea | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

 


</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><i class="fa fa-institution"></i> Aldea</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<?php echo constant ('URL');?>aldea">Estructura UBV</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Ubicación Geografica</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong> Editar Aldea o Sede</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                    <div class="title-action">
                    <?php  if($_SESSION['Agregar']==true){?>

                        <!-- boton agregar-->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                        <i class="fa fa-plus"></i> Registrar Aldea
                        </button> 
                    <?php } ?>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Listado de Aldeas </h5>
                       
                        <div class="ibox-tools">
                        


                        </div>
                    </div>
                    
                    <div class="ibox-content">
                    <?php echo $this->mensaje; ?>
                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Código de Aldea</th>
                        <th>Código Sucre</th>
                        <th>Aldea</th>
                        <th>Tipo de Aldea</th>
                        <th>Ambiente al que pertenece</th>                       
                        <th>Estatus</th>                        
                        <th>Acciones</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php include_once 'models/estructura.php';
                            foreach($this->aldeaosede as $row){
                                $aldeao= new Estructura();
                                $aldeao=$row;?>
                    <tr class="gradeX">
                    <td><?php echo $aldeao->codigo_aldea; ?> </td>
                    <td><?php echo $aldeao->codigo_sucre; ?></td>
                    <td><?php echo $aldeao->aldea; ?></td>
                    <td><?php echo $aldeao->aldea_tipo; ?></td>
                    <td><?php echo $aldeao->ambiente; ?></td>
                   
                       <td> <?php if($aldeao->estatus_aldea==0){
                           echo"<center style=margin-top:3%;><span class='label label-danger' style='font-size: 12px;'>Inactivo</span></center>";
                     
                    }else{
                        echo"<center style=margin-top:3%;><span class='label label-primary' style='font-size: 12px;'>&nbsp; Activo &nbsp;</span></center>";
                
                    }  ?>
                    
                    </td>
                  
                    
                    <td> 
                    <?php if($aldeao->estatus_ambiente==0){ ?>

                    <a class="btn btn-outline btn-danger" href="#" title="">Inactivo</a> &nbsp;
                    <?php }else{
                             if($_SESSION['Editar']==true){?>
 
                   <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal"   data-id="<?php echo $aldeao->id_aldea;?>" data-tipo="<?php echo $aldeao->id_aldea_tipo;?>"   data-aldea="<?php echo $aldeao->aldea;?>" data-ambiente="<?php echo $aldeao->id_ambiente;?>" data-sucre="<?php echo $aldeao->codigo_sucre;?>" data-codaldea="<?php echo $aldeao->codigo_aldea;?>"  data-estatus="<?php echo $aldeao->estatus_aldea;?>" ><i class="fa fa-edit"></i> Editar </a> 
                    <?php }
                    }?>             

                  </td>
                    </tr>
                            <?php }?>
                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
<!-- ///////////////Modal Agregar////////////////// -->
        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Registrar Aldea</h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            <h2>
                            Registrar Aldea
                            </h2>
                            <p>
                            &nbsp;Permite al usuario registrar una sede de
                            la UBV o una aldea de la misión Sucre donde se
                            dicten programas de la UBV.
                            </p>

                            <form id="form" action="<?php echo constant('URL');?>aldea/registrarAldea"  method="post" class="wizard-big">
                                <h1>Descripción</h1>
                                <fieldset>
                                    <h2>Información </h2>
                                    <div class="row">
                                        <div class="col-lg-8">

                                        <div class="form-group">
                                        <label>Código de Aldea <span style="color: red;">*</span></label>
                                        <input id="codigoaldea" placeholder="Ingrese el Código de la Aldea" name="codigoaldea" type="text" class="form-control required" maxlength='15' minlength="3">
                                         </div>

                                         <div class="form-group">
                                        <label>Código Sucre<span style="color: red;">*</span></label>
                                        <input id="codigosucre" placeholder="Ingrese el Código Sucre de la Aldea" name="codigosucre" type="text" class="form-control required" maxlength='15' minlength="3">
                                         </div>                                    

                                        <div class="form-group">
                                        <label>Aldea</label>
                                        <textarea id="aldea" name="aldea" placeholder="Ingrese el Nombre de la Aldea" type="text" class="form-control required valid" placeholder="Escriba una dirección..." aria-required="true" aria-invalid="false" onkeypress="return soloLetras(event)"  maxlength='70' minlength="4"></textarea>
                                        </div>
                            </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-institution" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </fieldset>
                            <h1>Ubicación</h1>
                                <fieldset>
                                    <h2>Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">

                                        <div class="form-group">
                                            <label>Tipo de Aldea<span style="color: red;">*</span></label>                                        
                                            <select  name="aldeatipo" id="aldeatipo"  class="chosen-select form-control required m-b">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/estructura.php';
                                                foreach($this->aldeas as $row){
                                                $aldea=new Estructura();
                                                $aldea=$row;?> 
                                            <option value="<?php echo $aldea->id_aldea_tipo;?>"><?php echo $aldea->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>


                                        <div class="form-group">
                                        <label>Ambiente al que pertenece<span style="color: red;">*</span></label>                                        
                                            <select class="chosen-select form-control required m-b" name="ambiente" id="ambiente">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/estructura.php';
                                                foreach($this->ambientes as $row){
                                                $ambiente=new Estructura();
                                                $ambiente=$row;?> 
                                            <option value="<?php echo $ambiente->id_ambiente;?>"><?php echo $ambiente->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>
                                                                        
                                            <div class="form-group">
                                            <label>Estatus <span style="color: red;">*</span></label>
                                            <select class="form-control required m-b " name="estatus" id="estatus">                                      
                                            <option value="0" selected="selected">Inactivo</option>
                                            <option value ="1">Activo</option>                                               
                                            </select>                                              
                                            </div>
                                            
                                          
                            </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-institution" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                </fieldset>
                               
                                
                          
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
  
<!-- ///////////////Modal Agregar////////////////// -->
<!-- ///////////////Modal Editar////////////////// -->
        <div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Editar Aldea</h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                                        <h2>
                                        Editar Aldea
                                        </h2>
                                        <p>
                                        &nbsp;Permite al usuario actualizar una sede de
                                        la UBV o una aldea de la misión Sucre donde se
                                        dicten programas de la UBV.
                                        </p>

                            <form id="form2" action="<?php echo constant('URL');?>aldea/ActualizarAldea"  method="post" class="wizard-big">
                                <h1>Descripción</h1>
                                <fieldset>
                                    <h2>Información </h2>
                                    <div class="row">
                                        <div class="col-lg-8">

                                        <input id="id_aldea" name="id_aldea" type="hidden" class="form-control required">
                                         
                                        <div class="form-group">
                                        <label>Código de Aldea <span style="color: red;">*</span></label>
                                        <input id="codaldea" placeholder="Ingrese el Código de la Aldea" name="codaldea" type="text" class="form-control required" maxlength='15' minlength="3">
                                         </div>

                                         <div class="form-group">
                                        <label>Código Sucre<span style="color: red;">*</span></label>
                                        <input id="sucre" placeholder="Ingrese el Código Sucre de la Aldea" name="sucre" type="text" class="form-control required" maxlength='15' minlength="3">
                                         </div>                                 

                                        <div class="form-group">
                                        <label>Aldea</label>
                                        <textarea id="aldean" name="aldean" placeholder="Ingrese el Nombre de la Aldea" type="text" class="form-control required valid" placeholder="Escriba una dirección..." aria-required="true" aria-invalid="false" onkeypress="return soloLetras(event)"  maxlength='70' minlength="4"></textarea>
                                        </div>
                            </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-institution" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </fieldset>
                            <h1>Ubicación</h1>
                                <fieldset>
                                    <h2>Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">

                                        <div class="form-group">
                                            <label>Tipo de Aldea<span style="color: red;">*</span></label>                                        
                                            <select class="chosen-select form-control  m-b required" name="tipoaldea" id="tipoaldea">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/estructura.php';
                                                foreach($this->aldeas as $row){
                                                $aldea=new Estructura();
                                                $aldea=$row;?> 
                                            <option value="<?php echo $aldea->id_aldea_tipo;?>"><?php echo $aldea->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>
                                        <div class="form-group">
                                        <label>Ambiente al que pertenece<span style="color: red;">*</span></label>                                        
                                            <select class="chosen-select form-control required m-b " name="ambienten" id="ambienten">                                    
                                            <option selected="selected" value="">Seleccione </option> 
                                            <?php include_once 'models/estructura.php';
                                                foreach($this->ambientes as $row){
                                                $ambiente=new Estructura();
                                                $ambiente=$row;?> 
                                            <option value="<?php echo $ambiente->id_ambiente;?>"><?php echo $ambiente->descripcion;?></option>
                                            <?php }?>                                            
                                            </select>   
                                            </div>

                                                                                
                                            <div class="form-group">
                                            <label>Estatus <span style="color: red;">*</span></label>
                                            <select class="form-control required m-b " name="estatuss" id="estatuss">                                      
                                            <option value="0" selected="selected">Inactivo</option>
                                            <option value ="1">Activo</option>                                               
                                            </select>                                              
                                            </div>
                                            
                                          
                            </div>
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-institution" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                </fieldset>
                               
                                
                          
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
  


    <?php require 'views/footer.php'; ?>

       
   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>

<!-- menu active -->
<script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>

<!-- Solo Letras -->
<script src="<?php echo constant ('URL');?>src/js/val_letras.js"></script>
  <!-- Chosen Select -->
  <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script> 
  




<script>
              
  $('#myModal1').on('shown.bs.modal', function () {  
       $('.chosen-select').chosen({width: "100%"});
       
       });

           
$('#myModal2').on('show.bs.modal', function(e) {
    
  var product = $(e.relatedTarget).data('id');
  $("#id_aldea").val(product);

  var product2 = $(e.relatedTarget).data('tipo');
  $("#tipoaldea").val(product2);

  var product3 = $(e.relatedTarget).data('aldea');
  $("#aldean").val(product3);

  var product4 = $(e.relatedTarget).data('ambiente');
//console.log(product4);
  $("#ambienten").val(product4);

  var product5 = $(e.relatedTarget).data('sucre');
  $("#sucre").val(product5);

  var product8 = $(e.relatedTarget).data('codaldea');
  $("#codaldea").val(product8);  

  var product7 = $(e.relatedTarget).data('estatus');
  $("#estatuss").val(product7);

  $('.chosen-select').chosen({width: "100%"});
  $('#tipoaldea').trigger('chosen:updated');
  $('#ambienten').trigger('chosen:updated');
  


});
</script>


    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("siguiente");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("anterior");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>





</body>
</html>
