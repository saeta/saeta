<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.8
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Estado | SIDTA</title>

    <link href="<?php echo constant ('URL');?>src/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!--  style -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/iCheck/custom.css" rel="stylesheet">
    <!--  steps -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/steps/jquery.steps.css" rel="stylesheet">

    <!--  datatables -->
    <link href="<?php echo constant ('URL');?>src/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <link href="<?php echo constant ('URL');?>src/css/animate.css" rel="stylesheet">
    <link href="<?php echo constant ('URL');?>src/css/style.css" rel="stylesheet">
 
 <link href="<?php echo constant ('URL');?>src/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

 


</head>

<body>
    <?php require 'views/header.php'; ?>
    
 


    <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><i class="fa fa-globe"></i> Estado</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">
                            <a href="<?php echo constant ('URL');?>estado">Extructura UBV</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a>Ubicación Geografica</a>
                        </li>
                        <li class="breadcrumb-item ">
                            <strong>Editar Estado</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                    <div class="title-action">
                    <?php  if($_SESSION['Agregar']==true){?>

                        <!-- boton agregar-->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1">
                            <i class="fa fa-plus"></i> Registrar Estado
                        </button> 
                    <?php } ?>
                    </div>
                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">

                
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Listado de Estados registrados</h5>
                       
                        <div class="ibox-tools">
                        


                        </div>
                    </div>
                    
                    <div class="ibox-content">
                    <?php echo $this->mensaje; ?>
                        <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                    <tr>
                        <th>Código INE</th>
                        <th><i class="fa fa-globe"></i> Estado</th>
                        <th><i class="fa fa-globe"></i> País al que Pertenece</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>

                    
                    <?php include_once 'models/estructura.php';
                            foreach($this->estados as $row){
                                $estado= new Estructura();
                                $estado=$row;?>
                    <tr class="gradeX">
                    <td><?php echo $estado->codigo; ?> </td>
                    <td><?php echo $estado->estado; ?></td>
                    <td><?php echo $estado->pais; ?></td>
                    <td class="center">
                        <?php if( $estado->estatus_pais=='false'){ ?>
                    <a class="btn btn-outline btn-danger" href="#" role="button" title="Pais inactivo">Inactivo</a> &nbsp;
                        <?php }else{?>      
                            <?php  if($_SESSION['Editar']==true){?>

                    <a class="btn btn-outline btn-success" href="#myModal2" role="button" data-toggle="modal" data-ide="<?php echo $estado->id_estado;?>" data-nombre="<?php echo  $estado->estado;?>"  data-cod="<?php echo $estado->codigo; ?>"data-pais="<?php echo $estado->id_pais; ?>" ><i class="fa fa-edit"></i> Editar</a>
                        <?php }
                    }?>

                    </td>
                    </tr>
                            <?php }?>
                    
                    </tbody>
                   
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
<!-- Modal para Agregar estados-->
        <div class="modal inmodal fade " style="width: 100%;" id="myModal1" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Registar Estado </h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            <h2>
                            Registar Estado
                            </h2>
                            <p>
                            Permite al usuario registrar un estado de
                            la división política del país.
                            </p>

                            <form id="form" action="<?php echo constant('URL');?>estado/registrarEstado"  method="post" class="wizard-big">
                                <h1>Descripción</h1>
                                <fieldset>
                                    <h2> Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">

                                        <div class="form-group">
                                            <label>País <span style="color: red;">*</span></label>
                                         
                                            <select class="chosen-select form-control required m-b " name="id_pais" id="id_pais">
                                       
                                            <option selected="selected" value="">Seleccione una País</option> 
                                            <?php include_once 'models/estructura.php';
                                          foreach($this->paises as $row){
                                          $pais=new Estructura();
                                            $pais=$row;?> 
                                            <option value="<?php echo $pais->id_pais;?>"><?php echo $pais->descripcion;?></option>
                                            <?php }?>
                                               
                                            </select>                                       
                                            </div>

                                            <div class="form-group">
                                                <label>Código INE <span style="color: red;">*</span></label>
                                                <input id="codigoine" name="codigoine" placeholder="Ingrese el Código INE del Estado" type="text" class="form-control required number" maxlength='6' minlength="2">
                                            </div>
                                            <div class="form-group">
                                                <label>Estado <span style="color: red;">*</span></label>
                                                <input id="estado" name="estado" type="text" placeholder="Ingrese el Nombre del Estado" class="form-control required" onkeypress="return soloLetras(event)" maxlength='50' minlength="5">
                                            </div>
                                       
                                           
                                           
                                        </div>
                                        
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-university" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                                
                          
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
         

        
             
<!-- Modal para Editar estados-->
        <div class="modal inmodal fade " style="width: 100%;" id="myModal2" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cancelar</span></button>
                                            <h4 class="modal-title">Editar  Estado </h4>
                                            <small class="font-bold"> Los campos identificados con <span style="color: red;">*</span> son obligatorios </small>
                                        </div>
                                        <div class="modal-body" >
                                            
                                        <div class="ibox-content">
                            <h2>
                            Editar  Estado
                            </h2>
                            <p>
                            Permite al usuario  actualizar un estado de
                            la división política del país.
                            </p>

                            <form id="form2" action="<?php echo constant('URL');?>estado/ActualizarEstado"  method="post" class="wizard-big">
                                <h1>Descripción</h1>
                                <fieldset>
                                    <h2> Información</h2>
                                    <div class="row">
                                        <div class="col-lg-8">

                                        <div class="form-group">
                                            <label>País <span style="color: red;">*</span></label>
                                            <select  name="id_pais2" id="id_pais2" class="chosen-select form-control required m-b" >
                                            <option selected="selected" value="">Seleccione una País</option> 
                                            <?php include_once 'models/estructura.php';
                                          foreach($this->paises as $row){
                                          $pais=new Estructura();
                                            $pais=$row;?> 
                                            <option value="<?php echo $pais->id_pais;?>"><?php echo $pais->descripcion;?></option>
                                            <?php }?>                                               
                                            </select>   
                                            </div>
                                            

                                            <div class="form-group">
                                            <input id="id_estado2" name="id_estado2" type="hidden" class="form-control" >
                                      
                                                <label>Código INE <span style="color: red;">*</span></label>
                                                <input id="codigoine2" name="codigoine2" placeholder="Ingrese el Código INE del Estado" type="text" class="form-control required number" maxlength='6' minlength="2">
                                            </div>
                                            <div class="form-group">
                                                <label>Estado <span style="color: red;">*</span></label>
                                                <input id="estado2" name="estado2" type="text" placeholder="Ingrese el Nombre del Estado" class="form-control required" onkeypress="return soloLetras(event)" maxlength='50' minlength="5">
                                            </div>
                                       

                                        </div>
                                    
                                        <div class="col-lg-4">
                                            <div class="text-center">
                                                <div style="margin-top: 20px">
                                                    <i class="fa fa-university" style="font-size: 180px;color: #e5e5e5 "></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </fieldset>
                                
                          
                            </form>
                        </div>
                                        
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                                            <!--<button type="button" class="btn btn-primary">Save changes</button>-->
                                  </div>
                              </div>
                         </div>
                     </div>        

<!--////////////////////////////////-->
         








    <?php require 'views/footer.php'; ?>

       
   <!-- dataTables -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/datatables.min.js"></script>
    <script src="<?php echo constant ('URL');?>src/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>

    <!-- Steps -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/steps/jquery.steps.min.js"></script>

    <!-- Jquery Validate -->
    <script src="<?php echo constant ('URL');?>src/js/plugins/validate/jquery.validate.min.js"></script>
   <!-- Chosen Select -->
   <script src="<?php echo constant ('URL');?>src/js/plugins/chosen/chosen.jquery.js"></script> 
    
    <!-- Solo Letras -->
    <script src="<?php echo constant ('URL');?>src/js/val_letras.js"></script>
    <!-- menu active -->
    <script src="<?php echo constant ('URL');?>src/js/activemenu.js"></script>



    <script>
  $('#myModal1').on('shown.bs.modal', function () {  
       $('.chosen-select').chosen({width: "100%"});
       });


$('#myModal2').on('show.bs.modal', function(e) {

   
    
  var product = $(e.relatedTarget).data('ide');
  $("#id_estado2").val(product);

  var product2 = $(e.relatedTarget).data('nombre');
  $("#estado2").val(product2);

  var product3 = $(e.relatedTarget).data('cod');
  $("#codigoine2").val(product3);

  var product4 = $(e.relatedTarget).data('pais');
  $("#id_pais2").val(product4);
  //actualiza chosen pues este se quedaba estatico

  $('.chosen-select').chosen({width: "100%"});
  $('#id_pais2').trigger('chosen:updated');

});
</script>


    <script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>

<script>
        $(document).ready(function(){
            $("#wizard").steps();
            $("#form2").steps({
                bodyTag: "fieldset",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex)
                    {
                        return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                        return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18)
                    {
                        $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3)
                    {
                        $(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    var form = $(this);

                    // Submit form input
                    form.submit();
                }
            }).validate({
                        errorPlacement: function (error, element)
                        {
                            element.before(error);
                        },
                        rules: {
                            confirm: {
                                equalTo: "#password"
                            }
                        }
                    });
       });
    </script>



    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    

                    
                ]

            });

        });

    </script>
</body>
</html>
