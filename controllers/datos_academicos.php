<?php
include_once 'sesiones/session_admin.php';
include_once 'scripts/files.php';

class Datos_academicos extends Controller{
    
    function __construct(){
        parent::__construct();
        $this->view->mensaje="";
        $this->view->estudios=[];
        $this->view->estudios_idioma=[];

        $this->view->estudio=[];
        $this->view->documento="";

        $this->view->paises=[];
        $this->view->modalidades=[];
        $this->view->tipos_institucion=[];
        $this->view->instituciones=[];
        $this->view->tipos_estudio=[];
        $this->view->idiomas=[];

      //  $this->view->render('ayuda/index');
    }

    function render(){

        //obtener estudios
        $estudios=$this->model->getEstudio($_SESSION['id_docente']);
       // var_dump($estudios['id_estudio']);
        $this->view->estudios=$estudios;

        $estudios_conducentes=$this->model->getEstudioConducente($_SESSION['id_docente']);
        //var_dump($estudios_conducentes);
        $this->view->estudios_conducentes=$estudios_conducentes;

        //obtener estudios
        $estudios_idioma=$this->model->getEstudioIdioma($_SESSION['id_docente']);

        //var_dump($estudios_idioma);
        $this->view->estudios_idioma=$estudios_idioma;
        
        $this->view->render('datos_academicos_idiomas/index');
    }

    //render de la vista de agregar
    function viewAdd(){

        //obtener paises
        $paises=$this->model->getPais();
        $this->view->paises=$paises;

        //obtener modalidades de estudio
        $modalidades=$this->model->getCatalogo('modalidad_estudio');
        $this->view->modalidades=$modalidades;
        
        //obtener tipos de institucion
        $tipos_institucion=$this->model->getCatalogo('institucion_tipo');
        $this->view->tipos_institucion=$tipos_institucion;

        //obtener tipos de estudio
        $tipos_estudio=$this->model->getCatalogo('tipo_estudio');
        $this->view->tipos_estudio=$tipos_estudio;

        //obtener niveles de estudio
        $niveles_academicos=$this->model->getCatalogo('nivel_academico');
        $this->view->niveles_academicos=$niveles_academicos;
        
        //obtener tipo de estudio de alto nivel
        $programa_niveles=$this->model->getCatalogo('programa_nivel');
        $this->view->programa_niveles=$programa_niveles;

        //obtener instituciones
        $instituciones=$this->model->getInstitucion();
        $this->view->instituciones=$instituciones;

        //obtener idiomas
        $idiomas=$this->model->getIdioma();
        $this->view->idiomas=$idiomas;
        
        $this->view->render('datos_academicos_idiomas/agregar_estudio');

    }

    //render de la vista de editar
    function viewEdit($param = null){
       
        list($id_estudio,$id_tipo_estudio)=explode(',', $param[0]);
        //redireccion dependiendo del tipo de estudio
        switch($id_tipo_estudio){
            case 1://estudio conducente
            $estudio=$this->model->getByIdEstudioConducente($id_estudio);
            $_SESSION['id_estudio_conducente']=$id_estudio;            
            break;
            case 2://estudio NO conducente
            $estudio=$this->model->getByIdEstudio($id_estudio);
            $_SESSION['id_estudio_conducente']=$id_estudio;
            break;
            case 3://estudio IDIOMA
            $estudio=$this->model->getByIDEstudioIdioma($id_estudio);
            $_SESSION['id_estudio_idioma']=$id_estudio;
            break;
        }
        
        
        //obtener nombre de documento
        $documento=$this->model->getDocumentbyID($estudio->id_estudio);
        $this->view->descripcion_documento=$documento->descripcion;
        $this->view->documento=$estudio->id_estudio;

        $_SESSION['id_tipo_estudio']=$id_tipo_estudio;
        $this->view->estudio=$estudio;

       
        //obtener paises
        $paises=$this->model->getPais();
        $this->view->paises=$paises;

        //obtener modalidades de estudio
        $modalidades=$this->model->getCatalogo('modalidad_estudio');
        $this->view->modalidades=$modalidades;
        
        //obtener tipos de institucion
        $tipos_institucion=$this->model->getCatalogo('institucion_tipo');
        $this->view->tipos_institucion=$tipos_institucion;

        //obtener tipos de estudio
        $tipos_estudio=$this->model->getCatalogo('tipo_estudio');
        $this->view->tipos_estudio=$tipos_estudio;

        //obtener niveles de estudio
        $niveles_academicos=$this->model->getCatalogo('nivel_academico');
        $this->view->niveles_academicos=$niveles_academicos;
        
        //obtener tipo de estudio de alto nivel
        $programa_niveles=$this->model->getCatalogo('programa_nivel');
        $this->view->programa_niveles=$programa_niveles;
        
        //obtener instituciones
        $instituciones=$this->model->getInstitucion();
        $this->view->instituciones=$instituciones;

        //obtener idiomas
        $idiomas=$this->model->getIdioma();
        $this->view->idiomas=$idiomas;
        
        $this->view->render('datos_academicos_idiomas/editar_estudio');
    }

    //render de la vista de editar
    function viewDetail($param = null){
       
        list($id_estudio,$id_tipo_estudio)=explode(',', $param[0]);
        //redireccion dependiendo del tipo de estudio
        switch($id_tipo_estudio){
            case 1://estudio conducente
            $estudio=$this->model->getByIdEstudioConducente($id_estudio);
            $_SESSION['id_estudio_conducente']=$id_estudio;            
            break;
            case 2://estudio NO conducente
            $estudio=$this->model->getByIdEstudio($id_estudio);
            $_SESSION['id_estudio_conducente']=$id_estudio;
            break;
            case 3://estudio IDIOMA
            $estudio=$this->model->getByIDEstudioIdioma($id_estudio);
            $_SESSION['id_estudio_idioma']=$id_estudio;
            break;
        }
        
        //obtener nombre de documento
        $documento=$this->model->getDocumentbyID($estudio->id_estudio);
        $this->view->descripcion_documento=$documento->descripcion;
        $this->view->documento=$estudio->id_estudio;

        $_SESSION['id_tipo_estudio']=$id_tipo_estudio;
        $this->view->estudio=$estudio;

        //obtener paises
        $paises=$this->model->getPais();
        $this->view->paises=$paises;

        //obtener modalidades de estudio
        $modalidades=$this->model->getCatalogo('modalidad_estudio');
        $this->view->modalidades=$modalidades;
        
        //obtener tipos de institucion
        $tipos_institucion=$this->model->getCatalogo('institucion_tipo');
        $this->view->tipos_institucion=$tipos_institucion;

        //obtener tipos de estudio
        $tipos_estudio=$this->model->getCatalogo('tipo_estudio');
        $this->view->tipos_estudio=$tipos_estudio;

        //obtener instituciones
        $instituciones=$this->model->getInstitucion();
        $this->view->instituciones=$instituciones;

        //obtener idiomas
        $idiomas=$this->model->getIdioma();
        $this->view->idiomas=$idiomas;
        
        $this->view->render('datos_academicos_idiomas/detalleEstudio');
    }

    function viewDocumento($param = null){

        $id_estudio=$param[0];
        $documento=$this->model->getDocumentbyID($id_estudio);
        //var_dump($documento->descripcion);
        $this->view->documento="datos_academicos_idiomas/".rawurlencode($documento->descripcion);
        $this->view->render('documentos/viewDocumento');

    }

    //si el tipo de estudio es "Conducente" se llama esta funcion
    function registrarEstudio(){
        
        //estudio paso 1
        $estudio=$_POST['estudio'];
        $pais=$_POST['pais'];
        $institucion_tipo=$_POST['institucion_tipo'];
        $institucion=$_POST['institucion'];
        $ano_r=$_POST['ano_r'];
        $modalidad_estudio=$_POST['modalidad'];
        //var_dump('estudio paso 1 ---',$estudio, $pais, $institucion, $institucion_tipo, $ano_r, $modalidad);
        //var_dump('modalidad', $modalidad_estudio);
        $tipo_estudio=$_POST['tipo_estudio'];
       // var_dump('>>>>>>>>>>>>>', $tipo_estudio);
        
       
       
      
        //var_dump('estudio conducentes ---',$titulo_estudio, $estatus);

        
        //var_dump('estudio conducentes-concluido ---',$resumen, $titulo_obtenido);
        $band=false;
        $id_institucion="";
        if($var=$this->model->existe_institucion($institucion)){
           $id_institucion=$var['id_institucion'];
           $band=true;
        }

        //var_dump($id_institucion, $band, $var);
        if($var2=$this->model->existe($estudio)){
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
             El estudio Conducente  <b>" . $var2 . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
            </div>";
            $this->view->mensaje=$mensaje;
            $this->viewAdd();
            exit();
        }

        if($tipo_estudio == 2){//2=noconducente

            //no conducentes
            $estatus=$_POST['estatus_nc'];
            $titulo_obtenido=$_POST['titulo_certificado'];

            if($_POST['estatus_nc']=="concluido-noconducente"){//estos valores vienen de la vista
                $estatus="concluido";
                //no conducentes-concluido
                $pdf_titulo=$_FILES['pdf_certificado'];
            }elseif($_POST['estatus_nc']=="en_curso-noconducente"){//estos valores vienen de la vista
                $estatus="en curso";
            }
            $id_nivel_academico="";
            $id_programa_nivel="";


        }elseif($tipo_estudio == 1){//1=conducente
            //conducentes
            $titulo_obtenido=$_POST['titulo_obtenido'];
            $estatus=$_POST['estatus_c'];

            //tabla estudio_nivel
            $id_nivel_academico=$_POST['nivel_academico'];
            $id_programa_nivel=$_POST['programa_nivel'];

            if($_POST['estatus_c']=="concluido-conducente"){
                $estatus="concluido";
                //conducentes-concluido
                $titulo_trabajo=$_POST['titulo_trabajo'];
                $resumen=$_POST['resumen'];
                $pdf_titulo=$_FILES['pdf_titulo'];

            }elseif($_POST['estatus_c']=="en_curso-conducente"){
                $estatus="en curso";
            }

            
        }elseif($tipo_estudio == 3){//3=Idioma
            
            //idiomas
            $descripcion=$_POST['idioma'];
            $nivel_escritura=$_POST['nivel_escritura'];
            $nivel_comprende=$_POST['nivel_comprende'];
            $nivel_lectura=$_POST['nivel_lectura'];
            $nivel_habla=$_POST['nivel_habla'];

            //vacio ya queno aplica para esta tabla 
            $id_nivel_academico="";
            $id_programa_nivel="";

            if($_POST['competencia']=='certificado-idioma'){
                $estatus="concluido";
                $pdf_titulo=$_FILES['file_certificado'];

            }elseif($_POST['competencia']=='nocertificado-idioma'){
                $estatus="en curso";

            }

        }



        
        //mover los archivos e insertar en la tabla conducente 
        // y en caso de concluido mover el archivo correspondiente
        if($this->model->insertEstudio([
            'estudio'=>$estudio,
            'id_tipo_estudio'=>$tipo_estudio,
            'estatus'=>$estatus,
            'id_pais'=>$pais,
            'id_modalidad_estudio'=>$modalidad_estudio,
            'ano_estudio'=>$ano_r,
            'institucion'=>$institucion,
            'id_institucion'=>$id_institucion,
            'id_docente'=>$_SESSION['id_docente'],
            'id_institucion_tipo'=>$institucion_tipo,
            'titulo_obtenido'=>$titulo_obtenido,
            'titulo_trabajo'=>$titulo_trabajo,
            'resumen'=>$resumen,
            'id_persona'=>$_SESSION['id_persona'],
            'pdf_titulo'=>$pdf_titulo,
            'nivel_lectura'=>$nivel_lectura,
            'nivel_escritura'=>$nivel_escritura,
            'nivel_habla'=>$nivel_habla,
            'nivel_comprende'=>$nivel_comprende,
            'id_idioma'=>$descripcion,
            'id_nivel_academico'=>$id_nivel_academico,
            'id_programa_nivel'=>$id_programa_nivel
            ])){
                $mensaje='<div class="alert alert-success">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        El estudio '.$estudio.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                    </div>';
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                    Ha ocurrido un error al registrar el <a class='alert-link' href='#'>Estudio Conducente</a>
                </div>";
            }
        


        



        
        $this->view->mensaje=$mensaje;
        $this->render();
    }


    function editar($param){


    
        $id_estudio_tabla=$_POST['id_estudio_tabla'];
        //var_dump('controll', $id_estudio_tabla);
        //estudio paso 1
        $estudio=$_POST['estudio'];
        $pais=$_POST['pais'];
        $institucion_tipo=$_POST['institucion_tipo'];
        $institucion=$_POST['institucion'];
        $ano_r=$_POST['ano_r'];
        $modalidad_estudio=$_POST['modalidad'];
        //var_dump('estudio paso 1 ---',$estudio, $pais, $institucion, $institucion_tipo, $ano_r, $modalidad);
        //var_dump('modalidad', $modalidad_estudio);
        $tipo_estudio=$_POST['tipo_estudio'];
       // var_dump('>>>>>>>>>>>>>', $tipo_estudio);
        
       
       
      
        //var_dump('estudio conducentes ---',$titulo_estudio, $estatus);

        
        //var_dump('estudio conducentes-concluido ---',$resumen, $titulo_obtenido);
        $band=false;
        $id_institucion="";
        if($var=$this->model->existe_institucion($institucion)){
           $id_institucion=$var['id_institucion'];
           $band=true;
        }
        //var_dump($id_institucion, $band);

        if($tipo_estudio == 2){//2=noconducente

            //no conducentes
            $estatus=$_POST['estatus_nc'];
            $titulo_obtenido=$_POST['titulo_certificado'];

            //var_dump('estudio no conducentes ---', $estatus, $titulo_certificado);

            //var_dump('estudio no conducentes-concluido ---', $pdf_certificado);
            //vacio
            $id_nivel_academico="";
            $id_programa_nivel="";

            if($_POST['estatus_nc']=="concluido-noconducente"){
                $estatus="concluido";
                //no conducentes-concluido
                $pdf_titulo=$_FILES['pdf_certificado'];
            }elseif($_POST['estatus_nc']=="en_curso-noconducente"){
                $estatus="en curso";
            }
           

        }elseif($tipo_estudio == 1){//1=conducente
            //conducentes
            $titulo_obtenido=$_POST['titulo_obtenido'];
            $estatus=$_POST['estatus_c'];
        
            //tabla estudio_nivel
            $id_nivel_academico=$_POST['nivel_academico'];
            $id_programa_nivel=$_POST['programa_nivel'];
            
            if($_POST['estatus_c']=="concluido-conducente"){
                $estatus="concluido";
                //conducentes-concluido
                $titulo_trabajo=$_POST['titulo_trabajo'];
                $resumen=$_POST['resumen'];
                $pdf_titulo=$_FILES['pdf_titulo'];

            }elseif($_POST['estatus_c']=="en_curso-conducente"){
                $estatus="en curso";
            }

            
        }elseif($tipo_estudio == 3){//3=Idioma
            
            //idiomas
            $descripcion=$_POST['idioma'];
            $nivel_escritura=$_POST['nivel_escritura'];
            $nivel_comprende=$_POST['nivel_comprende'];
            $nivel_lectura=$_POST['nivel_lectura'];
            $nivel_habla=$_POST['nivel_habla'];

            //vacio
            $id_nivel_academico="";
            $id_programa_nivel="";


            if($_POST['competencia']=='certificado-idioma'){
                $estatus="concluido";
                $pdf_titulo=$_FILES['file_certificado'];

            }elseif($_POST['competencia']=='nocertificado-idioma'){
                $estatus="en curso";

            }

        }

        $id_estudio=$_POST['id_estudio'];
        
        if($this->model->update([
            'estudio'=>$estudio,
            'id_tipo_estudio'=>$tipo_estudio,
            'estatus'=>$estatus,
            'id_pais'=>$pais,
            'id_modalidad_estudio'=>$modalidad_estudio,
            'ano_estudio'=>$ano_r,
            'institucion'=>$institucion,
            'id_institucion'=>$id_institucion,
            'id_docente'=>$_SESSION['id_docente'],
            'id_institucion_tipo'=>$institucion_tipo,
            'titulo_obtenido'=>$titulo_obtenido,
            'titulo_trabajo'=>$titulo_trabajo,
            'resumen'=>$resumen,
            'id_persona'=>$_SESSION['id_persona'],
            'pdf_titulo'=>$pdf_titulo,
            'nivel_lectura'=>$nivel_lectura,
            'nivel_escritura'=>$nivel_escritura,
            'nivel_habla'=>$nivel_habla,
            'nivel_comprende'=>$nivel_comprende,
            'id_idioma'=>$descripcion,
            'id_estudio_tabla'=>$id_estudio_tabla,
            'id_nivel_academico'=>$id_nivel_academico,
            'id_programa_nivel'=>$id_programa_nivel,
            'id_estudio'=>$id_estudio
            ])){
            $mensaje='<div class="alert alert-success">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    El estudio '.$estudio.' ha sido Actualizado <a class="alert-link" href="#">Exitosamente</a>.
                </div>';
                
        }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar el <a class='alert-link' href='#'>Estudio Conducente</a>
            </div>";
        }
    

        $this->view->mensaje=$mensaje;
        $this->render($param);

        


    }



    function delete($param=null){
        
        list($id_estudio_tabla,$id_tipo_estudio,$id_estudio, $estatus)=explode(',', $param[0]);
  
        if($this->model->delete([
            'id_estudio_tabla'=>$id_estudio_tabla,
            'id_tipo_estudio'=>$id_tipo_estudio,
            'id_estudio'=>$id_estudio,
            'estatus'=>$estatus
            ])){

            $mensaje="<div class='alert alert-success alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            <b>El Estudio </b> ha sido Removido correctamente! <a class='alert-link' href='#'></a>
        </div>";

       }else{
        $mensaje="<div class='alert alert-danger alert-dismissable'>
        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            <b>ERROR: </b>Ha ocurrido un error al intentar remover el <a class='alert-link' href='#'>Estudio</a>
        </div>";
 
       }

       $this->view->mensaje=$mensaje;
       $this->render();


    }

    

}
?>