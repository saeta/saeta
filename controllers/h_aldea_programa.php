<?php
include_once 'sesiones/session_admin.php';

class H_aldea_programa extends Controller{
    function __construct(){
        parent::__construct();
        $this->view->mensaje="";
        $this->view->aldeas=[];
        $this->view->programas=[];
        $this->view->aldeaprogramas=[];
        
       
    }

    function render(){
        $aldeas=$this->model->getAldea();
        $this->view->aldeas=$aldeas;
        $programas=$this->model->getProgramaF();
        $this->view->programas=$programas;
        $aldeaprogramas=$this->model->getAldeaP();
        $this->view->aldeaprogramas=$aldeaprogramas;
        $this->view->render('adm_datos_academicos/h_aldea_programa');
    }

   

}
?>