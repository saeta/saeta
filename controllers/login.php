<?php 
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require 'vendor/autoload.php';

class Login extends Controller{
    function __construct(){
        parent::__construct();
        $this->view->mensaje="";
        $this->view->cedula="";
        $this->view->ejes_regionales=[];
        $this->view->centros=[];
        $this->view->programas=[];
        $this->view->preguntas=[];
        $this->view->paises=[];
        $this->view->estados=[];
        $this->view->municipios=[];
        $this->view->parroquias=[];            
    }

    function render(){
        session_start();
        if(isset($_SESSION['id_persona'])){
            header("Location: ".constant('URL')."home");
        }else{
            $this->view->render('login/index');
        }
        
    }

    function render2(){
        
        $this->view->render('gestion_usuarios/agregar_usuario');
    }

    function viewRecuperar(){
        
        $this->view->render('login/recuperar');
    }
    
    //render de la vista de regitro de un Docente
    function viewRegistrar(){
        
        //obtener ejes regionales
        $ejes_regionales=$this->model->getEjeRegional();
        $this->view->ejes_regionales=$ejes_regionales;

        //obtener centros de estudio
        $centros=$this->model->getCentro();
        $this->view->centros=$centros;
        
        //obtener programas
        $programas=$this->model->getPrograma();
        $this->view->programas=$programas;

        //obtener preguntas
        $preguntas=$this->model->getPregunta();
        $this->view->preguntas=$preguntas;
        
        //obtener paises
        $paises=$this->model->getPais();
        $this->view->paises=$paises;
        
        //obtener estados
        $estados=$this->model->getEstado();
        $this->view->estados=$estados;
        
        //obtener municipios
        $municipios=$this->model->getMunicipio();
        $this->view->municipios=$municipios;
        
        //obtener parroquias
        $parroquias=$this->model->getParroquia();
        $this->view->parroquias=$parroquias;

        $aldeas_ubv=$this->model->getCatalogo("aldea_ubv");
        $this->view->aldeas_ubv=$aldeas_ubv;

        

        $this->view->render('login/registro');
    }

    function viewHome(){
        header("Refresh: 0; URL= ".constant('URL')."home");
    }

    //recibir datos de inicio de sesion para autenticacion
    function iniciar(){

        //ELIMINAMOS LAS SESION CREADA PARA EL CAPTCHA
        unset($_SESSION['captcha']);

        $usuario=$_POST["usuario"];
        $password=$_POST["password"];
        //$login=array('usuario'=>$usuario,'password'=>$password);
        $mensaje="";
        /*session_start();
        $captcha = $_POST['captcha'];
       
        //validar captcha
        $check = false;
        if(isset($_SESSION['captcha'])) {
                // Case sensitive Matching
            if ($captcha == $_SESSION['captcha']) {
                $check = true;
                // var_dump('paso');
            }else if($check==false){
                //var_dump('no paso');
    
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                    Error en validación de Captcha.
                    </div>";
    
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
            }
            unset($_SESSION['captcha']);
        }*/

            //////////////////////////////////////////////////////
      
       
            $fecha_actual=date('Y/m/d');

            if($this->model->getLogin([
                'usuario'=> $usuario, 
                'password' => $password,
                'fecha_actual'=>$fecha_actual
                ])){
                
                //se agrega esta linea
                $mensaje="<large style='color: green;'>Bienvenido, ".$_SESSION['nombre1']."exitosa!</large>";

                

                $this->viewHome();
                
            } else{
                $mensaje="<large style='color: red;'>Cédula o contraseña inválida **</large>";

            }
            
        

        $this->view->mensaje=$mensaje;
        $this->render();

    }


    function registrar(){


//ELIMINAMOS LAS SESION CREADA PARA EL CAPTCHA
unset($_SESSION['captcha']);

            $cedula=$_POST['cedula'];

            if($this->model->existe($cedula)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close type='button'></button>
                Usted ya posee cuenta en<a class='alert-link' href='#'> SIDTA</a> </div>";
                $this->view->mensaje=$mensaje;
                
                switch($_POST['admin-user']){
                    case 0:
                        $this->render();
                    break;
                    
                    case 1:
                        $this->render2();
                    break;
                }
                
                exit();
            }
            
            
           

            $registro=$this->model->getbyCedula($cedula);

            //var_dump(empty($registro->pais_nacimiento));
            
            // si el pais de nacimiento que viene de sigad esta vacío, insertamos el 
            // pais de ubicicación que selecciono el docente, y lo comparo con un ciclo para obtener la descripcion y no tocar el modelo
            //fecha de modificacion: 17-02-2020
            if(empty($registro->pais_nacimiento)){
                
                $id_pais=$_POST['pais'];

                $paises=$this->model->getPais();
                foreach($paises as $row){
                    $pais=new Estructura();
                    $pais=$row;
                    if($pais->id_pais==$id_pais){
                        $registro->pais_nacimiento=$pais->descripcion;
                    }
                }
                
            }
            
            //separar el campo cargo para insertarlo correctamente en nuestra base de datos
            switch($registro->cargo){
                case stristr($registro->cargo, 'INSTRUCTOR') !== false:
                $var=stristr($registro->cargo, 'INSTRUCTOR');
                $registro->cargo=substr($var, 0, 10);
                break;

                case stristr($registro->cargo, 'ASISTENTE') !== false:
                $var=stristr($registro->cargo, 'ASISTENTE');    
                $registro->cargo=substr($var, 0, 9);
                break;

                case stristr($registro->cargo, "AGREGADO") !== false:
                $var=stristr($registro->cargo, 'AGREGADO');  
                $registro->cargo=substr($var, 0, 8);
                break;

                case stristr($registro->cargo, "ASOCIADO") !== false:
                $var=stristr($registro->cargo, 'ASOCIADO');     
                $registro->cargo=substr($var, 0, 8);
                break;

                case stristr($registro->cargo, "TITULAR") !== false:
                $var=stristr($registro->cargo, 'TITULAR'); 
                $registro->cargo=substr($var, 0, 7);
                break;

                case stristr($registro->cargo, "EMÉRITO") !== false:
                $var=stristr($registro->cargo, 'EMÉRITO');  
                $registro->cargo=substr($var, 0, 8);              
                break;

                case stristr($registro->cargo, "AUXILIAR") !== false:
                $var=stristr($registro->cargo, 'AUXILIAR');
                $registro->cargo=substr($var, 0, 12);                
                break;
            }
            ////////////////////////////////////////////////////////////////////////////////////

            if(empty($registro->cedula)){
                
                
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                    Tu cédula no Esta Registrada en SIGAD, comunicate con Talento Humano para registrar tus datos
                    </div>";
                    
            }else{

                //separamos por coma ',' el nombre completo
                list($apellidos, $nombres) = explode(",", $registro->nombres);
                
                $apellido=$this->separar_cadena($apellidos);

                $nombre=$this->separar_cadena($nombres);
               
                
                //separar codigo de area del campo telefono
                if(!empty($registro->telefono)){
                    $cod_area=substr($registro->telefono, 0, 3);
                    $telefono=substr($registro->telefono, 3, 10);
                }else{
                    $cod_area="";
                    $telefono="";
                }

                //evaluar si el correo es Personal o Institucional
                if(!empty($registro->correo_electronico)){
                    //separamos el correo por '@'
                    list($correo, $dominio) = explode("@", $registro->correo_electronico);
                    //var_dump($correo, ">>", $dominio);
                    if($dominio != "ubv.edu.ve"){
                        $correo_tipo="Personal";
                    } else{
                        $correo_tipo="Institucional";
                    }
                }

                //datos docente
                $programa=$_POST['programa'];
                $centro_estudio=$_POST['centro_estudio'];
                $eje_regional=$_POST['eje_regional'];
                $eje_municipal=$_POST['eje_municipal'];
                $aldea=$_POST['aldea'];

                $password=$_POST['password'];

                //seguridad
                $pregunta1=$_POST['pregunta1'];
                $respuesta1=$_POST['respuesta1'];

                $pregunta2=$_POST['pregunta2'];
                $respuesta2=$_POST['respuesta2'];
                $parroquia=$_POST['parroquia'];

                if($pregunta1==$pregunta2){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close type='button'></button>
                    <i>ERROR:</i> Las <a class='alert-link' href='#'> preguntas de seguridad</a>  deben ser diferentes</div>";
                    $this->view->mensaje=$mensaje;
                    switch($_POST['admin-user']){
                        case 0:
                            $this->viewRegistrar();
                        break;
                        
                        case 1:
                            $this->render2();
                        break;
                    }
                    exit();
                    
                }elseif($respuesta1==$respuesta2){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close type='button'></button>
                    <i>ERROR:</i> Las <a class='alert-link' href='#'> respuestas de seguridad</a> deben ser diferentes</div>";
                    $this->view->mensaje=$mensaje;
                    switch($_POST['admin-user']){
                    case 0:
                        $this->viewRegistrar();
                    break;
                    
                    case 1:
                        $this->render2();
                    break;
                }
                    exit();

                }
                
                $usuario='ubv'.$cedula;

                $this->view->cedula=$registro->cedula;
                if($this->model->insert([
                    'id_nomina_docente_sigad'=>$registro->id_nomina_docente_sigad,
                    'cedula'=>$registro->cedula,
                    'primer_nombre'=>$nombre[0],
                    'segundo_nombre'=>$nombre[1],
                    'primer_apellido'=>$apellido[0],
                    'segundo_apellido'=>$apellido[1],
                    'nacionalidad'=>trim($registro->nacionalidad),
                    'fecha_nacimiento'=>$registro->fecha_nacimiento,
                    'pais_nacimiento'=>ucfirst(strtolower(trim($registro->pais_nacimiento))),
                    'estado_nacimiento'=>$registro->estado_nacimiento,
                    'genero'=>ucfirst(strtolower(trim($registro->genero))),
                    'estado_civil'=> ucfirst(strtolower(trim($registro->estado_civil))),
                    'telefono'=>$telefono,
                    'correo_electronico'=>$registro->correo_electronico,
                    'direccion'=>$registro->direccion,
                    'numero_hijos'=>$registro->numero_hijos,
                    'cargo'=>strtoupper($registro->cargo),
                    'clasificacion_docente'=>strtoupper($registro->clasificacion_docente),
                    'fecha_ingreso'=>$registro->fecha_ingreso,
                    'dedicacion'=>strtoupper($registro->dedicacion),
                    'estatus'=>strtoupper($registro->estatus),
                    'cod_area'=>$cod_area,
                    'correo_tipo'=>$correo_tipo,
                    'programa'=>$programa,
                    'centro_estudio'=>$centro_estudio,
                    'eje_regional'=>$eje_regional,
                    'password'=>$password,
                    'usuario'=>$usuario,
                    'pregunta1'=>$pregunta1,
                    'respuesta1'=>$respuesta1,
                    'pregunta2'=>$pregunta2,
                    'respuesta2'=>$respuesta2,
                    'parroquia'=>$parroquia,
                    'eje_municipal'=>$eje_municipal,
                    'aldea'=>$aldea
                    ])){
                        
                        $mensaje="<div class='alert alert-success alert-dismissable'>
                        Has Sido registrado satisfactoriamente! tu nombre de usuario es: <b>".$usuario."</b> <br>
                        </div>";

                        if(!empty($registro->correo_electronico)){
                            $mensaje="<div class='alert alert-success alert-dismissable'>
                            Has Sido registrado satisfactoriamente! tu nombre de usuario es: <b>".$usuario."</b> <br>
                            Se te ha enviado un Correo Eléctronico a la dirección que tienes Registrada en SIGAD: <b>".$registro->correo_electronico."</b> con tu nombre de Usuario: <b>".$usuario."</b>
                            </div>";
                            $mensaje2="<p>
                            ¡Felicidades! Sr.(a) ".$nombre[0]." ".$apellido[0]." se ha Registrado de manera 
                            Exitosa en el Sistema Integrado de Desarrollo de Las Trabajadoras 
                            y Los Trabajadores Académicos 
                            <b>(SIDTA)</b>. 
                            <br>Ahora Puedes Iniciar Sesión en SIDTA con tu Nombre de Usuario: <b>".$usuario."</b>
                            </p>
                            <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";

                            // Instantiation and passing `true` enables exceptions
                            $mail = new PHPMailer(true);

                            try {
                                //Server settings
                                $mail->SMTPDebug = 0;                      // Enable verbose debug output
                                $mail->isSMTP();                                            // Send using SMTP
                                $mail->Host       = constant('HOST_EMAIL');                    // Set the SMTP server to send through
                                $mail->SMTPAuth   = constant('SMTPAUTH');                                   // Enable SMTP authentication
                                $mail->Username   = constant('EMAIL');                     // SMTP username
                                $mail->Password   = constant('PASSWORD_EMAIL');                               // SMTP password
                                $mail->SMTPSecure = constant('SMTPSECURE');         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
                                $mail->Port       = constant('PORT_EMAIL');                                    // TCP port to connect to
                                $mail->CharSet=constant('CHARSET_EMAIL');   

                                //Recipients
                                $mail->setFrom(constant('EMAIL'), 'Sistema Integrado de Desarrollo de los y las Trabajadores Académicos');
                                $mail->addAddress($registro->correo_electronico, $nombre[0] ." ". $apellido[0]);     // Add a recipient
                                //$mail->addReplyTo('info@example.com', 'Information');
                                //$mail->addCC('cc@example.com');
                                //$mail->addBCC('bcc@example.com');

                                // Attachments
                                //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
                                //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

                                // Content
                                $mail->isHTML(true);                                  // Set email format to HTML
                                $mail->Subject = 'SIDTA - Registro Docente';
                                $mail->Body    = $mensaje2;
                                //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

                                if(!$mail->send()){
                                    $mensaje=$mensaje . "<div class='alert alert-warning alert-dismissable'>
                                    No se pudo enviar el mensaje por correo Electrónico. Verifique su conexion a Internet. 
                                    </div>";
                                    //var_dump($mensaje);
                                    //echo 'Detalles del error: '.$mail->ErrorInfo;
                                }

                            } catch (Exception $e) {
                                $mensaje=$mensaje . "<div class='alert alert-warning alert-dismissable'>
                                No se pudo enviar el mensaje por correo Electrónico. Verifique su conexion a Internet. 
                                </div>";
                                //var_dump($mensaje);
                            }

                        }

                        
                        
                }else{
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                        
                        <b>ERROR:</b> al Intentar registrar al usuario: ".$usuario." <br>
                        </div>";
                }
               
            }
            
            $this->view->mensaje=$mensaje;

            switch($_POST['admin-user']){
                case 0:
                    $this->render();
                break;
                
                case 1:
                    $this->render2();
                break;
            }
            

           
    }

    //funcion para separar cadenas de caracteres en 3 casos, 3 2 o 1 (se adapta a casos de nombres)
    function separar_cadena($cadena){
        $cuenta=str_word_count($cadena, 0, 'ÁáéÉíÍÓóÚúÑñç');

        //almacenamos un array indexado [1] etc
        $array_cadena = str_word_count($cadena, 1, 'ÁáéÉíÍÓóÚúÑñç');

        // separamos la cadena en cada uno de los casos si tiene 1 2 o 3 nombres para almacenarlo en la bbdd
        if($cuenta>=3){
            foreach ($array_cadena as $nombre) {
                $cadena1=$array_cadena[0];
                $cadena2=$array_cadena[1]. " ". $array_cadena[2];
            }
            $string_separada=array(
                $cadena1,
                $cadena2
            );
            return $string_separada;

        }elseif($cuenta==2){
            foreach ($array_cadena as $nombre) {
                $cadena1=$array_cadena[0];
                $cadena2=$array_cadena[1];
            }
            $string_separada=array(
                $cadena1,
                $cadena2
            );
            return $string_separada;

        }elseif($cuenta==1){
            foreach ($array_cadena as $nombre) {
                $cadena1=$array_cadena[0];
                $cadena2=NULL;
            }
            $string_separada=array(
                $cadena1,
                $cadena2
            );
            return $string_separada;
        }

    }


    function logout(){
        session_start();

        // Destruir todas las variables de sesión.
        $_SESSION = array(
                        $_SESSION["id_persona"],
                        $_SESSION['identificacion'],
                        $_SESSION['primer_nombre'], 
                        $_SESSION['primer_apellido'], 
                        $_SESSION['fecha_nacimiento'],
                        $_SESSION['password'],
                        $_SESSION['descripcion'],
                        $_SESSION['usuario'],
                        $_SESSION['id_perfil']
                        );
        //var_dump($_SESSION);
        // Si se desea destruir la sesión completamente, borre también la cookie de sesión.
        // Nota: ¡Esto destruirá la sesión, y no la información de la sesión!
        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        // Finalmente, destruir la sesión.
        session_destroy();
        session_unset();
        
        
        if(!headers_sent()) {
            echo '<script>
        alert("¡Hasta Pronto!");
        location.href="'.constant('URL').'main";
        </script>';
        //header("Location: ".constant('URL')."main");
        }
    }

    //<!-- 10 de septiembre 2020: agregando captcha -->

    function validacionCaptcha(){
        session_start();
        echo $_SESSION['captcha'];
    }

    // inicio combo dependiente ejes regional y municipal
    function getEjeMunipalbyER(){
        $id_eje_regional=$_POST['eje_regional'];
        $ejes_municipales = $this->model->getEjeMunipalbyER($id_eje_regional);
        echo '<option value="">Seleccione</option>';
        foreach($ejes_municipales as $row){
          $eje_municipal=new Estructura();
          $eje_municipal=$row;
          echo '<option value="'.$eje_municipal->id_eje_municipal.'">'.$eje_municipal->descripcion.'</option>';
        }
      }


}
?>
