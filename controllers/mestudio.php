    <?php
include_once 'sesiones/session_admin.php';

    class Mestudio extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->mensaje="";
            $this->view->mestudios=[];

        }

        function render(){

            $mestudios=$this->model->get();
            $this->view->mestudios=$mestudios;
            $this->view->render('adm_datos_academicos/mestudio');
        
        }

        function registrar(){

            if(isset($_POST['registrar'])){
                $descripcion=$_POST['descripcion'];
                
                
                if(empty($descripcion)){
                    $mensaje='<div class="alert alert-danger">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                El campo descripción es <a class="alert-link" href="#">Requerido</a>.
                            </div>';
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }
                
                if($var=$this->model->existe($descripcion)){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                     La modadlidad de estudio <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                    </div>";
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }

                if($this->model->insert([
                    'descripcion'=>$descripcion,
                    ])){
                        $mensaje='<div class="alert alert-success">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                La Modalidad de estudio '.$descripcion.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                            </div>';
                    }else{
                        $mensaje="<div class='alert alert-danger alert-dismissable'>
                            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                            Ha ocurrido un error al registrar la <a class='alert-link' href='#'>modalidad de estudio ".$descripcion."</a>
                        </div>";
                    }
                $this->view->mensaje=$mensaje;
                $this->render();
            }
            
        }

        function editar(){

            if(isset($_POST['registrar2'])){

                $descripcion2=$_POST['descripcion2'];
                $id_modalidad_estudio=$_POST['id_modalidad_estudio'];

                if(empty($descripcion2)){
                    $mensaje='<div class="alert alert-danger">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                El campo Modalidad de estudio es <a class="alert-link" href="#">Requerido</a>.
                            </div>';
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }

                if($this->model->update([
                    'descripcion'=>$descripcion2,
                    'id_modalidad_estudio'=>$id_modalidad_estudio
                    ])){    

              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              La Modalidad de Estudio <b> ".$descripcion2." </b> fue Actualizado <a class='alert-link' href='#'>Correctamente</a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar La Modalidad de Estudio <b> ".$descripcion2." </b><a class='alert-link' href='#'></a>
            </div>";
            }

            }

            $this->view->mensaje=$mensaje;
            $this->render();
        }

        function remover($param=null){
            
            
            $id_modalidad_estudio=$param[0];
            
            if($this->model->delete($id_modalidad_estudio)){
                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Modalidad de Estudio Eliminada<a class='alert-link' href='#'> Correctamente</a>
                </div>";
               
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al tratar de eliminar la <a class='alert-link' href='#'>Modalidad de Estudio</a>
                </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
           
        }
        

    }

    ?>