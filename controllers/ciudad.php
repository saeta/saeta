<?php
include_once 'sesiones/session_admin.php';

    class Ciudad extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->estados=[];
            $this->view->ciudades=[];
        }

        function render(){ 

            $ciudades=$this->model->get();
            $this->view->ciudades=$ciudades;

            $estados=$this->model->getEstado();
            $this->view->estados=$estados;

            
            $this->view->render('estructura/ciudad');        
        }



        function registrarCiudad(){
      
            //$id_persona=$_SESSION["id_persona"];
    
            $codigoine=$_POST['codigoine'];            
            $ciudad=$_POST['ciudad'];
            $id_estado=$_POST['estado'];

          
            
            $mensaje="";

            if($var=$this->model->existe($codigoine)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                 País <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($this->model->insert(['codigoine'=>$codigoine,'ciudad'=>$ciudad,'id_estado'=>$id_estado])){
              
    
   
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Estado agregado<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al agregar un Estado<a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
          }



          function ActualizarCiudad(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
    
            $id_ciudad=$_POST['id_ciudad'];
            $codigoine=$_POST['codigo'];
            $ciudad=$_POST['ciudadedit'];
            $id_estado=$_POST['estadoedit'];
            
            //var_dump( $id_estado,$codigoine, $estado,$id_pais2);
            $mensaje="";


           
    
            if($this->model->update(['id_ciudad'=>$id_ciudad,'codigoine'=>$codigoine,'ciudad'=>$ciudad,'id_estado'=>$id_estado])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Estado <b> ".$estado." </b> Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar Estado <b> ".$estado." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }

    }

?>