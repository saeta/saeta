<?php
include_once 'sesiones/session_admin.php';

    class Codarea extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->espacios=[]; 
            $this->view->estructuras=[]; 
            $this->view->ambientes=[];  
            $this->view->codigos=[];
            
        }

        function render(){ 
            //revisar estrutura
/*
            $espacios=$this->model->get();
            $this->view->espacios=$espacios;

            $estructuras=$this->model->getEstructuraFisica();
            $this->view->estructuras=$estructuras;


           $ambientes=$this->model->getAmbiente();
            $this->view->ambientes=$ambientes;*/

            $codigos=$this->model->get();
            $this->view->codigos=$codigos;

            $this->view->render('estructura/codarea');        
        }



        function registrarCodArea(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
    
            $codigo=$_POST['codigo'];            
                     
            
            $mensaje="";

            if($var=$this->model->existe($codigo)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Codigo Area Telefono <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($this->model->insert(['codigo'=>$codigo])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Codigo Area Telefono agregado<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al agregar un Codigo Area Telefono <a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
          }



          function ActualizarCodArea(){//se agrega esta linea
    
            //$id_persona=$_SESSION["id_persona"];

            $id_telefono_codigo_area=$_POST['id_telefono_codigo_area'];            
            $codigoarea=$_POST['codigoarea'];            
                     
           // var_dump($id_municipio);
            //var_dump( $id_estado,$codigoine, $estado,$id_pais2);
            $mensaje="";


            if($var=$this->model->existe($codigoarea)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Codigo Area Telefono <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
    
            if($this->model->update(['id_telefono_codigo_area'=>$id_telefono_codigo_area,'codigoarea'=>$codigoarea])){    
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Codigo Area Telefono <b> ".$espacio." </b> Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar Codigo Area Telefono <b> ".$espacio." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }




          function CodArea($param=null){
            $codigo=$param[0];
           if($this->model->delete($codigo)){
            
            $mensaje="<div class='alert alert-success alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
            Codigo Area Telefono Eliminado Correctamente<a class='alert-link' href='#'></a></div>";
 
           }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Ha ocurrido un error al Eliminar Codigo Area Telefono <a class='alert-link' href='#'></a>
        </div>";
             
           }
           $this->view->mensaje=$mensaje;
            $this->render();
            
        }
     




    }

?>