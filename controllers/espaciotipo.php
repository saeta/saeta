<?php
include_once 'sesiones/session_admin.php';

    class Espaciotipo extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->estructuras=[];            
            
            $this->view->espacios=[];
            
        }

        function render(){ 

/*
            $estructuras=$this->model->get();
            $this->view->estructuras=$estructuras;
*/
            $espacios=$this->model->get();
            $this->view->espacios=$espacios;

            $this->view->render('estructura/espaciotipo');        
        }



        function registrarEspacio(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
    
            $espacio=$_POST['espacio'];            
           
                    
            $mensaje="";

            if($var=$this->model->existe($espacio)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                 Extructura Física <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($this->model->insert(['espacio'=>$espacio])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
               Espacio registrado<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al registrar Espacio <a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
          }



          function ActualizarEspacio(){//se agrega esta linea
          
      
    
            //$id_persona=$_SESSION["id_persona"];
    

            $id_espacio_tipo=$_POST['id_espacio_tipo'];    
            $espacio=$_POST['espaciotipo'];            
                       
         
           // var_dump($id_municipio);
            //var_dump( $id_estado,$codigoine, $estado,$id_pais2);
            $mensaje="";


           
    
            if($this->model->update(['id_espacio_tipo'=>$id_espacio_tipo,'espacio'=>$espacio])){    
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Estructura Física <b> ".$estructura." </b> Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar Estructura Física <b> ".$estructura." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }

    }

?>