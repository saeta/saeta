<?php
include_once 'sesiones/session_admin.php';

    class comite_academico extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
                  
            $this->view->pfas=[];
            $this->view->docentes=[];
            $this->view->comites=[]; 
            $this->view->lista_comites=[]; 
            $this->view->integrantes=[]; 

            
        }

        function render(){ 


            $lista_comites=$this->model->get();
            $this->view->lista_comites=$lista_comites;

            $pfas=$this->model->getPFA();
            $this->view->pfas=$pfas;
          
            $docentes=$this->model->getDocente();
            $this->view->docentes=$docentes;

            $comites=$this->model->getComiteTipo();
            $this->view->comites=$comites;

            $this->view->render('adm_datos_academicos/comite_academico');        
        }

        function comite_academico_registros(){ 


            $lista_comites=$this->model->getHistory();
            $this->view->lista_comites=$lista_comites;
       
            $this->view->render('adm_datos_academicos/comite_academico_historico');        
        }


        function registrarComite(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
    
            $id_programa=$_POST['id_programa'];            
            $docentes=$_POST['docentes'];
            $id_comite_tipo=$_POST['id_comite_tipo'];
            //$estatus=$_POST['estatus'];

            //los comites se crean por defecto
            
            $estatus='1';
            $nombre_comite=$_POST['nombre_comite'];   

            
            $mensaje="";

            if($var=$this->model->existe($id_programa,$id_comite_tipo)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Comité Académico PFA <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($this->model->insert(['id_programa'=>$id_programa,'docentes'=>$docentes,'id_comite_tipo'=>$id_comite_tipo,'estatus'=>$estatus,'nombre_comite'=>$nombre_comite])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Comité Académico registrado<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al registrar Comité Académico <a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
          }

          function registrarDocente(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
    
            $id_programa=$_POST['id_programa'];            
            $docentes=$_POST['docentes'];
            $id_comite_tipo=$_POST['id_comite_tipo'];
            //$estatus=$_POST['estatus'];

            //los comites se crean por defecto
            
            $estatus='1';
            $nombre_comite=$_POST['nombre_comite'];   

            //var_dump($nombre_comite);
            $mensaje="";

            if($var=$this->model->existeDocente($docentes,$nombre_comite)){
             
                ?>
                <script>alert("Docente  <?php echo $var; ?> ya pertenece al comité <?php echo $nombre_comite; ?>");
                location.href='<?php echo constant ('URL').'comite_academico/verComite/'.$nombre_comite; ?>';
                </script>
                <?php
                
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
               Docente  <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
                </div>";           

               /* $this->view->mensaje=$mensaje;
                $this->render('');*/

                exit();
            }


            //var_dump('adm_datos_academicos/comite_academico/verComite/'.$nombre_comite.'');
            if($this->model->insert(['id_programa'=>$id_programa,'docentes'=>$docentes,'id_comite_tipo'=>$id_comite_tipo,'estatus'=>$estatus,'nombre_comite'=>$nombre_comite])){
              
    
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Docente agregado<a class='alert-link' href='#'></a></div>";

              ?>
              <script>alert("Registro actualizado");
              location.href='<?php echo constant ('URL').'comite_academico/verComite/'.$nombre_comite; ?>';
              </script>
              <?php
              
              
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al agregar docente <a class='alert-link' href='#'></a>
            </div>";

            ?>
            <script>alert("Ha ocurrido un error al Actualizar su registro");
            location.href='<?php echo constant ('URL').'comite_academico/verComite/'.$nombre_comite; ?>';
            </script>
            <?php

            }
            $this->view->mensaje=$mensaje;
            $this->render();
       
          }

          function verComite($param = null){
             $nombre_comite = $param[0];
             $integrantes = $this->model->getbyId($nombre_comite);
             $comites = $this->model->getComites($nombre_comite);
             $docentes =$this->model->getDocente();
             
             $this->view->integrantes = $integrantes;
             $this->view->comites = $comites;
             $this->view->docentes = $docentes;
             $this->view->mensaje ="";
             $this->view->render('adm_datos_academicos/comite_academico_detalle');
         }
 
         function verComiteHistoricos($param = null){
            $historico = $param[0];
            list($nombre_comite, $fecha_registro) = explode(",", $historico);

            $integrantes = $this->model->getbyIdHistoricos($nombre_comite,$fecha_registro);
            $comites = $this->model->getComitesHistory($nombre_comite,$fecha_registro);
            
            $this->view->integrantes = $integrantes;
            $this->view->comites = $comites;
            $this->view->mensaje ="";
            $this->view->render('adm_datos_academicos/comite_academico_historico_detalle');
        }


          function ActualizarComite(){//se agrega esta linea
          
            //$id_persona=$_SESSION["id_persona"];
            $id_para_comite_academico=$_POST['id_para_comite_academico'];
            $comite_academico=$_POST['comite_academico'];   
            $programa=$_POST['pfg'];            
            $comite_tipo=$_POST['comite_tipo'];
            
         
           // var_dump($id_municipio);
           
            $mensaje="";

          
           
    
            if($this->model->update(['id_para_comite_academico'=>$id_para_comite_academico,'comite_academico'=>$comite_academico,'programa'=>$programa,'comite_tipo'=>$comite_tipo])){
             
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Comité Académico PFA <b> ".$comite_academico." </b> Actualizada Correctamente<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar Comité Académico PFA <b> ".$comite_academico." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }


          function Aestatus($param=null){
            $comite=$param[0];
          
          //  var_dump( $id_comitepfa_integrante);
            list($id_comite,$nombre_comite) = explode(",", $comite);

               //var_dump($nombre_comite);
           if($this->model->updateÉstatus($id_comite)){
            
            $mensaje="<div class='alert alert-success alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
            Codigo Area Telefono Eliminado Correctamente<a class='alert-link' href='#'></a></div>";
            ?>

            <script>alert("Registro actualizado");
            location.href='<?php echo constant ('URL').'comite_academico/verComite/'.$nombre_comite; ?>';
            </script>
 
            <?php

           }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Ha ocurrido un error al Eliminar Codigo Area Telefono <a class='alert-link' href='#'></a>
           </div>";
           ?>

           <script>alert("Ha ocurrido un error al Actualizar su registro");
           location.href='<?php echo constant ('URL').'comite_academico/verComite/'.$nombre_comite;?>';
           </script>

           <?php

           }
           $this->view->mensaje=$mensaje;
           $this->view->render('adm_datos_academicos/comite_academico_detalle');
            
        }
     






    }

?>