<?php
include_once 'sesiones/session_admin.php';

class Perfil_docente extends Controller{
    
    function __construct(){
        parent::__construct();
        $this->view->mensaje="";
        

    }

    function render(){
        $this->view->render('perfil_docente/index');
    }

    function cargar() {
        
        if (isset($_POST['uploadBtn']) && $_POST['uploadBtn'] == 'Upload') {
            if (isset($_FILES['uploadedFile']) && $_FILES['uploadedFile']['error'] === UPLOAD_ERR_OK) {
                
                // get details of the uploaded file
                $archivo=$this->getData($_FILES['uploadedFile']);
                $delimiter=$_POST['delimiter'];
                // que implica colocar la ruta de esta forma, afecta al pasar a produccion ?
                $ruta=constant('RUTA_CSV').$archivo['nomb_archivo'];
                //var_dump($ruta);
                
                if($this->mover([
                    'extension'=>$archivo['extension'],
                    'ruta_destino'=>'src/csv/',
                    'ruta_tmp'=>$archivo['ruta_tmp'],
                    'nomb_archivo'=>$archivo['nomb_archivo']
                ])){
                    $mensaje2="<div class='alert alert-success alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
                    El csv  se cargo <a class='alert-link' href='#'>exitosamente.</a></div>";
                    
                    //var_dump($archivo['nomb_archivo']);
                    $ruta_eliminar='src/csv/' . $archivo['nomb_archivo'];
                    if($this->model->cargar([
                        'ruta'=>$ruta, 
                        'delimiter' => $delimiter])){
                            $mensaje="<div class='alert alert-success alert-dismissable'>
                            <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
                            CSV <b> ".$archivo['original_name']." </b> Cargado <a class='alert-link' href='#'>Exitosamente</a></div>";

                            
                            
                            unlink($ruta_eliminar);
                            
                            
                            

                    } else{
                            $mensaje="<div class='alert alert-danger alert-dismissable'>
                            <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
                            Ha ocurrido un error al intentar cargar el archivo <b> ".$archivo['original_name']." </b> <a class='alert-link' href='#'></a></div>";
                            unlink($ruta_eliminar);
                        }
                }else{
                    $mensaje2 = "<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
                    Se produjo un error al mover el archivo <b>".$archivo['original_name']."</b> para cargar el directorio. Asegúrese de que el servidor web pueda escribir en el directorio de carga.</div>";
                    $this->view->mensaje2=$mensaje2;
                    $this->render();
                    exit();
                }

                
                   

                //var_dump($archivo);
            }
        }
        $this->view->mensaje2=$mensaje2;

        $this->view->mensaje=$mensaje;
        $this->render();

    }

    //funcion para obtener los datos de las imagenes
    function getData($archivo){
        //var_dump($archivo);
        // Obtener información del archivo subido
        $ruta_tmp = $archivo['tmp_name'];
        $nomb_archivo = $archivo['name'];
        $tam_bytes = $archivo['size'];
        $tipo_archivo = $archivo['type'];
        //separa por el delimitador "."
        $fileNameCmps1 = explode(".", $nomb_archivo);
        /*
            la funcion end apunta al ultimo elemento de un arreglo
            la funcion strtolower convierte  mayusculas a minusculas
        */
        $extension_archivo = strtolower(end($fileNameCmps1));
        $nuevo_nomb = md5(time() . $nomb_archivo) . '.' . $extension_archivo;
        //var_dump($extension_archivo);
        $var=array(
                        'ruta_tmp'=>$archivo['tmp_name'],
                        'nomb_archivo'=>$nuevo_nomb,
                        'original_name'=>$archivo['name'],
                        'tam_bytes'=>$archivo['size'],
                        'tipo_archivo'=>$archivo['type'],
                        'extension'=>$extension_archivo
                    );

        return $var;
    }

    //funcion para mover los archivos
    function mover($datos){
        //var_dump($datos);
        
        $extensiones_permitidas = array('csv', 'gif', 'png', 'jpeg');
        if (in_array($datos['extension'], $extensiones_permitidas)) {
            $ruta_carga = $datos['ruta_destino'];
            //var_dump($ruta_carga);
            $destino = $ruta_carga . $datos['nomb_archivo'];
            /* 
                la funcion move_uploaded_file mueve el archivo cargado a una nueva ubicacion
            */
            if(move_uploaded_file($datos['ruta_tmp'], $destino))
            {
                return true;
            } else{
                //echo "primer if";

                return false;
            }
            

        }else{
            //echo "segundo if";
            return false;
        }

    }


 
    





}
?>