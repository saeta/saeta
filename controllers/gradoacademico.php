    <?php
include_once 'models/datosacademicos/nivel_academico.php';
include_once 'sesiones/session_admin.php';

    class Gradoacademico extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->mensaje="";
            $this->view->niveles=[];
            $this->view->grados=[];

        }

        function render(){

            $niveles=$this->model->getNivel();
            $this->view->niveles=$niveles;
            $grados=$this->model->getGrado();
            $this->view->grados=$grados;
            $this->view->render('adm_datos_academicos/gradoacademico');
        
        }

        function registrarGrado(){

            if(isset($_POST['registrar'])){
                $descripcion=$_POST['descripcion'];
                $id_nivel_academico=$_POST['id_nivel_academico'];
                $codigo_ine=$_POST['codigo_ine'];

                if(empty($descripcion)){
                    $mensaje='<div class="alert alert-danger">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                El campo grado académico es . <a class="alert-link" href="#">Requerido</a>.
                            </div>';
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }elseif(empty($id_nivel_academico)){
                    $mensaje='<div class="alert alert-danger">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    El campo Nivel académico es <a class="alert-link" href="#">Requerido</a>.
                    </div>';
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }
                
                if($var=$this->model->existe($descripcion)){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                     El grado <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                    </div>";
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }

                if($this->model->insert([
                    'descripcion'=>$descripcion,
                    'id_nivel_academico'=>$id_nivel_academico,
                    'codigo_ine'=>$codigo_ine
                    ])){
                        $mensaje='<div class="alert alert-success">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                Grado Académico  '.$descripcion.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                            </div>';
                    }else{
                        $mensaje="<div class='alert alert-danger alert-dismissable'>
                            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                            Ha ocurrido un error al registrar Páis<a class='alert-link' href='#'></a>
                        </div>";
                    }

                $this->view->mensaje=$mensaje;
                $this->render();
            }
            
        }

        function editarGrado(){

            if(isset($_POST['registrar2'])){
                $id_grado_academico=$_POST['id_grado_academico'];

                $descripcion2=$_POST['descripcion2'];
                $id_nivel_academico2=$_POST['id_nivel_academico2'];
                $codigo_ine2=$_POST['codigo_ine2'];

                if(empty($descripcion2)){
                    $mensaje='<div class="alert alert-danger">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                El campo grado académico es . <a class="alert-link" href="#">Requerido</a>.
                            </div>';
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }elseif(empty($id_nivel_academico2)){
                    $mensaje='<div class="alert alert-danger">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    El campo Nivel académico es <a class="alert-link" href="#">Requerido</a>.
                    </div>';
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }

                if($this->model->update([
                    'id_grado_academico'=>$id_grado_academico,
                    'descripcion'=>$descripcion2,
                    'id_nivel_academico'=>$id_nivel_academico2,
                    'codigo_ine'=>$codigo_ine2
                    ])){    

              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              El grado <b> ".$descripcion2." </b> fue Actualizado <a class='alert-link' href='#'>Correctamente</a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar el grado <b> ".$descripcion2." </b><a class='alert-link' href='#'></a>
            </div>";
            }

            }

            $this->view->mensaje=$mensaje;
            $this->render();
        }
        
        function removerGrado($param=null){
            
            
            $id_grado_academico=$param[0];
            
            if($this->model->delete($id_grado_academico)){
                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Grado Eliminado<a class='alert-link' href='#'> Correctamente</a>
                </div>";
               
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al tratar de eliminar el <a class='alert-link' href='#'>Grado</a>
                </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
           
        }
        

    }

    ?>