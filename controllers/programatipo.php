<?php
include_once 'models/datosacademicos/programanivel.php';
include_once 'sesiones/session_admin.php';

class Programatipo extends Controller{
    function __construct(){
        parent::__construct();
        $this->view->mensaje="";
        $this->view->niveles=[];
        $this->view->tipos=[];
       
    }

    function render(){
        $niveles=$this->model->getNivel();
        $this->view->niveles=$niveles;
        $tipos=$this->model->getTipo();
        $this->view->tipos=$tipos;
       

        $this->view->render('adm_datos_academicos/programa_tipo');
    }

    function registrarTipo(){

        if(isset($_POST['registrar'])){
            $descripcion=$_POST['descripcion'];
            $codigo=$_POST['codigo'];
            $id_programa_nivel=$_POST['id_programa_nivel'];
            

            if(empty($descripcion)){
                $mensaje='<div class="alert alert-danger">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            El campo Programa Tipo es . <a class="alert-link" href="#">Requerido</a>.
                        </div>';
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }elseif(empty($id_programa_nivel)){
                $mensaje='<div class="alert alert-danger">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                El campo Programa Nivel es <a class="alert-link" href="#">Requerido</a>.
                </div>';
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($var=$this->model->existe($descripcion)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                 El Tipo de programa <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }

            if($this->model->insert([
                'descripcion'=>$descripcion,
                'codigo'=>$codigo,
                'id_programa_nivel'=>$id_programa_nivel
                
                ])){
                    $mensaje='<div class="alert alert-success">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            Tipo Académico  '.$descripcion.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                        </div>';
                }else{
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                        Ha ocurrido un error al registrar Programa Tipo<a class='alert-link' href='#'></a>
                    </div>";
                }

            $this->view->mensaje=$mensaje;
            $this->render();
        }
        
    }

    function editarTipo(){

        if(isset($_POST['registrar2'])){
            $id_programa_tipo=$_POST['id_programa_tipo'];
            $descripcion2=$_POST['descripcion2'];
            $codigo2=$_POST['codigo2'];
            $id_programa_nivel2=$_POST['id_programa_nivel2'];
            

            if(empty($descripcion2)){
                $mensaje='<div class="alert alert-danger">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            El campo Tipo académico es . <a class="alert-link" href="#">Requerido</a>.
                        </div>';
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }elseif(empty($id_programa_nivel2)){
                $mensaje='<div class="alert alert-danger">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                El campo Centro Estudio es <a class="alert-link" href="#">Requerido</a>.
                </div>';
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }

            if($this->model->update([
                'id_programa_tipo'=>$id_programa_tipo,
                'descripcion'=>$descripcion2,
                'codigo'=>$codigo2,
                'id_programa_nivel'=>$id_programa_nivel2
                ])){    

          $mensaje="<div class='alert alert-success alert-dismissable'>
          <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
          El Tipo <b> ".$descripcion2." </b> fue Actualizado <a class='alert-link' href='#'>Correctamente</a></div>";
        
      }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Ha ocurrido un error al Actualizar el Tipo <b> ".$descripcion2." </b><a class='alert-link' href='#'></a>
        </div>";
        }

        }

        $this->view->mensaje=$mensaje;
        $this->render();
    }


      function removerTipo($param=null){
        
        $id_programa_tipo=$param[0];
  
        if($this->model->delete($id_programa_tipo)){

            $mensaje="<div class='alert alert-success alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
           Removido correctamente el  Tipo de Programa<b>  </b><a class='alert-link' href='#'></a>
        </div>";
       }else{
        $mensaje="<div class='alert alert-danger alert-dismissable'>
        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
       No se pudo remover el  Tipo de Programa<b> </b><a class='alert-link' href='#'></a>
        </div>";
 
       }
       $this->view->mensaje=$mensaje;
        $this->render();
    }

}
?>