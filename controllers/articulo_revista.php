<?php
include_once 'models/datosacademicos/docente.php';
include_once 'sesiones/session_admin.php';

    class Articulo_revista extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->mensaje="";
            $this->view->docente=[];
            $this->view->revista=[];

        }

        function render(){

            $docente=$this->model->getDocente();
            //var_dump($docente);
            $this->view->docente=$docente;
            
            $revista=$this->model->getRevista();
            //var_dump($arte);
            $this->view->revista=$revista;
            $this->view->render('produccion_intelectual/articulo_revista');
        
        }



        function view_registro(){

            $docente=$this->model->getDocente();
            //var_dump($docente);
            $this->view->docente=$docente;
            
            $this->view->revista=$revista;
            $this->view->render('produccion_intelectual/agregar_revista');
        
        }

        function viewEditar($param = null){

            $id_articulo_revista=$param[0];
            //var_dump($param[0]);
            $docente=$this->model->getDocente($id_articulo_revista);
            //var_dump($docente);
            $_SESSION['id_articulo_revista']= $id_articulo_revista;
            //var_dump($_SESSION['id_arte_software']);
            $this->view->docente=$docente;
            $articulo_revista=$this->model->getbyId($id_articulo_revista);
            //var_dump($arte_software);


            $this->view->revista=$articulo_revista;
            $this->view->render('produccion_intelectual/editar_revista');
        
        }

        function viewVer($param = null){

            $id_articulo_revista=$param[0];
            //var_dump($param[0]);
            $docente=$this->model->getDocente($id_articulo_revista);
            //var_dump($docente);
            $_SESSION['id_articulo_revista']= $id_articulo_revista;
            //var_dump($_SESSION['id_arte_software']);
            $this->view->docente=$docente;
            $articulo_revista=$this->model->getbyId($id_articulo_revista);
            //var_dump($arte_software);


            $this->view->revista=$articulo_revista;
            $this->view->render('produccion_intelectual/ver_revista');
        
        }


    

        function registrarRevista(){

            if(isset($_POST['registrar'])){
                $nombre_revista=$_POST['nombre_revista'];
                $titulo_articulo=$_POST['titulo_articulo'];
                $issn_revista=$_POST['issn_revista'];
                $ano_revista=$_POST['ano_revista'];
                $nro_revista=$_POST['nro_revista'];
                $nro_pag_inicial=$_POST['nro_pag_inicial'];
                $url_revista=$_POST['url_revista'];
                $id_docente=$_SESSION['id_docente'];

                //var_dump($mmmmmmm);


                
                if($var=$this->model->existe($nombre_revista)){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                     El articulo de revista <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                    </div>";
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }


                if($this->model->insert([
                    'nombre_revista'=>$nombre_revista,
                    'titulo_articulo'=>$titulo_articulo,
                    'issn_revista'=>$issn_revista,
                    'ano_revista'=>$ano_revista,
                    'nro_revista'=>$nro_revista,
                    'nro_pag_inicial'=>$nro_pag_inicial,
                    'url_revista'=>$url_revista,
                    'id_docente'=>$id_docente
                    ])){
                        $mensaje='<div class="alert alert-success">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        articulo de revista  '.$nombre_revista.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                            </div>';
                    }else{
                        $mensaje="<div class='alert alert-danger alert-dismissable'>
                            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                            Ha ocurrido un error al registrar  el articulo de revista<a class='alert-link' href='#'></a>
                        </div>";
                    }

                $this->view->mensaje=$mensaje;
                $this->render();
            }
            
        }




        function editarRevista(){

            
                $id_articulo_revista=$_POST['id_articulo_revista'];
                
                $nombre_revista2=$_POST['nombre_revista2'];
                $titulo_articulo2=$_POST['titulo_articulo2'];
                $issn_revista2=$_POST['issn_revista2'];
                $ano_revista2=$_POST['ano_revista2'];
                $nro_revista2=$_POST['nro_revista2'];
                $nro_pag_inicial2=$_POST['nro_pag_inicial2'];
                $url_revista2=$_POST['url_revista2'];
               // $id_docente2=$_SESSION['id_docente'];


                // var_dump($id_arte_software, $nombre_arte2, $ano_arte2, $entidad_promotora2, $url_otro2 );
               



                if($this->model->update([
                    'id_articulo_revista'=>$id_articulo_revista,

                    'nombre_revista2'=>$nombre_revista2,
                    'titulo_articulo2'=>$titulo_articulo2,
                    'issn_revista2'=>$issn_revista2,
                    'ano_revista2'=>$ano_revista2,
                    'nro_revista2'=>$nro_revista2,
                    'nro_pag_inicial2'=>$nro_pag_inicial2,
                    'url_revista2'=>$url_revista2
                  
                    ])){    


              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              El articulo de revista <b> ".$nombre_revista2." </b> fue Actualizado <a class='alert-link' href='#'>Correctamente</a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar el articulo de revista <b> ".$nombre_revista2." </b><a class='alert-link' href='#'></a>
            </div>";
            }

        

            $this->view->mensaje=$mensaje;
            $this->render();
           // $this->viewEditar();
        }


        function verRevista(){

            
            $this->view->render('produccion_intelectual/ver_revista');
        
        }





        
        function removerRevista($param=null){
            
            
            $id_articulo_revista=$param[0];
            
            if($this->model->delete($id_articulo_revista)){
                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                articulo de revista<a class='alert-link' href='#'> Correctamente</a>
                </div>";
               
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al tratar de eliminar el <a class='alert-link' href='#'>el articulo de revista</a>
                </div>";
            }
            echo $mensaje;
           
        }
        


        



    }

    ?>