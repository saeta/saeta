<?php
include_once 'models/datosacademicos/docente.php';
include_once 'sesiones/session_admin.php';

    class Registro_ponencia extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->mensaje="";
            $this->view->docente=[];
            $this->view->ponencia=[];

        }

        function render(){

            $docente=$this->model->getDocente();
            //var_dump($docente);
            $this->view->docente=$docente;
            
            $ponencia=$this->model->getPonencia();
            //var_dump($arte);
            $this->view->ponencia=$ponencia;
            $this->view->render('produccion_intelectual/registro_ponencia');
        
        }



        function view_registro(){

            $docente=$this->model->getDocente();
            //var_dump($docente);
            $this->view->docente=$docente;
            
            $this->view->ponencia=$ponencia;
            $this->view->render('produccion_intelectual/agregar_ponencia');
        
        }

        function viewEditar($param = null){

            $id_ponencia=$param[0];
            //var_dump($param[0]);
            $docente=$this->model->getDocente($id_libro);
            //var_dump($docente);
            $_SESSION['id_ponencia']= $id_ponencia;
            //var_dump($_SESSION['id_arte_software']);
            $this->view->docente=$docente;
            $registro_ponencia=$this->model->getbyId($id_ponencia);
            //var_dump($arte_software);


            $this->view->ponencia=$registro_ponencia;
            $this->view->render('produccion_intelectual/editar_ponencia');
        
        }

        function viewVer($param = null){

            $id_ponencia=$param[0];
            //var_dump($param[0]);
            $docente=$this->model->getDocente($id_ponencia);
            //var_dump($docente);
            $_SESSION['id_ponencia']= $id_ponencia;
            //var_dump($_SESSION['id_arte_software']);
            $this->view->docente=$docente;
            $registro_ponencia=$this->model->getbyId($id_ponencia);
            //var_dump($id_libro);


            $this->view->ponencia=$registro_ponencia;
            $this->view->render('produccion_intelectual/ver_ponencia');
        
        }


    

        function registrarPonencia(){

            if(isset($_POST['registrar'])){
                $nombre_ponencia=$_POST['nombre_ponencia'];
                $nombre_evento=$_POST['nombre_evento'];
                $ano_ponencia=$_POST['ano_ponencia'];
                $ciudad=$_POST['ciudad'];
                $titulo_memoria=$_POST['titulo_memoria'];
                $volumen=$_POST['volumen'];
                $identificador=$_POST['identificador'];
                $pag_inicial=$_POST['pag_inicial'];
                $formato=$_POST['formato'];
                $url_ponencia=$_POST['url_ponencia'];
                $id_docente=$_SESSION['id_docente'];

                //var_dump($mmmmmmm);


                
                if($var=$this->model->existe($nombre_ponencia)){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                     La ponencia <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                    </div>";
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }


                if($this->model->insert([
                    'nombre_ponencia'=>$nombre_ponencia,
                    'nombre_evento'=>$nombre_evento,
                    'ano_ponencia'=>$ano_ponencia,
                    'ciudad'=>$ciudad,
                    'titulo_memoria'=>$titulo_memoria,
                    'volumen'=>$volumen,
                    'identificador'=>$identificador,
                    'pag_inicial'=>$pag_inicial,
                    'formato'=>$formato,
                    'url_ponencia'=>$url_ponencia,
                    'id_docente'=>$id_docente
                    ])){
                        $mensaje='<div class="alert alert-success">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        Ponencia  '.$nombre_ponencia.' ha sido registrada <a class="alert-link" href="#">Exitosamente</a>.
                            </div>';
                    }else{
                        $mensaje="<div class='alert alert-danger alert-dismissable'>
                            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                            Ha ocurrido un error al registrar  la ponencia<a class='alert-link' href='#'></a>
                        </div>";
                    }

                $this->view->mensaje=$mensaje;
                $this->render();
            }
            
        }




        function editarPonencia(){

            
                $id_ponencia=$_POST['id_ponencia'];
                $nombre_ponencia2=$_POST['nombre_ponencia2'];
                $nombre_evento2=$_POST['nombre_evento2'];
                $ano_ponencia2=$_POST['ano_ponencia2'];
                $ciudad2=$_POST['ciudad2'];
                $titulo_memoria2=$_POST['titulo_memoria2'];
                $volumen2=$_POST['volumen2'];
                $identificador2=$_POST['identificador2'];
                $pag_inicial2=$_POST['pag_inicial2'];
                $formato2=$_POST['formato2'];
                
                $url_ponencia2=$_POST['url_ponencia2'];
               // $id_docente2=$_SESSION['id_docente'];
                // var_dump($id_arte_software, $nombre_arte2, $ano_arte2, $entidad_promotora2, $url_otro2 );
               



                if($this->model->update([
                    'id_ponencia'=>$id_ponencia,

                    'nombre_ponencia2'=>$nombre_ponencia2,
                    'nombre_evento2'=>$nombre_evento2,
                    'ano_ponencia2'=>$ano_ponencia2,
                    'ciudad2'=>$ciudad2,
                    'titulo_memoria2'=>$titulo_memoria2,
                    'volumen2'=>$volumen2,
                    'identificador2'=>$identificador2,
                    'pag_inicial2'=>$pag_inicial2,
                    'formato2'=>$formato2,
                    'url_ponencia2'=>$url_ponencia2
                  
                    ])){    


              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              La ponencia <b> ".$nombre_ponencia2." </b> fue Actualizado <a class='alert-link' href='#'>Correctamente</a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar La ponencia <b> ".$nombre_ponencia2." </b><a class='alert-link' href='#'></a>
            </div>";
            }

        

            $this->view->mensaje=$mensaje;
            $this->render();
           // $this->viewEditar();
        }


        function verPonencia(){

            
            $this->view->render('produccion_intelectual/ver_ponencia');
        
        }





        
        function removerPonencia($param=null){
            
            
            $id_ponencia=$param[0];
            
            if($this->model->delete($id_ponencia)){
                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ponencia><a class='alert-link' href='#'> Correctamente</a>
                </div>";
               
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al tratar de eliminar el <a class='alert-link' href='#'>la Ponencia</a>
                </div>";
            }
            echo $mensaje;
           
        }
        


        



    }

    ?>