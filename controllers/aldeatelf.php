<?php
include_once 'sesiones/session_admin.php';

    class Aldeatelf extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->aldeatelefonos=[]; 
            $this->view->aldeas=[]; 
            $this->view->codigos=[]; 
          
            
        }

        function render(){ 


            $aldeatelefonos=$this->model->get();
            $this->view->aldeatelefonos=$aldeatelefonos;

            $codigos=$this->model->getCodArea();
            $this->view->codigos=$codigos;

            $aldeas=$this->model->getAldea();
            $this->view->aldeas=$aldeas;


            $this->view->render('estructura/aldeatelf');        
        }



        function registrarAldeaTelf(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
    
            $codigoarea=$_POST['codigoarea']; 
            $numero=$_POST['numero'];             
            $aldea=$_POST['aldea']; 
            
            
            $mensaje="";

           if($var=$this->model->existe($numero)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Telefono <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($this->model->insert(['codigoarea'=>$codigoarea,'numero'=>$numero,'aldea'=>$aldea])){
              
        
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Aldea Telefono agregado<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al agregar una Telefono <a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
          }



          function ActualizarAldeaTelf(){//se agrega esta linea
    
            //$id_persona=$_SESSION["id_persona"];

            $id_aldea_telefono=$_POST['id_aldea_telefono'];            
            $codigoareaa=$_POST['codigoareaa']; 
            $numeroo=$_POST['numeroo'];             
            $aldeaa=$_POST['aldeaa']; 
                      
                     
           // var_dump($id_municipio);
            //var_dump( $id_estado,$codigoine, $estado,$id_pais2);
            $mensaje="";


            if($var=$this->model->existe($codigoarea)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Codigo Area Telefono <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
    
            if($this->model->update(['id_aldea_telefono'=>$id_aldea_telefono,'codigoareaa'=>$codigoareaa,'numeroo'=>$numeroo,'aldeaa'=>$aldeaa])){    
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Codigo Area Telefono <b> ".$espacio." </b> Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar Codigo Area Telefono <b> ".$espacio." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }

    }

?>