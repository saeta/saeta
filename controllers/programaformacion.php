<?php
include_once 'models/datosacademicos/centro_estudio.php';
include_once 'models/datosacademicos/programa_tipo.php';
include_once 'models/datosacademicos/nivel_academico.php';
include_once 'models/datosacademicos/lineainvestigacion.php';
include_once 'sesiones/session_admin.php';

class Programaformacion extends Controller{
    function __construct(){
        parent::__construct();
        $this->view->mensaje="";
        $this->view->centros=[];
        $this->view->tipos=[];
        $this->view->niveles=[];
        $this->view->lineas=[];
        $this->view->programas=[];

    }

    function render(){
        $centros=$this->model->getCentro();
        $this->view->centros=$centros;
        $tipos=$this->model->getTipo();
        $this->view->tipos=$tipos;
        $niveles=$this->model->getNivel();
        $this->view->niveles=$niveles;
        $lineas=$this->model->getLinea();
        $this->view->lineas=$lineas;
        $programas=$this->model->getPrograma();
        $this->view->programas=$programas;
        $this->view->render('adm_datos_academicos/programa_formacion');
    }

    function registrarPrograma(){

        if(isset($_POST['registrar'])){
            $descripcion=$_POST['descripcion'];
            $codigo=$_POST['codigo'];
            $id_centro_estudio=$_POST['id_centro_estudio'];
            $id_programa_tipo=$_POST['id_programa_tipo'];
            $id_nivel_academico=$_POST['id_nivel_academico'];
            $id_linea_investigacion=$_POST['id_linea_investigacion'];
            if($_POST['estatus'] == 'Activo'){
                $estatus=1;
            }elseif($_POST['estatus'] == 'Inactivo'){
                $estatus=0;
            }

            if(empty($descripcion)){
                $mensaje='<div class="alert alert-danger">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            El campo Programa de Formacion es . <a class="alert-link" href="#">Requerido</a>.
                        </div>';
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($var=$this->model->existe($descripcion)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Programa de Formacion <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }

            if($this->model->insert([
                'descripcion'=>$descripcion,
                'codigo'=>$codigo,
                'id_centro_estudio'=>$id_centro_estudio,
                'id_programa_tipo'=>$id_programa_tipo,
                'id_nivel_academico'=>$id_nivel_academico,
                'id_linea_investigacion'=>$id_linea_investigacion,
                'estatus'=>$estatus

                ])){
                    $mensaje='<div class="alert alert-success">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Programa de Formacion  '.$descripcion.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                        </div>';
                }else{
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                        Ha ocurrido un error al registrar el Programa de Formacion<a class='alert-link' href='#'></a>
                    </div>";
                }

            $this->view->mensaje=$mensaje;
            $this->render();
        }

    }

    function editarPrograma(){

        if(isset($_POST['registrar2'])){

            $id_programa=$_POST['id_programa'];
            $descripcion2=$_POST['descripcion2'];
            $codigo2=$_POST['codigo2'];
            $id_centro_estudio2=$_POST['id_centro_estudio2'];
            $id_programa_tipo2=$_POST['id_programa_tipo2'];
            $id_nivel_academico2=$_POST['id_nivel_academico2'];
            $id_linea_investigacion2=$_POST['id_linea_investigacion2'];
            //$estatus2=$_POST['estatus2'];
            if($_POST['estatus2'] == 'Activo'){
                $estatus2=1;
            }elseif($_POST['estatus2'] == 'Inactivo'){
                $estatus2=0;
            }

            if(empty($descripcion2)){
                $mensaje='<div class="alert alert-danger">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            El campo Programa de Formacion es . <a class="alert-link" href="#">Requerido</a>.
                        </div>';
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }

            if($this->model->update([
                'id_programa'=>$id_programa,
                'descripcion'=>$descripcion2,
                'codigo'=>$codigo2,
                'id_centro_estudio'=>$id_centro_estudio2,
                'id_programa_tipo'=>$id_programa_tipo2,
                'id_nivel_academico'=>$id_nivel_academico2,
                'id_linea_investigacion'=>$id_linea_investigacion2,
                'estatus'=>$estatus2

                ])){    

          $mensaje="<div class='alert alert-success alert-dismissable'>
          <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
          El Programa de Formacion <b> ".$descripcion2." </b> fue Actualizado <a class='alert-link' href='#'>Correctamente</a></div>";
        
      }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Ha ocurrido un error al Actualizar El Programa de Formacion <b> ".$descripcion2." </b><a class='alert-link' href='#'></a>
        </div>";
        }

        }

        $this->view->mensaje=$mensaje;
        $this->render();
    }


      function removerPrograma($param=null){

        $id_programa=$param[0];

        if($this->model->delete($id_programa)){

            $mensaje="<div class='alert alert-success alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
           Removido correctamente el Programa de Formacion<b>  </b><a class='alert-link' href='#'></a>
        </div>";

       }else{
        $mensaje="<div class='alert alert-danger alert-dismissable'>
        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
       No se pudo remover el Programa de Formacion<b> </b><a class='alert-link' href='#'></a>
        </div>";

       }
       echo $mensaje;
    }

    function cambiarEstatus($param=null){
        $id_programa=$param[0];

        if($this->model->delete($id_programa)){
            $mensaje="<div class='alert alert-success alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Programa Eliminado<a class='alert-link' href='#'> Correctamente</a>
            </div>";

        }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Ha ocurrido un error al tratar de eliminar el <a class='alert-link' href='#'>Programa</a>
            </div>";
        }
        echo $mensaje;

    }

}
?>