<?php
include_once 'sesiones/session_admin.php';

    class Municipio extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->municipios=[];
            $this->view->estados=[];
        }

        function render(){ 
            $estados=$this->model->getEstado();
            $this->view->estados=$estados;

            $municipios=$this->model->get();
            $this->view->municipios=$municipios;
            $this->view->render('estructura/municipio');        
        }



        function registrarMunicipio(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
    
            $codigoine=$_POST['codigoine'];
            $municipio=$_POST['municipio'];
            $estado=$_POST['estado'];
            //$estatus=$_POST['estatus'];
            
            $mensaje="";


            if($var=$this->model->existe($codigoine)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                 País <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
    
    
            if($this->model->insert(['codigoine'=>$codigoine,'municipio'=>$municipio,'estado'=>$estado])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Municipio registrado<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al registar municipio<a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }

          function ActualizarMunicipio(){//se agrega esta linea

             
      
            //$id_persona=$_SESSION["id_persona"];
    
            $id_municipio=$_POST['id_municipio'];
            $codigoine=$_POST['codigoine2'];
            $municipio=$_POST['municipio2'];
            $estado=$_POST['estado2'];
         
            
            //var_dump( $codigoine,$siglasine);
            $mensaje="";


           
    
            if($this->model->update(['id_municipio'=>$id_municipio,'codigoine'=>$codigoine,'municipio'=>$municipio,'estado'=>$estado])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Estado <b> ".$estado." </b>Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar Estado <b> ".$estado." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }


    }

?>