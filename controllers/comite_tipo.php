    <?php
include_once 'sesiones/session_admin.php';

    class Comite_tipo extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->mensaje="";
            $this->view->tipos=[];

        }

        function render(){

            $tipos=$this->model->get();
            $this->view->tipos=$tipos;
            $this->view->render('adm_datos_academicos/comite_tipo');
        
        }

        function registrar(){

            if(isset($_POST['registrar'])){

                $descripcion=$_POST['descripcion'];
                
                if(strcasecmp("Activo", $_POST['estatus']) == 0){
                    $estatus=1;
                }elseif(strcasecmp("Inactivo", $_POST['estatus']) == 0){
                    $estatus=0;

                }

                if(empty($descripcion)){
                    $mensaje='<div class="alert alert-danger">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                El campo descripción es . <a class="alert-link" href="#">Requerido</a>.
                            </div>';
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }
                
                if($var=$this->model->existe($descripcion)){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                     El Tipo de Comité <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                    </div>";
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }

                if($this->model->insert([
                    'descripcion'=>$descripcion,
                    'estatus'=>$estatus
                    ])){
                        $mensaje='<div class="alert alert-success">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                El Tipo de Comité  '.$descripcion.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                            </div>';
                    }else{
                        $mensaje="<div class='alert alert-danger alert-dismissable'>
                            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                            Ha ocurrido un error al registrar la<a class='alert-link' href='#'> Tipo Comité</a>
                        </div>";
                    }

                $this->view->mensaje=$mensaje;
                $this->render();
            }
            
        }

        function editar(){

            if(isset($_POST['registrar2'])){

                $descripcion2=$_POST['descripcion2'];

                if(strcasecmp("Activo", $_POST['estatus1']) == 0){
                    $estatus1=1;
                }elseif(strcasecmp("Inactivo", $_POST['estatus1']) == 0){
                    $estatus1=0;
                }
                $id_comite_tipo=$_POST['id_comite_tipo'];

                if(empty($descripcion2)){
                    $mensaje='<div class="alert alert-danger">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                El campo Tipo Comité es . <a class="alert-link" href="#">Requerido</a>.
                            </div>';
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }elseif(empty($id_comite_tipo)){
                    $mensaje='<div class="alert alert-danger">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    El campo Nivel académico es <a class="alert-link" href="#">Requerido</a>.
                    </div>';
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }

                if($this->model->update([
                    'descripcion'=>$descripcion2,
                    'id_comite_tipo'=>$id_comite_tipo,
                    'estatus'=>$estatus1
                    ])){    

              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              El grado <b> ".$descripcion2." </b> fue Actualizado <a class='alert-link' href='#'>Correctamente</a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar el grado <b> ".$descripcion2." </b><a class='alert-link' href='#'></a>
            </div>";
            }

            }

            $this->view->mensaje=$mensaje;
            $this->render();
        }

        function remover($param=null){
            
            
            $id_comite_tipo=$param[0];
            
            if($this->model->delete($id_comite_tipo)){
                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Tipo Comité Eliminado<a class='alert-link' href='#'> Correctamente</a>
                </div>";
               
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al tratar de eliminar el <a class='alert-link' href='#'>Tipo Comité</a>
                </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
           
        }
        

    }

    ?>