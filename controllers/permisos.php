<?php
include_once 'sesiones/session_admin.php';

    class Permisos extends Controller {
        function __construct(){
            parent::__construct();
            $this->view->permisos=[];  
            $this->view->roles=[];
            
        }

        function render(){ 
            //revisar estrutura

           /* $espacios=$this->model->get();
            $this->view->espacios=$espacios;

            $estructuras=$this->model->getEstructuraFisica();
            $this->view->estructuras=$estructuras;

            
//getOfertaAcademica
            */
            $permisos=$this->model->get();
            $this->view->permisos=$permisos;

            $roles=$this->model->getRoles();
            $this->view->roles=$roles;

            $this->view->render('gestion_usuarios/permisos');        
        }



        function RegistrarPermiso(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
    
            $permiso=$_POST['permiso'];            
            $roles=$_POST['roles'];
  
            $mensaje="";

            if($var=$this->model->existe($permiso)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Permiso <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($this->model->insert(['permiso'=>$permiso,'roles'=>$roles])){
              
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Permiso agregado<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al agregar un Permiso<a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
          }



          function ActualizarPermiso(){//se agrega esta linea
    
            //$id_persona=$_SESSION["id_persona"];

            $id_perfil=$_POST['id_perfil'];
            $permiso=$_POST['permisos'];            
            $id_rol=$_POST['id_rol'];
     
           // var_dump($id_municipio);
            //var_dump( $id_estado,$codigoine, $estado,$id_pais2);
            $mensaje="";
    

            if($var=$this->model->existeROL($id_rol,$permiso)){
                $mensaje="<div class='alert alert-warning alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Este Rol ya se encuantra asignado al Usuario <b>" . $var . "</b> <a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }



            if($this->model->update(['id_perfil'=>$id_perfil,'permiso'=>$permiso,'id_rol'=>$id_rol])){    
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Permiso <b> ".$permiso." </b> Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar Permiso <b> ".$permiso." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }

    }

?>