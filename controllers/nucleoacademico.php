<?php
include_once 'models/datosacademicos/centro_estudio.php';
include_once 'sesiones/session_admin.php';

class Nucleoacademico extends Controller{
    function __construct(){
        parent::__construct();
        $this->view->mensaje="";
        $this->view->centros=[];
        $this->view->nucleos=[];
       
    }

    function render(){
        $centros=$this->model->getCentro();
        $this->view->centros=$centros;
        $nucleos=$this->model->getNucleo();
        $this->view->nucleos=$nucleos;
        $this->view->render('adm_datos_academicos/nucleo_acad');
    }

    function registrarNucleo(){

        if(isset($_POST['registrar'])){
            $descripcion=$_POST['descripcion'];
            $id_centro_estudio=$_POST['id_centro_estudio'];
            

            if(empty($descripcion)){
                $mensaje='<div class="alert alert-danger">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            El campo Nucleo Academico es . <a class="alert-link" href="#">Requerido</a>.
                        </div>';
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }elseif(empty($id_centro_estudio)){
                $mensaje='<div class="alert alert-danger">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                El campo Centro estudios es <a class="alert-link" href="#">Requerido</a>.
                </div>';
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($var=$this->model->existe($descripcion)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                 El Nucleo <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }

            if($this->model->insert([
                'descripcion'=>$descripcion,
                'id_centro_estudio'=>$id_centro_estudio
                
                ])){
                    $mensaje='<div class="alert alert-success">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            Nucleo Académico  '.$descripcion.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                        </div>';
                }else{
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                        Ha ocurrido un error al registrar Nucleo Academico<a class='alert-link' href='#'></a>
                    </div>";
                }

            $this->view->mensaje=$mensaje;
            $this->render();
        }
        
    }

    function editarNucleo(){

        if(isset($_POST['registrar2'])){
            $id_nucleo_academico=$_POST['id_nucleo_academico'];

            $descripcion2=$_POST['descripcion2'];
            $id_centro_estudio2=$_POST['id_centro_estudio2'];
            

            if(empty($descripcion2)){
                $mensaje='<div class="alert alert-danger">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            El campo nucleo académico es . <a class="alert-link" href="#">Requerido</a>.
                        </div>';
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }elseif(empty($id_centro_estudio2)){
                $mensaje='<div class="alert alert-danger">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                El campo Centro Estudio es <a class="alert-link" href="#">Requerido</a>.
                </div>';
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }

            if($this->model->update([
                'id_nucleo_academico'=>$id_nucleo_academico,
                'descripcion'=>$descripcion2,
                'id_centro_estudio'=>$id_centro_estudio2
                ])){    

          $mensaje="<div class='alert alert-success alert-dismissable'>
          <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
          El nucleo <b> ".$descripcion2." </b> fue Actualizado <a class='alert-link' href='#'>Correctamente</a></div>";
        
      }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Ha ocurrido un error al Actualizar el nucleo <b> ".$descripcion2." </b><a class='alert-link' href='#'></a>
        </div>";
        }

        }

        $this->view->mensaje=$mensaje;
        $this->render();
    }


      function removerNucleo($param=null){
        
        $id_nucleo_academico=$param[0];
  
        if($this->model->delete($id_nucleo_academico)){

            $mensaje="<div class='alert alert-success alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
           Removido correctamente el Nucleo Academico<b>  </b><a class='alert-link' href='#'></a>
        </div>";

       }else{
        $mensaje="<div class='alert alert-danger alert-dismissable'>
        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
       No se pudo remover el Nucleo Academico<b> </b><a class='alert-link' href='#'></a>
        </div>";
 
       }
       echo $mensaje;
    }

}
?>