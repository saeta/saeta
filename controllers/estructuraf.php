<?php
include_once 'sesiones/session_admin.php';

    class Estructuraf extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->estructuras=[];            
            
            $this->view->ambientes=[];
            
        }

        function render(){ 


            $estructuras=$this->model->get();
            $this->view->estructuras=$estructuras;

            $ambientes=$this->model->getambiente();
            $this->view->ambientes=$ambientes;

            $this->view->render('estructura/estructuraf');        
        }



        function registrarEstrutura(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
    
            $estructura=$_POST['estructura'];            
            $id_ambiente=$_POST['id_ambiente'];
            $estatus=$_POST['estatus'];
      
            
            
            $mensaje="";

            if($var=$this->model->existe($estructura)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                 Extructura Física <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($this->model->insert(['estructura'=>$estructura,'id_ambiente'=>$id_ambiente,'estatus'=>$estatus])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Estructura Física registrada<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al registrar Estructura Física <a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
          }



          function ActualizarEstructura(){//se agrega esta linea
          
      
    
            //$id_persona=$_SESSION["id_persona"];
    

            $id_ambiente_detalle=$_POST['id_ambiente_detalle'];
            $estructura=$_POST['estruct'];            
            $id_ambiente=$_POST['id_ambienteedit'];
            $estatus=$_POST['estatuss'];


            
         
           // var_dump($id_municipio);
            //var_dump( $id_estado,$codigoine, $estado,$id_pais2);
            $mensaje="";


           
    
            if($this->model->update(['id_ambiente_detalle'=>$id_ambiente_detalle,'estructura'=>$estructura,'id_ambiente'=>$id_ambiente,'estatus'=>$estatus])){    
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Estructura Física <b> ".$estructura." </b> Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar Estructura Física <b> ".$estructura." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }

    }

?>