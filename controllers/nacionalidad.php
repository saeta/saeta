<?php
include_once 'sesiones/session_admin.php';

    class Nacionalidad extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->paises=[];
            $this->view->paises2=[];
        }

        function render(){ 

            $paises2=$this->model->geto();
            $this->view->paises2=$paises2;
        

            $paises=$this->model->get();
            $this->view->paises=$paises;
            $this->view->render('persona/nacionalidad');        
        }



        function registrarNacionalidad(){//se agrega esta linea  
            //$id_persona=$_SESSION["id_persona"];
    
            $nacionalidad=$_POST['nacionalidad'];
            $id_pais=$_POST["id_pais"];


        


            $mensaje="";

            if($var=$this->model->existe($id_pais)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                La Nacionalidad <b>" . $nacionalidad . "</b> para este pais ya esta registrada <a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($this->model->insert(['nacionalidad'=>$nacionalidad,'id_pais'=>$id_pais])){
           
               
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Nacionalidad agregado<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al agregar una nacionalidad <a class='alert-link' href='#'></a>

            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }
          


          function ActualizarNacionalidad(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
            $nacionalidad=$_POST['nacionalidad2'];
            $id_pais=$_POST['id_pais5'];

            
           // var_dump($id_pais);
           // var_dump($nacionalidad);
      //break;
            //var_dump( $codigoine,$siglasine);
            $mensaje="";


            if($this->model->update(['id_pais5'=>$id_pais,'nacionalidad2'=>$nacionalidad])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Nacionalidad <b> ".$nacionalidad." </b>Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar la Nacionalidad <b> ".$nacionalidad." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }



    }

?>