<?php
include_once 'sesiones/session_admin.php';

    class EstadoCivil extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->estadocivil=[];
  
        }

        function render(){ 
            $estadocivil=$this->model->get();
            $this->view->estadocivil=$estadocivil;
            $this->view->render('persona/estadocivil');        
        }



        function registrarestadocivil(){//se agrega esta linea  
            //$id_persona=$_SESSION["id_persona"];
    
            $estadocivil=$_POST['estadocivil'];
            $codigo=$_POST['codigo'];

       // var_dump($estadocivil);
        //var_dump($codigo);   
          

            $mensaje="";

            if($var=$this->model->existe($estadocivil)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El  estado civil <b>" . $estadocivil . "</b>  ya esta registrado <a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($this->model->insert(['estadocivil'=>$estadocivil,'codigo'=>$codigo,'id_estado_civil'=>$id_estado_civil])){
           
               
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              estado civil agregado<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al agregar una estado civil <a class='alert-link' href='#'></a>

            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }
          



          function ActualizarEstadocivil(){//se agrega esta linea
          
            //$id_persona=$_SESSION["id_persona"];
            $estadocivil2=$_POST['estadocivil2'];
            $id_estado_civil2=$_POST['id_estado_civil2'];
            $codigo2=$_POST['codigo2'];
            
           /* var_dump( $estadocivil2);
            var_dump( $id_estado_civil2);
            var_dump( $codigo2);*/
           // var_dump($id_estadocivil2);
           // var_dump($estadocivil2);
      //break;
            //var_dump( $codigoine,$siglasine);
            $mensaje="";
           /* if($var=$this->model->existe($estadocivil2)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El tipo  estadocivil <b>" . $estadocivil2 . "</b>  ya esta registrado <a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }*/

            if($this->model->update(['estadocivil'=>$estadocivil2,'codigo'=>$codigo2,'id_estado_civil'=>$id_estado_civil2])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              estadocivil <b> ".$estadocivil2." </b>Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar el estadocivil<b> ".$estadocivil2." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }


          function VerEstadoCivil($param=null){
            $cadena=$param[0];
            list($id_estado_civil) = explode(',', $cadena);
      
            if($this->model->delete($id_estado_civil)){

                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
               Removido correctamente <b>  </b><a class='alert-link' href='#'></a>
            </div>";

            $this->view->mensaje=$mensaje;
            $this->render();

               ?>
    
               
    
                <?php
    
           }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
           No se pudo remover <b> </b><a class='alert-link' href='#'></a>
            </div>";

            $this->view->mensaje=$mensaje;
            $this->render();
                ?>
    
                
    
                <?php
             
           }
           echo $mensaje;
        }



    }

?>