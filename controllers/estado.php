<?php
include_once 'sesiones/session_admin.php';

    class Estado extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->estados=[];
            $this->view->paises=[];
        }

        function render(){ 
            $estados=$this->model->get();
            $this->view->estados=$estados;

            $paises=$this->model->getPais();
            $this->view->paises=$paises;
            $this->view->render('estructura/estado');        
        }



        function registrarEstado(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
    
            $codigoine=$_POST['codigoine'];            
            $estado=$_POST['estado'];
            $id_pais=$_POST['id_pais'];
           
            
            $mensaje="";

            if($var=$this->model->existe($estado)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                 País <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($this->model->insert(['codigoine'=>$codigoine,'estado'=>$estado,'id_pais'=>$id_pais])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Estado registrado<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al registar Estado<a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
          }



          function ActualizarEstado(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
    
            $id_estado=$_POST['id_estado2'];
            $codigoine=$_POST['codigoine2'];
            $estado=$_POST['estado2'];
            $id_pais2=$_POST['id_pais2'];
            
            //var_dump( $id_estado,$codigoine, $estado,$id_pais2);
            $mensaje="";


           
    
            if($this->model->update(['id_estado'=>$id_estado,'codigoine'=>$codigoine,'estado'=>$estado,'id_pais'=>$id_pais2])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Estado <b> ".$estado." </b> Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar Estado <b> ".$estado." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }

    }

?>