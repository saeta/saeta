<?php
//ini_set("display_errors",1);
include_once 'models/datosacademicos/comision_excedencia.php';
include_once 'models/datosacademicos/tipo_info.php';
include_once 'models/datosacademicos/docente.php';
include_once 'sesiones/session_admin.php';

class Comision_Excedencia extends Controller{
    function __construct(){
        parent::__construct();
        $this->view->mensaje="";
        $this->view->infos=[];
        $this->view->docentes=[];
        $this->view->comisiones=[];
        
       
    }

    function render(){
        $infos=$this->model->getInfo();
        $this->view->infos=$infos;
        $docentes=$this->model->getDocente();
        $this->view->docentes=$docentes;
        $comisiones=$this->model->getComision($_SESSION['identificacion']);
        $this->view->comisiones=$comisiones;
        //var_dump($comisiones);
        $this->view->render('socio-academica/comision_excedencia');
    }


    function viewRegistrar(){
        $infos=$this->model->getInfo();
        $this->view->infos=$infos;
        $docentes=$this->model->getDocente();
        $this->view->docentes=$docentes;
        $comisiones=$this->model->getComision($_SESSION['identificacion']);
        $this->view->comisiones=$comisiones;
        
        $this->view->render('socio-academica/agregar_comision');
    }


    function viewEditar($param = null){
        $id_comision_excedencia=$param[0];
        
        $infos=$this->model->getInfo($id_comision_excedencia);
        $this->view->infos=$infos;
        $docentes=$this->model->getDocente($id_comision_excedencia);
        $this->view->docentes=$docentes;
        $comisiones=$this->model->getbyId($id_comision_excedencia);
        $_SESSION['id_comision_excedencia']= $id_comision_excedencia;
        $this->view->comisiones=$comisiones;
        //var_dump($_SESSION['id_comision_excedencia']);
        $this->view->render('socio-academica/editar_comision');
    }

    function viewVer($param = null){
        $id_comision_excedencia=$param[0];

        $infos=$this->model->getInfo($id_comision_excedencia);
        $this->view->infos=$infos;
        $docentes=$this->model->getDocente($id_comision_excedencia);
        $this->view->docentes=$docentes;
        $comisiones=$this->model->getbyId($id_comision_excedencia);
        $_SESSION['id_comision_excedencia']= $id_comision_excedencia;
        $this->view->comisiones=$comisiones;
        
        $this->view->render('socio-academica/ver_comision');
    }

    function registrarComision(){

        if(isset($_POST['registrar'])){
            $id_tipo_info=$_POST['id_tipo_info'];
            $lugar=$_POST['lugar'];
            $desde=$_POST['desde'];
            $hasta=$_POST['hasta'];
            $designacion_funcion=$_POST['designacion_funcion'];
            $causa=$_POST['causa'];
            $aprobacion=$_POST['aprobacion'];
            $id_docente=$_SESSION['id_docente'];
            $estatus_verificacion=$_POST['estatus_verificacion'];

//var_dump($aprobacion);

            if($this->model->insert([
                'id_tipo_info'=>$id_tipo_info,
                'lugar'=>$lugar,
                'desde'=>$desde,
                'hasta'=>$hasta,
                'designacion_funcion'=>$designacion_funcion,
                'causa'=>$causa,
                'aprobacion'=>$aprobacion,
                'id_docente'=>$id_docente,
                'estatus_verificacion'=>$estatus_verificacion,
                'id_persona'=>$_SESSION['id_persona']
                

                ])){
                    
                    $mensaje='<div class="alert alert-success">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    La Comision o Excedencia ha sido registrada <a class="alert-link" href="#">Exitosamente</a>.
                        </div>';
                }else{
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                        Ha ocurrido un error al registrar la Comision o Excedencia<a class='alert-link' href='#'></a>
                    </div>";
                }

            $this->view->mensaje=$mensaje;
            $this->render();
        }
        
    }

    function editarComision($param){
        
     
        
            $id_comision_excedencia=$_POST['id_comision_excedencia'];
            $id_tipo_info=$_POST['id_tipo_info'];
            $lugar=$_POST['lugar'];
            $desde=$_POST['desde'];
            $hasta=$_POST['hasta'];
            $designacion_funcion=$_POST['designacion_funcion'];
            $causa=$_POST['causa'];
            $aprobacion=$_POST['aprobacion'];
            $id_docente=$_SESSION['id_docente'];
            
            //var_dump($id_comision_excedencia);
            if($this->model->update([
                'id_comision_excedencia'=>$id_comision_excedencia,
                'id_tipo_info'=>$id_tipo_info,
                'lugar'=>$lugar,
                'desde'=>$desde,
                'hasta'=>$hasta,
                'designacion_funcion'=>$designacion_funcion,
                'causa'=>$causa,
                'aprobacion'=>$aprobacion,
                'id_docente'=>$id_docente,
                'id_persona'=>$_SESSION['id_persona']
                
                ])){    

          $mensaje="<div class='alert alert-success alert-dismissable'>
          <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
          La Comision o Excedencia fue Actualizada <a class='alert-link' href='#'>Correctamente</a></div>";
        
      }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Ha ocurrido un error al Actualizar la Comision o Excendencia <a class='alert-link' href='#'></a>
        </div>";
        }
        $this->view->mensaje=$mensaje;
        $this->render($param);

        

       
    }


      function removerComision($param=null){
        
        $id_comision_excedencia=$param[0];
  
        if($this->model->delete($id_comision_excedencia)){

            $mensaje="<div class='alert alert-success alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
           Removido correctamente el diseño de la unidad curricular<b>  </b><a class='alert-link' href='#'></a>
        </div>";

       }else{
        $mensaje="<div class='alert alert-danger alert-dismissable'>
        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
       No se pudo remover el diseño de la unidad curricular<b> </b><a class='alert-link' href='#'></a>
        </div>";
 
       }
       echo $mensaje;
    }

    
}
?>