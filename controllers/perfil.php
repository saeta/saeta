<?php
//ini_set("display_errors", 1);
    include_once 'sesiones/session_admin.php';
    include_once 'scripts/correos.php';

    class Perfil extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->usuarios=[]; 

            $this->view->perfiles=[]; 
            $this->view->paises=[]; 
            $this->view->estadosCivil=[];  
            $this->view->generos=[];
            $this->view->etnias=[];
            $this->view->documentoidentidad=[];
            $this->view->nacionalidades=[];    
            $this->view->tcorreos=[];
            $this->view->telefono_tipo=[];
            $this->view->cod_area_telefono=[];
            $this->view->discapacidades=[];
            $this->view->tipo_discapacidades=[];
            $this->view->estados=[];            
            $this->view->ciudades=[];
            $this->view->domicilios=[];
            $this->view->parroquias=[];
            $this->view->informacion=[];
            $this->view->informacion_u=[];
            $this->view->informacion_d=[];
            $this->view->perfiles=[];
            $this->view->roles=[];
           

            
        }

        function render($param = null){
            $id_persona = $param[0];
            $this->view->id_persona=$id_persona;
            //trae informacion personal de usuario
            $informacion = $this->model->getbyIdUsuarioInfo($id_persona);
            if($informacion->id_perfil==1){
              $programas_docente = $this->model->getProgramasDocente($informacion->id_docente);
              $k=1;
              foreach($programas_docente as $row){
                $programa_docente=new Persona();
                $programa_docente=$row;
                $programa = $this->model->getProgramaDocentebyID($programa_docente->id_docente_programa);
        
                $doc_prog[$k]=[
                  'id_docente_programa'.$k=>$programa->id_docente_programa,
                  'id_programa'.$k=>$programa->id_programa,
                  'programa'.$k=>$programa->programa
                ];
                $this->view->doc_prog[$k]=$doc_prog[$k];
                $k++;
              }
              $this->view->programas_docente = $programas_docente;
            }
            //trae informacion de usuario
            $informacion_u = $this->model->getbyIdUsuarioInfoU($id_persona);
        
            $j=1;
            foreach($informacion_u as $row){
              $roles_u=new Persona();
              $roles_u=$row;
              $rol_u = $this->model->getRolbyID($id_persona, $roles_u->id_rol);
        
              $rol[$j]=[
                'id_usuario'.$j=>$rol_u->id_usuario,
                'id_rol'.$j=>$rol_u->id_rol,
                'rol'.$j=>$rol_u->rol
              ];
        
              $this->view->rol[$j]=$rol[$j];
              $j++;
        
            }

            //trae informacion de domicilio de la persona
            $domicilio_p = $this->model->getDomicilioPersona($id_persona);
            $this->view->domicilio_p=$domicilio_p;
        
            //trae las discapacidades
            $informacion_d = $this->model->getbyIdUsuarioInfoD($id_persona);
            $i=1;
            //var_dump("Cuenta >>",count($informacion_d));
        
            foreach($informacion_d as $row){
              $disc_u=new Persona;
              $disc_u=$row;
              $obj=$this->model->getbyIdDisc($disc_u->id_persona_discapacidad);
              
              $disc[$i]=[
                        'id_persona_discapacidad'.$i=>$obj->id_persona_discapacidad,
                        'id_discapacidad'.$i=>$obj->id_discapacidad,
                        'codigo_conapdis'.$i=>$obj->codigo_conapdis,
                        'observacion'.$i=>$obj->observacion,
                        'discapacidad'.$i=>$obj->discapacidad,
                        'id_tipo_discapacidad'.$i=>$obj->id_tipo_discapacidad,
                        'tipo_discapacidad'.$i=>$obj->tipo_discapacidad
                      ];
        
              $this->view->disc[$i]=$disc[$i];
              $i++;
            }
            //var_dump($disc[1] , $disc[2]);
            $informacion_n = $this->model->getbyIdUsuarioInfoN($id_persona);
        
            //obtener preguntas del usuario
            $preguntas_u = $this->model->getPreguntas_u($id_persona);

            $this->view->informacion = $informacion;
            $this->view->informacion_u = $informacion_u;
            $this->view->informacion_d = $informacion_d;
            $this->view->informacion_n = $informacion_n;
            
            ///////////////////////////////////////////////////////////////////////
        
             //tablas catalogo donde se utilizo una sola funcion para mostrar sus datos
            $generos=$this->model->getCatalogo("genero");
            $this->view->generos=$generos;
        
            $estadosCivil=$this->model->getCatalogo("estado_civil");
            $this->view->estadosCivil=$estadosCivil;
        
            $etnias=$this->model->getCatalogo("etnia");
            $this->view->etnias=$etnias;
        
            $documentoidentidad=$this->model->getCatalogo("documento_identidad_tipo");
            $this->view->documentoidentidad=$documentoidentidad;
        
            $domicilios=$this->model->getCatalogo("domicilio_detalle_tipo");
            $this->view->domicilios=$domicilios;
        
            $tcorreos=$this->model->getCatalogo("correo_tipo");
            $this->view->tcorreos=$tcorreos;
        
            $telefono_tipo=$this->model->getCatalogo("telefono_tipo");
            $this->view->telefono_tipo=$telefono_tipo;
        
            $cod_area_telefono=$this->model->getCatalogo("telefono_codigo_area");
            $this->view->cod_area_telefono=$cod_area_telefono;
            
            $perfiles=$this->model->getCatalogo('perfil');
            $this->view->perfiles=$perfiles;
            //////////////////////////////////////////////////////////////////
            $paises=$this->model->getPais();
            $this->view->paises=$paises;
        
            $nacionalidades=$this->model->getNacionalidad();
            $this->view->nacionalidades=$nacionalidades;
        
            $roles=$this->model->getCatalogo('rol');
            $this->view->roles=$roles;
        
            //docentes
            //obtener ejes regionales
            $ejes_regionales=$this->model->getEjeRegional();
            $this->view->ejes_regionales=$ejes_regionales;
        
            //obtener centros de estudio
            $centros=$this->model->getCentro();
            $this->view->centros=$centros;
        
            //obtener programas
            $programas=$this->model->getPrograma();
            $this->view->programas=$programas;
            //obtener preguntas
            $preguntas=$this->model->getPregunta();
            $this->view->preguntas=$preguntas;
        
            $dedicaciones=$this->model->getCatalogo("docente_dedicacion");
            $this->view->dedicaciones=$dedicaciones;
        
            $docente_estatus=$this->model->getCatalogo("docente_estatus");
            $this->view->docente_estatus=$docente_estatus;
        
            $clasificaciones=$this->model->getCatalogo("clasificacion_docente");
            $this->view->clasificaciones=$clasificaciones;
        
            $escalafones=$this->model->getCatalogo("escalafon");
            $this->view->escalafones=$escalafones;
        
            $tipo_discapacidades=$this->model->getCatalogo("tipo_discapacidad");
            $this->view->tipo_discapacidades=$tipo_discapacidades;
           
            if($id_persona!=$_SESSION['id_persona']){
                $this->view->render('errores/index');

            }else{
                $this->view->render('perfil/index');

            }
        }

         function verPerfil($param = null){
          $id_persona = $param[0];
          //trae informacion personal de usuario
          $informacion = $this->model->getbyIdUsuarioInfo($id_persona);
          //trae informacion de usuario (roles)
         $informacion_u = $this->model->getbyIdUsuarioInfoU($id_persona);
         //trae las discapacidades
         $informacion_d = $this->model->getbyIdUsuarioInfoD($id_persona);

         $informacion_n = $this->model->getbyIdUsuarioInfoN($id_persona);
         

          $this->view->informacion = $informacion;
          $this->view->informacion_u = $informacion_u;
          $this->view->informacion_d = $informacion_d;
          $this->view->informacion_n = $informacion_n;
          $this->view->mensaje ="";
          //var_dump($informacion_n);
          $this->view->render('perfil/infoperfil');
      }

         

          










      function viewEdit($param = null){
        $id_persona = $param[0];
        $this->view->id_persona=$id_persona;
        //trae informacion personal de usuario
        $informacion = $this->model->getbyIdUsuarioInfo($id_persona);
        
        //trae informacion de usuario
        $informacion_u = $this->model->getbyIdUsuarioInfoU($id_persona);
    
        $j=1;
        foreach($informacion_u as $row){
          $roles_u=new Persona();
          $roles_u=$row;
          $rol_u = $this->model->getRolbyID($id_persona, $roles_u->id_rol);
    
          $rol[$j]=[
            'id_usuario'.$j=>$rol_u->id_usuario,
            'id_rol'.$j=>$rol_u->id_rol,
            'rol'.$j=>$rol_u->rol
          ];
    
          $this->view->rol[$j]=$rol[$j];
          $j++;
    
        }
    
        //trae informacion de domicilio de la persona
        $domicilio_p = $this->model->getDomicilioPersona($id_persona);
        $this->view->domicilio_p=$domicilio_p;
    
        //trae las discapacidades
        $informacion_d = $this->model->getbyIdUsuarioInfoD($id_persona);
        $i=1;
        //var_dump("Cuenta >>",count($informacion_d));
    
        foreach($informacion_d as $row){
          $disc_u=new Persona;
          $disc_u=$row;
          $obj=$this->model->getbyIdDisc($disc_u->id_persona_discapacidad);
          
          $disc[$i]=[
                    'id_persona_discapacidad'.$i=>$obj->id_persona_discapacidad,
                    'id_discapacidad'.$i=>$obj->id_discapacidad,
                    'codigo_conapdis'.$i=>$obj->codigo_conapdis,
                    'observacion'.$i=>$obj->observacion,
                    'discapacidad'.$i=>$obj->discapacidad,
                    'id_tipo_discapacidad'.$i=>$obj->id_tipo_discapacidad,
                    'tipo_discapacidad'.$i=>$obj->tipo_discapacidad
                  ];
    
          $this->view->disc[$i]=$disc[$i];
          $i++;
        }
        //var_dump($disc[1] , $disc[2]);
        $informacion_n = $this->model->getbyIdUsuarioInfoN($id_persona);
    
        //obtener preguntas del usuario
        $preguntas_u = $this->model->getPreguntas_u($id_persona);

        $k=1;
        foreach($preguntas_u as $row){
          $pre_u=new Persona;
          $pre_u=$row;
          $preg=$this->model->getPreguntasbyId($id_persona, $pre_u->id_pregunta);
          $preguntasU[$k]=[
            'id_pregunta'.$k=>$preg->id_pregunta,
            'pregunta'.$k=>$preg->pregunta,
            'respuesta'.$k=>$preg->respuesta,
            'id_usuario_pregunta'.$k=>$preg->id_usuario_pregunta
          ];
          $this->view->preguntasU[$k]=$preguntasU[$k];
          $k++;
        }

        $this->view->informacion = $informacion;
        $this->view->informacion_u = $informacion_u;
        $this->view->informacion_d = $informacion_d;
        $this->view->informacion_n = $informacion_n;
        
        ///////////////////////////////////////////////////////////////////////
    
         //tablas catalogo donde se utilizo una sola funcion para mostrar sus datos
        $generos=$this->model->getCatalogo("genero");
        $this->view->generos=$generos;
    
        $estadosCivil=$this->model->getCatalogo("estado_civil");
        $this->view->estadosCivil=$estadosCivil;
    
        $etnias=$this->model->getCatalogo("etnia");
        $this->view->etnias=$etnias;
    
        $documentoidentidad=$this->model->getCatalogo("documento_identidad_tipo");
        $this->view->documentoidentidad=$documentoidentidad;
    
        $domicilios=$this->model->getCatalogo("domicilio_detalle_tipo");
        $this->view->domicilios=$domicilios;
    
        $tcorreos=$this->model->getCatalogo("correo_tipo");
        $this->view->tcorreos=$tcorreos;
    
        $telefono_tipo=$this->model->getCatalogo("telefono_tipo");
        $this->view->telefono_tipo=$telefono_tipo;
    
        $cod_area_telefono=$this->model->getCatalogo("telefono_codigo_area");
        $this->view->cod_area_telefono=$cod_area_telefono;
        
        $perfiles=$this->model->getCatalogo('perfil');
        $this->view->perfiles=$perfiles;
        //////////////////////////////////////////////////////////////////
        $paises=$this->model->getPais();
        $this->view->paises=$paises;
    
        $nacionalidades=$this->model->getNacionalidad();
        $this->view->nacionalidades=$nacionalidades;
    
        $roles=$this->model->getCatalogo('rol');
        $this->view->roles=$roles;
    
        //docentes
        //obtener ejes regionales
        $ejes_regionales=$this->model->getEjeRegional();
        $this->view->ejes_regionales=$ejes_regionales;
    
        //obtener centros de estudio
        $centros=$this->model->getCentro();
        $this->view->centros=$centros;
    
        //obtener programas
        $programas=$this->model->getPrograma();
        $this->view->programas=$programas;
        //obtener preguntas
        $preguntas=$this->model->getPregunta();
        $this->view->preguntas=$preguntas;
    
        $dedicaciones=$this->model->getCatalogo("docente_dedicacion");
        $this->view->dedicaciones=$dedicaciones;
    
        $docente_estatus=$this->model->getCatalogo("docente_estatus");
        $this->view->docente_estatus=$docente_estatus;
    
        $clasificaciones=$this->model->getCatalogo("clasificacion_docente");
        $this->view->clasificaciones=$clasificaciones;
    
        $escalafones=$this->model->getCatalogo("escalafon");
        $this->view->escalafones=$escalafones;
    
        $tipo_discapacidades=$this->model->getCatalogo("tipo_discapacidad");
        $this->view->tipo_discapacidades=$tipo_discapacidades;
       
    
        $this->view->render('perfil/editProfile');
    
        
    }


    function editUser($param){//se agrega esta linea
    
        //$id_persona=$_SESSION["id_persona"];
        
        //ids
        $id_persona=$param[0]; 
                    
        //tabla persona
        $pnombre=$_POST['pnombre'];            
        $snombre=$_POST['snombre'];
        $papellido=$_POST['papellido'];
        $sapellido=$_POST['sapellido'];
        $fnac=$_POST['fnac'];
        $estadoCivil=$_POST['estadocivil'];
        $genero=$_POST['genero'];
        $pais=$_POST['pais'];
            

        //persona etnia no es requerido
        $id_etnia=$_POST['etnia'];

        //tabla persona nacionalidad

        $id_documento_identidad_tipo=$_POST['tdocumento'];            
        $id_nacionalidad=$_POST['nacionalidad'];           
        $numero_documento=$_POST['ndocumento'];

        //persona Telefono
        $id_telefono_tipo=$_POST['tipotelf'];
        $id_telefono_codigo_area=$_POST['cod_area'];
        $telefono_numero=$_POST['numero'];

        //Domicilio persona
        // $id_ciudad=$_POST['ciudad'];
        $id_parroquia=$_POST['parroquia'];
        //domicilio detalle
        $direccion=$_POST['direccion'];

        //domicilio detalle_tipo
        $tipo_domicilio=$_POST['tdomicilio'];


        //persona_correo
        $id_correo_tipo=$_POST['id_correo_personal'];
        $correo=$_POST['correo_personal'];
        $id_persona_correo=$_POST['id_persona_correo'];

        if(!empty($_POST['correo_institucional'])){
          $id_persona_institucional=$_POST['id_persona_institucional'];
          $id_correo_institucional=$_POST['id_correo_institucional'];
          $correo_institucional=$_POST['correo_institucional'];
        }else{
          $id_persona_institucional="";
          $id_correo_institucional="";
          $correo_institucional="";
        }



        //persona discapacidad no es requerido
        $id_persona_discapacidad=$_POST['id_persona_discapacidad']; 
        $id_discapacidad=$_POST['discapacidad'];
        $codigo_conais=$_POST['conadis'];
        $observaciones=$_POST['observaciones'];

        //persona discapacidad no es requerido OTRA SEGUNDA DISCAPACIDAD
        $id_persona_discapacidad1=$_POST['id_persona_discapacidad1']; 
        $id_discapacidad1=$_POST['discapacidad1'];
        $observaciones1=$_POST['observaciones1'];

        $password=$_POST['password'];

        //hijos y fecha de ingreso
        $fingreso=$_POST['fingreso'];
        $nhijos=$_POST['nhijos'];
        $npais=$_POST['paisnac'];
          /*  var_dump($id_persona,$pnombre,$snombre,$papellido,$sapellido,
            $fnac,$estadoCivil,$genero,$pais,
            $id_documento_identidad_tipo,$id_nacionalidad,$numero_documento,                                  
            $id_telefono_tipo,$id_telefono_codigo_area,$telefono_numero,
            $id_parroquia,
            $direccion,$tipo_domicilio,
            $id_correo_tipo,$correo,$id_etnia,
            $id_discapacidad,
            $codigo_conais,$observaciones,
            $id_discapacidad1,$observaciones1,
            $id_rol,$id_perfil,$estatus);*/

        
        //seguridad
        $pregunta1=$_POST['pregunta1'];
        list($id_pregunta1, $id_usuario_pregunta1) = explode(",", $pregunta1);
        $respuesta1=$_POST['respuesta1'];

        $pregunta2=$_POST['pregunta2'];
        list($id_pregunta2, $id_usuario_pregunta2) = explode(",",$pregunta2);

        $respuesta2=$_POST['respuesta2'];
        $parroquia=$_POST['parroquia'];

        if($id_pregunta1==$id_pregunta2){
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close type='button'></button>
            <i>ERROR:</i> Las <a class='alert-link' href='#'> preguntas de seguridad</a>  deben ser diferentes</div>";
            $this->view->mensaje=$mensaje;
            $this->viewEdit($param);
            exit();
        }elseif($respuesta1==$respuesta2){
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close type='button'></button>
            <i>ERROR:</i> Las <a class='alert-link' href='#'> respuestas de seguridad</a> deben ser diferentes</div>";
            $this->view->mensaje=$mensaje;
            $this->viewEdit($param);
            exit();
        }

        if($this->model->updateUser(['id_persona'=>$id_persona,'pnombre'=>$pnombre,'snombre'=>$snombre,'papellido'=>$papellido,'sapellido'=>$sapellido,
        'fnac'=>$fnac,'estadoCivil'=>$estadoCivil,'genero'=>$genero,'pais'=>$pais,
        'id_documento_identidad_tipo'=>$id_documento_identidad_tipo,'id_nacionalidad'=>$id_nacionalidad,'numero_documento'=>$numero_documento,                                  
        'id_telefono_tipo'=>$id_telefono_tipo,'id_telefono_codigo_area'=>$id_telefono_codigo_area,'telefono_numero'=>$telefono_numero,
        'id_parroquia'=>$id_parroquia,
        'direccion'=>$direccion,'tipo_domicilio'=>$tipo_domicilio,
        'id_correo_tipo'=>$id_correo_tipo,'correo'=>$correo,'id_etnia'=>$id_etnia,
        'id_persona_discapacidad'=>$id_persona_discapacidad,
        'id_discapacidad'=>$id_discapacidad,
        'codigo_conais'=>$codigo_conais,'observaciones'=>$observaciones,
        'id_persona_discapacidad1'=>$id_persona_discapacidad1,
        'id_discapacidad1'=>$id_discapacidad1,'observaciones1'=>$observaciones1,
        'password'=>$password,'nhijos'=>$nhijos,'fingreso'=>date("Y-m-d", strtotime($fingreso)), 
        'npais'=>$npais,'pregunta1'=>$id_pregunta1,'respuesta1'=>$respuesta1,
        'pregunta2'=>$id_pregunta2, 'respuesta2'=>$respuesta2, 'id_usuario_pregunta1'=>$id_usuario_pregunta1,
        'id_usuario_pregunta2'=>$id_usuario_pregunta2, 'id_correo_institucional'=>$id_correo_institucional,'correo_institucional'=>$correo_institucional,
        'id_persona_correo'=>$id_persona_correo,'id_persona_institucional'=>$id_persona_institucional])){

        $mensaje="<div class='alert alert-success alert-dismissable'>
        <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
        Haz Actualizado tu Perfil con Éxito su Usuario es: <b>ubv$numero_documento</b> y su contraseña es:<b> $password</b> <a class='alert-link' href='#'></a></div>";

          // enviar correos correspondientes

          $mensaje_correo="<p>
          ¡Felicidades! Sr.(a) ".$pnombre." ".$papellido." su Cuenta ha Sido Actualizada de manera 
          Exitosa en el Sistema Integrado de Desarrollo de Las Trabajadoras 
          y Los Trabajadores Académicos. Su Usuario de Acceso es: <b>ubv$numero_documento</b> y Su Clave de Acceso es: <b> $password</b> 
          <b>(SIDTA)</b>. 
          <br><br>
          </p>
          <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";

          $correo_usuario=new Correo();
              $var=$correo_usuario->EnviarCorreo([
                  'correo_destino'=>$correo,
                  'nombre_destino'=>$pnombre,
                  'apellido_destino'=>$papellido,
                  'asunto'=>'SIDTA - Actualización de Perfil de Usuario',
                  'mensaje'=>$mensaje_correo
                  ]);

        }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Ha Ocurrido un Error al Intentar Editar el Usuario <b>ubv$numero_documento</b>. <a class='alert-link' href='#'></a>
            </div>";
        
        }

        $this->view->mensaje=$mensaje;
        $this->render($param);
        
      }

      




    }

?>