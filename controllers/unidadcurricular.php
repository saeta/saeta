<?php
include_once 'models/datosacademicos/unidad_curricular.php';
include_once 'sesiones/session_admin.php';

class Unidadcurricular extends Controller{
    function __construct(){
        parent::__construct();
        $this->view->mensaje="";
        $this->view->unidades=[];

    }

    function render(){
        $unidades=$this->model->getUnidad();
        $this->view->unidades=$unidades;
        $this->view->render('adm_datos_academicos/unidad_curricular');
    }

    function registrarUnidad(){

        if(isset($_POST['registrar'])){
            $descripcion=$_POST['descripcion'];
            if($_POST['estatus'] == 'Activo'){
                $estatus=1;
            }elseif($_POST['estatus'] == 'Inactivo'){
                $estatus=0;
            }

            if(empty($descripcion)){
                $mensaje='<div class="alert alert-danger">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            El campo unidad curricular es . <a class="alert-link" href="#">Requerido</a>.
                        </div>';
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            if($var=$this->model->existe($descripcion)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                 La Unidad curricular <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }

            if($this->model->insert([
                'descripcion'=>$descripcion,
                'estatus'=>$estatus
                ])){
                    $mensaje='<div class="alert alert-success">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Unidad Curricular  '.$descripcion.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                        </div>';
                }else{
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                        Ha ocurrido un error al registrar Unidad Curricular<a class='alert-link' href='#'></a>
                    </div>";
                }

            $this->view->mensaje=$mensaje;
            $this->render();
        }
    }

    function editarUnidad(){

        if(isset($_POST['registrar2'])){

            $descripcion2=$_POST['descripcion2'];
            $id_unidad_curricular=$_POST['id_unidad_curricular'];
            if($_POST['estatus2'] == 'Activo'){
                $estatus2=1;
            }elseif($_POST['estatus2'] == 'Inactivo'){
                $estatus2=0;
            }

            if(empty($descripcion2)){
                $mensaje='<div class="alert alert-danger">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            El campo unidad curricular es . <a class="alert-link" href="#">Requerido</a>.
                        </div>';
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }

            if($this->model->update([
                'descripcion'=>$descripcion2,
                'id_unidad_curricular'=>$id_unidad_curricular,
                'estatus'=>$estatus2
                ])){

          $mensaje="<div class='alert alert-success alert-dismissable'>
          <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
          La Unidad Curricular <b> ".$descripcion2." </b> fue Actualizada <a class='alert-link' href='#'>Correctamente</a></div>";
      }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Ha ocurrido un error al Actualizar la unidad <b> ".$descripcion2." </b><a class='alert-link' href='#'></a>
        </div>";
        }

        }

        $this->view->mensaje=$mensaje;
        $this->render();
    }


      function removerUnidad($param=null){

        $id_unidad_curricular=$param[0];

        if($this->model->delete($id_unidad_curricular)){

            $mensaje="<div class='alert alert-success alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
           Removido correctamente la unidad curricular<b>  </b><a class='alert-link' href='#'></a>
        </div>";

       }else{
        $mensaje="<div class='alert alert-danger alert-dismissable'>
        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
       No se pudo remover la unidad curricular<b> </b><a class='alert-link' href='#'></a>
        </div>";

       }
       echo $mensaje;
    }

}
?>