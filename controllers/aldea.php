<?php
include_once 'sesiones/session_admin.php';

    class Aldea extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
                  
            $this->view->aldeaosede=[];

            $this->view->aldeas=[];
            $this->view->ambientes=[]; 

            
        }

        function render(){ 

            $aldeaosede=$this->model->get();
            $this->view->aldeaosede=$aldeaosede;
          
            $aldeas=$this->model->getAldeaTipo();
            $this->view->aldeas=$aldeas;

            $ambientes=$this->model->getAmbiente();
            $this->view->ambientes=$ambientes;

            $this->view->render('estructura/aldea');        
        }



        function registrarAldea(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
    
            $codigoaldea=$_POST['codigoaldea'];            
            $aldeatipo=$_POST['aldeatipo'];
            $aldea=$_POST['aldea'];

            $ambiente=$_POST['ambiente'];
            $codigosucre=$_POST['codigosucre'];
           
            $estatus=$_POST['estatus'];
      
            
            
            $mensaje="";

            if($var=$this->model->existe($codigoaldea)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                 Aldea <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($this->model->insert(['codigoaldea'=>$codigoaldea,'aldeatipo'=>$aldeatipo,'aldea'=>$aldea
            ,'ambiente'=>$ambiente,'codigosucre'=>$codigosucre,'estatus'=>$estatus])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Aldea registrada<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al registrar Aldea <a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
          }



          function ActualizarAldea(){//se agrega esta linea
          
      
    
            //$id_persona=$_SESSION["id_persona"];
    
            $id_aldea=$_POST['id_aldea'];   
            $codigoaldea=$_POST['codaldea'];            
            $aldeatipo=$_POST['tipoaldea'];
            $aldea=$_POST['aldean'];

            $ambiente=$_POST['ambienten'];
            $codigosucre=$_POST['sucre'];

            $estatus=$_POST['estatuss'];
    
         
           // var_dump($id_municipio);
            //var_dump( $id_estado,$codigoine, $estado,$id_pais2);
            $mensaje="";

          
           
    
            if($this->model->update(['id_aldea'=>$id_aldea,'codigoaldea'=>$codigoaldea,'aldeatipo'=>$aldeatipo,'aldea'=>$aldea
            ,'ambiente'=>$ambiente,'codigosucre'=>$codigosucre,'estatus'=>$estatus])){
             
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
             Aldea <b> ".$aldea." </b> Actualizada Correctamente<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar Aldea <b> ".$aldea." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }

    }

?>