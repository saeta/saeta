<?php
    include_once 'sesiones/session_admin.php';
    include_once 'scripts/correos.php';

    class Censo_docente extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->usuarios=[]; 

            $this->view->perfiles=[]; 
            $this->view->paises=[]; 
            $this->view->estadosCivil=[];  
            $this->view->generos=[];
            $this->view->etnias=[];
            $this->view->documentoidentidad=[];
            $this->view->nacionalidades=[];    
            $this->view->tcorreos=[];
            $this->view->telefono_tipo=[];
            $this->view->cod_area_telefono=[];
            $this->view->discapacidades=[];
            $this->view->tipo_discapacidades=[];
            $this->view->estados=[];            
            $this->view->ciudades=[];
            $this->view->domicilios=[];
            $this->view->parroquias=[];
            $this->view->informacion=[];
            $this->view->informacion_u=[];
            $this->view->informacion_d=[];
            $this->view->perfiles=[];
            $this->view->roles=[];
           

            
        }
    //render censodocente
        function render(){ 
            //revisar estrutura

            $usuarios=$this->model->get();
            $this->view->usuarios=$usuarios;
  
            $this->view->render('censo_docente/index');     
        }

    

        function verUsuario($param = null){
            $cedula = $param[0];
            $this->view->cedula=$cedula;
            //trae informacion personal de usuario
            
            $user=$this->model->getIdPersonabyCedula($cedula);
            //evaluar si la persona existe
            if(empty($user->id_persona)){
              /*
              revisa aqui
              esta es la consulta que hiciste que trae los datos de nomina
              a mi parecer hay que revisar bien lo que trae, por que los nombres aun los trae desordenados
              en algunos casos, ademas a veces no trae el escalafon (en la consulta si lo trae 
              pero no lo muestra al usuario). Tambien en algunos casos trae tambien el guion delante de el 
              numero de telefono ejemplo: "-2425931" en lugar de traer solo el numero "2425931"
              Aqui añadi dos full join para traer el id_dedicacion e id_clasificacion, para poder mostrarlos 
              al usuario, que era lo que te comentaba el otro dia.

              */
              //preparar insert
              $informacion = $this->model->getNominabyCedula($cedula);//consulta que habias colocado en getbyIdUsuarioInfo()
              $action="addUser";//variable en caso de  insertar
            }else{//cuando el usuario existe ejecutamos normalmente el edit
              $this->view->id_persona=$user->id_persona;//parametro para editar
              //preparar edit
              $informacion = $this->model->getbyIdUsuarioInfo($user->id_persona);//conserve el edit viejo con id_persona
              $programas_docente = $this->model->getProgramasDocente($informacion->id_docente);
            $k=1;//traer programas de formacion del docente
            foreach($programas_docente as $row){
              $programa_docente=new Persona();
              $programa_docente=$row;
              $programa = $this->model->getProgramaDocentebyID($programa_docente->id_docente_programa);

              $doc_prog[$k]=[
                'id_docente_programa'.$k=>$programa->id_docente_programa,
                'id_programa'.$k=>$programa->id_programa,
                'programa'.$k=>$programa->programa
              ];
              $this->view->doc_prog[$k]=$doc_prog[$k];
              $k++;
            }
            $this->view->programas_docente = $programas_docente;
            //trae informacion de usuario
            $informacion_u = $this->model->getbyIdUsuarioInfoU($user->id_persona);
        
            $j=1;//traer roles de usuario
            foreach($informacion_u as $row){
              $roles_u=new Persona();
              $roles_u=$row;
              $rol_u = $this->model->getRolbyID($user->id_persona, $roles_u->id_rol);
        
              $rol[$j]=[
                'id_usuario'.$j=>$rol_u->id_usuario,
                'id_rol'.$j=>$rol_u->id_rol,
                'rol'.$j=>$rol_u->rol
              ];
        
              $this->view->rol[$j]=$rol[$j];
              $j++;
        
            }
        
            //trae informacion de domicilio de la persona
            $domicilio_p = $this->model->getDomicilioPersona($user->id_persona);
            $this->view->domicilio_p=$domicilio_p;
        
            //trae las discapacidades
            $informacion_d = $this->model->getbyIdUsuarioInfoD($user->id_persona);
            $i=1;
            //var_dump("Cuenta >>",count($informacion_d));
            //traer discapacidades
            foreach($informacion_d as $row){
              $disc_u=new Persona;
              $disc_u=$row;
              $obj=$this->model->getbyIdDisc($disc_u->id_persona_discapacidad);
              
              $disc[$i]=[
                        'id_persona_discapacidad'.$i=>$obj->id_persona_discapacidad,
                        'id_discapacidad'.$i=>$obj->id_discapacidad,
                        'codigo_conapdis'.$i=>$obj->codigo_conapdis,
                        'observacion'.$i=>$obj->observacion,
                        'discapacidad'.$i=>$obj->discapacidad,
                        'id_tipo_discapacidad'.$i=>$obj->id_tipo_discapacidad,
                        'tipo_discapacidad'.$i=>$obj->tipo_discapacidad
                      ];
        
              $this->view->disc[$i]=$disc[$i];
              $i++;
            }
            //var_dump($disc[1] , $disc[2]);
            $informacion_n = $this->model->getbyIdUsuarioInfoN($user->id_persona);
            $this->view->informacion_u = $informacion_u;
            $this->view->informacion_d = $informacion_d;
            $this->view->informacion_n = $informacion_n;
              $action="editUser";//variable para editar
              //enviar 
            }
            $this->view->action = $action;//enviar variable para identificar si el form va a editar o insertar
            $this->view->informacion = $informacion;
            $this->view->mensaje ="";
            ///////////////////////////////////////////////////////////////////////
        
             //tablas catalogo donde se utilizo una sola funcion para mostrar sus datos
            $generos=$this->model->getCatalogo("genero");
            $this->view->generos=$generos;
        
            $estadosCivil=$this->model->getCatalogo("estado_civil");
            $this->view->estadosCivil=$estadosCivil;
        
            $etnias=$this->model->getCatalogo("etnia");
            $this->view->etnias=$etnias;
        
            $documentoidentidad=$this->model->getCatalogo("documento_identidad_tipo");
            $this->view->documentoidentidad=$documentoidentidad;
        
            $domicilios=$this->model->getCatalogo("domicilio_detalle_tipo");
            $this->view->domicilios=$domicilios;
        
            $tcorreos=$this->model->getCatalogo("correo_tipo");
            $this->view->tcorreos=$tcorreos;
        
            $telefono_tipo=$this->model->getCatalogo("telefono_tipo");
            $this->view->telefono_tipo=$telefono_tipo;
        
            $cod_area_telefono=$this->model->getCatalogo("telefono_codigo_area");
            $this->view->cod_area_telefono=$cod_area_telefono;
            
            $perfiles=$this->model->getCatalogo('perfil');
            $this->view->perfiles=$perfiles;
            //////////////////////////////////////////////////////////////////
            $paises=$this->model->getPais();
            $this->view->paises=$paises;
        
            $nacionalidades=$this->model->getNacionalidad();
            $this->view->nacionalidades=$nacionalidades;
        
            $roles=$this->model->getCatalogo('rol');
            $this->view->roles=$roles;
        
            //docentes
            //obtener ejes regionales
            $ejes_regionales=$this->model->getEjeRegional();
            $this->view->ejes_regionales=$ejes_regionales;
        
            //obtener centros de estudio
            $centros=$this->model->getCentro();
            $this->view->centros=$centros;
            
            $aldeas_ubv=$this->model->getCatalogo("aldea_ubv");
            $this->view->aldeas_ubv=$aldeas_ubv;
        
            //obtener programas
            $programas=$this->model->getPrograma();
            $this->view->programas=$programas;
            //obtener preguntas
            $preguntas=$this->model->getPregunta();
            $this->view->preguntas=$preguntas;
        
            $dedicaciones=$this->model->getCatalogo("docente_dedicacion");
            $this->view->dedicaciones=$dedicaciones;
        
            $docente_estatus=$this->model->getCatalogo("docente_estatus");
            $this->view->docente_estatus=$docente_estatus;
        
            $clasificaciones=$this->model->getCatalogo("clasificacion_docente");
            $this->view->clasificaciones=$clasificaciones;
        
            $escalafones=$this->model->getCatalogo("escalafon");
            $this->view->escalafones=$escalafones;
        
            $tipo_discapacidades=$this->model->getCatalogo("tipo_discapacidad");
            $this->view->tipo_discapacidades=$tipo_discapacidades;
           
            $this->view->render('censo_docente/censo');
        }

        function verUsuarioInfo($param = null){
          $id_persona = $param[0];

          $this->view->id_persona=$id_persona;
          //trae informacion personal de usuario
          $informacion = $this->model->getbyIdUsuarioInfo($id_persona);
          $programas_docente = $this->model->getProgramasDocente($informacion->id_docente);
            $k=1;
            foreach($programas_docente as $row){
              $programa_docente=new Persona();
              $programa_docente=$row;
              $programa = $this->model->getProgramaDocentebyID($programa_docente->id_docente_programa);

              $doc_prog[$k]=[
                'id_docente_programa'.$k=>$programa->id_docente_programa,
                'id_programa'.$k=>$programa->id_programa,
                'programa'.$k=>$programa->programa
              ];
              $this->view->doc_prog[$k]=$doc_prog[$k];
              $k++;
            }
            $this->view->programas_docente = $programas_docente;
          //trae informacion de usuario
          $informacion_u = $this->model->getbyIdUsuarioInfoU($id_persona);
      
          $j=1;
          foreach($informacion_u as $row){
            $roles_u=new Persona();
            $roles_u=$row;
            $rol_u = $this->model->getRolbyID($id_persona, $roles_u->id_rol);
      
            $rol[$j]=[
              'id_usuario'.$j=>$rol_u->id_usuario,
              'id_rol'.$j=>$rol_u->id_rol,
              'rol'.$j=>$rol_u->rol
            ];
      
            $this->view->rol[$j]=$rol[$j];
            $j++;
      
          }
      
          //trae informacion de domicilio de la persona
          $domicilio_p = $this->model->getDomicilioPersona($id_persona);
          $this->view->domicilio_p=$domicilio_p;
      
          //trae las discapacidades
          $informacion_d = $this->model->getbyIdUsuarioInfoD($id_persona);
          $i=1;
          //var_dump("Cuenta >>",count($informacion_d));
      
          foreach($informacion_d as $row){
            $disc_u=new Persona;
            $disc_u=$row;
            $obj=$this->model->getbyIdDisc($disc_u->id_persona_discapacidad);
            
            $disc[$i]=[
                      'id_persona_discapacidad'.$i=>$obj->id_persona_discapacidad,
                      'id_discapacidad'.$i=>$obj->id_discapacidad,
                      'codigo_conapdis'.$i=>$obj->codigo_conapdis,
                      'observacion'.$i=>$obj->observacion,
                      'discapacidad'.$i=>$obj->discapacidad,
                      'id_tipo_discapacidad'.$i=>$obj->id_tipo_discapacidad,
                      'tipo_discapacidad'.$i=>$obj->tipo_discapacidad
                    ];
      
            $this->view->disc[$i]=$disc[$i];
            $i++;
          }
          //var_dump($disc[1] , $disc[2]);
          $informacion_n = $this->model->getbyIdUsuarioInfoN($id_persona);
      
          $this->view->informacion = $informacion;
          $this->view->informacion_u = $informacion_u;
          $this->view->informacion_d = $informacion_d;
          $this->view->informacion_n = $informacion_n;
          //$this->view->mensaje ="";
          ///////////////////////////////////////////////////////////////////////
      
           //tablas catalogo donde se utilizo una sola funcion para mostrar sus datos
          $generos=$this->model->getCatalogo("genero");
          $this->view->generos=$generos;
      
          $estadosCivil=$this->model->getCatalogo("estado_civil");
          $this->view->estadosCivil=$estadosCivil;
      
          $etnias=$this->model->getCatalogo("etnia");
          $this->view->etnias=$etnias;
      
          $documentoidentidad=$this->model->getCatalogo("documento_identidad_tipo");
          $this->view->documentoidentidad=$documentoidentidad;
      
          $domicilios=$this->model->getCatalogo("domicilio_detalle_tipo");
          $this->view->domicilios=$domicilios;
      
          $tcorreos=$this->model->getCatalogo("correo_tipo");
          $this->view->tcorreos=$tcorreos;
      
          $telefono_tipo=$this->model->getCatalogo("telefono_tipo");
          $this->view->telefono_tipo=$telefono_tipo;
      
          $cod_area_telefono=$this->model->getCatalogo("telefono_codigo_area");
          $this->view->cod_area_telefono=$cod_area_telefono;
          
          $perfiles=$this->model->getCatalogo('perfil');
          $this->view->perfiles=$perfiles;
          //////////////////////////////////////////////////////////////////
          $paises=$this->model->getPais();
          $this->view->paises=$paises;
      
          $nacionalidades=$this->model->getNacionalidad();
          $this->view->nacionalidades=$nacionalidades;
      
          $roles=$this->model->getCatalogo('rol');
          $this->view->roles=$roles;
      
          //docentes
          //obtener ejes regionales
          $ejes_regionales=$this->model->getEjeRegional();
          $this->view->ejes_regionales=$ejes_regionales;
      
          //obtener centros de estudio
          $centros=$this->model->getCentro();
          $this->view->centros=$centros;
      
          //obtener programas
          $programas=$this->model->getPrograma();
          $this->view->programas=$programas;
          //obtener preguntas
          $preguntas=$this->model->getPregunta();
          $this->view->preguntas=$preguntas;
      
          $dedicaciones=$this->model->getCatalogo("docente_dedicacion");
          $this->view->dedicaciones=$dedicaciones;
      
          $docente_estatus=$this->model->getCatalogo("docente_estatus");
          $this->view->docente_estatus=$docente_estatus;
      
          $clasificaciones=$this->model->getCatalogo("clasificacion_docente");
          $this->view->clasificaciones=$clasificaciones;
      
          $escalafones=$this->model->getCatalogo("escalafon");
          $this->view->escalafones=$escalafones;
      
          $tipo_discapacidades=$this->model->getCatalogo("tipo_discapacidad");
          $this->view->tipo_discapacidades=$tipo_discapacidades;
         
          //DATOS DE FAMILIARES
          $familiares=$this->model->verFamiliares($id_persona);
          $this->view->familiares=$familiares;
      
          
          $this->view->render('censo_docente/detailUser');
      
          
      }


        //editar usuario y censar
        function editUser($param){//se agrega esta linea
    
          //ids
          $id_persona=$param[0]; 
                      
          //tabla persona
          $pnombre=$_POST['pnombre'];            
          $snombre=$_POST['snombre'];
          $papellido=$_POST['papellido'];
          $sapellido=$_POST['sapellido'];
          $fnac=$_POST['fnac'];
          $estadoCivil=$_POST['estadocivil'];
          $genero=$_POST['genero'];
          $pais=$_POST['pais'];
              

          //persona etnia no es requerido
          $id_etnia=$_POST['etnia'];

          //tabla persona nacionalidad

          $id_documento_identidad_tipo=$_POST['tdocumento'];            
          $id_nacionalidad=$_POST['nacionalidad'];           
          $numero_documento=$_POST['ndocumento'];

          //persona Telefono
          $id_telefono_tipo=$_POST['tipotelf'];
          $id_telefono_codigo_area=$_POST['cod_area'];
          $telefono_numero=$_POST['numero'];

          //Domicilio persona
          // $id_ciudad=$_POST['ciudad'];
          $id_parroquia=$_POST['parroquia'];
          //domicilio detalle
          $direccion=$_POST['direccion'];

          //domicilio detalle_tipo
          $tipo_domicilio=$_POST['tdomicilio'];

          //persona_correo
          $id_correo_tipo=$_POST['id_correo_personal'];
          $correo=$_POST['correo_personal'];
          $id_persona_correo=$_POST['id_persona_correo'];

          if(!empty($_POST['correo_institucional'])){
            $id_persona_institucional=$_POST['id_persona_institucional'];
            $id_correo_institucional=$_POST['id_correo_institucional'];
            $correo_institucional=$_POST['correo_institucional'];
          }else{
            $id_persona_institucional="";
            $id_correo_institucional="";
            $correo_institucional="";
          }

          //persona discapacidad no es requerido
          $id_persona_discapacidad=$_POST['id_persona_discapacidad']; 
          $id_discapacidad=$_POST['discapacidad'];
          $codigo_conais=$_POST['conadis'];
          $observaciones=$_POST['observaciones'];

          //persona discapacidad no es requerido OTRA SEGUNDA DISCAPACIDAD
          $id_persona_discapacidad1=$_POST['id_persona_discapacidad1']; 
          $id_discapacidad1=$_POST['discapacidad1'];
          $observaciones1=$_POST['observaciones1'];

          //Perfil (aqui va un arreglo de roles)
          $id_rol=$_POST['roles'];

          $perfil=$_POST['perfil'];
          $id_perfil=$_POST['perfil'];
          
            $id_docente=$_POST['id_docente'];
            $programa=$_POST['programa'];
            $centro_estudio=$_POST['centro_estudio'];
            $eje_regional=$_POST['eje_regional'];
            $eje_municipal=$_POST['eje_municipal'];
            $estatus_d=$_POST['estatus_d'];
            $dedicacion=$_POST['dedicacion'];
            $clasificacion=$_POST['clasificacion'];
            $escalafon=$_POST['escalafon'];
            $aldea=$_POST['aldea'];
      
          $estatus=$_POST['estatus'];
          $password=$_POST['password'];

          //hijos y fecha de ingreso
          $fingreso=$_POST['fingreso'];
          $nhijos=$_POST['nhijos'];
          $npais=$_POST['paisnac'];

          //OBSERVACIONES 11/02/21

          $observaciones=$_POST['observacion'];

          
          $mensaje="";
  
          if($this->model->updateUser(['id_persona'=>$id_persona,'pnombre'=>$pnombre,'snombre'=>$snombre,'papellido'=>$papellido,'sapellido'=>$sapellido,
          'fnac'=>$fnac,'estadoCivil'=>$estadoCivil,'genero'=>$genero,'pais'=>$pais,
          'id_documento_identidad_tipo'=>$id_documento_identidad_tipo,'id_nacionalidad'=>$id_nacionalidad,'numero_documento'=>$numero_documento,                                  
          'id_telefono_tipo'=>$id_telefono_tipo,'id_telefono_codigo_area'=>$id_telefono_codigo_area,'telefono_numero'=>$telefono_numero,
          'id_parroquia'=>$id_parroquia,
          'direccion'=>$direccion,'tipo_domicilio'=>$tipo_domicilio,
          'id_correo_tipo'=>$id_correo_tipo,'correo'=>$correo,'id_etnia'=>$id_etnia,
          'id_persona_discapacidad'=>$id_persona_discapacidad,
          'id_discapacidad'=>$id_discapacidad,
          'codigo_conais'=>$codigo_conais,'observaciones'=>$observaciones,
          'id_persona_discapacidad1'=>$id_persona_discapacidad1,
          'id_discapacidad1'=>$id_discapacidad1,'observaciones1'=>$observaciones1,
          'id_rol'=>$id_rol,'perfil'=>$perfil,'id_perfil'=>$id_perfil,'estatus'=>$estatus,'password'=>$password,
          'nhijos'=>$nhijos,'fingreso'=>date("Y-m-d", strtotime($fingreso)), 'npais'=>$npais,
          'programa'=>$programa,'centro_estudio'=>$centro_estudio,'eje_regional'=>$eje_regional,'eje_municipal'=>$eje_municipal,
          'estatus_d'=>$estatus_d,'dedicacion'=>$dedicacion,'clasificacion'=>$clasificacion,
          'escalafon'=>$escalafon,'id_correo_institucional'=>$id_correo_institucional,'correo_institucional'=>$correo_institucional,
          'id_persona_correo'=>$id_persona_correo,'id_persona_institucional'=>$id_persona_institucional,'observaciones'=>$observaciones,
          'aldea'=>$aldea,'id_docente'=>$id_docente])){

       /*   $mensaje="<div class='alert alert-success alert-dismissable'>
          <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
          Usuario  Actualizado con Éxito su Usuario es: <b>ubv$numero_documento</b> y su contraseña es:<b> $password</b> <a class='alert-link' href='#'></a></div>";
        */
          $mensaje="<div class='alert alert-success alert-dismissable' style='font-size: 14.5px;'>
          <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
          Registro actualizado con exito, Los cambios no se haran efectivos hasta que no entregue los documentos que lo avalen. <br>
          Para Continuar con el registro de Familiares </br>  <a class='alert-link' href='".constant ('URL')."censo_docente/verUsuarioInfo/".$id_persona. "'>
          <button class='btn btn-primary btn-rounded' type='button'><i class='fa fa-edit'></i><span class='bold'> Click aqui </span></button>
          </a></div>";

            // enviar correos correspondientes

            $mensaje_correo="<p>
            ¡Felicidades! Sr.(a) ".$pnombre." ".$papellido." su Cuenta ha Sido Actualizada de manera 
            Exitosa en el Sistema Integrado de Desarrollo de Las Trabajadoras 
            y Los Trabajadores Académicos. Su Usuario de Acceso es: <b>ubv$numero_documento</b> y Su Clave de Acceso es: <b> $password</b> 
            <b>(SIDTA)</b>. 
            <br><br>
            </p>
            <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";

            $correo_usuario=new Correo();
                $var=$correo_usuario->EnviarCorreo([
                    'correo_destino'=>$correo,
                    'nombre_destino'=>$pnombre,
                    'apellido_destino'=>$papellido,
                    'asunto'=>'SIDTA - Actualización de Usuario',
                    'mensaje'=>$mensaje_correo
                    ]);

          }else{
          $mensaje="<div class='alert alert-danger alert-dismissable'>
          <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
          Ha Ocurrido un Error al Intentar Editar el Usuario <b>ubv$numero_documento</b>. <a class='alert-link' href='#'></a>
          </div>";
          
          }
          $this->view->mensaje=$mensaje;
          $this->render();
          
         // echo "Nuevo Alumno Creado";
          //$this->model->insert();//se agrega esta linea
        
        }

                  


          //Agregar familiar 10/02/21


          function familiar($param = null){
                      
              $cedula=$param[0];
              $cedper=$param[0];
              $id_persona=$param[1];
              $informacion = $this->model->getbyIdUsuarioInfo($id_persona);
              $this->view->informacion=$informacion;

              $this->view->cedper=$cedper;
              $this->view->id_persona=$id_persona;
              $this->view->render('censo_docente/addFamiliar');
              
          }

            
            function AddFamiliar($param){
              $cedper=$_POST['cedper'];
              /*$minorguniadm=$_POST['minorguniadm'];
              $ofiuniadm=$_POST['ofiuniadm'];
              $uniuniadm=$_POST['uniuniadm'];

              $param= array($minorguniadm,$ofiuniadm, $uniuniadm);*/
              $pnombre=$_POST['pnombre'];
              $snombre=$_POST['snombre'];
              $fec_nac=date("Y-m-d", strtotime($_POST['fec_nac']));
              $est_civil=$_POST['est_civil'];
              $papellido=$_POST['papellido'];
              $sapellido=$_POST['sapellido'];
              $nacionalidad=$_POST['nacionalidad'];
              $genero=$_POST['genero'];
              $parentesco=$_POST['parentesco'];
              $pais=$_POST['pais'];
              $estado=$_POST['estado'];
              $municipio=$_POST['municipio'];
              $parroquia=$_POST['parroquia'];
              $correo=$_POST['correo'];
              $telefono=$_POST['telefono'];
              $dir=$_POST['dir'];
              $ciudad=$_POST['ciudad'];
              $tipo_trabajador=$_POST['tipo_trabajador'];
              $cedfam=$_POST['cedfam'];
              if(empty($cedfam)){
                $cedfam=0;
              }

              $codper=$_POST['codper'];
              //18/01/21
              $estatus=$_POST['estatus'];
              $observacion=$_POST['observacion'];
              
              
              $mensaje="";
          
              //VALIDA SI EXITE UN FAMILIAR A TRAVES DE SU CEDULA
              if($var=$this->model->existeFamiliar($cedfam)){
                  $mensaje="<div class='alert alert-danger alert-dismissable'>
                  <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                  El familar con Ci. <b>" . $cedfam . "</b>  ya esta registrado <a class='alert-link' href='#'></a>
                  </div>";
                  $this->view->mensaje=$mensaje;
               
                  $this->verUsuarioInfo($param);
                  exit();
              }
              //VALIDA SI EXITE UN FAMILIAR SIN CEDULA
              if(empty($cedfam)){
              if($var=$this->model->existeFamiliar_nomb($pnombre,$papellido,$cedper)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El familar  <b>" . $pnombre ." ".$papellido. "</b>  ya esta registrado <a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
             
                $this->verUsuarioInfo($param);
                exit();
            }
          }



              
              if($this->model->insertFamiliar([
                  'cedper'=>$cedper,
                  'pnombre'=>$pnombre,
                  'snombre'=>$snombre,
                  'fec_nac'=>$fec_nac,
                  'est_civil'=>$est_civil,
                  'papellido'=>$papellido,
                  'sapellido'=>$sapellido,
                  'nacionalidad'=>$nacionalidad,
                  'genero'=>$genero,
                  'parentesco'=>$parentesco,
                  'pais'=>$pais,
                  'estado'=>$estado,
                  'municipio'=>$municipio,
                  'parroquia'=>$parroquia,
                  'correo'=>$correo,
                  'telefono'=>$telefono,
                  'dir'=>$dir,
                  'ciudad'=>$ciudad,
                  'tipo_trabajador'=>$tipo_trabajador,
                  'estatus'=>$estatus,
                  'observacion'=>$observacion,
                  'cedfam'=>$cedfam
                  ])){

              

              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Familiar Agregado Exitosamente<a class='alert-link' href='#'></a></div>";
              
              }else{
                  $mensaje="<div class='alert alert-danger alert-dismissable'>
                  <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                  Ha ocurrido un error al Agregar al Familiar<a class='alert-link' href='#'></a>
                  </div>";
              }
              $this->view->mensaje=$mensaje;
              //$this->render();
              $this->verUsuarioInfo($param);
          
            

          // $this->view->render('censo/ver_familiar');
          }





                //VER FAMILIAR
                function VerDetalleFamiliar($param = null){
                  $id_sno_familia = $param[0];
                  $id_per= $param[1];
                  $vista= $param[2];

                  $id_persona = $param[1];
                  $this->view->id_persona=$id_persona;

                  $informacion = $this->model->getbyIdUsuarioInfo($id_per);
                  $this->view->informacion=$informacion;

                  $persona=$this->model->detalleFamiliar($id_sno_familia);
                  $this->view->persona=$persona;
                 
                  //var_dump($persona);
                  if($vista=='1'){ //Opcion ver
                  $this->view->render('censo_docente/verFamiliar');
                  }else{ //Opcion edita 
                    $this->view->render('censo_docente/detalleFamiliar');
                  }
                }



              function ActualizarFamiliar($paramm){ //07/01/21
                $id_sno_familia=$_POST['id_sno_familia'];
                $param=$_POST['id_sno_familia'];
                $cedper=$_POST['cedper'];
                $minorguniadm=$_POST['minorguniadm'];
                $ofiuniadm=$_POST['ofiuniadm'];
                $uniuniadm=$_POST['uniuniadm'];

              // $param= array($minorguniadm,$ofiuniadm, $uniuniadm);
                $pnombre=$_POST['pnombre'];
                $snombre=$_POST['snombre'];
                $fec_nac=date("Y-m-d", strtotime($_POST['fec_nac']));
                $est_civil=$_POST['est_civil'];
                $papellido=$_POST['papellido'];
                $sapellido=$_POST['sapellido'];
                $nacionalidad=$_POST['nacionalidad'];
                $genero=$_POST['genero'];
                $parentesco=$_POST['parentesco'];
                $pais=$_POST['pais'];
                $estado=$_POST['estado'];
                $municipio=$_POST['municipio'];
                $parroquia=$_POST['parroquia'];
                $correo=$_POST['correo'];
                $telefono=$_POST['telefono'];
                $dir=$_POST['dir'];
                $ciudad=$_POST['ciudad'];
                $tipo_trabajador=$_POST['tipo_trabajador'];
                $cedfam=$_POST['cedfam'];
                if(empty($cedfam)){
                  $cedfam=0;
                }
                $codper=$_POST['codper']; 

                    //18/01/21
                    $estatus=$_POST['estatus'];
                    $observacion=$_POST['observacion'];
                
                $mensaje="";

                /*if($var=$this->model->existe($correo)){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                    El tipo  correo <b>" . $correo . "</b>  ya esta registrado <a class='alert-link' href='#'></a>
                    </div>";
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }*/
                
                if($this->model->UpdateFamiliar([
                    'id_sno_familia'=>$id_sno_familia,
                    'cedper'=>$cedper,
                    'pnombre'=>$pnombre,
                    'snombre'=>$snombre,
                    'fec_nac'=>$fec_nac,
                    'est_civil'=>$est_civil,
                    'papellido'=>$papellido,
                    'sapellido'=>$sapellido,
                    'nacionalidad'=>$nacionalidad,
                    'genero'=>$genero,
                    'parentesco'=>$parentesco,
                    'pais'=>$pais,
                    'estado'=>$estado,
                    'municipio'=>$municipio,
                    'parroquia'=>$parroquia,
                    'correo'=>$correo,
                    'telefono'=>$telefono,
                    'dir'=>$dir,
                    'ciudad'=>$ciudad,
                    'tipo_trabajador'=>$tipo_trabajador,
                    'cedfam'=>$cedfam,
                    'estatus'=>$estatus,
                    'observacion'=>$observacion
                    ])){

                

                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
                Familiar Actualizado Exitosamente<a class='alert-link' href='#'></a></div>";
                
                }else{
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                    Ha ocurrido un error al Actualizar al Familiar<a class='alert-link' href='#'></a>
                    </div>";
                }

                $this->view->mensaje=$mensaje;
                            //$this->render();
                $this->verUsuarioInfo($paramm);

              }


              //añadir usuario dentro de SIDTA Y censar
              function addUser(){

                //tabla persona
                $pnombre=$_POST['pnombre'];            
                $snombre=$_POST['snombre'];
                $papellido=$_POST['papellido'];
                $sapellido=$_POST['sapellido'];
                $fnac=$_POST['fnac'];
                $estadoCivil=$_POST['estadocivil'];
                $genero=$_POST['genero'];
                $pais=$_POST['pais'];
                //persona etnia no es requerido
                $id_etnia=$_POST['etnia'];

                //tabla persona nacionalidad
                $id_documento_identidad_tipo=$_POST['tdocumento'];            
                $id_nacionalidad=$_POST['nacionalidad'];           
                $numero_documento=$_POST['ndocumento'];

                //persona Telefono
                $id_telefono_tipo=$_POST['tipotelf'];
                $id_telefono_codigo_area=$_POST['cod_area'];
                $telefono_numero=$_POST['numero'];

                //Domicilio persona
                // $id_ciudad=$_POST['ciudad'];
                $id_parroquia=$_POST['parroquia'];
                //domicilio detalle
                $direccion=$_POST['direccion'];

                //domicilio detalle_tipo
                $tipo_domicilio=$_POST['tdomicilio'];


                $correo_personal=$_POST['correo_personal'];
                $correo_institucional=$_POST['correo_institucional'];
                // EJE 
                $eje_municipal=$_POST['eje_municipal'];


                //persona discapacidad no es requerido
                $id_discapacidad=$_POST['discapacidad'];
                $codigo_conais=$_POST['conadis'];
                $observaciones=$_POST['observaciones'];

                //persona discapacidad no es requerido OTRA SEGUNDA DISCAPACIDAD
                $id_discapacidad1=$_POST['discapacidad1'];
                $observaciones1=$_POST['observaciones1'];

                //Perfil (aqui va un areglo de roles)

                $id_rol=[3, 4, 6];
                $id_perfil=1;//aqui solo hay trabajadores academicos

                $programa=$_POST['programa'];
                $centro_estudio=$_POST['centro_estudio'];
                $eje_regional=$_POST['eje_regional'];
                $estatus_d=$_POST['estatus_d'];
                $dedicacion=$_POST['dedicacion'];
                $clasificacion=$_POST['clasificacion'];
                $escalafon=$_POST['escalafon'];
                $aldea=$_POST['aldea'];
                
                
                $estatus=1;//para crear el usuario

                //hijos y fecha de ingreso
                $fingreso=$_POST['fingreso'];
                $nhijos=$_POST['nhijos'];

                
                     
                $mensaje="";

                if($var=$this->model->existe($numero_documento)){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                    Usuario con Identificación <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
                    </div>";
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }
                //editar nomina_docente_sigad

                if($this->model->insertUser(['pnombre'=>$pnombre,'snombre'=>$snombre,'papellido'=>$papellido,'sapellido'=>$sapellido,
                                        'fnac'=>$fnac,'estadoCivil'=>$estadoCivil,'genero'=>$genero,'pais'=>$pais,
                                        'id_documento_identidad_tipo'=>$id_documento_identidad_tipo,'id_nacionalidad'=>$id_nacionalidad,'numero_documento'=>$numero_documento,                                  
                                        'id_telefono_tipo'=>$id_telefono_tipo,'id_telefono_codigo_area'=>$id_telefono_codigo_area,'telefono_numero'=>$telefono_numero,
                                        'id_parroquia'=>$id_parroquia,
                                        'direccion'=>$direccion,'tipo_domicilio'=>$tipo_domicilio,'id_etnia'=>$id_etnia,
                                        'id_discapacidad'=>$id_discapacidad,
                                        'codigo_conais'=>$codigo_conais,'observaciones'=>$observaciones,
                                        'id_discapacidad1'=>$id_discapacidad1,'observaciones1'=>$observaciones1,
                                        'id_rol'=>$id_rol,'id_perfil'=>$id_perfil,'estatus'=>$estatus,
                                        'fecha_ingreso'=>$fingreso,'numero_hijos'=>$nhijos,
                                        'programa'=>$programa,'centro_estudio'=>$centro_estudio,'eje_regional'=>$eje_regional,
                                        'estatus_d'=>$estatus_d,'dedicacion'=>$dedicacion,'clasificacion'=>$clasificacion,
                                        'escalafon'=>$escalafon,'correo_personal'=>$correo_personal,'correo_institucional'=>$correo_institucional,
                                        'eje_municipal'=>$eje_municipal,'aldea'=>$aldea, 'observaciones'=>$observaciones])){

                  //Obtenemos el id persona a partir de la cedula
                  $id_persona_dcedula=$this->model->cedula($numero_documento);
                  

                  $mensaje="<div class='alert alert-success alert-dismissable' style='font-size: 14.5px;'>
                  <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
                  Registro actualizado con exito, El Usuario de Acceso es: <b>ubv$numero_documento</b> y Su Clave de Acceso es:<b> $numero_documento</b>
                  Los cambios no se haran efectivos hasta que no entregue los documentos que lo avalen. <br>
                  Para Continuar con el registro de Familiares </br>  <a class='alert-link' href='".constant ('URL')."censo_docente/verUsuarioInfo/".$id_persona_dcedula. "'>
                  <button class='btn btn-primary btn-rounded' type='button'><i class='fa fa-edit'></i><span class='bold'> Click aqui </span></button>
                  </a></div>";
                  // enviar correos correspondientes

                  $mensaje_correo="<p>
                  ¡Felicidades! Sr.(a) ".$pnombre." ".$papellido." su Cuenta ha Sido Registrada de manera 
                  Exitosa en el Sistema Integrado de Desarrollo de Las Trabajadoras 
                  y Los Trabajadores Académicos. Su Usuario de Acceso es: <b>ubv$numero_documento</b> y Su Clave de Acceso es: <b> $numero_documento</b>
                  Usted Ha sido censado Exitosamente como trabajador de la UBV
                  <b>(SIDTA)</b>. 
                  <br><br>
                  </p>
                  <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";

                  $correo_usuario=new Correo();
                      $var=$correo_usuario->EnviarCorreo([
                          'correo_destino'=>$correo_personal,
                          'nombre_destino'=>$pnombre,
                          'apellido_destino'=>$papellido,
                          'asunto'=>'SIDTA - Registro de Usuario',
                          'mensaje'=>$mensaje_correo
                          ]);


              }else{
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                    Ha ocurrido un Error al agregar un Usuario<a class='alert-link' href='#'></a>
                </div>";
                }
                $this->view->mensaje=$mensaje;
                $this->render();
                
              }

}//end-class




    

?>