    <?php
include_once 'sesiones/session_admin.php';

    class Frecuencia extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->mensaje="";
            $this->view->frecuencias=[];

        }

        function render(){

            $frecuencias=$this->model->getFrecuencia();
            $this->view->frecuencias=$frecuencias;
            $this->view->render('adm_datos_academicos/frecuencia');
        
        }

        function registrarFrecuencia(){

            if(isset($_POST['registrar'])){
                $descripcion=$_POST['descripcion'];
                
                
                if(empty($descripcion)){
                    $mensaje='<div class="alert alert-danger">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                El campo descripción es . <a class="alert-link" href="#">Requerido</a>.
                            </div>';
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }
                
                if($var=$this->model->existe($descripcion)){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                     La Frecuencia <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                    </div>";
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }

                if($this->model->insert([
                    'descripcion'=>$descripcion,
                    ])){
                        $mensaje='<div class="alert alert-success">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                La Frecuencia  '.$descripcion.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                            </div>';
                    }else{
                        $mensaje="<div class='alert alert-danger alert-dismissable'>
                            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                            Ha ocurrido un error al registrar la<a class='alert-link' href='#'> frecuencia</a>
                        </div>";
                    }

                $this->view->mensaje=$mensaje;
                $this->render();
            }
            
        }

        function editarFrecuencia(){

            if(isset($_POST['registrar2'])){

                $descripcion2=$_POST['descripcion2'];
                $id_frecuencia=$_POST['id_frecuencia'];

                if(empty($descripcion2)){
                    $mensaje='<div class="alert alert-danger">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                El campo Frecuencia es . <a class="alert-link" href="#">Requerido</a>.
                            </div>';
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }elseif(empty($id_frecuencia)){
                    $mensaje='<div class="alert alert-danger">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    El campo Nivel académico es <a class="alert-link" href="#">Requerido</a>.
                    </div>';
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }

                if($this->model->update([
                    'descripcion'=>$descripcion2,
                    'id_frecuencia'=>$id_frecuencia
                    ])){    

              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              El grado <b> ".$descripcion2." </b> fue Actualizado <a class='alert-link' href='#'>Correctamente</a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar el grado <b> ".$descripcion2." </b><a class='alert-link' href='#'></a>
            </div>";
            }

            }

            $this->view->mensaje=$mensaje;
            $this->render();
        }

        function removerFrecuencia($param=null){
            
            
            $id_frecuencia=$param[0];
            
            if($this->model->delete($id_frecuencia)){
                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Frecuencia Eliminado<a class='alert-link' href='#'> Correctamente</a>
                </div>";
               
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al tratar de eliminar el <a class='alert-link' href='#'>Frecuencia</a>
                </div>";
            }
            echo $mensaje;
           
        }
        

    }

    ?>