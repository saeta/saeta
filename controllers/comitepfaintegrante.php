<?php
include_once 'models/datosacademicos/programa_formacion.php';
include_once 'models/datosacademicos/docente.php';
include_once 'models/datosacademicos/comitetipo.php';

include_once 'sesiones/session_admin.php';

class Comitepfaintegrante extends Controller{
    function __construct(){
        parent::__construct();
        $this->view->mensaje="";
        $this->view->programas=[];
        $this->view->docentes=[];
        $this->view->tipos=[];
        $this->view->comitepfas=[];
        
       
    }

    function render(){
        $programas=$this->model->getProgramaF();
        $this->view->programas=$programas;
        $docentes=$this->model->getDocente();
        $this->view->docentes=$docentes;
        $tipos=$this->model->getComitetipo();
        $this->view->tipos=$tipos;
        $comitepfas=$this->model->getComitepfa();
        $this->view->comitepfas=$comitepfas;
        $this->view->render('adm_datos_academicos/comitepfa');
    }

    function registrarComitepfa(){

        if(isset($_POST['registrar'])){
            $fecha_inicio=$_POST['fecha_inicio'];
            $fecha_fin=$_POST['fecha_fin'];
            if($_POST['estatus'] == 'Activo'){
                $estatus=1;
            }elseif($_POST['estatus'] == 'Inactivo'){
                $estatus=0;
            }
            $id_programa=$_POST['id_programa'];
            $id_docente=$_POST['id_docente'];
            $id_comite_tipo=$_POST['id_comite_tipo'];
            
            
            if($var=$this->model->existe($id_comitepfa_integrante)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                 El Integrante del comite pfa <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }

            if($this->model->insert([
                'fecha_inicio'=>$fecha_inicio,
                'fecha_fin'=>$fecha_fin,
                'estatus'=>$estatus,
                'id_programa'=>$id_programa,
                'id_docente'=>$id_docente,
                'id_comite_tipo'=>$id_comite_tipo
                
                
                ])){
                    $mensaje='<div class="alert alert-success">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Integrante del comite pfa  '.$descripcion_docente.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                        </div>';
                }else{
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                        Ha ocurrido un error al registrar al Integrante del comite pfa<a class='alert-link' href='#'></a>
                    </div>";
                }

            $this->view->mensaje=$mensaje;
            $this->render();
        }
        
    }

    function editarComitepfa(){

        if(isset($_POST['registrar2'])){

            $id_comitepfa_integrante=$_POST['id_comitepfa_integrante'];
            $fecha_inicio2=$_POST['fecha_inicio2'];
            $fecha_fin2=$_POST['fecha_fin2'];
            //$estatus2=$_POST['estatus2'];
            if($_POST['estatus2'] == 'Activo'){
                $estatus2=1;
            }elseif($_POST['estatus2'] == 'Inactivo'){
                $estatus2=0;
            }
            $id_programa2=$_POST['id_programa2'];
            $id_docente2=$_POST['id_docente2'];
            $id_comite_tipo2=$_POST['id_comite_tipo2'];
            

            if(empty($descripcion_docente2)){
                $mensaje='<div class="alert alert-danger">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            El Integrante del comite pfa es . <a class="alert-link" href="#">Requerido</a>.
                        </div>';
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }

            if($this->model->update([
                'id_comitepfa_integrante'=>$id_comitepfa_integrante,
                'fecha_inicio'=>$fecha_inicio2,
                'fecha_fin'=>$fecha_fin2,
                'estatus'=>$estatus2,
                'id_programa'=>$id_programa2,
                'id_docente'=>$id_docente2,
                'id_comite_tipo'=>$id_comite_tipo2
                

                ])){    

          $mensaje="<div class='alert alert-success alert-dismissable'>
          <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
          El Integrante del comite pfa <b> ".$descripcion_docente2." </b> fue Actualizada <a class='alert-link' href='#'>Correctamente</a></div>";
        
      }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Ha ocurrido un error al Actualizar al Integrante del comite pfa <b> ".$descripcion_docente2." </b><a class='alert-link' href='#'></a>
        </div>";
        }

        }

        $this->view->mensaje=$mensaje;
        $this->render();
    }


      function removerComitepfa($param=null){
        
        $id_comitepfa_integrante=$param[0];
  
        if($this->model->delete($id_comitepfa_integrante)){

            $mensaje="<div class='alert alert-success alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
           Removido correctamente la malla curricular<b>  </b><a class='alert-link' href='#'></a>
        </div>";

       }else{
        $mensaje="<div class='alert alert-danger alert-dismissable'>
        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
       No se pudo remover la malla curricular<b> </b><a class='alert-link' href='#'></a>
        </div>";
 
       }
       echo $mensaje;
    }

    function cambiarEstatus($param=null){
            
            
        $id_comitepfa_integrante=$param[0];
        
        if($this->model->delete($id_comitepfa_integrante)){
            $mensaje="<div class='alert alert-success alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Malla Eliminada<a class='alert-link' href='#'> Correctamente</a>
            </div>";
           
        }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Ha ocurrido un error al tratar de eliminar la <a class='alert-link' href='#'>Malla</a>
            </div>";
        }
        echo $mensaje;
       
    }

}
?>