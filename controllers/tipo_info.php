    <?php
include_once 'models/datosacademicos/tipo_info.php';
include_once 'sesiones/session_admin.php';

    class Tipo_info extends Controller {
        function __construct(){
            parent::__construct();
            $this->view->mensaje="";
            $this->view->infos=[];

        }

        function render(){

            $infos=$this->model->getInfo();
            $this->view->infos=$infos;
            $this->view->render('socio-academica/tipo_info');
        
        }

        function registrarInfo(){

            if(isset($_POST['registrar'])){
                $descripcion=$_POST['descripcion'];
                
                
                if(empty($descripcion)){
                    $mensaje='<div class="alert alert-danger">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                El campo descripción es . <a class="alert-link" href="#">Requerido</a>.
                            </div>';
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }
                
                if($var=$this->model->existe($descripcion)){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                     El Tipo de Unidad Curricular <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                    </div>";
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }

                if($this->model->insert([
                    'descripcion'=>$descripcion,
                    ])){
                        $mensaje='<div class="alert alert-sinfocess">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                Tipo de Unidad Curricular  '.$descripcion.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                            </div>';
                    }else{
                        $mensaje="<div class='alert alert-danger alert-dismissable'>
                            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                            Ha ocurrido un error al registrar Páis<a class='alert-link' href='#'></a>
                        </div>";
                    }

                $this->view->mensaje=$mensaje;
                $this->render();
            }
            
        }

        function editarInfo(){

            if(isset($_POST['registrar2'])){

                $descripcion2=$_POST['descripcion2'];
                $id_tipo_info=$_POST['id_tipo_info'];

                if(empty($descripcion2)){
                    $mensaje='<div class="alert alert-danger">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                El campo Tipo de Unidad Curricular es . <a class="alert-link" href="#">Requerido</a>.
                            </div>';
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }

                if($this->model->update([
                    'descripcion'=>$descripcion2,
                    'id_tipo_info'=>$id_tipo_info
                    ])){    

              $mensaje="<div class='alert alert-sinfocess alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              El grado <b> ".$descripcion2." </b> fue Actualizado <a class='alert-link' href='#'>Correctamente</a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar el Tipo de Unidad Curricular <b> ".$descripcion2." </b><a class='alert-link' href='#'></a>
            </div>";
            }

            }

            $this->view->mensaje=$mensaje;
            $this->render();
        }

        function removerInfo($param=null){
            
            
            $id_tipo_info=$param[0];
            
            if($this->model->delete($id_tipo_info)){
                $mensaje="<div class='alert alert-sinfocess alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Tipo de Unidad Curricular Eliminado<a class='alert-link' href='#'> Correctamente</a>
                </div>";
               
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al tratar de eliminar el <a class='alert-link' href='#'>Tipo de Unidad Curricular</a>
                </div>";
            }
            echo $mensaje;
           
        }
        

    }

    ?>