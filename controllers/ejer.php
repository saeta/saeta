<?php
include_once 'sesiones/session_admin.php';

    class Ejer extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->ejesRegionales=[];
        }

        function render(){ 
            $ejesRegionales=$this->model->get();
            $this->view->ejesRegionales=$ejesRegionales;
            $this->view->render('estructura/ejer');        
        }



        function registrarEjeRegional(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
    
            $codigoine=$_POST['codigoine'];
            $ejeregional=$_POST['ejeregional'];
            $estatus=$_POST['estatus'];
            
            $mensaje="";


            if($var=$this->model->existe($codigoine)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Eje Regional <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
    
    
            if($this->model->insert(['codigoine'=>$codigoine,'ejeregional'=>$ejeregional,'estatus'=>$estatus])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Eje Regional registrado<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al registrar Eje Regional<a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }

          function ActualizarEjeRegional(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
    
            $id_eje_regional=$_POST['id_eje_regional'];
            $codigoEje=$_POST['codigoEje'];
            $eje=$_POST['eje'];
            $estatus=$_POST['estatusEje'];
            
            //var_dump( $codigoine,$siglasine);
            $mensaje="";


           
    
            if($this->model->update(['id_eje_regional'=>$id_eje_regional,'codigoEje'=>$codigoEje,'eje'=>$eje,'estatus'=>$estatus])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Eje Regional <b> ".$eje." </b>Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar  Eje Regional <b> ".$eje." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }


    }

?>