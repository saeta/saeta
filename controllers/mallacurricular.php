<?php
include_once 'models/datosacademicos/modalidadmalla.php';
include_once 'models/datosacademicos/programa_formacion.php';
include_once 'sesiones/session_admin.php';

class Mallacurricular extends Controller{
    function __construct(){
        parent::__construct();
        $this->view->mensaje="";
        $this->view->modalidades=[];
        $this->view->programas=[];
        $this->view->mallas=[];
        
       
    }

    function render(){
        $modalidades=$this->model->getModalidadM();
        $this->view->modalidades=$modalidades;
        $programas=$this->model->getProgramaF();
        $this->view->programas=$programas;
        $mallas=$this->model->getMalla();
        $this->view->mallas=$mallas;
        $this->view->render('adm_datos_academicos/mallacurricular');
    }

    function registrarMalla(){

        if(isset($_POST['registrar'])){
            $descripcion=$_POST['descripcion'];
            $codigo=$_POST['codigo'];
            if($_POST['salida_intermedia'] == 'Activo'){
                $salida_intermedia=1;
            }elseif($_POST['salida_intermedia'] == 'Inactivo'){
                $salida_intermedia=0;
            }
            $id_modalidad_malla=$_POST['id_modalidad_malla'];
            $id_programa=$_POST['id_programa'];
            if($_POST['estatus'] == 'Activo'){
                $estatus=1;
            }elseif($_POST['estatus'] == 'Inactivo'){
                $estatus=0;
            }

            if(empty($descripcion)){
                $mensaje='<div class="alert alert-danger">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            El campo malla curricular es . <a class="alert-link" href="#">Requerido</a>.
                        </div>';
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($var=$this->model->existe($descripcion)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                 La Malla curricular <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }

            if($this->model->insert([
                'descripcion'=>$descripcion,
                'codigo'=>$codigo,
                'salida_intermedia'=>$salida_intermedia,
                'id_modalidad_malla'=>$id_modalidad_malla,
                'id_programa'=>$id_programa,
                'estatus'=>$estatus
                
                ])){
                    $mensaje='<div class="alert alert-success">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Malla Curricular  '.$descripcion.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                        </div>';
                }else{
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                        Ha ocurrido un error al registrar la Malla Curricular<a class='alert-link' href='#'></a>
                    </div>";
                }

            $this->view->mensaje=$mensaje;
            $this->render();
        }
        
    }

    function editarMalla(){

        if(isset($_POST['registrar2'])){

            $id_malla_curricular=$_POST['id_malla_curricular'];
            $descripcion2=$_POST['descripcion2'];
            $codigo2=$_POST['codigo2'];
            if($_POST['salida_intermedia2'] == 'Activo'){
                $salida_intermedia2=1;
            }elseif($_POST['salida_intermedia2'] == 'Inactivo'){
                $salida_intermedia2=0;
            }
            $id_modalidad_malla2=$_POST['id_modalidad_malla2'];
            $id_programa2=$_POST['id_programa2'];
            //$estatus2=$_POST['estatus2'];
            if($_POST['estatus2'] == 'Activo'){
                $estatus2=1;
            }elseif($_POST['estatus2'] == 'Inactivo'){
                $estatus2=0;
            }

            if(empty($descripcion2)){
                $mensaje='<div class="alert alert-danger">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            El campo malla curricular es . <a class="alert-link" href="#">Requerido</a>.
                        </div>';
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }

            if($this->model->update([
                'id_malla_curricular'=>$id_malla_curricular,
                'descripcion'=>$descripcion2,
                'codigo'=>$codigo2,
                'salida_intermedia'=>$salida_intermedia2,
                'id_modalidad_malla'=>$id_modalidad_malla2,
                'id_programa'=>$id_programa2,
                'estatus'=>$estatus2

                ])){    

          $mensaje="<div class='alert alert-success alert-dismissable'>
          <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
          La Malla Curricular <b> ".$descripcion2." </b> fue Actualizada <a class='alert-link' href='#'>Correctamente</a></div>";
        
      }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Ha ocurrido un error al Actualizar la malla <b> ".$descripcion2." </b><a class='alert-link' href='#'></a>
        </div>";
        }

        }

        $this->view->mensaje=$mensaje;
        $this->render();
    }


      function removerMalla($param=null){
        
        $id_malla_curricular=$param[0];
  
        if($this->model->delete($id_malla_curricular)){

            $mensaje="<div class='alert alert-success alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
           Removido correctamente la malla curricular<b>  </b><a class='alert-link' href='#'></a>
        </div>";

       }else{
        $mensaje="<div class='alert alert-danger alert-dismissable'>
        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
       No se pudo remover la malla curricular<b> </b><a class='alert-link' href='#'></a>
        </div>";
 
       }
       echo $mensaje;
    }

    function cambiarEstatus($param=null){
            
            
        $id_malla_curricular=$param[0];
        
        if($this->model->delete($id_malla_curricular)){
            $mensaje="<div class='alert alert-success alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Malla Eliminada<a class='alert-link' href='#'> Correctamente</a>
            </div>";
           
        }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Ha ocurrido un error al tratar de eliminar la <a class='alert-link' href='#'>Malla</a>
            </div>";
        }
        echo $mensaje;
       
    }

}
?>