    <?php
include_once 'sesiones/session_admin.php';

    class Dedicacion extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->mensaje="";
            $this->view->dedicaciones=[];

        }

        function render(){

            $dedicaciones=$this->model->get();
            $this->view->dedicaciones=$dedicaciones;
            $this->view->render('adm_datos_academicos/dedicacion');
        
        }

        function registrar(){

            if(isset($_POST['registrar'])){
                
                $descripcion=$_POST['descripcion'];
                $horas_totales=$_POST['horas_totales'];
                $horas_minimas=$_POST['horas_minimas'];
                $horas_maximas=$_POST['horas_maximas'];

                
                if($var=$this->model->existe($descripcion)){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                     La dedicación  <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                    </div>";
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }

                if($horas_minimas > $horas_maximas){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                     El valor de <b>horas mínimas</b> no puede ser mayor que el valor de <a class='alert-link' href='#'>horas máximas.</a>
                    </div>";
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }elseif($horas_minimas > $horas_totales || $horas_maximas > $horas_totales){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                     El valor de los campos de <b>horas mínimas</b> ú <b>horas máximas</b> no puede ser mayor que el valor de <a class='alert-link' href='#'>horas totales.</a>
                    </div>";
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }

                if($this->model->insert([
                    'descripcion'=>$descripcion,
                    'horas_totales'=>$horas_totales,
                    'horas_minimas'=>$horas_minimas,
                    'horas_maximas'=>$horas_maximas
                    ])){
                        $mensaje='<div class="alert alert-success">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                Turno  '.$descripcion.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                            </div>';
                    }else{
                        $mensaje="<div class='alert alert-danger alert-dismissable'>
                            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                            Ha ocurrido un error al registrar la <a class='alert-link' href='#'>dedicación</a>
                        </div>";
                    }

                $this->view->mensaje=$mensaje;
                $this->render();
            }
            
        }

        function editar(){

            if(isset($_POST['registrar2'])){
                $id_docente_dedicacion=$_POST['id_docente_dedicacion'];
                //var_dump($id_docente_dedicacion);
                $descripcion=$_POST['descripcion1'];
                $horas_totales=$_POST['horas_totales1'];
                $horas_minimas=$_POST['horas_minimas1'];
                $horas_maximas=$_POST['horas_maximas1'];
                
                if($horas_minimas > $horas_maximas){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                     El valor de <b>horas mínimas</b> no puede ser mayor que el valor de <a class='alert-link' href='#'>horas máximas.</a>
                    </div>";
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }elseif($horas_minimas > $horas_totales || $horas_maximas > $horas_totales){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                     El valor de los campos de <b>horas mínimas</b> ú <b>horas máximas</b> no puede ser mayor que el valor de <a class='alert-link' href='#'>horas totales.</a>
                    </div>";
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }

                if($this->model->update([
                    'descripcion'=>$descripcion,
                    'horas_totales'=>$horas_totales,
                    'horas_minimas'=>$horas_minimas,
                    'horas_maximas'=>$horas_maximas,
                    'id_docente_dedicacion'=>$id_docente_dedicacion
                    ])){    

              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              El grado <b> ".$descripcion2." </b> fue Actualizado <a class='alert-link' href='#'>Correctamente</a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar el grado <b> ".$descripcion2." </b><a class='alert-link' href='#'></a>
            </div>";
            }

            }

            $this->view->mensaje=$mensaje;
            $this->render();
        }

        function remover($param=null){
            
            
            $id_turno=$param[0];
            
            if($this->model->delete($id_turno)){
                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Turno Eliminado<a class='alert-link' href='#'> Correctamente</a>
                </div>";
               
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al tratar de eliminar el <a class='alert-link' href='#'>Turno</a>
                </div>";
            }
            echo $mensaje;
           
        }
        

    }

    ?>