    <?php
include_once 'sesiones/session_admin.php';

    class Programa_nivel extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->mensaje="";
            $this->view->programas=[];

        }

        function render(){

            $programas=$this->model->get();
            $this->view->programas=$programas;
            $this->view->render('adm_datos_academicos/programa_nivel');

        }

        function registrar(){

            if(isset($_POST['registrar'])){
                
                $descripcion=$_POST['descripcion'];
                if($_POST['estatus'] == 'Activo'){
                    $estatus=true;
                }elseif($_POST['estatus'] == 'Inactivo'){
                    $estatus=0;
                }
                
                $codigo=$_POST['codigo'];

                if($var=$this->model->existe($descripcion)){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                     El Nivel del Programa  <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                    </div>";
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }

                if($this->model->insert([
                    'descripcion'=>$descripcion,
                    'estatus'=>$estatus,
                    'codigo'=>$codigo
                    ])){
                        $mensaje='<div class="alert alert-success">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                El Nivel del Programa  '.$descripcion.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                            </div>';
                    }else{
                        $mensaje="<div class='alert alert-danger alert-dismissable'>
                            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                            Ha ocurrido un error al registrar el <a class='alert-link' href='#'>Nivel del Programa</a>
                        </div>";
                    }

                $this->view->mensaje=$mensaje;
                $this->render();
            }
            
        }

        function editar(){

            if(isset($_POST['registrar2'])){
                $id_programa_nivel=$_POST['id_programa_nivel'];
                //var_dump($id_programa_nivel);
                $descripcion=$_POST['descripcion2'];
                
                if($_POST['estatus1'] == 'Activo'){
                    $estatus=1;
                }elseif($_POST['estatus1'] == 'Inactivo'){
                    $estatus=0;
                }
                //var_dump($_POST['codigo2']);
                $codigo=$_POST['codigo2'];

                if($this->model->update([
                    'descripcion'=>$descripcion,
                    'estatus1'=>$estatus,
                    'codigo'=>$codigo,
                    'id_programa_nivel'=>$id_programa_nivel
                    ])){    

              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              El Nivel del Programa <b> ".$descripcion2." </b> fue Actualizado <a class='alert-link' href='#'>Correctamente</a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar el Nivel del Programa <b> ".$descripcion2." </b><a class='alert-link' href='#'></a>
            </div>";
            }

            }

            $this->view->mensaje=$mensaje;
            $this->render();
        }

        function remover($param=null){
            
            
            $id_turno=$param[0];
            
            if($this->model->delete($id_turno)){
                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Nivel del Programa Eliminado<a class='alert-link' href='#'> Correctamente</a>
                </div>";
               
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al tratar de eliminar el <a class='alert-link' href='#'>Nivel del Programa</a>
                </div>";
            }
            echo $mensaje;
           
        }
        

    }

    ?>