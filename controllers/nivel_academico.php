    <?php
include_once 'sesiones/session_admin.php';

    class Nivel_academico extends Controller {
        function __construct(){
            parent::__construct();
            
            $this->view->mensaje="";
            $this->view->niveles=[];

        }

        function render(){

            $niveles=$this->model->get();
            $this->view->niveles=$niveles;
            $this->view->render('adm_datos_academicos/nivel_academico');
        
        }

        function registrar(){

            if(isset($_POST['registrar'])){
                $descripcion=$_POST['descripcion'];
                $codigo_ine=$_POST['codigo_ine'];

                if(empty($descripcion)){
                    $mensaje='<div class="alert alert-danger">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                El campo Nivel Académico es . <a class="alert-link" href="#">Requerido</a>.
                            </div>';
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }

                if($var=$this->model->existe($descripcion)){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                     El Nivel Académico <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                    </div>";
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }

                if($this->model->insert([
                    'descripcion'=>$descripcion,
                    'codigo_ine'=>$codigo_ine
                    ])){
                        $mensaje='<div class="alert alert-success">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                Nivel Académico  '.$descripcion.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                            </div>';
                    }else{
                        $mensaje="<div class='alert alert-danger alert-dismissable'>
                            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                            Ha ocurrido un error al registrar el Nivel Académico<a class='alert-link' href='#'></a>
                        </div>";
                    }

                $this->view->mensaje=$mensaje;
                $this->render();
            }
            
        }

        function editar(){

            if(isset($_POST['registrar2'])){
                $id_nivel_academico=$_POST['id_nivel_academico'];
                $descripcion2=$_POST['descripcion2'];
                $codigo_ine2=$_POST['codigo_ine2'];

                if(empty($descripcion2)){
                    $mensaje='<div class="alert alert-danger">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                El campo Nivel Académico es . <a class="alert-link" href="#">Requerido</a>.
                            </div>';
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }

                if($this->model->update([
                    'id_nivel_academico'=>$id_nivel_academico,
                    'descripcion'=>$descripcion2,
                    'codigo_ine'=>$codigo_ine2
                    ])){    

              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              El Nivel Académico <b> ".$descripcion2." </b> fue Actualizado <a class='alert-link' href='#'>Correctamente</a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar el Nivel Académico <b> ".$descripcion2." </b><a class='alert-link' href='#'></a>
            </div>";
            }

            }

            $this->view->mensaje=$mensaje;
            $this->render();
        }
        
        function remover($param=null){
            
            
            $id_nivel_academico=$param[0];
            
            if($this->model->delete($id_nivel_academico)){
                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Nivel Académico Eliminado<a class='alert-link' href='#'> Correctamente</a>
                </div>";
               
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al tratar de eliminar el <a class='alert-link' href='#'>Nivel Académico</a>
                </div>";
            }
            echo $mensaje;
           
        }
        

    }

    ?>