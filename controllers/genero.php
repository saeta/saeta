<?php
include_once 'sesiones/session_admin.php';

    class Genero extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->generos=[];
  
        }

        function render(){ 
            $generos=$this->model->get();
            $this->view->generos=$generos;
            $this->view->render('persona/genero');        
        }



        function registrargenero(){//se agrega esta linea  
            //$id_persona=$_SESSION["id_persona"];
    
            $genero=$_POST['genero'];
            $codigo=$_POST['codigo'];

       //var_dump($genero);
        //var_dump($codigo);   
          

            $mensaje="";

            if($var=$this->model->existe($genero)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El tipo  genero <b>" . $genero . "</b>  ya esta registrado <a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($this->model->insert(['genero'=>$genero,'codigo'=>$codigo,'id_genero'=>$id_genero])){
           
               
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              genero agregado<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al agregar una genero <a class='alert-link' href='#'></a>

            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }
          



          function Actualizargenero(){//se agrega esta linea
          
            //$id_persona=$_SESSION["id_persona"];
            $genero2=$_POST['genero2'];
            $id_genero2=$_POST['id_genero2'];
            $codigo2=$_POST['codigo2'];
            
           // var_dump($id_genero2);
           // var_dump($genero2);
      //break;
            //var_dump( $codigoine,$siglasine);
            $mensaje="";
           /* if($var=$this->model->existe($genero2)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El tipo  genero <b>" . $genero2 . "</b>  ya esta registrado <a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }*/

            if($this->model->update(['genero'=>$genero2,'codigo'=>$codigo2,'id_genero'=>$id_genero2])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              genero <b> ".$genero2." </b>Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar el genero<b> ".$genero2." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }


          function VerGenero($param=null){
            $cadena=$param[0];
            list($id_genero) = explode(',', $cadena);
      
            if($this->model->delete($id_genero)){

                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
               Removido correctamente <b>  </b><a class='alert-link' href='#'></a>
            </div>";

            $this->view->mensaje=$mensaje;
            $this->render();

               ?>
    
               
    
                <?php
    
           }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
           No se pudo remover <b> </b><a class='alert-link' href='#'></a>
            </div>";

            $this->view->mensaje=$mensaje;
            $this->render();
                ?>
    
                
    
                <?php
             
           }
           echo $mensaje;
        }





    }

?>