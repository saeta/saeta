<?php 


// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
//var_dump(constant('URL').'vendor/autoload.php');

require 'vendor/autoload.php';
class Correo{

    public function EnviarCorreo($datos){
        $mensaje2="<p>
                ¡Felicidades! Sr.(a) ".$docente->primer_nombre." ".$primer_apellido." su Solicitud se ha Registrado de manera 
                Exitosa en el Sistema Integrado de Desarrollo de Las Trabajadoras 
                y Los Trabajadores Académicos 
                <b>(SIDTA)</b>. 
                <br><br>
                </p>
                <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";

                // Instantiation and passing `true` enables exceptions
                $mail = new PHPMailer(true);

                try {
                    //Server settings
                    $mail->SMTPDebug = 0;                      // Enable verbose debug output
                    $mail->isSMTP();                                            // Send using SMTP
                    $mail->Host       = constant('HOST_EMAIL');                    // Set the SMTP server to send through
                    $mail->SMTPAuth   = constant('SMTPAUTH');                                   // Enable SMTP authentication
                    $mail->Username   = constant('EMAIL');                     // SMTP username
                    $mail->Password   = constant('PASSWORD_EMAIL');                               // SMTP password
                    $mail->SMTPSecure = constant('SMTPSECURE');         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
                    $mail->Port       = constant('PORT_EMAIL');                                    // TCP port to connect to
                    $mail->CharSet=constant('CHARSET_EMAIL');   

                    //Recipients
                    $mail->setFrom(constant('EMAIL'), 'Sistema Integrado de Desarrollo de los y las Trabajadores Académicos');
                    $mail->addAddress($datos['correo_destino'], $datos['nombre_destino'] ." ". $datos['apellido_destino']);     // Add a recipient
                    //$mail->addReplyTo('info@example.com', 'Information');
                    //$mail->addCC('cc@example.com');
                    //$mail->addBCC('bcc@example.com');

                    // Attachments
                    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
                    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

                    // Content
                    $mail->isHTML(true);                                  // Set email format to HTML
                    $mail->Subject = $datos['asunto'];
                    $mail->Body    = $datos['mensaje'];
                    //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

                    if(!$mail->send()){
                        return 'Detalles del error: '.$mail->ErrorInfo;
                        
                    }
                    return true;

                } catch (Exception $e) {
                    return 'Detalles del error: '.$mail->ErrorInfo;
                    //var_dump($mensaje);
                }

    }

}
?>