    <?php
include_once 'sesiones/session_admin.php';

    class Linea_investigacion extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->mensaje="";
            $this->view->nucleos=[];
            $this->view->lineas=[];

        }

        function render(){

            $nucleos=$this->model->getNucleo();

            $this->view->nucleos=$nucleos;
            $lineas=$this->model->getLinea();
            

            $this->view->lineas=$lineas;
            $this->view->render('adm_datos_academicos/linea_investigacion');
        
        }

        function registrar(){

            if(isset($_POST['registrar'])){
                //$especificacion_check=$_POST['especificacion_check'];
                
                $descripcion=$_POST['descripcion'];
                $codigo=$_POST['codigo'];
                $especificacion=$_POST['especificacion'];
                $id_nucleo_academico=$_POST['id_nucleo_academico'];

                if(empty($descripcion)){
                    $mensaje='<div class="alert alert-danger">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                El campo Línea de Investigación es . <a class="alert-link" href="#">Requerido</a>.
                            </div>';
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }
                
                if($var=$this->model->existe($descripcion)){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                     La Línea de Investigación <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                    </div>";
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }

                if($this->model->insert([
                    'descripcion'=>$descripcion,
                    'codigo'=>$codigo,
                    'especificacion'=>$especificacion,
                    'id_nucleo_academico'=>$id_nucleo_academico
                    ])){
                        $mensaje='<div class="alert alert-success">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        Línea de Investigación  '.$descripcion.' ha sido registrada <a class="alert-link" href="#">Exitosamente</a>.
                            </div>';
                    }else{
                        $mensaje="<div class='alert alert-danger alert-dismissable'>
                            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                            Ha ocurrido un error al registrar la Línea de Investigación<a class='alert-link' href='#'></a>
                        </div>";
                    }

                $this->view->mensaje=$mensaje;
                $this->render();
            }
            
        }

        function editar(){

            if(isset($_POST['registrar2'])){
                
                $id_linea_investigacion=$_POST['id_linea_investigacion'];
                $descripcion1=$_POST['descripcion1'];
                $codigo1=$_POST['codigo1'];
                $especificacion1=$_POST['especificacion1'];

                //var_dump($especificacion1);
                $id_nucleo_academico1=$_POST['id_nucleo_academico1'];

                if(empty($descripcion1)){
                    $mensaje='<div class="alert alert-danger">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                El campo Línea de Investigación académico es . <a class="alert-link" href="#">Requerido</a>.
                            </div>';
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }

                if($this->model->update([
                    'id_linea_investigacion'=>$id_linea_investigacion,
                    'descripcion'=>$descripcion1,
                    'id_nucleo_academico'=>$id_nucleo_academico1,
                    'codigo'=>$codigo1,
                    'especificacion'=>$especificacion1
                    ])){    

              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              La Línea de Investigación <b> ".$descripcion1." </b> fue Actualizado <a class='alert-link' href='#'>Correctamente</a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar Las Línea de Investigación <b> ".$descripcion1." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            }
            $this->view->mensaje=$mensaje;
            $this->render();
        }
        
        function remover($param=null){
            
            
            $id_linea_investigacion=$param[0];
            
            if($this->model->delete($id_linea_investigacion)){
                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Línea de Investigación Eliminada<a class='alert-link' href='#'> Correctamente</a>
                </div>";
               
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al tratar de eliminar la <a class='alert-link' href='#'>Línea de Investigación</a>
                </div>";
            }
            echo $mensaje;
           
        }

        function cambiarespecificacion($param=null){
            
            
            $id_linea_investigacion=$param[0];
            
            if($this->model->delete($id_linea_investigacion)){
                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Línea de Investigación Eliminada<a class='alert-link' href='#'> Correctamente</a>
                </div>";
               
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al tratar de eliminar la <a class='alert-link' href='#'>Línea de Investigación</a>
                </div>";
            }
            echo $mensaje;
           
        }
        

    }

    ?>