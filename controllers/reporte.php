<?php
include_once 'sesiones/session_admin.php';
class Reporte extends Controller{
    
    function __construct(){
        parent::__construct();
    
    }
    function render(){


        $total_docente=$this->model->get_total_docente();
        $this->view->total_docente=$total_docente;

        $censados=$this->model->get_total_docente_censados();
        $this->view->censados=$censados;

        $centros=$this->model->get_total_centro_estudio();
        $this->view->centros=$centros;

        $ejes=$this->model->get_total_eje_regional();
        $this->view->ejes=$ejes;

        $sedes=$this->model->get_sede();
        $this->view->sedes=$sedes;

        $this->view->render('reporte/index');
    }


    function sede($param = null){
        list($nombre)=explode('/', $param[0]);
        if($nombre == ''){
            $nombre=$_POST["nombre"];
        }else{
            $nombre;
        }
        

        if($sedes=$this->model->get_sede_estado($nombre)==false){


            $mensaje="<div  class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'></button>
            No hay censados en estos momentos <a class='alert-link' href='#'></a>
            </div>";

            $this->view->mensaje=$mensaje;
            $this->view->render('reporte/sede');

        }else{
            $sedes=$this->model->get_sede_estado($nombre);
            $this->view->sedes=$sedes;
            $this->view->render('reporte/sede');
        }
       
        
    }



    function verpersona($param = null){

        list($centro,$estado)=explode(',', $param[0]);
     
        $persona_centros=$this->model->persona($centro,$estado);
        $this->view->persona_centros=$persona_centros;
        $this->view->render('reporte/persona');
    }


}
?>