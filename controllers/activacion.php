<?php
include_once 'sesiones/session_admin.php';

    class Activacion extends Controller {
        function __construct(){
            parent::__construct();

            $this->view->modulos=[];  
            $this->view->aldeas=[];  
            $this->view->ofertas=[];
            $this->view->tipos_activacion=[];
            $this->view->ascensos=[];

        }

        function render(){ 

            $modulos=$this->model->get();
            $this->view->modulos=$modulos;
            
            $aldeas=$this->model->getAldea();
            $this->view->aldeas=$aldeas;
            //getOfertaAcademica
            $ofertas=$this->model->getOfertaAcademica();
            $this->view->ofertas=$ofertas;

            $this->view->render('modulos/activacion');        
        }

        //render de vista de agregar 
    function viewEdit($param = null){

        $fechas=$this->model->getModAscenso();
            $_SESSION['fecha_inicio']=$fechas['fecha_inicio'];
            $_SESSION['fecha_fin']=$fechas['fecha_fin']; 
            
        $id_activacion=$param[0];
        $this->view->id_activacion=$id_activacion;
        $activacion=$this->model->getbyID($id_activacion);
        $this->view->activacion=$activacion;

        $tipos_activacion=$this->model->getTipoActivacion();
        $this->view->tipos_activacion=$tipos_activacion;

        $this->view->render('modulos/editar_activacion');
    }

        //vista de modulo de activacion de ascensos
        function viewAscenso(){

            
            $fechas=$this->model->getModAscenso();
            $_SESSION['fecha_inicio']=$fechas['fecha_inicio'];
            $_SESSION['fecha_fin']=$fechas['fecha_fin'];

            
            

            $tipos_activacion=$this->model->getTipoActivacion();
            $this->view->tipos_activacion=$tipos_activacion;

            $ascensos=$this->model->getAscensos();
            $this->view->ascensos=$ascensos;

            $this->view->render('modulos/activacion_ascenso');        

        }

        //siea
        function ActivarModulo(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
    
            $modulo=$_POST['modulo'];            
            $motivo=$_POST['motivo'];
            $fecha_inicio=$_POST['start'];
            $fecha_fin=$_POST['end'];
            $aldea=$_POST['aldea'];           
            //arreglo de ofertas academicas
            $ofertas=$_POST["ofertas"]; 

            //date("d/m/Y", strtotime($noticia->publicacion_fecha));
                       
            $mensaje="";

            if($var=$this->model->existe($motivo)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Modulo <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($this->model->insert(['modulo'=>$modulo,'motivo'=>$motivo,'fecha_inicio'=>date("Y/m/d", strtotime($fecha_inicio)),
            'fecha_fin'=>date("Y/m/d", strtotime($fecha_fin)),'aldea'=>$aldea,'ofertas'=>$ofertas])){
              
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Modulo agregado<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al agregar un Modulo<a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
        }


        
        //para activar ascensos
        function ActivarAscenso(){//se agrega esta linea
          
    
            $tipo_activacion=$_POST['tipo_activacion'];
            $descripcion=$_POST['descripcion'];
            $periodo=$_POST['periodo'];
            $fecha_inicio=$_POST['start'];
            $fecha_fin=$_POST['end'];
            $mensaje="";
            if($this->model->existe_ascenso([
                'periodo_lectivo'=>$periodo,
                'id_tipo_activacion'=>$tipo_activacion
                ])){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ya existe un <b>Módulo de ASCENSO Activado </b> para este Período <a class='alert-link' href='#'></a>
                </div>";
               
                $this->view->mensaje=$mensaje;
                $this->viewAscenso();
                exit();
            }
           
            
            
            //var_dump($estatus);
            if($this->model->insert_ascenso([
                'tipo_activacion'=>$tipo_activacion,
                'descripcion'=>$descripcion,
                'fecha_inicio'=>date('Y-m-d', strtotime($fecha_inicio)),
                'fecha_fin'=>date('Y-m-d', strtotime($fecha_fin)),
                'periodo'=>$periodo
                
                ])){
              
                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
                Modulo de Ascenso activado<a class='alert-link' href='#'></a></div>";
            
            } else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Activar un Modulo<a class='alert-link' href='#'></a>
                </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->viewAscenso();
            
        }

        //SUREA
        function ActualizarModulo(){//se agrega esta linea
    
            //$id_persona=$_SESSION["id_persona"];

            $id_modulo_activacion=$_POST['id_modulo_activacion'];   
            $modulo=$_POST['moduloe'];            
            $motivo=$_POST['motivoe'];
            $fecha_inicio=$_POST['starte'];
            $fecha_fin=$_POST['ende'];
            $aldea=$_POST['aldeae'];           
            //arreglo de ofertas academicas
            $ofertas=$_POST["ofertae"];
            //var_dump($ofertas);
           // var_dump($id_municipio);
            //var_dump( $id_estado,$codigoine, $estado,$id_pais2);
            $mensaje="";
    
      
                if($this->model->update(['id_modulo_activacion'=> $id_modulo_activacion,'modulo'=>$modulo,'motivo'=>$motivo,'fecha_inicio'=>date("Y/m/d", strtotime($fecha_inicio)),
                'fecha_fin'=>date("Y/m/d", strtotime($fecha_fin)),'aldea'=>$aldea,'ofertas'=>$ofertas])){
    
      
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Modulo <b> ".$modulo." </b> Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
            
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar Modulo <b> ".$modulo." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
            // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
        }


        //SIDTA
        function actualizarAscenso($param){//se agrega esta linea
    


            $id_activacion=$_POST['id_activacion'];   
            $id_tipo_activacion=$_POST['tipo_activacion2'];
            $descripcion=$_POST['descripcion2'];
            $periodo=$_POST['periodo2'];
            $fecha_inicio=$_POST['starte'];
            $fecha_fin=$_POST['ende'];
            $mensaje="";
    
            
            
                
            if($this->model->update_ascenso([
                    'id_activacion'=>$id_activacion,
                    'periodo_lectivo'=>$periodo,
                    'estatus'=>$estatus,
                    'fecha_inicio'=>date('Y-m-d', strtotime($fecha_inicio)),
                    'fecha_fin'=>date('Y-m-d', strtotime($fecha_fin)),
                    'id_tipo_activacion'=>$id_tipo_activacion,
                    'descripcion'=>$descripcion
                    ])){
    
      
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Modulo <b> </b> Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
            
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar Modulo <b> </b><a class='alert-link' href='#'></a>
                </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->viewAscenso($param);
            
          
        }



    }

?>
