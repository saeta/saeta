<?php
include_once 'models/datosacademicos/docente.php';
//include_once 'models/datosacademicos/documento.php';
include_once 'sesiones/session_admin.php';
//include_once 'scripts/files.php';

    class Registro_reconocimiento extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->mensaje="";
            $this->view->docente=[];
            $this->view->recono=[];
           
            $this->view->tipos_reconocimiento=[];
            $this->view->instituciones=[];
        }

        function render(){

            $docente=$this->model->getDocente();
            //var_dump($docente);
            $this->view->docente=$docente;
            $recono=$this->model->getReconocimiento();
            //var_dump($recono);
           // $this->view->tipos_reconocimiento=$tipos_reconocimiento;
           // $tipos_reconocimiento=$this->model->getTipoReco();
           //var_dump($tipos_reconocimiento);

            $this->view->recono=$recono;
            $this->view->render('box_reconocimientos/registro_reconocimiento');
        
        }

        

        function view_registro(){

            $docente=$this->model->getDocente();
            //var_dump($docente);
            $this->view->docente=$docente;
            $recono=$this->model->getReconocimiento();
            $this->view->recono=$recono;
        
      //obtener tipos de institucion
  $tipos_reconocimiento=$this->model->getCatalogo('tipo_reconocimiento');
  $this->view->tipos_reconocimiento=$tipos_reconocimiento;
 // var_dump($tipos_reconocimiento);
 
 $this->view->render('box_reconocimientos/agregar_reconocimiento');
// var_dump($tipos_reconocimiento);
        }



        function viewEditar($param = null){

            $id_reconocimiento=$param[0];
            //var_dump($param[0]);
            $docente=$this->model->getDocente($id_reconocimiento);
            //var_dump($docente);
            
            //var_dump($_SESSION['id_reconocimiento']);
            $this->view->docente=$docente;
            $recono=$this->model->getbyId($id_reconocimiento);
            $this->view->recono=$recono;
            $_SESSION['id_reconocimiento']= $id_reconocimiento;
            //var_dump($arte_software);


        //obtener tipos de institucion
        $tipos_reconocimiento=$this->model->getCatalogo('tipo_reconocimiento');
        $this->view->tipos_reconocimiento=$tipos_reconocimiento;

           
            $this->view->render('box_reconocimientos/editar_reconocimiento');
        
        }

        function viewVer($param = null){
//var_dump($param[0]);
            $id_reconocimiento=$param[0];
            //var_dump($param[0]);
            $docente=$this->model->getDocente($id_reconocimiento);
            //var_dump($docente);
            $_SESSION['id_reconocimiento']= $id_reconocimiento;
            //var_dump($_SESSION['id_reconocimiento']);
            $this->view->docente=$docente;
            $reconocimiento=$this->model->getbyId($id_reconocimiento);
            $this->view->descripcion_documento=$reconocimiento->descripcion_documento;
            $this->view->id_reconocimiento=$reconocimiento->id_reconocimiento;

// ------------------------------



                  //obtener tipos de institucion
  $tipos_reconocimiento=$this->model->getCatalogo('tipo_reconocimiento');
  $this->view->tipos_reconocimiento=$tipos_reconocimiento;


            $this->view->recono=$reconocimiento;
            $this->view->render('box_reconocimientos/ver_reconocimiento');
        
        }


        function registrar(){
            //valida que todo este bien en el archivo
    
            $titulo=$_POST['titulo'];
            //obtener detalles de archivos y validar extensiones
            if($this->model->insert([
                'titulo'=>$titulo,
                'pdf_trabajo'=>$_FILES['reconocimiento'],
                'pdf_pida'=>$_FILES['acta_pida'],
                'pdf_proyecto'=>$_FILES['acta_proyecto'],
                'id_persona'=>$_SESSION['id_persona']
            ])){
                $mensaje='<div class="alert alert-success">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        La Solicitud Ha sido registrada <a class="alert-link" href="#">Exitosamente</a>.
                    </div>';
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                    Ha ocurrido un error al registrar la <a class='alert-link' href='#'>Solicitud de Ascenso</a>
                </div>";
            }
     
            $this->view->mensaje=$mensaje;
            $this->render();
        }




  //funcion para mover los archivos
  function mover($extension,$ruta_destino,$ruta_temporal,$name_archivo){
    //var_dump($extension,$ruta_destino,$ruta_temporal,$name_archivo);
    
    $extensiones_permitidas = array('csv', 'gif', 'png', 'jpeg', 'xlsx');
    if (in_array($extension, $extensiones_permitidas)) {
        $ruta_carga = $ruta_destino;
        $destino = $ruta_carga . $name_archivo;
        //var_dump($destino);

        /* 
            la funcion move_uploaded_file mueve el archivo cargado a una nueva ubicacion
        */
        if(move_uploaded_file($ruta_temporal, $destino))
        {
            return true;
        } else{
            //echo "primer if";

            return false;
        }
        

    }else{
        //echo "segundo if";
        return false;
    }

}
// ------------------------------

function viewDocumento($param = null){

    $id_reconocimiento=$param[0];
    $documento=$this->model->getbyId($id_reconocimiento);
    $this->view->documento="reconocimientos/".rawurlencode($documento->descripcion_documento);
    $this->view->render('documentos/viewDocumento');

}
// ------------------------------

        function registrarReconocimiento(){

            if(isset($_POST['registrar'])){
                $id_tipo_reconocimiento=$_POST['tipo_reconocimiento'];
                $descripcion=$_POST['descripcion'];
                $entidad_promotora=$_POST['entidad_promotora'];
                $ano_reconocimiento=$_POST['ano_reconocimiento'];
                $caracter=$_POST['caracter'];
                $id_documento=$_POST['id_documento'];
                $id_docente=$_SESSION['id_docente'];
                $estatus_verificacion=$_POST['estatus_verificacion'];
                $pdf_reconocimiento=$_FILES['pdf_certificado'];

              //  if($id_tipo_reconocimiento==1){
               //     $file_publico=$_FILES['file_publico'];
               // }
                //var_dump($mmmmmmm);


                
                if($var=$this->model->existe($descripcion)){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                     El reconocimiento <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                    </div>";
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }


                if($this->model->insert([
                    'id_tipo_reconocimiento'=>$id_tipo_reconocimiento,
                    'descripcion'=>$descripcion,
                    'entidad_promotora'=>$entidad_promotora,
                    'ano_reconocimiento'=>$ano_reconocimiento,
                    'caracter'=>$caracter,
                    'id_documento'=>$id_documento,
                    'id_docente'=>$id_docente,
                    'estatus_verificacion'=>$estatus_verificacion,
                    'pdf_reconocimiento'=>$pdf_reconocimiento,
                    'id_persona'=>$_SESSION['id_persona']
                    ])){
                        $mensaje='<div class="alert alert-success">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                reconocimiento '.$descripcion.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                            </div>';
                    }else{
                        $mensaje="<div class='alert alert-danger alert-dismissable'>
                            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                            Ha ocurrido un error al registrar  el reconocimiento<a class='alert-link' href='#'></a>
                        </div>";
                    }

                $this->view->mensaje=$mensaje;
                $this->render();
            }
            
        }




        function editarReconocimiento(){

            
                $id_reconocimiento=$_POST['id_reconocimiento'];
                $id_tipo_reconocimiento=$_POST['tipo_reconocimiento'];
                $descripcion=$_POST['descripcion'];
                $entidad_promotora=$_POST['entidad_promotora'];
                $ano_reconocimiento=$_POST['ano_reconocimiento'];
                $caracter=$_POST['caracter'];
               // $estatus_verificacion=$_POST['estatus_verificacion'];
               // $id_documento=$_POST['id_documento'];
               // $pdf_reconocimiento=$_FILES['pdf_certificado'];
                
               $id_docente=$_SESSION['id_docente'];

               

                // var_dump($id_reconocimiento, $descripcion2, $entidad_promotora2, $entidad_promotora2, $url_otro2 );
               



                if($this->model->update([
                    'id_reconocimiento'=>$id_reconocimiento,
                    'id_tipo_reconocimiento'=>$id_tipo_reconocimiento,
                    'descripcion'=>$descripcion,
                    'entidad_promotora'=>$entidad_promotora,
                    'ano_reconocimiento'=>$ano_reconocimiento,
                    'caracter'=>$caracter,
                    //'id_documento'=>$id_documento,
                    'id_docente'=>$id_docente,
                  //  'pdf_certificado'=>$pdf_reconocimiento,

                    'id_persona'=>$_SESSION['id_persona']
                   // 'estatus_verificacion2'=>$estatus_verificacion2
                   
                   // 'id_documento2'=>$id_documento2
                   
                  
                    ])){    


              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              el reconocimiento <b> ".$descripcion." </b> fue Actualizado <a class='alert-link' href='#'>Correctamente</a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar el reconocimiento <b> ".$descripcion." </b><a class='alert-link' href='#'></a>
            </div>";
            }

        

            $this->view->mensaje=$mensaje;
            $this->render();
           // $this->viewEditar();
        }


        function verReconocimiento(){

            
            $this->view->render('box_reconocimientos/ver_reconocimiento');
        
        }







        
        function removerReconocimiento($param=null){
            
            
            $id_reconocimiento=$param[0];
            
            if($this->model->delete($id_reconocimiento)){
                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                el reconocimiento<a class='alert-link' href='#'> Correctamente</a>
                </div>";
               
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al tratar de eliminar el <a class='alert-link' href='#'>arte o software</a>
                </div>";
            }
            echo $mensaje;
           
        }
        


        



    }

    ?>