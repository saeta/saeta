<?php
include_once 'sesiones/session_admin.php';

    class Ambiente extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->parroquias=[];            
            
            $this->view->ambientes=[];
            
        }

        function render(){ 


            $ambientes=$this->model->get();
            $this->view->ambientes=$ambientes;

            $parroquias=$this->model->getParroquia();
            $this->view->parroquias=$parroquias;

            $this->view->render('estructura/ambiente');        
        }



        function registrarAmbiente(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
    
            $codigo=$_POST['codigoambiente'];            
            $ambiente=$_POST['ambiente'];
            $estatus=$_POST['estatus'];
            $parroquia=$_POST['parroquia'];

            $latitud=$_POST['latitud'];
            $logitud=$_POST['logitud'];
            $direccion=$_POST['direccion'];


            
            
            $mensaje="";

            if($var=$this->model->existe($codigo)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                 Ambiente <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($this->model->insert(['codigo'=>$codigo,'ambiente'=>$ambiente,'estatus'=>$estatus,
            'parroquia'=>$parroquia,'latitud'=>$latitud,'logitud'=>$logitud,'direccion'=>$direccion])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Ambiente agregado<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al agregar un Ambiente<a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
          }



          function ActualizarAmbiente(){//se agrega esta linea
          
      
    
            //$id_persona=$_SESSION["id_persona"];
    

            $id_ambiente=$_POST['id_ambiente'];
            $codigo=$_POST['codigoambienteedit'];            
            $ambiente=$_POST['ambienteedit'];
            $estatus=$_POST['estatusedit'];
            $parroquia=$_POST['parroquiaedit'];

            $latitud=$_POST['latitudedit'];
            $logitud=$_POST['logitudedit'];
            $direccion=$_POST['direccionedit'];


            
         
           // var_dump($id_municipio);
            //var_dump( $id_estado,$codigoine, $estado,$id_pais2);
            $mensaje="";


           
    
            if($this->model->update(['id_ambiente'=>$id_ambiente,'codigo'=>$codigo,'ambiente'=>$ambiente,'estatus'=>$estatus,
            'parroquia'=>$parroquia,'latitud'=>$latitud,'logitud'=>$logitud,'direccion'=>$direccion])){    
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Ambiente <b> ".$ambiente." </b> Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar Ambiente <b> ".$ambiente." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }

    }

?>