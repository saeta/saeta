<?php
include_once 'models/datosacademicos/docente.php';
include_once 'sesiones/session_admin.php';

    class Registro_libro extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->mensaje="";
            $this->view->docente=[];
            $this->view->libro=[];

        }

        function render(){

            $docente=$this->model->getDocente();
            //var_dump($docente);
            $this->view->docente=$docente;
            
            $libro=$this->model->getLibro();
            //var_dump($arte);
            $this->view->libro=$libro;
            $this->view->render('produccion_intelectual/registro_libro');
        
        }



        function view_registro(){

            $docente=$this->model->getDocente();
            //var_dump($docente);
            $this->view->docente=$docente;
            
            $this->view->libro=$libro;
            $this->view->render('produccion_intelectual/agregar_libro');
        
        }

        function viewEditar($param = null){

            $id_libro=$param[0];
            //var_dump($param[0]);
            $docente=$this->model->getDocente($id_libro);
            //var_dump($docente);
            $_SESSION['id_libro']= $id_libro;
            //var_dump($_SESSION['id_arte_software']);
            $this->view->docente=$docente;
            $registro_libro=$this->model->getbyId($id_libro);
            //var_dump($arte_software);


            $this->view->libro=$registro_libro;
            $this->view->render('produccion_intelectual/editar_libro');
        
        }

        function viewVer($param = null){

            $id_libro=$param[0];
            //var_dump($param[0]);
            $docente=$this->model->getDocente($id_libro);
            //var_dump($docente);
            $_SESSION['id_libro']= $id_libro;
            //var_dump($_SESSION['id_arte_software']);
            $this->view->docente=$docente;
            $registro_libro=$this->model->getbyId($id_libro);
            //var_dump($id_libro);


            $this->view->libro=$registro_libro;
            $this->view->render('produccion_intelectual/ver_libro');
        
        }


    

        function registrarLibro(){

            if(isset($_POST['registrar'])){
                $nombre_libro=$_POST['nombre_libro'];
                $editorial=$_POST['editorial'];
                $isbn_libro=$_POST['isbn_libro'];
                $ano_publicacion=$_POST['ano_publicacion'];
                $ciudad_publicacion=$_POST['ciudad_publicacion'];
                $volumenes=$_POST['volumenes'];
                $nro_paginas=$_POST['nro_paginas'];
                $url_libro=$_POST['url_libro'];
                $id_docente=$_SESSION['id_docente'];

                //var_dump($mmmmmmm);


                
                if($var=$this->model->existe($nombre_libro)){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                     El Libro <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                    </div>";
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }


                if($this->model->insert([
                    'nombre_libro'=>$nombre_libro,
                    'editorial'=>$editorial,
                    'isbn_libro'=>$isbn_libro,
                    'ano_publicacion'=>$ano_publicacion,
                    'ciudad_publicacion'=>$ciudad_publicacion,
                    'volumenes'=>$volumenes,
                    'nro_paginas'=>$nro_paginas,
                    'url_libro'=>$url_libro,
                    'id_docente'=>$id_docente
                    ])){
                        $mensaje='<div class="alert alert-success">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        Libro  '.$nombre_libro.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                            </div>';
                    }else{
                        $mensaje="<div class='alert alert-danger alert-dismissable'>
                            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                            Ha ocurrido un error al registrar  el Libro<a class='alert-link' href='#'></a>
                        </div>";
                    }

                $this->view->mensaje=$mensaje;
                $this->render();
            }
            
        }




        function editarLibro(){

            
                $id_libro=$_POST['id_libro'];
                $nombre_libro2=$_POST['nombre_libro2'];
                $editorial2=$_POST['editorial2'];
                $isbn_libro2=$_POST['isbn_libro2'];
                $ano_publicacion2=$_POST['ano_publicacion2'];
                $ciudad_publicacion2=$_POST['ciudad_publicacion2'];
                $volumenes2=$_POST['volumenes2'];
                $nro_paginas2=$_POST['nro_paginas2'];
                $url_libro2=$_POST['url_libro2'];
               // $id_docente2=$_SESSION['id_docente'];


                // var_dump($id_arte_software, $nombre_arte2, $ano_arte2, $entidad_promotora2, $url_otro2 );
               



                if($this->model->update([
                    'id_libro'=>$id_libro,

                    'nombre_libro2'=>$nombre_libro2,
                    'editorial2'=>$editorial2,
                    'isbn_libro2'=>$isbn_libro2,
                    'ano_publicacion2'=>$ano_publicacion2,
                    'ciudad_publicacion2'=>$ciudad_publicacion2,
                    'volumenes2'=>$volumenes2,
                    'nro_paginas2'=>$nro_paginas2,
                    'url_libro2'=>$url_libro2
                  
                    ])){    


              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              El Libro <b> ".$nombre_libro2." </b> fue Actualizado <a class='alert-link' href='#'>Correctamente</a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar el Libro <b> ".$nombre_libro2." </b><a class='alert-link' href='#'></a>
            </div>";
            }

        

            $this->view->mensaje=$mensaje;
            $this->render();
           // $this->viewEditar();
        }


        function verLibro(){

            
            $this->view->render('produccion_intelectual/ver_libro');
        
        }





        
        function removerLibro($param=null){
            
            
            $id_libro=$param[0];
            
            if($this->model->delete($id_libro)){
                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Libro<a class='alert-link' href='#'> Correctamente</a>
                </div>";
               
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al tratar de eliminar el <a class='alert-link' href='#'>el Libro</a>
                </div>";
            }
            echo $mensaje;
           
        }
        


        



    }

    ?>