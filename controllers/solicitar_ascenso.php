<?php
include_once 'sesiones/session_admin.php';

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require 'vendor/autoload.php';
include_once 'scripts/correos.php';

class Solicitar_ascenso extends Controller{
    
    function __construct(){
        parent::__construct();
      //  $this->view->render('ayuda/index');
    }

    //vista de Trabajadores Académicos
    function render(){
        
        if($_SESSION['id_clasificacion_docente']==2){
            echo "<script>alert('Usted es un Docente Contratado');</script>";
            $this->view->render('errores/index');
            exit();
        }

        $concurso=$this->model->getConcursobyid($_SESSION['id_docente']);
        
        if($concurso==false){
            $mensaje="<script>alert('Por favor registra el concurso de oposición para solicitar tu ascenso');</script>";
            $this->view->mensaje=$mensaje;
            header("Refresh: 0; URL= ".constant('URL')."concurso/viewAdd");
        }

        if($_SESSION['id_escalafon'] == 3 || $_SESSION['id_escalafon'] == 4){
            $doctorado=$this->model->getDoctoradobyid($_SESSION['id_docente']);
            //var_dump($doctorado);
            //$doctorado=false;
    
            if($doctorado==false){
                $mensaje="<script>alert('Por favor registra un Doctorado Culminado para solicitar tu ascenso');</script>";
                $this->view->mensaje=$mensaje;
                header("Refresh: 0; URL= ".constant('URL')."datos_academicos/viewAdd");
            }
        }
        
        
        $ascensos=$this->model->get($_SESSION['id_docente']);


        $this->view->ascensos=$ascensos;

        $this->view->render('ascensos/index');
    }

    //cargar vista de documentos
    function viewDocumento($param = null){

        list($id_solicitud_ascenso,$id_tipo_documento)=explode(',', $param[0]);

        
        switch($id_tipo_documento){
            case 2://trabajo ascenso
            $trabajo_ascenso=$this->model->getByIdSolicitud_TRABAJO_ASCENSO($id_solicitud_ascenso);
            $this->view->trabajo_ascenso=$trabajo_ascenso;
            $this->view->documento="ascensos/".rawurlencode($trabajo_ascenso->trabajo_ascenso);
            break;

            case 3://ACTA PIDA
            $acta_pida=$this->model->getByIdSolicitud_ACTA_PIDA($id_solicitud_ascenso);
            $this->view->acta_pida=$acta_pida;
            $this->view->documento="ascensos/".rawurlencode($acta_pida->acta_pida);
            break;

            case 4://ACTA PROYECTO
            $acta_proyecto=$this->model->getByIdSolicitud_ACTA_PROYECTO($id_solicitud_ascenso);
            $this->view->acta_proyecto=$acta_proyecto;
            $this->view->documento="ascensos/".rawurlencode($acta_proyecto->acta_proyecto);
            break;

            case 5://RESOLUCION DE ASCENSO
            $resolucion_ascenso=$this->model->getByIdSolicitud_RESOLUCION($id_solicitud_ascenso);
            $this->view->resolucion_ascenso=$resolucion_ascenso;
            
            $this->view->documento="ascensos/".rawurlencode($resolucion_ascenso->resolucion);
            break;

            case 10://MEMO SOLICITUD 1
            $memo_solicitud1=$this->model->getMemos($id_solicitud_ascenso, 10);
            $this->view->memo_solicitud1=$memo_solicitud1;
            $this->view->documento="ascensos/".rawurlencode($memo_solicitud1->descripcion);
            break;
            case 11://MEMO SOLICITUD 2
            $memo_solicitud2=$this->model->getMemos($id_solicitud_ascenso, 11);
            $this->view->memo_solicitud2=$memo_solicitud2;
            $this->view->documento="ascensos/".rawurlencode($memo_solicitud2->descripcion);
            break;
            case 12://Pto de Cuenta
            $pto_cuentaS1=$this->model->getMemos($id_solicitud_ascenso, 12);
            $this->view->pto_cuentaS1=$pto_cuentaS1;
            $this->view->documento="ascensos/".rawurlencode($pto_cuentaS1->descripcion);
            break;
            case 13://MEMO ACTA DEFENSA 1
            $memo_R1=$this->model->getMemos($id_solicitud_ascenso, 13);
            $this->view->memo_R1=$memo_R1;
            $this->view->documento="ascensos/".rawurlencode($memo_R1->descripcion);
            break;
            case 14://MEMO ACTA DEFENSA 2
            $memo_R2=$this->model->getMemos($id_solicitud_ascenso, 14);
            $this->view->memo_R2=$memo_R2;
            $this->view->documento="ascensos/".rawurlencode($memo_R2->descripcion);
            break;
            case 15://ACTA DE DEFENSA
            $actaDefensa=$this->model->getMemos($id_solicitud_ascenso, 15);
            $this->view->actaDefensa=$actaDefensa;
            $this->view->documento="ascensos/".rawurlencode($actaDefensa->descripcion);
            break;
            case 16://PTO DE CUENTA RESOLUCION, FASE 2
            $pto_cuentaR1=$this->model->getMemos($id_solicitud_ascenso, 16);
            $this->view->pto_cuentaR1=$pto_cuentaR1;
            $this->view->documento="ascensos/".rawurlencode($pto_cuentaR1->descripcion);
            break;
            case 17://TITULO_DOCTOR
            $titulo_doctor=$this->model->getMemos($id_solicitud_ascenso, 17);
            $this->view->titulo_doctor=$titulo_doctor;
            $this->view->documento="ascensos/".rawurlencode($titulo_doctor->descripcion);
            break;
        }

       
        $this->view->render('documentos/viewDocumento');
    }

    //viewAdmin
    function viewAdmin(){

        $ascensos=$this->model->getAscensosAdmin($_SESSION['id_docente']);
        $this->view->ascensos=$ascensos;
        $this->view->render('ascensos/listAdmin');
    }

    //view Editar
    function viewAgregar(){
        $docente=$this->model->getByIdDocente($_SESSION['id_docente']);
        $this->view->docente=$docente;
        
        

        $escalafones=$this->model->getEscalafon('escalafon');
        $this->view->escalafones=$escalafones;

        $this->view->render('ascensos/agregar');
    }

    //view Ver
    function viewDetail($param=null){
        
        list($id_solicitud_ascenso, $id_docente) = explode(',', $param[0]);
        $this->view->id_solicitud_ascenso=$id_solicitud_ascenso;
        $this->view->id_docente=$id_docente;

        if($_SESSION['id_perfil']==1){
            
            $docente=$this->model->getByIdDocente($_SESSION['id_docente']);
            $solicitud_ascenso=$this->model->getByIdSolicitud($id_solicitud_ascenso);
            $escalafon_siguiente=$this->model->getByIdSolicitudEscalafonSiguiente($id_solicitud_ascenso);
            $docente->ciudad=$solicitud_ascenso->ciudad;
            $docente->escalafon_siguiente=$escalafon_siguiente->escalafon_siguiente;
            $docente->fecha_consulta=$solicitud_ascenso->fecha_consulta;
            $docente->fecha_solicitud=$solicitud_ascenso->fecha_solicitud;

            $this->view->docente=$docente;
        }

        $solicitud_ascenso=$this->model->getByIdSolicitud($id_solicitud_ascenso);

        $acta_proyecto=$this->model->getByIdSolicitud_ACTA_PROYECTO($id_solicitud_ascenso);
        $this->view->acta_proyecto=$acta_proyecto;

        $trabajo_ascenso=$this->model->getByIdSolicitud_TRABAJO_ASCENSO($id_solicitud_ascenso);
        $this->view->trabajo_ascenso=$trabajo_ascenso;

        $acta_pida=$this->model->getByIdSolicitud_ACTA_PIDA($id_solicitud_ascenso);
        $this->view->acta_pida=$acta_pida;

        $resolucion_ascenso=$this->model->getByIdSolicitud_RESOLUCION($id_solicitud_ascenso);
        $this->view->resolucion_ascenso=$resolucion_ascenso;
        $this->view->solicitud=$solicitud_ascenso;
        
        //verificaciones
        $estudios=$this->model->getEstudios($id_docente);
        $this->view->estudios=$estudios;

        $diseno_uc=$this->model->getDisenoUC($id_docente);
        $this->view->diseno_uc=$diseno_uc;

        $tutorias=$this->model->getTutoria($id_docente);
        $this->view->tutorias=$tutorias;

        $docencias_ubv=$this->model->getDocenciaUBV($id_docente);
        $this->view->docencias_ubv=$docencias_ubv;

        $comision=$this->model->getComision($id_docente);
        $this->view->comision=$comision;

        $articulo_revista=$this->model->getArticuloRevista($id_docente);
        $this->view->articulo_revista=$articulo_revista;
        
        $libro=$this->model->getLibro($id_docente);
        $this->view->libro=$libro;

        $arte_software=$this->model->getArteSoftware($id_docente);
        $this->view->arte_software=$arte_software;

        $ponencia=$this->model->getPonencia($id_docente);
        $this->view->ponencia=$ponencia;

        $reconocimiento=$this->model->getReconocimiento($id_docente);
        $this->view->reconocimiento=$reconocimiento;

        $experiencia=$this->model->getExperiencia($id_docente);
        $this->view->experiencia=$experiencia;

        //obteniendo estatus de propuesta de jurado
        $estatus_propuesta=$this->model->getPropuesta($id_solicitud_ascenso);
        $this->view->estatus_propuesta=$estatus_propuesta;

        //EL DEBER SER ES QUE EL TITULO ESTE CARGADO EN ESTUDIOS CONDUCENTES
        //AGREGAR TIPO DE TITULO EN ESTUDIOS CONDUCENTES Y TAMBIEN EL TIPO DE SI EL ESTUDIO CONDUCENTE 
        //DE NIVEL PRE GRADO O POSTGRADO
        //obtener titulo de doctor 3:AGREGADO o 4:ASOCIADO
        if($solicitud_ascenso->id_escalafon == 3 || $solicitud_ascenso->id_escalafon==4){
            $titulo_doctor=$this->model->getMemos($id_solicitud_ascenso, 17);//el valor 17 es el que trae el tipo detitulo de doctor
            $this->view->titulo_doctor=$titulo_doctor;
        }
       
        

        //estatus_memo1
        $estatus_memo1=$this->model->getMemos($id_solicitud_ascenso, 10);
        $this->view->estatus_memo1=$estatus_memo1;

        $estatus_memoS2=$this->model->getMemos($id_solicitud_ascenso, 11);
        $this->view->estatus_memoS2=$estatus_memoS2;

        $estatus_pto_cuentaS1=$this->model->getMemos($id_solicitud_ascenso, 12);
        $this->view->estatus_pto_cuentaS1=$estatus_pto_cuentaS1;

        if($solicitud_ascenso->id_estatus_ascenso==3 || $solicitud_ascenso->id_estatus_ascenso==4){//3: igual a fase 2 resultado de defensa
            //fase 2 Resultado de defensa
            $estatus_actaD=$this->model->getMemos($id_solicitud_ascenso, 15);
            $this->view->estatus_actaD=$estatus_actaD;

            $estatus_memoR1=$this->model->getMemos($id_solicitud_ascenso, 13);
            $this->view->estatus_memoR1=$estatus_memoR1;

            //14: memorandum acta de defensa 2
            $estatus_memoR2=$this->model->getMemos($id_solicitud_ascenso, 14);
            $this->view->estatus_memoR2=$estatus_memoR2;

            //16: memorandum Punto de Cuenta Resolución
            $estatus_pto_cuentaR=$this->model->getMemos($id_solicitud_ascenso, 16);
            $this->view->estatus_pto_cuentaR=$estatus_pto_cuentaR;

        }
        //sila solicitud esta declinada consultamos el motivo
        if($solicitud_ascenso->id_estatus_ascenso==5){
            $motivo=$this->model->getMotivoD($id_solicitud_ascenso);
            $this->view->motivo=$motivo;
        }
            

        $this->view->render('ascensos/detalleAscenso');
    }
    
    function viewIntermedia(){
        $docente=$this->model->getByIdDocente($_SESSION['id_docente']);

        $id_solicitud_ascenso=$this->model->getByIdSolicitudDocente($_SESSION['id_docente']);
        
        $solicitud_ascenso=$this->model->getByIdSolicitud($id_solicitud_ascenso);
        $escalafon_siguiente=$this->model->getByIdSolicitudEscalafonSiguiente($id_solicitud_ascenso);
        $docente->ciudad=$solicitud_ascenso->ciudad;
        $docente->escalafon_siguiente=$escalafon_siguiente->escalafon_siguiente;
        $docente->fecha_consulta=$solicitud_ascenso->fecha_consulta;
        $docente->fecha_solicitud=$solicitud_ascenso->fecha_solicitud;

        $this->view->docente=$docente;
        $this->view->render('ascensos/intermedia');
    }


//solicitar ascenso 
    function registrar(){
        
        
        $docente=$this->model->getByIdDocente($_SESSION['id_docente']);
        //valida que todo este bien en el archivo
        if(isset($_FILES['resolucion'])){
            $resolucion=$_FILES['resolucion'];
        }else{
            $resolucion="";
        }
        if($var=$this->model->existe($_SESSION['id_docente'])){
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
             Ya usted ha realizado su <a class='alert-link' href='#'>solicitud de ascenso</a> 
            </div>";
            $this->view->mensaje=$mensaje;
            $this->render();
            exit();
        }
        
        $titulo=$_POST['titulo'];
        $ciudad=$_POST['ciudad'];
        $fecha_consulta=$_POST['fecha_consulta'];
        $escalafon_next=$_POST['escalafon_sig_statico'];
        
        //correo persona que realiza la solicitud
        $correo=$_POST['correo'];

        /*switch($escalafon_next){
            case 4://asociado
            
            $titulo_doctor=$_FILES['titulo_doctor'];

            break;
            case 5://titular
            $titulo_doctor=$_FILES['titulo_doctor'];

            break;
            default:
            $titulo_doctor="";
            break;
        }*/
        
        
        //obtener detalles de archivos y validar extensiones
        if($this->model->insert([
            'titulo'=>$titulo,
            'pdf_trabajo'=>$_FILES['trabajo_ascenso'],
            'pdf_pida'=>$_FILES['acta_pida'],
            'pdf_proyecto'=>$_FILES['acta_proyecto'],
            'id_persona'=>$_SESSION['id_persona'],
            'pdf_resolucion'=>$resolucion,
            'id_docente'=>$_SESSION['id_docente'],
            'ciudad'=>$ciudad,
            'fecha_consulta'=>date("Y-m-d", strtotime($fecha_consulta)),
            'escalafon_next'=>$escalafon_next
            //'pdf_titulo_doctor'=>$titulo_doctor
        ])){
            $mensaje='<div class="alert alert-success">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    La Solicitud Ha sido registrada <a class="alert-link" href="#">Exitosamente</a>.
                </div>';
            //correo de pruebas: $docente->correo="anthony26334@gmail.com";
            // enviar correos correspondientes

            $mensaje_correo="<p>
            ¡Felicidades! Sr.(a) ".$docente->primer_nombre." ".$primer_apellido." su Solicitud se ha Registrado de manera 
            Exitosa en el Sistema Integrado de Desarrollo de Las Trabajadoras 
            y Los Trabajadores Académicos 
            <b>(SIDTA)</b>. 
            <br><br>
            </p>
            <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";

            $correo_Docente=new Correo();
                $var=$correo_Docente->EnviarCorreo([
                    'correo_destino'=>$docente->correo,
                    'nombre_destino'=>$docente->primer_nombre,
                    'apellido_destino'=>$docente->primer_apellido,
                    'asunto'=>'Solicitud de Ascenso - SIDTA',
                    'mensaje'=>$mensaje_correo
                    ]);


                // obtener correo y datos personales del coordinador del CE
                $coord=$this->model->getCorreoCoord([
                    'id_centro_estudio'=>$docente->id_centro_estudio,
                    'id_perfil'=>2//id_perfil 2=coordinador de centro de estudio
                ]);

                    $mensaje_coord="<p>
                    ¡El Trabajador(a) Académico(a)! ".$docente->primer_nombre." ".$primer_apellido." ha Registrado su Solicitud de manera 
                    Exitosa en el Sistema Integrado de Desarrollo de Las Trabajadoras 
                    y Los Trabajadores Académicos. <br>Como Coordinador de Centro de Estudio Ya puedes validar su Solicitud.
                    <b>(SIDTA)</b>. 
                    <br><br>
                    </p>
                    <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";
                    $correo_coord=new Correo();
                $var2=$correo_coord->EnviarCorreo([
                    'correo_destino'=>$coord->correo,
                    'nombre_destino'=>$coord->primer_nombre,
                    'apellido_destino'=>$coord->primer_apellido,
                    'asunto'=>'Solicitud de Ascenso - SIDTA',
                    'mensaje'=>$mensaje_coord
                    ]);
               
                //fin correo al coordinadordel CE

            // fin enviar correos correspondientes



        }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Registrar la <a class='alert-link' href='#'>Solicitud de Ascenso</a>
            </div>";
            $this->view->mensaje=$mensaje;
            $this->render();
            exit();
        }
   
        $this->view->mensaje=$mensaje;
        $this->render();
    }



    function updateF1($param){

        list($id_solicitud_ascenso, $id_docente) = explode(',', $param[0]);

        $trabajo_ascenso=$_POST['trabajo_ascenso'];
        $acta_pida=$_POST['acta_pida'];
        $acta_proyecto=$_POST['acta_proyecto'];
        if(!empty($resolucion)){
            $resolucion=$_POST['resolucion'];
        }else{
            $resolucion="";
        }
        $requisitos=$_POST['requisitos'];


        //var_dump($trabajo_ascenso, $acta_pida, $acta_proyecto, $resolucion, $requisitos);
        
        if($this->model->updateEstatusSolicitud([
            'id_solicitud_ascenso'=>$id_solicitud_ascenso,
            'id_estatus_ascenso'=>2
        ])){
            $mensaje='<div class="alert alert-success">
                    La Solicitud Ha sido trasladada <a class="alert-link" href="#">Exitosamente</a> a la Fase I, <b>ahora puedes Registrar la Propuesta de Jurado</b>.
                </div>';

            $docente=$this->model->getByIdDocente($id_docente);
            $mensaje_correo="<p>
            ¡Felicidades! Sr.(a) ".$docente->primer_nombre." ".$primer_apellido." su Solicitud de Ascenso ha sido Revisada de manera 
            Exitosa en el Sistema Integrado de Desarrollo de Las Trabajadoras 
            y Los Trabajadores Académicos. Por favor este atento a más Notificaciones. 
            <b>(SIDTA)</b>. 
            <br><br>
            </p>
            <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";
            //correo destino pruebas: $docente->correo="anthony26334@gmail.com";
            //correo para notificar al docente que se ha declinado su solicitud
            $correo=new Correo();
            $var=$correo->EnviarCorreo([
                'correo_destino'=>$docente->correo,
                'nombre_destino'=>$docente->primer_nombre,
                'apellido_destino'=>$docente->primer_apellido,
                'asunto'=>'Solicitud de Ascenso Aprobada - SIDTA',
                'mensaje'=>$mensaje_correo
            ]);

        }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
                Ha ocurrido un error al verificar <a class='alert-link' href='#'>Solicitud de Ascenso</a>
            </div>";
        }
        
        $this->view->mensaje=$mensaje;
        $this->viewDetail($param);

    }


///////////////// propuesta de jurado ///////////////////////////

    function render2($param){

        list($id_solicitud_ascenso, $id_docente) = explode(',', $param[0]);
        $this->view->id_solicitud_ascenso=$id_solicitud_ascenso;
        $this->view->id_docente=$id_docente;

        $escalafones=$this->model->getCatalogo('escalafon');
        $this->view->escalafones=$escalafones;

        $areas_ubv=$this->model->getUbv();
        $this->view->areas_ubv=$areas_ubv;

        $ejes_regionales=$this->model->getEjeRegional();
        $this->view->ejes_regionales=$ejes_regionales;

        $docente=$this->model->getByIdDocente($id_docente);

        //obtener docentes que sean de un escalafon superior al del docente que realizo la solicitud
        $docentes=$this->model->getDocentes($docente->id_escalafon);
        $this->view->docentes=$docentes;


        $this->view->render('propuesta_jurado/index');
    }

    function viewCuadroJ($param){

        list($id_solicitud_ascenso, $id_docente) = explode(',', $param[0]);
        $this->view->id_solicitud_ascenso=$id_solicitud_ascenso;
        $this->view->id_docente=$id_docente;

        $solicitud_escsig=$this->model->getSolicitudEscSig($id_solicitud_ascenso);
        $this->view->solicitud_escsig=$solicitud_escsig;

        $escalafon_actual=$this->model->getDocEscalafonActual($id_docente);
        $this->view->escalafon_actual=$escalafon_actual;

        $jurado_docentep=$this->model->getJuradoDocente($id_docente, 1);
        $this->view->jurado_docentep=$jurado_docentep;
       
        $jurado_docenteS=$this->model->getJuradoDocente($id_docente, 2);
        $this->view->jurado_docenteS=$jurado_docenteS;

        $jurado_docenteC=$this->model->getJuradoDocente($id_docente, 3);
        $this->view->jurado_docenteC=$jurado_docenteC;

        //obteniendo estatus de propuesta de jurado
        $estatus_propuesta=$this->model->getPropuesta($id_solicitud_ascenso);
        $this->view->estatus_propuesta=$estatus_propuesta;

        $this->view->render('propuesta_jurado/intermedia');
    }

    function registrarPropuesta($param){
        
        list($id_solicitud_ascenso, $id_docente) = explode(',', $param[0]);

        $docenteP1=$_POST['docenteP1'];
        $docenteP2=$_POST['docenteP2'];
        $docenteP3=$_POST['docenteP3'];

        $docenteS1=$_POST['docenteS1'];
        $docenteS2=$_POST['docenteS2'];
        $docenteS3=$_POST['docenteS3'];

        switch($docenteP1){
            case $docenteP2:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Coordinador debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Principal N°2.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP1=$docenteP1;
            $this->view->docenteP3=$docenteP3;
            $this->view->docenteS1=$docenteS1;
            $this->view->docenteS2=$docenteS2;
            $this->view->docenteS3=$docenteS3;
            $this->render2($param);
            exit();
            break;
            case $docenteP3:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Coordinador debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Principal N°3.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP1=$docenteP1;
            $this->view->docenteP2=$docenteP2;
            $this->view->docenteS1=$docenteS1;
            $this->view->docenteS2=$docenteS2;
            $this->view->docenteS3=$docenteS3;

            $this->render2($param);
            exit();
            break;
            case $docenteS1:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Coordinador debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Suplente N°1.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP1=$docenteP1;
            $this->view->docenteP2=$docenteP2;
            $this->view->docenteP3=$docenteP3;
            $this->view->docenteS2=$docenteS2;
            $this->view->docenteS3=$docenteS3;

            $this->render2($param);
            exit();
            break;
            case $docenteS2:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Coordinador debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Suplente N°2.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP1=$docenteP1;
            $this->view->docenteP2=$docenteP2;
            $this->view->docenteP3=$docenteP3;
            $this->view->docenteS1=$docenteS1;
            $this->view->docenteS3=$docenteS3;

            $this->render2($param);
            exit();
            break;
            case $docenteS3:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Coordinador debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Suplente N°3.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP1=$docenteP1;
            $this->view->docenteP2=$docenteP2;
            $this->view->docenteP3=$docenteP3;
            $this->view->docenteS1=$docenteS1;
            $this->view->docenteS2=$docenteS2;
            $this->view->docenteS3=$docenteS3;

            $this->render2($param);
            exit();
            break;
        }

        switch($docenteP2){
            case $docenteP1:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Principal N°2 debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Coordinador N°1.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP2=$docenteP2;
            $this->view->docenteP3=$docenteP3;
            $this->view->docenteS1=$docenteS1;
            $this->view->docenteS2=$docenteS2;
            $this->view->docenteS3=$docenteS3;
            $this->render2($param);
            exit();
            break;
            case $docenteP3:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Principal N°2 debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Principal N°3.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP1=$docenteP1;
            $this->view->docenteP2=$docenteP2;
            $this->view->docenteS1=$docenteS1;
            $this->view->docenteS2=$docenteS2;
            $this->view->docenteS3=$docenteS3;

            $this->render2($param);
            exit();
            break;
            case $docenteS1:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Principal N°2 debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Suplente N°1.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP1=$docenteP1;
            $this->view->docenteP2=$docenteP2;
            $this->view->docenteP3=$docenteP3;
            $this->view->docenteS2=$docenteS2;
            $this->view->docenteS3=$docenteS3;

            $this->render2($param);
            exit();
            break;
            case $docenteS2:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Principal N°2 debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Suplente N°2.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP1=$docenteP1;
            $this->view->docenteP2=$docenteP2;
            $this->view->docenteP3=$docenteP3;
            $this->view->docenteS1=$docenteS1;
            $this->view->docenteS3=$docenteS3;

            $this->render2($param);
            exit();
            break;
            case $docenteS3:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Principal N°2 debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Suplente N°3.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP1=$docenteP1;
            $this->view->docenteP2=$docenteP2;
            $this->view->docenteP3=$docenteP3;
            $this->view->docenteS1=$docenteS1;
            $this->view->docenteS2=$docenteS2;

            $this->render2($param);
            exit();
            break;
        }
        

        switch($docenteP3){
            case $docenteP1:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Principal N°3 debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Coordinador N°1.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP2=$docenteP2;
            $this->view->docenteP3=$docenteP3;
            $this->view->docenteS1=$docenteS1;
            $this->view->docenteS2=$docenteS2;
            $this->view->docenteS3=$docenteS3;
            $this->render2($param);
            exit();
            break;
            case $docenteP2:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Principal N°3 debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Principal N°2.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP1=$docenteP1;
            $this->view->docenteP2=$docenteP3;
            $this->view->docenteS1=$docenteS1;
            $this->view->docenteS2=$docenteS2;
            $this->view->docenteS3=$docenteS3;

            $this->render2($param);
            exit();
            break;
            case $docenteS1:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Principal N°3 debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Suplente N°1.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP1=$docenteP1;
            $this->view->docenteP2=$docenteP2;
            $this->view->docenteP3=$docenteP3;
            $this->view->docenteS2=$docenteS2;
            $this->view->docenteS3=$docenteS3;

            $this->render2($param);
            exit();
            break;
            case $docenteS2:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Principal N°3 debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Suplente N°2.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP1=$docenteP1;
            $this->view->docenteP2=$docenteP2;
            $this->view->docenteP3=$docenteP3;
            $this->view->docenteS1=$docenteS1;
            $this->view->docenteS3=$docenteS3;

            $this->render2($param);
            exit();
            break;
            case $docenteS3:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Principal N°3 debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Suplente N°3.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP1=$docenteP1;
            $this->view->docenteP2=$docenteP2;
            $this->view->docenteP3=$docenteP3;
            $this->view->docenteS1=$docenteS1;
            $this->view->docenteS2=$docenteS2;

            $this->render2($param);
            exit();
            break;
        }

        switch($docenteS1){
            case $docenteP1:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Suplente N°1 debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Coordinador N°1.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP2=$docenteP2;
            $this->view->docenteP3=$docenteP3;
            $this->view->docenteS1=$docenteS1;
            $this->view->docenteS2=$docenteS2;
            $this->view->docenteS3=$docenteS3;
            $this->render2($param);
            exit();
            break;
            case $docenteP2:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Suplente N°1 debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Principal N°2.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP1=$docenteP1;
            $this->view->docenteP2=$docenteP3;
            $this->view->docenteS1=$docenteS1;
            $this->view->docenteS2=$docenteS2;
            $this->view->docenteS3=$docenteS3;

            $this->render2($param);
            exit();
            break;
            case $docenteP3:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Suplente N°1 debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Principal N°3.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP1=$docenteP1;
            $this->view->docenteP2=$docenteP2;
            $this->view->docenteS1=$docenteS1;
            $this->view->docenteS2=$docenteS2;
            $this->view->docenteS3=$docenteS3;

            $this->render2($param);
            exit();
            break;
            case $docenteS2:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Suplente N°1 debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Suplente N°2.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP1=$docenteP1;
            $this->view->docenteP2=$docenteP2;
            $this->view->docenteP3=$docenteP3;
            $this->view->docenteS1=$docenteS1;
            $this->view->docenteS3=$docenteS3;

            $this->render2($param);
            exit();
            break;
            case $docenteS3:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Suplente N°1 debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Suplente N°3.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP1=$docenteP1;
            $this->view->docenteP2=$docenteP2;
            $this->view->docenteP3=$docenteP3;
            $this->view->docenteS1=$docenteS1;
            $this->view->docenteS2=$docenteS2;

            $this->render2($param);
            exit();
            break;
        }

        switch($docenteS2){
            case $docenteP1:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Suplente N°2 debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Coordinador N°1.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP2=$docenteP2;
            $this->view->docenteP3=$docenteP3;
            $this->view->docenteS1=$docenteS1;
            $this->view->docenteS2=$docenteS2;
            $this->view->docenteS3=$docenteS3;
            $this->render2($param);
            exit();
            break;
            case $docenteP2:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Suplente N°2 debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Principal N°2.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP1=$docenteP1;
            $this->view->docenteP2=$docenteP3;
            $this->view->docenteS1=$docenteS1;
            $this->view->docenteS2=$docenteS2;
            $this->view->docenteS3=$docenteS3;

            $this->render2($param);
            exit();
            break;
            case $docenteP3:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Suplente N°2 debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Principal N°3.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP1=$docenteP1;
            $this->view->docenteP2=$docenteP2;
            $this->view->docenteS1=$docenteS1;
            $this->view->docenteS2=$docenteS2;
            $this->view->docenteS3=$docenteS3;

            $this->render2($param);
            exit();
            break;
            case $docenteS1:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Suplente N°2 debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Suplente N°1.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP1=$docenteP1;
            $this->view->docenteP2=$docenteP2;
            $this->view->docenteP3=$docenteP3;
            $this->view->docenteS2=$docenteS2;
            $this->view->docenteS3=$docenteS3;

            $this->render2($param);
            exit();
            break;
            case $docenteS3:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Suplente N°2 debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Suplente N°3.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP1=$docenteP1;
            $this->view->docenteP2=$docenteP2;
            $this->view->docenteP3=$docenteP3;
            $this->view->docenteS1=$docenteS1;
            $this->view->docenteS2=$docenteS2;

            $this->render2($param);
            exit();
            break;
        }

        switch($docenteS3){
            case $docenteP1:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Suplente N°3 debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Coordinador N°1.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP2=$docenteP2;
            $this->view->docenteP3=$docenteP3;
            $this->view->docenteS1=$docenteS1;
            $this->view->docenteS2=$docenteS2;
            $this->view->docenteS3=$docenteS3;
            $this->render2($param);
            exit();
            break;
            case $docenteP2:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Suplente N°3 debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Principal N°2.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP1=$docenteP1;
            $this->view->docenteP2=$docenteP3;
            $this->view->docenteS1=$docenteS1;
            $this->view->docenteS2=$docenteS2;
            $this->view->docenteS3=$docenteS3;

            $this->render2($param);
            exit();
            break;
            case $docenteP3:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Suplente N°3 debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Principal N°3.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP1=$docenteP1;
            $this->view->docenteP2=$docenteP2;
            $this->view->docenteS1=$docenteS1;
            $this->view->docenteS2=$docenteS2;
            $this->view->docenteS3=$docenteS3;

            $this->render2($param);
            exit();
            break;
            case $docenteS1:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Suplente N°3 debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Suplente N°1.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP1=$docenteP1;
            $this->view->docenteP2=$docenteP2;
            $this->view->docenteP3=$docenteP3;
            $this->view->docenteS2=$docenteS2;
            $this->view->docenteS3=$docenteS3;

            $this->render2($param);
            exit();
            break;
            case $docenteS2:
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El Jurado Suplente N°3 debe ser diferente <a class='alert-link' href='#'>Distinto</a> del Jurado Suplente N°2.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->view->docenteP1=$docenteP1;
            $this->view->docenteP2=$docenteP2;
            $this->view->docenteP3=$docenteP3;
            $this->view->docenteS1=$docenteS1;
            $this->view->docenteS3=$docenteS3;

            $this->render2($param);
            exit();
            break;
        }

        if($var=$this->model->existeJurado($id_solicitud_ascenso)){
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Esta solicitud de ascenso ya tiene un <a class='alert-link' href='#'>Jurado Registrado</a>.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->viewDetail($param);
            exit();
        }

        if($this->model->insertPropuesta([
            'docenteP1'=>$docenteP1,
            'docenteP2'=>$docenteP2,
            'docenteP3'=>$docenteP3,
            'docenteS1'=>$docenteS1,
            'docenteS2'=>$docenteS2,
            'docenteS3'=>$docenteS3,
            'id_docente'=>$id_docente,
            'id_solicitud_ascenso'=>$id_solicitud_ascenso
        ])){
            //var_dump($var);
            $this->view->estatus_propuesta=$var['estatus'];
            $this->view->id_propuesta_jurado=$var['id_propuesta_jurado'];

            $mensaje='<div class="alert alert-success">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    La propuesta Ha sido registrada <a class="alert-link" href="#">Exitosamente</a>.
                </div>';
                // correo al docente
                $docente=$this->model->getByIdDocente($id_docente);
                
                $mensaje_correo="<p>
                ¡Felicidades! Sr.(a) ".$docente->primer_nombre." ".$primer_apellido." Han Registrado su Propuesta de Jurado de manera 
                Exitosa en el Sistema Integrado de Desarrollo de Las Trabajadoras 
                y Los Trabajadores Académicos 
                <b>(SIDTA)</b>. 
                <br><br>
                </p>
                <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";
    
                $correo_Docente=new Correo();
                    $var=$correo_Docente->EnviarCorreo([
                        'correo_destino'=>$docente->correo,
                        'nombre_destino'=>$docente->primer_nombre,
                        'apellido_destino'=>$docente->primer_apellido,
                        'asunto'=>'Propuesta de Jurado - SIDTA',
                        'mensaje'=>$mensaje_correo
                        ]);
                //fin correo docente
                
                // obtener correo y datos personales del Director del CE
                $coord=$this->model->getCorreoCoord([
                    'id_centro_estudio'=>$docente->id_centro_estudio,
                    'id_perfil'=>3//id_perfil 2=Director del centro de estudio
                ]);

                    $mensaje_coord="<p>
                    ¡El Trabajador(a) Académico(a)! ".$docente->primer_nombre." ".$primer_apellido." ha Registrado su Solicitud de manera 
                    Exitosa en el Sistema Integrado de Desarrollo de Las Trabajadoras 
                    y Los Trabajadores Académicos. <br>Como Director del Centro de Estudio Ya puedes Verificar la Propuesta de Jurado.
                    <b>(SIDTA)</b>. 
                    <br><br>
                    </p>
                    <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";
                    $correo_coord=new Correo();
                $var2=$correo_coord->EnviarCorreo([
                    'correo_destino'=>$coord->correo,
                    'nombre_destino'=>$coord->primer_nombre,
                    'apellido_destino'=>$coord->primer_apellido,
                    'asunto'=>'Propuesta de Jurado - SIDTA',
                    'mensaje'=>$mensaje_coord
                    ]);
               
                //fin correo al coordinadordel CE

        }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al registrar la <a class='alert-link' href='#'>propuesta de jurado</a>
            </div>";
            $this->view->mensaje=$mensaje;
            $this->render2($param);
            exit();
        }
        
        $this->view->mensaje=$mensaje;
        $this->viewCuadroJ($param);
    }

    function updatePropuesta($param){

        $id_solicitud_ascenso =  $param[0];

        
        $propuesta=$_POST['propuesta'];
        
       
        if($propuesta=='on'){
            $estatus="Verificado";
        }

        //var_dump($trabajo_ascenso, $acta_pida, $acta_proyecto, $resolucion, $requisitos);
        
        if($this->model->updateEstatusPropuesta([
            'id_solicitud_ascenso'=>$id_solicitud_ascenso,
            'estatus'=>$estatus
        ])){
            $mensaje='<div class="alert alert-success">
                    La Propuesta de Jurado ha sido verificada <a class="alert-link" href="#">Exitosamente</a>. Para Adjuntar el Memorandum de Solicitud N°1,
                    Haz Click En el botón de <b>"Adjuntar Memo 1"</b>. 
                </div>';
        }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
                Ha ocurrido un error al Verificar <a class='alert-link' href='#'>Propuesta de Jurado</a>
            </div>";
        }
        
        $this->view->mensaje=$mensaje;
        $this->viewDetail($param);

    }
//funcion para adjuntar memorandums de la solicitud de ascenso
    function adjuntarMemo($param){

        //list($id_solicitud_ascenso, $id_docente) = explode(',', $param[0]);
        list($id_solicitud_ascenso,$id_tipo_documento)=explode(',', $param[0]);

        
        if($var=$this->model->existeMemo($id_solicitud_ascenso, $id_tipo_documento)){
            //mensaje dependiendo del tipo
            switch($id_tipo_documento){
                case 10: 
                $mensaje='<div class="alert alert-danger">
                <b>ERROR:</b> El Memorandum N°1 de Solicitud ya <a class="alert-link" href="#">Existe en Esta Solicitud</a>, 
                para Visualizarlo Haz <a href="#doc_sol" class="alert-link">Click Aquí</a> ó
                Desliza <i class="fa fa-arrow-circle-down"></i> el Mouse para Visualizar la Sección de Documentos de la Solicitud.
                Comunícate Con la DGTA para Continuar con el Proceso.
                </div>';
                break;
                case 15: //acta de defensa
                $mensaje='<div class="alert alert-danger">
                <b>ERROR:</b> El Acta de Defensa de Solicitud ya <a class="alert-link" href="#">Existe en Esta Solicitud</a>, 
                para Visualizarla Haz <a href="#doc_sol" class="alert-link">Click Aquí</a> ó
                Desliza <i class="fa fa-arrow-circle-down"></i> el Mouse para Visualizar la Sección de Documentos de la Solicitud.
                Comunícate con el Director del CE para Continuar con el Proceso.
                </div>';
                break;
                case 13: //memo r1
                $mensaje='<div class="alert alert-danger">
                <b>ERROR:</b> El Memorandum de Resolución N°1 de Solicitud ya <a class="alert-link" href="#">Existe en Esta Solicitud</a>, 
                para Visualizaro Haz <a href="#doc_sol" class="alert-link">Click Aquí</a> ó
                Desliza <i class="fa fa-arrow-circle-down"></i> el Mouse para Visualizar la Sección de Documentos de la Solicitud.
                Comunícate Con la DGTA para Continuar con el Proceso.
                </div>';
                break;
            }
            
            $this->view->mensaje=$mensaje;
            $this->viewDetail($param);
            exit();
        }

        //dependiendo del tipo de documento se insertara un pdf con un tipo diferente
        switch($id_tipo_documento){
            case 10://memo 1 solicitud
            $pdf_solicitud=$_FILES['trabajo_ascenso'];
            $tipo_documento='MEMO SOLICITUD 1';
            
            break;
            
            case 15://acta de defensa
            $pdf_solicitud=$_FILES['acta_defensa'];
            $tipo_documento="ACTA_DEFENSA";
            
            break;
            case 13://memo resolucion de acta de defensa N°1
            $pdf_solicitud=$_FILES['memo_resolucion1'];
            $tipo_documento="MEMO RESOLUCIÓN 1";//DEJAR EL ACENTO
            break;
        }
        
        
        if($this->model->insertMemo([
            'id_solicitud_ascenso'=>$id_solicitud_ascenso,
            'pdf_memo_solicitud'=>$pdf_solicitud,
            'id_persona'=>$_SESSION['id_persona'],
            'tipo_documento'=>$tipo_documento
        ])){

            //en caso de variar el tipo de documento mostramos un mensaje distinto
            switch($id_tipo_documento){

                case 10://memo 1 solicitud
                $mensaje='<div class="alert alert-success">
                Memorandum N°1 de Solicitud Adjuntado <a class="alert-link" href="#">Exitosamente</a>, 
                para Visualizarlo Haz <a href="#doc_sol" class="alert-link">Click Aquí</a> ó
                Desliza <i class="fa fa-arrow-circle-down"></i> el Mouse para Visualizar la Sección de Documentos de la Solicitud.
                Comunícate Con la DGTA para Continuar con el Proceso.
                </div>';
                //obteniendo nombre y apellido del docente que realizo la solicitud de ascenso
                $docente=$this->model->getByIdSolicitud($id_solicitud_ascenso);
                //obteniendo los usuarios que tienen perfil 4 o 5 (usuarios de la DGTA)
                $users_dgta=$this->model->getUsersDGTA();
                //correo a cada uno de los usuarios que pertenecen a la dgta
                foreach($users_dgta as $row){
                    $user=new Estructura;
                    $user=$row;
                    $mensaje_correo="<p>
                    Sr(a). ".$user->primer_nombre." ". $user->primer_apellido.", El Director del Centro de Estudios ha Añadido el Memo N°1 
                    correspondiente a la Solicitud de Ascenso del Trabajador(a) Academico(a)
                    ".$docente->primer_nombre. " " . $docente->primer_apellido . ". <br>Como Administrador o Analista de la DGTA, Ya puedes validar su Veracidad.
                    <b>(SIDTA)</b>. 
                    <br><br>
                    </p>
                    <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";

                    $correo_usuario=new Correo();
                    $var=$correo_usuario->EnviarCorreo([
                      'correo_destino'=>$user->correo,
                      'nombre_destino'=>$user->primer_nombre,
                      'apellido_destino'=>$user->primer_apellido,
                      'asunto'=>'Validación de Memos de Solicitud - SIDTA',
                      'mensaje'=>$mensaje_correo
                    ]);
                }
                break;

                case 13://memo resolucion de acta de defensa N°1
                $mensaje='<div class="alert alert-success">
                Memorandum N°1 de Resolución Adjuntado <a class="alert-link" href="#">Exitosamente</a>, 
                para Visualizarlo Haz <a href="#doc_sol" class="alert-link">Click Aquí</a> ó
                Desliza <i class="fa fa-arrow-circle-down"></i> el Mouse para Visualizar la Sección de Documentos de la Solicitud.
                Comunícate Con la DGTA para Continuar con el Proceso.
                </div>';
                //obteniendo nombre y apellido del docente que realizo la solicitud de ascenso
                $docente=$this->model->getByIdSolicitud($id_solicitud_ascenso);
                //obteniendo los usuarios que tienen perfil 4 o 5 (usuarios de la DGTA)
                $users_dgta=$this->model->getUsersDGTA();
                //correo a cada uno de los usuarios que pertenecen a la dgta
                foreach($users_dgta as $row){
                    $user=new Estructura;
                    $user=$row;
                    $mensaje_correo="<p>
                    Sr(a). ".$user->primer_nombre." ". $user->primer_apellido.", El Director del Centro de Estudios ha Añadido el Memo de Resolución N°1 
                    correspondiente a la Solicitud de Ascenso del Trabajador(a) Academico(a)
                    ".$docente->primer_nombre. " " . $docente->primer_apellido . ". <br>Como Administrador o Analista de la DGTA, Ya puedes validar su Veracidad.
                    <b>(SIDTA)</b>. 
                    <br><br>
                    </p>
                    <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";

                    $correo_usuario=new Correo();
                    $var=$correo_usuario->EnviarCorreo([
                      'correo_destino'=>$user->correo,
                      'nombre_destino'=>$user->primer_nombre,
                      'apellido_destino'=>$user->primer_apellido,
                      'asunto'=>'Validación de Memos de Solicitud - SIDTA',
                      'mensaje'=>$mensaje_correo
                    ]);
                }

                break;

                case 15: // acta de defensa
                $mensaje='<div class="alert alert-success">
                El Acta de Defensa ha sido Adjuntada <a class="alert-link" href="#">Exitosamente</a>, 
                para Visualizarla Haz <a href="#doc_sol" class="alert-link">Click Aquí</a> ó
                Desliza <i class="fa fa-arrow-circle-down"></i> el Mouse para Visualizar la Sección de Documentos de la Solicitud.
                </div>';

                $docente=$this->model->getByIdSolicitud($id_solicitud_ascenso);

                // obtener correo y datos personales al Director del CE
                $coord=$this->model->getCorreoCoord([
                    'id_centro_estudio'=>$docente->id_centro_estudio,
                    'id_perfil'=>3//id_perfil 2=coordinador de centro de estudio
                ]);

                    $mensaje_coord="<p>
                    El Coordinador ha Adjuntado Exitosamente el Acta de Defensa de la Solicitud correspondiente al Trabajador(a) Académico(a) 
                    ".$docente->primer_nombre. " " .$docente->primer_apellido ." en el Sistema Integrado de Desarrollo de Las Trabajadoras 
                    y Los Trabajadores Académicos. <br>Como Director de Centro de Estudio Ya puedes Validar el Acta de Defensa.
                    <b>(SIDTA)</b>. 
                    <br><br>
                    </p>
                    <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";
                    $correo_coord=new Correo();
                    $var2=$correo_coord->EnviarCorreo([
                        'correo_destino'=>$coord->correo,
                        'nombre_destino'=>$coord->primer_nombre,
                        'apellido_destino'=>$coord->primer_apellido,
                        'asunto'=>'Acta de Ascenso - Fase II - SIDTA',
                        'mensaje'=>$mensaje_coord
                        ]);

                        //fin correo al Director del CE


                break;
            }

        }else{

            //en caso de variar el tipo de documento mostramos un mensaje distinto
            switch($id_tipo_documento){
                case 10://memo 1 solicitud
                $mensaje='<div class="alert alert-danger">
                <b>ERROR:</b> El Memorandum N°1 de Solicitud ya <a class="alert-link" href="#">Existe en Esta Solicitud</a>, 
                para Visualizarlo Haz <a href="#doc_sol" class="alert-link">Click Aquí</a> ó
                Desliza <i class="fa fa-arrow-circle-down"></i> el Mouse para Visualizar la Sección de Documentos de la Solicitud.
                Comunícate Con la DGTA para Continuar con el Proceso.
                </div>';
                break;
                case 15://Acta de Defensa
                $mensaje='<div class="alert alert-danger">
                <b>ERROR:</b> El Acta de Defensa de Solicitud ya <a class="alert-link" href="#">Existe en Esta Solicitud</a>, 
                para Visualizarla Haz <a href="#doc_sol" class="alert-link">Click Aquí</a> ó
                Desliza <i class="fa fa-arrow-circle-down"></i> el Mouse para Visualizar la Sección de Documentos de la Solicitud.
                Comunícate Con la DGTA para Continuar con el Proceso.
                </div>';
                break;
                case 13://Acta de Defensa
                $mensaje='<div class="alert alert-danger">
                <b>ERROR:</b> El Memorandum de Resolución N°1 de Solicitud ya <a class="alert-link" href="#">Existe en Esta Solicitud</a>, 
                para Visualizarla Haz <a href="#doc_sol" class="alert-link">Click Aquí</a> ó
                Desliza <i class="fa fa-arrow-circle-down"></i> el Mouse para Visualizar la Sección de Documentos de la Solicitud.
                Comunícate Con la DGTA para Continuar con el Proceso.
                </div>';
                break;
            }
            
            
        }
        
        $this->view->mensaje=$mensaje;
        $this->viewDetail($param);

    }

    function updateMemo($param){

        list($id_solicitud_ascenso,$id_tipo_documento)=explode(',', $param[0]);
        

        switch($id_tipo_documento){
            case 10://MEMO SOLICITUD 1
                $memo_solicitud1=$_POST['memo_solicitud1'];
                if($memo_solicitud1=='on'){
                    $estatus="Verificado";
                }
            break;
            case 11://MEMO SOLICITUD 2
                $memo_solicitud2=$_POST['memo_solicitud2'];
                if($memo_solicitud2=='on'){
                    $estatus="Verificado";
                    
                }
            break;
            case 12://PTO DE CUENTA 1
                $pto_cuenta_solicitud1=$_POST['pto_cuenta_solicitud1'];
                if($pto_cuenta_solicitud1=='on'){
                    $estatus="Verificado";
                    
                }
            break;
            case 13://MEMO R1
                $memo_R1=$_POST['memo_R1'];
                if($memo_R1=='on'){
                    $estatus="Verificado";
                    
                }
            break;
            case 14://MEMO R2 fase 3
                $memo_R2=$_POST['memo_R2'];
                if($memo_R2=='on'){
                    $estatus="Verificado";
                    
                }
            break;
            case 15://ACTA DE DEFENSA
                $actaD=$_POST['acta_defensa'];
                if($actaD=='on'){
                    $estatus="Verificado";
                }
            break;
            case 16://PTO DE CUENTA RESOLUCION 2
                $pto_cuentaR2=$_POST['pto_cuentaR2'];
                if($pto_cuentaR2=='on'){
                    $estatus="Verificado";
                }
            break;
            case 17://PTO DE CUENTA RESOLUCION 2
                $titulo_doctor=$_POST['titulo_doctor'];
                if($titulo_doctor=='on'){
                    $estatus="Verificado";
                }
            break;
        }

        if($this->model->updateEstatusMemo([
            'id_solicitud_ascenso'=>$id_solicitud_ascenso,
            'estatus'=>$estatus,
            'id_tipo_documento'=>$id_tipo_documento
        ])){

            switch($id_tipo_documento){
                case 10://MEMO SOLICITUD 1
                    $mensaje='<div class="alert alert-success">
                        El Memorandum N°1 ha sido Verificado <a class="alert-link" href="#">Exitosamente</a>. 
                        Para Adjuntar el Memorandum N°2 y el Punto de Cuenta N°1, haz <b>Click</b> en el 
                        Botón " <b><i class="fa fa-paperclip"></i> Adjuntar</b> ".
                    </div>';
                break;
                case 11://MEMO SOLICITUD 2
                    $mensaje='<div class="alert alert-success">
                        El Memorandum N°2 ha sido Verificado <a class="alert-link" href="#">Exitosamente</a>. 
                        Para continuar el Proceso de Ascenso, debes Esperar que el Coord. del CE adjunte el Acta de Defensa.
                    </div>';
                break;
                case 12://PTO DE CUENTA 1
                    $mensaje='<div class="alert alert-success">
                        El Punto de Cuenta Ha Sido Verificado <a class="alert-link" href="#">Exitosamente</a>. 
                        Para continuar el Proceso de Ascenso, debes Esperar que el Coord. del CE adjunte el Acta de Defensa.
                    </div>';
                break;
                case 13://MEMO R1
                $mensaje='<div class="alert alert-success">
                    El Memorandum del Acta de Defensa N°1 ha sido Verificado <a class="alert-link" href="#">Exitosamente</a>. 
                    Para Adjuntar el Memorandum N°2 y el Punto de Cuenta N°1 de Resolución, haz <b>Click</b> en el 
                    Botón "<b><i class="fa fa-paperclip"></i> Adjuntar</b>".
                </div>';
                break;
                case 14://MEMO R2 fase 3
                    $mensaje='<div class="alert alert-success">
                        El Memorandum del Acta de Defensa N°2 ha sido Verificado <a class="alert-link" href="#">Exitosamente</a>.
                    </div>';
                break;
                case 15://ACTA DE DEFENSA
                    $mensaje='<div class="alert alert-success">
                    El Acta de Defensa ha sido Verificada <a class="alert-link" href="#">Exitosamente</a>. Para Adjuntar El Memorandum N°1 de la Resolución Presiona el Botón "<b><i class="fa fa-paperclip"></i> Adjuntar Memo Resolución</b>".
                    </div>';
                break;
                case 16://PTO DE CUENTA RESOLUCION 2
                    $mensaje='<div class="alert alert-success">
                        El Punto de Cuenta de Resolución Ha Sido Verificado <a class="alert-link" href="#">Exitosamente</a>. 
                    </div>';
                break;
               
            }

            
        }else{

            switch($id_tipo_documento){
                case 10://MEMO SOLICITUD 1
                    $mensaje='<div class="alert alert-danger">
                        <b><i>ERROR:</i></b> Ha Ocurrido un Error al Intentar <a class="alert-link" href="#">Verificar</a> El Memorandum de Solicitud N°1.
                    </div>';
                break;
                case 11://MEMO SOLICITUD 2
                    $mensaje='<div class="alert alert-danger">
                    <b><i>ERROR:</i></b> Ha Ocurrido un Error al Intentar <a class="alert-link" href="#">Verificar</a> El Memorandum de Solicitud N°2.

                    </div>';
                break;
                case 12://PTO DE CUENTA 1
                    $mensaje='<div class="alert alert-danger">
                        <b><i>ERROR:</i></b> Ha Ocurrido un Error al Intentar <a class="alert-link" href="#">Verificar</a> El Punto de Cuenta de Solicitud.
                    </div>';
                break;
                case 13://MEMO R1
                $mensaje='<div class="alert alert-danger">
                    <b><i>ERROR:</i></b> Ha Ocurrido un Error al Intentar <a class="alert-link" href="#">Verificar</a> El Memorandum de Resolución N°1.
                </div>';
                break;
                case 14://MEMO R2 fase 3
                $mensaje='<div class="alert alert-danger">
                    <b><i>ERROR:</i></b> Ha Ocurrido un Error al Intentar <a class="alert-link" href="#">Verificar</a> El Memorandum de Resolución N°2.
                </div>';
                break;
                case 15://ACTA DE DEFENSA
                $mensaje='<div class="alert alert-danger">
                    <b><i>ERROR:</i></b> Ha Ocurrido un Error al Intentar <a class="alert-link" href="#">Verificar</a> El Acta de Defensa.
                </div>';
                break;
                case 16://PTO DE CUENTA RESOLUCION 2
                    $mensaje='<div class="alert alert-danger">
                        <b><i>ERROR:</i></b> Ha Ocurrido un Error al Intentar <a class="alert-link" href="#">Verificar</a> El Punto de Cuenta de Resolución.
                    </div>';
                break;
            }

        }
        
        $this->view->mensaje=$mensaje;
        $this->viewDetail($param);

    }

    function adjuntarMemoPuntoCuenta($param){

        list($id_solicitud_ascenso, $id_fase) = explode(',', $param[0]);
        //evaluar la fase
        switch($id_fase){
            case 1://fase 1

            $id_tipo_documento1=11;// memo 2
            $tipo_documento1="MEMO SOLICITUD 2";

            
            $id_tipo_documento2=12;//pto de cuenta 1 
            $tipo_documento2="PUNTO DE CUENTA SOLICITUD";

            $pdf_memo2=$_FILES['memo_solicitud_2'];
            $pdf_ptoC=$_FILES['pto_cuenta_solicitud1'];

            break;
            case 2://fase 2

            $id_tipo_documento1=14;// memo resolucion 2
            $tipo_documento1="MEMO RESOLUCIÓN 2";
            $id_tipo_documento2=16;//pto de cuenta 2
            $tipo_documento2="PUNTO DE CUENTA RESOLUCION";

            $pdf_memo2=$_FILES['memo_solicitud_2'];
            $pdf_ptoC=$_FILES['pto_cuenta_solicitud1'];

            break;
        }


        if($var=$this->model->existeMemo($id_solicitud_ascenso, $id_tipo_documento1)){
            $mensaje="<div class='alert alert-danger alert-dismissable'>
                Este Memorandum ó Punto de Cuenta ya ha sido<a class='alert-link' href='#'> Adjuntado</a> a la Solicitud.
            </div>";
            $this->view->mensaje=$mensaje;
            $this->viewDetail($param);
            exit();
        }
        
        

        if($var2=$this->model->existeMemo($id_solicitud_ascenso, $id_tipo_documento2)){
            $mensaje="<div class='alert alert-danger alert-dismissable'>
                Ya existe un Punto de cuenta <a class='alert-link' href='#'>Adjuntado</a>
            </div>";
            $this->view->mensaje=$mensaje;
            $this->viewDetail($param);
            exit();
        }

        
        
        //var_dump($trabajo_ascenso, $acta_pida, $acta_proyecto, $resolucion, $requisitos);
        
        if($this->model->insertMemoPtoCuenta([
            'id_solicitud_ascenso'=>$id_solicitud_ascenso,
            'pdf_memo_solicitud'=>$pdf_memo2,
            'pdf_pto_cuenta_solicitud'=>$pdf_ptoC,
            'id_persona'=>$_SESSION['id_persona'],
            'tipo_documento1'=>$tipo_documento1,
            'tipo_documento2'=>$tipo_documento2
        ])){

            switch($id_fase){
                case 1: 
                $var_msj='<div class="alert alert-success">
                    El Memorandum de Solicitud N°2 y El Punto de Cuenta N°1 han sido Adjuntado 
                    <a class="alert-link" href="#">Exitosamente</a>, 
                    para Visualizarlo Haz <a href="#doc_sol" class="alert-link">Click Aquí</a> ó
                    Desliza <i class="fa fa-arrow-circle-down"></i> el Mouse para Visualizar la Sección de Documentos de la Solicitud.
                    Comunícate Con la Vicerrectorado para Continuar con el Proceso.
                </div>';
                //obteniendo nombre y apellido del docente que realizo la solicitud de ascenso
                $docente=$this->model->getByIdSolicitud($id_solicitud_ascenso);
                //obteniendo los usuarios que tienen perfil 6 (usuarios de Vicerrectorado)
                $users_vice=$this->model->getUsersVICE();
                //correo a cada uno de los usuarios que pertenecen a la dgta
                foreach($users_vice as $row){
                    $user=new Estructura;
                    $user=$row;
                    $mensaje_correo="<p>
                    Sr(a). ".$user->primer_nombre." ". $user->primer_apellido.", La DGTA ha Añadido el Memo N°2 y El Punto de Cuenta de Solicitud
                    correspondiente a la Solicitud de Ascenso del Trabajador(a) Academico(a)
                    ".$docente->primer_nombre. " " . $docente->primer_apellido . ". <br>Como Administrador o Analista de Vicerrectorado, Ya puedes validar su Veracidad.
                    <b>(SIDTA)</b>. 
                    <br><br>
                    </p>
                    <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";

                    $correo_usuario=new Correo();
                    $var=$correo_usuario->EnviarCorreo([
                      'correo_destino'=>$user->correo,
                      'nombre_destino'=>$user->primer_nombre,
                      'apellido_destino'=>$user->primer_apellido,
                      'asunto'=>'Validación de Memo y Punto de Cuenta de Solicitud - Fase I - SIDTA',
                      'mensaje'=>$mensaje_correo
                    ]);
                }
                break;
                case 2: 
                $var_msj='<div class="alert alert-success">
                    El Memorandum de Resolución N°2 y El Punto de Cuenta de Resolución han sido Adjuntado 
                    <a class="alert-link" href="#">Exitosamente</a>, 
                    para Visualizarlo Haz <a href="#doc_sol" class="alert-link">Click Aquí</a> ó
                    Desliza <i class="fa fa-arrow-circle-down"></i> el Mouse para Visualizar la Sección de Documentos de la Solicitud.
                    Comunícate Con la Vicerrectorado para Continuar con el Proceso.
                </div>';
                //obteniendo nombre y apellido del docente que realizo la solicitud de ascenso
                $docente=$this->model->getByIdSolicitud($id_solicitud_ascenso);
                //obteniendo los usuarios que tienen perfil 6 (usuarios de Vicerrectorado)
                $users_vice=$this->model->getUsersVICE();
                //correo a cada uno de los usuarios que pertenecen a la dgta
                foreach($users_vice as $row){
                    $user=new Estructura;
                    $user=$row;
                    $mensaje_correo="<p>
                    Sr(a). ".$user->primer_nombre." ". $user->primer_apellido.", La DGTA ha Añadido el 
                    Memorandum de Resolución N°2 y El Punto de Cuenta de Resolución
                    correspondiente a la Solicitud de Ascenso del Trabajador(a) Academico(a)
                    ".$docente->primer_nombre. " " . $docente->primer_apellido . ". <br>Como Administrador o Analista de Vicerrectorado, Ya puedes validar su Veracidad.
                    <b>(SIDTA)</b>. 
                    <br><br>
                    </p>
                    <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";

                    $correo_usuario=new Correo();
                    $var=$correo_usuario->EnviarCorreo([
                      'correo_destino'=>$user->correo,
                      'nombre_destino'=>$user->primer_nombre,
                      'apellido_destino'=>$user->primer_apellido,
                      'asunto'=>'Validación de Memo y Punto de Cuenta de Resolución - Fase II - SIDTA',
                      'mensaje'=>$mensaje_correo
                    ]);
                }
                break;
            }
            
            $mensaje=$var_msj;
        }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
                Ha ocurrido un error al Adjuntar <a class='alert-link' href='#'>Memo 2 y el Pto de Cuenta.</a>
            </div>";
        }
        
        $this->view->mensaje=$mensaje;
        $this->viewDetail($param);

    }

    //PARA ACTUALIZAR ESTATUS DE LA SOLICITUD DE ASCENSO
    function updateF2($param){

        list($id_solicitud_ascenso, $id_estatus_ascenso) = explode(',', $param[0]);
//var_dump($param[0], "despues >>", $id_solicitud_ascenso, $id_estatus_ascenso);
        if($this->model->updateEstatusSolicitud([
            'id_solicitud_ascenso'=>$id_solicitud_ascenso,
            'id_estatus_ascenso'=>$id_estatus_ascenso
        ])){
            switch($id_estatus_ascenso){
                case 3:
                $mensaje='<div class="alert alert-success">
                    La Solicitud Ha sido trasladada <a class="alert-link" href="#">Exitosamente</a> a la Fase II (Resultado de Defensa).
                </div>';
                //obteniendo nombre y apellido del docente que realizo la solicitud de ascenso
                $docente=$this->model->getByIdSolicitud($id_solicitud_ascenso);
                $mensaje_correo="<p>
                    ¡Felicidades! Sr.(a) ".$docente->primer_nombre." ".$docente->primer_apellido." su Solicitud Fue Aprobada, Ahora Puede Presentar su Trabajo Ascenso 
                    y Verificar el Estatus de Su Solicitud en el Sistema Integrado de Desarrollo de Las Trabajadoras 
                    y Los Trabajadores Académicos 
                    <b>(SIDTA)</b>. 
                    <br><br>
                    </p>
                    <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";
                $correo=new Correo();
                $var=$correo->EnviarCorreo([
                    'correo_destino'=>$docente->correo,
                    'nombre_destino'=>$docente->primer_nombre,
                    'apellido_destino'=>$docente->primer_apellido,
                    'asunto'=>'Solicitud de Ascenso Aprobada - SIDTA',
                    'mensaje'=>$mensaje_correo
                ]);
               
                break;
                case 4:
                $mensaje='<div class="alert alert-success">
                    La Solicitud Ha sido Culminada <a class="alert-link" href="#">Exitosamente</a>.
                </div>';
                //obteniendo nombre y apellido del docente que realizo la solicitud de ascenso
                $docente=$this->model->getByIdSolicitud($id_solicitud_ascenso);
                $mensaje_correo="<p>
                    ¡Felicidades! Sr.(a) ".$docente->primer_nombre." ".$docente->primer_apellido." su Solicitud Fue Culminada, ahora Puede  
                     Verificar el Estatus de Su Solicitud en el Sistema Integrado de Desarrollo de Las Trabajadoras 
                    y Los Trabajadores Académicos 
                    <b>(SIDTA)</b>. 
                    <br><br>
                    </p>
                    <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";
                $correo=new Correo();
                $var=$correo->EnviarCorreo([
                    'correo_destino'=>$docente->correo,
                    'nombre_destino'=>$docente->primer_nombre,
                    'apellido_destino'=>$docente->primer_apellido,
                    'asunto'=>'Solicitud de Ascenso Culminada - SIDTA',
                    'mensaje'=>$mensaje_correo
                ]);
                break;
                case 5:
                $mensaje='<div class="alert alert-success">
                    La Solicitud Ha sido Declinada <a class="alert-link" href="#">Exitosamente</a>. Un Correo Electrónico ha sido enviado al Docente con El motivo de su declinación.
                </div>';
                $this->view->mensaje=$mensaje;
                $this->viewAdmin();
                exit();
                break;
            }
            
        }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
                Ha ocurrido un error al trasladar <a class='alert-link' href='#'>Solicitud de Ascenso</a>
            </div>";
        }
        
        echo $mensaje;

    }




///////////////// FIN propuesta de jurado ///////////////////////////



/////////////////////// actualizar memos y puntos de cuenta
    public function cambiarArchivo($param){
        list($id_solicitud_ascenso, $id_docente) = explode(',', $param[0]);
        $this->view->id_solicitud_ascenso=$id_solicitud_ascenso;
        $this->view->id_docente=$id_docente;

        $file=$_FILES['upt_file'];
        $id_tipo_documento=$_POST['id_tipo_documento'];
        $descripcion=$_POST['descripcion'];
        $ruta_rm='src/documentos/ascensos/'.$descripcion;

        if(unlink($ruta_rm)){
            
            if($this->model->updateArchivo([
                'id_solicitud_ascenso'=>$id_solicitud_ascenso,
                'file'=>$file,
                'id_tipo_documento'=>$id_tipo_documento
            ])){

                switch($id_tipo_documento){
                    case 10://MEMO SOLICITUD 1
                        $mensaje='<div class="alert alert-success">
                            El Memorandum N°1 ha sido Cambiado <a class="alert-link" href="#">Exitosamente</a>. 
                        </div>';
                        //obteniendo nombre y apellido del docente que realizo la solicitud de ascenso
                        $docente=$this->model->getByIdSolicitud($id_solicitud_ascenso);
                        //obteniendo los usuarios que tienen perfil 4 o 5 (usuarios de la DGTA)
                        $users_dgta=$this->model->getUsersDGTA();
                        //correo a cada uno de los usuarios que pertenecen a la dgta
                        foreach($users_dgta as $row){
                            $user=new Estructura;
                            $user=$row;
                            $mensaje_correo="<p>
                            Sr(a). ".$user->primer_nombre." ". $user->primer_apellido.", El Director del Centro de Estudios ha Cambiado el Memo N°1 
                            correspondiente a la Solicitud de Ascenso del Trabajador(a) Academico(a)
                            ".$docente->primer_nombre. " " . $docente->primer_apellido . ". <br>Como Administrador o Analista de la DGTA, Ya puedes validar su Veracidad.
                            <b>(SIDTA)</b>. 
                            <br><br>
                            </p>
                            <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";

                            $correo_usuario=new Correo();
                            $var=$correo_usuario->EnviarCorreo([
                            'correo_destino'=>$user->correo,
                            'nombre_destino'=>$user->primer_nombre,
                            'apellido_destino'=>$user->primer_apellido,
                            'asunto'=>'Cambio de Archivo de Solicitud - SIDTA',
                            'mensaje'=>$mensaje_correo
                            ]);
                        }
                    break;
                    case 11://MEMO SOLICITUD 2
                        $mensaje='<div class="alert alert-success">
                            El Memorandum N°2 ha sido Cambiado <a class="alert-link" href="#">Exitosamente</a>. 
                        </div>';
                        //obteniendo nombre y apellido del docente que realizo la solicitud de ascenso
                        $docente=$this->model->getByIdSolicitud($id_solicitud_ascenso);
                        //obteniendo los usuarios que tienen perfil 6 (usuarios de Vicerrectorado)
                        $users_vice=$this->model->getUsersVICE();
                        //correo a cada uno de los usuarios que pertenecen a la dgta
                        foreach($users_vice as $row){
                            $user=new Estructura;
                            $user=$row;
                            $mensaje_correo="<p>
                            Sr(a). ".$user->primer_nombre." ". $user->primer_apellido.", La DGTA Cambiado el Memo N°2 de Solicitud
                            correspondiente a la Solicitud de Ascenso del Trabajador(a) Academico(a)
                            ".$docente->primer_nombre. " " . $docente->primer_apellido . ". <br>Como Administrador o Analista de Vicerrectorado, Ya puedes validar su Veracidad.
                            <b>(SIDTA)</b>. 
                            <br><br>
                            </p>
                            <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";

                            $correo_usuario=new Correo();
                            $var=$correo_usuario->EnviarCorreo([
                            'correo_destino'=>$user->correo,
                            'nombre_destino'=>$user->primer_nombre,
                            'apellido_destino'=>$user->primer_apellido,
                            'asunto'=>'Actualizacion de Memo N°2 de Solicitud - Fase I - SIDTA',
                            'mensaje'=>$mensaje_correo
                            ]);
                        }
                    break;
                    case 12://PTO DE CUENTA 1
                        $mensaje='<div class="alert alert-success">
                            El Punto de Cuenta Ha Sido Cambiado <a class="alert-link" href="#">Exitosamente</a>. 
                        </div>';
                        $mensaje='<div class="alert alert-success">
                            El Memorandum N°2 ha sido Cambiado <a class="alert-link" href="#">Exitosamente</a>. 
                        </div>';
                        //obteniendo nombre y apellido del docente que realizo la solicitud de ascenso
                        $docente=$this->model->getByIdSolicitud($id_solicitud_ascenso);
                        //obteniendo los usuarios que tienen perfil 6 (usuarios de Vicerrectorado)
                        $users_vice=$this->model->getUsersVICE();
                        //correo a cada uno de los usuarios que pertenecen a la dgta
                        foreach($users_vice as $row){
                            $user=new Estructura;
                            $user=$row;
                            $mensaje_correo="<p>
                            Sr(a). ".$user->primer_nombre." ". $user->primer_apellido.", La DGTA ha Cambiado el Punto de Cuenta de Solicitud
                            correspondiente a la Solicitud de Ascenso del Trabajador(a) Academico(a)
                            ".$docente->primer_nombre. " " . $docente->primer_apellido . ". <br>Como Administrador o Analista de Vicerrectorado, Ya puedes validar su Veracidad.
                            <b>(SIDTA)</b>. 
                            <br><br>
                            </p>
                            <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";

                            $correo_usuario=new Correo();
                            $var=$correo_usuario->EnviarCorreo([
                            'correo_destino'=>$user->correo,
                            'nombre_destino'=>$user->primer_nombre,
                            'apellido_destino'=>$user->primer_apellido,
                            'asunto'=>'Actualizacion de Punto de Cuenta de Solicitud - Fase I - SIDTA',
                            'mensaje'=>$mensaje_correo
                            ]);
                        }
                    break;
                    case 13://MEMO R1
                    $mensaje='<div class="alert alert-success">
                        El Memorandum del Acta de Defensa N°1 ha sido Cambiado <a class="alert-link" href="#">Exitosamente</a>. 
                    </div>';
                    //obteniendo nombre y apellido del docente que realizo la solicitud de ascenso
                    $docente=$this->model->getByIdSolicitud($id_solicitud_ascenso);
                    //obteniendo los usuarios que tienen perfil 4 o 5 (usuarios de la DGTA)
                    $users_dgta=$this->model->getUsersDGTA();
                    //correo a cada uno de los usuarios que pertenecen a la dgta
                    foreach($users_dgta as $row){
                        $user=new Estructura;
                        $user=$row;
                        $mensaje_correo="<p>
                        Sr(a). ".$user->primer_nombre." ". $user->primer_apellido.", El Director del Centro de Estudios ha Cambiado el Memo de Resolución N°1 
                        correspondiente a la Solicitud de Ascenso del Trabajador(a) Academico(a)
                        ".$docente->primer_nombre. " " . $docente->primer_apellido . ". <br>Como Administrador o Analista de la DGTA, Ya puedes validar su Veracidad.
                        <b>(SIDTA)</b>. 
                        <br><br>
                        </p>
                        <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";

                        $correo_usuario=new Correo();
                        $var=$correo_usuario->EnviarCorreo([
                        'correo_destino'=>$user->correo,
                        'nombre_destino'=>$user->primer_nombre,
                        'apellido_destino'=>$user->primer_apellido,
                        'asunto'=>'Cambio de Archivo de Resolución - Fase II - SIDTA',
                        'mensaje'=>$mensaje_correo
                        ]);
                    }
                    break;
                    case 14://MEMO R2 fase 2
                        $mensaje='<div class="alert alert-success">
                            El Memorandum del Acta de Defensa N°2 ha sido Cambiado <a class="alert-link" href="#">Exitosamente</a>.
                        </div>';
                        //obteniendo nombre y apellido del docente que realizo la solicitud de ascenso
                        $docente=$this->model->getByIdSolicitud($id_solicitud_ascenso);
                        //obteniendo los usuarios que tienen perfil 6 (usuarios de Vicerrectorado)
                        $users_vice=$this->model->getUsersVICE();
                        //correo a cada uno de los usuarios que pertenecen a la dgta
                        foreach($users_vice as $row){
                            $user=new Estructura;
                            $user=$row;
                            $mensaje_correo="<p>
                            Sr(a). ".$user->primer_nombre." ". $user->primer_apellido.", La DGTA Cambiado el Memo de Resolución de Defensa N°2,
                            correspondiente a la Solicitud de Ascenso del Trabajador(a) Academico(a)
                            ".$docente->primer_nombre. " " . $docente->primer_apellido . ". <br>Como Administrador o Analista de Vicerrectorado, Ya puedes validar su Veracidad.
                            <b>(SIDTA)</b>. 
                            <br><br>
                            </p>
                            <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";

                            $correo_usuario=new Correo();
                            $var=$correo_usuario->EnviarCorreo([
                            'correo_destino'=>$user->correo,
                            'nombre_destino'=>$user->primer_nombre,
                            'apellido_destino'=>$user->primer_apellido,
                            'asunto'=>'Actualizacion de Memo N°2 de Resolución - Fase II - SIDTA',
                            'mensaje'=>$mensaje_correo
                            ]);
                        }
                    break;
                    case 15://ACTA DE DEFENSA
                        $mensaje='<div class="alert alert-success">
                        El Acta de Defensa ha sido Cambiada <a class="alert-link" href="#">Exitosamente</a>.
                        </div>';
                        // correo al docente
                        $docente=$this->model->getByIdDocente($id_docente);
                         // obtener correo y datos personales del coordinador del CE
                        $coord=$this->model->getCorreoCoord([
                            'id_centro_estudio'=>$docente->id_centro_estudio,
                            'id_perfil'=>3//id_perfil 3=Director del centro de estudio
                        ]);

                        $mensaje_coord="<p>
                        El Coordinador del Centro de Estudio ha Cambiado el Acta de Defensa
                        correspondiente a la Solicitud de Ascenso del Trabajador(a) Academico(a) ".
                        $docente->primer_nombre. " " . $docente->primer_apellido . ".
                        Dentro del Sistema Integrado de Desarrollo de Las Trabajadoras 
                        y Los Trabajadores Académicos. <br>Como Director de Centro de Estudio Ya puedes Verificar el Acta.
                        <b>(SIDTA)</b>. 
                        <br><br>
                        </p>
                        <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";
                        $correo_coord=new Correo();
                        $var2=$correo_coord->EnviarCorreo([
                            'correo_destino'=>$coord->correo,
                            'nombre_destino'=>$coord->primer_nombre,
                            'apellido_destino'=>$coord->primer_apellido,
                            'asunto'=>'Cambio de Acta de Defensa - Fase II - SIDTA',
                            'mensaje'=>$mensaje_coord
                            ]);
                
                    //fin correo al coordinadordel CE
                    break;
                    case 16://PTO DE CUENTA RESOLUCION 2
                        $mensaje='<div class="alert alert-success">
                            El Punto de Cuenta de Resolución Ha Sido Cambiado <a class="alert-link" href="#">Exitosamente</a>. 
                        </div>';
                        //obteniendo nombre y apellido del docente que realizo la solicitud de ascenso
                        $docente=$this->model->getByIdSolicitud($id_solicitud_ascenso);
                        //obteniendo los usuarios que tienen perfil 6 (usuarios de Vicerrectorado)
                        $users_vice=$this->model->getUsersVICE();
                        //correo a cada uno de los usuarios que pertenecen a la dgta
                        foreach($users_vice as $row){
                            $user=new Estructura;
                            $user=$row;
                            $mensaje_correo="<p>
                            Sr(a). ".$user->primer_nombre." ". $user->primer_apellido.", La DGTA Cambiado el Punto de Cuenta de Resolución de Defensa N°2,
                            correspondiente a la Solicitud de Ascenso del Trabajador(a) Academico(a)
                            ".$docente->primer_nombre. " " . $docente->primer_apellido . ". <br>Como Administrador o Analista de Vicerrectorado, Ya puedes validar su Veracidad.
                            <b>(SIDTA)</b>. 
                            <br><br>
                            </p>
                            <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";

                            $correo_usuario=new Correo();
                            $var=$correo_usuario->EnviarCorreo([
                            'correo_destino'=>$user->correo,
                            'nombre_destino'=>$user->primer_nombre,
                            'apellido_destino'=>$user->primer_apellido,
                            'asunto'=>'Actualizacion de Pto. Cuenta de Resolución - Fase II - SIDTA',
                            'mensaje'=>$mensaje_correo
                            ]);
                        }
                    break;
                
                }

                
            }else{

                switch($id_tipo_documento){
                    case 10://MEMO SOLICITUD 1
                        $mensaje='<div class="alert alert-danger">
                            <b><i>ERROR:</i></b> Ha Ocurrido un Error al Intentar <a class="alert-link" href="#">Cambiar</a> El Memorandum de Solicitud N°1.
                        </div>';
                    break;
                    case 11://MEMO SOLICITUD 2
                        $mensaje='<div class="alert alert-danger">
                        <b><i>ERROR:</i></b> Ha Ocurrido un Error al Intentar <a class="alert-link" href="#">Cambiar</a> El Memorandum de Solicitud N°2.

                        </div>';
                    break;
                    case 12://PTO DE CUENTA 1
                        $mensaje='<div class="alert alert-danger">
                            <b><i>ERROR:</i></b> Ha Ocurrido un Error al Intentar <a class="alert-link" href="#">Cambiar</a> El Punto de Cuenta de Solicitud.
                        </div>';
                    break;
                    case 13://MEMO R1
                    $mensaje='<div class="alert alert-danger">
                        <b><i>ERROR:</i></b> Ha Ocurrido un Error al Intentar <a class="alert-link" href="#">Cambiar</a> El Memorandum de Resolución N°1.
                    </div>';
                    break;
                    case 14://MEMO R2 fase 3
                    $mensaje='<div class="alert alert-danger">
                        <b><i>ERROR:</i></b> Ha Ocurrido un Error al Intentar <a class="alert-link" href="#">Cambiar</a> El Memorandum de Resolución N°2.
                    </div>';
                    break;
                    case 15://ACTA DE DEFENSA
                    $mensaje='<div class="alert alert-danger">
                        <b><i>ERROR:</i></b> Ha Ocurrido un Error al Intentar <a class="alert-link" href="#">Cambiar</a> El Acta de Defensa.
                    </div>';
                    break;
                    case 16://PTO DE CUENTA RESOLUCION 2
                        $mensaje='<div class="alert alert-danger">
                            <b><i>ERROR:</i></b> Ha Ocurrido un Error al Intentar <a class="alert-link" href="#">Cambiar</a> El Punto de Cuenta de Resolución.
                        </div>';
                    break;
                }

            }
        }

        $this->view->mensaje=$mensaje;
        $this->viewDetail($param);

    }



    function declinar($param){

        list($id_solicitud_ascenso, $id_docente) = explode(',', $param[0]);

        $motivo=$_POST['motivo'];
        $tipo_declinar=$_POST['tipo_declinar'];

        if($this->model->insertDeclinar([
            'descripcion'=>$motivo,
            'id_solicitud_ascenso'=>$id_solicitud_ascenso,
            'id_tipo_declinar'=>$tipo_declinar
            ])){
                //QUEDO AQUI
                $i=5;
                $dato=array($id_solicitud_ascenso.",".$i);
                $mensaje_correo='Su solicitud de Ascenso ha sido declinada debido a ' . $motivo;
                $docente=$this->model->getByIdDocente($id_docente);
                
                //correo para notificar al docente que se ha declinado su solicitud
                $correo=new Correo();
                $var=$correo->EnviarCorreo([
                    'correo_destino'=>$docente->correo,
                    'nombre_destino'=>$docente->primer_nombre,
                    'apellido_destino'=>$docente->primer_apellido,
                    'asunto'=>'Declinación de Solicitud de Ascenso - SIDTA',
                    'mensaje'=>$mensaje_correo
                    ]);

                // fin correo para notificar al docente que se ha declinado su solicitud
                //funcion para cambiar el estatus
                $this->updateF2($dato);
                
            
        }else{
            $mensaje='<div class="alert alert-danger">
                <b><i>ERROR:</i></b> Ha Ocurrido un Error al Intentar <a class="alert-link" href="#">Declinar</a> La Propuesta de Jurado.
            </div>';
            $this->view->mensaje=$mensaje;
            $this->viewDetail($param);
            exit();
        }
        
        
    }

}
?>