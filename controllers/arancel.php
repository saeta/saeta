<?php

include_once 'sesiones/session_admin.php';


class arancel extends Controller{
    
    function __construct(){
        parent::__construct();
      //  $this->view->render('ayuda/index');
      $this->view->aranceles=[];
    }

    function render(){


        $aranceles=$this->model->get();
        $this->view->aranceles=$aranceles;
        $this->view->render('ingpropios/arancel');
    }




    function RegistrarArancel(){//se agrega esta linea  
        //$id_persona=$_SESSION["id_persona"];

        $arancel=$_POST['arancel'];
        $fechai=$_POST['fechai'];
        $fechaf=$_POST['fechaf'];
        $estatus=$_POST['estatus'];
        $cantidad=$_POST['cantidad'];
        $modo=$_POST['modo'];
        $codigo=$_POST['codigo'];
       //  var_dump($arancel);
        // var_dump($cantidad);
         //var_dump($fechai);
        //var_dump($fechaf);
        //var_dump($estatus);
           //var_dump($codigo);
       // var_dump($modo);
         if($var=$this->model->existe($codigo)){
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            el codigo  <b>" . $codigo . "</b>  ya esta registrado por favor verifique e intente nuevamente <a class='alert-link' href='#'></a>
            </div>";
            $this->view->mensaje=$mensaje;
            $this->render();
            exit();
        }
        
        if($this->model->insert(['arancel'=>$arancel,'fechai'=>$fechai,'fechaf'=>$fechaf,'cantidad'=>$cantidad,'estatus'=>$estatus,'modo'=>$modo,'codigo'=>$codigo])){    
    /*      if ($datos['nomb_archivo'] != "") {
              copy($datos['ruta'], $datos['destino']);
             }*/

          $mensaje="<div class='alert alert-success alert-dismissable'>
          <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
          arancel agregado<a class='alert-link' href='#'></a></div>";
        
      }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Ha ocurrido un error al agregar una arancel <a class='alert-link' href='#'></a>

        </div>";
        }
        $this->view->mensaje=$mensaje;
        $this->render();
        
       // echo "Nuevo Alumno Creado";
        //$this->model->insert();//se agrega esta linea
      
      }


      function ActualizarArancel(){//se agrega esta linea
          
        //$id_persona=$_SESSION["id_persona"];
        $id_arancel2=$_POST['id_arancel2'];
        $arancel2=$_POST['arancel2'];
        $fechai2=$_POST['fechai2'];
        $fechaf2=$_POST['fechaf2'];
        $estatus2=$_POST['estatus2'];
        $cantidad2=$_POST['cantidad2'];
        $modo2=$_POST['modo2'];
        $codigo2=$_POST['codigo2'];
        
            //var_dump($id_arancel);
            //var_dump($arancel2);
            //var_dump($fechai2);
            //var_dump($fechaf2);
            //var_dump($estatus2);
            //var_dump($cantidad2);
            //var_dump($modo2);
            //var_dump($codigo2);

        

        $mensaje="";

        if($this->model->update(['id_arancel2'=>$id_arancel2,'arancel2'=>$arancel2,'fechai2'=>$fechai2,'fechaf2'=>$fechaf2,'cantidad2'=>$cantidad2,
        'estatus2'=>$estatus2,'modo2'=>$modo2,'codigo2'=>$codigo2])){
          

    /*      if ($datos['nomb_archivo'] != "") {
              copy($datos['ruta'], $datos['destino']);
             }*/

          $mensaje="<div class='alert alert-success alert-dismissable'>
          <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
          arancel <b> ".$codigo2." </b>Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
        
      }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Ha ocurrido un error al Actualizar el arancel<b> ".$codigo2." </b><a class='alert-link' href='#'></a>
        </div>";
        }
        $this->view->mensaje=$mensaje;
        $this->render();
        
       // echo "Nuevo Alumno Creado";
        //$this->model->insert();//se agrega esta linea
      
      }

}
?>