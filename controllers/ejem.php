<?php 
//33163 extension de prensa 
include_once 'sesiones/session_admin.php';

    class Ejem extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->ejesRegionales=[];
            $this->view->ejesMunicipales=[];
        }

        function render(){ 

            $ejesMunicipales=$this->model->get();
            $this->view->ejesMunicipales=$ejesMunicipales;

            $ejesRegionales=$this->model->getEjeRegional();
            $this->view->ejesRegionales=$ejesRegionales;          

            $this->view->render('estructura/ejem');        
        }



        function registrarEjeMunicipal(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
    
            $codigoejem=$_POST['codigoejem'];
            $ejemunicipal=$_POST['ejemunicipal'];
            $ejeregional=$_POST['ejeregional'];            
            $estatus=$_POST['estatus'];
            
            $mensaje="";


            if($var=$this->model->existe($codigoejem)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Eje Regional <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
    
    
            if($this->model->insert(['codigoejem'=>$codigoejem,'ejemunicipal'=>$ejemunicipal,'ejeregional'=>$ejeregional,'estatus'=>$estatus])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Eje Regional registrado<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al registrar Eje Regional<a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }

          function ActualizarEjeMunicipal(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
    
            $id_eje_municipal=$_POST['id_eje_municipaledit'];
            $codigoeje=$_POST['codigoejemedit'];
            $ejemunicipal=$_POST['ejemunicipaledit'];
            $id_eje_regional=$_POST['ejeregionaledit'];          
            $estatus=$_POST['estatusejeedit'];
            
            //var_dump( $codigoine,$siglasine);
            $mensaje="";


           
    
            if($this->model->update(['id_eje_municipal'=>$id_eje_municipal,'codigoeje'=>$codigoeje,'ejemunicipal'=>$ejemunicipal,'id_eje_regional'=>$id_eje_regional,'estatus'=>$estatus])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Eje Regional <b> ".$eje." </b>Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar  Eje Regional <b> ".$eje." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }


    }

?>