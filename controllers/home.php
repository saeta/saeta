<?php
include_once 'sesiones/session_admin.php';

class Home extends Controller{
    
    function __construct(){
        parent::__construct();
      //  $this->view->render('ayuda/index');
    }

    function render(){

        $fechas=$this->model->getModAscenso();
        $_SESSION['fecha_inicio']=$fechas['fecha_inicio'];
        $_SESSION['fecha_fin']=$fechas['fecha_fin'];
        
        $varconcurso=$this->model->getCantidadConcursos();
        $this->view->varconcurso=$varconcurso;

        $varascensos=$this->model->getCantidadAscensos();
        $this->view->varascensos=$varascensos;


        $preypostgrado=$this->model->getCantidadPostPreGrado();
        $this->view->preypostgrado=$preypostgrado;


        $ordinarios=$this->model->getDocentesOrdinarios();
        $this->view->ordinarios=$ordinarios;

        $contratados=$this->model->getDocentesContratados();
        $this->view->contratados=$contratados;


        $conducentes=$this->model->getEstudioConducente();
        $this->view->conducentes=$conducentes;

        $noconducentes=$this->model->getEstudioNoconducente();
        $this->view->noconducentes=$noconducentes;

        $idioma=$this->model->getIdioma();
        $this->view->idioma=$idioma;












        $this->view->render('home/index');
    }

    function notify(){
        
        $solicitudes=$this->model->getSolicitudes();
        echo '<div class="alert alert-success">
            Tienes <a class="alert-link" href="#">'.count($solicitudes) .' Solicitudes </a> de Ascenso Sin Revisar.
            <a class="alert-link" href="'.constant('URL').'solicitar_ascenso/viewAdmin">Click Aquí</a> para Revisarlas! 
        </div> ';
        /*foreach($solicitudes as $row){
            $solicitud=new Estructura();
            $solicitud=$row;
            echo $solicitud->id_solicitud_ascenso; 
        }*/
    }

    function notifySolicitud(){
        
        $propuestas=$this->model->getPropuestas();
        echo '<div class="alert alert-success">
            Tienes <a class="alert-link" href="#">'.count($propuestas) .' Propuestas de Jurado </a> Sin Revisar.
            <a class="alert-link" href="'.constant('URL').'solicitar_ascenso/viewAdmin">Click Aquí</a> para Revisarlas! 
        </div> ';
        /*foreach($solicitudes as $row){
            $solicitud=new Estructura();
            $solicitud=$row;
            echo $solicitud->id_solicitud_ascenso; 
        }*/
    }

    function notifyMemos($param=null){
        $id_tipo_documento=$param[0];
        switch($id_tipo_documento){
            case 10:
            $memos=$this->model->getMemos($id_tipo_documento);
            echo '<div class="alert alert-success">
                Tienes <a class="alert-link" href="#">'.count($memos) .' Memorandum de Solicitud N°1 </a> Sin Revisar.
                <a class="alert-link" href="'.constant('URL').'solicitar_ascenso/viewAdmin">Click Aquí</a> para Revisarlas! 
            </div> ';
            break;
            case 11:
            $memoS2=$this->model->getMemos($id_tipo_documento);
            echo '<div class="alert alert-success">
                Tienes <a class="alert-link" href="#">'.count($memoS2) .' Memorandum de Solicitud N°2</a>  Sin Revisar.
                <a class="alert-link" href="'.constant('URL').'solicitar_ascenso/viewAdmin">Click Aquí</a> para Revisarlas! 
            </div> ';
            break;
            case 12:
            $ptoS1=$this->model->getMemos($id_tipo_documento);
            echo '<div class="alert alert-success">
                Tienes <a class="alert-link" href="#">'.count($ptoS1) .' Puntos de Cuenta de Solicitud N°1</a>  Sin Revisar.
                <a class="alert-link" href="'.constant('URL').'solicitar_ascenso/viewAdmin">Click Aquí</a> para Revisarlas! 
            </div> ';
            break;
            case 14://memorandum acta de defensa 2
            $memo_R2=$this->model->getMemos($id_tipo_documento);
            echo '<div class="alert alert-success">
                Tienes <a class="alert-link" href="#">'.count($memo_R2) .' Memorandum de Resolución N°2</a>  Sin Revisar.
                <a class="alert-link" href="'.constant('URL').'solicitar_ascenso/viewAdmin">Click Aquí</a> para Revisarlas! 
                </div> ';
            break;
            case 15:
            $actaD=$this->model->getMemos($id_tipo_documento);
            
            echo '<div class="alert alert-success">
                Tienes <a class="alert-link" href="#">'.count($actaD) .' Actas de Defensa</a>  Sin Revisar.
                <a class="alert-link" href="'.constant('URL').'solicitar_ascenso/viewAdmin">Click Aquí</a> para Revisarlas! 
            </div> ';
            break;
            case 16://Punto de Cuenta de Resolucion1 
            $pto_cuentaR1=$this->model->getMemos($id_tipo_documento);
            echo '<div class="alert alert-success">
                Tienes <a class="alert-link" href="#">'.count($pto_cuentaR1) .' Punto de Cuenta de Resolución N°2</a> Sin Revisar.
                <a class="alert-link" href="'.constant('URL').'solicitar_ascenso/viewAdmin">Click Aquí</a> para Revisarlas! 
                </div> ';
            break;
        }
        
        
    }

    

}
?>