<?php
include_once 'sesiones/session_admin.php';

    class TipoDiscapacidad extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->discapacidades=[];
  
        }

        function render(){ 
            $discapacidades=$this->model->get();
            $this->view->discapacidades=$discapacidades;
            $this->view->render('persona/tipodiscapacidad');        
        }



        function registrartipodiscapacidad(){//se agrega esta linea  
            //$id_persona=$_SESSION["id_persona"];
    
            $discapacidad=$_POST['discapacidad'];
          

      //  var_dump($discapacidad);
     
          

            $mensaje="";

            if($var=$this->model->existe($discapacidad)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El tipo  discapacidad <b>" . $discapacidad . "</b>  ya esta registrado <a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($this->model->insert(['discapacidad'=>$discapacidad,'id_tipo_discapacidad'=>$id_tipo_discapacidad])){
           
               
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              discapacidad agregado<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al agregar una discapacidad <a class='alert-link' href='#'></a>

            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }
          



          function Actualizartipodiscapacidad(){//se agrega esta linea
          
            //$id_persona=$_SESSION["id_persona"];
            $discapacidad2=$_POST['discapacidad2'];
            $id_tipo_discapacidad2=$_POST['id_tipo_discapacidad2'];

            
           // var_dump($id_tipo_discapacidad);
           // var_dump($discapacidad2);
      //break;
            //var_dump( $codigoine,$siglasine);
            $mensaje="";
            if($var=$this->model->existe($discapacidad)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El tipo  discapacidad <b>" . $discapacidad2 . "</b>  ya esta registrado <a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }

            if($this->model->update(['id_tipo_discapacidad2'=>$id_tipo_discapacidad2,'discapacidad2'=>$discapacidad2])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              discapacidad <b> ".$discapacidad2." </b>Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar el discapacidad<b> ".$discapacidad2." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }

          function aggtipodiscapacidad($param=null){
            $cadena=$param[0];
            list($id_tipo_discapacidad) = explode(',', $cadena);
      
            if($this->model->delete($id_tipo_discapacidad)){

                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
               Removido correctamente <b>  </b><a class='alert-link' href='#'></a>
            </div>";

            $this->view->mensaje=$mensaje;
            $this->render();

               ?>
    
               
    
                <?php
    
           }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
           No se pudo remover <b> </b><a class='alert-link' href='#'></a>
            </div>";

            $this->view->mensaje=$mensaje;
            $this->render();
                ?>
    
                
    
                <?php
             
           }
           echo $mensaje;
        }




    }

?>