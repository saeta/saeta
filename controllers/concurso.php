<?php
include_once 'sesiones/session_admin.php';

clasS Concurso extends Controller{
    
    function __construct(){
        parent::__construct();
      //  $this->view->render('ayuda/index');
    }

    function render(){
        $concursos=$this->model->get($_SESSION['id_docente']);
        $this->view->concursos=$concursos;

        $this->view->render('concurso/index');
    }

    function viewVer($param = null){

        $id_concurso=$param[0];
        $_SESSION['id_concurso']= $id_concurso;
       
        $concursos=$this->model->getbyId($id_concurso);
        $this->view->id_concurso=$concursos->id_concurso;

        $this->view->concursos=$concursos;
       
       // $this->view->render('concurso/index');

        $this->view->render('concurso/ver_concurso');
    }


    function viewAdd(){

        //obtener modalidades de estudio
        $escalafones=$this->model->getCatalogo('escalafon');
        $this->view->escalafones=$escalafones;

        $this->view->render('concurso/agregar_concurso');
    }


    function viewEdit($param = null) {
        $id_concurso=$param[0];
      

        $concursos=$this->model->getbyId($id_concurso);
        $this->view->concursos=$concursos;
        $this->view->id_concurso= $id_concurso;

        $escalafones=$this->model->getCatalogo('escalafon');
        $this->view->escalafones=$escalafones;
    
       $this->view->render('concurso/editar_concurso');

    }

    
    function viewDocumento($param = null){

        $id_concurso=$param[0];
        $documento=$this->model->getbyId($id_concurso);
        $this->view->documento="concursos/".rawurlencode($documento->documento);
        $this->view->render('documentos/viewDocumento');
    
    }

    function registrar(){

        $escalafon=$_POST['escalafon'];
        $pdf_resolucion=$_FILES['pdf_resolucion'];
        $fecha_concurso=$_POST['ano_r'];
        
        if($var=$this->model->existe($_SESSION['id_docente'])){
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
             Ya usted ha registrado su <a class='alert-link' href='#'>resolución de concurso.</a> 
            </div>";
            $this->view->mensaje=$mensaje;
            $this->viewAdd();
            exit();
        }

        if($this->model->insert([
            'id_escalafon'=>$escalafon,
            'pdf_resolucion'=>$pdf_resolucion,
            'fecha_concurso'=>date("Y-m-d", strtotime($fecha_concurso)),
            'id_docente'=>$_SESSION['id_docente'],
            'id_persona'=>$_SESSION['id_persona']
            ])){
            $mensaje='<div class="alert alert-success">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>                        El concurso ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
            </div>';
        }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al registrar el <a class='alert-link' href='#'>Concurso de Oposición</a>
            </div>";
        }
        $this->view->mensaje=$mensaje;
        $this->render();
    
    }


    function editarConcurso($param){
       
        $id_concurso=$param[0];
        $id_escalafon=$_POST['id_escalafon'];
        $fecha_concurso=$_POST['fecha_concurso'];
        $escalafon=$_POST['escalafon'];
      

        if($this->model->update([
            'id_concurso'=>$id_concurso,
            'id_escalafon'=>$escalafon,
            'fecha_concurso'=>date("Y-m-d", strtotime($fecha_concurso)),
            'id_docente'=>$_SESSION['id_docente'],
            'id_persona'=>$_SESSION['id_persona']
        
            ])){    


      $mensaje="<div class='alert alert-success alert-dismissable'>
      <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
      el concurso fue Actualizado <a class='alert-link' href='#'>Correctamente</a></div>";
    
  }else{
        $mensaje="<div class='alert alert-danger alert-dismissable'>
        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
        Ha ocurrido un error al Actualizar el concurso <a class='alert-link' href='#'></a>
    </div>";
    }



    $this->view->mensaje=$mensaje;
    $this->render($param);
   // $this->viewEditar();
}













    function delete($param){

        $id_concurso=$param[0];

        if($this->model->delete($id_concurso)){
            $mensaje="<div class='alert alert-success alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            <b>El Concurso </b> ha sido Removido correctamente! <a class='alert-link' href='#'></a>
        </div>";

       }else{
        $mensaje="<div class='alert alert-danger alert-dismissable'>
        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            <b>ERROR: </b>Ha ocurrido un error al intentar remover el <a class='alert-link' href='#'>Concurso</a>
        </div>";
 
       }
        $this->view->mensaje=$mensaje;
        $this->render();
 
    }


}
?>