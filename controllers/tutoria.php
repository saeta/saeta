<?php
//ini_set("display_errors", 1);
include_once 'models/datosacademicos/tutoria.php';
include_once 'models/datosacademicos/docente.php';
include_once 'models/datosacademicos/areaubv.php';
include_once 'models/datosacademicos/lineainvestigacion.php';
include_once 'models/datosacademicos/nucleo_academico.php';
include_once 'models/datosacademicos/centro_estudio.php';
include_once 'models/datosacademicos/programa_formacion.php';
include_once 'models/estructura.php';
include_once 'sesiones/session_admin.php';



class Tutoria extends Controller{
    function __construct(){
        parent::__construct();
        $this->view->mensaje="";
        $this->view->docentes=[];
        $this->view->areasubv=[];
        $this->view->lineas=[];
        $this->view->nucleos=[];
        $this->view->centros=[];
        $this->view->programas=[];
        $this->view->regionales=[];
        $this->view->tutorias=[];
        
       
    }

    function render(){
        
        $docentes=$this->model->getDocente();
        $this->view->docentes=$docentes;
        $areasubv=$this->model->getUbv();
        $this->view->areasubv=$areasubv;
        $lineas=$this->model->getLinea();
        $this->view->lineas=$lineas;
        $nucleos=$this->model->getNucleo();
        $this->view->nucleos=$nucleos;
        $centros=$this->model->getCentro();
        $this->view->centros=$centros;
        $programas=$this->model->getProgramaF();
        $this->view->programas=$programas;
        $regionales=$this->model->getEjeRegional();
        $this->view->regionales=$regionales;
        $tutorias=$this->model->getTutoria($_SESSION['identificacion']);
        $this->view->tutorias=$tutorias;
        //var_dump( $tutorias);
        $this->view->render('desempeno_docente/tutoria');
    }


    function viewRegistrar(){
        $docentes=$this->model->getDocente();
        $this->view->docentes=$docentes;
        $areasubv=$this->model->getUbv();
        $this->view->areasubv=$areasubv;
        $lineas=$this->model->getLinea();
        $this->view->lineas=$lineas;
        $nucleos=$this->model->getNucleo();
        $this->view->nucleos=$nucleos;
        $centros=$this->model->getCentro();
        $this->view->centros=$centros;
        $programas=$this->model->getProgramaF();
        $this->view->programas=$programas;
        $regionales=$this->model->getEjeRegional();
        $this->view->regionales=$regionales;
        $tutorias=$this->model->getTutoria($_SESSION['identificacion']);
        $this->view->tutorias=$tutorias;
        $this->view->render('desempeno_docente/agregar_tutoria');
}
  

    function viewEditar($param = null ){
        $id_tutoria=$param[0];
        $docentes=$this->model->getDocente($id_tutoria);
        $this->view->docentes=$docentes;
        $areasubv=$this->model->getUbv($id_tutoria);
        $this->view->areasubv=$areasubv;
        $lineas=$this->model->getLinea($id_tutoria);
        $this->view->lineas=$lineas;
        $nucleos=$this->model->getNucleo($id_tutoria);
        $this->view->nucleos=$nucleos;
        $centros=$this->model->getCentro($id_tutoria);
        $this->view->centros=$centros;
        $programas=$this->model->getProgramaF($id_tutoria);
        $this->view->programas=$programas;
        $regionales=$this->model->getEjeRegional($id_tutoria);
        $this->view->regionales=$regionales;
        $tutorias=$this->model->getbyId($id_tutoria);
        $_SESSION['id_tutoria']= $id_tutoria;
        $this->view->tutorias=$tutorias;
        //var_dump($_SESSION['id_tutoria']);
        $this->view->render('desempeno_docente/editar_tutoria');
}

function viewVer($param = null ){
    $id_tutoria=$param[0];

    $docentes=$this->model->getDocente($id_tutoria);
    $this->view->docentes=$docentes;
    $areasubv=$this->model->getUbv($id_tutoria);
    $this->view->areasubv=$areasubv;
    $lineas=$this->model->getLinea($id_tutoria);
    $this->view->lineas=$lineas;
    $nucleos=$this->model->getNucleo($id_tutoria);
    $this->view->nucleos=$nucleos;
    $centros=$this->model->getCentro($id_tutoria);
    $this->view->centros=$centros;
    $programas=$this->model->getProgramaF($id_tutoria);
    $this->view->programas=$programas;
    $regionales=$this->model->getEjeRegional($id_tutoria);
    $this->view->regionales=$regionales;
    $tutorias=$this->model->getbyId($id_tutoria);
    $_SESSION['id_tutoria']= $id_tutoria;
    $this->view->tutorias=$tutorias;
    //var_dump($tutorias);
    $this->view->render('desempeno_docente/ver_tutoria');
}


    function registrarTutoria(){

        if(isset($_POST['registrar'])){
            $ano_tutoria=$_POST['ano_tutoria'];
            $periodo_lectivo=$_POST['periodo_lectivo'];
            if($_POST['estatus_verificacion'] == 'Verificado'){
                $estatus_verificacion='Verificado';
            }elseif($_POST['estatus_verificacion'] == 'Sin Verificar'){
                $estatus_verificacion='Sin Verificar';
            }
            $id_area_conocimiento_ubv=$_POST['id_area_conocimiento_ubv'];
            $id_linea_investigacion=$_POST['id_linea_investigacion'];
            $id_nucleo_academico=$_POST['id_nucleo_academico'];
            $id_centro_estudio=$_POST['id_centro_estudio'];
            $id_programa=$_POST['id_programa'];
            $id_eje_regional=$_POST['id_eje_regional'];
            $nro_estudiantes=$_POST['nro_estudiantes'];
            $descripcion=$_POST['descripcion'];
            $id_docente=$_SESSION['id_docente'];
            
            //var_dump($estatus_verificacion);
            if(empty($descripcion)){
                $mensaje='<div class="alert alert-danger">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            La Tutoria es . <a class="alert-link" href="#">Requerido</a>.
                        </div>';
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($var=$this->model->existe($descripcion)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                La Tutoria <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }

            if($this->model->insert([
                'ano_tutoria'=>$ano_tutoria,
                'periodo_lectivo'=>$periodo_lectivo,
                'estatus_verificacion'=>$estatus_verificacion,
                'id_docente'=>$id_docente,
                'id_area_conocimiento_ubv'=>$id_area_conocimiento_ubv,
                'id_linea_investigacion'=>$id_linea_investigacion,
                'id_nucleo_academico'=>$id_nucleo_academico,
                'id_centro_estudio'=>$id_centro_estudio,
                'id_programa'=>$id_programa,
                'id_eje_regional'=>$id_eje_regional,
                'nro_estudiantes'=>$nro_estudiantes,
                'descripcion'=>$descripcion,
                'id_persona'=>$_SESSION['id_persona']

                ])){
                    $mensaje='<div class="alert alert-success">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    La Tutoria  '.$descripcion.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                        </div>';
                }else{
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                        Ha ocurrido un error al registrar La Tutoria<a class='alert-link' href='#'></a>
                    </div>";
                }
            $this->view->mensaje=$mensaje;
            $this->viewRegistrar();
        }
        
    }

    function editarTutoria($param){
        

            $id_tutoria=$_POST['id_tutoria'];
            $ano_tutoria=$_POST['ano_tutoria'];
            $periodo_lectivo=$_POST['periodo_lectivo'];
            $id_area_conocimiento_ubv=$_POST['id_area_conocimiento_ubv'];
            $id_linea_investigacion=$_POST['id_linea_investigacion'];
            $id_nucleo_academico=$_POST['id_nucleo_academico'];
            $id_centro_estudio=$_POST['id_centro_estudio'];
            $id_programa=$_POST['id_programa'];
            $id_eje_regional=$_POST['id_eje_regional'];
            $nro_estudiantes=$_POST['nro_estudiantes'];
            $descripcion=$_POST['descripcion'];
            $id_docente=$_SESSION['id_docente'];
            
            //var_dump($id_tutoria);
            //var_dump($id_tutoria,$ano_tutoria,$periodo_lectivo,$id_area_conocimiento_ubv,$id_linea_investigacion,$id_nucleo_academico,$id_centro_estudio,$id_programa,$id_eje_regional,$nro_estudiantes,$descripcion);
            
            if($this->model->update([
                'id_tutoria'=>$id_tutoria,
                'ano_tutoria'=>$ano_tutoria,
                'periodo_lectivo'=>$periodo_lectivo,
                'id_area_conocimiento_ubv'=>$id_area_conocimiento_ubv,
                'id_linea_investigacion'=>$id_linea_investigacion,
                'id_nucleo_academico'=>$id_nucleo_academico,
                'id_centro_estudio'=>$id_centro_estudio,
                'id_programa'=>$id_programa,
                'id_eje_regional'=>$id_eje_regional,
                'nro_estudiantes'=>$nro_estudiantes,
                'descripcion'=>$descripcion,
                'id_docente'=>$id_docente,
                'id_persona'=>$_SESSION['id_persona']

                
                ])){

                    $mensaje="<div class='alert alert-success alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
                    La Tutoria <b> ".$descripcion." </b> fue Actualizada <a class='alert-link' href='#'>Correctamente</a></div>";
                }else{
                      $mensaje="<div class='alert alert-danger alert-dismissable'>
                      <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                      Ha ocurrido un error al Actualizar la tutoria <b> ".$descripcion." </b><a class='alert-link' href='#'></a>
                  </div>";
                  }
          
        
          
                  $this->view->mensaje=$mensaje;
                  $this->viewEditar($param);
}


      function removerTutoria($param=null){
        
        $id_tutoria=$param[0];
  
        if($this->model->delete($id_tutoria)){

            $mensaje="<div class='alert alert-success alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
           Removido correctamente la tutoria<b>  </b><a class='alert-link' href='#'></a>
        </div>";

       }else{
        $mensaje="<div class='alert alert-danger alert-dismissable'>
        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
       No se pudo remover la tutoria<b> </b><a class='alert-link' href='#'></a>
        </div>";
 
       }
       echo $mensaje;
    }

    
}
?>