<?php
include_once 'sesiones/session_admin.php';

class Aldea_programa extends Controller{
    function __construct(){
        parent::__construct();
        $this->view->mensaje="";
        $this->view->aldeas=[];
        $this->view->programas=[];
        $this->view->aldeaprogramas=[];
        
       
    }

    function render(){
        $aldeas=$this->model->getAldea();
        $this->view->aldeas=$aldeas;
        $programas=$this->model->getProgramaF();
        $this->view->programas=$programas;
        $aldeaprogramas=$this->model->getAldeaP();
        $this->view->aldeaprogramas=$aldeaprogramas;
        $this->view->render('adm_datos_academicos/aldea_programa');
    }

    function registrar(){

        if(isset($_POST['registrar'])){
            
            $codigo_opsu=$_POST['codigo_opsu'];
            $id_aldea=$_POST['id_aldea'];
            $id_programa=$_POST['id_programa'];
            if($_POST['estatus'] == 'Activo'){
                $estatus=1;
            }elseif($_POST['estatus'] == 'Inactivo'){
                $estatus=0;
            }

            if($var=$this->model->existe($codigo_opsu)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                 El Código OPSU <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }

            if($this->model->insert([
                
                'codigo_opsu'=>$codigo_opsu,
                'id_aldea'=>$id_aldea,
                'id_programa'=>$id_programa,
                'estatus'=>$estatus
                
                ])){
                    $mensaje='<div class="alert alert-success">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        Asociar la Aldea y Programa ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                            </div>';
                }else{
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                        Ha ocurrido un error al <a class='alert-link' href='#'>asociar el Aldea Programa</a>
                    </div>";
                }
                

                $this->view->mensaje=$mensaje;
                $this->render();
   
        }
        
    }

    function editar(){

        if(isset($_POST['registrar2'])){
            $id_aldea_programa=$_POST['id_aldea_programa'];
            //var_dump($id_aldea_programa);
            $id_aldea2=$_POST['id_aldea2'];
            $id_programa2=$_POST['id_programa2'];
            $codigo_opsu2=$_POST['codigo_opsu2'];
            //$estatus2=$_POST['estatus2'];
            if($_POST['estatus2'] == 'Activo'){
                $estatus2=1;
            }elseif($_POST['estatus2'] == 'Inactivo'){
                $estatus2=0;
            }

            if($this->model->update([
                'id_aldea_programa'=>$id_aldea_programa,
                
                'id_aldea'=>$id_aldea2,
                'id_programa'=>$id_programa2,
                'codigo_opsu'=>$codigo_opsu2,
                'estatus'=>$estatus2

                ])){ 
                    $mensaje='<div class="alert alert-success">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        Editar Asociacion de Aldea y Programa ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                            </div>';   
                }else{
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                        Ha ocurrido un error al <a class='alert-link' href='#'>editar la asociacion de Aldea Programa</a>
                    </div>";
                }
                

                $this->view->mensaje=$mensaje;
                $this->render();
        }

       
    }


      function removerAldeaP($param=null){
        
        $id_aldea_programa=$param[0];
  
        if($this->model->delete($id_aldea_programa)){

            $mensaje="<div class='alert alert-success alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
           Removido correctamente la Aldea Programa<b>  </b><a class='alert-link' href='#'></a>
        </div>";

       }else{
        $mensaje="<div class='alert alert-danger alert-dismissable'>
        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
       No se pudo remover la Aldea Programa<b> </b><a class='alert-link' href='#'></a>
        </div>";
 
       }
       echo $mensaje;
    }

    function cambiarEstatus($param=null){
            
            
        $id_aldea_programa=$param[0];
        
        if($this->model->delete($id_aldea_programa)){
            $mensaje="<div class='alert alert-success alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Malla Eliminada<a class='alert-link' href='#'> Correctamente</a>
            </div>";
           
        }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Ha ocurrido un error al tratar de eliminar la <a class='alert-link' href='#'>Malla</a>
            </div>";
        }
        echo $mensaje;
       
    }

}
?>