<?php
include_once 'sesiones/session_admin.php';

    class Parroquia extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->parroquias=[];            
            $this->view->municipios=[];
            $this->view->ejesMunicipal=[];
            
        }

        function render(){ 

            $parroquias=$this->model->get();
            $this->view->parroquias=$parroquias;

            $ejesMunicipal=$this->model->getEjeMunicipal();
            $this->view->ejesMunicipal=$ejesMunicipal;

            $municipios=$this->model->getMunicipio();
            $this->view->municipios=$municipios;
            $this->view->render('estructura/parroquia');        
        }



        function registrarParroquia(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
    
            $codigoine=$_POST['codigoine'];            
            $parroquia=$_POST['parroquia'];
            $eje_municipal=$_POST['eje_municipal'];
            $municipio=$_POST['municipio'];
            
            
            $mensaje="";

            if($var=$this->model->existe($codigoine)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                 Parroquia <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($this->model->insert(['codigoine'=>$codigoine,'parroquia'=>$parroquia,'eje_municipal'=>$eje_municipal,'municipio'=>$municipio])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Estado agregado<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al agregar un Estado<a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
          }



          function ActualizarParroquia(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
    
            $id_parroquia=$_POST['id_parroquiaedit'];
            $codigoine=$_POST['codigoedita'];
            $parroquia=$_POST['parroquiaedit'];
            $id_municipio=$_POST['id_municipioedit'];
            $id_eje_municipal=$_POST['id_eje_municipaledit'];
         
           // var_dump($id_municipio);
            //var_dump( $id_estado,$codigoine, $estado,$id_pais2);
            $mensaje="";


           
    
            if($this->model->update(['id_parroquia'=>$id_parroquia,'codigoine'=>$codigoine,'parroquia'=>$parroquia,'id_municipio'=>$id_municipio,'id_eje_municipal'=>$id_eje_municipal])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Parroquia <b> ".$estado." </b> Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar Parroquia <b> ".$estado." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }

    }

?>