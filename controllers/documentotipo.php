<?php
include_once 'sesiones/session_admin.php';

    class DocumentoTipo extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->documentos=[];
  
        }

        function render(){ 
            $documentos=$this->model->get();
            $this->view->documentos=$documentos;
            $this->view->render('persona/documentotipo');        
        }



        function registrardocumento(){//se agrega esta linea  
            //$id_persona=$_SESSION["id_persona"];
    
            $documento=$_POST['documento'];
          

             // var_dump($documento);
       
          

            $mensaje="";

            if($var=$this->model->existe($documento)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El tipo  documento <b>" . $documento . "</b>  ya esta registrado <a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($this->model->insert(['documento'=>$documento,'id_documento_tipo'=>$id_documento_tipo])){
           
               
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              documento agregado<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al agregar una documento <a class='alert-link' href='#'></a>

            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }
          



          function Actualizardocumento(){//se agrega esta linea
          
            //$id_persona=$_SESSION["id_persona"];
            $documento2=$_POST['documento2'];
            $id_documento_tipo=$_POST['id_documento_tipo2'];

            
           // var_dump($id_documento_tipo);
           // var_dump($documento2);
      //break;
            //var_dump( $codigoine,$siglasine);
            $mensaje="";
            if($var=$this->model->existe($documento2)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                El tipo  documento <b>" . $documento2 . "</b>  ya esta registrado <a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            

            if($this->model->update(['id_documento_tipo2'=>$id_documento_tipo,'documento2'=>$documento2])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              documento <b> ".$documento2." </b>Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar el documento<b> ".$documento2." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }



          function VerDocumentoTipo($param=null){
            $cadena=$param[0];
            list($id_documento_tipo) = explode(',', $cadena);
      
            if($this->model->delete($id_documento_tipo)){

                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
               Removido correctamente <b>  </b><a class='alert-link' href='#'></a>
            </div>";

            $this->view->mensaje=$mensaje;
            $this->render();

               ?>
    
               
    
                <?php
    
           }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
           No se pudo remover <b> </b><a class='alert-link' href='#'></a>
            </div>";

            $this->view->mensaje=$mensaje;
            $this->render();
                ?>
    
                
    
                <?php
             
           }
           echo $mensaje;
        }

    }

?>