    <?php
include_once 'sesiones/session_admin.php';

    class Area_ubv extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->mensaje="";
            $this->view->areasopsu=[];
            $this->view->areasubv=[];

        }

        function render(){

            $areasopsu=$this->model->getOpsu();

            $this->view->areasopsu=$areasopsu;
            $areasubv=$this->model->getUbv();
            

            $this->view->areasubv=$areasubv;
            $this->view->render('adm_datos_academicos/area_ubv');
        
        }

        function registrar(){

            if(isset($_POST['registrar'])){
                //$estatus_check=$_POST['estatus_check'];
                
                $descripcion=$_POST['descripcion'];
                $codigo=$_POST['codigo'];
                if($_POST['estatus'] == 'Activo'){
                    $estatus=1;
                }elseif($_POST['estatus'] == 'Inactivo'){
                    $estatus=0;
                }
                //var_dump($estatus);
                $id_area_conocimiento_opsu=$_POST['id_area_conocimiento_opsu'];

                if(empty($descripcion)){
                    $mensaje='<div class="alert alert-danger">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                El campo Area Conocimiento UBV es . <a class="alert-link" href="#">Requerido</a>.
                            </div>';
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }
                
                if($var=$this->model->existe($descripcion)){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                     El grado <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                    </div>";
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }

                if($this->model->insert([
                    'descripcion'=>$descripcion,
                    'codigo'=>$codigo,
                    'estatus'=>$estatus,
                    'id_area_conocimiento_opsu'=>$id_area_conocimiento_opsu
                    ])){
                        $mensaje='<div class="alert alert-success">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        Área de Conocimiento UBV  '.$descripcion.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                            </div>';
                    }else{
                        $mensaje="<div class='alert alert-danger alert-dismissable'>
                            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                            Ha ocurrido un error al registrar el Área de Conocimiento UBV<a class='alert-link' href='#'></a>
                        </div>";
                    }

                $this->view->mensaje=$mensaje;
                $this->render();
            }
            
        }

        function editar(){

            if(isset($_POST['registrar2'])){
                
                $id_area_conocimiento_ubv=$_POST['id_area_conocimiento_ubv'];
                $descripcion1=$_POST['descripcion1'];
                $codigo1=$_POST['codigo1'];
                
                if($_POST['estatus1'] == 'Activo'){
                    $estatus1=1;
                }elseif($_POST['estatus1'] == 'Inactivo'){
                    $estatus1=0;
                }
                //var_dump($estatus1);
                $id_area_conocimiento_opsu1=$_POST['id_area_conocimiento_opsu1'];

                if(empty($descripcion1)){
                    $mensaje='<div class="alert alert-danger">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                El campo grado académico es . <a class="alert-link" href="#">Requerido</a>.
                            </div>';
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }

                if($this->model->update([
                    'id_area_conocimiento_ubv'=>$id_area_conocimiento_ubv,
                    'descripcion'=>$descripcion1,
                    'id_area_conocimiento_opsu'=>$id_area_conocimiento_opsu1,
                    'codigo'=>$codigo1,
                    'estatus'=>$estatus1
                    ])){    

              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              El Área de Conocimiento UBV <b> ".$descripcion1." </b> fue Actualizado <a class='alert-link' href='#'>Correctamente</a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar el Área de Conocimiento UBV <b> ".$descripcion1." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            }
            $this->view->mensaje=$mensaje;
            $this->render();
        }
        
        function remover($param=null){
            
            
            $id_area_conocimiento_ubv=$param[0];
            
            if($this->model->delete($id_area_conocimiento_ubv)){
                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Grado Eliminado<a class='alert-link' href='#'> Correctamente</a>
                </div>";
               
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al tratar de eliminar el <a class='alert-link' href='#'>Grado</a>
                </div>";
            }
            echo $mensaje;
           
        }

        function cambiarEstatus($param=null){
            
            
            $id_area_conocimiento_ubv=$param[0];
            
            if($this->model->delete($id_area_conocimiento_ubv)){
                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Grado Eliminado<a class='alert-link' href='#'> Correctamente</a>
                </div>";
               
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al tratar de eliminar el <a class='alert-link' href='#'>Grado</a>
                </div>";
            }
            echo $mensaje;
           
        }
        

    }

    ?>