<?php
include_once 'sesiones/session_admin.php';

class Verificar extends Controller{
    
    function __construct(){
        parent::__construct();
        $this->view->docentes=[];            
        $this->view->datos_academicos=[];            
        $this->view->idiomas=[];            

      //  $this->view->render('ayuda/index');
    }

    function render(){

        //obtener docentes
        $docentes=$this->model->getDocentes();
        $this->view->docentes=$docentes;
        $this->view->render('verificar/index');
        
    }

    function detalle($param = null){

        $id_docente=$param[0];

        $detalleDocente=$this->model->getDocenteByid($id_docente);
        $this->view->detalleDocente=$detalleDocente;
        
        

        $idiomas=$this->model->getDatosAcademicosIdiomasDocente($id_docente);
        $this->view->idiomas=$idiomas;

        $administrativos=$this->model->getExperienciaAdministrativo($id_docente);
        $this->view->administrativos=$administrativos;

        $docenciaspr=$this->model->getExperienciaDocencia($id_docente);

        $this->view->docenciaspr=$docenciaspr;

        $comisionesG=$this->model->getExperienciaComision($id_docente);
        $this->view->comisionesG=$comisionesG;

        $libro=$this->model->getLibroDocente($id_docente);
        $this->view->libro=$libro;
                

        $revista=$this->model->getRevistaDocente($id_docente);
        $this->view->revista=$revista;


        $arte=$this->model->getArtesoftwareDocente($id_docente);
        $this->view->arte=$arte;

        $ponencia=$this->model->getPonenciaDocente($id_docente);
        $this->view->ponencia=$ponencia;

        //revisar si es tipos_reconocimiento
        $tipos_reconocimiento=$this->model->getReconocimientoDocente($id_docente);
        $this->view->tipos_reconocimiento=$tipos_reconocimiento;

        $disenos=$this->model->getDisenoDocente($id_docente);
        $this->view->disenos=$disenos;

        $tutorias=$this->model->getTutoriaDocente($id_docente);
        $this->view->tutorias=$tutorias;

        $docencias=$this->model->getDocenciaUBVDocente($id_docente);
        $this->view->docencias=$docencias;

        $comisiones=$this->model->getComisionexcDocente($id_docente);
        $this->view->comisiones=$comisiones;

        //obtener estudios no conducentes
        $datos_academicos=$this->model->getDatosAcedemicosDocente($id_docente);
        $this->view->datos_academicos=$datos_academicos;

        //obtener estudios conducentes
        $conducentes=$this->model->getEstudiosConducentesDocente($id_docente);
        $this->view->conducentes=$conducentes;


        $this->view->render('verificar/detalle');

    }
    
    //cambia el estatus de verificacion de estudios
    function cambiar(){

        $id_estudio=$_POST['id'];
        
        if($this->model->verificacion($id_estudio)){
            echo '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> Verificado</span>';
        }
    }

     //cambia el estatus de verificacion de experiencia laboral
     function cambiarExp(){
        $id_experiencia_docente=$_POST['id'];
        if($this->model->verificacionExp($id_experiencia_docente)){
            print '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> Verificado</span>';
        }else{
            echo "ERROR";
        }
    }



    function cambiarLibro(){
        $id_libro=$_POST['id'];
        if($this->model->verificacionLibro($id_libro)){
            print '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> Verificado</span>';
        }else{
            echo "ERROR";
        }
    }

    function cambiarRevista(){
        $id_articulo_revista=$_POST['id'];
        if($this->model->verificacionRevista($id_articulo_revista)){
            print '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> Verificado</span>';
        }else{
            echo "ERROR";
        }
    }

    function cambiarArtesoftware(){
        $id_arte_software=$_POST['id'];
        if($this->model->verificacionArtesoftware($id_arte_software)){
            print '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> Verificado</span>';
        }else{
            echo "ERROR";
        }
    }


    function cambiarPonencia(){
        $id_ponencia=$_POST['id'];
        if($this->model->verificacionPonencia($id_ponencia)){
            print '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> Verificado</span>';
        }else{
            echo "ERROR";
        }
    }



    function cambiarReconocimiento(){
        $id_reconocimiento=$_POST['id'];
        if($this->model->verificacionReconocimiento($id_reconocimiento)){
            print '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> Verificado</span>';
        }else{
            echo "ERROR";
        }
    }


























    function cambiarDiseno(){
        $id_diseno_uc=$_POST['id'];
        if($this->model->verificacionDiseno($id_diseno_uc)){
            print '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> Verificado</span>';
        }else{
            echo "ERROR";
        }
    }


    function cambiarTutoria(){
        $id_tutoria=$_POST['id'];
        if($this->model->verificacionTutoria($id_tutoria)){
            print '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> Verificado</span>';
        }else{
            echo "ERROR";
        }
    }

    function cambiarDocencia(){
        $id_docencia_previa_ubv=$_POST['id'];
        if($this->model->verificacionDocencia($id_docencia_previa_ubv)){
            print '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> Verificado</span>';
        }else{
            echo "ERROR";
        }
    }

    function cambiarComisionexc(){
        $id_comision_excedencia=$_POST['id'];
        if($this->model->verificacionComisionexc($id_comision_excedencia)){
            print '<span class="label label-success" style="font-size: 12px;"><i class="fa fa-check"></i> Verificado</span>';
        }else{
            echo "ERROR";
        }
    }



    
}
?>