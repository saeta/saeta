    <?php
include_once 'sesiones/session_admin.php';

    class Convenio extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->mensaje="";
            $this->view->convenios=[];

        }

        function render(){

            $convenios=$this->model->get();
            $this->view->convenios=$convenios;
            $this->view->render('adm_datos_academicos/convenio');
        
        }

        function registrar(){

            if(isset($_POST['registrar'])){
                
                $descripcion=$_POST['descripcion'];
                $institucion_convenio=$_POST['institucion_convenio'];
                $fecha_convenio=$_POST['fecha_convenio'];

                if($var=$this->model->existe($descripcion)){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                     El convenio  <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                    </div>";
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }

                

                if($this->model->insert([
                    'descripcion'=>$descripcion,
                    'institucion_convenio'=>$institucion_convenio,
                    'fecha_convenio'=>$fecha_convenio
                    ])){
                        $mensaje='<div class="alert alert-success">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                El convenio  '.$descripcion.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                            </div>';
                    }else{
                        $mensaje="<div class='alert alert-danger alert-dismissable'>
                            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                            Ha ocurrido un error al registrar el <a class='alert-link' href='#'>Convenio</a>
                        </div>";
                    }

                $this->view->mensaje=$mensaje;
                $this->render();
            }
            
        }

        function editar(){

            if(isset($_POST['registrar2'])){
                $id_convenio=$_POST['id_convenio'];
                //var_dump($id_convenio);
                $descripcion=$_POST['descripcion1'];
                $institucion_convenio=$_POST['institucion_convenio1'];
                $fecha_convenio=$_POST['fecha_convenio1'];               

                if($this->model->update([
                    'descripcion'=>$descripcion,
                    'institucion_convenio'=>$institucion_convenio,
                    'fecha_convenio'=>$fecha_convenio,
                    'id_convenio'=>$id_convenio
                    ])){    

              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              El Convenio <b> ".$descripcion2." </b> fue Actualizado <a class='alert-link' href='#'>Correctamente</a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar el convenio <b> ".$descripcion2." </b><a class='alert-link' href='#'></a>
            </div>";
            }

            }

            $this->view->mensaje=$mensaje;
            $this->render();
        }

        function remover($param=null){
            
            
            $id_turno=$param[0];
            
            if($this->model->delete($id_turno)){
                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Convenio Eliminado<a class='alert-link' href='#'> Correctamente</a>
                </div>";
               
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al tratar de eliminar el <a class='alert-link' href='#'>Convenio</a>
                </div>";
            }
            echo $mensaje;
           
        }
        

    }

    ?>