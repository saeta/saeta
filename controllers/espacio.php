<?php
include_once 'sesiones/session_admin.php';

    class Espacio extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->espacios=[]; 
            $this->view->estructuras=[]; 
            $this->view->ambientes=[];  
            $this->view->espaciotipos=[];
            
        }

        function render(){ 
            //revisar estrutura

            $espacios=$this->model->get();
            $this->view->espacios=$espacios;

            $estructuras=$this->model->getEstructuraFisica();
            $this->view->estructuras=$estructuras;


          /* $ambientes=$this->model->getAmbiente();
            $this->view->ambientes=$ambientes;*/

            $espaciotipos=$this->model->getEspacioTipo();
            $this->view->espaciotipos=$espaciotipos;

            $this->view->render('estructura/espacio');        
        }



        function registrarEspacio(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
    
            $espacio=$_POST['espacio'];            
            $espaciotipo=$_POST['espaciotipo'];
            $capacidad=$_POST['capacidad'];
            $ubicacion=$_POST['ubicacion'];
            //$ambiente=$_POST['ambiente'];

            $estatus=$_POST['estatus'];
           

            
            
            $mensaje="";

            if($var=$this->model->existe($espacio)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Espacio <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($this->model->insert(['espacio'=>$espacio,'espaciotipo'=>$espaciotipo,'capacidad'=>$capacidad,
            'ubicacion'=>$ubicacion,'estatus'=>$estatus])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Espacio agregado<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al agregar un Espacio<a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
          }



          function ActualizarEspacio(){//se agrega esta linea
    
            //$id_persona=$_SESSION["id_persona"];

            $id_ambiente_espacio=$_POST['id_ambiente_espacio'];
            $espacio=$_POST['espacioedit'];            
            $espaciotipo=$_POST['espaciotipoedit'];
            $capacidad=$_POST['capacidadedit'];
            $estructuraf=$_POST['estructuraf'];
            $estatus=$_POST['estatusedit'];

           // var_dump($id_municipio);
            //var_dump( $id_estado,$codigoine, $estado,$id_pais2);
            $mensaje="";
    
            if($this->model->update(['id_ambiente_espacio'=>$id_ambiente_espacio,'espacio'=>$espacio,'espaciotipo'=>$espaciotipo,'capacidad'=>$capacidad,
            'estructuraf'=>$estructuraf,'estatus'=>$estatus])){    
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Espacio <b> ".$espacio." </b> Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar Espacio <b> ".$espacio." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }

    }

?>