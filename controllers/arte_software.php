<?php
include_once 'models/datosacademicos/docente.php';
include_once 'sesiones/session_admin.php';

    class Arte_software extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->mensaje="";
            $this->view->docente=[];
            $this->view->arte=[];

        }

        function render(){

            $docente=$this->model->getDocente();
            //var_dump($docente);
            $this->view->docente=$docente;
            $arte=$this->model->getArte();
            //var_dump($arte);

            $this->view->arte=$arte;
            $this->view->render('produccion_intelectual/arte_software');
        
        }



        function view_registro(){

            $docente=$this->model->getDocente();
            //var_dump($docente);
            $this->view->docente=$docente;
            
            $this->view->arte=$arte;
            $this->view->render('produccion_intelectual/agregar_arte');
        
        }

        function viewEditar($param = null){

            $id_arte_software=$param[0];
            //var_dump($param[0]);
            $docente=$this->model->getDocente($id_arte_software);
            //var_dump($docente);
            $_SESSION['id_arte_software']= $id_arte_software;
            //var_dump($_SESSION['id_arte_software']);
            $this->view->docente=$docente;
            $arte_software=$this->model->getbyId($id_arte_software);
            //var_dump($arte_software);


            $this->view->arte=$arte_software;
            $this->view->render('produccion_intelectual/editar_arte');
        
        }

        function viewVer($param = null){

            $id_arte_software=$param[0];
            //var_dump($param[0]);
            $docente=$this->model->getDocente($id_arte_software);
            //var_dump($docente);
            $_SESSION['id_arte_software']= $id_arte_software;
            //var_dump($_SESSION['id_arte_software']);
            $this->view->docente=$docente;
            $arte_software=$this->model->getbyId($id_arte_software);
            //var_dump($arte_software);


            $this->view->arte=$arte_software;
            $this->view->render('produccion_intelectual/ver_arte');
        
        }


    

        function registrarArte(){

            if(isset($_POST['registrar'])){
                $nombre_arte=$_POST['nombre_arte'];
                $ano_arte=$_POST['ano_arte'];
                $entidad_promotora=$_POST['entidad_promotora'];
                $url_otro=$_POST['url_otro'];
                $id_docente=$_SESSION['id_docente'];

                //var_dump($mmmmmmm);


                
                if($var=$this->model->existe($nombre_arte)){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                     El arte o software <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                    </div>";
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }


                if($this->model->insert([
                    'nombre_arte'=>$nombre_arte,
                    'ano_arte'=>$ano_arte,
                    'entidad_promotora'=>$entidad_promotora,
                    'url_otro'=>$url_otro,
                    'id_docente'=>$id_docente
                    ])){
                        $mensaje='<div class="alert alert-success">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                arte o software  '.$nombre_arte.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                            </div>';
                    }else{
                        $mensaje="<div class='alert alert-danger alert-dismissable'>
                            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                            Ha ocurrido un error al registrar  el arte o software<a class='alert-link' href='#'></a>
                        </div>";
                    }

                $this->view->mensaje=$mensaje;
                $this->render();
            }
            
        }




        function editarArte(){

            
                $id_arte_software=$_POST['id_arte_software'];
                
                $nombre_arte2=$_POST['nombre_arte2'];
                $ano_arte2=$_POST['ano_arte2'];
                $entidad_promotora2=$_POST['entidad_promotora2'];
                $url_otro2=$_POST['url_otro2'];
               // $id_docente2=$_SESSION['id_docente'];


                // var_dump($id_arte_software, $nombre_arte2, $ano_arte2, $entidad_promotora2, $url_otro2 );
               



                if($this->model->update([
                    'id_arte_software'=>$id_arte_software,

                    'nombre_arte2'=>$nombre_arte2,
                    'ano_arte2'=>$ano_arte2,
                    'entidad_promotora2'=>$entidad_promotora2,
                    'url_otro2'=>$url_otro2
                   
                  
                    ])){    


              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              El arte o software <b> ".$nombre_arte2." </b> fue Actualizado <a class='alert-link' href='#'>Correctamente</a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar el arte o software <b> ".$nombre_arte2." </b><a class='alert-link' href='#'></a>
            </div>";
            }

        

            $this->view->mensaje=$mensaje;
            $this->render();
           // $this->viewEditar();
        }


        function verArte(){

            
            $this->view->render('produccion_intelectual/ver_arte');
        
        }





        
        function removerArte($param=null){
            
            
            $id_arte_software=$param[0];
            
            if($this->model->delete($id_arte_software)){
                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                arte o software<a class='alert-link' href='#'> Correctamente</a>
                </div>";
               
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al tratar de eliminar el <a class='alert-link' href='#'>arte o software</a>
                </div>";
            }
            echo $mensaje;
           
        }
        


        



    }

    ?>