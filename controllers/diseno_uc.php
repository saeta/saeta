<?php
//ini_set("display_errors", 1);
include_once 'models/datosacademicos/diseno_uc_docente.php';
include_once 'models/datosacademicos/tramo.php';
include_once 'models/datosacademicos/tipouc.php';
include_once 'models/datosacademicos/programa_formacion.php';
include_once 'models/datosacademicos/areaubv.php';
include_once 'models/datosacademicos/docente.php';
include_once 'sesiones/session_admin.php';


class Diseno_uc extends Controller{
    function __construct(){
        parent::__construct();
        $this->view->mensaje="";
        $this->view->tramos=[];
        $this->view->tipos=[];
        $this->view->programas=[];
        $this->view->areasubv=[];
        $this->view->docentes=[];
        $this->view->disenos=[];
        
       
    }

    function render(){
        $tramos=$this->model->getTramo();
        $this->view->tramos=$tramos;
        $tipos=$this->model->getTipo();
        $this->view->tipos=$tipos;
        $programas=$this->model->getProgramaF();
        $this->view->programas=$programas;
        $areasubv=$this->model->getUbv();
        $this->view->areasubv=$areasubv;
        $docentes=$this->model->getDocente();
        $this->view->docentes=$docentes;
        $disenos=$this->model->getDiseno($_SESSION['identificacion']);
        $this->view->disenos=$disenos;
        //var_dump($disenos);
        $this->view->render('desempeno_docente/diseno_uc');
    }

    function viewRegistrar(){
        
        $tramos=$this->model->getTramo();
        $this->view->tramos=$tramos;
        $tipos=$this->model->getTipo();
        $this->view->tipos=$tipos;
        $programas=$this->model->getProgramaF();
        $this->view->programas=$programas;
        $areasubv=$this->model->getUbv();
        $this->view->areasubv=$areasubv;
        $docentes=$this->model->getDocente();
        $this->view->docentes=$docentes;
        $disenos=$this->model->getDiseno($_SESSION['identificacion']);
        $this->view->disenos=$disenos;
        $nombre=$this->model->getCatalogo('nombre');
        $this->view->nombre=$nombre;
        $this->view->render('desempeno_docente/agregar_diseno');
    }

    function viewEditar($param = null){

        $id_diseno_uc=$param[0];
        //var_dump($param[0]);

        $tramos=$this->model->getTramo($id_diseno_uc);
        $this->view->tramos=$tramos;
        $tipos=$this->model->getTipo($id_diseno_uc);
        $this->view->tipos=$tipos;
        $programas=$this->model->getProgramaF($id_diseno_uc);
        $this->view->programas=$programas;
        $areasubv=$this->model->getUbv($id_diseno_uc);
        $this->view->areasubv=$areasubv;
        $nombre=$this->model->getCatalogo('nombre');
        $this->view->nombre=$nombre;
        $docentes=$this->model->getDocente($id_diseno_uc);
        $this->view->docentes=$docentes;
        $documento=$this->model->getbyID($id_diseno_uc);
        $this->view->descripcion_documento=$documento->descripcion;
        $this->view->id_diseno_uc=$documento->id_diseno_uc;
        $disenos=$this->model->getbyId($id_diseno_uc);
        $_SESSION['id_diseno_uc']= $id_diseno_uc;
        $this->view->disenos=$disenos;
        //var_dump($disenos);
        $this->view->render('desempeno_docente/editar_diseno');
    }

    function viewVer($param = null){

        $id_diseno_uc=$param[0];
        //var_dump($param[0]);
        $tramos=$this->model->getTramo($id_diseno_uc);
        $this->view->tramos=$tramos;
        $tipos=$this->model->getTipo($id_diseno_uc);
        $this->view->tipos=$tipos;
        $programas=$this->model->getProgramaF($id_diseno_uc);
        $this->view->programas=$programas;
        $areasubv=$this->model->getUbv($id_diseno_uc);
        $this->view->areasubv=$areasubv;
        $nombre=$this->model->getCatalogo('nombre');
        $this->view->nombre=$nombre;
        $docentes=$this->model->getDocente($id_diseno_uc);
        $this->view->docentes=$docentes;
        $documento=$this->model->getbyID($id_diseno_uc);
        $this->view->descripcion_documento=$documento->descripcion;
        $this->view->id_diseno_uc=$documento->id_diseno_uc;
        $disenos=$this->model->getbyId($id_diseno_uc);
        $_SESSION['id_diseno_uc']= $id_diseno_uc;
        $this->view->disenos=$disenos;
        $this->view->render('desempeno_docente/ver_diseno');
    }

    function viewDocumento($param = null){

        $id_diseno_uc=$param[0];
        
        $documento=$this->model->getbyID($id_diseno_uc);
        $this->view->documento="diseno/".rawurlencode($documento->descripcion_documento);
        $this->view->render('documentos/viewDocumento');

    }

    function registrar(){
        //valida que todo este bien en el archivo

        $titulo=$_POST['titulo'];
        //obtener detalles de archivos y validar extensiones
        if($this->model->insert([
            'titulo'=>$titulo,
            'pdf_documento'=>$_FILES['pdf_diseno'],
            'pdf_pida'=>$_FILES['acta_pida'],
            'pdf_proyecto'=>$_FILES['acta_proyecto'],
            'id_persona'=>$_SESSION['id_persona']
        ])){
            $mensaje='<div class="alert alert-success">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    La Solicitud Ha sido registrada <a class="alert-link" href="#">Exitosamente</a>.
                </div>';
        }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al registrar la <a class='alert-link' href='#'>Solicitud de Ascenso</a>
            </div>";
        }
 
        $this->view->mensaje=$mensaje;
        $this->render();
    }

    function getData($archivo){
        //var_dump($archivo);
        // Obtener información del archivo subido
        
        $ruta_tmp = $archivo['tmp_name'];
        $nomb_archivo = $archivo['name'];
        $tam_bytes = $archivo['size'];
        $tipo_archivo = $archivo['type'];
        //separa por el delimitador "."
        $fileNameCmps1 = explode(".", $nomb_archivo);
        /*
            la funcion end apunta al ultimo elemento de un arreglo
            la funcion strtolower convierte  mayusculas a minusculas
        */
        $extension_archivo = strtolower(end($fileNameCmps1));
        $nuevo_nomb = md5(time() . $nomb_archivo) . '.' . $extension_archivo;
        //var_dump($extension_archivo);
        $var=array(
                        'ruta_tmp'=>$archivo['tmp_name'],
                        'nomb_archivo'=>$nuevo_nomb,
                        'original_name'=>$archivo['name'],
                        'tam_bytes'=>$archivo['size'],
                        'tipo_archivo'=>$archivo['type'],
                        'extension'=>$extension_archivo
                    );

        return $var;
    }



  //funcion para mover los archivos
  function mover($extension,$ruta_destino,$ruta_temporal,$name_archivo){
    //var_dump($extension,$ruta_destino,$ruta_temporal,$name_archivo);
    
    $extensiones_permitidas = array('csv', 'gif', 'png', 'jpeg', 'xlsx');
    if (in_array($extension, $extensiones_permitidas)) {
        $ruta_carga = $ruta_destino;
        $destino = $ruta_carga . $name_archivo;
        //var_dump($destino);

        /* 
            la funcion move_uploaded_file mueve el archivo cargado a una nueva ubicacion
        */
        if(move_uploaded_file($ruta_temporal, $destino))
        {
            return true;
        } else{
            //echo "primer if";

            return false;
        }
        

    }else{
        //echo "segundo if";
        return false;
    }

}

    function registrarDiseno(){

        

        if(isset($_POST['registrar'])){
            $nombre=$_POST['nombre'];
            $ano_creacion=$_POST['ano_creacion'];
            $id_tramo=$_POST['id_tramo'];
            $id_unidad_curricular_tipo=$_POST['id_unidad_curricular_tipo'];
            $id_programa=$_POST['id_programa'];
            $id_area_conocimiento_ubv=$_POST['id_area_conocimiento_ubv'];
            $id_documento=$_POST['id_documento'];
            $id_docente=$_SESSION['id_docente'];
            $estatus_verificacion=$_POST['estatus_verificacion'];
            $pdf_diseno=$_FILES['pdf_diseno'];

            
            
            
            if($var=$this->model->existe($nombre)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                 La Diseño de Unidad Curricurar Docente <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }

          
            
            

            if($this->model->insert([
                'nombre'=>$nombre,
                'ano_creacion'=>$ano_creacion,
                'id_tramo'=>$id_tramo,
                'id_unidad_curricular_tipo'=>$id_unidad_curricular_tipo,
                'id_programa'=>$id_programa,
                'id_area_conocimiento_ubv'=>$id_area_conocimiento_ubv,
                'id_documento'=>$id_documento,
                'id_docente'=>$id_docente,
                'estatus_verificacion'=>$estatus_verificacion,
                'pdf_diseno'=>$pdf_diseno,
                'id_persona'=>$_SESSION['id_persona']

                ])){
                    
                    $mensaje='<div class="alert alert-success">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Diseño de Unidad Curricurar Docente  '.$nombre.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                        </div>';
                }else{
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                        Ha ocurrido un error al registrar Diseño de Unidad Curricurar Docente<a class='alert-link' href='#'></a>
                    </div>";
                }

            $this->view->mensaje=$mensaje;
            $this->render();
        }
        
    }

    

    function editarDiseno($param){

        
            $id_diseno_uc=$_POST['id_diseno_uc'];
            //var_dump($id_diseno_uc);
            
            $nombre=$_POST['nombre'];
            $ano_creacion=$_POST['ano_creacion'];
            $id_tramo=$_POST['id_tramo'];
            $id_unidad_curricular_tipo=$_POST['id_unidad_curricular_tipo'];
            $id_programa=$_POST['id_programa'];
            $id_area_conocimiento_ubv=$_POST['id_area_conocimiento_ubv'];
            $id_docente=$_SESSION['id_docente'];
            
            //var_dump($id_diseno_uc,$nombre,$ano_creacion,$id_tramo,$id_unidad_curricular_tipo,$id_programa,$id_area_conocimiento_ubv,$id_docente);
           //var_dump($id_documento,$pdf_diseno);
            
            if($this->model->update([
                'id_diseno_uc'=>$id_diseno_uc,
                'nombre'=>$nombre,
                'ano_creacion'=>$ano_creacion,
                'id_tramo'=>$id_tramo,
                'id_unidad_curricular_tipo'=>$id_unidad_curricular_tipo,
                'id_programa'=>$id_programa,
                'id_area_conocimiento_ubv'=>$id_area_conocimiento_ubv,
                'id_docente'=>$id_docente,
                'id_persona'=>$_SESSION['id_persona']
                
                ])){    

          $mensaje="<div class='alert alert-success alert-dismissable'>
          <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
          La Diseño de Unidad Curricurar Docente <b> ".$nombre." </b> fue Actualizada <a class='alert-link' href='#'>Correctamente</a></div>";
        
      }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Ha ocurrido un error al Actualizar el diseño de la unidad <b> ".$nombre." </b><a class='alert-link' href='#'></a>
        </div>";
        }

        

        $this->view->mensaje=$mensaje;
        $this->render($param);
    }


      function removerDiseno($param=null){
        
        $id_diseno_uc=$param[0];
  
        if($this->model->delete($id_diseno_uc)){

            $mensaje="<div class='alert alert-success alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
           Removido correctamente el diseño de la unidad curricular<b>  </b><a class='alert-link' href='#'></a>
        </div>";

       }else{
        $mensaje="<div class='alert alert-danger alert-dismissable'>
        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
       No se pudo remover el diseño de la unidad curricular<b> </b><a class='alert-link' href='#'></a>
        </div>";
 
       }
       echo $mensaje;
    }

    
}
?>