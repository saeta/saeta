<?php
include_once 'sesiones/session_admin.php';

    class Cuentau extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->cuentas=[];
            $this->view->cuentas2=[];
            $this->view->cuentas3=[];
  
        }

        function render(){ 

            $cuentas3=$this->model->get3();
            $this->view->cuentas3=$cuentas3;


            $cuentas2=$this->model->get2();
            $this->view->cuentas2=$cuentas2;

            $cuentas=$this->model->get();
            $this->view->cuentas=$cuentas;
            
            $this->view->render('ingpropios/cuentau');        
        }



        function RegistrarCuentaU(){//se agrega esta linea  
            //$id_persona=$_SESSION["id_persona"];
    
            $cuenta=$_POST['cuenta'];
            $banco=$_POST['banco'];
            $tipo=$_POST['tipo_cuenta'];
            $estatus=$_POST['estatus'];
             //var_dump($cuenta);
             //var_dump($tipo);
             //var_dump($banco);

            
             if($var=$this->model->existe($cuenta)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                La cuenta <b>" . $cuenta . "</b> para este pais ya esta registrada <a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($this->model->insert(['cuenta'=>$cuenta,'banco'=>$banco,'tipo_cuenta'=>$tipo,'estatus'=>$estatus])){
           
               
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              cuenta agregado<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al agregar una cuenta <a class='alert-link' href='#'></a>

            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }
          



          function ActualizarCuentaU(){//se agrega esta linea
          
            //$id_persona=$_SESSION["id_persona"];
            $cuenta2=$_POST['id_cuenta_bancaria'];
            $cuenta=$_POST['cuenta2'];
            $estatus2=$_POST['estatus2'];
                  //var_dump($cuenta2);
              
                  //var_dump($cuenta);
      //break;
            //var_dump( $codigoine,$siglasine);
            $mensaje="";
       

            if($this->model->update(['id_cuenta_bancaria'=>$cuenta2,'cuenta2'=>$cuenta,'estatus2'=>$estatus2])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              cuenta <b> ".$cuenta2." </b>Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
            
          }else
          
          if($var=$this->model->existe($cuenta)){
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            La cuenta <b>" . $cuenta . "</b> para este pais ya esta registrada <a class='alert-link' href='#'></a>
            </div>";
            $this->view->mensaje=$mensaje;
            $this->render();
            exit();
        }
          
        else
          
          {
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar el cuenta<b> ".$cuenta2." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }



          function VerCuentaU($param=null){
            $cadena=$param[0];
            list($id_cuenta_bancaria) = explode(',', $cadena);
      
            if($this->model->delete($id_cuenta_bancaria)){

                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
               Removido correctamente <b>  </b><a class='alert-link' href='#'></a>
            </div>";

            $this->view->mensaje=$mensaje;
            $this->render();

               ?>
    
               
    
                <?php
    
           }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
           No se pudo remover <b> </b><a class='alert-link' href='#'></a>
            </div>";

            $this->view->mensaje=$mensaje;
            $this->render();
                ?>
    
                
    
                <?php
             
           }
           echo $mensaje;
        }

    }

?>