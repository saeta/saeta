<?php
include_once 'sesiones/session_admin.php';

    class Pais extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->paises=[];
        }

        function render(){ 
            $paises=$this->model->get();
            $this->view->paises=$paises;
            $this->view->render('estructura/pais');        
        }



        function registrarPais(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
    
            $codigoine=$_POST['codigoine'];
            $pais=$_POST['pais'];
            $estatus=$_POST['estatus'];
            
            $mensaje="";


            if($var=$this->model->existe($pais)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                 País <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
    
    
            if($this->model->insert(['codigoine'=>$codigoine,
            'pais'=>$pais,
            'estatus'=>$estatus])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              País registrado<a class='alert-link' href='#'>Correctamente</a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha Ocurrido un Error al Registrar el País<a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }

          function ActualizarPais(){//se agrega esta linea
            
            //$id_persona=$_SESSION["id_persona"];
    
            $id_pais=$_POST['id_pais'];
            $codigoine=$_POST['codigoine2'];

            $pais=$_POST['pais2'];
            $estatus=$_POST['estatus2'];
            
            //var_dump( $codigoine,$siglasine);
            $mensaje="";


           if($estatus=='1'){
            $estatus='true';
           }else{
            $estatus='false';
           }
    
            if($this->model->update(['id_pais'=>$id_pais,'codigoine'=>$codigoine,'pais'=>$pais,'estatus'=>$estatus])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              País <b> ".$pais." </b>Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar País <b> ".$pais." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }



    }

?>