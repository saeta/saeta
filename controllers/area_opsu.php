    <?php
include_once 'sesiones/session_admin.php';

    class Area_opsu extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->mensaje="";
            $this->view->areasopsu=[];

        }

        function render(){

            $areasopsu=$this->model->get();
            $this->view->areasopsu=$areasopsu;
            $this->view->render('adm_datos_academicos/area_opsu');

        }

        function registrar(){

            if(isset($_POST['registrar'])){
                
                $descripcion=$_POST['descripcion'];
                $codigo=$_POST['codigo'];

                if($var=$this->model->existe($descripcion)){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                     El Área de Conocimiento OPSU  <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                    </div>";
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }

                if($this->model->insert([
                    'descripcion'=>$descripcion,
                    'codigo'=>$codigo
                    ])){
                        $mensaje='<div class="alert alert-success">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                El Área de Conocimiento OPSU  '.$descripcion.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                            </div>';
                    }else{
                        $mensaje="<div class='alert alert-danger alert-dismissable'>
                            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                            Ha ocurrido un error al registrar el <a class='alert-link' href='#'>Área de Conocimiento OPSU</a>
                        </div>";
                    }

                $this->view->mensaje=$mensaje;
                $this->render();
            }
            
        }

        function editar(){

            if(isset($_POST['registrar2'])){
                $id_area_conocimiento_opsu=$_POST['id_area_conocimiento_opsu'];
                //var_dump($id_area_conocimiento_opsu);
                $descripcion=$_POST['descripcion1'];
                $codigo=$_POST['codigo1'];

                if($this->model->update([
                    'descripcion'=>$descripcion,
                    'codigo'=>$codigo,
                    'id_area_conocimiento_opsu'=>$id_area_conocimiento_opsu
                    ])){    

              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              El Área de Conocimiento OPSU <b> ".$descripcion2." </b> fue Actualizado <a class='alert-link' href='#'>Correctamente</a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar el Área de Conocimiento OPSU <b> ".$descripcion2." </b><a class='alert-link' href='#'></a>
            </div>";
            }

            }

            $this->view->mensaje=$mensaje;
            $this->render();
        }

        function remover($param=null){
            
            
            $id_turno=$param[0];
            
            if($this->model->delete($id_turno)){
                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Área de Conocimiento OPSU Eliminado<a class='alert-link' href='#'> Correctamente</a>
                </div>";
               
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al tratar de eliminar el <a class='alert-link' href='#'>Área de Conocimiento OPSU</a>
                </div>";
            }
            echo $mensaje;
           
        }
        

    }

    ?>