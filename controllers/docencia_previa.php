<?php
//ini_set("display_errors", 1);
include_once 'models/datosacademicos/docencia_previa_ubv.php';
include_once 'models/datosacademicos/areaubv.php';
include_once 'models/datosacademicos/docente.php';
include_once 'models/datosacademicos/programa_formacion.php';
include_once 'models/datosacademicos/aldea.php';
include_once 'models/estructura.php';
include_once 'models/datosacademicos/centro_estudio.php';
include_once 'sesiones/session_admin.php';

class Docencia_previa extends Controller{
    function __construct(){
        parent::__construct();
        $this->view->mensaje="";
        $this->view->areasubv=[];
        $this->view->docentes=[];
        $this->view->programas=[];
        $this->view->regionales=[];
        $this->view->municipales=[];
        $this->view->aldea=[];
        $this->view->centros=[];
        $this->view->docencias=[];

    }

    function render(){
        $areasubv=$this->model->getUbv();
        $this->view->areasubv=$areasubv;
        $docentes=$this->model->getDocente();
        $this->view->docentes=$docentes;
        $programas=$this->model->getProgramaF();
        $this->view->programas=$programas;
        $regionales=$this->model->getEjeRegional();
        $this->view->regionales=$regionales; 
        $municipales=$this->model->getEjeMunicipal();
        $this->view->municipales=$municipales;
        $aldeas=$this->model->getAldea();
        $this->view->aldeas=$aldeas;
        $centros=$this->model->getCentro();
        $this->view->centros=$centros;
        $docencias=$this->model->getDocencia($_SESSION['identificacion']);
        $this->view->docencias=$docencias;
        //var_dump($docencias);
        $this->view->render('desempeno_docente/docencia_previa');
        //$this->view->render('desempeno_docente/agregar_docencia');
        
    }

    function viewRegistrar(){
        $areasubv=$this->model->getUbv();
        $this->view->areasubv=$areasubv;
        $docentes=$this->model->getDocente();
        $this->view->docentes=$docentes;
        $programas=$this->model->getProgramaF();
        $this->view->programas=$programas;
        $regionales=$this->model->getEjeRegional();
        $this->view->regionales=$regionales; 
        $municipales=$this->model->getEjeMunicipal();
        $this->view->municipales=$municipales;
        $aldeas=$this->model->getAldea();
        $this->view->aldeas=$aldeas;
        $centros=$this->model->getCentro();
        $this->view->centros=$centros;
        $docencias=$this->model->getDocencia($_SESSION['identificacion']);
        $this->view->docencias=$docencias;
        //var_dump($docencias);
        $this->view->render('desempeno_docente/agregar_docencia');
    }

    function viewEditar($param = null){
        $id_docencia_previa_ubv=$param[0];
        //var_dump($param[0]);
        
        $areasubv=$this->model->getUbv($id_docencia_previa_ubv);
        $this->view->areasubv=$areasubv;
        $docentes=$this->model->getDocente($id_docencia_previa_ubv);
        $this->view->docentes=$docentes;
        $programas=$this->model->getProgramaF($id_docencia_previa_ubv);
        $this->view->programas=$programas;
        $regionales=$this->model->getEjeRegional($id_docencia_previa_ubv);
        $this->view->regionales=$regionales; 
        $municipales=$this->model->getEjeMunicipal($id_docencia_previa_ubv);
        $this->view->municipales=$municipales;
        $aldeas=$this->model->getAldea($id_docencia_previa_ubv);
        $this->view->aldeas=$aldeas;
        $centros=$this->model->getCentro($id_docencia_previa_ubv);
        $this->view->centros=$centros;
        $docencias=$this->model->getbyId($id_docencia_previa_ubv);
        $_SESSION['id_docencia_previa_ubv']= $id_docencia_previa_ubv;
        $this->view->docencias=$docencias;
        //var_dump($docencia);

        $this->view->render('desempeno_docente/editar_docencia');
    }


    function viewVer($param = null){
        $id_docencia_previa_ubv=$param[0];
        //var_dump($param[0]);
        $_SESSION['id_docencia_previa_ubv']= $id_docencia_previa_ubv;
        $areasubv=$this->model->getUbv($id_docencia_previa_ubv);
        $this->view->areasubv=$areasubv;
        $docentes=$this->model->getDocente($id_docencia_previa_ubv);
        $this->view->docentes=$docentes;
        $programas=$this->model->getProgramaF($id_docencia_previa_ubv);
        $this->view->programas=$programas;
        $regionales=$this->model->getEjeRegional($id_docencia_previa_ubv);
        $this->view->regionales=$regionales; 
        $municipales=$this->model->getEjeMunicipal($id_docencia_previa_ubv);
        $this->view->municipales=$municipales;
        $aldeas=$this->model->getAldea($id_docencia_previa_ubv);
        $this->view->aldeas=$aldeas;
        $centros=$this->model->getCentro($id_docencia_previa_ubv);
        $this->view->centros=$centros;
        $docencias=$this->model->getbyId($id_docencia_previa_ubv);
        $this->view->docencias=$docencias;
        //var_dump($docencia);
        $this->view->render('desempeno_docente/ver_docencia');
        
    }




    function registrarDocencia(){

        if(isset($_POST['registrar'])){
            $id_area_conocimiento_ubv=$_POST['id_area_conocimiento_ubv'];
            $id_programa=$_POST['id_programa'];
            $id_eje_regional=$_POST['id_eje_regional'];
            $id_eje_municipal=$_POST['id_eje_municipal'];
            $id_aldea=$_POST['id_aldea'];
            $id_centro_estudio=$_POST['id_centro_estudio'];
            $fecha_ingreso=$_POST['fecha_ingreso'];
            $id_docente=$_SESSION['id_docente'];
           
            //var_dump($id_eje_regional,$id_eje_municipal,$id_aldea,$estatus_verificacion);
           
            
             //var_dump($id_area_conocimiento_ubv,$id_programa,$id_eje_regional,$id_eje_municipal,$id_aldea,$id_centro_estudio,$fecha_ingreso,$estatus_verificacion);


            if($this->model->insert([
                'id_area_conocimiento_ubv'=>$id_area_conocimiento_ubv,
                'id_docente'=>$id_docente,
                'id_programa'=>$id_programa,
                'id_eje_regional'=>$id_eje_regional,
                'id_eje_municipal'=>$id_eje_municipal,
                'id_aldea'=>$id_aldea,
                'id_centro_estudio'=>$id_centro_estudio,
                'fecha_ingreso'=>$fecha_ingreso,
                'id_persona'=>$_SESSION['id_persona']

                ])){
                    $mensaje='<div class="alert alert-success">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Docencia Previa  '.$id_area_conocimiento_ubv.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                        </div>';
                }else{
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                        Ha ocurrido un error al registrar el Docencia Previa<a class='alert-link' href='#'></a>
                    </div>";
                }

            $this->view->mensaje=$mensaje;
            $this->render();
        }

    }

    function editarDocencia($param ){
  
       

            $id_docencia_previa_ubv=$_POST['id_docencia_previa_ubv'];
            $id_area_conocimiento_ubv=$_POST['id_area_conocimiento_ubv'];
            $id_programa=$_POST['id_programa'];
            $id_eje_regional=$_POST['id_eje_regional'];
            $id_eje_municipal=$_POST['id_eje_municipal'];
            $id_aldea=$_POST['id_aldea'];
            $id_centro_estudio=$_POST['id_centro_estudio'];
            $fecha_ingreso=$_POST['fecha_ingreso'];
            $id_docente=$_SESSION['id_docente'];
            //$estatus2=$_POST['estatus2'];
          
            //var_dump($id_docencia_previa_ubv);
            //var_dump($id_docencia_previa_ubv,$id_area_conocimiento_ubv,$id_programa,$id_eje_regional,$id_eje_municipal,$id_aldea,$id_centro_estudio,$fecha_ingreso,$estatus_verificacion);


            if($this->model->update([
                'id_docencia_previa_ubv'=>$id_docencia_previa_ubv,
                'id_area_conocimiento_ubv'=>$id_area_conocimiento_ubv,
                'id_programa'=>$id_programa,
                'id_eje_regional'=>$id_eje_regional,
                'id_eje_municipal'=>$id_eje_municipal,
                'id_aldea'=>$id_aldea,
                'id_centro_estudio'=>$id_centro_estudio,
                'fecha_ingreso'=>$fecha_ingreso,
                'id_docente'=>$id_docente,
                'id_persona'=>$_SESSION['id_persona']
                
                ])){    
                    //var_dump($id_area_conocimiento_ubv );
          $mensaje="<div class='alert alert-success alert-dismissable'>
          <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
          El Docencia Previa  fue Actualizado <a class='alert-link' href='#'>Correctamente</a></div>";
        
      }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Ha ocurrido un error al Actualizar El Docencia Previa <a class='alert-link' href='#'></a>
        </div>";
        }

        

        $this->view->mensaje=$mensaje;
        //$this->render();
        $this->render();
        
    }


      function removerDocencia($param=null){

        $id_docencia_previa_ubv=$param[0];

        if($this->model->delete($id_docencia_previa_ubv)){

            $mensaje="<div class='alert alert-success alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
           Removido correctamente el Docencia Previa<b>  </b><a class='alert-link' href='#'></a>
        </div>";

       }else{
        $mensaje="<div class='alert alert-danger alert-dismissable'>
        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
       No se pudo remover el Docencia Previa<b> </b><a class='alert-link' href='#'></a>
        </div>";

       }
       echo $mensaje;
    }

    function cambiarEstatus($param=null){
        $id_docencia_previa_ubv=$param[0];

        if($this->model->delete($id_docencia_previa_ubv)){
            $mensaje="<div class='alert alert-success alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Programa Eliminado<a class='alert-link' href='#'> Correctamente</a>
            </div>";

        }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Ha ocurrido un error al tratar de eliminar el <a class='alert-link' href='#'>Programa</a>
            </div>";
        }
        echo $mensaje;

    }

}
?>