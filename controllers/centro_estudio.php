<?php
include_once 'models/datosacademicos/areaopsu.php';
include_once 'sesiones/session_admin.php';

    class Centro_estudio extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->mensaje="";
            $this->view->areasopsu=[];
            $this->view->centros=[];

        }

        function render(){

            $areasopsu=$this->model->getAreaOpsu();
            //var_dump($areasopsu);
            $this->view->areasopsu=$areasopsu;
            $centros=$this->model->getCentro();
            //var_dump($centros);

            $this->view->centros=$centros;
            $this->view->render('adm_datos_academicos/centro_estudio');
        
        }

        // render de asociar
        function viewAssoc(){

            $asociaciones=$this->model->getAsociaciones();
            $this->view->asociaciones=$asociaciones;


            $this->view->render('adm_datos_academicos/asociar_director');
        }

        // render de AGREGAR de asociacion
        function viewAdd(){
            $centros=$this->model->getCentro();
            $this->view->centros=$centros;

            $dircoord=$this->model->getCoordDir();
            $this->view->dircoord=$dircoord;
            
            $this->view->render('adm_datos_academicos/agregar_asociacion_CE');
        }

        function viewEdit($param = null){
            
            $id_persona_centro_estudio=$param[0];
            $asociacion=$this->model->getByIdAsoc($id_persona_centro_estudio);

            $this->view->asociacion=$asociacion;

            $centros=$this->model->getCentro();
            $this->view->centros=$centros;

            $dircoord=$this->model->getCoordDir();
            $this->view->dircoord=$dircoord;
            
            $this->view->render('adm_datos_academicos/editar_asociacion_CE');
        }

        function registrarCentro(){

            if(isset($_POST['registrar'])){
                $descripcion=$_POST['descripcion'];
                $codigo=$_POST['codigo'];
                $id_area_conocimiento_opsu=$_POST['id_area_conocimiento_opsu'];


/*
                if($_POST['estatus'] == 'Activo'){
                    $estatus=1;
                }elseif($_POST['estatus'] == 'Inactivo'){
                    $estatus=0;
                }
                */


                $var1 = $_POST['estatus'];
                $var2 = "Activo";
                $var3 = "Inactivo";

                if (strcasecmp($var1, $var2) === 0){
                    $estatus=1;
                    
                } elseif(strcasecmp($var1, $var3) === 0){
                    $estatus=0;
                }

                //var_dump($estatus);









                if(empty($descripcion)){
                    $mensaje='<div class="alert alert-danger">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                El campo centro de estudio es . <a class="alert-link" href="#">Requerido</a>.
                            </div>';
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }elseif(empty($id_area_conocimiento_opsu)){
                    $mensaje='<div class="alert alert-danger">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    El campo centro de estudio es <a class="alert-link" href="#">Requerido</a>.
                    </div>';
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }
                
                if($var=$this->model->existe($descripcion)){
                    $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                     El grado <b>" . $var . "</b> ya <a class='alert-link' href='#'>existe</a> en la base de datos
                    </div>";
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }



                if($this->model->insert([
                    'descripcion'=>$descripcion,
                    'codigo'=>$codigo,
                    'id_area_conocimiento_opsu'=>$id_area_conocimiento_opsu,
                    'estatus'=>$estatus
                    ])){
                        $mensaje='<div class="alert alert-success">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                centro de estudio  '.$descripcion.' ha sido registrado <a class="alert-link" href="#">Exitosamente</a>.
                            </div>';
                    }else{
                        $mensaje="<div class='alert alert-danger alert-dismissable'>
                            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                            Ha ocurrido un error al registrar el <a class='alert-link' href='#'>Centro de Estudio</a>
                        </div>";
                    }

                $this->view->mensaje=$mensaje;
                $this->render();
            }
            
        }

        function editarCentro(){

            if(isset($_POST['registrar2'])){
                $id_centro_estudio=$_POST['id_centro_estudio'];

                $descripcion2=$_POST['descripcion2'];
                $codigo2=$_POST['codigo1'];

                if($_POST['estatus1'] == 'Activo'){
                    $estatus=1;
                }elseif($_POST['estatus1'] == 'Inactivo'){
                    $estatus=0;
                }
               // var_dump($estatus);
               // var_dump($codigo2);
               // coloque este bloque de estatus
                /*
                $var1 = $_POST['estatus'];
                $var2 = "Activo";
                $var3 = "Inactivo";

                if (strcasecmp($var1, $var2) === 0){
                    $estatus=1;
                    
                } elseif(strcasecmp($var1, $var3) === 0){
                    $estatus=0;
                }*/

                $id_area_conocimiento_opsu2=$_POST['id_area_conocimiento_opsu2'];
               //var_dump($estatus1);
               

                if(empty($descripcion2)){
                    $mensaje='<div class="alert alert-danger">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                El campo centro de estudio es . <a class="alert-link" href="#">Requerido</a>.
                            </div>';
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }elseif(empty($id_area_conocimiento_opsu2)){
                    $mensaje='<div class="alert alert-danger">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    El campo centro de estudio es <a class="alert-link" href="#">Requerido</a>.
                    </div>';
                    $this->view->mensaje=$mensaje;
                    $this->render();
                    exit();
                }

                if($this->model->update([
                    'id_centro_estudio'=>$id_centro_estudio,
                    'descripcion'=>$descripcion2,
                    'codigo1'=>$codigo2,
                    'id_area_conocimiento_opsu'=>$id_area_conocimiento_opsu2,
                    'estatus1'=>$estatus
                    ])){    


              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              El centro <b> ".$descripcion2." </b> fue Actualizado <a class='alert-link' href='#'>Correctamente</a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar el centro <b> ".$descripcion2." </b><a class='alert-link' href='#'></a>
            </div>";
            }

            }

            $this->view->mensaje=$mensaje;
            $this->render();
        }
        
        function removerCentro($param=null){
            
            
            $id_centro_estudio=$param[0];
            
            if($this->model->delete($id_centro_estudio)){
                $mensaje="<div class='alert alert-success alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Grado Eliminado<a class='alert-link' href='#'> Correctamente</a>
                </div>";
               
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al tratar de eliminar el <a class='alert-link' href='#'>Grado</a>
                </div>";
            }
            echo $mensaje;
           
        }
        
       
        function asociar(){

            list($id_persona,$id_perfil)=explode(',', $_POST['director']);
            $id_centro_estudio=$_POST['id_centro_estudio'];
            $estatus=$_POST['estatus'];

            //por si la ascociacion ya existe
            if($this->model->existeAsociar([
                'id_persona'=>$id_persona,
                'id_centro_estudio'=>$id_centro_estudio
            ])){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Esta Asociación  ya <a class='alert-link' href='#'>existe</a>.
                </div>";
                $this->view->mensaje=$mensaje;
                $this->viewAdd();
                exit();
            }

            //por si el centro de estudio tiene una asociacion activa
            if($this->model->existeCentroActivo([
                'id_centro_estudio'=>$id_centro_estudio
            ])){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Esta Centro tiene una Asociación <a class='alert-link' href='#'>Activa</a> Actualmente.
                </div>";
                $this->view->mensaje=$mensaje;
                $this->viewAdd();
                exit();
            }
            
            //por si la persona tiene una asociacion activa
            if($this->model->existePersonaActivo([
                'id_persona'=>$id_persona,
                'id_perfil'=>$id_perfil
            ])){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Esta Persona tiene una Asociación <a class='alert-link' href='#'>Activa</a> Actualmente.
                </div>";
                $this->view->mensaje=$mensaje;
                $this->viewAdd();
                exit();
            }   

            if($this->model->insertAsociar([
                'id_persona'=>$id_persona,
                'id_centro_estudio'=>$id_centro_estudio,
                'estatus'=>$estatus
            ])){
                $mensaje='<div class="alert alert-success">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Centro de Estudio y Persona Asociados <a class="alert-link" href="#">Exitosamente</a>.
                </div>';
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                    Ha ocurrido un error al <a class='alert-link' href='#'>Asociar</a> el Centro de Estudio y la Persona.
                </div>";
                $this->view->mensaje=$mensaje;
                $this->viewAdd();
                exit();
            }

            $this->view->mensaje=$mensaje;
            $this->viewAssoc();
        }

        function editarAssoc($param){

            list($id_persona,$id_perfil)=explode(',', $_POST['director']);
            $id_centro_estudio=$_POST['id_centro_estudio'];
            $estatus=$_POST['estatus'];
            $id_persona_centro_estudio=$param[0];
            
            if($this->model->editAsociar([
                'id_persona_centro_estudio'=>$id_persona_centro_estudio,
                'id_persona'=>$id_persona,
                'id_centro_estudio'=>$id_centro_estudio,
                'estatus'=>$estatus
            ])){
                $mensaje='<div class="alert alert-success">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    Asociación Actualizada <a class="alert-link" href="#">Exitosamente</a>.
                </div>';
            }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                    <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                    Ha ocurrido un error al intentar <a class='alert-link' href='#'>Actualizar</a> la Asociación.
                </div>";
                $this->view->mensaje=$mensaje;
                $this->viewEdit($param);
                exit();
            }

            $this->view->mensaje=$mensaje;
            $this->viewAssoc();
        }






    }

    ?>