<?php
include_once 'sesiones/session_admin.php';

class Experiencia_Laboral extends Controller{
    
    function __construct(){
        parent::__construct();

        $this->view->mensaje="";
        $this->view->paises=[];
        $this->view->tipos_institucion=[];
        $this->view->instituciones=[];
        $this->view->tipos_experiencia=[];
        $this->view->niveles=[];
        $this->view->modalidades=[];
        $this->view->cargos_experiencia=[];

      //  $this->view->render('ayuda/index');
    }

    //render de vista de experiencia laboral 
    function render(){
        
        $administrativos=$this->model->getExperienciaAdministrativo($_SESSION['id_docente']);
        $this->view->administrativos=$administrativos;

        $docencias=$this->model->getExperienciaDocencia($_SESSION['id_docente']);
        $this->view->docencias=$docencias;

        $comisiones=$this->model->getExperienciaComision($_SESSION['id_docente']);
        $this->view->comisiones=$comisiones;

        $this->view->render('experiencia_laboral/index');
    }

    //render de vista de agregar 
    function viewAdd(){

        //obtener paises
        $paises=$this->model->getPais();
        $this->view->paises=$paises;

        //obtener tipos de institucion
        $tipos_institucion=$this->model->getCatalogo('institucion_tipo');
        $this->view->tipos_institucion=$tipos_institucion;

        //obtener nivel_academico
        $niveles=$this->model->getCatalogo('nivel_academico');
        $this->view->niveles=$niveles;
        
        //obtener modalidad_estudio
        $modalidades=$this->model->getCatalogo('modalidad_estudio');
        $this->view->modalidades=$modalidades;


        //obtener instituciones
        $instituciones=$this->model->getInstitucion();
        $this->view->instituciones=$instituciones;

        //obtener instituciones
        $cargos_experiencia=$this->model->getCatalogo('cargo_experiencia');
        $this->view->cargos_experiencia=$cargos_experiencia;
        
        //obtener tipo_experiencia
        $tipos_experiencia=$this->model->getCatalogo('tipo_experiencia');
        $this->view->tipos_experiencia=$tipos_experiencia;

        $this->view->render('experiencia_laboral/agregar_experiencia');
    }

    //render de vista de agregar 
    function viewEdit($param = null){
        

        list($id_experiencia_tabla,$id_tipo_experiencia)=explode(',', $param[0]);
        
        switch($id_tipo_experiencia){
            case 1:
                $admin=$this->model->getByIdAdmin($id_experiencia_tabla);
                $this->view->experiencia=$admin;
               

                //obtener nombre de documento
                $documento=$this->model->getDocumentbyID($admin->id_trabajo_administrativo);
                //var_dump($documento);
                $this->view->descripcion_documento=$documento->descripcion;
                $this->view->id_trabajo_administrativo=$documento->id_trabajo_administrativo;
                break;
            case 2:
                $docencia=$this->model->getbyIdDocencia($id_experiencia_tabla);
                $this->view->experiencia=$docencia;                
                break;
            case 3:
                $comision=$this->model->getByIdComision($id_experiencia_tabla);
                $this->view->experiencia=$comision;                
                break;

        }
        $_SESSION['id_experiencia_tabla']=$id_experiencia_tabla;
        $_SESSION['id_tipo_experiencia']=$id_tipo_experiencia;
        //obtener paises
        $paises=$this->model->getPais();
        $this->view->paises=$paises;

        //obtener tipos de institucion
        $tipos_institucion=$this->model->getCatalogo('institucion_tipo');
        $this->view->tipos_institucion=$tipos_institucion;

        //obtener nivel_academico
        $niveles=$this->model->getCatalogo('nivel_academico');
        $this->view->niveles=$niveles;
        
        //obtener modalidad_estudio
        $modalidades=$this->model->getCatalogo('modalidad_estudio');
        $this->view->modalidades=$modalidades;


        //obtener instituciones
        $instituciones=$this->model->getInstitucion();
        $this->view->instituciones=$instituciones;

        //obtener instituciones
        $cargos_experiencia=$this->model->getCatalogo('cargo_experiencia');
        $this->view->cargos_experiencia=$cargos_experiencia;
        
        //obtener tipo_experiencia
        $tipos_experiencia=$this->model->getCatalogo('tipo_experiencia');
        $this->view->tipos_experiencia=$tipos_experiencia;

        

        $this->view->render('experiencia_laboral/editar_experiencia');
    }

    //render de vista de agregar 
    function viewDetail($param = null){

        list($id_experiencia_tabla,$id_tipo_experiencia)=explode(',', $param[0]);
        
        switch($id_tipo_experiencia){
            case 1:
                $admin=$this->model->getByIdAdmin($id_experiencia_tabla);
                $this->view->experiencia=$admin;
                
                //obtener nombre de documento
                $documento=$this->model->getDocumentbyID($admin->id_trabajo_administrativo);
                //var_dump($documento);
                $this->view->descripcion_documento=$documento->descripcion;
                $this->view->id_trabajo_administrativo=$documento->id_trabajo_administrativo;
                break;
            case 2:
                $docencia=$this->model->getbyIdDocencia($id_experiencia_tabla);
                $this->view->experiencia=$docencia;                
                break;
            case 3:
                $comision=$this->model->getByIdComision($id_experiencia_tabla);
                $this->view->experiencia=$comision;                
                break;
        }
        $_SESSION['id_experiencia_tabla']=$id_experiencia_tabla;
        $_SESSION['id_tipo_experiencia']=$id_tipo_experiencia;
        //obtener paises
        $paises=$this->model->getPais();
        $this->view->paises=$paises;

        //obtener tipos de institucion
        $tipos_institucion=$this->model->getCatalogo('institucion_tipo');
        $this->view->tipos_institucion=$tipos_institucion;

        //obtener nivel_academico
        $niveles=$this->model->getCatalogo('nivel_academico');
        $this->view->niveles=$niveles;
        
        //obtener modalidad_estudio
        $modalidades=$this->model->getCatalogo('modalidad_estudio');
        $this->view->modalidades=$modalidades;


        //obtener instituciones
        $instituciones=$this->model->getInstitucion();
        $this->view->instituciones=$instituciones;

        //obtener instituciones
        $cargos_experiencia=$this->model->getCatalogo('cargo_experiencia');
        $this->view->cargos_experiencia=$cargos_experiencia;
        
        //obtener tipo_experiencia
        $tipos_experiencia=$this->model->getCatalogo('tipo_experiencia');
        $this->view->tipos_experiencia=$tipos_experiencia;

        $this->view->render('experiencia_laboral/detalle_experiencia');
    }

    function viewDocumento($param = null){

        $id_trabajo_administrativo=$param[0];
        $documento=$this->model->getDocumentbyID($id_trabajo_administrativo);
        $this->view->documento="experiencia_laboral/".rawurlencode($documento->descripcion);
        $this->view->render('documentos/viewDocumento');

    }

    function registrar(){

        //experiencia
        $id_tipo_experiencia=$_POST['tipo_experiencia'];
        $id_institucion_tipo=$_POST['institucion_tipo'];

        //trabajo administrativo==1
        if($id_tipo_experiencia==1){
            $periodo_admin=$_POST['periodo_admin'];

            // institucion tipo==1
            if($id_institucion_tipo==1){
                $file_publico=$_FILES['file_publico'];
            }
        }
        
        //docencia previa==2
        if($id_tipo_experiencia==2){
            $unidad_c=$_POST['unidad_c'];
            $nivel=$_POST['nivel'];
            $modalidad=$_POST['modalidad'];
            $semestre=$_POST['semestre'];
            $horas=$_POST['horas'];
            $nro_estudiantes=$_POST['nro_estudiantes'];
            $naturaleza=$_POST['naturaleza'];
            $periodo_docencia=$_POST['periodo_docencia'];
        }

        //comision gubernamental==3
        if($id_tipo_experiencia==3){
            $direccion=$_POST['direccion'];
        }

        $cargo=$_POST['cargo'];

        $band=false;
        $id_cargo_experiencia="";
        if($var=$this->model->existe_cargo($cargo)){
           $id_cargo_experiencia=$var['id_cargo_experiencia'];
           $band=true;
        }
        $pais=$_POST['pais'];
        $estado=$_POST['estado'];
        $ano_experiencia=$_POST['ano_r'];
        $institucion=$_POST['institucion'];

        $band_2=false;
        $id_institucion="";
        if($var=$this->model->existe_institucion($institucion)){
           $id_institucion=$var['id_institucion'];
           $band_2=true;
        }


        $fecha_ingreso=$_POST['start'];
        $fecha_egreso=$_POST['end'];
        
        if($this->model->insert([
            'institucion'=>$institucion,
            'id_pais'=>$pais,
            'id_institucion_tipo'=>$id_institucion_tipo,
            'id_institucion'=>$id_institucion,
            'cargo'=>$cargo,
            'id_cargo_experiencia'=>$id_cargo_experiencia,
            'id_tipo_experiencia'=>$id_tipo_experiencia,
            'fecha_ingreso'=>date("Y-m-d", strtotime($fecha_ingreso)),
            'fecha_egreso'=>date("Y-m-d", strtotime($fecha_egreso)),
            'id_estado'=>$estado,
            'id_docente'=>$_SESSION['id_docente'],
            'ano_experiencia'=>$ano_experiencia,
            'periodo_admin'=>$periodo_admin,
            'pdf_constancia'=>$file_publico,
            'id_persona'=>$_SESSION['id_persona'],
            'materia'=>$unidad_c,
            'id_nivel_academico'=>$nivel,
            'id_modalidad_estudio'=>$modalidad,
            'semestre'=>$semestre,
            'periodo_lectivo'=>$periodo_docencia,
            'naturaleza'=>$naturaleza,
            'horas_semanales'=>$horas,
            'estudiantes_atendidos'=>$nro_estudiantes,
            'lugar'=>$direccion
        ])){
            $mensaje='<div class="alert alert-success">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    La Experiencia Laboral ha sido registrada <a class="alert-link" href="#">Exitosamente</a>.
                </div>';
        }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al registrar La <a class='alert-link' href='#'>Experiencia Laboral</a>
            </div>";
        }




        $this->view->mensaje=$mensaje;
        $this->render();

    }


    function editar($param){

        //experiencia
        $id_tipo_experiencia=$_POST['tipo_experiencia'];
        $id_institucion_tipo=$_POST['institucion_tipo'];

        //trabajo administrativo==1
        if($id_tipo_experiencia==1){
            $periodo_admin=$_POST['periodo_admin'];

            // institucion tipo==1
            if($id_institucion_tipo==1){
                $file_publico=$_FILES['file_publico'];
            }
        }
        
        //docencia previa==2
        if($id_tipo_experiencia==2){
            $unidad_c=$_POST['unidad_c'];
            $nivel=$_POST['nivel'];
            $modalidad=$_POST['modalidad'];
            $semestre=$_POST['semestre'];
            $horas=$_POST['horas'];
            $nro_estudiantes=$_POST['nro_estudiantes'];
            $naturaleza=$_POST['naturaleza'];
            $periodo_docencia=$_POST['periodo_docencia'];
        }

        //comision gubernamental==3
        if($id_tipo_experiencia==3){
            $direccion=$_POST['direccion'];
        }

        $cargo=$_POST['cargo'];

        $band=false;
        $id_cargo_experiencia="";
        if($var=$this->model->existe_cargo($cargo)){
           $id_cargo_experiencia=$var['id_cargo_experiencia'];
           $band=true;
        }
        //var_dump($id_cargo_experiencia ," con");

        $pais=$_POST['pais'];
        $estado=$_POST['estado'];
        $ano_experiencia=$_POST['ano_r'];
        $institucion=$_POST['institucion'];

        $band_2=false;
        $id_institucion="";
        if($var=$this->model->existe_institucion($institucion)){
           $id_institucion=$var['id_institucion'];
           $band_2=true;
        }
        //var_dump($id_institucion ," con");

        $id_experiencia_tabla=$_POST['id_experiencia_tabla'];
        //var_dump("aqui",$id_experiencia_tabla);
        $fecha_ingreso=$_POST['start'];
        $fecha_egreso=$_POST['end'];
        //var_dump($fecha_ingreso);
        if($this->model->update([
            'institucion'=>$institucion,
            'id_pais'=>$pais,
            'id_institucion_tipo'=>$id_institucion_tipo,
            'id_institucion'=>$id_institucion,
            'cargo'=>$cargo,
            'id_cargo_experiencia'=>$id_cargo_experiencia,
            'id_tipo_experiencia'=>$id_tipo_experiencia,
            'fecha_ingreso'=>date("Y/m/d", strtotime($fecha_ingreso)),
            'fecha_egreso'=>date("Y/m/d", strtotime($fecha_egreso)),
            'id_estado'=>$estado,
            'id_docente'=>$_SESSION['id_docente'],
            'ano_experiencia'=>$ano_experiencia,
            'periodo_admin'=>$periodo_admin,
            'pdf_constancia'=>$file_publico,
            'id_persona'=>$_SESSION['id_persona'],
            'materia'=>$unidad_c,
            'id_nivel_academico'=>$nivel,
            'id_modalidad_estudio'=>$modalidad,
            'semestre'=>$semestre,
            'periodo_lectivo'=>$periodo_docencia,
            'naturaleza'=>$naturaleza,
            'horas_semanales'=>$horas,
            'estudiantes_atendidos'=>$nro_estudiantes,
            'lugar'=>$direccion,
            'id_experiencia_tabla'=>$id_experiencia_tabla          
            ])){
            $mensaje='<div class="alert alert-success">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    La Experiencia ha sido Actualizada <a class="alert-link" href="#">Exitosamente</a>.
                </div>';
                
        }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar la <a class='alert-link' href='#'>Experiencia</a>
            </div>";
        }

        $this->view->mensaje=$mensaje;
        $this->render($param);

    }

    function delete($param=null){
        
        list($id_experiencia_tabla,$id_tipo_experiencia, $id_institucion_tipo, $id_experiencia_docente)=explode(',', $param[0]);

        if($this->model->delete([
            'id_experiencia_tabla'=>$id_experiencia_tabla,
            'id_tipo_experiencia'=>$id_tipo_experiencia,
            'id_experiencia_docente'=>$id_experiencia_docente,
            'id_institucion_tipo'=>$id_institucion_tipo
            ])){

            $mensaje="<div class='alert alert-success alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            <b>El Estudio </b> ha sido Removido correctamente! <a class='alert-link' href='#'></a>
        </div>";

       }else{
        $mensaje="<div class='alert alert-danger alert-dismissable'>
        <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            <b>ERROR: </b>Ha ocurrido un error al intentar remover el <a class='alert-link' href='#'>Estudio</a>
        </div>";
 
       }

       $this->view->mensaje=$mensaje;
       $this->render();


    }


}
?>