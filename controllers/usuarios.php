<?php
    include_once 'sesiones/session_admin.php';
    include_once 'scripts/correos.php';

    class Usuarios extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->usuarios=[]; 

            $this->view->perfiles=[]; 
            $this->view->paises=[]; 
            $this->view->estadosCivil=[];  
            $this->view->generos=[];
            $this->view->etnias=[];
            $this->view->documentoidentidad=[];
            $this->view->nacionalidades=[];    
            $this->view->tcorreos=[];
            $this->view->telefono_tipo=[];
            $this->view->cod_area_telefono=[];
            $this->view->discapacidades=[];
            $this->view->tipo_discapacidades=[];
            $this->view->estados=[];            
            $this->view->ciudades=[];
            $this->view->domicilios=[];
            $this->view->parroquias=[];
            $this->view->informacion=[];
            $this->view->informacion_u=[];
            $this->view->informacion_d=[];
            $this->view->perfiles=[];
            $this->view->roles=[];
           

            
        }

        function render(){ 
            //revisar estrutura

            $usuarios=$this->model->get();
            $this->view->usuarios=$usuarios;

            $this->view->render('gestion_usuarios/usuarios');        
        }

        //render censodocente
        function censodocente(){ 
          //revisar estrutura

          $usuarios=$this->model->get();
          $this->view->usuarios=$usuarios;

          $this->view->render('censodocente/index');        
      }

     function viewAdd(){

        //obtener ejes regionales
        $ejes_regionales=$this->model->getEjeRegional();
        $this->view->ejes_regionales=$ejes_regionales;

        //obtener centros de estudio
        $centros=$this->model->getCentro();
        $this->view->centros=$centros;
        
        //obtener programas
        $programas=$this->model->getPrograma();
        $this->view->programas=$programas;
        //obtener preguntas
        $preguntas=$this->model->getPregunta();
        $this->view->preguntas=$preguntas;

        $perfiles=$this->model->getCatalogo("perfil");
        $this->view->perfiles=$perfiles;
          //tablas catalogo donde se utilizo una sola funcion para mostrar sus datos
          $generos=$this->model->getCatalogo("genero");
          $this->view->generos=$generos;

          $estadosCivil=$this->model->getCatalogo("estado_civil");
          $this->view->estadosCivil=$estadosCivil;

          $etnias=$this->model->getCatalogo("etnia");
          $this->view->etnias=$etnias;

          $documentoidentidad=$this->model->getCatalogo("documento_identidad_tipo");
          $this->view->documentoidentidad=$documentoidentidad;

          $domicilios=$this->model->getCatalogo("domicilio_detalle_tipo");
          $this->view->domicilios=$domicilios;

          $parroquias=$this->model->getCatalogo("parroquia");
          $this->view->parroquias=$parroquias;

          $tcorreos=$this->model->getCatalogo("correo_tipo");
          $this->view->tcorreos=$tcorreos;

          $telefono_tipo=$this->model->getCatalogo("telefono_tipo");
          $this->view->telefono_tipo=$telefono_tipo;
     
          $cod_area_telefono=$this->model->getCatalogo("telefono_codigo_area");
          $this->view->cod_area_telefono=$cod_area_telefono;

          $perfiles=$this->model->getCatalogo('perfil');
          $this->view->perfiles=$perfiles;

          $roles=$this->model->getCatalogo('rol');
          $this->view->roles=$roles;

          $tipo_discapacidades=$this->model->getCatalogo("tipo_discapacidad");
          $this->view->tipo_discapacidades=$tipo_discapacidades;

          $dedicaciones=$this->model->getCatalogo("docente_dedicacion");
          $this->view->dedicaciones=$dedicaciones;

          $docente_estatus=$this->model->getCatalogo("docente_estatus");
          $this->view->docente_estatus=$docente_estatus;

          $clasificaciones=$this->model->getCatalogo("clasificacion_docente");
          $this->view->clasificaciones=$clasificaciones;

          $aldeas_ubv=$this->model->getCatalogo("aldea_ubv");
          $this->view->aldeas_ubv=$aldeas_ubv;
      
          $escalafones=$this->model->getCatalogo("escalafon");
          $this->view->escalafones=$escalafones;

        /*  $discapacidades=$this->model->getCatalogo("discapacidad");
          $this->view->discapacidades=$discapacidades;

          */


      //////////////////////////////////////////////////////////////////////

       /*   

          $estados=$this->model->getEstado();
          $this->view->estados=$estados;*/
          $paises=$this->model->getPais();
          $this->view->paises=$paises;
          $nacionalidades=$this->model->getNacionalidad();
          $this->view->nacionalidades=$nacionalidades;

        /*  $ciudades=$this->model->getCiudad();
          $this->view->ciudades=$ciudades;*/

          $this->view->render('gestion_usuarios/addUser'); 


     }
        
     function viewAdd2(){

      //obtener ejes regionales
      $ejes_regionales=$this->model->getEjeRegional();
      $this->view->ejes_regionales=$ejes_regionales;

      //obtener centros de estudio
      $centros=$this->model->getCentro();
      $this->view->centros=$centros;
      
      //obtener programas
      $programas=$this->model->getPrograma();
      $this->view->programas=$programas;
      //obtener preguntas
      $preguntas=$this->model->getPregunta();
      $this->view->preguntas=$preguntas;

      $perfiles=$this->model->getCatalogo("perfil");
      $this->view->perfiles=$perfiles;

        //tablas catalogo donde se utilizo una sola funcion para mostrar sus datos
        $generos=$this->model->getCatalogo("genero");
        $this->view->generos=$generos;

        $estadosCivil=$this->model->getCatalogo("estado_civil");
        $this->view->estadosCivil=$estadosCivil;

        $etnias=$this->model->getCatalogo("etnia");
        $this->view->etnias=$etnias;

        $documentoidentidad=$this->model->getCatalogo("documento_identidad_tipo");
        $this->view->documentoidentidad=$documentoidentidad;

        $domicilios=$this->model->getCatalogo("domicilio_detalle_tipo");
        $this->view->domicilios=$domicilios;

        $parroquias=$this->model->getCatalogo("parroquia");
        $this->view->parroquias=$parroquias;

        $tcorreos=$this->model->getCatalogo("correo_tipo");
        $this->view->tcorreos=$tcorreos;

        $telefono_tipo=$this->model->getCatalogo("telefono_tipo");
        $this->view->telefono_tipo=$telefono_tipo;
   
        $cod_area_telefono=$this->model->getCatalogo("telefono_codigo_area");
        $this->view->cod_area_telefono=$cod_area_telefono;

        $perfiles=$this->model->getCatalogo('perfil');
        $this->view->perfiles=$perfiles;

        $roles=$this->model->getCatalogo('rol');
        $this->view->roles=$roles;

      /*  $discapacidades=$this->model->getCatalogo("discapacidad");
        $this->view->discapacidades=$discapacidades;

        $tipo_discapacidades=$this->model->getCatalogo("tipo_discapacidad");
        $this->view->tipo_discapacidades=$tipo_discapacidades;*/


    //////////////////////////////////////////////////////////////////////

     /*   $paises=$this->model->getPais();
        $this->view->paises=$paises;

        $estados=$this->model->getEstado();
        $this->view->estados=$estados;*/

        $nacionalidades=$this->model->getNacionalidad();
        $this->view->nacionalidades=$nacionalidades;

      /*  $ciudades=$this->model->getCiudad();
        $this->view->ciudades=$ciudades;*/

        $this->view->render('gestion_usuarios/usuarioregistro'); 


   }

   function verUsuario($param = null){
    $id_persona = $param[0];
    $this->view->id_persona=$id_persona;
    //trae informacion personal de usuario
    $informacion = $this->model->getbyIdUsuarioInfo($id_persona);
    if($informacion->id_perfil==1){
      $programas_docente = $this->model->getProgramasDocente($informacion->id_docente);
      $k=1;
      foreach($programas_docente as $row){
        $programa_docente=new Persona();
        $programa_docente=$row;
        $programa = $this->model->getProgramaDocentebyID($programa_docente->id_docente_programa);

        $doc_prog[$k]=[
          'id_docente_programa'.$k=>$programa->id_docente_programa,
          'id_programa'.$k=>$programa->id_programa,
          'programa'.$k=>$programa->programa
        ];
        $this->view->doc_prog[$k]=$doc_prog[$k];
        $k++;
      }
      $this->view->programas_docente = $programas_docente;
    }

    //trae informacion de usuario
    $informacion_u = $this->model->getbyIdUsuarioInfoU($id_persona);

    $j=1;
    foreach($informacion_u as $row){
      $roles_u=new Persona();
      $roles_u=$row;
      $rol_u = $this->model->getRolbyID($id_persona, $roles_u->id_rol);

      $rol[$j]=[
        'id_usuario'.$j=>$rol_u->id_usuario,
        'id_rol'.$j=>$rol_u->id_rol,
        'rol'.$j=>$rol_u->rol
      ];

      $this->view->rol[$j]=$rol[$j];
      $j++;

    }

    //trae informacion de domicilio de la persona
    $domicilio_p = $this->model->getDomicilioPersona($id_persona);
    $this->view->domicilio_p=$domicilio_p;

    //trae las discapacidades
    $informacion_d = $this->model->getbyIdUsuarioInfoD($id_persona);
    $i=1;
    //var_dump("Cuenta >>",count($informacion_d));

    foreach($informacion_d as $row){
      $disc_u=new Persona;
      $disc_u=$row;
      $obj=$this->model->getbyIdDisc($disc_u->id_persona_discapacidad);
      
      $disc[$i]=[
                'id_persona_discapacidad'.$i=>$obj->id_persona_discapacidad,
                'id_discapacidad'.$i=>$obj->id_discapacidad,
                'codigo_conapdis'.$i=>$obj->codigo_conapdis,
                'observacion'.$i=>$obj->observacion,
                'discapacidad'.$i=>$obj->discapacidad,
                'id_tipo_discapacidad'.$i=>$obj->id_tipo_discapacidad,
                'tipo_discapacidad'.$i=>$obj->tipo_discapacidad
              ];

      $this->view->disc[$i]=$disc[$i];
      $i++;
    }
    //var_dump($disc[1] , $disc[2]);
    $informacion_n = $this->model->getbyIdUsuarioInfoN($id_persona);

    $this->view->informacion = $informacion;
    $this->view->informacion_u = $informacion_u;
    $this->view->informacion_d = $informacion_d;
    $this->view->informacion_n = $informacion_n;
    $this->view->mensaje ="";
    ///////////////////////////////////////////////////////////////////////

     //tablas catalogo donde se utilizo una sola funcion para mostrar sus datos
    $generos=$this->model->getCatalogo("genero");
    $this->view->generos=$generos;

    $estadosCivil=$this->model->getCatalogo("estado_civil");
    $this->view->estadosCivil=$estadosCivil;

    $etnias=$this->model->getCatalogo("etnia");
    $this->view->etnias=$etnias;

    $documentoidentidad=$this->model->getCatalogo("documento_identidad_tipo");
    $this->view->documentoidentidad=$documentoidentidad;

    $domicilios=$this->model->getCatalogo("domicilio_detalle_tipo");
    $this->view->domicilios=$domicilios;

    $tcorreos=$this->model->getCatalogo("correo_tipo");
    $this->view->tcorreos=$tcorreos;

    $telefono_tipo=$this->model->getCatalogo("telefono_tipo");
    $this->view->telefono_tipo=$telefono_tipo;

    $cod_area_telefono=$this->model->getCatalogo("telefono_codigo_area");
    $this->view->cod_area_telefono=$cod_area_telefono;
    
    $perfiles=$this->model->getCatalogo('perfil');
    $this->view->perfiles=$perfiles;
    //////////////////////////////////////////////////////////////////
    $paises=$this->model->getPais();
    $this->view->paises=$paises;

    $nacionalidades=$this->model->getNacionalidad();
    $this->view->nacionalidades=$nacionalidades;

    $roles=$this->model->getCatalogo('rol');
    $this->view->roles=$roles;

    //docentes
    //obtener ejes regionales
    $ejes_regionales=$this->model->getEjeRegional();
    $this->view->ejes_regionales=$ejes_regionales;

    //obtener centros de estudio
    $centros=$this->model->getCentro();
    $this->view->centros=$centros;

    //obtener programas
    $programas=$this->model->getPrograma();
    $this->view->programas=$programas;
    //obtener preguntas
    $preguntas=$this->model->getPregunta();
    $this->view->preguntas=$preguntas;

    $dedicaciones=$this->model->getCatalogo("docente_dedicacion");
    $this->view->dedicaciones=$dedicaciones;

    $docente_estatus=$this->model->getCatalogo("docente_estatus");
    $this->view->docente_estatus=$docente_estatus;

    $clasificaciones=$this->model->getCatalogo("clasificacion_docente");
    $this->view->clasificaciones=$clasificaciones;

    $escalafones=$this->model->getCatalogo("escalafon");
    $this->view->escalafones=$escalafones;

    $aldeas_ubv=$this->model->getCatalogo("aldea_ubv");
    $this->view->aldeas_ubv=$aldeas_ubv;
    
    $tipo_discapacidades=$this->model->getCatalogo("tipo_discapacidad");
    $this->view->tipo_discapacidades=$tipo_discapacidades;
   

    $this->view->render('gestion_usuarios/editUser');

    
}
        function verUsuario2($param = null){
            $id_persona = $param[0];
            //trae informacion personal de usuario
            $informacion = $this->model->getbyIdUsuarioInfo($id_persona);
            
            //trae informacion de usuario
           $informacion_u = $this->model->getbyIdUsuarioInfoU($id_persona);
           //trae las discapacidades
           $informacion_d = $this->model->getbyIdUsuarioInfoD($id_persona);
            
           $informacion_n = $this->model->getbyIdUsuarioInfoN($id_persona);

            $this->view->informacion = $informacion;
            $this->view->informacion_u = $informacion_u;
            $this->view->informacion_d = $informacion_d;
            $this->view->informacion_n = $informacion_n;
            $this->view->mensaje ="";
            ///////////////////////////////////////////////////////////////////////

             //tablas catalogo donde se utilizo una sola funcion para mostrar sus datos
            $generos=$this->model->getCatalogo("genero");
            $this->view->generos=$generos;

            $estadosCivil=$this->model->getCatalogo("estado_civil");
            $this->view->estadosCivil=$estadosCivil;

            $etnias=$this->model->getCatalogo("etnia");
            $this->view->etnias=$etnias;

            $documentoidentidad=$this->model->getCatalogo("documento_identidad_tipo");
            $this->view->documentoidentidad=$documentoidentidad;

            $domicilios=$this->model->getCatalogo("domicilio_detalle_tipo");
            $this->view->domicilios=$domicilios;

            $tcorreos=$this->model->getCatalogo("correo_tipo");
            $this->view->tcorreos=$tcorreos;

            $telefono_tipo=$this->model->getCatalogo("telefono_tipo");
            $this->view->telefono_tipo=$telefono_tipo;
       
            $cod_area_telefono=$this->model->getCatalogo("telefono_codigo_area");
            $this->view->cod_area_telefono=$cod_area_telefono;
            
            $perfiles=$this->model->getCatalogo('perfil');
            $this->view->perfiles=$perfiles;
           //////////////////////////////////////////////////////////////////
            $nacionalidades=$this->model->getNacionalidad();
            $this->view->nacionalidades=$nacionalidades;

            $roles=$this->model->getCatalogo('rol');
            $this->view->roles=$roles;

            $this->view->render('gestion_usuarios/usuariodetalle');

            
        }

        function verUsuarioInfo($param = null){
          $id_persona = $param[0];

          $this->view->id_persona=$id_persona;
          //trae informacion personal de usuario
          $informacion = $this->model->getbyIdUsuarioInfo($id_persona);
          if($informacion->id_perfil==1){
            $programas_docente = $this->model->getProgramasDocente($informacion->id_docente);
            $k=1;
            foreach($programas_docente as $row){
              $programa_docente=new Persona();
              $programa_docente=$row;
              $programa = $this->model->getProgramaDocentebyID($programa_docente->id_docente_programa);
      
              $doc_prog[$k]=[
                'id_docente_programa'.$k=>$programa->id_docente_programa,
                'id_programa'.$k=>$programa->id_programa,
                'programa'.$k=>$programa->programa
              ];
              $this->view->doc_prog[$k]=$doc_prog[$k];
              $k++;
            }
            $this->view->programas_docente = $programas_docente;
          }
          //trae informacion de usuario
          $informacion_u = $this->model->getbyIdUsuarioInfoU($id_persona);
      
          $j=1;
          foreach($informacion_u as $row){
            $roles_u=new Persona();
            $roles_u=$row;
            $rol_u = $this->model->getRolbyID($id_persona, $roles_u->id_rol);
      
            $rol[$j]=[
              'id_usuario'.$j=>$rol_u->id_usuario,
              'id_rol'.$j=>$rol_u->id_rol,
              'rol'.$j=>$rol_u->rol
            ];
      
            $this->view->rol[$j]=$rol[$j];
            $j++;
      
          }
      
          //trae informacion de domicilio de la persona
          $domicilio_p = $this->model->getDomicilioPersona($id_persona);
          $this->view->domicilio_p=$domicilio_p;
      
          //trae las discapacidades
          $informacion_d = $this->model->getbyIdUsuarioInfoD($id_persona);
          $i=1;
          //var_dump("Cuenta >>",count($informacion_d));
      
          foreach($informacion_d as $row){
            $disc_u=new Persona;
            $disc_u=$row;
            $obj=$this->model->getbyIdDisc($disc_u->id_persona_discapacidad);
            
            $disc[$i]=[
                      'id_persona_discapacidad'.$i=>$obj->id_persona_discapacidad,
                      'id_discapacidad'.$i=>$obj->id_discapacidad,
                      'codigo_conapdis'.$i=>$obj->codigo_conapdis,
                      'observacion'.$i=>$obj->observacion,
                      'discapacidad'.$i=>$obj->discapacidad,
                      'id_tipo_discapacidad'.$i=>$obj->id_tipo_discapacidad,
                      'tipo_discapacidad'.$i=>$obj->tipo_discapacidad
                    ];
      
            $this->view->disc[$i]=$disc[$i];
            $i++;
          }
          //var_dump($disc[1] , $disc[2]);
          $informacion_n = $this->model->getbyIdUsuarioInfoN($id_persona);
      
          $this->view->informacion = $informacion;
          $this->view->informacion_u = $informacion_u;
          $this->view->informacion_d = $informacion_d;
          $this->view->informacion_n = $informacion_n;
          $this->view->mensaje ="";
          ///////////////////////////////////////////////////////////////////////
      
           //tablas catalogo donde se utilizo una sola funcion para mostrar sus datos
          $generos=$this->model->getCatalogo("genero");
          $this->view->generos=$generos;
      
          $estadosCivil=$this->model->getCatalogo("estado_civil");
          $this->view->estadosCivil=$estadosCivil;
      
          $etnias=$this->model->getCatalogo("etnia");
          $this->view->etnias=$etnias;
      
          $documentoidentidad=$this->model->getCatalogo("documento_identidad_tipo");
          $this->view->documentoidentidad=$documentoidentidad;
      
          $domicilios=$this->model->getCatalogo("domicilio_detalle_tipo");
          $this->view->domicilios=$domicilios;
      
          $tcorreos=$this->model->getCatalogo("correo_tipo");
          $this->view->tcorreos=$tcorreos;
      
          $telefono_tipo=$this->model->getCatalogo("telefono_tipo");
          $this->view->telefono_tipo=$telefono_tipo;
      
          $cod_area_telefono=$this->model->getCatalogo("telefono_codigo_area");
          $this->view->cod_area_telefono=$cod_area_telefono;
          
          $perfiles=$this->model->getCatalogo('perfil');
          $this->view->perfiles=$perfiles;
          //////////////////////////////////////////////////////////////////
          $paises=$this->model->getPais();
          $this->view->paises=$paises;
      
          $nacionalidades=$this->model->getNacionalidad();
          $this->view->nacionalidades=$nacionalidades;
      
          $roles=$this->model->getCatalogo('rol');
          $this->view->roles=$roles;
      
          //docentes
          //obtener ejes regionales
          $ejes_regionales=$this->model->getEjeRegional();
          $this->view->ejes_regionales=$ejes_regionales;
      
          //obtener centros de estudio
          $centros=$this->model->getCentro();
          $this->view->centros=$centros;
      
          //obtener programas
          $programas=$this->model->getPrograma();
          $this->view->programas=$programas;
          //obtener preguntas
          $preguntas=$this->model->getPregunta();
          $this->view->preguntas=$preguntas;
      
          $dedicaciones=$this->model->getCatalogo("docente_dedicacion");
          $this->view->dedicaciones=$dedicaciones;
      
          $docente_estatus=$this->model->getCatalogo("docente_estatus");
          $this->view->docente_estatus=$docente_estatus;
      
          $clasificaciones=$this->model->getCatalogo("clasificacion_docente");
          $this->view->clasificaciones=$clasificaciones;
      
          $escalafones=$this->model->getCatalogo("escalafon");
          $this->view->escalafones=$escalafones;
      
          $tipo_discapacidades=$this->model->getCatalogo("tipo_discapacidad");
          $this->view->tipo_discapacidades=$tipo_discapacidades;
         
      
          $this->view->render('gestion_usuarios/detailUser');
      
          
      }

        function verUsuarioInfo2($param = null){
             $id_persona = $param[0];
             //trae informacion personal de usuario
             $informacion = $this->model->getbyIdUsuarioInfo($id_persona);
             //trae informacion de usuario (roles)
            $informacion_u = $this->model->getbyIdUsuarioInfoU($id_persona);
            //trae las discapacidades
            $informacion_d = $this->model->getbyIdUsuarioInfoD($id_persona);

            $informacion_n = $this->model->getbyIdUsuarioInfoN($id_persona);
            

             $this->view->informacion = $informacion;
             $this->view->informacion_u = $informacion_u;
             $this->view->informacion_d = $informacion_d;
             $this->view->informacion_n = $informacion_n;
             $this->view->mensaje ="";
             //var_dump($informacion_n);
             $this->view->render('gestion_usuarios/usuarioinfo');
         }

         function verUsuarioPerfil($param = null){
          $id_persona = $param[0];
          //trae informacion personal de usuario
          $informacion = $this->model->getbyIdUsuarioInfo($id_persona);
          //trae informacion de usuario (roles)
         $informacion_u = $this->model->getbyIdUsuarioInfoU($id_persona);
         //trae las discapacidades
         $informacion_d = $this->model->getbyIdUsuarioInfoD($id_persona);

         $informacion_n = $this->model->getbyIdUsuarioInfoN($id_persona);
         

          $this->view->informacion = $informacion;
          $this->view->informacion_u = $informacion_u;
          $this->view->informacion_d = $informacion_d;
          $this->view->informacion_n = $informacion_n;
          $this->view->mensaje ="";
          //var_dump($informacion);
          $this->view->render('perfil/usuarioinfo');
      }

         function RegistarUsuario(){//se agrega esta linea
          
            //tabla persona
            $pnombre=$_POST['pnombre'];            
            $snombre=$_POST['snombre'];
            $papellido=$_POST['papellido'];
            $sapellido=$_POST['sapellido'];
            $fnac=$_POST['fnac'];
            $estadoCivil=$_POST['estadocivil'];
            $genero=$_POST['genero'];
            $pais=$_POST['pais'];
                

           //persona etnia no es requerido
           $id_etnia=$_POST['etnia'];

            //tabla persona nacionalidad

            $id_documento_identidad_tipo=$_POST['tdocumento'];            
            $id_nacionalidad=$_POST['nacionalidad'];           
            $numero_documento=$_POST['ndocumento'];

            //persona Telefono
            $id_telefono_tipo=$_POST['tipotelf'];
            $id_telefono_codigo_area=$_POST['cod_area'];
            $telefono_numero=$_POST['numero'];

            //Domicilio persona
           // $id_ciudad=$_POST['ciudad'];
            $id_parroquia=$_POST['parroquia'];
            //domicilio detalle
            $direccion=$_POST['direccion'];

            //domicilio detalle_tipo
            $tipo_domicilio=$_POST['tdomicilio'];

           
           //persona_correo
           $id_correo_tipo=$_POST['tcorreo'];
           $correo=$_POST['correo'];

           

           //persona discapacidad no es requerido
           $id_discapacidad=$_POST['discapacidad'];
           $codigo_conais=$_POST['conadis'];
           $observaciones=$_POST['observaciones'];

           //persona discapacidad no es requerido OTRA SEGUNDA DISCAPACIDAD
           $id_discapacidad1=$_POST['discapacidad1'];
           $observaciones1=$_POST['observaciones1'];

           //Perfil (aqui va un areglo de roles)
 
           $id_rol=$_POST['roles'];
           $id_perfil=$_POST['perfil'];
          /* var_dump($id_perfil);
           exit();*/
           $estatus=$_POST['estatus'];
          


           //usuario se generar automaticamente con datos anteriores..

                       
            $mensaje="";

            if($var=$this->model->existe($numero_documento)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Usuario con Identificación <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($this->model->insert(['pnombre'=>$pnombre,'snombre'=>$snombre,'papellido'=>$papellido,'sapellido'=>$sapellido,
                                    'fnac'=>$fnac,'estadoCivil'=>$estadoCivil,'genero'=>$genero,'pais'=>$pais,
                                    'id_documento_identidad_tipo'=>$id_documento_identidad_tipo,'id_nacionalidad'=>$id_nacionalidad,'numero_documento'=>$numero_documento,                                  
                                    'id_telefono_tipo'=>$id_telefono_tipo,'id_telefono_codigo_area'=>$id_telefono_codigo_area,'telefono_numero'=>$telefono_numero,
                                    'id_ciudad'=>$id_ciudad,
                                    'id_parroquia'=>$id_parroquia,
                                    'direccion'=>$direccion,'tipo_domicilio'=>$tipo_domicilio,
                                    'id_correo_tipo'=>$id_correo_tipo,'correo'=>$correo,'id_etnia'=>$id_etnia,
                                    'id_discapacidad'=>$id_discapacidad,
                                    'codigo_conais'=>$codigo_conais,'observaciones'=>$observaciones,
                                    'id_discapacidad1'=>$id_discapacidad1,'observaciones1'=>$observaciones1,
                                    'id_rol'=>$id_rol,'id_perfil'=>$id_perfil,'estatus'=>$estatus])){
              
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Usuario  agregado Su usario es : <b>ubv$numero_documento</b>  y Su contraseña es :<b> $numero_documento</b> <a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al agregar un Usuario<a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
          }


          function editUser($param){//se agrega esta linea
    
            //ids
            $id_persona=$param[0]; 
                        
            //tabla persona
            $pnombre=$_POST['pnombre'];            
            $snombre=$_POST['snombre'];
            $papellido=$_POST['papellido'];
            $sapellido=$_POST['sapellido'];
            $fnac=$_POST['fnac'];
            $estadoCivil=$_POST['estadocivil'];
            $genero=$_POST['genero'];
            $pais=$_POST['pais'];
                

            //persona etnia no es requerido
            $id_etnia=$_POST['etnia'];

            //tabla persona nacionalidad

            $id_documento_identidad_tipo=$_POST['tdocumento'];            
            $id_nacionalidad=$_POST['nacionalidad'];           
            $numero_documento=$_POST['ndocumento'];

            //persona Telefono
            $id_telefono_tipo=$_POST['tipotelf'];
            $id_telefono_codigo_area=$_POST['cod_area'];
            $telefono_numero=$_POST['numero'];

            //Domicilio persona
            // $id_ciudad=$_POST['ciudad'];
            $id_parroquia=$_POST['parroquia'];
            //domicilio detalle
            $direccion=$_POST['direccion'];

            //domicilio detalle_tipo
            $tipo_domicilio=$_POST['tdomicilio'];

            //persona_correo
            $id_correo_tipo=$_POST['id_correo_personal'];
            $correo=$_POST['correo_personal'];
            $id_persona_correo=$_POST['id_persona_correo'];

            if(!empty($_POST['correo_institucional'])){
              $id_persona_institucional=$_POST['id_persona_institucional'];
              $id_correo_institucional=$_POST['id_correo_institucional'];
              $correo_institucional=$_POST['correo_institucional'];
            }else{
              $id_persona_institucional="";
              $id_correo_institucional="";
              $correo_institucional="";
            }

            //persona discapacidad no es requerido
            $id_persona_discapacidad=$_POST['id_persona_discapacidad']; 
            $id_discapacidad=$_POST['discapacidad'];
            $codigo_conais=$_POST['conadis'];
            $observaciones=$_POST['observaciones'];

            //persona discapacidad no es requerido OTRA SEGUNDA DISCAPACIDAD
            $id_persona_discapacidad1=$_POST['id_persona_discapacidad1']; 
            $id_discapacidad1=$_POST['discapacidad1'];
            $observaciones1=$_POST['observaciones1'];

            //Perfil (aqui va un areglo de roles)

            $id_rol=$_POST['roles'];

            $perfil=$_POST['perfil'];
            $id_perfil=$_POST['perfil'];
            
            if($id_perfil==1){//si el perfil es igual a trabajador académico
              $id_docente=$_POST['id_docente'];
              $programa=$_POST['programa'];
              $centro_estudio=$_POST['centro_estudio'];
              $eje_regional=$_POST['eje_regional'];
              $eje_municipal=$_POST['eje_municipal'];
              $estatus_d=$_POST['estatus_d'];
              $dedicacion=$_POST['dedicacion'];
              $clasificacion=$_POST['clasificacion'];
              $escalafon=$_POST['escalafon'];
              $aldea=$_POST['aldea'];
            }else{
              $id_docente="";
              $programa="";
              $centro_estudio="";
              $eje_regional="";
              $eje_municipal="";
              $estatus_d="";
              $dedicacion="";
              $clasificacion="";
              $escalafon="";
              $aldea="";
            }
            $estatus=$_POST['estatus'];
            $password=$_POST['password'];

            //hijos y fecha de ingreso
            $fingreso=$_POST['fingreso'];
            $nhijos=$_POST['nhijos'];
            $npais=$_POST['paisnac'];
              /*  var_dump($id_persona,$pnombre,$snombre,$papellido,$sapellido,
                $fnac,$estadoCivil,$genero,$pais,
                $id_documento_identidad_tipo,$id_nacionalidad,$numero_documento,                                  
                $id_telefono_tipo,$id_telefono_codigo_area,$telefono_numero,
                $id_parroquia,
                $direccion,$tipo_domicilio,
                $id_correo_tipo,$correo,$id_etnia,
                $id_discapacidad,
                $codigo_conais,$observaciones,
                $id_discapacidad1,$observaciones1,
                $id_rol,$id_perfil,$estatus);*/

            $mensaje="";
    
            if($this->model->updateUser(['id_persona'=>$id_persona,'pnombre'=>$pnombre,'snombre'=>$snombre,'papellido'=>$papellido,'sapellido'=>$sapellido,
            'fnac'=>$fnac,'estadoCivil'=>$estadoCivil,'genero'=>$genero,'pais'=>$pais,
            'id_documento_identidad_tipo'=>$id_documento_identidad_tipo,'id_nacionalidad'=>$id_nacionalidad,'numero_documento'=>$numero_documento,                                  
            'id_telefono_tipo'=>$id_telefono_tipo,'id_telefono_codigo_area'=>$id_telefono_codigo_area,'telefono_numero'=>$telefono_numero,
            'id_parroquia'=>$id_parroquia,
            'direccion'=>$direccion,'tipo_domicilio'=>$tipo_domicilio,
            'id_correo_tipo'=>$id_correo_tipo,'correo'=>$correo,'id_etnia'=>$id_etnia,
            'id_persona_discapacidad'=>$id_persona_discapacidad,
            'id_discapacidad'=>$id_discapacidad,
            'codigo_conais'=>$codigo_conais,'observaciones'=>$observaciones,
            'id_persona_discapacidad1'=>$id_persona_discapacidad1,
            'id_discapacidad1'=>$id_discapacidad1,'observaciones1'=>$observaciones1,
            'id_rol'=>$id_rol,'perfil'=>$perfil,'id_perfil'=>$id_perfil,'estatus'=>$estatus,'password'=>$password,
            'nhijos'=>$nhijos,'fingreso'=>date("Y-m-d", strtotime($fingreso)), 'npais'=>$npais,
            'programa'=>$programa,'centro_estudio'=>$centro_estudio,'eje_regional'=>$eje_regional,'eje_municipal'=>$eje_municipal,
            'estatus_d'=>$estatus_d,'dedicacion'=>$dedicacion,'clasificacion'=>$clasificacion,
            'escalafon'=>$escalafon,'id_correo_institucional'=>$id_correo_institucional,'correo_institucional'=>$correo_institucional,
            'id_persona_correo'=>$id_persona_correo,'id_persona_institucional'=>$id_persona_institucional,
            'aldea'=>$aldea,'id_docente'=>$id_docente])){

            $mensaje="<div class='alert alert-success alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
            Usuario  Actualizado con Éxito su Usuario es: <b>ubv$numero_documento</b> y su contraseña es:<b> $password</b> <a class='alert-link' href='#'></a></div>";

              // enviar correos correspondientes

              $mensaje_correo="<p>
              ¡Felicidades! Sr.(a) ".$pnombre." ".$papellido." su Cuenta ha Sido Actualizada de manera 
              Exitosa en el Sistema Integrado de Desarrollo de Las Trabajadoras 
              y Los Trabajadores Académicos. Su Usuario de Acceso es: <b>ubv$numero_documento</b> y Su Clave de Acceso es: <b> $password</b> 
              <b>(SIDTA)</b>. 
              <br><br>
              </p>
              <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";

              $correo_usuario=new Correo();
                  $var=$correo_usuario->EnviarCorreo([
                      'correo_destino'=>$correo,
                      'nombre_destino'=>$pnombre,
                      'apellido_destino'=>$papellido,
                      'asunto'=>'SIDTA - Actualización de Usuario',
                      'mensaje'=>$mensaje_correo
                      ]);

            }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Ha Ocurrido un Error al Intentar Editar el Usuario <b>ubv$numero_documento</b>. <a class='alert-link' href='#'></a>
            </div>";
            
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }


          // respaldo del actualizar
          function ActualizarUsuario(){//se agrega esta linea
    
            //$id_persona=$_SESSION["id_persona"];
            
            //ids
            $id_persona=$_POST['id_persona']; 
                        
            //tabla persona
            $pnombre=$_POST['pnombre'];            
            $snombre=$_POST['snombre'];
            $papellido=$_POST['papellido'];
            $sapellido=$_POST['sapellido'];
            $fnac=$_POST['fnac'];
            $estadoCivil=$_POST['estadocivil'];
            $genero=$_POST['genero'];
            $pais=$_POST['pais'];
                

            //persona etnia no es requerido
            $id_etnia=$_POST['etnia'];

            //tabla persona nacionalidad

            $id_documento_identidad_tipo=$_POST['tdocumento'];            
            $id_nacionalidad=$_POST['nacionalidad'];           
            $numero_documento=$_POST['ndocumento'];

            //persona Telefono
            $id_telefono_tipo=$_POST['tipotelf'];
            $id_telefono_codigo_area=$_POST['cod_area'];
            $telefono_numero=$_POST['numero'];

            //Domicilio persona
            // $id_ciudad=$_POST['ciudad'];
            $id_parroquia=$_POST['parroquia'];
            //domicilio detalle
            $direccion=$_POST['direccion'];

            //domicilio detalle_tipo
            $tipo_domicilio=$_POST['tdomicilio'];


            //persona_correo
            $id_correo_tipo=$_POST['tcorreo'];
            $correo=$_POST['correo'];



            //persona discapacidad no es requerido
            $id_persona_discapacidad=$_POST['id_persona_discapacidad']; 
            $id_discapacidad=$_POST['discapacidad'];
            $codigo_conais=$_POST['conadis'];
            $observaciones=$_POST['observaciones'];

            //persona discapacidad no es requerido OTRA SEGUNDA DISCAPACIDAD
            $id_persona_discapacidad1=$_POST['id_persona_discapacidad1']; 
            $id_discapacidad1=$_POST['discapacidad1'];
            $observaciones1=$_POST['observaciones1'];

            //Perfil (aqui va un areglo de roles)

            $id_rol=$_POST['roles'];

            $perfil=$_POST['perfil'];
            $id_perfil=$_POST['perfil'];
           
          
            $estatus=$_POST['estatus'];
            $password=$_POST['password'];

              /*  var_dump($id_persona,$pnombre,$snombre,$papellido,$sapellido,
                $fnac,$estadoCivil,$genero,$pais,
                $id_documento_identidad_tipo,$id_nacionalidad,$numero_documento,                                  
                $id_telefono_tipo,$id_telefono_codigo_area,$telefono_numero,
                $id_parroquia,
                $direccion,$tipo_domicilio,
                $id_correo_tipo,$correo,$id_etnia,
                $id_discapacidad,
                $codigo_conais,$observaciones,
                $id_discapacidad1,$observaciones1,
                $id_rol,$id_perfil,$estatus);*/

            $mensaje="";
    
            if($this->model->update(['id_persona'=>$id_persona,'pnombre'=>$pnombre,'snombre'=>$snombre,'papellido'=>$papellido,'sapellido'=>$sapellido,
            'fnac'=>$fnac,'estadoCivil'=>$estadoCivil,'genero'=>$genero,'pais'=>$pais,
            'id_documento_identidad_tipo'=>$id_documento_identidad_tipo,'id_nacionalidad'=>$id_nacionalidad,'numero_documento'=>$numero_documento,                                  
            'id_telefono_tipo'=>$id_telefono_tipo,'id_telefono_codigo_area'=>$id_telefono_codigo_area,'telefono_numero'=>$telefono_numero,
            'id_parroquia'=>$id_parroquia,
            'direccion'=>$direccion,'tipo_domicilio'=>$tipo_domicilio,
            'id_correo_tipo'=>$id_correo_tipo,'correo'=>$correo,'id_etnia'=>$id_etnia,
            'id_persona_discapacidad'=>$id_persona_discapacidad,
            'id_discapacidad'=>$id_discapacidad,
            'codigo_conais'=>$codigo_conais,'observaciones'=>$observaciones,
            'id_persona_discapacidad1'=>$id_persona_discapacidad1,
            'id_discapacidad1'=>$id_discapacidad1,'observaciones1'=>$observaciones1,
            'id_rol'=>$id_rol,'perfil'=>$perfil,'id_perfil'=>$id_perfil,'estatus'=>$estatus,'password'=>$password])){

            $mensaje="<div class='alert alert-success alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
            Usuario  agregado Su usario es : <b>ubv$numero_documento</b>  y Su contraseña es :<b> $numero_documento</b> <a class='alert-link' href='#'></a></div>";


            ?>

            <script>alert("Usuario actualizado Correctamente");
            location.href='<?php echo constant ('URL').'usuarios/verUsuario/'.$id_persona; ?>'
            </script>
    
            <?php

            }else{
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
            Ha ocurrido un error al agregar un Usuario<a class='alert-link' href='#'></a>
            </div>";

            ?>

            <script>alert("Ha ocurrido un error al actualizar usuario");
             location.href='<?php echo constant ('URL').'usuarios/verUsuario/'.$id_persona; ?>'
            </script>
    
            <?php

            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }

          //GET DE PAIS PARA SECCION FAMILIAR 11/02/21

          function getByPais(){
            $paises = $this->model->getCatalogo('pais');
            echo '<option value="">Seleccione el estado correspondiente</option>';
            foreach($paises as $row){
              $pais=new Estructura();
              $pais=$row;
              echo '<option value="'.$pais->id.'">'.$pais->descripcion.'</option>';
            }
          }

          


          // inicio combo dependiente ubicacion
      function getEstadosPais(){
        $id_pais=$_POST['pais'];
        $estados = $this->model->getEstadosbyPais($id_pais);
        echo '<option value="">Seleccione el estado correspondiente</option>';
        foreach($estados as $row){
          $estado=new Estructura();
          $estado=$row;
          echo '<option value="'.$estado->id_estado.'">'.$estado->descripcion.'</option>';
        }
      }

      function getMunicipiosEstado(){
        $id_estado=$_POST['estado'];
        $municipios = $this->model->getMunicipiosbyEstado($id_estado);
        echo '<option value="">Seleccione el municipio correspondiente</option>';
        foreach($municipios as $row){
          $municipio=new Estructura();
          $municipio=$row;
          echo '<option value="'.$municipio->id_municipio.'">'.$municipio->descripcion.'</option>';
        }
      }

      function getParroquiasMunicipio(){
        $id_municipio=$_POST['municipio'];
        $parroquias = $this->model->getParroquiasbyMunicipio($id_municipio);
        echo '<option value="">Seleccione el municipio correspondiente</option>';
        foreach($parroquias as $row){
          $parroquia=new Estructura();
          $parroquia=$row;
          echo '<option value="'.$parroquia->id_parroquia.'">'.$parroquia->descripcion.'</option>';
        }
      }
      // fin combo dependiente ubicacion
      // inicio combo dependiente discapacidades
      function getDiscapacidadesTipo(){
        $id_tipo_discapacidad=$_POST['tipo_discapacidad'];
        $discapacidades = $this->model->getDiscapacidadesbyTipo($id_tipo_discapacidad);
        
        echo '<option value="">Seleccione la discapacidad correspondiente</option>';
        foreach($discapacidades as $row){
          $discapacidad=new Estructura();
          $discapacidad=$row;
          echo '<option value="'.$discapacidad->id_discapacidad.'">'.$discapacidad->descripcion.'</option>';
        }
      }
      // fin combo dependiente discapacidades

      //añadir usuario 
      function addUser(){

          //tabla persona
          $pnombre=$_POST['pnombre'];            
          $snombre=$_POST['snombre'];
          $papellido=$_POST['papellido'];
          $sapellido=$_POST['sapellido'];
          $fnac=$_POST['fnac'];
          $estadoCivil=$_POST['estadocivil'];
          $genero=$_POST['genero'];
          $pais=$_POST['pais'];
          //persona etnia no es requerido
          $id_etnia=$_POST['etnia'];

          //tabla persona nacionalidad
          $id_documento_identidad_tipo=$_POST['tdocumento'];            
          $id_nacionalidad=$_POST['nacionalidad'];           
          $numero_documento=$_POST['ndocumento'];

          //persona Telefono
          $id_telefono_tipo=$_POST['tipotelf'];
          $id_telefono_codigo_area=$_POST['cod_area'];
          $telefono_numero=$_POST['numero'];

          //Domicilio persona
          // $id_ciudad=$_POST['ciudad'];
          $id_parroquia=$_POST['parroquia'];
          //domicilio detalle
          $direccion=$_POST['direccion'];

          //domicilio detalle_tipo
          $tipo_domicilio=$_POST['tdomicilio'];


          //persona_correo
       //   $id_correo_tipo=$_POST['tcorreo'];
         // $correo=$_POST['correo'];

         $correo_personal=$_POST['correo_personal'];
         $correo_institucional=$_POST['correo_institucional'];
        // EJE 
        $eje_municipal=$_POST['eje_municipal'];
        

          //persona discapacidad no es requerido
          $id_discapacidad=$_POST['discapacidad'];
          $codigo_conais=$_POST['conadis'];
          $observaciones=$_POST['observaciones'];

          //persona discapacidad no es requerido OTRA SEGUNDA DISCAPACIDAD
          $id_discapacidad1=$_POST['discapacidad1'];
          $observaciones1=$_POST['observaciones1'];

          //Perfil (aqui va un areglo de roles)

          $id_rol=$_POST['roles'];
          $id_perfil=$_POST['perfil'];

          if($id_perfil==1){//si el perfil es igual a trabajador académico
            $programa=$_POST['programa'];
            $centro_estudio=$_POST['centro_estudio'];
            $eje_regional=$_POST['eje_regional'];
            $estatus_d=$_POST['estatus_d'];
            $dedicacion=$_POST['dedicacion'];
            $clasificacion=$_POST['clasificacion'];
            $escalafon=$_POST['escalafon'];
            $aldea=$_POST['aldea'];
          }else{
            $programa="";
            $centro_estudio="";
            $eje_regional="";
            $estatus_d="";
            $dedicacion="";
            $clasificacion="";
            $escalafon="";
            $aldea="";
          }
          /* var_dump($id_perfil);
          exit();*/
          $estatus=$_POST['estatus'];

          //hijos y fecha de ingreso
          $fingreso=$_POST['fingreso'];
          $nhijos=$_POST['nhijos'];

          //seguridad de accceso y recuperar contraseña
          //seguridad
          $pregunta1=$_POST['pregunta1'];
          $respuesta1=$_POST['respuesta1'];

          $pregunta2=$_POST['pregunta2'];
          $respuesta2=$_POST['respuesta2'];
          if($pregunta1==$pregunta2){
            $mensaje="<div class='alert alert-danger alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close type='button'></button>
            <i>ERROR:</i> Las <a class='alert-link' href='#'> preguntas de seguridad</a>  deben ser diferentes</div>";
            $this->view->mensaje=$mensaje;
            $this->render();
            exit();
            
          }elseif($respuesta1==$respuesta2){
              $mensaje="<div class='alert alert-danger alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'></button>
              <i>ERROR:</i> Las <a class='alert-link' href='#'> respuestas de seguridad</a> deben ser diferentes</div>";
              $this->view->mensaje=$mensaje;
              $this->render();
              exit();

          }
          //usuario se generar automaticamente con datos anteriores..

                       
          $mensaje="";

          if($var=$this->model->existe($numero_documento)){
              $mensaje="<div class='alert alert-danger alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
              Usuario con Identificación <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
              </div>";
              $this->view->mensaje=$mensaje;
              $this->render();
              exit();
          }
          
          if($this->model->insertUser(['pnombre'=>$pnombre,'snombre'=>$snombre,'papellido'=>$papellido,'sapellido'=>$sapellido,
                                  'fnac'=>$fnac,'estadoCivil'=>$estadoCivil,'genero'=>$genero,'pais'=>$pais,
                                  'id_documento_identidad_tipo'=>$id_documento_identidad_tipo,'id_nacionalidad'=>$id_nacionalidad,'numero_documento'=>$numero_documento,                                  
                                  'id_telefono_tipo'=>$id_telefono_tipo,'id_telefono_codigo_area'=>$id_telefono_codigo_area,'telefono_numero'=>$telefono_numero,
                                  'id_ciudad'=>$id_ciudad,
                                  'id_parroquia'=>$id_parroquia,
                                  'direccion'=>$direccion,'tipo_domicilio'=>$tipo_domicilio,
                                  'id_correo_tipo'=>$id_correo_tipo,'correo'=>$correo,'id_etnia'=>$id_etnia,
                                  'id_discapacidad'=>$id_discapacidad,
                                  'codigo_conais'=>$codigo_conais,'observaciones'=>$observaciones,
                                  'id_discapacidad1'=>$id_discapacidad1,'observaciones1'=>$observaciones1,
                                  'id_rol'=>$id_rol,'id_perfil'=>$id_perfil,'estatus'=>$estatus,
                                  'fecha_ingreso'=>$fingreso,'numero_hijos'=>$nhijos,'pregunta1'=>$pregunta1,
                                  'respuesta1'=>$respuesta1,'pregunta2'=>$pregunta2,'respuesta2'=>$respuesta2,
                                  'programa'=>$programa,'centro_estudio'=>$centro_estudio,'eje_regional'=>$eje_regional,
                                  'estatus_d'=>$estatus_d,'dedicacion'=>$dedicacion,'clasificacion'=>$clasificacion,
                                  'escalafon'=>$escalafon,'correo_personal'=>$correo_personal,'correo_institucional'=>$correo_institucional,
                                  'eje_municipal'=>$eje_municipal,'aldea'=>$aldea])){

                                  
            
            $mensaje="<div class='alert alert-success alert-dismissable'>
            <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
            El Usuario ha sido Registrado su Usuario de Acceso es: <b>ubv$numero_documento</b> y Su Clave de Acceso es:<b> $numero_documento</b> <a class='alert-link' href='#'></a></div>";
          
            // enviar correos correspondientes

            $mensaje_correo="<p>
            ¡Felicidades! Sr.(a) ".$pnombre." ".$papellido." su Cuenta ha Sido Registrada de manera 
            Exitosa en el Sistema Integrado de Desarrollo de Las Trabajadoras 
            y Los Trabajadores Académicos. Su Usuario de Acceso es: <b>ubv$numero_documento</b> y Su Clave de Acceso es: <b> $numero_documento</b> 
            <b>(SIDTA)</b>. 
            <br><br>
            </p>
            <p>Viviendo y Venciendo, SIDTA Hecho en Socialismo.</p>";

            $correo_usuario=new Correo();
                $var=$correo_usuario->EnviarCorreo([
                    'correo_destino'=>$correo,
                    'nombre_destino'=>$pnombre,
                    'apellido_destino'=>$papellido,
                    'asunto'=>'SIDTA - Registro de Usuario',
                    'mensaje'=>$mensaje_correo
                    ]);


        }else{
              $mensaje="<div class='alert alert-danger alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
              Ha ocurrido un Error al agregar un Usuario<a class='alert-link' href='#'></a>
          </div>";
          }
          $this->view->mensaje=$mensaje;
          $this->render();
          
        }

// funcion para autocompletar
        function nomina(){
          $cedula=$_POST['ndocumento'];
          $docente=$this->model->getbyCedula($cedula);
          if($docente==false){
            echo trim("false");
          }else{

          

          $id_estado_civil=$this->model->getID(ucfirst(strtolower(trim($docente->estado_civil))), 1);
          $id_genero=$this->model->getID(ucfirst(strtolower(trim($docente->genero))), 2);
          $id_documento_identidad_tipo=$this->model->getID(trim($docente->nacionalidad), 3);
          
          if(!empty($docente->pais_nacimiento)){
            $id_nacionalidad=$this->model->getID(trim($docente->pais_nacimiento), 4);
            $pais_nacimiento=$this->model->getID(trim($docente->pais_nacimiento), 5);

          }else{
            $id_nacionalidad="";
            $pais_nacimiento="";
          }

          list($apellidos, $nombres) = explode(",", $docente->nombres);
          $apellido=$this->separar_cadena($apellidos);
          $nombre=$this->separar_cadena($nombres);

          //separar codigo de area del campo telefono
          if(!empty($docente->telefono)){
            $cod_area=substr($docente->telefono, 0, 3);
            $id_telefono_codigo_area=$this->model->getID($cod_area, 6);
            $telefono=substr($docente->telefono, 3, 10);

          }else{
            $cod_area="";
            $telefono="";
          }

          //evaluar si el correo es Personal o Institucional
          if(!empty($docente->correo_electronico)){
            //separamos el correo por '@'
            list($correo, $dominio) = explode("@", $docente->correo_electronico);
            //var_dump($correo, ">>", $dominio);
            if($dominio != "ubv.edu.ve"){
                $correo_tipo="Personal";
            } else{
                $correo_tipo="Institucional";
            }
            $id_correo_tipo=$this->model->getID($correo_tipo, 7);
          }

          $id_docente_dedicacion=$this->model->getID(strtoupper($docente->dedicacion), 8);
          $id_docente_estatus=$this->model->getID(strtoupper($docente->estatus), 9);
          $id_clasificacion_docente=$this->model->getID(strtoupper($docente->clasificacion_docente), 10);

          //separar el campo cargo para insertarlo correctamente en nuestra base de datos
          switch($docente->cargo){
            case stristr($docente->cargo, 'INSTRUCTOR') !== false:
            $var=stristr($docente->cargo, 'INSTRUCTOR');
            $docente->cargo=substr($var, 0, 10);
            break;

            case stristr($docente->cargo, 'ASISTENTE') !== false:
            $var=stristr($docente->cargo, 'ASISTENTE');    
            $docente->cargo=substr($var, 0, 9);
            break;

            case stristr($docente->cargo, "AGREGADO") !== false:
            $var=stristr($docente->cargo, 'AGREGADO');  
            $docente->cargo=substr($var, 0, 8);
            break;

            case stristr($docente->cargo, "ASOCIADO") !== false:
            $var=stristr($docente->cargo, 'ASOCIADO');     
            $docente->cargo=substr($var, 0, 8);
            break;

            case stristr($docente->cargo, "TITULAR") !== false:
            $var=stristr($docente->cargo, 'TITULAR'); 
            $docente->cargo=substr($var, 0, 7);
            break;

            case stristr($docente->cargo, "EMÉRITO") !== false:
            $var=stristr($docente->cargo, 'EMÉRITO');  
            $docente->cargo=substr($var, 0, 8);              
            break;

            case stristr($docente->cargo, "AUXILIAR") !== false:
            $var=stristr($docente->cargo, 'AUXILIAR');
            $docente->cargo=substr($var, 0, 12);                
            break;
        }
        ////////////////////////////////////////////////////////////////////////////////////
        $id_escalafon=$this->model->getID(strtoupper($docente->cargo), 11);
        //var_dump($nombre[1]);
          echo  $id_estado_civil."|".
                $id_genero ."|".
                $id_documento_identidad_tipo."|".
                $id_nacionalidad."|".
                $nombre[0]."|".
                $nombre[1]."|".
                $apellido[0]."|".
                $apellido[1]."|".
                date("m/d/Y", strtotime($docente->fecha_nacimiento))."|".
                $pais_nacimiento."|".
                $id_telefono_codigo_area."|".
                $telefono."|".
                $id_correo_tipo."|".
                $docente->correo_electronico."|".
                $docente->numero_hijos."|".
                date("m/d/Y", strtotime($docente->fecha_ingreso))."|".
                $id_docente_dedicacion."|".
                $id_docente_estatus."|".
                $id_clasificacion_docente."|".
                $id_escalafon."|".
                $docente->cedula."|".
                $docente->direccion;
            }

        }

        //funcion para separar cadenas de caracteres en 3 casos, 3 2 o 1 (se adapta a casos de nombres)
        function separar_cadena($cadena){
          $cuenta=str_word_count($cadena, 0, 'ÁáéÉíÍÓóÚúÑñç');
//var_dump($cuenta);
          //almacenamos un array indexado [1] etc
          $array_cadena = str_word_count($cadena, 1, 'ÁáéÉíÍÓóÚúÑñç');

          // separamos la cadena en cada uno de los casos si tiene 1 2 o 3 nombres para almacenarlo en la bbdd
          if($cuenta>=3){
              foreach ($array_cadena as $nombre) {
                  $cadena1=$array_cadena[0];
                  $cadena2=$array_cadena[1]. " ". $array_cadena[2]." ". $array_cadena[3];
              }
              $string_separada=array(
                  $cadena1,
                  $cadena2
              );
              return $string_separada;

          }elseif($cuenta==2){
              foreach ($array_cadena as $nombre) {
                  $cadena1=$array_cadena[0];
                  $cadena2=$array_cadena[1];
              }
              $string_separada=array(
                  $cadena1,
                  $cadena2
              );
              return $string_separada;

          }elseif($cuenta==1){
              foreach ($array_cadena as $nombre) {
                  $cadena1=$array_cadena[0];
                  $cadena2=NULL;
              }
              $string_separada=array(
                  $cadena1,
                  $cadena2
              );
              return $string_separada;
          }

        }

        // inicio combo dependiente ejes regional y municipal
        function getEjeMunipalbyER(){
          $id_eje_regional=$_POST['eje_regional'];
          $ejes_municipales = $this->model->getEjeMunipalbyER($id_eje_regional);
          echo '<option value="">Seleccione</option>';
          foreach($ejes_municipales as $row){
            $eje_municipal=new Estructura();
            $eje_municipal=$row;
            echo '<option value="'.$eje_municipal->id_eje_municipal.'">'.$eje_municipal->descripcion.'</option>';
          }
        }
      }

    

?>