<?php
include_once 'sesiones/session_admin.php';

    class Tipoaldea extends Controller {
        function __construct(){
            parent::__construct();
           // $this->view->render('main/index');
            //echo "<p>Nuevo Controller</p>";
            $this->view->estructuras=[];            
            
            $this->view->aldeatipos=[];
            
        }

        function render(){ 


           /* $estructuras=$this->model->get();
            $this->view->estructuras=$estructuras;
*/
            $aldeatipos=$this->model->get();
            $this->view->aldeatipos=$aldeatipos;

            $this->view->render('estructura/tipoaldea');        
        }



        function registrarTipoAldea(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
    
            $aldeatipo=$_POST['aldeatipo'];            
          
            
            
            $mensaje="";

            if($var=$this->model->existe($aldeatipo)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                 Tipo de Aldea <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($this->model->insert(['aldeatipo'=>$aldeatipo])){
              
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Tipo de Aldea registrada<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al registrar un tipo de aldea <a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
          }



          function ActualizarTipoAldea(){//se agrega esta linea
          
      
    
            //$id_persona=$_SESSION["id_persona"];
    

            $id_aldea_tipo=$_POST['id_aldea_tipo'];
            $aldeatipo=$_POST['aldeatipoedit'];            
           

            
         
           // var_dump($id_municipio);
            //var_dump( $id_estado,$codigoine, $estado,$id_pais2);
            $mensaje="";


           
    
            if($this->model->update(['id_aldea_tipo'=>$id_aldea_tipo,'aldeatipo'=>$aldeatipo])){    
    
        /*      if ($datos['nomb_archivo'] != "") {
                  copy($datos['ruta'], $datos['destino']);
                 }*/
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Aldea  <b> ".$aldeatipo." </b> Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar Aldea <b> ".$aldeatipo." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }

    }

?>