<?php
include_once 'sesiones/session_admin.php';

    class Roles extends Controller {
        function __construct(){
            parent::__construct(); 
            $this->view->roles=[];
            
        }

        function render(){ 
            //revisar estrutura
                $roles=$this->model->get();
                $this->view->roles=$roles;
                $this->view->render('gestion_usuarios/roles');        
        }



        function RegistrarRol(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
    
            $rol=$_POST['rol'];            
        
                       
            $mensaje="";

            if($var=$this->model->existe($rol)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Rol <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($this->model->insert(['rol'=>$rol])){
              
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Rol agregado<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al agregar un Rol<a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
          }



          function ActualizarRol(){//se agrega esta linea
    
            //$id_persona=$_SESSION["id_persona"];

            $id_rol=$_POST['id_rol'];
            $rol=$_POST['roledit'];            
           

           // var_dump($id_municipio);
            //var_dump( $id_estado,$codigoine, $estado,$id_pais2);
            $mensaje="";
    
            if($this->model->update(['id_rol'=>$id_rol,'rol'=>$rol])){    
    
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Rol <b> ".$rol." </b> Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Actualizar Rol <b> ".$espacio." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }

    }

?>