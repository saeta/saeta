<?php
include_once 'sesiones/session_admin.php';

    class Periodo extends Controller {
        function __construct(){
            parent::__construct();

            $this->view->modulos=[];  
            $this->view->aldeas=[];  
            $this->view->periodoacademico=[];
            
        }

        function render(){ 

          
          /*  $modulos=$this->model->get();
            $this->view->modulos=$modulos;
            
            $aldeas=$this->model->getAldea();
            $this->view->aldeas=$aldeas;
            //getOfertaAcademica*/

            $periodoacademico=$this->model->get();
            $this->view->periodoacademico=$periodoacademico;

           

            $this->view->render('modulos/periodo');        
        }



        function ActivarPeriodoAcademico(){//se agrega esta linea
          
      
            //$id_persona=$_SESSION["id_persona"];
    
            $periodo=$_POST['periodo'];            
            $calendario=$_POST['calendario'];
           
            //date("d/m/Y", strtotime($noticia->publicacion_fecha));
                       
            $mensaje="";

            if($var=$this->model->existe($periodo)){
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Periodo Académico <b>" . $var . "</b> ya EXISTE en la base de datos<a class='alert-link' href='#'></a>
                </div>";
                $this->view->mensaje=$mensaje;
                $this->render();
                exit();
            }
            
            if($this->model->insert(['periodo'=>$periodo,'calendario'=>date("Y/m/d", strtotime($calendario))])){
              
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Periodo Académico agregado<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al agregar un Periodo Académico<a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
          }



          function ActualizarPeriodoAcademico(){//se agrega esta linea
    
            $id_periodo_academico=$_POST['id_periodo_academico'];   
            $periodo=$_POST['periodoo'];            
            $calendario=$_POST['calendarioa'];
            $estatus=$_POST['estatusa'];
           
            $mensaje="";
    
      
                if($this->model->update(['id_periodo_academico'=> $id_periodo_academico,'periodo'=>$periodo,'calendario'=>date("Y/m/d", strtotime($calendario)),
                'estatus'=>$estatus])){
    
      
    
              $mensaje="<div class='alert alert-success alert-dismissable'>
              <button aria-hidden='true' data-dismiss='alert' class='close type='button'>×</button>
              Periodo Académico <b> ".$periodo." </b> Actualizado Correctamente<a class='alert-link' href='#'></a></div>";
            
          }else{
                $mensaje="<div class='alert alert-danger alert-dismissable'>
                <button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
                Ha ocurrido un error al Periodo Académico<b> ".$periodo." </b><a class='alert-link' href='#'></a>
            </div>";
            }
            $this->view->mensaje=$mensaje;
            $this->render();
            
           // echo "Nuevo Alumno Creado";
            //$this->model->insert();//se agrega esta linea
          
          }

    }

?>